<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	// public $plantguid = "";
	// public $plantpoguid = "";
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
	}
	
	public function uploaddata()
	{
		// $this->plantguid = uniqid();
		// $this->plantpoguid = uniqid();

		$data = $this->input->post('data');
		
		$data_array = json_decode($data);

		foreach ($data_array as $tablename => $tabledata) {

			//echo "<pre>";
			//print_r($tablename.'-'.$tabledata);
			//print_r($tabledata);
			
			switch($tablename)
			{
				
				case "tblPATPlantDetail" : $res['Table'][0]['PlantGUID'] = $this->tblPATPlantDetail($tabledata);
				//case "tblPATPlantDetail" : $this->tblPATPlantDetail($tabledata);
				break;
			 	case "tblPATWaterContaminants" : $this->tblPATWaterContaminants($tabledata);
				break;
				case "tblPATPlantAssetFunder" : $this->tblPATPlantAssetFunder($tabledata);
				break;
				case "tblPATPlantProdDetail" : $this->tblPATPlantProdDetail($tabledata);
				break;
				case "tblPATPlantPurificationstep" : $this->tblPATPlantPurificationstep($tabledata);
				break;
				case "tblPATPlantInstitutionalDocument" : $res['Table1'][] = $this->tblPATPlantInstitutionalDocument($tabledata);
				// case "tblPATPlantInstitutionalDocument" : $this->tblPATPlantInstitutionalDocument($tabledata);
				break;
				case "tblPATPODetail" : $res['Table'][0]['PlantPOGUID'] = $this->tblPATPODetail($tabledata);
				 //case "tblPATPODetail" : $this->tblPATPODetail($tabledata);
				break;
				case "tblPATPlantDistribution" : $this->tblPATPlantDistribution($tabledata);
				break;


			}


		}
		// print_r($res);
		// echo json_encode($res);
		// echo json_encode("success");
		//return $res;
		return NULL;

	}


private function tblPATPODetail($tabledata)
{

		foreach ($tabledata as $row) 
		{

			if($row->SamplingOrGift == "")
			{
				$row->SamplingOrGift = 0;	
			}
			if($row->LastWQtestreport == "")
			{
				$row->LastWQtestreport = 0;	
			}
			if($row->UpdatedOn == "")
			{
				$row->UpdatedOn = 0;	
			}
			if($row->Anyother == "")
			{
				$row->Anyother = 0;	
			}
			if($row->Operatorcertificate == "")
			{
				$row->Operatorcertificate = 0;	
			}
			if($row->Microbial == "")
			{
				$row->Microbial = 0;	
			}

    		if($row->ResidualChlorine == "")
			{
				$row->ResidualChlorine = 0;	
			}
			if($row->OperatorSalary == "")
			{
				$row->OperatorSalary = NULL;	
			}
			if($row->PostMonsoonTWLabAdr == "")
			{
				$row->PostMonsoonTWLabAdr = Null;	
			}
			if($row->AssestRepaymentFund == "")
			{
				$row->AssestRepaymentFund = 0;	
			}
			if($row->ConsumerAwareness == "")
			{
				$row->ConsumerAwareness = 0;	
			}
			if($row->pH == "")
			{
				$row->pH = 0;	
			}

			if($row->UV == "")
			{
				$row->UV = 0;	
			}
			if($row->RejectWaterTesting == "")
			{
				$row->RejectWaterTesting = 0;	
			}
			if($row->ContactNumber == "")
			{
				$row->ContactNumber = 0;	
			}
			if($row->ElectricalSafety == "")
			{
				$row->ElectricalSafety = 0;	
			}
			if($row->AuditingAgencyAddress == "")
			{
				$row->AuditingAgencyAddress = NULL;	
			}
			if($row->PostMonsoonRWtestingdate == "")
			{
				$row->PostMonsoonRWtestingdate = Null;	
			}

    		if($row->MiscSecondAmount == "")
			{
				$row->MiscSecondAmount = 0;	
			}
			if($row->Email == "")
			{
				$row->Email = NULL;	
			}
			if($row->PreMonsoonRWLabAdr == "")
			{
				$row->PreMonsoonRWLabAdr = Null;	
			}
			if($row->WaterQuality == "")
			{
				$row->WaterQuality = 0;	
			}
			if($row->PostMonsoonTWtestProof == "")
			{
				$row->PostMonsoonTWtestProof = NULL;	
			}
			if($row->ServiceCharge == "")
			{
				$row->ServiceCharge = 0;	
			}

			if($row->PostMonsoonRWLabadr == "")
			{
				$row->PostMonsoonRWLabadr = Null;	
			}
			if($row->PreMonsoonTWTestingdate == "")
			{
				$row->PreMonsoonTWTestingdate = NULL;	
			}
			if($row->PlantOM == "")
			{
				$row->PlantOM = 0;	
			}
			if($row->PreMonsoonTWProof == "")
			{
				$row->PreMonsoonTWProof = 0;	
			}
			if($row->TDS == "")
			{
				$row->TDS = 0;	
			}
			if($row->UtilizationOther3 == "")
			{
				$row->UtilizationOther3 = 0;	
			}

    		if($row->PostMonsoonTWtestingdate == "")
			{
				$row->PostMonsoonTWtestingdate = 0;	
			}
			if($row->UtilizationOther2 == "")
			{
				$row->UtilizationOther2 = 0;	
			}
			if($row->UtilizationOther4 == "")
			{
				$row->UtilizationOther4 = 0;	
			}
			if($row->UtilizationOther1 == "")
			{
				$row->UtilizationOther1 = 0;	
			}
			if($row->OperatorName == "")
			{
				$row->OperatorName = NULL;	
			}
			if($row->Latitude == "")
			{
				$row->Latitude = 0;	
			}

			if($row->RawWaterOpenwellcovered == "")
			{
				$row->RawWaterOpenwellcovered = 0;	
			}
			if($row->InfrastructureCost == "")
			{
				$row->InfrastructureCost = NULL;	
			}
			if($row->Checkmossoralgee == "")
			{
				$row->Checkmossoralgee = 0;	
			}
			if($row->LiteracyID == "")
			{
				$row->LiteracyID = 0;	
			}
			if($row->CheckRWUtilisation == "")
			{
				$row->CheckRWUtilisation = 0;	
			}
			if($row->IsSynched == "")
			{
				$row->IsSynched = 0;	
			}

    		if($row->ABKeeping == "")
			{
				$row->ABKeeping = 0;	
			}
			if($row->PeakSale == "")
			{
				$row->PeakSale = NULL;	
			}
			if($row->WaterProductionMonthly == "")
			{
				$row->WaterProductionMonthly = Null;	
			}
			if($row->ContactName == "")
			{
				$row->ContactName = 0;	
			}
			if($row->PreMonsoonRWTesting == "")
			{
				$row->PreMonsoonRWTesting = 0;	
			}
			if($row->AgencyAnyOther == "")
			{
				$row->AgencyAnyOther = 0;	
			}
			if($row->WaterBill == "")
			{
				$row->WaterBill = 0;	
			}
			if($row->MiscFirstText == "")
			{
				$row->MiscFirstText = NULL;	
			}
			if($row->PostMonsoonTWtesting == "")
			{
				$row->PostMonsoonTWtesting = 0;	
			}
			if($row->RejectWaterTestProof == "")
			{
				$row->RejectWaterTestProof = 0;	
			}
			if($row->VisitDate == "")
			{
				$row->VisitDate = NULL;	
			}

			if($row->CheckWharvesting == "")
			{
				$row->CheckWharvesting = 0;	
			}

    		if($row->NoOfDays == "")
			{
				$row->NoOfDays = 0;	
			}
			if($row->MiscThirdText == "")
			{
				$row->MiscThirdText = NULL;	
			}
			if($row->WQuantification == "")
			{
				$row->WQuantification = Null;	
			}
			if($row->CheckforReceipts == "")
			{
				$row->CheckforReceipts = 0;	
			}
			if($row->CreatedBy == "")
			{
				$row->CreatedBy = 0;	
			}
			if($row->AuditedBy == "")
			{
				$row->AuditedBy = 0;	
			}

			if($row->Rent == "")
			{
				$row->Rent = 0;	
			}
			if($row->UtilizationActivity4 == "")
			{
				$row->UtilizationActivity4 = NULL;	
			}
			if($row->UtilizationActivity3 == "")
			{
				$row->UtilizationActivity3 = 0;	
			}
			if($row->checkRWQuantification == "")
			{
				$row->checkRWQuantification = 0;	
			}
			if($row->UtilizationActivity2 == "")
			{
				$row->UtilizationActivity2 = NULL;	
			}
			if($row->CleanlinessNearTreatmentPlant == "")
			{
				$row->CleanlinessNearTreatmentPlant = 0;	
			}
    		if($row->UtilizationActivity1 == "")
			{
				$row->UtilizationActivity1 = 0;	
			}
			if($row->Electricityoutage == "")
			{
				$row->Electricityoutage = 0;	
			}
			if($row->ElectricityBill == "")
			{
				$row->ElectricityBill = Null;	
			}
			if($row->MiscellaneousChk == "")
			{
				$row->MiscellaneousChk = 0;	
			}
			if($row->WUtilisation == "")
			{
				$row->WUtilisation = NULL;	
			}
			if($row->Rawwaterproblem == "")
			{
				$row->Rawwaterproblem = 0;	
			}

			if($row->WaterProductionDaily == "")
			{
				$row->WaterProductionDaily = Null;	
			}
			if($row->UpdatedBy == "")
			{
				$row->UpdatedBy = NULL;	
			}
			if($row->CheckRWDisposal == "")
			{
				$row->CheckRWDisposal = 0;	
			}
			if($row->Earthing == "")
			{
				$row->Earthing = 0;	
			}
			if($row->CheckforIECmaterial == "")
			{
				$row->CheckforIECmaterial = 0;	
			}
			if($row->NoOfFault == "")
			{
				$row->NoOfFault = 0;	
			}

    		if($row->Leakage == "")
			{
				$row->Leakage = 0;	
			}
			if($row->CleanlinessinPlant == "")
			{
				$row->CleanlinessinPlant = 0;	
			}
			if($row->TechnicalDowntime == "")
			{
				$row->TechnicalDowntime = Null;	
			}
			if($row->OPExservicecharge == "")
			{
				$row->OPExservicecharge = 0;	
			}
			if($row->MiscFirstAmount == "")
			{
				$row->MiscFirstAmount = NULL;	
			}
			if($row->AuditingAgency == "")
			{
				$row->AuditingAgency = 0;	
			}

			if($row->Presenceofpuddle == "")
			{
				$row->Presenceofpuddle = 0;	
			}
			if($row->MiscSecondText == "")
			{
				$row->MiscSecondText = NULL;	
			}
			if($row->CreatedOn == "")
			{
				$row->CreatedOn = 0;	
			}
			if($row->WDisposal == "")
			{
				$row->WDisposal = 0;	
			}
			if($row->Longitude == "")
			{
				$row->Longitude = NULL;	
			}
			if($row->EducationAnyOther == "")
			{
				$row->EducationAnyOther = Null;	
			}

    		if($row->POGUID == "")
			{
				$row->POGUID = 0;	
			}
			if($row->PreMonsoonRWTestProof == "")
			{
				$row->PreMonsoonRWTestProof = NULL;	
			}
			if($row->IsComplete == "")
			{
				$row->IsComplete = 0;	
			}
			if($row->WaterTreatmentPlantCost == "")
			{
				$row->WaterTreatmentPlantCost = 0;	
			}
			if($row->PostMonsoonRWTesting == "")
			{
				$row->PostMonsoonRWTesting = 0;	
			}
			if($row->DesignationID == "")
			{
				$row->DesignationID = 0;	
			}
			if($row->TransportExpense == "")
			{
				$row->TransportExpense = 0;	
			}
			if($row->PreMonsoonRWTestingdate == "")
			{
				$row->PreMonsoonRWTestingdate = NULL;	
			}
			if($row->LeakageEdit == "")
			{
				$row->LeakageEdit = 0;	
			}
			if($row->ChemicalAndOtherConsumable == "")
			{
				$row->ChemicalAndOtherConsumable = 0;	
			}
			if($row->AssestRenewalFund == "")
			{
				$row->AssestRenewalFund = NULL;	
			}
			if($row->IsTablet == "")
			{
				$row->IsTablet = 0;	
			}

    		if($row->Covered == "")
			{
				$row->Covered = 0;	
			}
			if($row->MiscThirdAmount == "")
			{
				$row->MiscThirdAmount = NULL;	
			}
			if($row->OPEx == "")
			{
				$row->OPEx = Null;	
			}
			if($row->MembraneChoke == "")
			{
				$row->MembraneChoke = 0;	
			}
			if($row->Insidetheplant == "")
			{
				$row->Insidetheplant = 0;	
			}
			if($row->opExSCMRAssetRepayment == "")
			{
				$row->opExSCMRAssetRepayment = 0;	
			}
			if($row->GeneratorMaintainance == "")
			{
				$row->GeneratorMaintainance = 0;	
			}
			if($row->SalesDayLost == "")
			{
				$row->SalesDayLost = NULL;	
			}
			if($row->PreMonsoonTWTesting == "")
			{
				$row->PreMonsoonTWTesting = 0;	
			}
			if($row->RawWaterBoreWellCasing == "")
			{
				$row->RawWaterBoreWellCasing = 0;	
			}
			if($row->AnyotherEdit == "")
			{
				$row->AnyotherEdit = NULL;	
			}
			if($row->OPExservicechargemaintenance == "")
			{
				$row->OPExservicechargemaintenance = Null;	
			}
    		if($row->PreMonsoonTWLabAdr == "")
			{
				$row->PreMonsoonTWLabAdr = 0;	
			}
			if($row->RejectWatertestingdate == "")
			{
				$row->RejectWatertestingdate = NULL;	
			}
			if($row->PostMonsoonRWTestProof == "")
			{
				$row->PostMonsoonRWTestProof = Null;	
			}
			
			if($row->RejectWaterLabAdr == "")
			{
				$row->RejectWaterLabAdr = NULL;	
			}
			if($row->MotorRepair== "")
			{
				$row->MotorRepair= 0;	
			}

												
			$plantGUID = $row->PlantGUID;

			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatpodetail)->num_rows();

			// echo "asdasd".$sql_count; die();
			if($sql_count > 0)
			{
				// die('cvcxv');
				$this->db->update('tblpatpodetail', $row, 'PlantGUID',$row->PlantGUID);
				return $row->POGUID;
			}
			else
			{
				// print_r($row); die();
				// $row->POGUID = uniqid();
				// $row->PlantGUID = $this->plantguid;
				$this->db->insert('tblpatpodetail', $row);
				// return array("PlantPOGUID"=>$row->POGUID);
				return $row->POGUID;
			}

		}
	}



private function tblPATPlantInstitutionalDocument($tabledata)
{

	$docs = array();
		foreach ($tabledata as $row) 
		{
			if($row->PlantInstitutionalDocumentID == "")
			{
				$row->PlantInstitutionalDocumentID = 0;	
			}
			if($row->DocumentName == "")
			{
				$row->DocumentName = NULL;	
			}
			if($row->IsDeleted == "")
			{
				$row->IsDeleted = 0;	
			}
									
			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantinstitutionaldocument)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantinstitutionaldocument', $row, 'PlantGUID',$row->PlantGUID);
			}
			else
			{
				// $row->PlantGUID = $this->plantguid;
				$this->db->insert('tblpatplantinstitutionaldocument', $row);
			}

			$docs = ['DOCUMENTNAME'=>$row->DocumentName, 'PLANTINSTITUTIONALDOCUMENTID'=>$row->PlantInstitutionalDocumentID];
		}
		return $docs;
	}


	private function tblPATPlantPurificationstep($tabledata)
	{

		foreach ($tabledata as $row) 
		{
			if($row->PlantPurificationstepID == "")
			{
				$row->PlantPurificationstepID = 0;	
			}
			if($row->PlantGUID == "")
			{
				$row->PlantGUID = 0;	
			}
			if($row->IsDeleted == "")
			{
				$row->IsDeleted = 0;	
			}
									
			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantpurificationstep)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantpurificationstep', $row, 'PlantGUID',$row->PlantGUID);
			}
			else
			{
				// $row->PlantGUID = $this->plantguid;
				$this->db->insert('tblpatplantpurificationstep', $row);
			}
		}
	}


private function tblPATPlantProdDetail($tabledata)
{

		foreach ($tabledata as $row) 
		{
			if($row->POGUID == "")
			{
				$row->POGUID = 0;	
			}
			if($row->DistributionplantID == "")
			{
				$row->DistributionplantID = 0;	
			}
			if($row->Price == "")
			{
				$row->Price = 0;	
			}
			if($row->Volume == "")
			{
				$row->Volume = 0;	
			}
			if($row->HomeDeliveryPrice == "")
			{
				$row->HomeDeliveryPrice = 0;	
			}
						
			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantproddetail)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantproddetail', $row, 'PlantGUID',$row->PlantGUID);
			}
			else
			{
				// $row->PlantProdDetailGUID = uniqid();
				// $row->PlantGUID = $this->plantguid;
				// $row->POGUID = $this->plantpoguid;
				$this->db->insert('tblpatplantproddetail', $row);
			}
		}
	}
	


	private function tblPATPlantAssetFunder($tabledata)
	{

		foreach ($tabledata as $row) 
		{
			if($row->AssetFunderID == "")
			{
				$row->AssetFunderID = 0;	
			}
			if($row->FlagID == "")
			{
				$row->FlagID = 0;	
			}
			if($row->POGUID == "")
			{
				$row->POGUID = 0;	
			}
						

			$AssetFunderID 	= $row->AssetFunderID;
			$PlantGUID 		= $row->PlantGUID;
			$FlagID 		= $row->FlagID;
			$id_tblpatplantassetfunderAssetFunderID = $row->id_tblpatplantassetfunder;

			$this->db->select('*');
			$this->db->where(PlantGUID,$PlantGUID);
			$this->db->where(AssetFunderID,$AssetFunderID);
			$this->db->where(FlagID,$FlagID); 
			$sql_count = $this->db->get(tblpatplantassetfunder)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantassetfunder', $row, 'id_tblpatplantassetfunder',$row->id_tblpatplantassetfunder);
				
			}
			else
			{
				// $row->PlantGUID = $this->plantguid;
				// $row->POGUID = $this->plantpoguid;
				$this->db->insert('tblpatplantassetfunder', $row);
			}
		}
	}

	private function tblPATWaterContaminants($tabledata)
	{
		foreach ($tabledata as $row) 
		{
 		   if($row->WaterContaminantsID == "")
			{
				$row->WaterContaminantsID = 0;	
			}
			if($row->PlantGUID == "")
			{
				$row->PlantGUID = 0;	
			}
			if($row->WaterQualityChallengeID == "")
			{
				$row->WaterQualityChallengeID = 0;	
			}
			if($row->IsDeleted == "")
			{
				$row->IsDeleted = 0;	
			}
			
			$plantGUID = $row->PlantGUID;

			$this->db->select('*');
			$this->db->where('PlantGUID',$plantGUID);

			$sql_count = $this->db->get('tblpatwatercontaminants')->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('	tblpatwatercontaminants', $row, 'PlantGUID',$row->PlantGUID);
			}
			else
			{
				// $row->PlantGUID = $this->plantguid;
				$this->db->insert('tblpatwatercontaminants', $row);
			}
		}

	}



private function tblPATPlantDetail($tabledata)
{

	foreach ($tabledata as $row) 
		{	
			if($row->SWNID == "")
			{
				$row->SWNID = null;	
			}
			if($row->VisitDate == "")
			{
				$row->VisitDate = null;	
			}
			if($row->Email == "")
			{
				$row->Email = null;	
			}
			if($row->MailingAddress == "")
			{
				$row->MailingAddress = null;	
			}
			if($row->PlantLocation == "")
			{
				$row->PlantLocation = null;	
			}
			if($row->PlantLatitude == "")
			{
				$row->PlantLatitude = null;	
			}
			if($row->PlantLongitude == "")
			{
				$row->PlantLongitude = null;	
			}
			if($row->PlantPhotoLink == "")
			{
				$row->PlantPhotoLink = null;	
			}
			if($row->UserID == "")
			{
				$row->UserID = null;	
			}
			if($row->AgencyID == "")
			{
				$row->AgencyID = null;	
			}
			if($row->LocationType == "")
			{
				$row->LocationType = null;	
			}
			if($row->CountryID == "")
			{
				$row->CountryID = null;	
			}
			if($row->StateID == "")
			{
				$row->StateID = null;	
			}
			if($row->District == "")
			{
				$row->District = null;	
			}
			if($row->BlockName == "")
			{
				$row->BlockName = null;	
			}
			if($row->VillageName == "")
			{
				$row->VillageName = null;	
			}
			if($row->PinCode == "")
			{
				$row->PinCode = 0;	
			}
			if($row->SC == "")
			{
				$row->SC = 0;	
			}
			if($row->ST == "")
			{
				$row->ST = 0;	
			}
			if($row->OBC == "")
			{
				$row->OBC = 0;	
			}
			if($row->General == "")
			{
				$row->General = 0;	
			}
			if($row->AllInclusion == "")
			{
				$row->AllInclusion = 0;	
			}
			if($row->OperatorName == "")
			{
				$row->OperatorName = null;	
			}
			if($row->DesignationID == "")
			{
				$row->DesignationID = 0;	
			}
			if($row->ContactNumber == "")
			{
				$row->ContactNumber = 0;	
			}
			if($row->LiteracyID == "")
			{
				$row->LiteracyID = 0;	
			}
			if($row->PlantSpecificationID == "")
			{
				$row->PlantSpecificationID = 0;	
			}
			if($row->PlantManufacturerID == "")
			{
				$row->PlantManufacturerID = 0;	
			}
			if($row->ManufacturerName == "")
			{
				$row->ManufacturerName = null;	
			}
			if($row->EstablishmentDate == "")
			{
				$row->EstablishmentDate = null;	
			}
			if($row->AgeOfPlant == "")
			{
				$row->AgeOfPlant = null;	
			}
			if($row->RemoteMonitoringSystem == "")
			{
				$row->RemoteMonitoringSystem = null;	
			}
			if($row->LandCost == "")
			{
				$row->LandCost = null;	
			}
			if($row->LandOther == "")
			{
				$row->LandOther = null;	
			}
			if($row->MachineryCost == "")
			{
				$row->MachineryCost = null;	
			}
			if($row->MachineryOther == "")
			{
				$row->MachineryOther = null;	
			}
			if($row->BuildingCost == "")
			{
				$row->BuildingCost = null;	
			}
			if($row->BuildingOther == "")
			{
				$row->BuildingOther = null;	
			}
			if($row->RawWaterSourceCost == "")
			{
				$row->RawWaterSourceCost = null;	
			}
			if($row->RawWaterSourceOther == "")
			{
				$row->RawWaterSourceOther = null;	
			}
			if($row->ElectricityCost == "")
			{
				$row->ElectricityCost = null;	
			}
			if($row->ElectricityOther == "")
			{
				$row->ElectricityOther = null;	
			}
			if($row->Distribution == "")
			{
				$row->Distribution = 0;	
			}
			if($row->NoOfhhregistered == "")
			{
				$row->NoOfhhregistered = null;	
			}
			if($row->AvgMonthlyCards == "")
			{
				$row->AvgMonthlyCards = null;	
			}
			if($row->GramPanchayatApproval == "")
			{
				$row->GramPanchayatApproval = null;	
			}
			if($row->LegalElectricityConnection == "")
			{
				$row->LegalElectricityConnection = null;	
			}
			if($row->LandApproval == "")
			{
				$row->LandApproval = null;	
			}
			if($row->RawWaterSourceApproval == "")
			{
				$row->RawWaterSourceApproval = null;	
			}
			if($row->RejectWaterDischargeApproval == "")
			{
				$row->RejectWaterDischargeApproval = null;	
			}
			if($row->ServiceProviderApproval == "")
			{
				$row->ServiceProviderApproval = null;	
			}
			if($row->OperatorQualification == "")
			{
				$row->OperatorQualification = null;	
			}
			if($row->CreatedBy == "")
			{
				$row->CreatedBy = 0;	
			}
			if($row->CreatedOn == "")
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedBy == "")
			{
				$row->UpdatedBy = 0;	
			}
			if($row->UpdatedOn == "")
			{
				$row->UpdatedOn = null;	
			}
			if($row->IsTablet == "")
			{
				$row->IsTablet = 0;	
			}
			if($row->WaterBrandName == "")
			{
				$row->WaterBrandName = null;	
			}
			if($row->PlantSpecificationAnyOther == "")
			{
				$row->PlantSpecificationAnyOther = null;	
			}
			if($row->PAssetSourceFundedBy == "")
			{
				$row->PAssetSourceFundedBy = null;	
			}
			if($row->auditAgencyOther == "")
			{
				$row->auditAgencyOther = null;	
			}
			if($row->EducationAnyOther == "")
			{
				$row->EducationAnyOther = null;	
			}
			if($row->ContaminantAnyOther == "")
			{
				$row->ContaminantAnyOther = null;	
			}
			if($row->Population == "")
			{
				$row->Population = null;	
			}
			if($row->NoOfHousehold == "")
			{
				$row->NoOfHousehold = null;	
			}
			if($row->NoOfHouseholdWithin2km == "")
			{
				$row->NoOfHouseholdWithin2km = null;	
			}
			if($row->BPL == "")
			{
				$row->BPL = null;	
			}
			

			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantdetail)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantdetail', $row, 'PlantGUID',$row->PlantGUID);
				return $row->PlantGUID;
				

			}
			else
			{
				// print_r($row);
				// $this->plantguid = uniqid(); 
				// $row->PlantGUID = $this->plantguid;
				// echo $this->plantguid; 
				// die();
				$this->db->insert('tblpatplantdetail', $row);
				// return array("PlantGUID"=>$row->PlantGUID);
				return $row->PlantGUID;
			}
		}

}

private function tblPATPlantDistribution($tabledata)
	{			
		foreach ($tabledata as $row) 
		{
			if($row->DPID == "")
			{
				$row->DPID = 0;	
			}
			if($row->Distance == "")
			{
				$row->Distance = 0;	
			}
			if($row->DPName == "")
			{
				$row->DPName = NULL;	
			}
			if($row->PlantGUID == "")
			{
				$row->PlantGUID = 0;	
			}
		
			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID); 
			$sql_count = $this->db->get(tblpatplantdistribution)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantdistribution', $row, 'PlantGUID',$row->PlantGUID);
			}
			else
			{
				// $row->PlantGUID = $this->plantguid;
				// $row->POGUID = $this->plantpoguid;
				$this->db->insert('tblpatplantdistribution', $row);
			}
		}
	}

}