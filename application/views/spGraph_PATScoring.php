BEGIN
DECLARE IsDocument INT;
if (p_Tab='Tab0')
then
CREATE TEMPORARY TABLE Temp
(	
PlantGUID VARCHAR(50),
LocalOperation INT, 
GramPanchayatMunicipalCorporationApproval INT, 
LegalElecricityConnection INT, 
ApprovalOfPreisesAndLand INT, 
AprovalForRawSourceWater INT,
ApprovalForRejectWaterDischarge INT,
ServiceProviders INT
);

SET IsDocument = (SELECT COUNT(PlantInstitutionalDocumentID) FROM tblPATPlantInstitutionalDocument WHERE PlantGUID=p_PlantGuid);

if(IsDocument>0)

THEN

Insert Into Temp

Select tblPATPlantDetail.PlantGUID ,CASE WHEN OperatorQualification='True' THEN 3 ELSE 0 END AS LocalOperation, 
CASE WHEN GramPanchayatApproval='True' THEN 3 ELSE 0 END AS GramPanchayatMunicipalCorporationApproval, 
CASE WHEN LegalElectricityConnection='True' THEN 3 ELSE 0 END AS LegalElecricityConnection,
CASE WHEN LandApproval='True' THEN 3 Else 0 END AS ApprovalOfPreisesAndLand,
CASE WHEN RawWaterSourceApproval='True' THEN 3 ELSE 0 END AS AprovalForRawSourceWater, 
Case  WHEN RejectWaterDischargeApproval='True' THEN 2 ELSE 0 END AS ApprovalForRejectWaterDischarge, 
Case  WHEN ServiceProviderApproval='True' THEN 3 ELSE 0 END AS ServiceProviders 
from tblpatPlantDetail WHERE PLANTGUID=p_PlantGuid;

else

Insert Into Temp

Select tblPATPlantDetail.PlantGUID ,
CASE WHEN OperatorQualification='True' THEN 1 ELSE 0 END AS LocalOperation,  
CASE WHEN GramPanchayatApproval='True' THEN 1 ELSE 0 END AS GramPanchayatMunicipalCorporationApproval, 
CASE WHEN LegalElectricityConnection='True' THEN 1 ELSE 0 END AS LegalElecricityConnection,
CASE WHEN LandApproval='True' THEN 1 Else 0 END AS ApprovalOfPreisesAndLand,
CASE WHEN RawWaterSourceApproval='True' THEN 1 ELSE 0 END AS AprovalForRawSourceWater, 
Case  WHEN RejectWaterDischargeApproval='True' THEN 1 ELSE 0 END AS ApprovalForRejectWaterDischarge, 
Case  WHEN ServiceProviderApproval='True' THEN 1 ELSE 0 END AS ServiceProviders 
from tblpatPlantDetail WHERE PLANTGUID=p_PlantGuid;

END IF;

SELECT T.PlantGUID ,IFNULL((T.Coverage+T.Affordability),0) AS Social,IfNULL((Temp.ApprovalForRejectWaterDischarge+Temp.ApprovalOfPreisesAndLand+Temp.AprovalForRawSourceWater+Temp.GramPanchayatMunicipalCorporationApproval+
Temp.LegalElecricityConnection+Temp.LocalOperation+Temp.ServiceProviders),0) AS Institutional,IFNULL((Opera.TrainingCapacityAndBuilding+Opera.WaterQuality+Opera.ReliabilityOfOperations+Opera.QualityAssurance),0) AS Operational,IFNULL(Finance.FinancialSustainability,0) AS Financial,
 IFNULL((Environ.RejctWaterQuantification+Environ.RejectWaterUtilisation+Environ.RejectWaterDisposal+Environ.GroundWaterRecharge),0) AS Environmental FROM 

(SELECT t.PlantGUID, t.Inclusion+t.VolumOfWaterSold+t.Adoption AS Coverage, CASE WHEN (SUM(t.Price)/COUNT(t.Price))<4 THEN 4

WHEN (SUM(t.Price)/COUNT(t.Price))<6 AND (SUM(t.Price)/COUNT(t.Price))>3 THEN 3

WHEN (SUM(t.Price)/COUNT(t.Price))<7 AND (SUM(t.Price)/COUNT(t.Price))>4 THEN 2

WHEN (SUM(t.Price)/COUNT(t.Price))<8 AND (SUM(t.Price)/COUNT(t.Price))>5 THEN 1

ELSE 0 END +t.DPATM+t.HomeDeliveryPrice AS Affordability FROM (SELECT tblPATPlantProdDetail.PlantGUID ,CASE WHEN tblPATPlantDetail.NoOfHousehold > 800  THEN 4

  WHEN tblPATPlantDetail.NoOfHousehold <= 800 AND tblPATPlantDetail.NoOfHousehold > 600  THEN 3

  WHEN tblPATPlantDetail.NoOfHousehold >= 400 AND tblPATPlantDetail.NoOfHousehold <= 600 THEN 2

  WHEN tblPATPlantDetail.NoOfHousehold >= 200 AND tblPATPlantDetail.NoOfHousehold <= 400 THEN 1

END AS Adoption , CASE WHEN AllInclusion='True' THEN 2 ELSE 0 END AS Inclusion , CASE WHEN tblPATPODetail.WaterProductionDaily >8000 THEN 4

WHEN tblPATPODetail.WaterProductionDaily <=8000 AND tblPATPODetail.WaterProductionDaily>=6001 THEN 3

WHEN tblPATPODetail.WaterProductionDaily <6000 AND tblPATPODetail.WaterProductionDaily > 4000 THEN 2

WHEN tblPATPODetail.WaterProductionDaily < 4000 AND tblPATPODetail.WaterProductionDaily > 0 THEN 1 END AS VolumOfWaterSold,



	CASE WHEN tblPATPlantProdDetail.Price<4 THEN 4 

	WHEN tblPATPlantProdDetail.Price>3 AND tblPATPlantProdDetail.Price<6 THEN 3

	WHEN tblPATPlantProdDetail.Price>4 AND tblPATPlantProdDetail.Price<7 THEN 2

	WHEN tblPATPlantProdDetail.Price>5 AND tblPATPlantProdDetail.Price<8 THEN 1

	ELSE 0 END AS DPATM,

	CASE WHEN tblPATPlantProdDetail.HomeDeliveryPrice<15 THEN 2 

	WHEN tblPATPlantProdDetail.HomeDeliveryPrice=15  THEN 1

	ELSE 0 END AS HomeDeliveryPrice,

	

 tblPATPlantProdDetail.Price FROM tblPATPlantProdDetail INNER JOIN tblPATPlantDetail ON tblPATPlantDetail.PlantGUID=tblPATPlantProdDetail.plantguid INNER JOIN tblPATPODetail ON tblPATPODetail.POGUID = tblPATPlantProdDetail.POGUID WHERE tblPATPODetail.POGUID=p_POGUID AND  tblPATPODetail.PlantGUID=p_PlantGuid AND tblPATPlantProdDetail.DistributionplantID>0)t GROUP BY t.Inclusion,t.VolumOfWaterSold,t.Adoption ,t.DPATM, t.HomeDeliveryPrice,t.PlantGUID)T 

	

	 

INNER JOIN (SELECT TB.PlantGUID,(TB.ElectricalSafety+WaterQuality+TB.PlantOM+TB.ConsumerAwareness+TB.ABKeeping) AS TrainingCapacityAndBuilding,WaterQuality,(QA.RawWaterSource+QA.TreatedWaterTank+QA.PlantHygieneInside+QA.PlantHygieneOutside+QA.WaterFillStation) AS QualityAssurance,(RO.TechnicalDownTime+RO.SalesDownTime) AS ReliabilityOfOperations FROM 

(SELECT PlantGUID,CASE WHEN ElectricalSafety='True' AND Earthing='True' THEN 1 ELSE 0 END AS ElectricalSafety, PlantOM, CASE WHEN WaterQuality='True' AND LastWQTestReport='True' THEN 1 ELSE 0 END  AS WaterQuality,ConsumerAwareness,CASE WHEN ABKeeping='True' AND CheckForReceipts='True' THEN 1 ELSE 0 END ABKeeping FROM tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)TB INNER JOIN

(SELECT PlantGUID,CASE WHEN RawWaterOpenwellcovered='True' AND RawWaterBoreWellCasing='True' THEN 1 ELSE 0 END AS RawWaterSource,CASE WHEN InsideThePlant='True' AND Covered='True' THEN 1 ELSE 0 END AS TreatedWaterTank,CleanlinessinPlant AS PlantHygieneInside, CleanlinessNearTreatmentPlant AS PlantHygieneOutside,CASE WHEN Checkmossoralgee='True' THEN 0 ELSE 1 END AS WaterFillStation  FROM tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)QA ON TB.PlantGUID=QA.PlantGUID

INNER JOIN		 

(SELECT PlantGUID,CASE WHEN SalesDayLost>5 THEN 0 WHEN SalesDayLost>4 AND SalesDayLost<5 THEN 1 WHEN SalesDayLost<4 THEN 2 END AS SalesDownTime,CASE WHEN TechnicalDownTime<=3 THEN 3 WHEN SalesDayLost>3 AND SalesDayLost<6 THEN 1 WHEN SalesDayLost>7 THEN 0 
END AS TechnicalDownTime FROM tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)RO ON QA.PLANTGUID=RO.PlantGUID)Opera ON Opera.PlantGUID = T.PlantGUID

INNER JOIN (SELECT tblPATPODetail.PlantGUID,CASE WHEN OPExservicechargemaintenance>=3500 THEN 20 

WHEN OPExservicechargemaintenance>=3000 AND OPExservicechargemaintenance<3500 THEN 15 

WHEN OPExservicechargemaintenance>=2500 AND OPExservicechargemaintenance<3000 THEN 10

WHEN OPExservicechargemaintenance<2500 THEN 5 END AS FinancialSustainability FROM tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)Finance ON Finance.PlantGUID=Opera.PlantGUID

INNER JOIN (SELECT PlantGUID,CASE WHEN WQuantification > 50  THEN 0

  WHEN WQuantification <= 50 AND WQuantification > 45  THEN 1

  WHEN WQuantification >= 40 AND WQuantification <= 45 THEN 2

  WHEN WQuantification >= 35 AND WQuantification <= 40 THEN 3

  WHEN WQuantification >= 30 AND WQuantification <= 35 THEN 4

  WHEN WQuantification >= 25 AND WQuantification <= 30 THEN 5

END AS RejctWaterQuantification,CASE WHEN WUtilisation > 50  THEN 5

  WHEN WUtilisation <= 50 AND WUtilisation > 45  THEN 4

  WHEN WUtilisation >= 40 AND WUtilisation <= 45 THEN 3

  WHEN WUtilisation >= 35 AND WUtilisation <= 40 THEN 2

  WHEN WUtilisation >= 30 AND WUtilisation <= 35 THEN 1

  WHEN WUtilisation <= 30 THEN 0

END as RejectWaterUtilisation,
CASE WHEN CheckRWDisposal='True' THEN 5 ELSE 0 END AS RejectWaterDisposal, 
CASE WHEN CheckWharvesting='True' THEN 5 ELSE 0 END AS GroundWaterRecharge 
FROM tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)Environ ON Environ.PlantGUID=Opera.PlantGUID

INNER JOIN Temp ON Temp.PlantGUID=Environ.PlantGUID;

DROP TABLE Temp;

ELSEIF(p_Tab='Tab1')

THEN

select case when (NoOfhhregistered/ NoOfHousehold)*100 >=80 then 10
when (NoOfhhregistered/ NoOfHousehold)*100 >=60 and <80 then 8
when (NoOfhhregistered/ NoOfHousehold)*100 >=40 and < 60 then 6
when (NoOfhhregistered/ NoOfHousehold)*100 >=20 and < 40 then 4
when (NoOfhhregistered/ NoOfHousehold)*100 < 20 then 0 else 0 end as coverage
  where PlantGUID = p_PlantGUID;

-- SELECT t.PlantGUID, t.Inclusion+t.VolumOfWaterSold+t.Adoption AS Coverage, 
-- (CASE WHEN (SUM(t.Price)/COUNT(t.Price))<4 THEN 4

-- WHEN (SUM(t.Price)/COUNT(t.Price))<6 AND (SUM(t.Price)/COUNT(t.Price))>3 THEN 3

-- WHEN (SUM(t.Price)/COUNT(t.Price))<7 AND (SUM(t.Price)/COUNT(t.Price))>4 THEN 2

-- WHEN (SUM(t.Price)/COUNT(t.Price))<8 AND (SUM(t.Price)/COUNT(t.Price))>5 THEN 1

-- ELSE 0 END) +t.DPATM+t.HomeDeliveryPrice AS Affordability FROM (SELECT tblPATPlantProdDetail.PlantGUID ,CASE WHEN tblPATPlantDetail.NoOfHousehold > 800  THEN 4

--   WHEN tblPATPlantDetail.NoOfHousehold <= 800 AND tblPATPlantDetail.NoOfHousehold > 600  THEN 3

--   WHEN tblPATPlantDetail.NoOfHousehold >= 400 AND tblPATPlantDetail.NoOfHousehold <= 600 THEN 2

--   WHEN tblPATPlantDetail.NoOfHousehold >= 200 AND tblPATPlantDetail.NoOfHousehold <= 400 THEN 1

-- END AS Adoption , CASE WHEN AllInclusion='True' THEN 2 ELSE 0 END AS Inclusion , CASE WHEN tblPATPODetail.WaterProductionDaily >8000 THEN 4

-- WHEN tblPATPODetail.WaterProductionDaily <=8000 AND tblPATPODetail.WaterProductionDaily>=6001 THEN 3

-- WHEN tblPATPODetail.WaterProductionDaily <6000 AND tblPATPODetail.WaterProductionDaily > 4000 THEN 2

-- WHEN tblPATPODetail.WaterProductionDaily < 4000 AND tblPATPODetail.WaterProductionDaily > 0 THEN 1 END AS VolumOfWaterSold,

-- CASE WHEN tblPATPlantProdDetail.Price<4 THEN 4 

-- WHEN tblPATPlantProdDetail.Price>3 AND tblPATPlantProdDetail.Price<6 THEN 3

-- WHEN tblPATPlantProdDetail.Price>4 AND tblPATPlantProdDetail.Price<7 THEN 2

-- WHEN tblPATPlantProdDetail.Price>5 AND tblPATPlantProdDetail.Price<8 THEN 1
-- ELSE 0 END AS DPATM,
-- CASE WHEN tblPATPlantProdDetail.HomeDeliveryPrice<15 THEN 2 
-- WHEN tblPATPlantProdDetail.HomeDeliveryPrice=15  THEN 1
-- ELSE 0 END AS HomeDeliveryPrice,tblPATPlantProdDetail.Price 
-- FROM tblPATPlantProdDetail INNER JOIN tblPATPlantDetail 
-- ON tblPATPlantDetail.PlantGUID=tblPATPlantProdDetail.plantguid INNER JOIN tblPATPODetail 
-- ON tblPATPODetail.POGUID = tblPATPlantProdDetail.POGUID WHERE tblPATPODetail.POGUID=p_POGUID 
-- AND tblPATPODetail.PlantGUID=p_PlantGuid AND tblPATPlantProdDetail.DistributionplantID>0)t 
-- GROUP BY t.Inclusion,t.VolumOfWaterSold,t.Adoption ,t.DPATM, t.HomeDeliveryPrice,t.PlantGUID;


elseif (p_Tab='Tab2')



then

select (TB.ElectricalSafety+WaterQuality+TB.PlantOM+TB.ConsumerAwareness+TB.ABKeeping) AS TrainingCapacityAndBuilding,
WaterQuality=5,
(QA.RawWaterSource+QA.TreatedWaterTank+QA.PlantHygieneInside+QA.PlantHygieneOutside+QA.WaterFillStation) AS QualityAssurance,
(RO.TechnicalDownTime+RO.SalesDownTime) AS ReliabilityOfOperations FROM 

(select PlantGUID,CASE WHEN ElectricalSafety='True' THEN 1 ELSE 0 END AS ElectricalSafety, CASE WHEN PlantOM='True' THEN 1 ELSE 0 END AS PlantOM, CASE WHEN WaterQuality='True' THEN 1 ELSE 0 END  AS WaterQuality,CASE WHEN ConsumerAwareness='TRUE' THEN 1 else 0 END AS ConsumerAwareness,CASE WHEN ABKeeping='True' THEN 1 ELSE 0 END ABKeeping from tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)TB INNER JOIN

(SELECT PlantGUID,CASE WHEN RawWaterOpenwellcovered='True' AND RawWaterBoreWellCasing='True' THEN 1 ELSE 0 END AS RawWaterSource,CASE WHEN InsideThePlant='True' AND Covered='True' THEN 1 ELSE 0 END AS TreatedWaterTank,CASE WHEN  CleanlinessinPlant='TRUE' 
AND Leakage='TRUE' THEN 1 ELSE 0 END AS PlantHygieneInside, CleanlinessNearTreatmentPlant AS PlantHygieneOutside,case when Checkmossoralgee='True' Then 0 Else 1 END AS WaterFillStation  from tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)QA ON TB.PlantGUID=QA.PlantGUID

Inner JOIN		 

(SELECT PlantGUID,CASE WHEN SalesDayLost>5 THEN 0 WHEN SalesDayLost>4 AND SalesDayLost<5 THEN 1 WHEN SalesDayLost<4 THEN 2 END AS SalesDownTime,CASE WHEN TechnicalDownTime<=3 THEN 3 WHEN SalesDayLost>3 AND SalesDayLost<6 THEN 1 WHEN SalesDayLost>7 THEN 0 
END AS TechnicalDownTime from tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID)RO ON QA.PLANTGUID=RO.PlantGUID;


elseif (p_Tab='Tab3')

then

select CASE WHEN OPExservicechargemaintenance>=3500 THEN 20 

WHEN OPExservicechargemaintenance>=3000 AND OPExservicechargemaintenance<3500 THEN 15 

WHEN OPExservicechargemaintenance>=2500 AND OPExservicechargemaintenance<3000 THEN 10

WHEN OPExservicechargemaintenance<2500 THEN 5 END AS FinancialSustainability FROM tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID;

elseif (p_Tab='Tab4')



then

SET IsDocument = (Select Count(PlantInstitutionalDocumentID) From tblPATPlantInstitutionalDocument 

WHERE PlantGUID=p_PlantGuid);

if(IsDocument>0)

THEN

Select CASE WHEN OperatorQualification='True' THEN 3 ELSE 0 END AS LocalOperation, 
CASE WHEN GramPanchayatApproval='True' THEN 3 ELSE 0 END AS GramPanchayatMunicipalCorporationApproval, 
CASE WHEN LegalElectricityConnection='True' THEN 3 ELSE 0 END AS LegalElecricityConnection,
CASE WHEN LandApproval='True' THEN 3 Else 0 END AS ApprovalOfPreisesAndLand,
CASE WHEN RawWaterSourceApproval='True' THEN 3 ELSE 0 END AS AprovalForRawSourceWater, 
Case  WHEN RejectWaterDischargeApproval='True' THEN 2 ELSE 0 END AS ApprovalForRejectWaterDischarge, 
Case  WHEN ServiceProviderApproval='True' THEN 3 ELSE 0 END AS ServiceProviders 
from tblpatPlantDetail WHERE PLANTGUID=p_PlantGuid;


else


Select CASE WHEN OperatorQualification='True' THEN 1 ELSE 0 END AS LocalOperation,  
CASE WHEN GramPanchayatApproval='True' THEN 3 ELSE 0 END AS GramPanchayatMunicipalCorporationApproval, 
CASE WHEN LegalElectricityConnection='True' THEN 3 ELSE 0 END AS LegalElecricityConnection,
CASE WHEN LandApproval='True' THEN 1 Else 0 END AS ApprovalOfPreisesAndLand,
CASE WHEN RawWaterSourceApproval='True' THEN 1 ELSE 0 END AS AprovalForRawSourceWater, 
Case  WHEN RejectWaterDischargeApproval='True' THEN 1 ELSE 0 END AS ApprovalForRejectWaterDischarge, 
Case  WHEN ServiceProviderApproval='True' THEN 1 ELSE 0 END AS ServiceProviders 
from tblpatPlantDetail WHERE PLANTGUID=p_PlantGuid;

END IF;


elseif (p_Tab='Tab5')

then

	select IFNULL(CASE WHEN WQuantification > 50  THEN 0

  WHEN WQuantification <= 50 AND WQuantification > 45  THEN 1

  WHEN WQuantification >= 40 AND WQuantification <= 45 THEN 2

  WHEN WQuantification >= 35 AND WQuantification <= 40 THEN 3

  WHEN WQuantification >= 30 AND WQuantification <= 35 THEN 4

  WHEN WQuantification >= 25 AND WQuantification <= 30 THEN 5

END, 0) AS RejctWaterQuantification,IFNULL(CASE WHEN WUtilisation > 50  THEN 5

  WHEN WUtilisation <= 50 AND WUtilisation > 45  THEN 4

  WHEN WUtilisation >= 40 AND WUtilisation <= 45 THEN 3

  WHEN WUtilisation >= 35 AND WUtilisation <= 40 THEN 2

  WHEN WUtilisation >= 30 AND WUtilisation <= 35 THEN 1

  WHEN WUtilisation <= 30 THEN 0

END, 0) AS RejectWaterUtilisation,
Case WHEN CheckRWDisposal='True' THEN 5 else 0 END AS RejectWaterDisposal, 
CASE WHEN CheckWharvesting='True' THEN 5 else 0 END AS GroundWaterRecharge 
from tblPATPODetail WHERE tblPATPODetail.POGUID=p_POGUID;

end if;

END