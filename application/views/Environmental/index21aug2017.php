 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>






 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Environmental Sustainability 
    </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Environmental Sustainability </li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Country</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($_POST['Country']==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($_POST['Country']==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <?php if($_POST['State'] ==''){ ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <?php } else{ ?>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <?php } ?>

        <!-- /.col -->
         <?php if($_POST['District'] !=''){ ?>
         <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
       
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <?php } else{ ?>
          <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ echo "select";?>

                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <?php } ?>
       
	   <?php
     
       if($_POST['Plant'] !=''){
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']); ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                  if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant']==$prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
        <!-- /.col -->
      </div>
       <?php }else{ 
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']);
         ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

       <?php } ?>
      <!-- /.row -->
	  </form>
            <!-- /.row -->

   <div class="row">
<?php 
$WaterRe       =  $getRegectedWater->WaterRejected;
$RejectWa      =   $getRegectedWater->RejectWaterUtilisation;
$WaterRejected = ROUND(($WaterRe)/1000);
$WaterRejectedUtili = ROUND(($RejectWa)/1000);

?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Rejectwater',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Reject Water Utillization (1,000 L)",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Total Utillization",
                "numberPrefix": "",
                "formatNumber" : "0",
                "formatNumberScale":"0",
                "paletteColors": "#0075c2",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                 //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Water Rejected",
                    "value": "<?php echo $WaterRejected;?>",
                    "Color": "#599DDC",
                    "seriesname": "Water Rejected"
                }, 
                {
                    "label": "Reject Water Utilisation",
                    "value": "<?php echo $WaterRejectedUtili;?>",
                    "Color": "#ED7320",
                    "seriesname": "Reject Water Utilisation"
                }
            ]
        }
    }).render();
});
</script>


<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title" style="margin-left:10px;">Reject Water Utillization</h3>
              </div>
           <div id="chart-container-Rejectwater"></div>
          
          </div>
	</section>
<?php  
  //$PieChartData =  $this->model->CreatePieChartRejectWater();
  //echo "<pre>";
  //print_r($getPieChartRejectWater); //die;

  $piechartRW1 =array();
  $piechartRW2 =array();
  $piechartRW3 =array();
  $piechartRW4 =array();
  $piechartRW5 =array();

  foreach($getPieChartRejectWater as $row){
         //echo $row->Utilizewater;
       //echo  ROUND($row->Utilizewater);
    if(ROUND($row->Utilizewater) == 100){
      $piechartRW1[]= ROUND($row->Utilizewater);
    }
    if((ROUND($row->Utilizewater) < 100) && (ROUND($row->Utilizewater) > 75)){
      $piechartRW2[]= ROUND($row->Utilizewater);
    }
    if((ROUND($row->Utilizewater) < 75) && ROUND(($row->Utilizewater > 50))){
      $piechartRW3[]= ROUND($row->Utilizewater);
    }
     if((ROUND($row->Utilizewater) < 50) && (ROUND($row->Utilizewater) > 0)){
      $piechartRW4[]= ROUND($row->Utilizewater);
    }
     if((ROUND($row->Utilizewater)== 0)){
      $piechartRW5[]= ROUND($row->Utilizewater);
    }
  }
 // echo "zdcz";
  $piechar1 = count($piechartRW1);
  $piechar2 = count($piechartRW2);
  $piechar3 = count($piechartRW3);
  $piechar4 = count($piechartRW4);
  $piechar5 = count($piechartRW5);
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-Utillization',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Reject Water Utillization",
                "subCaption": " ",
               // "paletteColors": "#FBC412,#4C92D0,#F07B2C,#4876C4,",
                "bgColor": "#c4d8e0",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Utillized 100%",
                    "value": "<?php echo $piechar1;?>",
                    "color": "#62A0DB"
                }, 
                 {
                    "label": "<100% and >75%",
                    "value": "<?php echo $piechar2;?>",
                     "color": "#F17221"
                }, 
                 {
                    "label": "<75% and >50%",
                    "value": "<?php echo  $piechar3;?>",
                     "color": "#AAAAAA"
                }, 
                 {
                    "label": ">50% and 0%",
                    "value": "<?php echo $piechar4;?>",
                     "color": "#FFC602"
                }, 
                 {
                    "label": "Utillized 0%",
                    "value": "<?php echo $piechar5;?>",
                     "color": "#3D6FC8"
                }
            ]
        }
    }).render();
});
</script>

       


	
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Reject Water Utillization</h3>
              </div>
           <div id="chart-container-Utillization"></div>
          
          </div>
	</section>

</div>


	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexEnvironmental').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexEnvironmental').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
           
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexEnvironmental').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   