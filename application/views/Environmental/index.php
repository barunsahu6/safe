 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>
<style>
/*.dropdownmenu ul, .dropdownmenu li {
	margin: 0;
	padding: 0;
}
.dropdownmenu ul {
	background: gray;
	list-style: none;
	width: 100%;
}
.dropdownmenu li {
	float: left;
	position: relative;
	width:auto;
}
.dropdownmenu a {
	background: #3c8dbc;
	color: #FFFFFF;
	display: block;
	font: bold 11px/20px sans-serif;
	padding: 8px 15px;
	text-align: center;
	text-decoration: none;
	-webkit-transition: all .25s ease;
	-moz-transition: all .25s ease;
	-ms-transition: all .25s ease;
	-o-transition: all .25s ease;
	transition: all .25s ease;
}
.dropdownmenu li:hover a {
	background: #000000;
}*/
#submenu {
	left: 0;
	opacity: 0;
	position: absolute;
	top: 35px;
	visibility: hidden;
	z-index: 1;
}
li:hover ul#submenu {
	opacity: 1;
	top: 40px;	/* adjust this as per top nav padding top & bottom comes */
	visibility: visible;
}
#submenu li {
	float: none;
	width: 100%;
}
#submenu a:hover {
	background: #DF4B05;
}
#submenu a {
	background-color:#000000;
}
</style>
<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class ="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <nav class="dropdownmenu">
  <ul>
    <li><a href="<?php echo site_url().'Dashboard/index';?>" >PAT Dashboard</a></li>
    <li><a href="<?php echo site_url().'Social/index';?>">Social Sustainability</a></li>
    <li><a href="<?php echo site_url().'Operational/index';?>">Operational Sustainability</a> </li>
    <li><a href="<?php echo site_url().'Financial/index';?>">Financial Sustainability</a></li>
    <li><a href="<?php echo site_url().'Institutional/index';?>">Institutional Sustainability</a></li>
    <li><a href="<?php echo site_url().'Environmental/index';?>" style="background-color:#000000;">Environmental Sustainability</a></li>
  </ul>
</nav>

</div>
</div>
     </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
          <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($this->session->userdata("Country")==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($this->session->userdata("Country")==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
          </div>
        <!-- /.col -->
      <div class="col-xs-1" style="top:7px;font-size:12px;"><b>State/Province:</b></div>
         
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($this->session->userdata("Country"));  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="0">---All ---</option>
                     <?php foreach($State as $row){
                         if($this->session->userdata("State")==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($this->session->userdata("State")==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
         

        <!-- /.col -->
        <div class="col-xs-1" style="top:7px;font-size:12px;"><b>District/Zone:</b></div>
          
         <?php $District = $this->model->getDistricts($this->session->userdata("Country"),$this->session->userdata("State")); ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="0">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($this->session->userdata("District")==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($this->session->userdata("District")==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>
           
	     <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Plant:</b></div>
          <div class="col-xs-2">
     <?php $Plant = $this->model->getPlants($this->session->userdata("Country"),$this->session->userdata("State"),$this->session->userdata("District")); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="0">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($this->session->userdata("Plant")==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($this->session->userdata("Plant") == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
    
          </div>
    </div>
 	  </form>
 <?php 
$WaterRe       =  $getRegectedWater->WaterRejected;
$RejectWa      =   $getRegectedWater->RejectWaterUtilisation;
$WaterRejected = ROUND(($WaterRe)/1000);
$WaterRejectedUtili = ROUND(($RejectWa)/1000);
?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Rejectwater',
       width: '450',
        height: '315',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Reject Water Utillization (1,000 L)",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
               // "yAxisName": "Total Utillization",
                "numberPrefix": "",
                "formatNumber" : "0",
                "formatNumberScale":"0",
                "paletteColors": "#0075c2",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                 //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBol50d": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Water Rejected",
                    "value": "<?php echo $WaterRejected;?>",
                    "Color": "#008080",
                    "seriesname": "Water Rejected"
                }, 
                {
                    "label": "Reject Water Utilisation",
                    "value": "<?php echo $WaterRejectedUtili;?>",
                    "Color": "#ED7320",
                    "seriesname": "Reject Water Utilisation"
                }
            ]
        }
    }).render();
});
</script>
<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 405px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
.rotate {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */}
</style>
<div class="scrollbar" id="style-3">
 <div class="row">
    <div class="col-lg-12"  style="padding-left: 15px;">
        <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color:#003494;" ><b>REJECT WATER UTILISATION</b></div>
    </div>
    </div>
    
  </div>
       

<div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:315px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 145px;"><b>TOTAL UTLISATION</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
               <div id="chart-container-Rejectwater"></div>
        </div>
    </div>
   </div>
   </div>
   


<?php  
  $piechartRW1 =array();
  $piechartRW2 =array();
  $piechartRW3 =array();
  $piechartRW4 =array();
  $piechartRW5 =array();
// echo "<pre>";
// print_R($getPieChartRejectWater);

  foreach($getPieChartRejectWater as $row){
         //echo $row->Utilizewater;
       //echo  ROUND($row->Utilizewater);
    if(ROUND($row->Utilizewater) == 100){
      $piechartRW1[]= ROUND($row->Utilizewater);
    }
    if((ROUND($row->Utilizewater) < 100) && (ROUND($row->Utilizewater) > 75)){
      $piechartRW2[]= ROUND($row->Utilizewater);
    }
    if((ROUND($row->Utilizewater) < 75) && ROUND(($row->Utilizewater > 50))){
      $piechartRW3[]= ROUND($row->Utilizewater);
    }
     if((ROUND($row->Utilizewater) < 50) && (ROUND($row->Utilizewater) > 0)){
      $piechartRW4[]= ROUND($row->Utilizewater);
    }
     if((ROUND($row->Utilizewater)== 0)){
      $piechartRW5[]= ROUND($row->Utilizewater);
    }
  }
 // echo "zdcz";
  $piechar1 = count($piechartRW1);
  $piechar2 = count($piechartRW2);
  $piechar3 = count($piechartRW3);
  $piechar4 = count($piechartRW4);
  $piechar5 = count($piechartRW5);
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-Utillization',
        width: '450',
        height: '315',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Reject Water Utillization",
                "subCaption": " ",
               // "paletteColors": "#FBC412,#4C92D0,#F07B2C,#4876C4,",
                "bgColor": "#FFFFFF",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Utillized 100%",
                    "value": "<?php echo $piechar1;?>",
                    "color": "#62A0DB"
                }, 
                 {
                    "label": "<100% and >75%",
                    "value": "<?php echo $piechar2;?>",
                     "color": "#F17221"
                }, 
                 {
                    "label": "<75% and >50%",
                    "value": "<?php echo  $piechar3;?>",
                     "color": "#008080"
                }, 
                 {
                    "label": ">50% and 0%",
                    "value": "<?php echo $piechar4;?>",
                     "color": "#FFC602"
                }, 
                 {
                    "label": "Utillized 0%",
                    "value": "<?php echo $piechar5;?>",
                     "color": "#3D6FC8"
                }
            ]
        }
    }).render();
});
</script>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:315px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 145px;"><b>% UTILISATION </b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
                <div id="chart-container-Utillization"></div>
        </div>
            </div>
        </div>
    </div>
   </div>
</div>     

	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexEnvironmental').submit();
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        // function getAllState(){
        //      var id = $('#State').val();
        //       var Countryid = $('#Country').val();
        //      if(id !=''){
           
        //       $.ajax({
        //             url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
        //              type: 'POST',
        //              dataType: 'text',
        //          })
        //      .done(function(data) {
        //             console.log(data);
        //             //$('#indexDashboard').submit();
        //             $("#District").html(data);
        //            // $("#Plant").html("<option value= ''>--All--</option>");
        //             $('#indexEnvironmental').submit();
        //              return false;
        //         })
        //         .fail(function() {
        //             console.log("error");
        //         })
        //         .always(function() {
        //             console.log("complete");
        //         });

        //      }
                       
        // }

         function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').su9bmit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexEnvironmental').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }



         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexEnvironmental').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            $('#indexEnvironmental').submit();                       
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   