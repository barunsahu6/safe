 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////
?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>
<?php 
  $jsonplantaged = json_encode($getPlantAge); 
 ?>
<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 450px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

.scrollbar1
{
	margin-left: 0px;
	float: left;
	height: 250px;
	width: 450px;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}
#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
#style-4::-webkit-scrollbar-thumb
{
	background-color: #F5F5F5;
}
</style>
<style>
.dropdownmenu ul, .dropdownmenu li {
	margin: 0;
	padding: 0;
}
.dropdownmenu ul {
	background: gray;
	list-style: none;
	width: 100%;
}
.dropdownmenu li {
	float: left;
	position: relative;
	width:auto;
}
.dropdownmenu a {
	background: #3c8dbc;
	color: #FFFFFF;
	display: block;
	font: bold 11px/20px sans-serif;
	padding: 8px 15px;
	text-align: center;
	text-decoration: none;
	-webkit-transition: all .25s ease;
	-moz-transition: all .25s ease;
	-ms-transition: all .25s ease;
	-o-transition: all .25s ease;
	transition: all .25s ease;
}
.dropdownmenu li:hover a {
	background: #000000;
}
#submenu {
	left: 0;
	opacity: 0;
	position: absolute;
	top: 35px;
	visibility: hidden;
	z-index: 1;
}
li:hover ul#submenu {
	opacity: 1;
	top: 40px;	/* adjust this as per top nav padding top & bottom comes */
	visibility: visible;
}
#submenu li {
	float: none;
	width: 100%;
}
#submenu a:hover {
	background: #DF4B05;
}
#submenu a {
	background-color:#000000;
}
</style>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
<div class ="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <nav class="dropdownmenu">
  <ul>
    <li><a href="<?php echo site_url().'Dashboard/index';?>" style="background-color:#000000;" >PAT Dashboard</a></li>
    <li><a href="<?php echo site_url().'Social/index';?>">Social Sustainability</a></li>
    <li><a href="<?php echo site_url().'Operational/index';?>">Operational Sustainability</a> </li>
    <li><a href="<?php echo site_url().'Financial/index';?>">Financial Sustainability</a></li>
    <li><a href="<?php echo site_url().'Institutional/index';?>">Institutional Sustainability</a></li>
    <li><a href="<?php echo site_url().'Environmental/index';?>">Environmental Sustainability</a></li>
  </ul>
</nav>

</div>
</div>


    </section>
	
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>




<div class="row">
          <div class="col-xs-1" style="top:7px; font-size:16px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="0">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($this->session->userdata("Country")==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($this->session->userdata("Country")==$row->CountryID) { echo 'selected="selected"';} ?>><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>
                    <?php } }?>
                 </select>
                </div>
          </div>
          
          <div class="col-xs-1"style="top:7px;font-size:16px;"><b>State:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($this->session->userdata("Country"));  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="0">---All ---</option>
                     <?php foreach($State as $row){
                         if($this->session->userdata("State")==$row->StateID){ ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($this->session->userdata("State")==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>

          <div class="col-xs-1" style="top:7px;font-size:16px;"><b>Dirstict:</b></div>
         
         <?php $District = $this->model->getDistricts($this->session->userdata("Country"),$this->session->userdata("State")); ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="0">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($this->session->userdata("District")==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($this->session->userdata("District")==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>         
          <div class="col-xs-1" style="top:7px;font-size:16px;"><b>Plant:</b></div>
          <div class="col-xs-2">
     <?php
        $Plant = $this->model->getPlants($this->session->userdata("Country"),$this->session->userdata("State"),$this->session->userdata("District")); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="0">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($this->session->userdata("Plant")==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($this->session->userdata("Plant") == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
          </div>
    </div>
 
 </form>
            <!-- /.row -->
<?php
//print_r($getPerformanceSofiScore);
$perSofiSocial          = $getPerformanceSofiScore->social;
$perSofiOperational     = $getPerformanceSofiScore->operational;
$perSofiFinancial       = $getPerformanceSofiScore->financial;
$perSofiInstitutional   = $getPerformanceSofiScore->Institutional;
$perSofiEnvironmental   = $getPerformanceSofiScore->Environmental;  
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-bar2dperformance',
        width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "SOFIE ",
                "subCaption": "  ",
                "yAxisName": "",
               // "xAxisName": "SOFIE SCORE ",
                //"numberPrefix": "Social ",
                "paletteColors": "#ff9800",
                "bgColor": "#FFFFFF",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#FFFFFF",
               // "valueFontColor": "#cddc39",19
                "showAxisLines": "1",
               // "axisLineAlpha": "15",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
               // "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
               // "toolTipColor": "#cddc39",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#FFFF01,#00AF50,#FFC000,#F8CBAC,#90D151",
                //"toolTipBgAIn Percentagelpha": "80",
                //"toolTipBorderRadius": "2",
                //"toolTipPadding": "5",
                 "theme": "ocean"
            },
            
            "data": [
                {
                    "label": "Social",
                    "value": "<?php echo $perSofiSocial;?>",
                    "color": "#FFFF01",
                    "link": "<?php echo site_url().'Social/index';?>"
                }, 
                {
                    "label": " Operational ",
                    "value": "<?php echo $perSofiOperational;?>",
                    "color" : "#00AF50",
                    "link": "<?php echo site_url().'Operational/index';?>"
                 }, 
                {
                    "label": " Financial ",
                    "value": "<?php echo $perSofiFinancial;?>",
                    "color" : "#FFC000",
                    "link": "<?php echo site_url().'Financial/index';?>"
                    
                }, 
                {
                    "label": " Institutional ",
                    "value": "<?php echo $perSofiInstitutional;?>",
                    "color" : "#F8CBAC",
                    "link": "<?php echo site_url().'Institutional/index';?>"
                }, 
                {
                    "label": " Environmental ",
                    "value": "<?php echo $perSofiEnvironmental;?>",
                    "color" : "#90D151",
                    "link": "<?php echo site_url().'Environmental/index';?>"
                }
            ]
        }
    })
    .render();
});
</script>

<style>

.rotate {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */}
</style>

<div class="scrollbar" id="style-3">
   <div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate" style="width: 300px;height: 40px;margin-left: -119px; margin-top:calc(2% - -100%);"><b>SOFIE SCORE</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
            <div id="chart-container-bar2dperformance"></div>
        </div>
    </div>
   </div>
   </div>
        <!-- Left col -->
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:540px;height:auto;margin-left:0px; background-color: #FFFFFF;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate" style="width: 300px;height: 40px;margin-left: -119px; margin-top:calc(50% - -100%);"><b>SCORE DISTRIBUTION</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%;">
        <div class="scrollbar1" id="style-4">
      <div class="box box-info">
        <table  class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>District</th>
            <th># Station</th>
            <th>S</th>
            <th>O</th>
            <th>F</th>
            <th>I</th>
            <th>E</th>
            <th>Avg Score</th>
        </tr>
        </thead>
        <tbody>
        <?php 
      // echo "<pre>";
      // print_r($Getstatesofie);
        foreach ($Getstatesofie as $row) {
             $districtwhr =  $this->session->userdata["District"];
              $plantwhr = $this->session->userdata["Plant"];
               $strstatewhr = $this->session->userdata["State"];
               $strwhr = $this->session->userdata["Country"];

            $totalsofieavg = ROUND(($row->social+$row->operational+$row->financial+$row->Institutional+$row->Environmental)/5);
            $sql= "SELECT Count(`PlantGUID`) AS Plant FROM `vw_plant_sofiescore` WHERE `district`='".$row->DistrictID."'";
           	if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$row->PlantGUID."'";
			}
			$sql .= " ";
          // echo  $sql;
            $res = $this->Common_model->query_data($sql);

           // print_r($res);
          if($row->DistrictName !=''){
         ?>
        <tr>
            <td><?php echo $row->DistrictName;?></td>
            <td align="center"><?php echo $res[0]->Plant;?></td>
            <td align="center"><?php echo $row->social; ?></td>
            <td align="center"> <?php echo $row->operational; ?></td>
            <td align="center"><?php echo $row->financial; ?></td>
            <td align="center"><?php echo $row->Institutional; ?></td>
            <td align="center"><?php echo $row->Environmental; ?></td>
            <td align="center"><?php echo $totalsofieavg;?></td>
        </tr>
        <?php }} ?>
        </tbody>
      </table>
   </div>
   </div>
   </div>
   </div>
  </div>
   <div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12"  style="padding-left: 28px;">
        <div class="panel panel-primary">
        <div class="panel-heading text-center"><b>COUNTRY SNAPSHORT</b></div>
    </div>
    </div>
  </div>
<div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 28px;">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:310px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate" style="width: 300px;height: 40px;margin-left: -119px; margin-top:calc(35% - -100%);"><b>GEO - TAGS</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
           <div id="map" style="width: 450px; height: 305px; border: 1px #a3c86d; border-style: none solid solid solid;"></div>
        </div>
    </div>
   </div>
   </div>
        <!-- Left col -->
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:550px;height:auto;">
   <div style="float:left;width:40px; height:310px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate" style="width: 300px;height: 40px;margin-left: -119px; margin-top:calc(200% - -100%);"><b>ACCESS & QUALITY AFFECTED HABITATIONS</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
       <div class="box box-info">
       <img src="<?php print FIMAGES;?>contmap.png" width="450px" height="305px" alt="map">
   </div>
   </div>

   </div>
  </div>
   </div>
   </div>
  </div> 
 <script type="text/javascript">
          $(document).ready(function(){
            $("#Country").change(function(){
              
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getStates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= '0'>--All--</option>");
                    $("#Plant").html("<option value= '0'>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     

        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').su9bmit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexDashboard').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexDashboard').submit();
                     return73% false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                      
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
            
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                // alert(data);
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexDashboard').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        }

    </script>
<?php   
        $strwhr         = $this->session->userdata("Country"); 
        $strstatewhr    = $this->session->userdata("State");
        $districtwhr    =  $this->session->userdata("District");
        $plantwhr       = $this->session->userdata("Plant"); 

    $getLocation = $this->model->DatafromOnWebService_Map($strwhr,$strstatewhr,$districtwhr,$plantwhr);
    foreach($getLocation as $row){
                $nama_kabkot  = $row->description;
                $longitude    = $row->Longitude;                              
                $latitude     = $row->Latitude;
                $Markercolor  = $row->Markercolor;
                /* Each row is added as a new array */
                $locations[]=array($nama_kabkot, $latitude, $longitude);
    }
    /* Convert data to json */
            $markers = json_encode($locations);
        // print_r($markers);
?>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   