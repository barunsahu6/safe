 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>


<?php  
//$Getgaugedata = $this->model->getguage();
$gaugevalue = $Getgaugedata->adpotion;
 ?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container-gage",
        "width": "210",
        "height": "250",
        "dataFormat": "json",
            "dataSource": {
                   "chart": {
                      "caption": "Adoption in (%)",
                     // "subcaption": "Last week",
                      "lowerLimit": "0",
                      "upperLimit": "100",
					   "bgColor": "#ffffff",
                      "theme": "fint"
                   },
                   "colorRange": {
                      "color": [
                         {
                            "minValue": "0",
                            "maxValue": "<?php echo $gaugevalue;?>",
                            "code": "#efbf60"
                         },
                         {
                            "minValue": "<?php echo $gaugevalue;?>",
                            "maxValue": "100",
                            "code": "#efbf60"
                         }
                      ]
                   },
                   "dials": {
                      "dial": [
                         {
                            "value": "<?php echo $gaugevalue;?>"
                         }
                      ]
                   }
            }
      });

    csatGauge.render();
});
</script>
<?php  
  $PieChartData =  $this->model->CreatePieChart();
 // print_r($PieChartData); //die;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-pie2d',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "TotalHouseholds",
                "subCaption": "<?php echo $PieChartData->TotalHouseholds ?>",
                "paletteColors": "#328dca,#2caf67",
                "bgColor": "#c4d8e0",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Registered",
                    "value": "<?php echo $PieChartData->perc;?>"
                }, 
                 {
                    "label": "Uncovered",
                    "value": "<?php echo $PieChartData->perc2;?>"
                }
            ]
        }
    }).render();
});
</script>


<?php  
//print_r($Getfinandata); die;
$getCapitalSource = $Getfinandata->CapitalSource;
$getRevenue = $Getfinandata->OpExandRevenue;
?>



<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'msbar2d',
        renderAt: 'chart-container-Report',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                //"yAxisname": "Sales (In USD)",
                //"numberPrefix": "$",
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showHoverEffect":"1",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "1",
                "valueFontColor": "#ffffff",
                "showXAxisLine": "1",
                              
                "divLineIsDashed": "1",
                "showAlternateVGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            },            
            "categories": [
                {
                    "category": [
                        {
                            "label": "Total"
                        }, 
                        {
                            "label": "Active"
                        }, 
                        {
                            "label": "Partially Active"
                        }, 
                        {
                            "label": "InActive "
                        }
                    ]
                }
            ],            
            "dataset": [
                {
                    "seriesname": "User Report",
                    "data": [
                        {
                            "value": "<?php echo $getuser->Total; ?>"
                        }, 
                        {
                            "value": "<?php echo $getuser->Active; ?>"
                        }, 
                        {
                            "value": "<?php echo $getuser->Partially; ?>"
                        }, 
                        {
                            "value": "<?php echo $getuser->InActive; ?>"
                        }
                    ]
                }, 
               
            ],
           
        }
    }).render();    
});
</script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Country</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($_POST['Country']==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($_POST['Country']==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <?php if($_POST['State'] ==''){ ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <?php } else{ ?>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <?php } ?>

        <!-- /.col -->
         <?php if($_POST['District'] !=''){ ?>
         <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
       
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <?php } else{ ?>
          <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ echo "select";?>

                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <?php } ?>
       
	   <?php
     
       if($_POST['Plant'] !=''){
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']); ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                  if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant']==$prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
        <!-- /.col -->
      </div>
       <?php }else{ 
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']);
         ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

       <?php } ?>
      <!-- /.row -->
	  </form>
            <!-- /.row -->
<?php
//print_r($getPerformanceSofiScore);
$perSofiSocial          = $getPerformanceSofiScore->social;
$perSofiOperational     = $getPerformanceSofiScore->operational;
$perSofiFinancial       = $getPerformanceSofiScore->financial;
$perSofiInstitutional   = $getPerformanceSofiScore->Institutional;
$perSofiEnvironmental   = $getPerformanceSofiScore->Environmental;  
?>


<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-bar2dperformance',
        width: '510',
        height: '580',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "SOFIE ",
                "subCaption": "  ",
                "yAxisName": "",
                "xAxisName": "SOFIE SCORE ",
                //"numberPrefix": "Social ",
                "paletteColors": "#ff9800",
                "bgColor": "#cddc39",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
               // "valueFontColor": "#cddc39",
                "showAxisLines": "1",
               // "axisLineAlpha": "15",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
               // "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
               // "toolTipColor": "#cddc39",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                //"toolTipBgAIn Percentagelpha": "80",
                //"toolTipBorderRadius": "2",
                //"toolTipPadding": "5",
                 "theme": "ocean"
            },
            
            "data": [
                {
                    "label": "Social",
                    "value": "<?php echo $perSofiSocial;?>",
                    "color": "#FFFF01",
                    "link": "<?php echo site_url().'Social/index';?>"
                }, 
                {
                    "label": " Operational ",
                    "value": "<?php echo $perSofiOperational;?>",
                    "color" : "#00AF50"
                    

                }, 
                {
                    "label": " Financial ",
                    "value": "<?php echo $perSofiFinancial;?>",
                    "color" : "#FFC000"
                    
                }, 
                {
                    "label": " Institutional ",
                    "value": "<?php echo $perSofiInstitutional;?>",
                    "color" : "#F8CBAC",
                    "link": "<?php echo site_url().'Institutional/index';?>"
                }, 
                {
                    "label": " Environmental ",
                    "value": "<?php echo $perSofiEnvironmental;?>",
                    "color" : "#90D151",
                    "link": "<?php echo site_url().'Environmental/index';?>"
                }
            ]
        }
    })
    .render();
});
</script>
   <div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div id="chart-container-bar2dperformance"></div>
        </div>
       </section>
       	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
        <table id="example1" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>District</th>
            <th>No. of Station</th>
            <th>S</th>
            <th>O</th>
            <th>F</th>
            <th>I</th>
            <th>E</th>
            <th>Avg Score</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        // echo "<pre>";
        // print_r($Getstatesofie);
        
        foreach ($Getstatesofie as $row) {
            $totalsofieavg = ROUND(($row->social+$row->operational+$row->financial+$row->Institutional+$row->Environmental)/5);
           $sql= "SELECT Count(`PlantGUID`) AS Plant FROM `tblpatplantdetail` WHERE District='".$row->DistrictID."'";
            $res = $this->Common_model->query_data($sql);
            //echo $res[0]->Plant;
                        //print_r($res);

         if($row->DistrictName !=''){
         ?>
        <tr>
            <td><?php echo $row->DistrictName;?></td>
            <td><?php echo $res[0]->Plant;?></td>
            <td><?php echo $row->social; ?></td>
            <td> <?php echo $row->operational; ?></td>
            <td><?php echo $row->financial; ?></td>
            <td><?php echo $row->Institutional; ?></td>
            <td><?php echo $row->Environmental; ?></td>
            <td><?php echo $totalsofieavg;?></td>
        </tr>
        <?php }} ?>
        </tbody>
      </table>
    </section>
    </div>

<div class="row">
        <!-- Left col -->
    <section class="col-lg-6 connectedSortable">
        <div class="box box-info">
            <div class="box-header">
            <h3 class="box-title">Map</h3>
                <div id="map" style="width: 100%; height: 450px; border: 1px #a3c86d; border-style: none solid solid solid;">
                </div>
            </div>
        </div>
    </section>


    <div class="row">
        <!-- Left col -->
    <section class="col-lg-6 connectedSortable">
        <div class="box box-info">
            <div class="box-header">
            <h3 class="box-title"></h3>
              
               	<img src="<?php print FIMAGES;?>contmap.jpg" width="450px" height="450px" alt="map">
            </div>
        </div>
    </section>

		  <!-- /.nav-tabs-custom -->
	</div>
	

	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });
      
        });     

        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexDashboard').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexDashboard').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
           
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexDashboard').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }

    </script>
<?php   
        if(!empty($this->input->post())){
		 	 $countryid 	= $this->input->post('Country');
		 	 $stateid 	    = $this->input->post('State');
		 	 $districtid    = $this->input->post('District');
		 	 $plantid 	    = $this->input->post('Plant');

		 if(!empty($countryid)){
			 $strwhr = $countryid;
		 }
		  if(!empty($stateid)){
			 $strstatewhr = $stateid;
		 }
		 if(!empty($districtid)){
			 $districtwhr = $districtid;
		 }
		 if(!empty($plantid)){
			 $plantwhr = $plantid;
		 }
	}

    $getLocation = $this->model->DatafromOnWebService_Map($strwhr,$strstatewhr,$districtid,$plantwhr);
    foreach($getLocation as $row){
                $nama_kabkot  = $row->description;
                $longitude    = $row->Longitude;                              
                $latitude     = $row->Latitude;
                $Markercolor  = $row->Markercolor;
                /* Each row is added as a new array */
                $locations[]=array($nama_kabkot, $latitude, $longitude);
    }
    /* Convert data to json */
            $markers = json_encode($locations);
        // print_r($markers);
?>


   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   