 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>

<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-bar2d',
        width: '200',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Plant and age wise dist...",
                "subCaption": "",
                "yAxisName": " ",
				"xAxisName": "(Year)",
                //"numberPrefix": "$",
                "paletteColors": "#7080a0",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "15",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            
            "data": <?php echo $jsonplantaged; ?>
        }
    })
    .render();
});


</script>
<?php  
$Getgaugedata = $this->model->getguage();
$gaugevalue = $Getgaugedata->adpotion;
 ?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container-gage",
        "width": "210",
        "height": "250",
        "dataFormat": "json",
            "dataSource": {
                   "chart": {
                      "caption": "Adoption in (%)",
                     // "subcaption": "Last week",
                      "lowerLimit": "0",
                      "upperLimit": "100",
					   "bgColor": "#ffffff",
                      "theme": "fint"
                   },
                   "colorRange": {
                      "color": [
                         {
                            "minValue": "0",
                            "maxValue": "<?php echo $gaugevalue;?>",
                            "code": "#efbf60"
                         },
                         {
                            "minValue": "<?php echo $gaugevalue;?>",
                            "maxValue": "100",
                            "code": "#efbf60"
                         }
                      ]
                   },
                   "dials": {
                      "dial": [
                         {
                            "value": "<?php echo $gaugevalue;?>"
                         }
                      ]
                   }
            }
      });

    csatGauge.render();
});
</script>
<?php  
  $PieChartData =  $this->model->CreatePieChart();
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-pie2d',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Split of Visitors by Age Group",
                //"subCaption": "Last year",
                "paletteColors": "#328dca,#2caf67",
                "bgColor": "#c4d8e0",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Walkin",
                    "value": "<?php echo $PieChartData->Walkin;?>"
                }, 
                 {
                    "label": "Distribution",
                    "value": "<?php echo $PieChartData->Distribution;?>"
                }
            ]
        }
    }).render();
});
</script>

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-2d',
        width: '254',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Performance",
                "paletteColors": "#8e0000,#f45b00,#8bc34a",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "valueFontColor": "#ffffff",                
               
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Range"
                        }
                      ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "< 60",
                    "data": [
                        {
                            "value": "<?php echo $getPerformanceWithLimit->lessthan60; ?>"
                        }
                    ]
                },
                {
                    "seriesname": "> 60",
                    "data": [
                        {
                            "value": "<?php echo $getPerformanceWithLimit->morethan60; ?>"
                        }
                      ]
                },  {
                    "seriesname": "60",
                    "data": [
                        {
                            "value": "<?php echo $getPerformanceWithLimit->eql60; ?>"
                        }
                      ]
                }
            ]
        }
    }).render();    
});
</script>
<?php  
//print_r($Getfinandata); die;
$getCapitalSource = $Getfinandata->CapitalSource;
$getRevenue = $Getfinandata->OpExandRevenue;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-financial',
        width: '254',
        height: '400',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Financial",
               // "subCaption": "Harry's SuperMart",
                "xAxisname": "Range",
                //"yAxisName": "Revenue (In USD)",
                //"numberPrefix": "$",
                "paletteColors": "#8e0000,#f45b00,#8bc34a",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Range"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "OPEx",
                    "data": [
                        {
                            "value": "<?php echo $getCapitalSource;?>"
                        }
                    ]
                },
                {
              "seriesname": "OpEx + Servicecharge",
                    "data": [
                        {
                            "value": "<?php echo $getRevenue;?>"
                        }
                    ]
                },
                {
              "seriesname": "OpEx + Service Charge + Asset Renewal Fund",
                    "data": [
                        {
                            "value": "<?php echo $getRevenue;?>"
                        }
                    ]
                }
            ]
        }
    }).render();    
});
</script>

<?php 
$getuser = $this->model->getDashboardUserActiveAndInactive();
?>

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'msbar2d',
        renderAt: 'chart-container-Report',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                //"yAxisname": "Sales (In USD)",
                //"numberPrefix": "$",
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showHoverEffect":"1",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "1",
                "valueFontColor": "#ffffff",
                "showXAxisLine": "1",
                              
                "divLineIsDashed": "1",
                "showAlternateVGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            },            
            "categories": [
                {
                    "category": [
                        {
                            "label": "Total"
                        }, 
                        {
                            "label": "Active"
                        }, 
                        {
                            "label": "Partially Active"
                        }, 
                        {
                            "label": "InActive "
                        }
                    ]
                }
            ],            
            "dataset": [
                {
                    "seriesname": "User Report",
                    "data": [
                        {
                            "value": "<?php echo $getuser->Total; ?>"
                        }, 
                        {
                            "value": "<?php echo $getuser->Active; ?>"
                        }, 
                        {
                            "value": "<?php echo $getuser->Partially; ?>"
                        }, 
                        {
                            "value": "<?php echo $getuser->InActive; ?>"
                        }
                    ]
                }, 
               
            ],
           
        }
    }).render();    
});
</script>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Country</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ ?>
                  <option value="<?php echo $row->CountryID; ?>" ><?php echo $row->CountryName;?></option>
                    <?php } ?>
                 </select>
                </div>
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <?php //echo "<pre>";print_r($State); ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
                  <select  class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){ ?>
                    <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" name="District" id="District">
                            <option value="">---All ---</option>
                   <?php foreach($District as $row){ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
		 <div class="clearfix visible-sm-block"></div>
        
		 <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            

            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $row){?>
                    <option value="<?php echo $row->PlantGUID;?>" ><?php echo $row->SWNID;?></option>
                   <?php } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		</form>
        <!-- /.col -->
      </div>
      <!-- /.row -->
	  
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SWE's</span>
              <span class="info-box-number"><?php echo $CountPlants->count;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Performance</span>
              <span class="info-box-number"><?php echo $CountPerformance->PERFORMANCE; ?><small>%</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Financial</span>
              <span class="info-box-number">85</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Adoption</span>
              <span class="info-box-number"><?php echo round($gaugevalue); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
   <div class="row">
        <!-- Left col -->
	<section class="col-lg-3 connectedSortable">
       <div class="box box-info">
              <div id="chart-container-bar2d"></div>
    <div class="row" style="background: #545454; padding: 8px; color: #ffffff;">
                <div class="col-md-7" >Indicators</div>
                <div class="col-md-5"  style="text-align:right;">Achievements</div>
    </div>
    
    <div class="row" style="background: #4463a1;padding: 8px; color: #ffffff;">
                <div class="col-md-7" >Operational</div>
                <div class="col-md-5" style="text-align:right;"><?php echo $getPlantEvl[0]->Plant;?></div>
    </div>
  
    <div class="row" style="background: #7080a0;padding: 8px; color: #ffffff;">
                <div class="col-md-7">Population Served</div>
                <div class="col-md-5" style="text-align:right;"><?php echo $getPopulation[0]->Population; ?></div>
    </div>
    
    <div class="row" style="background: #4463a1;padding: 8px; color: #ffffff;">
        <div class="col-md-7">Distribution Point</div>
        <div class="col-md-5" style="text-align:right;"><?php echo $getDistributionPoint[0]->DistributionPoint; ?></div>
    </div>
           </div>
       </section>

<?php
//print_r($getPerformanceSofiScore);
$perSofiSocial          = $getPerformanceSofiScore->social;
$perSofiOperational     = $getPerformanceSofiScore->operational;
$perSofiFinancial       = $getPerformanceSofiScore->financial;
$perSofiInstitutional   = $getPerformanceSofiScore->Institutional;
$perSofiEnvironmental   = $getPerformanceSofiScore->Environmental;  
?>


<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-bar2dperformance',
        width: '255',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": " Sofia Score ",
                "yAxisName": " In Percentage ",
                "xAxisName": " ",
                //"numberPrefix": "$",
                "paletteColors": "#ff9800",
                "bgColor": "#cddc39",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
               // "plotBorderAlpha": "10",
              //  "placeValuesInside": "1",
               // "valueFontColor": "#cddc39",
                "showAxisLines": "1",
               // "axisLineAlpha": "15",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#cddc39",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            
            "data": [
                {
                    "label": "S",
                    "value": "<?php echo $perSofiSocial;?>"
                }, 
                {
                    "label": "O ",
                    "value": "<?php echo $perSofiOperational;?>"
                }, 
                {
                    "label": "F",
                    "value": "<?php echo $perSofiFinancial;?>"
                }, 
                {
                    "label": "I",
                    "value": "<?php echo $perSofiInstitutional;?>"
                }, 
                {
                    "label": "E",
                    "value": "<?php echo $perSofiEnvironmental;?>"
                }
            ]
        }
    })
    .render();
});
</script>



 
<section class="col-lg-3 connectedSortable" style="background-color: #cddc39; color: #fff; padding: 0px; 
border: 2px solid #cddc39;">
       <div id="chart-container-2d"></div>
       <div id="chart-container-bar2dperformance"></div>
</section>
		  
 <div class="clearfix visible-sm-block" ></div>
	<section class="col-lg-3 connectedSortable" style="padding-left: 5px; background-color: #e91e63; color: #fff; padding: 0px; border: 2px solid #e91e63;" >
	<div id="chart-container-financial"></div>
</section>
		 
	 
		<section class="col-lg-3 connectedSortable"  >
        <div class="box box-info">
           <div id="chart-container-gage">FusionCharts will render here</div>
           </div>
		  </section>

	</div>
	        <!-- Middle Chart -->
	
<?php 
 //print_r($Tab_TotalScore); 
 $totalval              = 20;
 $social                = $Tab_TotalScore->Social;
 $oprational            = $Tab_TotalScore->operational;
 $financial             = $Tab_TotalScore->financial;
 $institutional         = $Tab_TotalScore->Institutional;
 $environmental         = $Tab_TotalScore->Environmental;
 $sumtotal              = $social+$oprational+$financial+$institutional+$environmental;


 $SocialRemaining           = $totalval - $Tab_TotalScore->Social;
 $OprationalRemaining       = $totalval - $oprational;
 $FinancialRemaining        = $totalval - $financial;
 $InstitutionalRemaining    = $totalval - $institutional;
 $EnvironmentalRemaining    = $totalval - $environmental;

?>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'multilevelpie',
    renderAt: 'chart-container-piemultilevel',
    id: "myChart",
    width: '450',
    height: '450',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "",
            "subcaption": "",
            "showPlotBorder": "1",
            "piefillalpha": "60",
            "pieborderthickness": "2",
            "hoverfillcolor": "#CCCCCC",
            "piebordercolor": "#000000",
            "hoverfillcolor": "#CCCCCC",
            //"numberprefix": "$",
            "plottooltext": "$label, $$valueK, $percentValue",
            //Theme
            "theme": "fint"
        },
        "category": [{
            "label": "<?php echo $sumtotal;?>",
            "color": "#fdfa19",
            "value": "<?php echo $sumtotal;?>",
            "category": [{
                "label": "Financial",
                "color": "#cccccc",
                "value": "<?php echo $totalval;?>",
                "tooltext": "Financial",
                "category": [{
                    "label": "<?php echo $financial;?>",
                    "color": "#cccccc",
                    "value": "<?php echo $financial;?>"
                }, {
                    "label": "<?php echo $FinancialRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $FinancialRemaining;?>"
                }]
            }, {
                "label": "institutional",
                "color": "#17a086",
                "value": "<?php echo $totalval; ?>",
                "tooltext": "institutional",
                "category": [{
                    "label": "<?php echo $institutional;?>",
                    "color": "#17a086",
                    "value": "<?php echo $institutional;?>"
                }, {
                    "label": "<?php echo $InstitutionalRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $InstitutionalRemaining;?>"
                }]
            },{
                "label": "Environmental",
                "color": "#efa232",
                "value": "<?php echo $totalval; ?>",
                "tooltext": "Environmental",
                "category": [{
                    "label": "<?php echo $environmental;?>",
                    "color": "#efa232",
                    "value": "<?php echo $environmental;?>"
                }, {
                    "label": "<?php echo $EnvironmentalRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $EnvironmentalRemaining;?>"
                }]
            },
             {
                "label": "Social",
                "color": "#ff7857",
                "value": "20",
                "tooltext": "Social",
                "category": [{
                    "label": "<?php echo $social;?>",
                    "color": "#ff7857",
                    "value": "<?php echo $social;?>",
                    "tooltext": "<?php echo $social;?>",

                }, {
                    "label": "<?php echo $SocialRemaining; ?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $SocialRemaining; ?>"
                }]
            }, {
                "label": "Oprational",
                "color": "#3669b0",
                "value": "<?php echo $totalval;?>",
                "category": [{
                    "label": "<?php echo $oprational;?>",
                    "color": "#3669b0",
                    "value": "<?php echo $oprational;?>"
                }, {
                    "label": "<?php echo $OprationalRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $OprationalRemaining;?>"
                }]
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>


   <div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
               <h3 class="box-title">Sofie</h3>
             </div>
			 <div id="chart-container-piemultilevel"></div>
             
		</div>
	</section>
		  
		  
		  <section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Map</h3>
              <div id="map" style="width: 100%; height: 450px; border: 1px #a3c86d; border-style: none solid solid solid;">
            </div>
              </div>
            
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>
	<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Walkin vs distribution</h3>
              </div>
           <div id="chart-container-pie2d"></div>
          
          </div>
       
	</section>
		  
	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">User Report</h3>
              </div>
            <div id="chart-container-Report"></div>
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){

                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

           $("#State").change(function(){
                var id = $('#State').val();
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+$("#Country").val()+'/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                    $("#Plant").html("<option value= ''>--All--</option>");

                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });

            $("#District").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getplants/'+$("#Country").val()+'/'+$("#State").val()+'/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#Plant").html(data);
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });
        });     


        // function populate_submenu(id) {
        //   $.ajax({
        //     type: "POST",
        //     url: '<?php //echo site_url(); ?>Dashboard/getAllDistrict/' + id,
        //     contentType: "application/json;charset=utf-8",
        //     dataType: 'json',
        //     data : {"id" : id},
        //     success: function(data) {
        //         console.log(data);
               
        //    }
        // });
        // }
    </script>
<?php   
    $getLocation = $this->model->DatafromOnWebService_Map($strwhr);
    foreach($getLocation as $row){
                $nama_kabkot  = $row->description;
                $longitude    = $row->Longitude;                              
                $latitude     = $row->Latitude;
                $Markercolor  = $row->Markercolor;
                /* Each row is added as a new array */
                $locations[]=array($nama_kabkot, $latitude, $longitude);
    }
    /* Convert data to json */
            $markers = json_encode($locations);
        // print_r($markers);
?>


   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   