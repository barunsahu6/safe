
<script src="<?php print FJS; ?>fusioncharts.js"></script>
<script src="<?php print FJS; ?>fusioncharts.theme.fint.js?cacheBust=56"></script>
 

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Scoring
        <small>preview of Scoring</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"> Scoring </li>
      </ol>
    </section>
	    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Total Score</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
			  <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
      		  <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			  	<div id="chart-container"></div>
			</div>
             
<!-- Tab of social Sustainability-->
<?php 
    $jsonSoc = array(); 
	     foreach ($Tab_Social_Sustainability as $key => $value) {
             $jsonSoc[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonSocial = json_encode($jsonSoc);

        $Coverage = $Tab_Social_Sustainability->Coverage;
		$Affordability = $Tab_Social_Sustainability->Affordability;
		/////////////////////////////////////////////////
		$totaltab1=$Coverage+$Affordability;
        $remaining1 = 20-$totaltab1; 

?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_2',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Social",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonSocial;  ?>
        }
    });

    revenueChart.render();
});
</script>

<!-- Social Guage -->

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'angulargauge',
    renderAt: 'chart-container-guage-tab_2',
    width: '400',
    height: '250',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Social",
            "subcaption": "",
            "lowerLimit": "0",
            "upperLimit": "20",
            "lowerLimitDisplay": "Bad",
            "upperLimitDisplay": "Good",
            "showValue": "1",
            "valueBelowPivot": "1",
            //"theme": "fint"
        },
        "colorRange": {
            "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab1;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab1;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }]
        },
        "dials": {
            "dial": [{
                "value": "<?php echo $totaltab1; ?>"
            }]
        }
    }
}
);
    fusioncharts.render();
});
</script>

              <div class="tab-pane" id="tab_2">
                 
				<div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_2"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_2"></div>
					</div>
				</div>
		 </div>


<?php   

 foreach ($Tab_Operational as $key => $value) {
             $jsonOperat[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonOperational = json_encode($jsonOperat);
       
        $Quality =$Tab_Operational->Quality;
		$Reliability =$Tab_Operational->Reliability;
		//////////////////////////////////////////////////
	    $totaltab2 = $Quality+$Reliability;
        $remaining2 = 20-$totaltab2; 

?>

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_3',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Operational",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonOperational;  ?>
        }
    });

    revenueChart.render();
});
</script>
<!-- Operational Guage -->

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'angulargauge',
    renderAt: 'chart-container-guage-tab_3',
    width: '400',
    height: '250',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Operational",
            "subcaption": "",
            "lowerLimit": "0",
            "upperLimit": "20",
            "lowerLimitDisplay": "Bad",
            "upperLimitDisplay": "Good",
            "showValue": "1",
            "valueBelowPivot": "1",
            //"theme": "fint"
        },
        "colorRange": {
            "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab2;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab2;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }]
        },
        "dials": {
            "dial": [{
                "value": "<?php echo $totaltab2; ?>"
            }]
        }
    }
}
);
    fusioncharts.render();
});
</script>
         <div class="tab-pane" id="tab_3">
			 <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_3"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_3"></div>
					</div>
				</div>
		</div>
<?php  
    $jsonfinancial = array(); 
	     foreach ($Tab_Financial as $key => $value) {
             $jsonfinancial[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonfinancial = json_encode($jsonfinancial);
         $CapitalSource  = $Tab_Financial->CapitalSource;
         $OpExandRevenue = $Tab_Financial->OpExandRevenue;

         $totalfinance = $CapitalSource + $OpExandRevenue;
         $remaining3 = 20-$totalfinance;
?>			  

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_4',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Financial",
                //"subCaption": "Last year",
                //"xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonfinancial;  ?>
        }
    });

    revenueChart.render();
});
</script>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'angulargauge',
    renderAt: 'chart-container-guage-tab_4',
    width: '400',
    height: '250',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Financial",
            "subcaption": "",
            "lowerLimit": "0",
            "upperLimit": "20",
            "lowerLimitDisplay": "Bad",
            "upperLimitDisplay": "Good",
            "showValue": "1",
            "valueBelowPivot": "1",
            //"theme": "fint"
        },
        "colorRange": {
            "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totalfinance;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totalfinance;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }]
        },
        "dials": {
            "dial": [{
                "value": "<?php echo $totalfinance; ?>"
            }]
        }
    }
}
);
    fusioncharts.render();
});
</script>
		<div class="tab-pane" id="tab_4">
			 <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_4"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_4"></div>
					</div>
				</div>
	</div>
<?php 
$jsoninstitutional = array(); 
	     foreach ($Tab_Institutional as $key => $value) {
             $jsoninstitutional[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
$jsoninstitutional = json_encode($jsoninstitutional);


$Regulatory = $Tab_Institutional->Regulatory;
$Training = $Tab_Institutional->Training;
//////////////////////////////////////
$totaltab4 = $Regulatory+$Training;
$remaining4 = 20-$totaltab4;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_5',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Institutional",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsoninstitutional;  ?>
        }
    });

    revenueChart.render();
});
</script>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'angulargauge',
    renderAt: 'chart-container-guage-tab_5',
    width: '400',
    height: '250',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Institutional",
            "subcaption": "",
            "lowerLimit": "0",
            "upperLimit": "20",
            "lowerLimitDisplay": "Bad",
            "upperLimitDisplay": "Good",
            "showValue": "1",
            "valueBelowPivot": "1",
            //"theme": "fint"
        },
        "colorRange": {
            "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab4;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab4;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }]
        },
        "dials": {
            "dial": [{
                "value": "<?php echo $totaltab4; ?>"
            }]
        }
    }
}
);
    fusioncharts.render();
});
</script>
<div class="tab-pane" id="tab_5">
              
			 <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_5"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_5"></div>
					</div>
				</div>
		</div>
<?php 
      // print_r($Tab_Environmental);
        $jsonenvironmental = array(); 
            foreach ($Tab_Environmental as $key => $value) {
                $jsonenvironmental[] =array(
                        'value'=> $value,
                        'label'=>$key);
            }
        $jsonEnvironmental = json_encode($jsonenvironmental);

	    $REJECTWATERUTILIZATION = $Tab_Environmental->REJECTWATERUTILIZATION;
		
		///////////////////////////////////////////////////////////////////
		$totaltab5 = $REJECTWATERUTILIZATION;
        $remaining5 = 20-$totaltab5;
        $grandTotal = $totaltab1+$totaltab2+$totalfinance+$totaltab4+$totaltab5; //die;
?>			
<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_6',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Environmental",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonEnvironmental;  ?>
        }
    });

    revenueChart.render();
});
</script>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'angulargauge',
    renderAt: 'chart-container-guage-tab_6',
    width: '400',
    height: '250',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Environmental",
            "subcaption": "",
            "lowerLimit": "0",
            "upperLimit": "20",
            "lowerLimitDisplay": "Bad",
            "upperLimitDisplay": "Good",
            "showValue": "1",
            "valueBelowPivot": "1",
            //"theme": "fint"
        },
        "colorRange": {
            "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab5;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab5;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }]
        },
        "dials": {
            "dial": [{
                "value": "<?php echo $totaltab5; ?>"
            }]
        }
    }
}
);
    fusioncharts.render();
});
</script>
		   <div class="tab-pane" id="tab_6">
			   <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_6"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_6"></div>
					</div>
				</div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->

<?php 
 $totalval              = 20;
 $social                = $Tab_TotalScore->SS;
 $oprational            = $Tab_TotalScore->OS;
 $financial             = $Tab_TotalScore->FS;
 $institutional         = $Tab_TotalScore->IS_t;
 $environmental         = $Tab_TotalScore->ES;
 $sumtotal              = $Tab_TotalScore->total;


 $SocialRemaining           = $totalval - $social;
 $OprationalRemaining       = $totalval - $oprational;
 $FinancialRemaining        = $totalval - $financial;
 $InstitutionalRemaining    = $totalval - $institutional;
 $EnvironmentalRemaining    = $totalval - $environmental;

?>
<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'multilevelpie',
    renderAt: 'chart-container',
    id: "myChart",
    width: '450',
    height: '450',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "SOFIE",
            "subcaption": "",
            "showPlotBorder": "1",
            "piefillalpha": "60",
            "pieborderthickness": "2",
            "hoverfillcolor": "#CCCCCC",
            "piebordercolor": "#000000",
            "hoverfillcolor": "#CCCCCC",
            //"numberprefix": "$",
            "plottooltext": "$label",
            //Theme
            "theme": "fint"
        },
        "category": [{
            "label": "<?php echo $sumtotal;?>",
            "color": "#fdfa19",
            "value": "<?php echo $sumtotal;?>",
            "category": [{
                "label": "Financial",
                "color": "#cccccc",
                "value": "<?php echo $totalval;?>",
                "tooltext": "Financial",
                "category": [{
                    "label": "<?php echo $financial;?>",
                    "color": "#cccccc",
                    "value": "<?php echo $financial;?>"
                }, {
                    "label": "<?php echo $FinancialRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $FinancialRemaining;?>"
                }]
            }, {
                "label": "institutional",
                "color": "#17a086",
                "value": "<?php echo $totalval; ?>",
                "tooltext": "institutional",
                "category": [{
                    "label": "<?php echo $institutional;?>",
                    "color": "#17a086",
                    "value": "<?php echo $institutional;?>"
                }, {
                    "label": "<?php echo $InstitutionalRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $InstitutionalRemaining;?>"
                }]
            },{
                "label": "Environmental",
                "color": "#efa232",
                "value": "<?php echo $totalval; ?>",
                "tooltext": "Environmental",
                "category": [{
                    "label": "<?php echo $environmental;?>",
                    "color": "#efa232",
                    "value": "<?php echo $environmental;?>"
                }, {
                    "label": "<?php echo $EnvironmentalRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $EnvironmentalRemaining;?>"
                }]
            },
             {
                "label": "Social",
                "color": "#ff7857",
                "value": "20",
                "tooltext": "Social",
                "category": [{
                    "label": "<?php echo $social;?>",
                    "color": "#ff7857",
                    "value": "<?php echo $social;?>",
                    "tooltext": "<?php echo $social;?>",

                }, {
                    "label": "<?php echo $SocialRemaining; ?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $SocialRemaining; ?>"
                }]
            }, {
                "label": "Operational",
                "color": "#3669b0",
                "value": "<?php echo $totalval;?>",
                "category": [{
                    "label": "<?php echo $oprational;?>",
                    "color": "#3669b0",
                    "value": "<?php echo $oprational;?>"
                }, {
                    "label": "<?php echo $OprationalRemaining;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $OprationalRemaining;?>"
                }]
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>