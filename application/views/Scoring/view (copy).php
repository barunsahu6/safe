 <?php 
		$token  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$patpod = $this->model->getPatPodDetail($token);

        //print_r($patpod);
		///////////////////////////////////////////////////////
		$tab1='Tab1'; 
        $jsonSoc = array(); 
		$Social  = $this->model->viewDetail($tab1,$token,$patpod->POGUID);
         foreach ($Social as $key => $value) {
             $jsonSoc[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonSocial = json_encode($jsonSoc);

        $Coverage = $Social->Coverage;
		$Affordability = $Social->Affordability;
		/////////////////////////////////////////////////
		$totaltab1=$Coverage+$Affordability;
		$remaining1 = 20-$totaltab1; 
		///////////////////////////////////////////////////
		$tab2='Tab2';
        $jsonOperat = array(); 
		$Operational  = $this->model->viewDetail($tab2,$token,$patpod->POGUID);
		
        foreach ($Operational as $key => $value) {
             $jsonOperat[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonOperational = json_encode($jsonOperat);

		$Training =$Operational->TrainingCapacityAndBuilding;
		$WaterQuality =$Operational->WaterQuality=5;
		$Quality =$Operational->QualityAssurance;
		$Reliability =$Operational->ReliabilityOfOperations;
		//////////////////////////////////////////////////
	    $totaltab2 = $Training+$WaterQuality+$Quality+$Reliability;
		$remaining2 = 20-$totaltab2; 
		////////////////////////////////////
		$tab3='Tab3'; 
		$financial = $this->model->viewDetail($tab3,$token,$patpod->POGUID);
       // print_r($financial); die;
		/////////////////////////////////////////////
		$totalfinance = $financial->FinancialSustainability;
		$remaining3 = 20-$totalfinance;
		
		/////////////////Tab 4 ////////////////
		$tab4='Tab4'; 
        $jsonInst = array();
		$Institutional = $this->model->viewDetail($tab4,$token,$patpod->POGUID);
       // print_r($Institutional); die;
        foreach ($Institutional as $key => $value) {
             $jsonInst[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonInstitutional1 = json_encode($jsonInst);

		$LocalOperation = $Institutional->LocalOperation;
		$Municipal = $Institutional->GramPanchayatMunicipalCorporationApproval;
		$Elecricity = $Institutional->LegalElecricityConnection;
		$ApprovalOfPreisesAndLand = $Institutional->ApprovalOfPreisesAndLand;
		$AprovalForRawSourceWater = $Institutional->AprovalForRawSourceWater;
		$ApprovalForRejectWaterDischarge = $Institutional->ApprovalForRejectWaterDischarge;
		$ServiceProviders = $Institutional->ServiceProviders;
		//////////////////////////////////////
		$totaltab4 = $LocalOperation+$Municipal+$Elecricity+$ApprovalOfPreisesAndLand+$AprovalForRawSourceWater+$ApprovalForRejectWaterDischarge+$ServiceProviders;
		$remaining4 = 20-$totaltab4;
		/////////////////////Tab 5 /////////////////
		$tab5='Tab5';
        $json = array(); 
		$Environmental  = $this->model->viewDetail($tab5,$token,$patpod->POGUID);
        foreach ($Environmental as $key => $value) {
             $json[] =array(
                    'value'=> $value,
                    'label'=>$key);
        }
         $jsonEnvironmental = json_encode($json);
      	$GroundWaterRecharge = $Environmental->GroundWaterRecharge;
		$RejctWaterQuantification = $Environmental->RejctWaterQuantification;
		$RejectWaterUtilisation = $Environmental->RejectWaterUtilisation;
		$RejectWaterDisposal = $Environmental->RejectWaterDisposal;
		///////////////////////////////////////////////////////////////////
		$totaltab5 = $RejectWaterDisposal+$RejectWaterUtilisation+$RejctWaterQuantification+$GroundWaterRecharge;
		$remaining5 = 20-$totaltab5;
		/////////////////////////Grand Total ///////////////////////////////////////
		$grandTotal = $totaltab1+$totaltab2+$totalfinance+$totaltab4+$totaltab5; //die;
		
 ?>
<script src="<?php print FJS; ?>fusioncharts.js"></script>
<script src="<?php print FJS; ?>fusioncharts.theme.fint.js?cacheBust=56"></script>
 <script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'multilevelpie',
    renderAt: 'chart-container',
    id: "myChart",
    width: '450',
    height: '450',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "SOFIE",
            "subcaption": "",
            "showPlotBorder": "2",
            "piefillalpha": "60",
            "pieborderthickness": "2",
            "hoverfillcolor": "#CCCCCC",
            "piebordercolor": "#000000",
            "hoverfillcolor": "#FFFFFF",
            "numberprefix": "$",
            "plottooltext": "$label",
            //Theme
            "theme": "fint"
        },
        "category": [{
            "label": "<?php echo $grandTotal;?>",
            "color": "#fdfa19",
            "value": "<?php echo $grandTotal;?>",
            "category": [{
                "label": "Social ",
                "color": "#ff7857",
                "value": "",
                "tooltext": "Social",
                "category": [{
                    "label": "<?php echo $Social->Coverage;?>",
                    "color": "#ff7857",
					"tooltext": "<?php echo $Social->Coverage;?>",
                    "value": "<?php echo $Social->Coverage;?>",
                }, {
                    "label": "<?php echo $Social->Affordability;?>",
                    "color": "#FFFFFF",
					"tooltext": "<?php echo $Social->Affordability;?>",
                    "value": "<?php echo $Social->Affordability;?>",
                }]
            }, {
                "label": "Operational ",
                "color": "#3669b0",
                "value": "20",
                "tooltext": "Operational",
                "category": [{
                    "label": "<?php echo $totaltab2; ?>",
                    "color": "#3669b0",
                    "value": "<?php echo $totaltab2;?>",
					"tooltext": "<?php echo $totaltab2;?>",
                }, {
                    "label": "<?php echo $remaining;?>",
                    "color": "#FFFFFF",
					"value": "<?php echo $remaining; ?>",
					"tooltext": "<?php echo $remaining; ?>",
                }]
            }, {
                "label": "Institutional",
                "color": "#17a086",
                "value": "20",
                "tooltext": "Institutional",
                "category": [{
                    "label": "<?php echo $totaltab4;?>",
                    "color": "#17a086",
                    "value": "<?php echo $totaltab4;?>",
					 "tooltext": "<?php echo $totaltab4; ?>",
                }, {
                    "label": "<?php echo $remaining4;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $remaining4;?>",
					 "tooltext": "<?php echo $remaining4; ?>",
                }]
            }, {
                "label": "Financial",
                "color": "#cccccc",
                "value": "20",
                "tooltext": "Financial",
                "category": [{
                    "label": "<?php echo $totalfinance; ?>",
                    "color": "#cccccc",
                    "value": "<?php echo $totalfinance; ?>",
                    "tooltext": "<?php echo $totalfinance; ?>",
                },{
                    "label": "<?php echo $remaining3; ?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $remaining3; ?>",
                    "tooltext": "<?php echo $remaining3; ?>",
                }]
            }, {
                "label": "Environmental",
                "color": "#efa232",
                "value": "20",
				 "tooltext": "Environmental",
                "category": [{
                    "label": "<?php echo $totaltab5;?>",
                    "color": "#efa232",
                    "value": "<?php echo $totaltab5;?>"
                }, {
                    "label": "<?php echo $remaining5;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $remaining5;?>"
                }]
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>
<!--  -->

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'column2d',
    renderAt: 'chart-container-social-tab_2',
    width: '450',
    height: '300',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Social",
            //"subCaption": "For current year",
            "xAxisname": "Month",
            //"yAxisName": "Revenues (In USD)",
            "showSum": "1",
           // "numberPrefix": "$",
            //"theme": "fint"
        },

        "categories": [{
            "category": [{
                "label": "<?php echo $Social;?>"
            }, {
                "label": "<?php echo $Social;?>"
            }]
        }],

        "dataset": [{
            "seriesname": "Coverage",
            "data": [{
                "value": "<?php echo $Social->Coverage;?>"
            }, {
                "value": "<?php echo $Social->Affordability;?>"
            }]
        }, {
            "seriesname": "Non-Food Products",
            "data": [{
                "value": "<?php echo $Social->Coverage;?>"
            }, {
                "value": "<?php echo $Social->Affordability;?>"
            }, {
                "value": "8300"
            }, {
                "value": "11800"
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>



<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_3',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Operational",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonOperational;  ?>
        }
    });

    revenueChart.render();
});
</script>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'column2d',
    renderAt: 'chart-container-social-tab_4',
    width: '450',
    height: '300',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Financial",
            //"subCaption": "For current year",
            "xAxisname": "Financial",
            //"yAxisName": "Revenues (In USD)",
            "showSum": "1",
           // "numberPrefix": "$",
           // "theme": "fint"
        },

        "categories": [{
            "category": [{
                "label": "Q1"
            }, {
                "label": "Q4"
            }]
        }],

        "dataset": [{
            "seriesname": "Financial",
            "data": [{
                "value": "<?php echo $financial->FinancialSustainability;?>"
            }]
        }, {
            "seriesname": "Financial",
            "data": [{
                "value": "<?php echo $financial->FinancialSustainability;?>"
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>


<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_5',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Institutional",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonInstitutional1;  ?>
        }
    });

    revenueChart.render();
});
</script>

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-social-tab_6',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Environmental",
                //"subCaption": "Last year",
                "xAxisName": "Month",
               // "yAxisName": "Amount (In USD)",
               // "numberPrefix": "$",
               // "canvasBgAlpha": "0",
                //Background color,ratio and alpha
                "bgColor": "EEEEEE,CCCCCC",
                //"bgratio": "60,40",
                //"bgAlpha": "70,80",
                //Theme 
               // "theme" : "fint"
                
            },

            "data": <?php echo $jsonEnvironmental;  ?>
        }
    });

    revenueChart.render();
});
</script>
<script type="text/javascript">
 FusionCharts.ready(function () {
    var cSatScoreChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container-guage-tab_2',
        width: '400',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Social",
                "subcaption": "",
                "lowerLimit": "0",
                "upperLimit": "20",
                //"lowerLimitDisplay": "Bad",
                //"upperLimitDisplay": "Good",
                //"showValue": "1",
                //"valueBelowPivot": "1",
                //"theme": "fint"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab1;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab1;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }
                ]
            },
           }
    }).render();    
});
</script>
<!-- Operational Guage -->
<script type="text/javascript">
 FusionCharts.ready(function () {
    var cSatScoreChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container-guage-tab_3',
        width: '400',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Operational",
                "subcaption": "",
                "lowerLimit": "0",
                "upperLimit": "20",
                "lowerLimitDisplay": "Bad",
                "upperLimitDisplay": "Good",
                //"showValue": "1",
                //"valueBelowPivot": "1",
                //"theme": "fint"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab2;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab2;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }]
            },
           }
    }).render();    
});
</script>
<script type="text/javascript">
 FusionCharts.ready(function () {
    var cSatScoreChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container-guage-tab_4',
        width: '400',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Financial",
                "subcaption": "",
                "lowerLimit": "0",
                "upperLimit": "20",
                //"lowerLimitDisplay": "Bad",
                //"upperLimitDisplay": "Good",
                //"showValue": "1",
                //"valueBelowPivot": "1",
                //"theme": "fint"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $financial;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $financial;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }
                ]
            },
           }
    }).render();    
});
</script>
<script type="text/javascript">
 FusionCharts.ready(function () {
    var cSatScoreChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container-guage-tab_5',
        width: '400',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Institutional",
                "subcaption": "",
                "lowerLimit": "0",
                "upperLimit": "20",
                //"lowerLimitDisplay": "Bad",
                //"upperLimitDisplay": "Good",
                //"showValue": "1",
                //"valueBelowPivot": "1",
                //"theme": "fint"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab4;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab4;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }
                ]
            },
           }
    }).render();    
});
</script>

<script type="text/javascript">
 FusionCharts.ready(function () {
    var cSatScoreChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container-guage-tab_6',
        width: '400',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Environmental",
                "subcaption": "",
                "lowerLimit": "0",
                "upperLimit": "20",
                //"lowerLimitDisplay": "Bad",
                //"upperLimitDisplay": "Good",
                //"showValue": "1",
                //"valueBelowPivot": "1",
                //"theme": "fint"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "<?php echo $totaltab5;?>",
                        "code": "#FF0000"
                    },              
                    {
                        "minValue": "<?php echo $totaltab5;?>",
                        "maxValue": "20",
                        "code": "#FFFFFF"
                    }
                ]
            },
           }
    }).render();    
});
</script>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Scoring
        <small>preview of Scoring</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"> Scoring </li>
      </ol>
    </section>
	    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Total Score</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
			  <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
      		  <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			  	<div id="chart-container"></div>
			</div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <!-- Start Asset details Here ----->
				<div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_2"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_2"></div>
					</div>
				</div>
		 </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
			 <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_3"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_3"></div>
					</div>
				</div>
		</div>
			  <!-- /.tab-pane -->
		<div class="tab-pane" id="tab_4">
			 <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_4"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_4"></div>
					</div>
				</div>
	</div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_5">
                
			 <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_5"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_5"></div>
					</div>
				</div>
		</div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_6">
			   <div class="row">
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-guage-tab_6"></div>
					</div>
					<div class="col-sm-9 col-md-6">
						<div id="chart-container-social-tab_6"></div>
					</div>
				</div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->
