  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Scoring 
        <small>preview of Scoring Management</small>
      </h1>
	  
	    
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Scoring Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Scoring </h3>
            </div>
            <!-- /.box-header -->
			 <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Operations</th>
                  <th>Date</th>
                  <!--<th>Update</th>-->
                </tr>
                </thead>
                <tbody>
				<?php 	
				$i=1;
				$getdata = $this->model->getDetail();
				foreach($getdata as $key=>$detail){
					$URL = $detail->CountryName."/".$detail->StateName."/".$detail->DistrictName."/".$detail->BlockName."/".$detail->VillageName;
				?>
                <td><?php echo $i;?></td>
                  <td><?php echo $URL;?></td>
                  <td><a href="<?php print base_url().$this->router->class.'/view/'.$detail->PlantGUID; ?>" >
				  <?php echo date('d-m-Y', strtotime($detail->VisitDate)); ?>
				  </a></td>
 <!--<td><a href="<?php //print base_url().$this->router->class.'/edit/'.$detail->PlantGUID; ?>" ><span class="fa fa-fw fa-edit"> </span></a></td>-->
                   
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Operations</th>
                  <th>Date</th>
                  <!--<th>Update</th>-->
                  
                </tr>
                </tfoot>
              </table>
            </div>
           
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
         
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 
<script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
      <script>
		function confirm_delete() {
			var r = confirm("Are you sure to delete record?");
			if (r == true) {
				return true;
			} else {
				return false;
			}
		}

   </script>	