  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PATOOL
        <small>preview of PATOOL</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"> PATOOL </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
			  <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
      		  <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			  
			  <!-- Start Assessors details Here ----->
			  
			   <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Assessors details</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Visit Date </th>
                  <th><input type="Date" class="form-control"  name="VisitDate" id="VisitDate" placeholder=" Visit Date" required></th>
                  <th>Assessed by</th>
                  <th><input type="text" class="form-control"  name="Assessing_by" id="Assessing_by" placeholder=" Enter Assessing By" required></th>
                  <th col="2">Location:</th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Assessing agency</th>
                  <th><input type="Date" class="form-control"  name="Assessing_agency" id="Assessing_agency" placeholder=" Enter Assessing Agency" required>
                  </th>
                  <th>Assessing address</th>
                  <th> <input type="text" class="form-control"  name="Assessing_address" id="Assessing_address" placeholder=" Enter Assessing Address" required></th>
                  <th>Latitude</th>
				  <th><input type="email" class="form-control"  name="Latitude" id="Latitude" required></th>
                </tr>
                <tr>
                  <th>Contact name</td>
                  <th><input type="text" class="form-control"  name="Contact_name" id="Contact_name" placeholder="Enter Contact Name" required></th>
                  <th>Email</th>
                  <th><input type="email" class="form-control"  name="Email" id="Email" placeholder=" Enter Email Id " required></th>
                  <th>Longitude	</th>
				  <th><input type="email" class="form-control"  name="Longitude" id="Longitude" required></th>
                </tr>
                </tbody>
              </table>
			 </div>
			  <!-- End Assessors details Here ----->
			   <!-- Start Plant Address Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Plant Address</h3>
			</div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>PlantID/Plant name</th>
                  <th><input type="Date" class="form-control"  name="Plant_name" id="Plant_name" placeholder=" Enter Plant Name" required></th>
                  <th>Country</th>
                  <th><input type="text" class="form-control"  name="Country" id="Country" placeholder=" Enter Country" required></th>
                  <th >State</th>
				  <th><input type="text" class="form-control"  name="State" id="state" placeholder=" Enter State" required></th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>District</th>
                  <th><input type="text" class="form-control"  name="District" id="District" placeholder=" Enter District " required>
                  </th>
                  <th>City</th>
                  <th> <input type="text" class="form-control"  name="City" id="City" placeholder=" Enter City " required></th>
                  <th>Ward number</th>
				  <th><input type="email" class="form-control"  name="Ward_number" id="Ward_number" required></th>
                </tr>
                <tr>
                  <th>Pincode</td>
                  <th><input type="text" class="form-control"  name="Contact_name" id="Contact_name" placeholder="Enter Contact Name" required></th>
                  <th colspan="2">
                         <input type="radio" name="Urban" id="Urban" value="Urban">Urban
						 <input type="radio" name="Rural" id="Rural" value="Urban">Rural
                   </th>
                  
                  <th></th>	
				  <th></th>
                </tr>
                </tbody>
              </table>
			 </div>
			 <!-- End Plant Address Here ----->
			 
			 <!-- Start Existing Plant Details Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Existing Plant Details</h3>
			</div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Date of establishment </th>
                  <th><input type="Date" class="form-control"  name="Date_of_establishment" id="Date_of_establishment" placeholder=" Date of establishment" required></th>
                  <th>Age of plant(month)</th>
                  <th><input type="text" class="form-control"  name="Age_of_plant" id="Age_of_plant" placeholder=" Enter Age of plant " required></th>
                  <th>Plant capacity(lph)</th>
				  <th><input type="text" class="form-control"  name="Plant_capacity" id="Plant_capacity" placeholder=" Enter Plant capacity " required></th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Plant Manufactured by</th>
                  <th><input type="Date" class="form-control"  name="Plant_Manufactured_by" id="Plant_Manufactured_by" placeholder=" Enter Plant Manufactured by" required>
                  </th>
                  <th>Remote Monitoring System</th>
                  <th> <Select type="Select" class="form-control"  name="remote_monitoring_system" id="remote_monitoring_system" placeholder=" Enter Remote Monitoring System" required>
				  <option value"No">No</option>
				  <option value"Yes">Yes</option>
				  </select>
				  </th>
                  <th></th>
				  <th></th>
                </tr>
                <tr>
                  <th colspan="2">
				   <div class="box box-primary">contaminant in raw water
					<span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
			  </td>
                  <th colspan="2">
				  <div class="box box-primary">Treatment steps
					<span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
				  </th>
                  <th colspan="2">
				  <div class="box box-primary">Plant funded by
					 <span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
					</th>
				  </tr>
                </tbody>
              </table>
			 </div>
			  <!-- End Existing Plant Details Here ----->
			   <!-- Start Asset details Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Asset details</h3>
			</div>
			     <table  class="table table-bordered table-hover">
                <thead>
               <tr>
                  <th colspan="2">
				   <div class="box box-primary">Machinery
					<span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
			  </th>
			  <th colspan="2">
				   <div class="box box-primary">Land
					<span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
			  </th>
			  <th colspan="2">
				   <div class="box box-primary">Building
					<span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
			  </th>
                  <th colspan="2">
				  <div class="box box-primary">Raw water source
					<span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
				  </th>
                  <th colspan="2">
				  <div class="box box-primary">Electricity connection
					 <span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
					</th>
					<th colspan="2">
				  <div class="box box-primary">Aggregator
					 <span class="input-group-addon">
                         <input type="checkbox">
                      </span>
				     <span class="input-group-addon">
                          <input type="checkbox">
                     </span>
					<span class="input-group-addon">
                          <input type="checkbox">
                    </span>
					</div>
					</th>
					</tr>
				  </thead>
                
              </table>
			   </div>
			 
               <!-- End Asset details Here ----->
		</div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <!-- Start Asset details Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Social sustainability</h3>
			</div>
			 <table  class="table table-bordered table-hover">
                <thead>
               <tr>
                  <th>Population </th>
                  <th><input type="text" class="form-control"  name="Population" id="Population" placeholder=" Population" required></th>
                  <th>Number of household</th>
                  <th><input type="text" class="form-control"  name="Number_of_household" id="Number_of_household" placeholder=" Enter Number Of Household" required></th>
                  <th>Number of household within 500m</th>
				  <th><input type="text" class="form-control"  name="Number_of_household_within_500m" id="Number_of_household_within_500m" placeholder=" Enter Number Of Household" required></th>
			  </tr>
			   <tr>
                  <th>Number of household registered </th>
                  <th><input type="text" class="form-control"  name="Number_of_household_registered" id="Number_of_household_registered" placeholder=" Number of household registered" required></th>
                  <th>Average number of monthly water cards/RFID cards</th>
                  <th><input type="text" class="form-control"  name="Average_number_of_monthly_water_cards" id="Average_number_of_monthly_water_cards" placeholder=" Enter Average number of monthly water cards" required></th>
                  <th>Distribution</th>
				  <th><input type="text" class="form-control"  name="Distribution" id="Distribution" placeholder=" Enter Distribution" required></th>
			  </tr>
			   <tr>
                  <th colspan="2">
					<div class="box box-primary">Social composition
					<div class="checkbox">
                    <label><input type="checkbox">SC</label>
				    </div>
					
					<div class="checkbox">
                    <label><input type="checkbox">ST</label>
				    </div>
					<div class="checkbox">
                    <label><input type="checkbox">OBC</label>
				    </div>

					<div class="checkbox">
                    <label><input type="checkbox">General</label>
				    </div>					
				     <div class="checkbox">
                    <label><input type="checkbox">All inclusion</label>
				    </div>
			  </th>
                  <th colspan="4">
				  <div class="box box-primary">Distribution/Distance points
					<table  class="table table-bordered table-hover">
					<thead>
					 <tr>
					  <th>Distribution point</th>
					  <th><input type="text" class="form-control"  name="Distribution_point" id="Distribution_point" placeholder=" Enter Distribution point" required></th>
					  <th>Distance</th>
					  <th><input type="text" class="form-control"  name="Distance" id="Distance" placeholder=" Enter Distance " required></th>
					  
					</tr>
					</thead>
					</table>
				  </th>
			</tr>
			 </thead>
           </table>
			   </div>
		 </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
               <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Plant production capacity(litres)</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Avg daily water produced in litres</th>
                  <th><input type="Date" class="form-control"  name="daily_water_produced_litres" id="daily_water_produced_litres" placeholder="Enter daily water produced in litres" required></th>
                  
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Avg monthly water production in litres</th>
                  <th><input type="Date" class="form-control"  name="monthly_water_produced_litres" id="monthly_water_produced_litres" placeholder=" Enter Avg monthly water production in litres" required>
                  </th>
                 
                </tr>
                <tr>
                  <th>PeakSale</td>
                  <th><input type="text" class="form-control"  name="Peak_Sale" id="Peak_Sale" placeholder="Enter Peak Sale" required></th>
                  
                </tr>
                </tbody>
              </table>
			 </div>
			 
			 
              <!----  Start Here    -->			 
			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Kiosk operator details</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th><input type="text" class="form-control"  name="name" id="name" placeholder="Enter Name " required></th>
                  <th>Designation</th>
                  <th>
				  <Select type="select" class="form-control"  name="Designation" id="Designation" required>
				  <option value=""></option>
				  </select>
				  </th>
				  <th>Contact number</th>
                  <th>
				  <input type="text" class="form-control"  name="contact_number" id="contact_number" placeholder="Enter Contact Number " required>
				  </th>
			    </tr>
              </table>
			 </div>
			 <!----- End here -->
			 
			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training and capacity building</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>
				  
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training recieved</h3>
			  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="" name="Electrical_safety" id="Electrical_safety" >
                     Electrical safety
                    </label>
                  </div>
                   <div class="checkbox">
                    <label>
                       <input type="checkbox" value="" name="Plant_opration_management" id="Plant_opration_management" >
                      Plant operation and managment
                    </label>
                  </div>
				<div class="checkbox" value="" name="Water_quality" id="Water_quality">
                    <label>
                      <input type="checkbox">
                      Water quality
                    </label>
                  </div>
				<div class="checkbox" value="" name="Consumer_awareness" id="Consumer_awareness">
                    <label>
                      <input type="checkbox">
                      Consumer awareness
                    </label>
                  </div>
					<div class="checkbox" value="" name="Accounts_book_keeping" id="Accounts_book_keeping">
                    <label>
                      <input type="checkbox">
                     Accounts and book keeping
                    </label>
                  </div>
				  
			 </div>
			 
			 
			 
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Quality assurance and Hygiene</h3>
			  </div>
			<div class="row">
			  <div class="col-sm-4">Earthing</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Earthing" id="Earthing">
                    <option>option 1</option>
                   
                  </select>
				  </div>
			  </div> 

		<div class="row">
			<div class="row">Raw water source</div>
			  <div class="col-sm-4">Open well covered</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="open_well_covered" id="open_well_covered">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			  <div class="col-sm-4">Bore well casing</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Bore_well_casing" id="Bore_well_casing">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 			  
			
			<div class="row">
			<div class="row">Treated water tank</div>
			  <div class="col-sm-4">Inside the plant</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="open_well_covered" id="open_well_covered">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			  <div class="col-sm-4">Covered</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Bore_well_casing" id="Bore_well_casing">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 		
			
			
			<div class="row">
			<div class="row">Plant hygiene inside</div>
			  <div class="col-sm-4">Cleanliness in plant</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Cleanliness_in_plant" id="Cleanliness_in_plant">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			  <div class="col-sm-4">Leakage</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Leakage" id="Leakage">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 


			<div class="row">
			<div class="row">Plant hygiene outside</div>
			  <div class="col-sm-4">Cleanliness near treatment plant</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Cleanliness_in_plant" id="Cleanliness_in_plant">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			<div class="row">Water fill station</div>
			  <div class="col-sm-4">Moss/algae growth</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Cleanliness_in_plant" id="Cleanliness_in_plant">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 
			<div class="row">
			  <div class="col-sm-4">Presence of puddle</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Leakage" id="Leakage">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 			  
			</div>

				  </th>
			<th valign="top">
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Water quality test report from accredited laboratory</h3>
			  </div>
			<div class="row">
			  <div class="col-sm-4">
			<div class="form-group">Pre monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Raw water
                    </label>
                  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Treated water
                    </label>
                  </div>
			  
			  </div>
			  </div>
			 <div class="form-group">Post monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Raw water
                    </label>
                  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Treated water
                    </label>
                  </div>
			      <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Reject water
                    </label>
                  </div>				   
			  </div>
			</div>
			  </div>	
	<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Reliability of operations</h3>
			  </div>
			<div class="row">
			  <div class="col-sm-4">Technical downtime</div>
			  <div class="col-sm-4">
			  <select class="form-control" name="Leakage" id="Leakage">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
				  </div>
			  </div> 	
			  
			  <div class="row">
			  <div class="col-sm-4">No of days</div>
			  <div class="col-sm-4">
			  <input type="text" class="form-control" name="No_of_days" id="No_of_days" value="">
				  </div>
			  </div> 
			  <div class="row">
			  <div class="col-sm-4">No of reason for downtime</div>
			  <div class="col-sm-4">
			  <input type="text" class="form-control" name="reason_for_downtime" id="reason_for_downtime" value="">
				  </div>
			  </div> 	
			  
			  
			  <div class="row">
			  <div class="col-sm-4">Sales day lost</div>
			  <div class="col-sm-6">
			  <input type="text" class="form-control" name="Sales_day_lost" id="Sales_day_lost" value="">
			  
				  </div>
			  </div> 	
			
			
			</div>
			  <div class="col-sm-4">
			<div class="form-group">Pre monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Raw water
                    </label>
                  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Treated water
                    </label>
                  </div>
			  
			  </div>
			  </div>
			 <div class="form-group">Post monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Raw water
                    </label>
                  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Treated water
                    </label>
                  </div>
			      <div class="checkbox">
                    <label>
                      <input type="checkbox">
                     Reject water
                    </label>
                  </div>				   
			  </div>
			</div>
		</div>			  
			
          </th>
                   </tr>
              </table>
			 </div>
			 
           </div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_4">
               
			<div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Capital expenditure</h3>
			  </div>
			  <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Machinery cost</th>
                  <th><input type="text" class="form-control"  name="Machinery_cost" id="Machinery_cost" placeholder=" Machinery cost" required></th>
                  <th>Raw water source cost</th>
                  <th><input type="text" class="form-control"  name="source_cost" id="source_cost" placeholder=" Enter Source cost" required></th>
                  <th>Building cost</th>
				  <th><input type="text" class="form-control"  name="Building_cost" id="Building_cost" placeholder=" Enter Building cost" required></th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Land cost</th>
                  <th><input type="text" class="form-control"  name="Land_cost" id="Land_cost" placeholder=" Enter Land cost" required>
                  </th>
                  <th>Electricity connection cost</th>
                  <th> <input type="text" class="form-control"  name="Electricity_connection_cost" id="Electricity_connection_cost" placeholder=" Enter Assessing Address" required></th>
                  <th>Total cost</th>
				  <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" required></th>
                </tr>
                </tbody>
              </table>
		 </div>
		 
		 
		 <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Revenue</h3>
			  </div>
			
		 </div>
		 
		 
		  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Operating expense (per month)</h3>
			  </div>
			   <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Raw water bill(If any)</th>
                  <th><input type="text" class="form-control"  name="Machinery_cost" id="Machinery_cost" placeholder=" Machinery cost" required></th>
                  <th>Rent of station</th>
                  <th><input type="text" class="form-control"  name="source_cost" id="source_cost" placeholder=" Enter Source cost" required></th>
                  <th>Transport Expense</th>
				  <th><input type="text" class="form-control"  name="Building_cost" id="Building_cost" placeholder=" Enter Building cost" required></th>
				  <th>Chemicals and other consumables</th>
				  <th><input type="text" class="form-control"  name="Building_cost" id="Building_cost" placeholder=" Enter Building cost" required></th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Electricity bill</th>
                  <th><input type="text" class="form-control"  name="Land_cost" id="Land_cost" placeholder=" Enter Land cost" required></th>
                  <th>Operator salary</th>
                  <th> <input type="text" class="form-control"  name="Electricity_connection_cost" id="Electricity_connection_cost" placeholder=" Enter Assessing Address" required></th>
                  <th></th>
				  <th></th>
                </tr>
				 <tr>
                  <th>Generator for electricity</th>
                  <th><input type="text" class="form-control"  name="Land_cost" id="Land_cost" placeholder=" Enter Land cost" required></th>
                  <th></th>
                  <th></th>
                  <th></th>
				  <th></th>
                </tr>
                </tbody>
              </table>
			  <table  class="table table-bordered table-hover">
			  <thead>
			  <tr>
                  <th colspan="3">
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
						Miscellaneous/Any other
                    </label>
                  </div>
				  
				  <th>
				 </tr>
                </thead>
                <tr>
                  <th>S.No.</th>
                  <th>Name</th>
                  <th>Amount</th>
				 </tr>
                
                <tbody>
                <tr>
                  <th>1</th>
                  <th> <input type="text" class="form-control"  name="Electricity_connection_cost" id="Electricity_connection_cost" placeholder=" " required></th>
                  <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" required></th>
                </tr>
				 <tr>
                  <th>2</th>
                  <th> <input type="text" class="form-control"  name="Electricity_connection_cost" id="Electricity_connection_cost" placeholder="" required></th>
                  <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" required></th>
                </tr>
				<tr>
                  <th>3</th>
                  <th> <input type="text" class="form-control"  name="Electricity_connection_cost" id="Electricity_connection_cost" placeholder="" required></th>
                  <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" required></th>
                </tr>
                </tbody>
              </table>
			 <table  class="table table-bordered table-hover">
			  <thead>
			    
                <tr>
                  <th>Service charge</th>
                  <th> <input type="text" class="form-control"  name="Electricity_connection_cost" id="Electricity_connection_cost" placeholder=" " required></th>
                  <th>Sustainability fund</th>
				  <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" required></th>
                </tr>
				 </tbody>
              </table>
			  
			  <table  class="table table-bordered table-hover">
			  <thead>
			   
				<tr>
                  <th colspan="3"> Financial sustainability</th>
                  <tr>
                  <th>OpEx</th>
                  <th> <input type="text" class="form-control"  name="OpEx" id="OpEx" placeholder=" " required></th>
                  </tr>
				   <tr>
                  <th>OpEx +Service charge</th>
                  <th> <input type="text" class="form-control"  name="OpEx_Servicecharge" id="OpEx_Servicecharge" placeholder=" " required></th>
                  </tr>
				   <tr>
                  <th>OpEx +Service charge maintenance reserve</th>
                  <th> <input type="text" class="form-control"  name="OpEx_Servicecharge_maintenance_reserve" id="OpEx_Servicecharge_maintenance_reserve" placeholder=" " required></th>
                  </tr>
				 </thead>
              </table>
			</div>
	 </div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_5">
                
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Institutional sustainability</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
				<th colspan="2">Approved Yes/No</th>
				<th colspan="2">Approved Yes/No</th>
                 </tr>
				 <tr>
                  <th>Local government approval</th>
                  <th><input type="text" class="form-control"  name="name" id="name" placeholder="Enter Name " required></th>
				  <th>reject water discharge</th>
                  <th><input type="text" class="form-control"  name="contact_number" id="contact_number" placeholder="Enter Contact Number " required>
				  </th>
			    </tr>
				
				<tr>
                  <th>Legal electicity connection</th>
                  <th><input type="text" class="form-control"  name="name" id="name" placeholder="Enter Name " required></th>
				  <th>Trained Operator</th>
                  <th><input type="text" class="form-control"  name="contact_number" id="contact_number" placeholder="Enter Contact Number " required>
				  </th>
			    </tr>
				
				<tr>
                  <th>Premises and land</th>
                  <th><input type="text" class="form-control"  name="name" id="name" placeholder="Enter Name " required></th>
				  <th>Raw source water</th>
                  <th><input type="text" class="form-control"  name="contact_number" id="contact_number" placeholder="Enter Contact Number " required>
				  </th>
			    </tr>
				
				<tr>
                  <th colspan="4">Document Upload</th>
                </tr>
				
              </table>
			 </div>
		</div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_6">
               
			   <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Enviromental sustainability</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
				 <tr>
                  <th>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    Reject water Produced
                    </label>
                  </div>
				  </th>
                  <th><input type="text" class="form-control"  name="Reject_water_Produced" id="Reject_water_Produced" required></th>
				  <th></th>
				  <th></th>
			    </tr>
				<tr>
				<th>
				   <div class="checkbox">
                    <label>
                      <input type="checkbox">
                   Reject water disposed
                    </label>
                  </div>
				  </th>
                  <th><input type="text" class="form-control"  name="Reject_water_disposed" id="Reject_water_disposed" required></th>
				  <th></th>
				  <th></th>
				</tr>
				<tr>
                  <th colspan="4" > 
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                   Ground water recharge / rain water harvesting
                    </label>
                  </div>
				  </th>
			    </tr>
				<tr>
                  <th colspan="2">
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                  Reject water utilised
                    </label>
                  </div>			  
				  </th>
				  <th>
				  Activity
				  <input type="text" class="form-control"  name="name" id="name" required>
				  <input type="text" class="form-control"  name="name" id="name" required>
				  <input type="text" class="form-control"  name="name" id="name"  required>
				  <input type="text" class="form-control"  name="name" id="name"  required></th>
				  </th>
                  <th>
				  Litres/Day
				  <input type="text" class="form-control"  name="name" id="name"  required>
				  <input type="text" class="form-control"  name="name" id="name"  required>
				  <input type="text" class="form-control"  name="name" id="name"  required>
				  <input type="text" class="form-control"  name="name" id="name"  required></th>
				  </tr>
				</table>
			 </div>
			   
			   
			   
			   
			   
			   
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->








<div class="left-sidebar">
<style type="text/css">
table > tbody > tr > th{
border:none !important;
}

table > tbody > tr > td{
border:none !important;
}
</style>
	<!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst($this->router->method).' '.ucfirst(str_replace('_',' ',$this->router->class)); ?></div>
                	<span class="tools"><?php print form_button('goback','<i class="fa fa-arrow-circle-left"> Back to Lists</i>',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
						<?php print form_open_multipart($this->router->class.'/'.$this->router->method,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <table class="table table-striped" 
                            <tr>
                              <td colspan="2"></td>
                            </tr>
                            
                            <tr>
                              <th width="20%"> Type :</th>
                              <td width="80%">
                                <?php if(form_error('form[level_id]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('form[level_id]',$types,set_value('form[level_id]'),'id="level_id" required="required" class="form-control"')?>
                                <?php if(form_error('form[level_id]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[level_id]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Full Name :</th>
                              <td>
                                <?php if(form_error('form[first_name]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[first_name]','id'=>'first_name','value'=>set_value('form[first_name]'),'class'=>'form-control','placeholder'=>'First Name','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[first_name]')){?>
                                <label class="control-label" for="inputError"><?php print form_error('form[first_name]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Username :</th>
                              <td>
                                <?php if(form_error('form[access_text]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[access_text]','id'=>'access_text','value'=>set_value('form[access_text]'),'class'=>'form-control','placeholder'=>'Username','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[access_text]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[access_text]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
							
							
							<tr>
                              <th>Password :</th>
                              <td>
                                <?php if(form_error('form[access_key]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('type'=>'password','name'=>'form[access_key]','id'=>'access_key','value'=>set_value('form[access_key]'),'class'=>'form-control','placeholder'=>'Password','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[access_key]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[access_key]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Email Address  :</th>
                              <td>
                                <?php if(form_error('form[email]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('type'=>'text','name'=>'form[email]','id'=>'email','value'=>set_value('form[email]'),'class'=>'form-control','placeholder'=>'Email','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[email]')) {?><label class="control-label" for="inputError"><?php print form_error('form[email]')?></label></div><?php }?>
                              </td>
                            </tr>
                            
                            <tr>
                              <td><?php print form_button('goback','Cancel',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></td>
                              <td><?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?></td>
                            </tr>
                          </table>
                        <?php print form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>