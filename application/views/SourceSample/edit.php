<div class="left-sidebar">
<style type="text/css">
table > tbody > tr > th{
border:none !important;
}

table > tbody > tr > td{
border:none !important;
}
</style>
	<!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst($this->router->method).' '.ucfirst(str_replace('_',' ',$this->router->class)); ?></div>
                	<span class="tools"><?php print form_button('goback','<i class="fa fa-arrow-circle-left"> Back to Lists</i>',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
						<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <table class="table table-striped" 
                            <tr>
                              <td colspan="2"></td>
                            </tr>
                            
                            <tr>
                              <th width="20%">Title :</th>
                              <td width="80%">
                                <?php if(form_error('form[title]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[title]','id'=>'title','value'=>set_value('form[title]',$detail->title),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' title','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[title]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[title]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Header :</th>
                              <td>
							  	<textarea name="form[header]" id="form[header]"><?php print $detail->header; ?></textarea><?php print display_ckeditor($ck_header); ?>
                                <?php /*if(form_error('form[header]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[header]','id'=>'header','value'=>set_value('form[header]',$detail->header),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' header','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[header]')){?>
                                <label class="control-label" for="inputError"><?php print form_error('form[header]')?></label></div>
                                <?php }*/ ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Summary :</th>
                              <td>
							  <textarea name="form[summary]" id="form[summary]"><?php print $detail->summary; ?></textarea><?php print display_ckeditor($ck_summary); ?>
                                <?php /*if(form_error('form[summary]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[summary]','id'=>'summary','value'=>set_value('form[summary]',$detail->summary),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' summary','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[summary]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[summary]')?></label></div>
                                <?php }*/ ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Footer :</th>
                              <td>
							  <textarea name="form[footer]" id="form[footer]"><?php print $detail->footer; ?></textarea><?php print display_ckeditor($ck_footer); ?>
                                <?php /*if(form_error('form[footer]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('type'=>'text','name'=>'form[footer]','id'=>'footer','value'=>set_value('form[footer]',$detail->footer),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' footer','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[footer]')) {?><label class="control-label" for="inputError"><?php print form_error('form[footer]')?></label></div><?php }*/?>
                              </td>
                            </tr>
                
                            <tr>
                              <th>Status :</th>
                              <td><?php print form_dropdown('form[status_id]',$status,set_value('form[status_id]',$detail->status_id),'id="status_id" required="required" class="form-control"')?></td>
                            </tr>
                            
                            <tr>
                              <td><?php print form_button('goback','Cancel',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></td>
                              <td><?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?></td>
                            </tr>
                          </table>
                        <?php print form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>