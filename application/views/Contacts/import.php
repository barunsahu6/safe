<div class="left-sidebar">
<style type="text/css">
table > tbody > tr > th{
border:none !important;
}

table > tbody > tr > td{
border:none !important;
}

input[type="file"]{
padding:0;
height:28px;
}
</style>
	<!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst($this->router->method).' '.ucfirst(str_replace('_',' ',$this->router->class)); ?></div>
                	<span class="tools"><?php print form_button('goback','<i class="fa fa-arrow-circle-left"> Back to Lists</i>',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
						<?php print form_open_multipart($this->router->class.'/'.$this->router->method,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <table class="table table-striped" 
                            <tr>
                              <td colspan="2"></td>
                            </tr>
                            
                            <tr>
                              <th width="20%" height="50px"> Client :</th>
                              <td width="80%">
                                <?php if(form_error('client_id')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('client_id',$clients,set_value('client_id',$token),'id="client_id" required="required" class="form-control"')?>
                                <?php if(form_error('client_id')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('client_id')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th height="50px"><?php print ucfirst(str_replace('_',' ',$this->router->class)); ?> File :</th>
                              <td>
                                <?php print form_input(array('type'=>'file','name'=>'userfile','id'=>'userfile','class'=>'form-control','autocomplete'=>'off','required'=>'required')); ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th height="50px"> Template :</th>
                              <td>
                                <?php if(form_error('template_type')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('template_type',array('HTML'=>'HTML','DIV'=>'DIV'),set_value('template_type'),'id="template_type" required="required" class="form-control"')?>
                                <?php if(form_error('template_type')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[template_type]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th height="50px"> Preferences :</th>
                              <td>
                                <?php if(form_error('types')){ ?><div class="form-group has-error"><?php } //print_r($_POST);die; ?>
								<?php if(count($types)>0) { foreach($types as $type) { $chk = '';//(in_array($type->email_type_id,$_POST['types'])) ? 'checked="checked"' : '';?>
                                <input type="checkbox" name="types[]" id="<?php print $type->email_type_id; ?>" value="<?php print $type->email_type_id; ?>" /> &nbsp;&nbsp; <?php print $type->type_name; ?><br />
                                <?php } } if(form_error('types')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('types')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th height="50px"> Applicable Updates :</th>
                              <td>
                                <?php if(form_error('updates')){ ?><div class="form-group has-error"><?php } ?>
								<?php if(count($updates)>0) { foreach($updates as $update) {?>
                                <input type="checkbox" name="updates[]" id="<?php print $update->update_id; ?>" value="<?php print $update->update_id; ?>" <?php print $chk; ?> /> &nbsp;&nbsp; <?php print $update->title; ?><br />
                                <?php } } if(form_error('updates')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('updates')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <td><?php print form_button('goback','Cancel',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></td>
                              <td><?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?></td>
                            </tr>
                          </table>
                        <?php print form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>