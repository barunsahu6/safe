<div class="left-sidebar">
<style type="text/css">
table > tbody > tr > th{
border:none !important;
}

table > tbody > tr > td{
border:none !important;
}
</style>
	<!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst($this->router->method).' '.ucfirst(str_replace('_',' ',$this->router->class)); ?></div>
                	<span class="tools"><?php print form_button('goback','<i class="fa fa-arrow-circle-left"> Back to Lists</i>',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
						<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <table class="table table-striped" 
                            <tr>
                              <td colspan="2"></td>
                            </tr>
                            
                            <tr>
                              <th width="20%"> Client :</th>
                              <td width="80%">
                                <?php if(form_error('form[client_id]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('form[client_id]',$clients,set_value('form[client_id]',$detail->client_id),'id="client_id" required="required" class="form-control"')?>
                                <?php if(form_error('form[client_id]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[client_id]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th><?php print ucfirst(str_replace('_',' ',$this->router->class)); ?> Name :</th>
                              <td>
                                <?php if(form_error('form[name]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[name]','id'=>'name','value'=>set_value('form[name]',$detail->name),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Name','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[contact_person]')){?>
                                <label class="control-label" for="inputError"><?php print form_error('form[contact_person]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th><?php print ucfirst(str_replace('_',' ',$this->router->class)); ?> Email :</th>
                              <td>
                                <?php if(form_error('form[email]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('type'=>'email','name'=>'form[email]','id'=>'email','value'=>set_value('form[email]',$detail->email),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Email','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[email]')) {?><label class="control-label" for="inputError"><?php print form_error('form[email]')?></label></div><?php }?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th> Template :</th>
                              <td>
                                <?php if(form_error('form[template_type]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('form[template_type]',array('HTML'=>'HTML','DIV'=>'DIV'),set_value('form[template_type]',$detail->template_type),'id="template_type" required="required" class="form-control"')?>
                                <?php if(form_error('form[template_type]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[template_type]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th> Preferences :</th>
                              <td>
                                <?php if(form_error('types')){ ?><div class="form-group has-error"><?php } //print_r($emailtypes);die; ?>
								<?php if(count($types)>0) { foreach($types as $type) { $chk = (array_key_exists($type->email_type_id,$emailtypes)) ? 'checked="checked"' : '';?>
                                <input type="checkbox" name="types[]" id="<?php print $type->email_type_id; ?>" value="<?php print $type->email_type_id; ?>" <?php print $chk; ?> /> &nbsp;&nbsp; <?php print $type->type_name; ?><br />
                                <?php } } if(form_error('types')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('types')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th> Applicable Updates :</th>
                              <td>
                                <?php if(form_error('updates')){ ?><div class="form-group has-error"><?php } ?>
								<?php if(count($updates)>0){ foreach($updates as $update){ $chk = (array_key_exists($update->update_id,$contUpdate)) ? 'checked="checked"' : '';?>
                                <input type="checkbox" name="updates[]" id="<?php print $update->update_id; ?>" value="<?php print $update->update_id; ?>" <?php print $chk; ?> /> &nbsp;&nbsp; <?php print $update->title; ?><br />
                                <?php } } if(form_error('updates')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('updates')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th> <?php print ucfirst(str_replace('_',' ',$this->router->class)); ?> Status :</th>
                              <td>
                                <?php if(form_error('form[status_id]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('form[status_id]',$status,set_value('form[status_id]',$detail->status_id),'id="status_id" required="required" class="form-control"')?>
                                <?php if(form_error('form[status_id]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[status_id]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <td><?php print form_button('goback','Cancel',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></td>
                              <td><?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?></td>
                            </tr>
                          </table>
                        <?php print form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>