<div class="left-sidebar">
	<!-- Search/Filter -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title">Filter / Search</div>
                </div>
                <div class="widget-body">
                    <?php print form_open_multipart($this->router->class.'/'.$this->router->method,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <div class="form-group">
                            <div class="row">
									
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="col-md-4 col-sm-4 col-xs-12"><label class="control-label">Client: </label></div>
                                	<div class="col-md-8 col-sm-8 col-xs-12">
                     <?php print form_dropdown('client_id',$clients,set_value('client_id'),'id="client_id" class="form-control"')?>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                	<div class="col-md-4 col-sm-4 col-xs-12"><label class="control-label">Keyword: </label></div>
                                	<div class="col-md-8 col-sm-8 col-xs-12">
                                        <?php print form_input(array('name'=>'keyword','id'=>'keyword','value'=>set_value('keyword'),'class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="col-md-4 col-sm-4 col-xs-12"><label class="control-label">Order By: </label></div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <select data-placeholder="Order By..." name="Order_by" id="Order_by" class="chosen-select form-control"  tabindex="2">
                                          <option value="Client"> Client</option>
                                            <option value="Contact Person"> Contact Person</option>
											<option value="Email Address"> Email Address</option>
											<option value="Add Date"> Add Date</option>
											<option value="Last Updated"> Last Updated</option>
                                        </select>
										<script type="text/javascript">
  document.getElementById('Order_by').value = "<?php echo $_POST['Order_by'];?>";
</script>		
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <?php if(form_error('types')){ ?><div class="form-group has-error"><?php } //print_r($_POST);die; ?>
								<?php if(count($types)>0) { foreach($types as $type) { $chk = '';//(in_array($type->email_type_id,$_POST['types'])) ? 'checked="checked"' : '';?>
                                <input type="checkbox" name="types[]" id="<?php print $type->email_type_id; ?>" value="<?php print $type->email_type_id; ?>" /> &nbsp;&nbsp; <?php print $type->type_name; ?><br />
                                <?php } } if(form_error('types')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('types')?></label></div>
                                <?php } ?>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                	<div class="col-md-4 col-sm-4 col-xs-12"><label class="control-label">Template: </label></div>
                                	<div class="col-md-8 col-sm-8 col-xs-12">
                                   <?php print form_dropdown('form[template_type]',array('ALL'=>'ALL','HTML'=>'HTML','DIV'=>'DIV'),set_value('form[template_type]'),'id="template_type" required="required" class="form-control"')?>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                            	<div class="col-md-12 col-sm-12 col-xs-12">
                            		<input type="submit" class="btn btn-primary" style="float:right; margin-right:10px;" value="Search" />
                            	</div>
                            </div>
                		</div>
                    <?php print form_close(); ?>
                </div>
            </div>
        </div>
    </div>
      
    <!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst(str_replace('_',' ',$this->router->class)); ?> Lists</div>
                	<span class="tools" style="margin-top:3px;"><a href="<?php print base_url().$this->router->class.'/add'; ?>" class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Add New"><i class="fa fa-plus-square"></i> Add New</a></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
                        <div class="sctab">
                            <table class="table table-striped table-bordered table-hover no-margin sortable">
								<?php foreach($rows as $key=>$row) { $page++; if($key==0) { ?>
                                <thead>
                                    <tr>
                                        <th>Sl.No.</th>
										<?php if(count($row)>=0) { foreach($row as $label=>$value) { if($label!='token') { ?>
                                        <th><?php print ucwords(str_replace('_',' ',$label)); ?></th>
                                        <?php } } } ?>
                                        <th style="text-align:center;"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php } ?>
                                    <tr>
                                        <td><?php print $page.'.'; ?></td>
										<?php if(count($row)>=0) { foreach($row as $lab=>$val) { if($lab!='token') { ?>
                                        <td><?php print $val; ?></td>
                                        <?php } else { ?>
                                        <td align="center">
                                        	<a href="<?php print base_url().$this->router->class.'/edit/'.$val; ?>" class="fa fa-pencil-square-o" title="Edit"></a> &nbsp;|&nbsp;
                                            <a href="<?php print base_url().$this->router->class.'/view/'.$val; ?>" class="fa fa-file-text-o" title="View"></a> &nbsp;|&nbsp;
                                            <a href="<?php print base_url().$this->router->class.'/delete/'.$val; ?>" class="fa fa-times" title="Delete" onClick="return confirm('Are you sure want to delete record?')"></a>
                                        </td>
                                        <?php } } } ?>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <ul class="pagination no-margin"><?php print $paging; ?></ul>
                        </div>
                    	<!--<ul class="pagination no-margin">
                            <li class="disabled"><a href="#">«</a></li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">»</a></li>
                    	</ul>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>