  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Standard Management
        <small>preview of Standard Management</small>
      </h1>
	  
	   <a href="<?php echo site_url()."/Standard/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add Standard
       </a>
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Standard Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Standard </h3>
            </div>
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			
			
			 <div class="box-body">
	
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Country</th>
                  <th>Standard Name </th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
				<?php 	
				$i=1;
				$getdata =$this->db->query("SELECT mstc.CountryID, mstc.CountryName, mstst.`StandardID`, mstst.`StandardName` 
					FROM `mststandard` AS mstst INNER JOIN mstcountry AS mstc ON mstc.CountryID=mstst.`CountryID` WHERE mstst.IsDeleted=0 
					 ORDER BY mstst.StandardID DESC")->result();
				foreach($getdata as $detail){
				?>
                <td><?php echo $i;?></td>
                  <td><?php echo $detail->CountryName;?></td>
                  <td><?php echo $detail->StandardName;?></td>
				  <td><a href="<?php print base_url().$this->router->class.'/edit/'.$detail->StandardID; ?>" ><span class="fa fa-fw fa-edit"> </span></a></td>
                   <td><a href="<?php print base_url().$this->router->class.'/delete/'.$detail->StandardID; ?>" onclick="return confirm_delete()" ><span class="fa fa-fw fa-remove"> </span></a></td>
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Country</th>
                  <th>Standard Name</th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
           
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
         
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 
<script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
      <script>
		function confirm_delete() {
			var r = confirm("Are you sure to delete record?");
			if (r == true) {
				return true;
			} else {
				return false;
			}
		}

   </script>	