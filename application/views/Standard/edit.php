 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Standard Management
        <small>preview of Standard Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Standard Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit Standard </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
			<?php 
			 $token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		     $data = $this->model->getDetail($token);
			 ?>
			  <div class="widget-body">
                    <div class="tab-pane" >
				<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
              <div class="box-body">
                <div class="form-group">
               <?php  $Country = $this->model->getCountry(); ?>
				<div class="form-group">
                  <label for="exampleInputPassword1">Country:</label>
					<select class="form-control" name="form[CountryID]" id="CountryID" required>
                    <option value="">Select </option>
					<?php foreach($Country as $key => $val){  
					if($val->CountryID == $data->CountryID){
					?>
					<option value="<?php echo $val->CountryID;?>"SELECTED ><?php echo $val->CountryName;?></option>
					<?php  }else{ ?>
					
                    <option value="<?php echo $val->CountryID;?>"><?php echo $val->CountryName;?></option>
                    <?php }} ?>
                  </select>
				  </div>
				  <label for="exampleInputEmail1">Standard</label>
				  <?php if(form_error('form[StandardName]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[StandardName]','id'=>'StandardName','value'=>set_value('form[StandardName]',$data->StandardName),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Name','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[StandardName]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[StandardName]')?></label></div>
				<?php } ?>
				  
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            <?php print form_close(); ?>
					  
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	
