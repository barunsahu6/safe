  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TSTForm Management
        <small>preview of TSTForm Management</small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">TSTForm Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          
		<div class="col-xs-12">
         
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List TSTForm</h3>
            </div>
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Location</th>
                 <th>Action</th>
                 </tr>
                </thead>
                <tbody>
				<?php 	
        $i=1;
				foreach($getdata as $detail){
           // $url = print base_url().$this->router->class.'/edit/'.$detail->GUID;
          ?>
                <td><?php echo $i;?></td>
                  <td><?php echo $detail->StateName."/".$detail->DistrictName."/".$detail->BlockName."/".$detail->VillageName;?></td>
                  <td>
                  <a href="<?php print base_url().$this->router->class.'/edit/'.$detail->GUID; ?>" >
                <span class="glyphicon glyphicon-pencil"></span>
                  </a>
                  </td>
                  
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Location</th>
                 <th>Action</th>
                 <!--  <th>Update</th>-->
                  
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->		
 
          </div>
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 
	 
	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>