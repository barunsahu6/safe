 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Aggregator Management
        <small>preview of Aggregator Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Aggregator Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit Aggregator </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
			<?php 
			 $token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		     $data = $this->model->getDetail($token);
			 ?>
			  <div class="widget-body">
                    <div class="tab-pane" >
				<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
              <div class="box-body">
                <div class="form-group">
               <label for="exampleInputEmail1">Aggregator Code</label>
				  <?php if(form_error('form[AggregatorCode]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[AggregatorCode]','id'=>'AggregatorCode','value'=>set_value('form[AggregatorCode]',$data->AggregatorCode),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Code','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[AggregatorCode]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[AggregatorCode]')?></label></div>
				<?php } ?>
				
				<label for="exampleInputEmail1">Aggregator Name</label>
				  <?php if(form_error('form[AggregatorName]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[AggregatorName]','id'=>'AggregatorName','value'=>set_value('form[AggregatorName]',$data->AggregatorName),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Name','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[AggregatorName]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[AggregatorName]')?></label></div>
				<?php } ?>
				  
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            <?php print form_close(); ?>
					  
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	
