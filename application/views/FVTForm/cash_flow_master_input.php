  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FVTForm Management
        <small>preview of <?php echo ucwords('Cash Flow Master Input'); ?></small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">FVTForm Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-xs-12">
    <div id="jstree_demo"></div>
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
      <?php } ?>
</div>
<?php $this->load->view('FVTForm/leftmenu');//include('leftmenu.php')  ?>
 <div class="col-xs-9"  style="overflow-y: scroll; max-height: 500px;">

<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
  <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
  <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
  <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
  <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
<div class="panel panel-default">
    <div class="panel-heading"><label>Community Profile</label></div>
    <div class="panel-body">
      
    <div class="col-md-2">
    <label>Population</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Population" id="Population" value="<?php  echo $details->Population;?>" class="form-control" >
    </div>

    <div class="col-md-2">
    <label>Household size</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Household_size" id="Household_size" value="<?php  echo $details->Household_Size;?>" class="form-control" >
    </div>
    </div>


     <div class="panel-body">
      
    <div class="col-md-2">
    <label>Registered Household</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Registered_Population" id="Registered_Population" value="<?php  echo $details->RegHousehold;?>" class="form-control" >
    </div>

    <div class="col-md-2">
    <label>Consumption (Ltr./HH/Day)</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Consumption" id="Consumption" value="<?php  echo $details->ConsumptionPerHouseHoldPerDay;?>" class="form-control" >
    </div>
    </div>

    <div class="panel-body">
      
    <div class="col-md-2">
    <label>Can size (Ltr.)</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="can_size" id="can_size" value="<?php  echo $details->CanSize;?>" class="form-control" >
    </div>
    

     <div class="col-md-2">
    <label>Distribution hamlets</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Distribution_hamlets" id="Distribution_hamlets" value="<?php  echo $details->DistributionHamlets;?>" class="form-control" >
    </div>


    </div>
</div>

<div class="panel panel-default">

 <div class="panel-heading"><label>Pricing</label></div>
    <div class="panel-body">
      
    <div class="col-md-2">
    <label>Rs.per can</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="rspercan" id="rspercan" value="<?php  echo 5;?>" class="form-control" >
    </div>

 

   
      
    <div class="col-md-2">
    <label>Rs.per liter</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="rsperliter" id="rsperliter" value="<?php  echo 5;?>" class="form-control" readonly>
     </div>

  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><label>Operation Cost</label></div>

    <div class="panel-heading">
    <div class="col-md-6"><label>Variable (per month)</label></div>
    <label>Variable (per month)</label>
    </div>

    <div class="panel-body">
      
    <div class="col-md-2">
    <label>Electricity bill</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Electricity_bill" id="Electricity_bill" value="<?php  echo $details->ElectricityBill;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Electricity_can" id="Electricity_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" readonly >
     </div>
  </div>

  <div class="panel-body">
      
    <div class="col-md-2">
    <label>Chemicals</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Chemicals" id="Chemicals" value="<?php  echo $details->ChemicalsAndConsumables;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Chemicals_can" id="Chemicals_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" readonly>
     </div>
  </div>

   <div class="panel-body">
      
    <div class="col-md-2">
    <label>Cost of delivery</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Cost_of_delivery" id="Cost_of_delivery" value="<?php  echo $details->CostOfDelivery;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Cost_of_delivery_can" id="Cost_of_delivery_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" readonly>
     </div>
  </div>

  <div class="panel-body">
      
    <div class="col-md-2">
    <label>Generator fuel</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Generator_fuel" id="Generator_fuel" value="<?php  echo $details->GeneratorFule;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Generator_fuel_can" id="Generator_fuel_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" readonly>
     </div>
  </div>

<div class="panel-body">
      
    <div class="col-md-2">
    <label>Other OpEx</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="other_opex" id="other_opex" value="<?php  echo $details->OtherOperatorExp;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="other_opex_can" id="other_opex_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" readonly>
     </div>
  </div>

<div class="panel-body">
      
    <div class="col-md-2">
    <label>Entrepreneur ROI (as % of revvenue)</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Entrepreneur_roi" id="Entrepreneur_roi" value="<?php  echo $details->EnterpreneurROI;?>" class="form-control" >
    </div>
      
   <!--  <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Entrepreneur_roi_can" id="Entrepreneur_roi_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div> -->
  </div>

</div>



<div class="panel panel-default">
  <div class="panel-heading"><label>Fixed</label></div>
<div class="panel-body">
      
    <div class="col-md-2">
    <label>Operator salary</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Operator_salary" id="Operator_salary" value="<?php  echo $details->OperatorSalary;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label>Land rent</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Land_rent" id="Land_rent" value="<?php  echo $details->LandRent;?>" class="form-control" >
     </div>
  </div>

<div class="panel-body">
      
    <div class="col-md-2">
    <label>Raw water source rent</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Raw_water_source_rent" id="Raw_water_source_rent" value="<?php  echo $details->RawWaterrSourceRent;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label>Service fee paid</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Service_fee_paid" id="Service_fee_paid" value="<?php  echo $details->ServiceFeePaid;?>" class="form-control" >
     </div>
  </div>

<div class="panel-body">
      
    <div class="col-md-2">
    <label>Spares</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Spares" id="Spares" value="<?php  echo $details->Spares;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label>Maintenance reserve</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Maintenance_reserve" id="Maintenance_reserve" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div>
  </div>

  <div class="panel-body">
      
    <div class="col-md-2">
    <label>Bank loan EMI</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Bank_loan_EMI" id="Bank_loan_EMI" value="<?php  echo $details->BankLoanEMI;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label>Funded by donor</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Funded_by_donor" id="Funded_by_donor" value="<?php  echo $details->FundedbyDonor;?>" class="form-control" >
     </div>
  </div>

<div class="panel-body">
      
    <div class="col-md-2">
    <label>Repayment by donor (in years)</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Repayment_by_donor" id="Repayment_by_donor" value="<?php  echo $details->RepaymentToDonor;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label>Capital repayment</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Capital_repayment" id="Capital_repayment" value="<?php  echo $details->CapitalRepayment;?>" class="form-control" readonly >
     </div>
  </div>

<div class="panel-body">
      
    <div class="col-md-2">
    <label>LNGO cost</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="LNGO_cost" id="LNGO_cost" value="<?php  echo $details->LocalNGO_month;?>" class="form-control" >
    </div>
      
    <div class="col-md-2">
    <label>Field support</label>
    </div>
    <div class="col-md-4">
    <input type="Number" name="Field_support" id="Field_support" value="<?php  echo $details->FieldSupport;?>" class="form-control" >
     </div>
  </div>

 </div>  



<div class="row" style="margin-bottom: 10px;">
  <div class="col-md-6">
    <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?>
<?php print form_close(); ?>
  </div>
</div>
      
    </div>
  </div>

 </div>
 </div>
 </section>
 <script type="text/javascript">
   $(function() {
     $("#example1").dataTable();
   });
     

  $(document).ready(function(){
    calculator_electricity();
  calculator_canprice();
   Chemicalsval();
  Capital_repayment();
other_opex();
Generator_fuel();
Cost_of_delivery();



  $("#can_size").on("change", function(){
    calculator_canprice();
  });
    $("#rspercan").on("change", function(){
    calculator_canprice();
    calculator_electricity();
   
  });

  function calculator_canprice(){

    var can_size = $('#can_size').val();
    var rspercan = $('#rspercan').val();
    var rsperlitercal = rspercan/can_size;

    //$('#rsperliter').val(rsperlitercal.toFixed(2));
    $('#rsperliter').val(rsperlitercal);

  } 

$("#Electricity_bill").on("change", function(){
    calculator_electricity();

  });

$("#Registered_Population").on("change", function(){
    calculator_electricity();

  });

$("#Consumption").on("change", function(){
    calculator_electricity();

  });

$("#can_size").on("change", function(){
    calculator_electricity();

  });
$("#Distribution_hamlets").on("change", function(){
    calculator_electricity();

  });


function calculator_electricity(){

var Electricity_bill = $('#Electricity_bill').val();

/*calculate Kiosk*/
var Registered_Population = $('#Registered_Population').val();
var Consumption = $('#Consumption').val();
var can_size = $('#can_size').val();
var Kiosk = Registered_Population * Consumption/can_size;
var Distribution_hamlets = parseInt($('#Distribution_hamlets').val());

var Electricity_cancal= (Electricity_bill/(Kiosk+Distribution_hamlets)/30);
$('#Electricity_can').val(Electricity_cancal.toFixed(3));

}

$("#Chemicals").on("change", function(){
Chemicalsval();
});

function Chemicalsval(){



    var Chemicals = $('#Chemicals').val();
    /*calculate Kiosk*/
var Registered_Population = $('#Registered_Population').val();
var Consumption = $('#Consumption').val();
var can_size = $('#can_size').val();
var Kiosk = Registered_Population * Consumption/can_size;
var Distribution_hamlets = parseInt($('#Distribution_hamlets').val());

    var Chemicals_cancal= (Chemicals/(Kiosk+Distribution_hamlets)/30);
$('#Chemicals_can').val(Chemicals_cancal.toFixed(3));


  //});

}

$("#Cost_of_delivery").on("change", function(){

Cost_of_delivery();
});
function Cost_of_delivery(){

    var Cost_of_delivery = $('#Cost_of_delivery').val();
   /*calculate Kiosk*/
var Registered_Population = $('#Registered_Population').val();
var Consumption = $('#Consumption').val();
var can_size = $('#can_size').val();
var Kiosk = Registered_Population * Consumption/can_size;


var Distribution_hamlets = parseInt($('#Distribution_hamlets').val());

    var Cost_of_deliverycal= (Cost_of_delivery/(Kiosk+Distribution_hamlets)/30);
$('#Cost_of_delivery_can').val(Cost_of_deliverycal.toFixed(2));

}


$("#Generator_fuel").on("change", function(){
Generator_fuel();
});

function Generator_fuel(){
    var Generator_fuel = $('#Generator_fuel').val();
   /*calculate Kiosk*/
var Registered_Population = $('#Registered_Population').val();
var Consumption = $('#Consumption').val();
var can_size = $('#can_size').val();
var Kiosk = Registered_Population * Consumption/can_size;
var Distribution_hamlets = parseInt($('#Distribution_hamlets').val());

    var Generator_fuelcal= (Generator_fuel/(Kiosk+Distribution_hamlets)/30);
$('#Generator_fuel_can').val(Generator_fuelcal.toFixed(2));
}

$("#other_opex").on("change", function(){
other_opex();

  });
function other_opex(){
    var other_opex = $('#other_opex').val();
    /*calculate Kiosk*/
var Registered_Population = $('#Registered_Population').val();
var Consumption = $('#Consumption').val();
var can_size = $('#can_size').val();
var Kiosk = Registered_Population * Consumption/can_size;
var Distribution_hamlets = parseInt($('#Distribution_hamlets').val());

    var other_opexcal= (other_opex/(Kiosk+Distribution_hamlets)/30);
$('#other_opex_can').val(other_opexcal.toFixed(2));

}

$("#Repayment_by_donor").on("change", function(){
Capital_repayment();
});

function Capital_repayment(){

  var Funded_by_donor = $('#Funded_by_donor').val();
  var Repayment_by_donor = $('#Repayment_by_donor').val();
  var Capital_repayment = $('#Capital_repayment').val();

  var Capital_repaymentcal = Funded_by_donor/Repayment_by_donor/12;
  $('#Capital_repayment').val(Capital_repaymentcal)
}

  });

 </script>
