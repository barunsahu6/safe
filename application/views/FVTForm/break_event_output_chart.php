  <style type="text/css">
    .center{
width: 150px;
  margin: 40px auto;
  
}
  </style>
  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FVTForm Management
        <small>preview of <?php echo ucwords('break event output chart'); ?></small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">FVTForm Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-xs-12">
    <div id="jstree_demo"></div>
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
      <?php } ?>
</div>
<?php $this->load->view('FVTForm/leftmenu');//include('leftmenu.php')  ?>
 <div class="col-xs-9"  style="overflow-y: scroll; max-height: 500px;">

<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
  <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
  <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
  <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
  <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
<div class="panel panel-default">
    <div class="panel-heading"><label>Community Profile</label></div>

      

 <div class="panel-body">
   
   

    <div class="center">
   
    <p>
      </p><div class="input-group">
          <span class="input-group-btn">
              <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
                <span class="glyphicon glyphicon-minus"></span>
              </button>
          </span>
          <input type="text" name="quant[2]" id="iPrice" class="form-control input-number" value="5" min="1" max="100">
          <span class="input-group-btn">
              <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
                  <span class="glyphicon glyphicon-plus"></span>
              </button>
          </span>
      </div>
  <p></p>
</div>



   <div id="chart-container"></div>


    </div>


 </div> 



      
    </div>
  </div>

 </div>
 </div>
 </section>
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
   var count_value=5;
function genrate_breakchart()
{
   var dataSource = {
  chart: {
    caption: "Treated water price",
    yaxisname: "Coust/Revenue",
    subcaption: "2012-2016",
    showhovereffect: "1",
    numbersuffix: "",
    drawcrossline: "1",
    plottooltext: "<b>$dataValue</b> of youth were on $seriesName",
    theme: "fusion"
  },
  categories: [
    {
      category: [
        {
          label: "0"
        },
        {
          label: "50"
        },
        {
          label: "100"
        },
        {
          label: "150"
        },
        {
          label: "200"
        },
        {
          label: "250"
        },
        {
          label: "300"
        },
        {
          label: "350"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Total Cost(Station Level)",
    "data": (function() {
    var data = [];
   
    //var Canperday = ['0', '50', '100','150','200','250','300','350'];
    for (var i = 0; i < 8; i++) {
      var Canperday= 4;
    var OperatorSalary = <?php echo $details->OperatorSalary; ?>;
    var FieldSupport = <?php echo $details->FieldSupport; ?>;
    var LandRent = <?php  echo $details->LandRent;?>;
    var RawWaterrSourceRent = <?php  echo $details->RawWaterrSourceRent;?>;
    var ServiceFeePaid = <?php  echo $details->ServiceFeePaid;?>;
    var Spares =  <?php  echo $details->Spares;?>;
   // var Spares =  <?php // echo $details->Spares;?>;
    //var Maintenancereserve = 10;

    


    var BankLoanEMI = <?php  echo $details->BankLoanEMI;?>;
    var CapitalRepayment = <?php   echo (($details->FundedbyDonor / $details->RepaymentToDonor) / 12);?>;
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var LNGOCost = <?php echo $details->LocalNGO_month; ?>; 

     

    var ElectricityBill= <?php  echo $details->ElectricityBill;?>;
    var EnterpreneurROI = <?php  echo $details->EnterpreneurROI;?>;
    var ChemicalsAndConsumables1 = <?php  echo $details->ChemicalsAndConsumables;?>;
    var Costofdelivery1 = <?php  echo $details->CostOfDelivery;?>;
    var Generatorfuel1 = <?php  echo $details->GeneratorFule;?>;
    var OtherOperatorExp1 = <?php  echo $details->OtherOperatorExp;?>;
    var DistributionHamlets =<?php  echo $details->DistributionHamlets;?>;

var Registered_Population = <?php  echo $details->RegHousehold;?>;
var Consumption = <?php  echo $details->ConsumptionPerHouseHoldPerDay;?>;
var can_size = <?php  echo $details->CanSize;?>;

var Kiosk = parseFloat(Registered_Population) * (parseFloat(Consumption)/parseFloat(can_size));


var capitalvalue = <?php  echo $details->Capitalvalueto_bereplaced;?>;
var ElectricityConn= <?php  echo $details->ElectricityConn;?>;
var RMS = <?php  echo $details->RMS;?>;
var AVR = <?php  echo $details->AVR;?>;
var RO = <?php  echo $details->ROPlant;?>;
var Accessories = <?php  echo $details->Other_Accessories;?>;
var Asset_Life = <?php  echo $details->Assetlifeyears;?>;

var Required_MR = ( (parseFloat(capitalvalue)*(parseFloat(RMS))) + (parseFloat(AVR)+parseFloat(ElectricityConn) + parseFloat(RO) + parseFloat(Accessories)) )/100;

var MaintenanceReservecontribution_annum =Required_MR/parseFloat(Asset_Life);

var Maintenancereserve = MaintenanceReservecontribution_annum / 12;



     var electricity = (parseInt(ElectricityBill) / (Kiosk + parseInt(DistributionHamlets))) / 30;
    

   
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var Costofdelivery = ( parseFloat(Costofdelivery1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
    var ChemicalsAndConsumables = (parseFloat(ChemicalsAndConsumables1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
    var Generatorfuel = (parseFloat(Generatorfuel1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

    var OtherOperatorExp = (parseFloat(OtherOperatorExp1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

        var Variable = ( (parseInt(electricity) * (Canperday*[i]) * 25) + (parseInt(ChemicalsAndConsumables) * (Canperday*[i]) * 25) + (parseInt(Costofdelivery) * (Canperday*[i]) * 25) + (parseInt(Generatorfuel) * (Canperday*[i]) * 25) + (parseInt(OtherOperatorExp) * (Canperday*[i]) * 25) + (parseInt(EnterpreneurROI) * (Canperday*[i]) * 25));



        var Fixed = (parseInt(OperatorSalary) + parseInt(LandRent) + parseInt(RawWaterrSourceRent) + parseInt(ServiceFeePaid) + parseInt(Spares) + parseInt(Maintenancereserve) + parseInt(BankLoanEMI) + parseInt(CapitalRepayment) + parseInt(LNGOCost) + parseInt(FieldSupport));

        var TotalCost_inclmaintreserveLNGOfieldcost = Variable + Fixed;

    var TotalCost_stationlevel = TotalCost_inclmaintreserveLNGOfieldcost -  (parseInt(Maintenancereserve) + parseInt(BankLoanEMI) + parseInt(CapitalRepayment) + parseInt(LNGOCost) + parseInt(FieldSupport));

    

        data.push({
          
            "value": Math.ceil(TotalCost_stationlevel)               
        })
    }

    return data;
})()
    },
    {
      seriesname: "Total Cost(Incl maint reserve)",
     

        "data": (function() {
    var data = [];
   

    

    for (var i = 0; i < 8; i++) {

    var Canperday = 4;
    var OperatorSalary = <?php echo $details->OperatorSalary; ?>;
    var FieldSupport = <?php echo $details->FieldSupport; ?>;
    var LandRent = <?php  echo $details->LandRent;?>;
    var RawWaterrSourceRent = <?php  echo $details->RawWaterrSourceRent;?>;
    var ServiceFeePaid = <?php  echo $details->ServiceFeePaid;?>;
    var Spares =  <?php  echo $details->Spares;?>;
   // var Spares =  <?php // echo $details->Spares;?>;
    //var Maintenancereserve = 10;

    


    var BankLoanEMI = <?php  echo $details->BankLoanEMI;?>;
    var CapitalRepayment = <?php   echo (($details->FundedbyDonor / $details->RepaymentToDonor) / 12);?>;
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var LNGOCost = <?php echo $details->LocalNGO_month; ?>; 

     

    var ElectricityBill= <?php  echo $details->ElectricityBill;?>;
    var EnterpreneurROI = <?php  echo $details->EnterpreneurROI;?>;
    var ChemicalsAndConsumables1 = <?php  echo $details->ChemicalsAndConsumables;?>;
    var Costofdelivery1 = <?php  echo $details->CostOfDelivery;?>;
    var Generatorfuel1 = <?php  echo $details->GeneratorFule;?>;
    var OtherOperatorExp1 = <?php  echo $details->OtherOperatorExp;?>;
    var DistributionHamlets =<?php  echo $details->DistributionHamlets;?>;

var Registered_Population = <?php  echo $details->RegHousehold;?>;
var Consumption = <?php  echo $details->ConsumptionPerHouseHoldPerDay;?>;
var can_size = <?php  echo $details->CanSize;?>;

var Kiosk = parseFloat(Registered_Population) * (parseFloat(Consumption)/parseFloat(can_size));


var capitalvalue = <?php  echo $details->Capitalvalueto_bereplaced;?>;
var ElectricityConn= <?php  echo $details->ElectricityConn;?>;
var RMS = <?php  echo $details->RMS;?>;
var AVR = <?php  echo $details->AVR;?>;
var RO = <?php  echo $details->ROPlant;?>;
var Accessories = <?php  echo $details->Other_Accessories;?>;
var Asset_Life = <?php  echo $details->Assetlifeyears;?>;

var Required_MR = ( (parseFloat(capitalvalue)*(parseFloat(RMS))) + (parseFloat(AVR)+parseFloat(ElectricityConn) + parseFloat(RO) + parseFloat(Accessories)) )/100;

var MaintenanceReservecontribution_annum =Required_MR/parseFloat(Asset_Life);

var Maintenancereserve = MaintenanceReservecontribution_annum / 12;



     var electricity = (parseInt(ElectricityBill) / (Kiosk + parseInt(DistributionHamlets))) / 30;
    

   
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var Costofdelivery = ( parseFloat(Costofdelivery1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
    var ChemicalsAndConsumables = (parseFloat(ChemicalsAndConsumables1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
    var Generatorfuel = (parseFloat(Generatorfuel1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

    var OtherOperatorExp = (parseFloat(OtherOperatorExp1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

        var Variable = ( (parseInt(electricity.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(ChemicalsAndConsumables.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(Costofdelivery.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(Generatorfuel.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(OtherOperatorExp.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(EnterpreneurROI.toFixed(2)) * (Canperday*[i]) * 25));



        var Fixed = (parseInt(OperatorSalary) + parseInt(LandRent) + parseInt(RawWaterrSourceRent) + parseInt(ServiceFeePaid) + parseInt(Spares) + parseInt(Maintenancereserve) + parseInt(BankLoanEMI) + parseInt(CapitalRepayment) + parseInt(LNGOCost) + parseInt(FieldSupport));

        var TotalCost_inclmaintreserveLNGOfieldcost = Variable + Fixed;

       var TotalCost_inclmaintreserve = TotalCost_inclmaintreserveLNGOfieldcost -  (BankLoanEMI + CapitalRepayment + LNGOCost + FieldSupport);

//alert(TotalCost_inclmaintreserveLNGOfieldcost+'/'+(Maintenancereserve + BankLoanEMI + CapitalRepayment + LNGOCost + FieldSupport));
       
        data.push({
        

            "value": Math.ceil(TotalCost_inclmaintreserve)               
        })
    }

    return data;
})()

      
    },
    {
      seriesname: "Total Cost(Incl maint reserve,LNGO and field cost)",
     

        "data": (function() {
    var data = [];
   

    

    for (var i = 0; i < 8; i++) {

    var Canperday = 4;
    var OperatorSalary = <?php echo $details->OperatorSalary; ?>;
    var FieldSupport = <?php echo $details->FieldSupport; ?>;
    var LandRent = <?php  echo $details->LandRent;?>;
    var RawWaterrSourceRent = <?php  echo $details->RawWaterrSourceRent;?>;
    var ServiceFeePaid = <?php  echo $details->ServiceFeePaid;?>;
    var Spares =  <?php  echo $details->Spares;?>;
   // var Spares =  <?php // echo $details->Spares;?>;
    //var Maintenancereserve = 10;

    


    var BankLoanEMI = <?php  echo $details->BankLoanEMI;?>;
    var CapitalRepayment = <?php   echo (($details->FundedbyDonor / $details->RepaymentToDonor) / 12);?>;
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var LNGOCost = <?php echo $details->LocalNGO_month; ?>; 

     

    var ElectricityBill= <?php  echo $details->ElectricityBill;?>;
    var EnterpreneurROI = <?php  echo $details->EnterpreneurROI;?>;
    var ChemicalsAndConsumables1 = <?php  echo $details->ChemicalsAndConsumables;?>;
    var Costofdelivery1 = <?php  echo $details->CostOfDelivery;?>;
    var Generatorfuel1 = <?php  echo $details->GeneratorFule;?>;
    var OtherOperatorExp1 = <?php  echo $details->OtherOperatorExp;?>;
    var DistributionHamlets =<?php  echo $details->DistributionHamlets;?>;

var Registered_Population = <?php  echo $details->RegHousehold;?>;
var Consumption = <?php  echo $details->ConsumptionPerHouseHoldPerDay;?>;
var can_size = <?php  echo $details->CanSize;?>;

var Kiosk = parseFloat(Registered_Population) * (parseFloat(Consumption)/parseFloat(can_size));


var capitalvalue = <?php  echo $details->Capitalvalueto_bereplaced;?>;
var ElectricityConn= <?php  echo $details->ElectricityConn;?>;
var RMS = <?php  echo $details->RMS;?>;
var AVR = <?php  echo $details->AVR;?>;
var RO = <?php  echo $details->ROPlant;?>;
var Accessories = <?php  echo $details->Other_Accessories;?>;
var Asset_Life = <?php  echo $details->Assetlifeyears;?>;

var Required_MR = ( (parseFloat(capitalvalue)*(parseFloat(RMS))) + (parseFloat(AVR)+parseFloat(ElectricityConn) + parseFloat(RO) + parseFloat(Accessories)) )/100;

var MaintenanceReservecontribution_annum =Required_MR/parseFloat(Asset_Life);

var Maintenancereserve = MaintenanceReservecontribution_annum / 12;



     var electricity = (parseInt(ElectricityBill) / (Kiosk + parseInt(DistributionHamlets))) / 30;


    

   
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var Costofdelivery = ( parseFloat(Costofdelivery1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
    var ChemicalsAndConsumables = (parseFloat(ChemicalsAndConsumables1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
    var Generatorfuel = (parseFloat(Generatorfuel1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

    var OtherOperatorExp = (parseFloat(OtherOperatorExp1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

        var Variable = ( (parseInt(electricity.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(ChemicalsAndConsumables.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(Costofdelivery.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(Generatorfuel.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(OtherOperatorExp.toFixed(2)) * (Canperday*[i]) * 25) + (parseInt(EnterpreneurROI.toFixed(2)) * (Canperday*[i]) * 25));



        var Fixed = (parseInt(OperatorSalary) + parseInt(LandRent) + parseInt(RawWaterrSourceRent) + parseInt(ServiceFeePaid) + parseInt(Spares) + parseInt(Maintenancereserve) + parseInt(BankLoanEMI) + parseInt(CapitalRepayment) + parseInt(LNGOCost) + parseInt(FieldSupport));

        var TotalCost_inclmaintreserveLNGOfieldcost = Variable + Fixed;

       
       
        data.push({
        

            "value": Math.ceil(TotalCost_inclmaintreserveLNGOfieldcost)               
        })
    }

    return data;
})()

    },
    {
      seriesname: "Revenue",
      "data": (function() {
    var data = [];
    //alert(count_value);
    for (var i = 0; i < 8; i++) {
      
        data.push({
          
            "value": Math.ceil((50*[i])*count_value*25)               
        })
    }

    return data;
})()

    }
  ]
};
var myChart = new FusionCharts({
    type: "msline",
    renderAt: "chart-container",
    width: "750",
    height: "450",
    dataFormat: "json",
    dataSource
  }).render();
}	  



  $(document).ready(function(){
    FusionCharts.ready(function() {
      genrate_breakchart();
    });
    $('#iPrice').change(function(){
      count_value=$(this).val();
      genrate_breakchart();
    });

 /*const dataSource = {
  chart: {
    caption: "Treated water price",
    yaxisname: "Cost/Revenue",
    subcaption: "[2005-2016]",
    numbersuffix: " mph",
    rotatelabels: "1",
    setadaptiveymin: "1",
    theme: "fusion"
  },
  data: [
    {
      label: "2005",
      value: "89.45"
    },
    {
      label: "2006",
      value: "89.87"
    }
  ],
  data: [
    {
      label: "2005",
      value: "34.45"
    },
    {
      label: "2006",
      value: "56.87"
    }
  ]
};

FusionCharts.ready(function() {
  var myChart = new FusionCharts({
    type: "line",
    renderAt: "chart-container",
    width: "750",
    height: "450",
    dataFormat: "json",
    dataSource
  }).render();
});*/


  });



//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


 </script>
