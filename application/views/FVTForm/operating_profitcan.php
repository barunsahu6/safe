  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FVTForm Management
        <small>preview of <?php echo ucwords('Operating Profit/Can'); ?></small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">FVTForm Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-xs-12">
    <div id="jstree_demo"></div>
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
      <?php } ?>
</div>
<?php $this->load->view('FVTForm/leftmenu');//include('leftmenu.php')  ?>
 <div class="col-xs-9"  style="overflow-y: scroll; max-height: 500px;">

<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
  <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
  <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
  <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
  <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
<div class="panel panel-default">
    <div class="panel-heading"><label>Operating Profit/Can</label></div>

   <div class="panel panel-default">
  
 





 <div class="panel-body">
   
    <div id="chart-container-Regulartory"></div>
    </div>


 </div> 



      
    </div>
  </div>

 </div>
 </div>
 </section>
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   

  $(document).ready(function(){
  
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Regulartory',
       width: '750',
        height: '415',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Regulartory",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                //"yAxisName": "Regulartory",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                 //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Only operating cost",
                    "value": "<?php echo 200;?>",
                    "Color": "#4A8FD0",
                    
                }, 
                {
                    "label": "Operating cost +Maintenance reserve",
                    "value": "<?php echo 250;?>",
                    "Color": "#F5833C",
                    
                }, 
                {
                    "label": "Operating cost +Bank loan EMI/capital repayment",
                    "value": "<?php echo 150;?>",
                    "Color": "#008080",
                   
                }, 
                {
                    "label": "Operating cost +Bank loan EMI/capital repayment + LNGO  cost",
                    "value": "<?php echo 90;?>",
                    "Color": "#FFC501",
                   
                },
                {
                    "label": "Operating cost +Bank loan EMI/capital repayment + LNGO  cost +Field support",
                    "value": "<?php echo 150;?>",
                    "Color": "#008080",
                   
                }
            ]
        }
    }).render();
});


 /*const dataSource = {
  chart: {
    caption: "Treated water price",
    yaxisname: "Cost/Revenue",
    subcaption: "[2005-2016]",
    numbersuffix: " mph",
    rotatelabels: "1",
    setadaptiveymin: "1",
    theme: "fusion"
  },
  data: [
    {
      label: "2005",
      value: "89.45"
    },
    {
      label: "2006",
      value: "89.87"
    }
  ],
  data: [
    {
      label: "2005",
      value: "34.45"
    },
    {
      label: "2006",
      value: "56.87"
    }
  ]
};

FusionCharts.ready(function() {
  var myChart = new FusionCharts({
    type: "line",
    renderAt: "chart-container",
    width: "750",
    height: "450",
    dataFormat: "json",
    dataSource
  }).render();
});*/


  });

 </script>
