  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FVTForm Management
        <small>preview of <?php echo ucwords('Cash Flow Output'); ?></small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">FVTForm Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-xs-12">
    <div id="jstree_demo"></div>
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
      <?php } ?>
</div>
<?php $this->load->view('FVTForm/leftmenu');//include('leftmenu.php')  ?>
 <div class="col-xs-9"  style="overflow-y: scroll; max-height: 500px;">

<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
  <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
  <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
  <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
  <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
<div class="panel panel-default">
   <style>
  .new-table-2 th {
    background: #337ab7;
    color: #fff;
}
  </style>
<?php 
$Registered_Population[0] = $details->Population;
$master_population=1+($details->PopulationGrothRate/100);

$Household_arr[0]=round($Registered_Population[0]/$details->Household_Size);
$capita_perday[0]= round($details->ConsumptionPerHouseHoldPerDay/$details->Household_Size,2);
$Penetration[0]=($details->RegHousehold/$Household_arr[0])*100;
//$volume_cap[0]=(($total_ammount_capicity-$Kiosk[0]-$Distribution[0])<0 ? $total_ammount_capicity-$Kiosk[0]-$Distribution[0] : 0);
$Kiosk[0]=$Household_arr[0]*$capita_perday[0]*($Penetration[0]/100)*$details->Household_Size*360;
$Distribution[0]=$details->CanSize*$details->DistributionHamlets*360;
$CapacityOfPlant=$details->CapacityOfPlant;
$OperatingHours=$details->OperatingHours;
$Backwash=$details->Backwash;
$Seasonality=$details->Seasonality;
$daily_output_produce=($CapacityOfPlant*($OperatingHours-$Backwash));
$total_ammount_capicity=round(($daily_output_produce*365*(1/12))/$Seasonality*100);
//print_r($daily_output_produce."/".$total_ammount_capicity);exit();
$TreatedWater[0]=$details->TreatedWater;

$Total[0]=min(($Kiosk[0]+$Distribution[0]),$total_ammount_capicity);
$Revenue[0]=$Total[0]*$TreatedWater[0]/$details->CanSize;
$Electricity[0]=$details->ElectricityBill*12;
$CostOfDelivery[0]=$details->CostOfDelivery*12;
$ChemicalsAndConsumables[0]=$details->ChemicalsAndConsumables*12;
$OperatorSalary[0]=$details->OperatorSalary*12;
$LandRent[0]=$details->LandRent*12;
$RawWaterrSourceRent[0]=$details->RawWaterrSourceRent*12;
$ServiceFeePaid[0]=$details->ServiceFeePaid*12;
$Spares[0]=$details->Spares*12;
$Generator[0]=$details->GeneratorFule*12;

for ($i=1; $i < 10 ; $i++) { 
$master_cunsumotion=1+($masterpl_input[$i]->Consumption_Incr/100);    
$Penetration_per=$masterpl_input[$i]->Penetration_Incr;
$Distribution_Volmume=$masterpl_input[$i]->Distribution_vol_Incr;
$Registered_Population[$i]=$Registered_Population[$i-1]*$master_population;
$Household_arr[$i]=round($Registered_Population[$i]/$details->Household_Size);
$capita_perday[$i]=($capita_perday[$i-1]*$master_cunsumotion);
$Penetration[$i]=($Penetration[$i-1]+($Penetration_per));
$Kiosk[$i]=($Household_arr[$i])*($capita_perday[$i])*($Penetration[$i]/100)*$details->Household_Size*360;
$Distribution[$i]=($Distribution[$i-1]*(1+$Distribution_Volmume))/10;
$Total[$i]=min(($Kiosk[$i]+$Distribution[$i]),$total_ammount_capicity);
$TreatedWater[$i]=round($TreatedWater[$i-1]*(1+$masterpl_input[$i]->Price_Incr));
$Revenue[$i]=$Total[$i]*$TreatedWater[$i]/$details->CanSize;
$Electricity[$i]=($Electricity[$i-1]/$Total[$i-1]*$Total[$i])*(1+$masterpl_input->Variable_Exp);
$CostOfDelivery[$i]=($CostOfDelivery[$i-1]/$Total[$i-1]*$Total[$i])*(1+$masterpl_input->Variable_Exp);
$ChemicalsAndConsumables[$i]=($ChemicalsAndConsumables[$i-1]/$Total[$i-1]*$Total[$i])*(1+$masterpl_input->Variable_Exp);
$Generator[$i]=$Generator[$i-1]*(1+$masterpl_input[$i]->Distribution_vol_Incr);
$OperatorSalary[$i]=$OperatorSalary[$i-1]*(1+$masterpl_input[$i]->Fixed_Exp);
$LandRent[$i]=$LandRent[$i-1]*(1+$masterpl_input[$i]->Fixed_Exp);
$RawWaterrSourceRent[$i]=$RawWaterrSourceRent[$i-1]*(1+$masterpl_input[$i]->Fixed_Exp);
$ServiceFeePaid[$i]=$ServiceFeePaid[$i-1]*(1+$masterpl_input[$i]->Fixed_Exp);
$Spares[$i]=$Spares[$i-1]*(1+$masterpl_input[$i]->Fixed_Exp);

}
//print_r($Total);exit();
?>
  
    <div class="container">            
  <table class="table table-bordered new-table-2" id="userTable" style="width:100%;">
    <thead>
              <tr>
                <th></th>
                <?php for ($i=1; $i <= 10 ; $i++) { ?>
                <th>Year <?php echo $i; ?></th>
                <?php } ?>
               
               





              </tr>
            </thead>
            <tbody style="background: #cecece;">
   <tr style="background: #fff;">
        <td colspan="3">Volume build up</td> 
      </tr>
                              <tr>
                                <td>Population</td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Registered_Population[$i]);?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Household </td>
                                 <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo ($Household_arr[$i]);?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Liter per capita per day </td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($capita_perday[$i],2);?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Penetration </td>
                               <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo ($Penetration[$i]);?></td>
                              <?php } ?>
                            </tr>
                            <tr>
                                <td>Kiosk </td>
                                 <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Kiosk[$i]);?></td>
                              <?php } ?>
                            </tr>
                             <tr>
                                <td>Distribution </td>
                                 <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Distribution[$i]);?></td>
                              <?php } ?>
                            </tr>
                             <tr>
                                <td><b>Total</b> </td>
                                 <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Total[$i]); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td><b>Can per day</b> </td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Total[$i]/360/20); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Price per can </td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo ($TreatedWater[$i]); ?></td>
                              <?php } ?>
                            </tr>
                           
                           
                               <!--  <tr>
                                    <td class="text-center" colspan="10">No Data Found.</td>
                                </tr> -->
                           
                        </tbody>

                         <tbody style="background: #fceb21;">
   <tr style="background: #fff;">
        <td colspan="3">Cash-flow</td> 
      </tr>
                              <tr>
                                <td>Revenue</td>
                               <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Revenue[$i]); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td><b>Expenses </b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>

                            <tr>
                                <td>Variable </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>

                            <tr>
                                <td>Electricity bill </td>
                               <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Electricity[$i]); ?></td>
                              <?php } ?>
                            </tr>
                            <tr>
                                <td>Generator fuel </td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Generator[$i]); ?></td>
                              <?php } ?>
                            </tr>
                             <tr>
                                <td>Cost of delivery </td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($CostOfDelivery[$i]); ?></td>
                              <?php } ?>
                            </tr>
                             <tr>
                                <td>Total </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>

                            <tr>
                                <td>Chemicals </td>
                               <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($ChemicalsAndConsumables[$i]); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Operator Salary </td>
                                 <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($OperatorSalary[$i]); ?></td>
                              <?php } ?>
                            </tr>
                            <tr>
                                <td>Land rent </td>
                              <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($LandRent[$i]); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Raw water source rent </td>
                               <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($RawWaterrSourceRent[$i]); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Service fee paid </td>
                               <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($ServiceFeePaid[$i]); ?></td>
                              <?php } ?>
                            </tr>

                            <tr>
                                <td>Spares </td>
                                <?php for ($i=0; $i < 10 ; $i++) { ?>
                                <td><?php echo round($Spares[$i]); ?></td>
                              <?php } ?>
                            </tr>
                            <tr>
                                <td>Non-recurring expenses</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Membrane replacement </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Motor replacement</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Total expenses</b> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Gross profit </b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Bank loan installment</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Operating profit</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Entrepreneur ROI </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Net surplus</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Maintenance reserve (MR) </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Surplus after MR</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Capital repayment </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Surplus after MR and CR</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>LNGO support expenses</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Surplus after MR,CP and LNGO </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Field support </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Surplus after MR,CR,LNGO and field support</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Cumulative net surplus </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Total return to entrepreneur </b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td><b>Payback period (Years) </b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Capital recovery for the year </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>Commulative capital recovery </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            <tr>
                                <td>%Capital recovery </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>  
                            </tr>
                            

                           
                           
                               <!--  <tr>
                                    <td class="text-center" colspan="10">No Data Found.</td>
                                </tr> -->
                           
                        </tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
   



 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   

  $(document).ready(function(){
  

  });

 </script>
