  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FVTForm Management
        <small>preview of <?php echo ucwords('Per can analysis'); ?></small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">FVTForm Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-xs-12">
    <div id="jstree_demo"></div>
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
      <?php } ?>
</div>
<?php $this->load->view('FVTForm/leftmenu');//include('leftmenu.php')  ?>
 <div class="col-xs-9"  style="overflow-y: scroll; max-height: 500px;">

<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
  <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
  <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
  <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
  <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
<div class="panel panel-default">
    <div class="panel-heading"><label>Break Event Graph</label></div>

  
    <div class="row mt-3">
      <div class="col-md-12">
        <div class="table-bg mb-4">
          <table class="table-new datatable_formate table-hover dt-responsive" id="userTable"  style="width:100%;">
            <thead>
              <tr>
                <th>Breaked own of</th>
                <th>Per month cost</th>
                <th>Per can cost</th>
                <th>Action</th>
               
               
              </tr>
            </thead>
            <tbody>
                           
                            <tr>
                                <td>OpEx</td>
                                <td>9900</td>
                                <td>2.36</td>
                                
                                <td><a href="" class="edit_table_icon">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a></td> 
                            </tr>
                            <tr>
                                <td>FSE fee</td>
                                <td>3000</td>
                                <td>0.71</td>
                                
                                <td><a href="" class="edit_table_icon">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a></td> 
                            </tr>

                             <tr>
                                <td>Entrep reneur ROI</td>
                                <td>4200</td>
                                <td>1</td>
                                
                                <td><a href="" class="edit_table_icon">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a></td> 
                            </tr>
                           
                               <!--  <tr>
                                    <td class="text-center" colspan="10">No Data Found.</td>
                                </tr> -->
                           
                        </tbody>
          </table>
        </div>
      </div>
    </div>


    </div>
  </div>

 </div>
 </div>
 </section>
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   

  $(document).ready(function(){
  

  });

 </script>
