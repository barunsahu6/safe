 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      TST Form 
      <small>TST Form</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">TST Form</a></li>

    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default" >


      <div class="row">
       <div class="col-md-3"  style="overflow: scroll; height: 500px;">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 10px">S.No.</th>
                  <th>Location</th>
                </tr>
              </thead>
              <?php   
              $i=1;
              foreach($getdata as $detail){
                ?>
                <tbody>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td><a href="<?php print base_url().$this->router->class.'/edit/'.$detail->GUID; ?>" >
                      <?php echo $detail->StateName."/".$detail->DistrictName."/".$detail->BlockName."/".$detail->VillageName;?>
                    </a></td>
                  </tr>
                </tbody>
                <?php $i++; } ?>              
                
              </table>
            </div>
            <!-- /.box-body -->
           <!--  <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div> -->
          </div>
          <!-- /.box -->


        </div>



        <div class="col-md-9"  style="overflow-y: scroll; height: 500px;">
         <div class="box box-default">
          <div class="box-header with-border">

            <h3 class="box-title">Sample Page<?php //echo "<pre>"; print_r($details);?></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div>
          <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
          <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
          <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
          <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
          <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
          <div class="panel panel-default">


            <div class="panel-heading"><label>Collected By</label></div>
            <div class="panel-body">

              <div class="col-md-2">
                <label>Name</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="CollectedBy" id="CollectedBy" value="<?php  echo $details->CollectedBy;?>" class="form-control" >
              </div>

              <div class="col-md-2">
                <label>Mobile</label>
              </div>
              <div class="col-md-4">
                <input type="Number" name="Collected_Mobile" id="Collected_Mobile" value="<?php  echo $details->Mobile;?>" class="form-control" >
              </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading"><label>Sent In</label></div>
            <div class="panel-body">

              <div class="col-md-2">
                <label>ContainerType </label>
              </div>

              <div class="col-md-4">
                <select name="Sent_ContainerType"  id="Sent_ContainerType" class="form-control">
                  <option value="0">--Select--</option>
                  <?php  foreach ($ContinerType as $row) { 
                    if ($details->ContainerType=$row->ContainerType) {?>
                    <option value="<?php echo $row->ContainerID;?>" selected ="SELECTED"><?php echo $row->ContainerType;?></option>
                    <?php } } ?>
                  </select>
                </div>

                <div class="col-md-2">
                  <label>Quantity</label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="Sent_Quantity" id="Sent_Quantity" value="<?php echo $details->Quantity;?>" class="form-control" >
                </div>

              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading"><label>Couriered By</label></div>
              <div class="panel-body">
                <div class="col-md-2">
                  <label>Agency/Individual</label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="Coriered_AgencyName" id="Coriered_AgencyName" value="<?php echo $details->AgencyName;?>" class="form-control" >
                </div>

                <div class="col-md-2">
                  <label>Contact</label>
                </div>
                <div class="col-md-4">
                  <input type="number" name="Coriered_Contact" id="Coriered_Contact" value="<?php echo $details->Contact;?>" class="form-control" >
                </div>

                <div class="col-md-2">
                  <label>Date</label>
                </div>
                <div class="col-md-4">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control datepicker" 
                    value="<?php echo date('d-m-Y',strtotime($details->CourieredDate)); ?>" id="Coriered_CourieredDate" name="Coriered_CourieredDate" size="32">
                  </div>
                </div>

                <div class="col-md-2">
                  <label>Time</label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="Coriered_CourieredTime" id="Coriered_CourieredTime" 
                  value="<?php echo $details->CourieredTime;?>" class="form-control" >
                </div>
              </div>
            </div>     


            <div class="panel panel-default">
              <div class="panel-heading"><label>Couriered To</label></div>
              <div class="panel-body">
              <div class="row">
                
                <div class="col-md-2">
                  <label>Laboratory Name  </label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="Coriered_LabName" id="Coriered_LabName" value="<?php echo $details->LabName;?>" class="form-control" >
                </div>

                <div class="col-md-2">
                  <label>Address</label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="Coriered_labAddress" id="Coriered_labAddress" value="<?php echo $details->Address;?>" class="form-control" >
                </div>
                
              </div>
                <div class="row">
                  
                <div class="col-md-2">
                  <label>Contact</label>
                </div>
                <div class="col-md-4">
                  <input type="number" name="Coriered_labContact" id="Coriered_labContact" value="<?php echo $details->contact;?>" class="form-control" >
                </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="col-md-6"><label>Water Source Location</label></div>
                <label>Water Source Condition</label>
              </div>
              <div class="panel-body">
                <div class="col-md-6">
                 <div class="row">
                  <div class="col-md-4">
                   <label>Country</label>
                 </div>
                 <div class="col-md-8">
                  <select name="WSL_CountryID"  id="WSL_CountryID" class="form-control">
                    <option value="0">--SELECT--</option>
                    <?php  foreach ($country as $row) { 
                      if ($details->CountryID == $row->CountryID) {?>
                      <option value="<?php echo $row->CountryID;?>" selected="SELECTED"><?php echo $row->CountryName;?></option>
                      <?php }else{ ?>
                      <option value="<?php echo $row->CountryID;?>" ><?php echo $row->CountryName;?></option>
                      <?php } } ?>
                    </select>
                  </div>

                  <div class="col-md-4">
                   <label>State</label>
                 </div>
                 <div class="col-md-8">
                   <select name="WSL_StateID"  id="WSL_StateID" class="form-control">
                    <option value="0">--SELECT--</option>
                    <?php  foreach ($state as $row) {
                      if ($details->StateID == $row->StateID) { ?>
                      <option value="<?php echo $row->StateID;?>" selected="SELECTED"><?php echo $row->StateName;?></option>
                      <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                      <?php } } ?>
                    </select>
                  </div>

                  <div class="col-md-4">
                   <label>District</label>
                 </div>
                 <div class="col-md-8">
                  <select name="WSL_DistrictID"  id="WSL_DistrictID" class="form-control">
                    <option value="0">--SELECT--</option>
                    <?php  foreach ($district as $row) { 
                      if ($details->DistrictID == $row->DistrictID) { ?>
                      <option value="<?php echo $row->DistrictID;?>" selected="SELECTED"><?php echo $row->DistrictName;?></option>
                      <?php }else{ ?>
                      <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                      <?php } }?>
                    </select>
                  </div>

                  <div class="col-md-4">
                   <label>Block/Mandal</label>
                 </div>
                 <div class="col-md-8">
                   <input type="text" name="WSL_BlockName" id="WSL_BlockName" value="<?php echo $details->BlockName;?>" class="form-control" >
                 </div>

                 <div class="col-md-4">
                   <label>City/Village</label>
                 </div>
                 <div class="col-md-8">
                   <input type="text" name="WSL_VillageName" id="WSL_VillageName" value="<?php echo $details->VillageName;?>" class="form-control" >
                 </div>

                 <div class="col-md-4">
                   <label>Habitation</label>
                 </div>
                 <div class="col-md-8">
                   <input type="text" name="WSL_Habitation" id="WSL_Habitation" value="<?php echo $details->VillageName;?>" class="form-control" >
                 </div>

                 <div class="col-md-4">
                   <label>Date</label>
                 </div>

                 <div class="col-md-8">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control datepicker" id="WSL_CollectedDate" name="WSL_CollectedDate" 
                    value="<?php echo date('d-m-Y',strtotime($details->CollectedDate));?>" >
                  </div>
                </div>

                <div class="col-md-4">
                 <label>Time</label>
               </div>
               <div class="col-md-8">
                 <input type="text" name="WSL_CollectedTime" id="WSL_CollectedTime" value="<?php echo $details->CollectedTime; ?>" class="form-control" >
               </div>
               <div class="col-md-4">
                 <label>Address</label>
               </div>
               <div class="col-md-8">
                 <input type="text" name="WSL_Address" id="WSL_Address" value="<?php echo $details->Address; ?>" class="form-control" >
               </div>
             </div>
           </div>
           <div class="col-md-6">
            <div class="col-md-6">
              <label>SourceType</label>
            </div>
            <div class="col-md-6">
              <select name="WSC_SourceID"  id="WSC_SourceID" class="form-control">
                <option value="0">--SELECT--</option>
                <?php  foreach ($source as $row) { 
                  if ($details->SourceID == $row->SourceID) {?>
                  <option value="<?php echo $row->SourceID;?>" selected="SELECTED"><?php echo $row->SourceName;?></option>
                  <?php } else{ ?>
                  <option value="<?php echo $row->SourceID;?>" ><?php echo $row->SourceName;?></option>
                  <?php } }?>
                </select>
              </div>

              <div class="col-md-6">
                <label>IsWasteHeatAvailable </label>
              </div>
              <div class="col-md-6">
                <select name="IsWasteHeatAvailable"  id="IsWasteHeatAvailable" class="form-control">
                  <option value="0">--SELECT--</option>
                  <?php foreach ($status as $key=>$value) { 
                    if ($details->IsWasteHeatAvailable == $key) {?>
                    <option value="<?php echo $key;?>" selected="SELECTED"><?php echo $value;?></option>
                    <?php }else{ ?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                    <?php } } ?>
                  </select>
                </div>

                <div class="col-md-6">
                  <label>Electricity</label>
                </div>
                <div class="col-md-6">
                  <select name="Electricity"  id="Electricity" class="form-control">
                    <option value="0">--SELECT--</option>
                    <?php  foreach ($status as $key=>$value) {
                      if ($details->IsElectricityAvailable == $key) {
                        ?>
                        <option value="<?php echo $key;?>" selected="SELECTED"><?php echo $value;?></option>
                        <?php } else{?>
                        <option value="<?php echo $key;?>" ><?php echo $value;?></option>
                        <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-6">
                      <label>Season</label>
                    </div>
                    <div class="col-md-6">
                      <select name="WSC_Season"  id="WSC_Season" class="form-control">
                        <option value="0">--SELECT--</option>
                        <?php  foreach ($season as $row) { 
                          if ($details->SeasonID == $row->SeasonID) {
                            ?>
                            <option value="<?php echo $row->SeasonID;?>" selected="SELECTED"><?php echo $row->SeasonName;?></option>
                            <?php } else{ ?>
                            <option value="<?php echo $row->SeasonID;?>" ><?php echo $row->SeasonName;?></option>
                            <?php } } ?>
                          </select>
                        </div>

                        <div class="col-md-6">
                          <label>Weather</label>
                        </div>
                        <div class="col-md-6">
                          <select name="WSC_Weather"  id="WSC_Weather" class="form-control">
                            <option value="0">--SELECT--</option>
                            <?php  foreach ($weather as $row) { 
                              if ($details->WeatherID == $row->WeatherID) {
                                ?>
                                <option value="<?php echo $row->WeatherID;?>" selected="SELECTED"><?php echo $row->WeatherName;?></option>
                                <?php }else{ ?>
                                <option value="<?php echo $row->WeatherID;?>" selected="SELECTED"><?php echo $row->WeatherName;?></option>
                                <?php }  }?>
                              </select>
                            </div>

                            <div class="col-md-6">
                              Ground Water Depth(ft)
                            </div>
                            <div class="col-md-6">
                              <input type="text" name="WSC_GWDft" id="WSC_GWDft" 
                              value="<?php echo $details->GroundWaterDepth; ?>" class="form-control" >
                            </div>

                            <div class="col-md-6">
                              Ground Water Depth(mtr)
                            </div>
                            <div class="col-md-6">
                              <input type="text" name="WSC_GWDmtr" id="WSC_GWDmtr" value="" class="form-control" >
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!---- Location & Image div  -->
                    <div class="panel panel-default">
                      <div class="panel-heading"><label></label></div>
                      <div class="panel-body">

                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-4"><label>Latitude</label> </div>
                            <div class="col-md-8"><input type="text" name="Xvalue" id="Xvalue" value="<?php  echo $details->Xvalue; ?>" class="form-control"></div>
                          </div>
                          <div class="row">
                            <div class="col-md-4"><label>Longitude</label></div>
                            <div class="col-md-8"><input type="text" name="Yvalue" id="Yvalue" value="<?php  echo $details->Yvalue; ?>" class="form-control"> </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="col-md-6"><input type="file" name="fileupload" id="fileupload"></div>
                          <div class="col-md-6">
                            <div class="col-md-12">
                              <input type="hidden" class="form-control"  name="OldImage" id="OldImage" value="<?php  echo $details->PhotoLink;?>" >
                              <div class="panel panel-default">
                                <?php  if (!empty($details->PhotoLink)) { ?>
                                <img src="<?php print FIMAGES.'/'. $details->PhotoLink; ?>View_Image_Icon.png" class="img-rounded"
                                alt="<?php echo $details->PhotoLink;?>" width="100" height="100">
                                <?php } else { ?>
                                <img src="<?php print FIMAGES; ?>View_Image_Icon.png" class="img-rounded" alt="Cinque Terre" width="100" height="100">
                                <?php } ?>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!----      -->
                      <div class="panel panel-default">
                        <div class="panel-heading"><label>Conducted By</label></div>
                        <div class="panel-body">
                          <div class="col-md-6" >
                            <div class="col-md-6">Laboratory Name</div>
                            <div class="col-md-6"> <input type="text" name="Conducted_LaboratoryName" id="Conducted_LaboratoryName" value="<?php echo $details->LabName ?>"></div>
                            <div class="col-md-6">Address</div>
                            <div class="col-md-6"><input type="text" name="Conducted_Address" id="Conducted_Address" value="<?php echo $details->Address ?>"></div>
                          </div>

                          <div class="col-md-6">
                            <div class="col-md-6">Test Result Date</div>
                            <div class="col-md-6">
                              <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" name="testresultdate" id="testresultdate" class="form-control datepicker" 
                                value="<?php echo date('d-m-Y',strtotime($details->ReportDate)); ?>">
                              </div>
                            </div>
                            <div class="col-md-6">UserType</div>
                            <div class="col-md-6"><select name="Conducted_UserType" id="Conducted_UserType" class="form-control">
                              <?php if ($details->UserType == 4) { ?>
                              <option value="4" selected="SELECTED">Community User</option>
                              <option value="5" >Home User</option>
                              <?php }else{ ?>
                              <option value="4">Community User</option>
                              <option value="5" selected="SELECTED">Home User</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="col-md-6">Report ID</div>
                          <div class="col-md-6"><input type="text" name="Conducted_ReportID" id="Conducted_ReportID" value="<?php echo $details->ReportID;?>"></div>
                        </div>

                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading"><label>Recommended Treatment Technology</label></div>
                      <div class="panel-body">
                        <div class="row"></div>
                      </div>
                    </div>

                    <div class="row" style="margin-bottom: 10px;">
                      <div class="col-md-6">
                        <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?>
                      </div>

                    </div>
                    <?php print form_close(); ?>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </div>