  <style type="text/css">
    .center{
width: 150px;
  margin: 40px auto;
  
}
  </style>
  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FVTForm Management
        <small>preview of <?php echo ucwords('Sensitivity Graph'); ?></small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">FVTForm Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-xs-12">
    <div id="jstree_demo"></div>
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
      <?php } ?>
</div>
<?php $this->load->view('FVTForm/leftmenu');//include('leftmenu.php')  ?>
 <div class="col-xs-9"  style="overflow-y: scroll; max-height: 500px;">

<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
  <input type="hidden" class="form-control"  name="LabGUID" id="LabGUID" value="<?php  echo $details->LabGUID;?>" >
  <input type="hidden" class="form-control"  name="LocationGUID" id="LocationGUID" value="<?php  echo $details->LocationGUID;?>" >
  <input type="hidden" class="form-control"  name="SampleGUID" id="SampleGUID" value="<?php  echo $details->SampleGUID;?>" >
  <input type="hidden" class="form-control"  name="TestGUID" id="TestGUID" value="<?php  echo $details->TestGUID;?>" >
<div class="panel panel-default">
    <div class="panel-heading"><label>Sensitivity Graph</label></div>

      

 <div class="panel-body">
   
   

    



   <div id="chart-container"></div>


    </div>


 </div> 



      
    </div>
  </div>

 </div>
 </div>
 </section>
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   

  $(document).ready(function(){
  

  const dataSource = {
  chart: {
   /* caption: "Treated water price",
    yaxisname: "Coust/Revenue",
    subcaption: "2012-2016",*/
    showhovereffect: "1",
    numbersuffix: "",
    drawcrossline: "1",
    plottooltext: "<b>$dataValue</b> of youth were on $seriesName",
    theme: "fusion"
  },
  categories: [
    {
      category: [
        {
          label: "1"
        },
        {
          label: "2"
        },
        {
          label: "3"
        },
        {
          label: "4"
        },
        {
          label: "5"
        },
        {
          label: "6"
        },
        {
          label: "7"
        },
        {
          label: "8"
        },
        {
          label: "9"
        },
        {
          label: "10"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Revenue",
      data: [
        {
          value: "62"
        },
        {
          value: "72"
        },
        {
          value: "82"
        },
        {
          value: "92"
        },
        {
          value: "102"
        },
         {
          value: "112"
        },
         {
          value: "122"
        },
         {
          value: "132"
        },
        {
          value: "132"
        }

      ]
    },
    {
      seriesname: "Fixed",
      data: [
        {
          value: "16"
        },
        {
          value: "26"
        },
        {
          value: "36"
        },
        {
          value: "46"
        },
        {
          value: "56"
        },
         {
          value: "66"
        },
         {
          value: "77"
        },
         {
          value: "88"
        },

         {
          value: "88"
        }
        ,
         {
          value: "88"
        }
        

      ]
    },
    {
      seriesname: "Variable",
      data: [
        {
          value: "20"
        },
        {
          value: "30"
        },
        {
          value: "40"
        },
        {
          value: "50"
        },
        {
          value: "60"
        },
         {
          value: "70"
        },
         {
          value: "80"
        },
         {
          value: "90"
        },

         {
          value: "88"
        }
        ,
         {
          value: "88"
        }
      ]
    }

  ]
};

FusionCharts.ready(function() {
  var myChart = new FusionCharts({
    type: "msline",
    renderAt: "chart-container",
    width: "750",
    height: "450",
    dataFormat: "json",
    dataSource
  }).render();
});


 /*const dataSource = {
  chart: {
    caption: "Treated water price",
    yaxisname: "Cost/Revenue",
    subcaption: "[2005-2016]",
    numbersuffix: " mph",
    rotatelabels: "1",
    setadaptiveymin: "1",
    theme: "fusion"
  },
  data: [
    {
      label: "2005",
      value: "89.45"
    },
    {
      label: "2006",
      value: "89.87"
    }
  ],
  data: [
    {
      label: "2005",
      value: "34.45"
    },
    {
      label: "2006",
      value: "56.87"
    }
  ]
};

FusionCharts.ready(function() {
  var myChart = new FusionCharts({
    type: "line",
    renderAt: "chart-container",
    width: "750",
    height: "450",
    dataFormat: "json",
    dataSource
  }).render();
});*/


  });



//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


 </script>
