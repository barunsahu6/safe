<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

 <script type="text/javascript">
        var marker;
        function toShowMap(str, mflag, divID) {
                  
            var mapOptions = {
                center: new google.maps.LatLng(23.1667, 79.9333),
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById(divID), mapOptions);

            $.ajax({
                type: "POST",
                url: "<?php  echo base_url(); ?>Ajax/DatafromOnWebService_Map",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
               // data: "{flag:" + mflag + " ,  flag2:'"+ str +"',PlantGUID:'"+ PlantGUID +"',UserID:'"+ UserID +"',RoleID:'"+RoleID+"'}",
                success: function (data) {
                    var events = [];
                    $.each(data.d, function (i, d) {
                    
                        var description = d.description;
                        var lat = d.Latitude;
                        var long = d.Longitude;
                        var myLatlng = new google.maps.LatLng(lat, long);
                        var pinColor = d.Markercolor;

                        marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: d.description,
                            // icon: new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor),
                            icon: new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + pinColor),
                        });
                        (function (marker, description) {
                            google.maps.event.addListener(marker, "click", function (e) {
                                infoWindow.setContent(description);
                                infoWindow.open(map, marker);
                            });
                        })(marker, description);
                    });
                }
            });
        }
    </script> 


 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TST Dashboard
        <small> TST Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">TST Dashboard</li>
      </ol>
    </section>
	 
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
	   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Country</span>
              <span class="info-box-text">
			   <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="">--All--</option>
        				   <?php foreach($Country as $row){	?>
        				  <option value="<?php echo $row->CountryID; ?>" ><?php echo $row->CountryName;?></option>
        					<?php } ?>
                 </select>
                </div>
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			         <div class="form-group">
                  <select class="form-control" Name="State" id="State">
        				    <option value="">---All ---</option>
                     <?php foreach($State as $row){ ?>
                    <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } ?>
                  </select>
                </div>		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			         <div class="form-group">
                  <select class="form-control" name="District" id="District">
				            <option value="">---All ---</option>
                   <?php foreach($District as $row){ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } ?>
                  </select>
                </div>			  
			       </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
		 <div class="clearfix visible-sm-block"></div>
        
		 <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            

            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			         <div class="form-group">
                  <select class="form-control" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $row){?>
                    <option value="<?php echo $row->PlantGUID;?>" ><?php echo $row->SWNID;?></option>
                   <?php } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
        <!-- /.col -->
      </div>
      <!-- /.row -->
	  
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Water Quality Contaminant</span>
              <span class="info-box-number">58</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Recommended Technologies</span>
              <span class="info-box-number">58</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">user statistics</span>
              <span class="info-box-number">9</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
<?php  
$Getgaugedata = $this->model->getguage();
$gaugevalue = $Getgaugedata->adpotion;
 ?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container-gage",
        "width": "325",
        "height": "150",
        "dataFormat": "json",
            "dataSource": {
                   "chart": {
                      "caption": "Site With major parameters",
                     // "subcaption": "Last week",
                      "lowerLimit": "0",
                      "upperLimit": "100",
             "bgColor": "#ffffff",
                      "theme": "fint"
                   },
                   "colorRange": {
                      "color": [
                         {
                            "minValue": "0",
                            "maxValue": "<?php echo $gaugevalue;?>",
                            "code": "#efbf60"
                         },
                         {
                            "minValue": "<?php echo $gaugevalue;?>",
                            "maxValue": "100",
                            "code": "#efbf60"
                         }
                      ]
                   },
                   "dials": {
                      "dial": [
                         {
                            "value": "<?php echo $gaugevalue;?>"
                         }
                      ]
                   }
            }
      });

    csatGauge.render();
});
</script>
    
<div class="row">
        <!-- Left col -->
   <section class="col-lg-4 connectedSortable" style="border: 3px solid #ddd;">
      
       <div id="chart-container-gage">FusionCharts will render here</div>
       <div class="row" style="background: #f27357; padding: 8px; color: #ffffff;">
                <div class="col-md-7" >Microbial</div>
                <div class="col-md-5"  style="text-align:right;">35</div>
    </div>
    <div class="row" style="background: #8ed2d1;padding: 8px; color: #ffffff;">
                <div class="col-md-7" >Arsenic</div>
                <div class="col-md-5" style="text-align:right;">17</div>
    </div>
    <div class="row" style="background: #8182bf;padding: 8px; color: #ffffff;">
                <div class="col-md-7">Fluoride</div>
                <div class="col-md-5" style="text-align:right;">12</div>
    </div>
    <div class="row" style="background: #f9ec18;padding: 8px; color: #ffffff;">
        <div class="col-md-7">Iron</div>
        <div class="col-md-5" style="text-align:right;">11</div>
    </div>
     <div class="row" style="background: #8ed2d1;padding: 8px; color: #ffffff;">
        <div class="col-md-7">Nitrate</div>
        <div class="col-md-5" style="text-align:right;">10</div>
    </div>
     <div class="row" style="background: #8182bf;padding: 8px; color: #ffffff;">
        <div class="col-md-7">Iron</div>
        <div class="col-md-5" style="text-align:right;">11</div>
    </div>

</section>

<script type="text/javascript">
    FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Technologies',
        width: '300',
        height: '365',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Technologies",
                "subCaption": "",
                "bgColor": "#ffffff",
                "theme" : "fint"
                
            },

            "data": <?php echo $this->model->DashBoard_WaterTreatmentTechnology();  ?>
        }
    });

    revenueChart.render();
});


</script>
  
<div class="col-lg-4" style="border: 3px solid #ddd; background-color: #cddc39;">
   <div id="chart-container-Technologies"></div>
</div>
	
<?php   
$UserBarChartReport = $this->model->CreateBar2dUserBarChart();
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-Report',
        width: '300',
        height: '365',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisName": "Year",
                //"numberPrefix": "$",
                "paletteColors": "#7080a0",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            
            "data": [
                {
                    "label": "Total",
                    "value": "<?php  echo $UserBarChartReport[0]->Total; ?>"
                }, 
                {
                    "label": "Active",
                    "value": "<?php  echo $UserBarChartReport[1]->Total; ?>"
                }, 
                {
                    "label": "Partially Active",
                    "value": "<?php  echo $UserBarChartReport[2]->Total; ?>"
                }, 
                {
                    "label": "InActive",
                    "value": "<?php  echo $UserBarChartReport[3]->Total; ?>"
                }
            ]
        }
    })
    .render();
});
</script>
  
	 <div class="col-lg-4" style="border: 3px solid #607d8b; background-color: #ffffff;">
             <div id="chart-container-Report"></div>
     </div>
		
	</div>
	        <!-- Middle Chart -->
	
	   
   <div class="row">
        <!-- Left col -->
	<div class="col-lg-6" style="border: 1px solid #607d8b; background-color: #ffffff;">
      
       <h3 class="box-title">Water Quality Contamination Map</h3>
       <div><img src="<?php  print FIMAGES;?>/contmap.jpg" width="500px" height="400px" alt=""></div>
      
        <!-- Safe  -->		
             
		</div>
		  
		  
		  <section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Map</h3>
                <div id="dvMap" style="width: 100%; height: 386px; border: 1px #a3c86d; border-style: none solid solid solid;">
            </div>
            </div>
            
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'column2d',
    renderAt: 'chart-container-Contamination',
    width: '500',
    height: '350',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "",
            "theme": "fint"
        },

        "data": <?php echo $this->model->DashBoard_Contamination(); ?>
    }
}
);
    fusioncharts.render();
});
</script>
	<div class="row">
        <!-- Left col -->
	<div class="col-lg-6" style="border: 1px solid #607d8b; background-color: #ffffff;">
    <h3 class="box-title">Contamination</h3>
    <div id="chart-container-Contamination">FusionCharts XT will load here!</div>
       
	</div>

<script type="text/javascript">
FusionCharts.ready(function () {
    var siteTrafficChart = new FusionCharts({
        type: 'inversemsarea',
        renderAt: 'chart-container',
        width: '500',
        height: '350',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Daily bounce rate",                
                "subCaption": "Last week",
                "xAxisName": "Day",
                "yAxisName": "Percentage",
                "numberSuffix": "%",
                "showBorder": "0",
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",                
                'usePlotGradientColor': "0",
                "plotFillAlpha": "50",
                "showCanvasBorder": "0",                                                                
                "LegendShadow": "0",
                "legendBorderAlpha": "0",                
                "showXAxisLine": "1",                
                "axisLineAlpha": "40",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHgridColor": "0",
                "showValues": "0",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Mon"
                        }, 
                        {
                            "label": "Tue"
                        },
                        {
                            "label": "Wed"
                        },
                        {
                            "label": "Thu"
                        },
                        {
                            "label": "Fri"
                        },
                        {
                            "label": "Sat"
                        },
                        {
                            "label": "Sun"
                        }]
                }],
            "dataset": [
                {
                    "seriesname": "food.hsm.com",                
                    "data": [
                        {
                            "value": "27"
                        },
                        {
                            "value": "22"
                        },
                        {
                            "value": "25"
                        },
                        {
                            "value": "27"
                        },
                        {
                            "value": "21"
                        },
                        {
                            "value": "29"
                        },
                        {
                            "value": "22"
                        }
                    ]
                },
                {
                    "seriesname": "cloth.hsm.com",                
                    "data": [
                        {
                            "value": "42"
                        },
                        {
                            "value": "38"
                        },
                        {
                            "value": "39"
                        },
                        {
                            "value": "36"
                        },
                        {
                            "value": "43"
                        },
                        {
                            "value": "44"
                        },
                        {
                            "value": "35"
                        }
                    ]
                }                               
            ]
        }
         }).render();
});
</script>
<div class="col-lg-6" style="border: 1px solid #607d8b; background-color: #ffffff;">
    <h3 class="box-title">State Wise Impurities</h3>
    <div id="chart-container1">FusionCharts will render here</div>
  </div>
  
		  
	
		  <!-- /.nav-tabs-custom -->
	</div>

    <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){

                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

            $("#State").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+$("#Country").val()+'/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#District").html(data);
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });

            $("#District").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getplants/'+$("#Country").val()+'/'+$("#State").val()+'/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#Plant").html(data);
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });
        });
    </script>

  <script type="text/javascript">
        $(document).ready(function(){
   toShowMap('','','dvMap');
});
    </script>