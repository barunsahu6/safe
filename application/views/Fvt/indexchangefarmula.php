<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

 <style>
   .dkjfd{
        width: 90%;
        height: auto;
        border:3px solid black;
        margin: auto;
    } 
	</style>
<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 450px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
#style-4::-webkit-scrollbar-thumb
{
	background-color: #F5F5F5;
}
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header" style="margin-left: 35px;color: #3c8dbc;">
      <h1>
      <b> Dashboard - FVT</b>
    </h1>
     
    </section>
	 
    <!-- Main content -->
    <section class="content">
      <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->
      <!-- Info boxes -->
      <div class="scrollbar" id="style-3">
	   <div class="row" style="height: 550px; width:100%;">
       <div  style="border:2px solid black;padding:10px; margin-left:20px;" >
        <div class="container" style="width:100%;">
        <div class="row" style="padding: 10px;">

  
            <div class="col-md-4">
            <div class="form-group" style="float: left;">
                <label for="" class="col-sm-6">Investments (&#x20b9;)</label>
                <div class="col-sm-6" style="float: left;">
                <input type="number" class="form-control" name="InvestmentsSearch" id="InvestmentsSearch" value="" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-6" >Access (#)</label>
                <div class="col-sm-6" style="float: right;">
                <input type="number" class="form-control" name="AccessSearch" id="AccessSearch" value="" disabled>
                </div>
            </div>
            </div>

            
            <div class="col-md-4">
            <div class="form-group" style="float: left;">
                <label for="" class="col-sm-6">Revenue (&#x20b9;/Month)</label>
                <div class="col-sm-6"  style="float: left;">
                <input type="number" class="form-control" name="RevenueSearch" id="RevenueSearch" value="" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-6">Adoption (#)</label>
                <div class="col-sm-6" style="float: left;">
                <input type="number" class="form-control" name="AdoptionSearch" id="AdoptionSearch" value="" disabled>
                </div>
            </div>
          </div>

      <div class="col-md-3">
            <div class="form-group" style="float: left;width:">
                <label for="" class="col-sm-6">Total Cost (&#x20b9;/Month)</label>
                <div class="col-sm-6" style="float: left;">
                <input type="number" class="form-control" name="OpExSearch" id="OpExSearch" value="" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-6">Consumption (Ltr/capita/day)</label>
                <div class="col-sm-6" style="float: left;">
                <input type="number" class="form-control" name="ConsumptionSearch" id="ConsumptionSearch" value="" disabled>
                </div>
            </div>
          </div>
          <div class="col-md-1"> 
           <div class="form-group" style="float: left;width: 100%">
               
                <div class="col-sm-12" style="float: left;">
                <button class="btn  btn-primary" data-toggle="modal" data-target="#myModal" style="font-weight:bold; margin-top: 20px;">EDIT</button>
                </div>  
            </div>

            
           </div> 
                 
          </div>
        </div>

       <!-- /.col -->
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-XS">
    
      <div class="modal-content" style="background:#f5596;">
        <div class="modal-header" style="background-color:#015c90;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color:#FFFFFF;">FVT Parameters</h4>
        </div>
        
        <div class="modal-body" style="height:400px;overflow:auto;" >
        <div class="row" style="padding:15px;background:#ecf0f5;border-bottom:2px solid black;">
        	<div class="col-md-4">
        		<h8 style="padding:x;font-weight:bold;">Investments (&#x20b9;)</h8>
        	</div>
        	<div class="col-md-8">
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Entrepreneur (₹)</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Entrepreneur" id="Entrepreneur" value="">
                <input type="hidden" class="form-control" name="Entrepreneur_hid" id="Entrepreneur_hid" value="">
        		</div>
        	</div>
        	<div class="form-group">
        		<label for="" class="col-sm-6">Donor (₹)</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Donor" id="Donor" value="">
                <input type="hidden" class="form-control" name="Donor_hid" id="Donor_hid" value="">
        		</div>
        	</div>
        		
        	</div>
        </div>


 <div class="row" style="padding:15px;background:#ecf0f5;border-bottom:2px solid black;">
        	<div class="col-md-4">
        		<h8 style="padding:x;font-weight:bold;">
                Access,<br>Adoption,<br>
        		&Consumption</h8>
        	</div>
        	<div class="col-md-8">
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Population</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Population" id="Population" value="">
                <input type="hidden" class="form-control" name="Population_hid" id="Population_hid" value="">
        		</div>
        	</div>
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Adoption (#)</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Adiption" id="Adiption" value="">
                <input type="hidden" class="form-control" name="Adiption_hid" id="Adiption_hid" value="">
        		</div>
        	</div>

        	<div class="form-group">
        		<label for="" class="col-sm-6">Consumption (Ltr/capita/day)</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Consumption_head_day" id="Consumption_head_day" value="">
                <input type="hidden" class="form-control" name="Consumption_head_day_hid" id="Consumption_head_day_hid" value="">
        		</div>
        	</div>
        		<div class="row"></div>
        	</div>
        </div>

 <div class="row" style="padding:15px;background:#ecf0f5;border-bottom:2px solid black;">
        	<div class="col-md-4">
        		<h8 style="padding:x;font-weight:bold;">Revenue (&#x20b9;)</h8>
        	</div>
        	<div class="col-md-8">
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Pricing (₹/can)</label>
        		<div class="col-sm-6">
                <input type="number" class="form-control" name="Water_price" id="Water_price" value="">
                 <input type="hidden" class="form-control" name="Water_price_hid" id="Water_price_hid" value="">
        		</div>
        	</div>
        	</div>
        </div>
         <div class="row" style="padding:15px;background:#ecf0f5;border-bottom:2px solid black;">
        	<div class="col-md-4">
        		<h8 style="padding:x;font-weight:bold;">OpEx-Variable (&#x20b9;)</h8>
        	</div>
        	<div class="col-md-8">
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Electricity</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Electricity" id="Electricity" value="">
                 <input type="hidden" class="form-control" name="Electricity_hid" id="Electricity_hid" value="">
        		</div>
        	</div>
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Chemicals & Consumbles</label>
        		<div class="col-sm-6">
                <input type="hidden" class="form-control" name="Chemicals_Consumbles_hd" id="Chemicals_Consumbles_hid" value="">
        		<input type="number" class="form-control" name="Chemicals_Consumbles" id="Chemicals_Consumbles" value="">
                
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Cost of Delivery</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Cost_of_delviery" id="Cost_of_delviery" value="">
                <input type="hidden" class="form-control" name="Cost_of_delviery_hid" id="Cost_of_delviery_hid" value="">
                
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Generator Fuel</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Generator_fuel" id="Generator_fuel" value="">
                <input type="hidden" class="form-control" name="Generator_fuel_hid" id="Generator_fuel_hid" value="">
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Other Expenses</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Other_Expenses" id="Other_Expenses" value="">
                <input type="hidden" class="form-control" name="Other_Expenses_hid" id="Other_Expenses_hid" value="">
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Entrepreneur ROI</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Entrepreneur_Rol" id="Entrepreneur_Rol" value="">
                <input type="hidden" class="form-control" name="Entrepreneur_Rol_hid" id="Entrepreneur_Rol_hid" value="">
        		</div>
        	</div>
        	</div>
        </div>

        <div class="row" style="padding:15px;background:#ecf0f5;">
        	<div class="col-md-4">
        		<h8 style="padding:x;font-weight:bold;">OpEx-Fixed (&#x20b9;)</h8>
        	</div>
        	<div class="col-md-8">
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Operator Salary</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="0perator_Salary" id="0perator_Salary" value="">
               
        		</div>
        	</div>
        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Land Rent</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Land_Rent" id="Land_Rent" value="">
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Raw Water Source</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Raw_Water_Source" id="Raw_Water_Source" value="">
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Service Fee Paid</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Service_Fee_Paid" id="Service_Fee_Paid" value="">
                <input type="hidden" class="form-control" name="Service_Fee_Paid_hid" id="Service_Fee_Paid_hid" value="">
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Spares</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Spares" id="Spares" value="">
                <input type="hidden" class="form-control" name="Spares_hid" id="Spares_hid" value="">
        		</div>
        	</div>

        	<div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Maintenance Reserve</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Maintenance_Reserve" id="Maintenance_Reserve" value="">
                <input type="hidden" class="form-control" name="Maintenance_Reserve_hid" id="Maintenance_Reserve_hid" value="">
        		</div>
        	</div>

            <div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Bank Loan EMI</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Bank_Loan_EMI" id="Bank_Loan_EMI" value="">
                <input type="hidden" class="form-control" name="Bank_Loan_EMI_hid" id="Bank_Loan_EMI_hid" value="">
        		</div>
        	</div>

            <div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Capital Repayment</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Capital_Repayment" id="Capital_Repayment" value="">
                <input type="hidden" class="form-control" name="Capital_Repayment_hid" id="Capital_Repayment_hid" value="">
        		</div>
        	</div>

            <div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">LNGO Cost</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="LNGO_Cost" id="LNGO_Cost" value="">
                <input type="hidden" class="form-control" name="LNGO_Cost_hid" id="LNGO_Cost_hid" value="">
        		</div>
        	</div>

            <div class="form-group" style="float: left;width: 100%">
        		<label for="" class="col-sm-6">Field Support</label>
        		<div class="col-sm-6">
        		<input type="number" class="form-control" name="Field_Support" id="Field_Support" value="">
                <input type="hidden" class="form-control" name="Field_Support_hid" id="Field_Support_hid" value="">
        		</div>
        	</div>
        	</div>
        </div>
        </div>
        <div class="modal-footer" style="background-color:#015c90;">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           <button type="button" class="btn btn-warning" data-dismiss="modal" data-target="#myModal" id="SubmitGraph" onclick="FVTParameters()">Submit</button>
        
        </div>
      </div>
      
    </div>
  </div>
  
</div> 

<script>
function FillTextboxValue(){
document.getElementById('Entrepreneur').value = 700000;
document.getElementById('Donor').value = 750000;
document.getElementById('Population').value = 5000;
document.getElementById('Operator_Salary').value = 5000;
document.getElementById('Land_Rent').value = 1000;
document.getElementById('Raw_Water_Source').value = 2000;
document.getElementById('Service_Fee_Paid').value = 3000;
document.getElementById('Spares').value = 2000;
document.getElementById('Maintenance_Reserve').value = 5000;
document.getElementById('Bank_Loan_EMI').value = 0;
document.getElementById('Capital_Repayment').value = 6250;
document.getElementById('LNGO_Cost').value = 3000;
document.getElementById('Field_Support').value = 7500;
document.getElementById('Water_price').value = 6;
document.getElementById('Electricity').value = 5000;
document.getElementById('Adiption').value = 2500;
document.getElementById('Consumption_head_day').value = 1.50;
document.getElementById('Chemicals_Consumbles').value = 1200;
document.getElementById('Cost_of_delviery').value = 3000;
document.getElementById('Generator_fuel').value = 2000;
document.getElementById('Other_Expenses').value = 500;
document.getElementById('Entrepreneur_Rol').value = 10000;

}
</script>
<script>
function FVTParameters(){
 // alert('hi');
var Entrepreneurvalue               = parseInt(document.getElementById('Entrepreneur').value);
var Donorvalue                      = parseInt(document.getElementById('Donor').value);
var Populationvalue                 = parseInt(document.getElementById('Population').value);
var Operator_Salaryvalue            = parseInt(document.getElementById('0perator_Salary').value);
var Land_Rentvalue                  = parseInt(document.getElementById('Land_Rent').value);
var Raw_Water_Sourcevalue           = parseInt(document.getElementById('Raw_Water_Source').value);
var Service_Fee_Paidvalue           = parseInt(document.getElementById('Service_Fee_Paid').value);
var Sparesvalue                     = parseInt(document.getElementById('Spares').value);
var MaintenanceReservevalue         = parseInt(document.getElementById('Maintenance_Reserve').value);
var BankLoanEMIvalue                = parseInt(document.getElementById('Bank_Loan_EMI').value);
var CapitalRepaymentvalue           = parseInt(document.getElementById('Capital_Repayment').value);
var LNGOCostvalue                   = parseInt(document.getElementById('LNGO_Cost').value);
var FieldSupportvalue               = parseInt(document.getElementById('Field_Support').value);



var Water_pricevalue            = parseInt(document.getElementById('Water_price').value);
var Electricityvalue            = parseInt(document.getElementById('Electricity').value);
var Adiptionvalue               = parseInt(document.getElementById('Adiption').value);
var Consumption_head_dayvalue   = parseFloat(document.getElementById('Consumption_head_day').value);
var ChemicalsConsumblesvalue    = parseInt(document.getElementById('Chemicals_Consumbles').value);
var Costofdelvieryvalue         = parseInt(document.getElementById('Cost_of_delviery').value);
var Generatorfuelvalue          = parseInt(document.getElementById('Generator_fuel').value);
var OtherExpensesvalue          = parseInt(document.getElementById('Other_Expenses').value);
var EntrepreneurRolvalue        = parseInt(document.getElementById('Entrepreneur_Rol').value);


if (isNaN(Entrepreneurvalue) == true) {
         Entrepreneurvalue = 0;
}
if (isNaN(Donorvalue) == true) {
         Donorvalue = 0;
}

if (isNaN(Operator_Salaryvalue) == true) {
         Operator_Salaryvalue = 0;
}
if (isNaN(Land_Rentvalue) == true) {
         Land_Rentvalue = 0;
}
if (isNaN(Raw_Water_Sourcevalue) == true) {
         Raw_Water_Sourcevalue = 0;
}
if (isNaN(Service_Fee_Paidvalue) == true) {
         Service_Fee_Paidvalue = 0;
}
if (isNaN(Sparesvalue) == true) {
         Sparesvalue = 0;
}
if (isNaN(Electricityvalue) == true) {
         Electricityvalue = 0;
}
if (isNaN(ChemicalsConsumblesvalue) == true) {
         ChemicalsConsumblesvalue = 0;
}
if (isNaN(Costofdelvieryvalue) == true) {
         Costofdelvieryvalue = 0;
}
if (isNaN(Generatorfuelvalue) == true) {
         Generatorfuelvalue = 0;
}
if (isNaN(OtherExpensesvalue) == true) {
         OtherExpensesvalue = 0;
}
if (isNaN(EntrepreneurRolvalue) == true) {
         EntrepreneurRolvalue = 0;
}
if (isNaN(Adiptionvalue) == true) {
         Adiptionvalue = 0;
}
if (isNaN(Consumption_head_dayvalue) == true) {
         Consumption_head_dayvalue = 0;
}


var RevenueHiden = Math.round((Adiptionvalue * Consumption_head_dayvalue * Water_pricevalue * 30)/20);
//alert(RevenueHiden);
var TotalCost = Math.round(Electricityvalue + ChemicalsConsumblesvalue + Costofdelvieryvalue + Generatorfuelvalue + OtherExpensesvalue + EntrepreneurRolvalue + Operator_Salaryvalue + Land_Rentvalue + Raw_Water_Sourcevalue + Service_Fee_Paidvalue + Sparesvalue + MaintenanceReservevalue + BankLoanEMIvalue + CapitalRepaymentvalue + LNGOCostvalue + FieldSupportvalue);

var varadd1 = Operator_Salaryvalue+Land_Rentvalue+Raw_Water_Sourcevalue+Service_Fee_Paidvalue+Sparesvalue;
var varadd2 = Operator_Salaryvalue+Land_Rentvalue+Raw_Water_Sourcevalue+Service_Fee_Paidvalue+Sparesvalue+MaintenanceReservevalue;
var varadd3 = Operator_Salaryvalue+Land_Rentvalue+Raw_Water_Sourcevalue+Service_Fee_Paidvalue+Sparesvalue+MaintenanceReservevalue+BankLoanEMIvalue+CapitalRepaymentvalue;
var varadd4 = Operator_Salaryvalue+Land_Rentvalue+Raw_Water_Sourcevalue+Service_Fee_Paidvalue+Sparesvalue+MaintenanceReservevalue+BankLoanEMIvalue+CapitalRepaymentvalue+LNGOCostvalue+FieldSupportvalue;


//var varcal = varadd1/30*(Water_pricevalue -(Electricityvalue*20));
//var FixedNumber = 50+100+150+200+250+300+350;

var C14 = parseFloat((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)).toFixed(2);
//alert(C14);
var C15 = parseFloat((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)).toFixed(2);
//alert(C15);
var C16 = parseFloat((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)).toFixed(2);
//alert(C16);
var C17 = parseFloat((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)).toFixed(2);
//alert(C17);
var C18 = parseFloat((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)).toFixed(2);
//alert(C18);
var C19 = parseFloat((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)).toFixed(2);
//alert(C19);
var varSumAllCall = parseFloat(parseFloat(C14) + parseFloat(C15) + parseFloat(C16) + parseFloat(C17) + parseFloat(C18) + parseFloat(C19)).toFixed(2);
//alert(varSumAllCall);


var RC_14_50 = Math.round(50 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_50 = Math.round(50 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_50 = Math.round(50 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_50 = Math.round(50 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_50 = Math.round(50 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_50 = Math.round(50 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall50 = parseInt(parseInt(RC_14_50) + parseInt(RC_15_50) + parseInt(RC_16_50) + parseInt(RC_17_50) + parseInt(RC_18_50) + parseInt(RC_19_50));
//alert(varRevenueSumAllCall50);

var RC_14_100 = Math.round(100 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_100 = Math.round(100 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_100 = Math.round(100 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_100 = Math.round(100 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_100 = Math.round(100 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_100 = Math.round(100 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall100 = parseInt(parseInt(RC_14_100) + parseInt(RC_15_100) + parseInt(RC_16_100) + parseInt(RC_17_100) + parseInt(RC_18_100) + parseInt(RC_19_100));
//alert(varRevenueSumAllCall100);

var RC_14_150 = Math.round(150 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_150 = Math.round(150 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_150 = Math.round(150 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_150 = Math.round(150 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_150 = Math.round(150 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_150 = Math.round(150 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall150 = parseInt(parseInt(RC_14_150) + parseInt(RC_15_150) + parseInt(RC_16_150) + parseInt(RC_17_150) + parseInt(RC_18_150) + parseInt(RC_19_150));
//alert(varRevenueSumAllCall150);

var RC_14_200 = Math.round(200 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_200 = Math.round(200 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_200 = Math.round(200 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_200 = Math.round(200 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_200 = Math.round(200 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_200 = Math.round(200 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall200 = parseInt(parseInt(RC_14_200) + parseInt(RC_15_200) + parseInt(RC_16_200) + parseInt(RC_17_200) + parseInt(RC_18_200) + parseInt(RC_19_200));
//alert(varRevenueSumAllCall200);

var RC_14_250 = Math.round(250 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_250 = Math.round(250 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_250 = Math.round(250 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_250 = Math.round(250 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_250 = Math.round(250 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_250 = Math.round(250 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall250 = parseInt(parseInt(RC_14_250) + parseInt(RC_15_250) + parseInt(RC_16_250) + parseInt(RC_17_250) + parseInt(RC_18_250) + parseInt(RC_19_250));
//alert(varRevenueSumAllCall250);



var RC_14_300 = Math.round(300 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_300 = Math.round(300 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_300 = Math.round(300 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_300 = Math.round(300 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_300 = Math.round(300 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_300 = Math.round(300 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall300 = parseInt(parseInt(RC_14_300) + parseInt(RC_15_300) + parseInt(RC_16_300) + parseInt(RC_17_300) + parseInt(RC_18_300) + parseInt(RC_19_300));
//alert(varRevenueSumAllCall300);



var RC_14_350 = Math.round(350 * 30 * ((Electricityvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_15_350 = Math.round(350 * 30 * ((ChemicalsConsumblesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_16_350 = Math.round(350 * 30 *((Costofdelvieryvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_17_350 = Math.round(350 * 30 *((Generatorfuelvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_18_350 = Math.round(350 * 30 *((OtherExpensesvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));
var RC_19_350 = Math.round(350 * 30 *((EntrepreneurRolvalue * 20)/(30 * Adiptionvalue * Consumption_head_dayvalue)));


var varRevenueSumAllCall350 = parseInt(parseInt(RC_14_350) + parseInt(RC_15_350) + parseInt(RC_16_350) + parseInt(RC_17_350) + parseInt(RC_18_350) + parseInt(RC_19_350));
//alert(varRevenueSumAllCall350);





var D = 30 * (5-parseFloat(varSumAllCall));
 // alert(D);
  var OpEx = Math.round(parseInt(varadd1)/parseFloat(D));
 // alert(OpEx);

var OpExMaintRes = Math.round(parseInt(varadd2)/parseFloat(D));
  //alert(OpExMaintRes);
  var OpExMaintResCapRepayment = Math.round(parseInt(varadd3)/parseFloat(D));
  //alert(OpExMaintResCapRepayment);

   var OpExMaintResCapRepaymentSupportcost = Math.round(parseInt(varadd4)/parseFloat(D));
  //alert(OpExMaintResCapRepaymentSupportcost);


    var Revenue50 = parseInt(parseInt(Water_pricevalue)* 50 * 30);
   // alert(Revenue50);
    var TotalCostStationLevel50 = parseInt(varadd1) + parseInt(varRevenueSumAllCall50);
   // alert(TotalCostStationLevel50);
    var TotalCostinclMaintRes50 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall50);
  // alert(TotalCostinclMaintRes50);

    var TotalCostinclmaintreserve_LNGOandfieldcost50 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall50);
   // alert(TotalCostinclmaintreserve_LNGOandfieldcost50);



    var Revenue100 = parseInt(parseInt(Water_pricevalue)* 100 * 30);
   //alert(Revenue100);
    var TotalCostStationLevel100 = parseInt(varadd1) + parseInt(varRevenueSumAllCall100);
    //alert(TotalCostStationLevel100);
    var TotalCostinclMaintRes100 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall100);
    //alert(TotalCostinclMaintRes100);

    var TotalCostinclmaintreserve_LNGOandfieldcost100 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall100);
   // alert(TotalCostinclmaintreserve_LNGOandfieldcost100);


    var Revenue150 = parseInt(parseInt(Water_pricevalue)* 150 * 30);
   // alert(Revenue);
    var TotalCostStationLevel150 = parseInt(varadd1) + parseInt(varRevenueSumAllCall150);
   // alert(TotalCostStationLevel);
    var TotalCostinclMaintRes150 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall150);
   // alert(TotalCostinclMaintRes);

    var TotalCostinclmaintreserve_LNGOandfieldcost150 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall150);
    //alert(TotalCostinclmaintreserve_LNGOandfieldcost);


    var Revenue200 = parseInt(parseInt(Water_pricevalue)* 200 * 30);
   // alert(Revenue);
    var TotalCostStationLevel200 = parseInt(varadd1) + parseInt(varRevenueSumAllCall200);
   // alert(TotalCostStationLevel);
    var TotalCostinclMaintRes200 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall200);
   // alert(TotalCostinclMaintRes);

    var TotalCostinclmaintreserve_LNGOandfieldcost200 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall200);
    //alert(TotalCostinclmaintreserve_LNGOandfieldcost);


    var Revenue250 = parseInt(parseInt(Water_pricevalue)* 250 * 30);
   // alert(Revenue);
    var TotalCostStationLevel250 = parseInt(varadd1) + parseInt(varRevenueSumAllCall250);
   // alert(TotalCostStationLevel);
    var TotalCostinclMaintRes250 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall250);
   // alert(TotalCostinclMaintRes);

    var TotalCostinclmaintreserve_LNGOandfieldcost250 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall250);
    //alert(TotalCostinclmaintreserve_LNGOandfieldcost);


     var Revenue300 = parseInt(parseInt(Water_pricevalue)* 300 * 30);
   // alert(Revenue);
    var TotalCostStationLevel300 = parseInt(varadd1) + parseInt(varRevenueSumAllCall300);
   // alert(TotalCostStationLevel);
    var TotalCostinclMaintRes300 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall300);
   // alert(TotalCostinclMaintRes);

    var TotalCostinclmaintreserve_LNGOandfieldcost300 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall300);
    //alert(TotalCostinclmaintreserve_LNGOandfieldcost);

     var Revenue350 = parseInt(parseInt(Water_pricevalue)* 350 * 30);
   // alert(Revenue);
    var TotalCostStationLevel350 = parseInt(varadd1) + parseInt(varRevenueSumAllCall350);
   // alert(TotalCostStationLevel);
    var TotalCostinclMaintRes350 = parseInt(MaintenanceReservevalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall350);
   // alert(TotalCostinclMaintRes);

    var TotalCostinclmaintreserve_LNGOandfieldcost350 = parseInt(MaintenanceReservevalue) + parseInt(LNGOCostvalue) + parseInt(FieldSupportvalue) + parseInt(varadd1) + parseInt(varRevenueSumAllCall350);
    //alert(TotalCostinclmaintreserve_LNGOandfieldcost);

     var FixedCost50  = parseInt(varadd1);
     var FixedCost100 = parseInt(varadd1);
     var FixedCost150 = parseInt(varadd1);
     var FixedCost200 = parseInt(varadd1);
     var FixedCost250 = parseInt(varadd1);
     var FixedCost300 = parseInt(varadd1);
     var FixedCost350 = parseInt(varadd1);
    //alert(FixedCost);

    document.getElementById("OpExSearch").value              = TotalCost;
    document.getElementById("RevenueSearch").value           = RevenueHiden //parseInt(Revenue50 + Revenue100 + Revenue150 + Revenue200 + Revenue250 + Revenue300 + Revenue350);
    document.getElementById("AccessSearch").value            = Populationvalue;
    document.getElementById("AdoptionSearch").value          = Adiptionvalue;
    document.getElementById("ConsumptionSearch").value       = Consumption_head_dayvalue;
    document.getElementById("InvestmentsSearch").value       = parseInt(Entrepreneurvalue + Donorvalue);

    var fusioncharts = new FusionCharts({
    type: 'column2d',
    renderAt: 'chart-container-Break-even-Volumes',
    id: 'myChart',
    width: '450',
    height: '350',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Cans/day",
            "toolTipBorderColor": "#FFFFFF",
            "toolTipBgColor": "#666666",
            "toolTipBgAlpha": "80",
             "toolTipBorderColor": "#FFFFFF",
            "toolTipFontColor": "#FFFFFF",
            "showToolTipShadow" : "1",
            "toolTipBgColor": "#ff6a07",
            "theme": "fint"
        },
        "data": [{
            "label": "OpEx",
            "Color": "#e08e0b",
            "value": OpEx
            
        }, {
            "label": "OpEx + Maint. Res.",
            "Color": "#e08e0b",
            "value": OpExMaintRes
        }, {
            "label": "OpEx + Maint. Res. + Cap. Repayment",
            "Color": "#e08e0b",
            "value": OpExMaintResCapRepayment
        }, {
            "label": "OpEx + Maint. Res. + Cap. Repayment + Support cost",
            "Color": "#e08e0b",
            "value": OpExMaintResCapRepaymentSupportcost
        }]
    }
});
  fusioncharts.render();

var visitChart = new FusionCharts({
          type: 'msline',
          renderAt: 'chart-container-revenue',
          width: '450',
          height: '350',
          dataFormat: 'json',
          dataSource: {
              "chart": {
                  "caption": "Revenue v/s Costs (₹ month)",
                      //"subCaption": "Last week vs Current week",
                     // "xAxisName": "Day",
                     "formatNumberScale": "0",
                     "formatNumber" : "0",
                     "paletteColors": "#5b9bd5,#ed7d31,#a8a8a8,#ffc823,#4472c4",
                     "bgAlpha": "0",
                     "borderAlpha": "20",
                     "usePlotGradientColor": "0",
                     "legendBorderAlpha": "0",
                     "labeldisplay": "rotate",
                     "slantlabels": "1",
                     "legendShadow": "0",
                     "captionpadding": "20",
                     "showAxisLines": "1",
                     "axisLineAlpha": "25",
                     "divLineAlpha": "10",
                     "borderAlpha": "80",
                     "showValues": "0",
                     "canvasbgColor": "#FFFFFF",      
                     "toolTipBorderColor": "#FFFFFF",
                     "toolTipFontColor": "#FFFFFF",
                     "showToolTipShadow" : "1",
                     "toolTipBgColor": "#ff6a07",
                     "toolTipBgAlpha": "80",
                     "yFormatNumberScale": "1",
                     "theme": "fint"
                      
              },

                  "categories": [{
                  "category": [{
                      "label": "50"
                  }, {
                      "label": "100"
                  }, {
                      "label": "150"
                  }, {
                      "label": "200"
                  }, {
                      "label": "250"
                  }, {
                      "label": "300"
                  }, {
                      "label": "350"
                  }]
              }],

                  "dataset": [{
                  "seriesname": "Revenue",
                      "data": [{
                      "value": Revenue50
                  }, {
                      "value": Revenue100
                  }, {
                      "value": Revenue150
                  }, {
                      "value": Revenue200
                  }, {
                      "value": Revenue250
                  }, {
                      "value": Revenue300
                  }, {
                      "value": Revenue350
                  }]
              }, {
                  "seriesname": "Total Cost Station Level",
                      "data": [{
                      "value": TotalCostStationLevel50
                  }, {
                      "value": TotalCostStationLevel100
                  }, {
                      "value": TotalCostStationLevel150
                  }, {
                      "value": TotalCostStationLevel200
                  }, {
                      "value": TotalCostStationLevel250
                  }, {
                      "value": TotalCostStationLevel300
                  }, {
                      "value": TotalCostStationLevel350
                  }]
              }, {
                  "seriesname": "Total Cost (incl Maint. Res.)",
                      "data": [{
                      "value": TotalCostinclMaintRes50
                  }, {
                      "value": TotalCostinclMaintRes100
                  }, {
                      "value": TotalCostinclMaintRes150
                  }, {
                      "value": TotalCostinclMaintRes200
                  }, {
                      "value": TotalCostinclMaintRes250
                  }, {
                      "value": TotalCostinclMaintRes300
                  }, {
                      "value": TotalCostinclMaintRes350
                  }]
              }, {
                  "seriesname": "Total Cost (incl Maint. Res.+ LNGO+Field Cost)",
                      "data": [{
                       "value": TotalCostinclmaintreserve_LNGOandfieldcost50
                  }, {
                      "value": TotalCostinclmaintreserve_LNGOandfieldcost100
                  }, {
                      "value": TotalCostinclmaintreserve_LNGOandfieldcost150
                  }, {
                      "value": TotalCostinclmaintreserve_LNGOandfieldcost200
                  }, {
                      "value": TotalCostinclmaintreserve_LNGOandfieldcost250
                  }, {
                      "value": TotalCostinclmaintreserve_LNGOandfieldcost300
                  }, {
                      "value": TotalCostinclmaintreserve_LNGOandfieldcost350
                  }]
              }, {
                  "seriesname": "Fixed Cost",
                      "data": [{
                      "value": FixedCost50
                  }, {
                      "value": FixedCost100
                  }, {
                      "value": FixedCost150
                  }, {
                      "value": FixedCost200
                  }, {
                      "value": FixedCost250
                  }, {
                      "value": FixedCost300
                  }, {
                      "value": FixedCost350
                  }]
              }]
          }
      });

      visitChart.render();
}
</script>   

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
     $("#SubmitGraph").click(function(){
        $("#Showgraph").show();
    });
});
</script>

  <style>
.rotate {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */}
</style>
<!--<div id="Showgraph" style="display:none;">-->

<div class="row" style="margin-top:10px;margin-left:25px;">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 0px;">
   <div style="float:left; width:550px;height:auto;">
   <div style="float:left;width:40px; height:350px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate" style="width: 300px;height: 40px;margin-left: -119px; margin-top:calc(200% - 100%);">
   <b>BREAK EVEN VOLUMES</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
            <div id="chart-container-Break-even-Volumes"></div>
        </div>
    </div>
   </div>
   </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:530px;height:auto;">
   <div style="float:left;width:40px; height:354px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate" style="width: 300px;height: 40px;margin-left: -119px; margin-top:calc(140% - 100%);">
   <b>MONTHLY P/L</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
       <div class="box box-info">
      <div id="chart-container-revenue"></div>
   </div>
   </div>
            <!--</div>-->
            </div>
   </div>
  </div>
</div>         
</div>    