 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Social Sustainability 
      </h1>
      <ol class="breadcrumb">
        <li  Style="font-size:14px;"><a href="<?php echo site_url().'Dashboard/index';?>"><i class="fa fa-dashboard"></i> PAT Dashboard</a></li>
        <li class="active"  Style="font-size:14px;">Social Sustainability </li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-xs-1" style="top:7px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($_POST['Country']==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($_POST['Country']==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
          </div>




       <div class="col-xs-1" style="top:7px;"><b>State:</b></div>
          <?php if($_POST['State'] ==''){ ?>
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
          <?php } else{ ?>
           <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
           <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
          <?php } ?>

        <!-- /.col -->
       <div class="col-xs-1" style="top:7px;"><b>Dirstict:</b></div>
           <?php if($_POST['District'] !=''){ ?>
         <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
          <div class="col-xs-2">
             <div class="form-group">       
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>
           <?php }else{ ?>
            <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
             <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ echo "select";?>

                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  
                </div>		  
           <?php } ?>
       
	    <div class="col-xs-1" style="top:7px;"><b>Plant:</b></div>
          <div class="col-xs-2">
     <?php  if($_POST['Plant'] !=''){
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
     <?php }else{ 
      $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']);
     ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>

     <?php } ?>
          </div>
    </div>   
      
      <!-- /.row -->
	  </form>
            <!-- /.row -->

   <div class="row">
        <!-- Left col -->


<?php   
        if(!empty($this->input->post())){
		 	 $countryid 	= $this->input->post('Country');
		 	 $stateid 	    = $this->input->post('State');
		 	 $districtid    = $this->input->post('District');
		 	 $plantid 	    = $this->input->post('Plant');

		 if(!empty($countryid)){
			 $strwhr = $countryid;
		 }
		  if(!empty($stateid)){
			 $strstatewhr = $stateid;
		 }
		 if(!empty($districtid)){
			 $districtwhr = $districtid;
		 }
		 if(!empty($plantid)){
			 $plantwhr = $plantid;
		 }
	}  
  $PieChartData =  $this->model->CreatePieChart($strwhr,$strstatewhr,$districtid,$plantwhr);
 // print_r($PieChartData); //die;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-pie2d',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "TotalHouseholds",
                "subCaption": "<?php echo $PieChartData->TotalHouseholds ?>",
                "paletteColors": "#328dca,#2caf67",
                "bgColor": "#c4d8e0",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',CreatePieChart
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Uncovered",
                    "value": "<?php echo $PieChartData->perc;?>"
                }, 
                 {
                    "label": "Registered",
                    "value": "<?php echo $PieChartData->perc2;?>"
                }
            ]
        }
    }).render();
});
</script>

	<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title"></h3>
              </div>
           <div id="chart-container-pie2d"></div>
          
          </div>
	</section>

<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-plantandage',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Consumer Participation - Plant age wise",
                "subCaption": "",
                "xAxisname": "",
                "yAxisName": "Participation - By Age",
                "numbersuffix": "%",
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "< 1 Year <br>(<?php echo $getPlantAge[0]->total; ?>)"
                        },
                        {
                            "label":  "1 to 3 Years <br>(<?php echo $getPlantAge[1]->total; ?>)"
                        },
                        {
                            "label": "1 to 5 Years <br>(<?php echo $getPlantAge[2]->total; ?>)"
                        },
                        {
                            "label": "> 5 Years <br>(<?php echo $getPlantAge[3]->total; ?>)"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Uncovered HH",
                    "data": [
                        {
                            "value": "<?php echo $getPlantAge[0]->UncoveredHH;?>"
                        },
                        {
                            "value": "<?php echo $getPlantAge[1]->UncoveredHH;?>"
                        },
                        {
                            "value": "<?php echo $getPlantAge[2]->UncoveredHH;?>"
                        },
                        {
                            "value": "<?php echo $getPlantAge[3]->UncoveredHH;?>"
                        }
                    ]
                },
                {
                    "seriesname": "Registered HH",
                    "data": [
                        {
                            "value": "<?php echo $getPlantAge[0]->RegisteredHH;?>"
                        },
                        {
                            "value": "<?php echo $getPlantAge[1]->RegisteredHH;?>"
                        },
                        {
                            "value": "<?php echo $getPlantAge[2]->RegisteredHH;?>"
                        },
                        {
                            "value": "<?php echo $getPlantAge[3]->RegisteredHH;?>"
                        }
                    ]
                }
            ]
        }
    }).render();    
});

</script>


	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Consumer Participation</h3>
              </div>
            <div id="chart-container-plantandage"></div>
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>

<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-RuralAfford',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Affordability - Rural",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Rural",
                "numberPrefix": "",
                "paletteColors": "#0075c2",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            }, 
       
            "data": [
                {
                    "label": "<=0.1",
                    "value": "<?php echo $GetRuralAffordability[0]->VALUE;?>",
                    "Color": "#599DDC",
                    "seriesname": "<=0.1"
                }, 
                {
                    "label": ">0.1 and <=0.25",
                    "value": "<?php echo $GetRuralAffordability[1]->VALUE;?>",
                    "Color": "#ED7320",
                    "seriesname": ">0.1 and <=0.25"
                }, 
                {
                    "label": "> 0.25",
                    "value": "<?php echo $GetRuralAffordability[2]->VALUE;?>",
                    "Color": "#A1A1A1",
                    "seriesname": "> 0.25"
                }
            ]
        }
    }).render();
});
</script>

<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title" style="margin-left:10px;">  Affordability - Rural</h3>
              </div>
           <div id="chart-container-RuralAfford"></div>
          
          </div>
	</section>

<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-UrbanAfford',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Affordability - Urban",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Urban",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            }, 
        
            "data": [
                {
                    "label": "<=0.1",
                    "value": "<?php echo $GetUrnabAffordability[0]->VALUE;?>",
                    "Color": "#599DDC",
                    "seriesname": "<=0.1"
                }, 
                {
                    "label": ">0.1 and <=0.25",
                    "value": "<?php echo $GetUrnabAffordability[1]->VALUE;?>",
                    "Color": "#ED7320",
                    "seriesname": ">0.1 and <=0.25"
                }, 
                {
                    "label": "> 0.25",
                    "value": "<?php echo $GetUrnabAffordability[2]->VALUE;?>",
                    "Color": "#A1A1A1",
                    "seriesname": "> 0.25"
                }
            ]
        }
    }).render();
});
</script>

   	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Affordability - Urban</h3>
              </div>
            <div id="chart-container-UrbanAfford"></div>
          
          </div>
		  </section>


</div>


	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexSocial').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexSocial').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
           
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexSocial').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }

    </script>

   

   
   