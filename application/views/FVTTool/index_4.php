<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">

  <!-- Content Header (Page header) -->
  <?php $data = $this->model->aggregatorploutput(); ?>
  <section class="inner-wrapper-contetent">
    <div class="common-box">

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Role</label>
            <select name="" id="" class="form-control">
              <option value="">Breakeven Master Input</option>
              <option value="">Breakeven Output Chart</option>
              <option value="">Breakeven Graph</option>
              <option value="">Per Can Analysis</option>
            </select>
          </div>
        </div>

        <div class="col-md-2">
          <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/breakeven_analysis"); ?>">Go</a></button>
        </div>

        <div class="col-md-12">
          <ul class="tab-analysis">
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
            </li>
          </ul>
        </div>
      </div>
    </div>


    <div class="common-box">
      <div class="col-ms-12">
        <div class="row">
          <div class="col-md-6" style="width:auto;background-color:#12AC38  ;">
            <div class="form-group" style="margin-top: 5px;">
              <button   style="text-align: center;font-weight:bold; width:180px;border:none;" onclick="read(this);" value="Community Profile" id="btn1">Community Profile</button>
              <!-- <input type="text" value="Community Profile" background-color:#50AD66 ;"> -->
            </div>
            <div class="form-group">
              <button style="text-align: center;font-weight:bold; width:180px;border:none;" value="Pricing" onclick="read(this);" id="btn2">Pricing</button>

            </div>
            <div class="form-group">
              <button style="text-align: center;font-weight:bold; width:180px;border:none;" value="Variable Operating Cost" onclick="read(this);" id="btn3">Variable Operating Cost</button>

            </div>
            <div class="form-group">
              <button style="text-align: center;font-weight:bold; width:180px;border:none;" value="Fixed Operating cost" onclick="read(this);" id="btn4">Fixed Operating cost</button>

            </div>

          </div>
          <div class="col-md-6" id="profile">
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Population
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,000</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide" onclick="read(this)"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->Population; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Household Size
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0 </span>
                      <span>10,00,000</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->Household_Size; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Registered Household
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>1,00,000</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->RegHousehold; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Consumption (L/HH/Day)
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0
                      </span>
                      <span>20.00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="20" value="<?php echo $detail[0]->ConsumptionPerHouseHoldPerDay; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>


          </div>

          <div class="col-md-6" id="pricing">
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Per 20 l Can
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>2</span>
                      <span>20</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide" ></span>
                      <input class="range" type="range" min="0" max="20" value="<?php echo $detail[0]->TreatedWater; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Per 1l
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider"> 
                  <?php echo $detail[0]->BreakEven_CostPerLitre; ?>

                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6" id="variable">
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Electricity Bil
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->ElectricityBill ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>



            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Chemicals
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->ChemicalsAndConsumables ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>


            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Cost of Delivery

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->CostOfDelivery ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>


            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Generator Fuel

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->GeneratorFule ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Others

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->OtherOperatorExp ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Entrepreneur RoI (as % of revenue)

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider"> 
                  <?php echo $detail[0]->EnterpreneurROI; ?>

                  </div>
                  
                </div>
              </div>
            </div>




          </div>

          <div class="col-md-6" id="fixed">
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Operator Salary

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->OperatorSalary ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>



            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Land Rent
                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->LandRent ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>


            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Row Water Source Rent

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->RawWaterrSourceRent ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>


            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Service Fee Paid

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->ServiceFeePaid ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>
            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Spares

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->Spares ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Maintenance Reserve

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $data[0]->MaintenanceReserve ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Bank Loan EMI

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->BankLoanEMI ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Funded By Donor

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->FundedbyDonor ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>

            <div class="row list-row">
              <div class="col-md-5">
                <h5 class="name-range-label">Repayment to Donor (Years)

                </h5>
              </div>
              <div class="col-md-7">
                <div class="rsilder-wro">
                  <div class="rSlider">
                    <div class="slide-min-max">
                      <span>0</span>
                      <span>10,00,00</span>
                    </div>
                    <div class="slide-rge">
                      <span class="slide"></span>
                      <input class="range" type="range" min="0" max="100000" value="<?php echo $detail[0]->RepaymentToDonor ; ?>">
                    </div>
                  </div>
                  <span class="value-range"></span>
                </div>
              </div>
            </div>





          </div>




        </div>
      </div>
    </div>
</div>


</div>
</div>


</div>
</section>
</div>
<script>
  $(window).on("load", function() {
    $('.range').each(function(index) {
      var range = $(this).attr("value");
      var index = $('.range').index(this);
      
      if( index == 4)
      {
        $(".value-range").eq(index).html(range);
        var per = $(this).val() * 100 / 20;
        $(".slide").eq(index).css("width", per + "%");
      }
      else if( index == 3)
      {
        $(".value-range").eq(index).html(range);
        var per = $(this).val() * 100 / 20;
        $(".slide").eq(index).css("width", per + "%");
      }
     
      else
      {
        $(".value-range").eq(index).html(range);
      var per = $(this).val() * 100 / 100000;

      $(".slide").eq(index).css("width", per + "%");
      }
      


    });

    $(document).on('input change', '.range', function() {
      var index = $('.range').index(this);
      // alert(index);
      if(index == 4)
      {
        $('.value-range').eq(index).html($(this).val());
        var slideWidth = $(this).val() * 100 / 20;
        $(".slide").eq(index).css("width", slideWidth + "%");
      }
      if(index == 3)
      {
        $('.value-range').eq(index).html($(this).val());
        var slideWidth = $(this).val() * 100 / 20;
        $(".slide").eq(index).css("width", slideWidth + "%");
      }
      else
      {
        $('.value-range').eq(index).html($(this).val());
        var slideWidth = $(this).val() * 100 / 100000;
        $(".slide").eq(index).css("width", slideWidth + "%");
      }
      
    });
  });

  $(document).ready(function() {

    $('#profile').show();
    $('#pricing').hide();
    $('#variable').hide();
    $('#fixed').hide();
    $("#btn1").css("background-color", "#50AD66");

  });

  function read(elem) {
    var status = $(elem).val();
    if (status == 'Community Profile') {
      $('#profile').show();
      $('#pricing').hide();
      $('#variable').hide();
      $('#fixed').hide();
      $("#btn1").css("background-color", "#50AD66");
      $("#btn2").css("background-color", "");
      $("#btn3").css("background-color", "");
      $("#btn4").css("background-color", "");
    } else if (status == 'Pricing') {
      $('#profile').hide();
      $('#pricing').show();
      $('#variable').hide();
      $('#fixed').hide();
      $("#btn2").css("background-color", "#50AD66");
      $("#btn1").css("background-color", "");
      $("#btn3").css("background-color", "");
      $("#btn4").css("background-color", "");
    } else if (status == 'Variable Operating Cost') {
      $('#variable').show();
      $('#profile').hide();
      $('#pricing').hide();
      $('#fixed').hide();
      $("#btn3").css("background-color", "#50AD66");
      $("#btn1").css("background-color", "");
      $("#btn2").css("background-color", "");
      $("#btn4").css("background-color", "");
    } else if (status == 'Fixed Operating cost') {
      $('#variable').hide();
      $('#profile').hide();
      $('#pricing').hide();
      $('#fixed').show();
      $("#btn4").css("background-color", "#50AD66");
      $("#btn1").css("background-color", "");
      $("#btn3").css("background-color", "");
      $("#btn2").css("background-color", "");
    }


  }
</script>