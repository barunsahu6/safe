<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">
  
  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
        <div class="common-box">

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Role</label>
                <select name="" id="" class="form-control">
                  <option value="">Aggregator</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Tool
                </label>
                <select name="" id="" class="form-control">
                  <option value="">Advanced</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_4"); ?>">Go</a></button>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
        </div>

        <div class="common-box">
          <div class="row">
            <div class="col-md-6">
              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Entrepreneur Investment
                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>1,00,000</span>
                        <span>10,00,000</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000" value="<?php echo $detail[0]->FundedbyEnterpreCommunity; ?>">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>



              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Donor Investment

                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>1,00,000 </span>
                        <span>10,00,000</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000" value="<?php echo $detail[0]->FundedbyDonor; ?>">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>


              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Population

                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>100</span>
                        <span>1,00,000</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000" value="<?php echo $detail[0]->Population; ?>">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>


              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Price per 20L Water


                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>2.00
                        </span>
                        <span>20.00</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000" value="<?php echo $detail[0]->TreatedWater; ?>">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>
              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Monthly Variable Cost
                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>2.00
                        </span>
                        <span>20.00</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000" value="<?php echo $detail[0]->ElectricityBill + $detail[0]->GeneratorFule + $detail[0]->CostOfDelivery + $detail[0]->ChemicalsAndConsumables + $detail[0]->OperatorIncentive + $detail[0]->OperatorIncentive + $detail[0]->OtherOperatorExp; ?>">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>
              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Monthly Fixed Cost

                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>2.00
                        </span>
                        <span>20.00</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000" value="<?php echo $detail[0]->OperatorSalary + $detail[0]->LandRent + $detail[0]->RawWaterrSourceRent + $detail[0]->ServiceFeePaid + $detail[0]->Spares ; ?>">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>

            </div>
          </div>
           



 <div id="chart-container"></div>
        </div>
        </div>
  </div>
  </div>


  </div>
  </section>
</div>
<script>
   $(window).on("load", function() {
    $('.range').each(function(index) {
      var range = $(this).attr("value");
      var index = $('.range').index(this);
      
      
        $(".value-range").eq(index).html(range);
      var per = $(this).val() * 100 / 50000;

      $(".slide").eq(index).css("width", per + "%");
      
      


    });

    $(document).on('input change', '.range', function() {
      var index = $('.range').index(this);
      // alert(index);
    
        $('.value-range').eq(index).html($(this).val());
        var slideWidth = $(this).val() * 100 / 50000;
        $(".slide").eq(index).css("width", slideWidth + "%");
      
      
    });
  });



$(document).ready(function(){
  
  FusionCharts.ready(function(){
      var chartObj = new FusionCharts({
    type: 'mscombi2d',
        renderAt: 'chart-container',
            width: '50%',
                height: '390',
                    dataFormat: 'json',
                        dataSource: {
            "chart": {
             "caption": "Operating Profit (per Can)",
                "subCaption": "",
                "xAxisname": "Year",
                "pYAxisName": "Cans Sold",
                "sYAxisName": "Profit %",
                "sYAxisMaxValue": "10",
                "sNumberSuffix": "%",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "showHoverEffect":"1",
                 "valueFontColor": "#000000",
                "valueFontSize": "14",
                "rotateValues": "0",
               "formatNumberScale": "1",
                "divlineColor": "#ffffff",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "showToolTip":"0",
               "decimals": "0",
                "baseFontSize": "14"
        },
        "categories": [{
            "category": [{
                "label": "1"
            },
            {
                "label": "2"
            },
            {
                "label": "3"
            },
            {
                "label": "4"
            },
            {
                "label": "5"
            },
            {
                "label": "6"
            },
            {
                "label": "7"
            },
            {
                "label": "8"
            },
            {
                "label": "9"
            },
            {
                "label": "10"
            }
            ]
        }],
            "dataset": [{
                "seriesName": "Cans per day",
                
                "data": [{
                    "value": "16000"
                },
                {
                    "value": "20000"
                },
                {
                    "value": "18000"
                },
                {
                    "value": "19000"
                },
                {
                    "value": "15000"
                },
                {
                    "value": "21000"
                },
                {
                    "value": "16000"
                },
                {
                    "value": "20000"
                },
                {
                    "value": "17000"
                },
                {
                    "value": "25000"
                }
                ]
            },
            {
                 "seriesname": "Operating Profit/Can",
                    "renderAs": "line",
                    "parentYAxis": "S",
                    "showValues": "1",
                "data": [{
                    "value": "15000"
                },
                {
                    "value": "16000"
                },
                {
                    "value": "17000"
                },
                {
                    "value": "18000"
                },
                {
                    "value": "19000"
                },
                {
                    "value": "19000"
                },
                {
                    "value": "19000"
                },
                {
                    "value": "19000"
                },
                {
                    "value": "20000"
                },
                {
                    "value": "21000"
                }
                ]
            },
            {
                 "seriesname": "3 per.Mov.Avg",
            "parentYAxis": "S",

            "renderAs": "Line",
                "data": [{
                    "value": "4000"
                },
                {
                    "value": "5000"
                },
                {
                    "value": "3000"
                },
                {
                    "value": "4000"
                },
                {
                    "value": "1000"
                },
                {
                    "value": "7000"
                },
                {
                    "value": "1000"
                },
                {
                    "value": "4000"
                },
                {
                    "value": "1000"
                },
                {
                    "value": "8000"
                }
                ]
            }
            ]
    }
});
      chartObj.render();
    });

});

  </script>