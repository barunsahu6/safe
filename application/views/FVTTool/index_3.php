<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">
  
  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
        <div class="common-box">

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Role</label>
                <select name="" id="" class="form-control">
                  <option value="">Aggregator</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Tool
                </label>
                <select name="" id="" class="form-control">
                  <option value="">Advanced</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_4"); ?>">Go</a></button>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
        </div>

        <div class="common-box">
          <div class="row">
            <div class="col-md-6">
              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Entrepreneur Investment
                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>1,00,000</span>
                        <span>10,00,000</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>



              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Donor Investment

                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>1,00,000 </span>
                        <span>10,00,000</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>


              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Population

                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>100</span>
                        <span>1,00,000</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>


              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Price per 20L Water


                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>2.00
                        </span>
                        <span>20.00</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>
              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Monthly Variable Cost
                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>2.00
                        </span>
                        <span>20.00</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>
              <div class="row list-row">
                <div class="col-md-5">
                  <h5 class="name-range-label">Monthly Fixed Cost

                  </h5>
                </div>
                <div class="col-md-7">
                  <div class="rsilder-wro">
                    <div class="rSlider">
                      <div class="slide-min-max">
                        <span>2.00
                        </span>
                        <span>20.00</span>
                      </div>
                      <div class="slide-rge">
                        <span class="slide"></span>
                        <input class="range" type="range" min="0" max="50000">
                      </div>
                    </div>
                    <span class="value-range"></span>
                  </div>
                </div>
              </div>

            </div>
          </div>
           








        </div>
        </div>


  </div>
  </div>


  </div>
  </section>
</div>
<script>
    $(window).on("load", function () {
      var range = $(".range").attr("value");
      $(".value-range").html(range);
      $(".slide").css("width", "50%");
      $(document).on('input change', '.range', function () {
        $('.value-range').html($(this).val());
        var slideWidth = $(this).val() * 100 / 50000;

        $(".slide").css("width", slideWidth + "%");
      });
    });
  </script>