<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">

    <!-- Content Header (Page header) -->

    <section class="inner-wrapper-contetent">
        <div class="common-box">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Role</label>
                        <select name="" id="" class="form-control">
                            <option value="">Breakeven Master Input</option>
                            <option value="">Breakeven Output Chart</option>
                            <option value="">Breakeven Graph</option>
                            <option value="">Per Can Analysis</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_11"); ?>">Go</a></button>
                </div>

                <div class="col-md-12">
                    <ul class="tab-analysis">
                        <li>
                            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
                        </li>
                        <li>
                            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                                </span></a>
                        </li>
                        <li>
                            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
                        </li>
                        <li>
                            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                                </span></a>
                        </li>
                        <li>
                            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="row">

          <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
        </div> -->
        <?php
        //  op exp
        $op_month = (($detail[0]->OperatorSalary + $detail[0]->LandRent + $detail[0]->RawWaterrSourceRent + $detail[0]->ServiceFeePaid + $detail[0]->Spares + $detail[0]->EllecticityBill + $detail[0]->GenratorFule + $detail[0]->CostOfDelivery + $detail[0]->ChemicalsAndConsumables + $detail[0]->OtherOperatorExp + $detail[0]->OperatorIncentive) - $detail[0]->ServiceFeePaid - $detail[0]->Spares);
        $op_per = ($op_month / $output[0]->Cans_perDay) / 30;

        // FSE Fee
        $fse_fee = $detail[0]->ServiceFeePaid + $detail[0]->Spares;
        $fse_per = ($fse_fee / $output[0]->Cans_perDay) / 30;

        // Entrepreneur RoI
        $entr_roi = $detail[0]->TreatedWater * $output[0]->Cans_perDay * $detail[0]->EnterpreneurROI * 30;
        $entr_per = ($detail[0]->EnterpreneurROI / $output[0]->Cans_perDay) / 30;
        // Management Firm Cost
        $mng_per = ($detail[0]->LocalNGO_month / $output[0]->Cans_perDay) / 30;
        // asset
        $asset = $detail[0]->TreatedWater - ($op_per + ((($detail[0]->ServiceFeePaid + $detail[0]->Spares) / $output[0]->Cans_perDay) / 30)) + (($detail[0]->EnterpreneurROI / $output[0]->Cans_perDay) / 30) + (($detail[0]->LocalNGO_month / $output[0]->Cans_perDay) / 30);
        $asset_ren = $asset * $output[0]->Cans_perDay * 30;

        ?>
        <div class="common-box">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped" style="margin-left: 250px;">
                        <thead>
                            <tr>
                                <th>
                                    Cans Sold Per Day
                                </th>

                                <th>140</th>
                                <th><a data-toggle="modal" href="#model_5" data-target="#model_5"><i class="fa fa-pencil"></i></a></th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-bordered table-striped" style="margin-left: 250px;">
                        <thead>
                            <tr>
                                <th>
                                    Breakdown of Revenue
                                </th>
                                <th>
                                    Per Month Cost ($)
                                </th>
                                <th>
                                    Per Can Cost ($)
                                </th>
                                <th>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Operating Expenses</td>
                                <td><?php echo $op_month; ?></td>
                                <td><?php echo round($op_per, 2); ?></td>
                                <td><a data-toggle="modal" href="#model_1" data-target="#model_1"><i class="fa fa-pencil"></i></a></td>

                            </tr>
                            <tr>
                                <td>FSE Fee</td>
                                <td><?php echo $fse_fee; ?></td>
                                <td><?php echo round($fse_per, 2); ?></td>
                                <td><a data-toggle="modal" href="#model_2" data-target="#model_2"><i class="fa fa-pencil"></i></a></td>


                            </tr>
                            <tr>
                                <td>Entrepreneur RoI</td>
                                <td><?php echo $entr_roi; ?></td>
                                <td><?php echo round($entr_per, 2); ?></td>
                                <td><a data-toggle="modal" href="#model_3" data-target="#model_3"><i class="fa fa-pencil"></i></a></td>

                            </tr>
                            <tr>
                                <td>Management Firm Cost</td>
                                <td><?php echo $detail[0]->LocalNGO_month; ?></td>
                                <td><?php echo round($mng_per, 2) ?></td>
                                <td></td>

                            </tr>

                            <tr>
                                <td>Asset Renewal Fund</td>
                                <td><?php echo $asset_ren; ?></td>
                                <td><?php echo round($asset,2); ?></td>
                                <td></td>

                            </tr>
                            <tr style="font-weight:bold;">
                                <td>TOTAL</td>
                                <td><?php echo $op_month + $fse_fee + $entr_roi + $detail[0]->LocalNGO_month + $asset_ren; ?></td>
                                <td><?php echo round($op_per + $fse_per + $entr_per + $mng_per + $asset, 2); ?></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- model_1 -->
        <div class="modal fade" id="model_1" tabindex="-1" role="dialog">
            <div class="modal-dialog mw-100 w-75" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <table class="table  table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="2" style="text-align:center;">
                                    Operating Expenses (per month)
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        $attributes = array(
                                'id' => 'op_exp',
                                'name' => 'op_exp',
                                'autocomplete' => 'off',
                            );
                            echo form_open('', $attributes); ?>
                            <tr>
                                <th colspan="2">
                                    FIXED
                                </th>
                            </tr>
                            <tr>
                                <td>Operating Salary</td>
                                <td><input type="text" name="OperatorSalary" value="<?php echo $detail[0]->OperatorSalary; ?> "></td>
                            </tr>
                            <tr>
                                <td>Land Rent</td>
                                <td><input type="text" name="LandRent" value="<?php echo $detail[0]->LandRent; ?>"></td>
                            </tr>
                            <tr>
                                <td>Raw Water Source Rent</td>
                                <td><input type="text" name="RawWaterrSourceRent" value="<?php echo $detail[0]->RawWaterrSourceRent; ?>"></td>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    VARIABLE
                                </th>
                            </tr>
                            <tr>
                                <td>Electercity Bill</td>
                                <td><input type="text" name="ElectricityBill" value="<?php echo $detail[0]->ElectricityBill; ?>"></td>
                            </tr>
                            <tr>
                                <td>Generator Fuel</td>
                                <td><input type="text" name="GeneratorFule" value="<?php echo $detail[0]->GeneratorFule; ?>"></td>
                            </tr>
                            <tr>
                                <td>Cash of Delivery</td>
                                <td><input type="text" name="CostOfDelivery" value="<?php echo $detail[0]->CostOfDelivery; ?>"></td>
                            </tr>
                            <tr>
                                <td>Chemicals</td>
                                <td><input type="text" name="ChemicalsAndConsumables" value="<?php echo $detail[0]->ChemicalsAndConsumables; ?>"></td>
                            </tr>
                            <tr>
                                <td>Other Expenses</td>
                                <td><input type="text" name="OtherOperatorExp" value="<?php echo $detail[0]->OtherOperatorExp; ?>"></td>
                            </tr>
                            <tr>
                                <td>Operator Incentive</td>
                                <td><input type="text" name="OperatorIncentive" value="<?php echo $detail[0]->OperatorIncentive; ?>"></td>
                            </tr>
                            <tr>
                                <th colspan="2" style="text-align: center;">
                                    <button style="background-color:#33C3F1 " id="op_exp_save">OK</button>
                                </th>
                            </tr>
                            <?php echo form_close(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- model_2 -->
        <div class="modal fade" id="model_2" tabindex="-1" role="dialog">
            <div class="modal-dialog mw-100 w-75" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <table class="table  table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="2" style="text-align:center;">
                                    FSE Fee
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $attributes = array(
                                'id' => 'FSE',
                                'name' => 'FSE',
                                'autocomplete' => 'off',
                            );
                            echo form_open('', $attributes); ?>
                            <tr>
                                <td>Service Fee Paid</td>
                                <td><input type="text" name="ServiceFeePaid" value="<?php echo $detail[0]->ServiceFeePaid; ?> "></td>
                            </tr>
                            <tr>
                                <td>Spares</td>
                                <td><input type="text" name="Spares" value="<?php echo $detail[0]->Spares; ?> "></td>
                            </tr>
                            <tr>
                                <th colspan="2" style="text-align: center;">
                                    <button style="background-color:#33C3F1 " id="FSE_save">OK</button>
                                </th>
                            </tr>
                            <?php echo form_close(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- model_3 -->
        <div class="modal fade" id="model_3" tabindex="-1" role="dialog">
            <div class="modal-dialog mw-100 w-75" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <table class="table  table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="2" style="text-align:center;">
                                    Entrepreneur RoI
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $attributes = array(
                                'id' => 'Entrepreneur',
                                'name' => 'Entrepreneur',
                                'autocomplete' => 'off',
                            );
                            echo form_open('', $attributes); ?>
                            <tr>
                                <td>Treated Water Price (/20 L)</td>
                                <td><input type="text" name="TreatedWater" value="<?php echo $detail[0]->TreatedWater; ?> "></td>
                            </tr>
                            <tr>
                                <td>Entrepreneur RoI (as % of Revenue)</td>
                                <td><input type="text" name="EnterpreneurROI" value="<?php echo $detail[0]->EnterpreneurROI; ?> "></td>
                            </tr>
                            <tr>
                                <th colspan="2" style="text-align: center;">
                                    <button style="background-color:#33C3F1 " id="Entrepreneur_save">OK</button>
                                </th>
                            </tr>
                            <?php echo form_close(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- model_4 -->
        <div class="modal fade" id="model_4" tabindex="-1" role="dialog">
            <div class="modal-dialog mw-100 w-75" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->

                </div>
            </div>
        </div>
        <!-- model_5 -->
        <div class="modal fade" id="model_5" tabindex="-1" role="dialog">
            <div class="modal-dialog mw-100 w-75" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <!-- Modal Header -->
                    <table class="table  table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="2" style="text-align:center;">
                                    Calculation - Can per day
                                </th>
                                <span id="form-error"></span>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $attributes = array(
                                'id' => 'can_sold',
                                'name' => 'can_sold',
                                'autocomplete' => 'off',
                            );
                            echo form_open('', $attributes); ?>

                            <tr>
                                <td>Household size</td>
                                <td><input type="text" name="Household_Size" value="<?php echo $detail[0]->Household_Size; ?> "></td>
                            </tr>
                            <tr>
                                <td>Regd. Household</td>
                                <td><input type="text" name="RegHousehold" value="<?php echo $detail[0]->RegHousehold; ?> "></td>
                            </tr>
                            <tr>
                                <td>Consumption (Lt/HH/day)</td>
                                <td><input type="text" name="ConsumptionPerHouseHoldPerDay" value="<?php echo $detail[0]->ConsumptionPerHouseHoldPerDay; ?> "></td>
                            </tr>
                            <tr>
                                <td>Can size(Ltr)</td>
                                <td><input type="text" name="CanSize" value="<?php echo $detail[0]->CanSize; ?> "></td>
                            </tr>
                            <tr>
                                <td>Distribution sale(Cans)</td>
                                <td><input type="text" name="DistributionHamlets" value="<?php echo $detail[0]->DistributionHamlets; ?> "></td>
                            </tr>
                            <tr>
                                <td>Plant Capacity (Ltr/hr)</td>
                                <td><input type="text" name="CapacityOfPlant" value="<?php echo $detail[0]->CapacityOfPlant; ?> "></td>
                            </tr>
                            <tr>
                                <td>Operating hours</td>
                                <td><input type="text" name="OperatingHours" value="<?php echo $detail[0]->OperatingHours; ?> "></td>
                            </tr>
                            <tr>
                                <td>Backwash (hrs.)</td>
                                <td><input type="text" name="Backwash" value="<?php echo $detail[0]->Backwash; ?> "></td>
                            </tr>
                            <tr>
                                <th colspan="2" style="text-align: center;">
                                    <button style="background-color:#33C3F1 " id="can_sold_save" name="submit" type="submit">OK</button>
                                </th>
                            </tr>
                            <?php echo form_close(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>


</div>
</div>


</div>
</section>
</div>
<script>
    $(window).on("load", function() {
        var range = $(".range").attr("value");
        $(".value-range").html(range);
        $(".slide").css("width", "50%");
        $(document).on('input change', '.range', function() {
            $('.value-range').html($(this).val());
            var slideWidth = $(this).val() * 100 / 50000;

            $(".slide").css("width", slideWidth + "%");
        });
    });
</script>
<script type="text/javascript">
    $('#can_sold_save').click(function(e) {

        e.preventDefault();
        $("#form-error").html('');


        var formData = new FormData($('#can_sold')[0]);


        $.ajax({
            url: '<?php echo base_url(); ?>FVTTool/update_masterdata/<?php echo count($detail) > 0 ? $detail[0]->GUID : ''; ?>',
            data: {
                <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
            },
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);

                if (data['status'] == 'true') {

                   alert("hello");


                } else {
                    $("#form-error").html(data['message']);
                    // $('#btnSaveIt').prop('disabled',false);
                    return false;
                }
            }
        });



    });


    $('#Entrepreneur_save').click(function(e) {

        e.preventDefault();
        $("#form-error").html('');


        var formData = new FormData($('#Entrepreneur')[0]);


        $.ajax({
            url: '<?php echo base_url(); ?>FVTTool/update_Entrepreneur/<?php echo count($detail) > 0 ? $detail[0]->GUID : ''; ?>',
            data: {
                <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
            },
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);

                if (data['status'] == 'true') {

                    bootbox.alert(data['message']);

                    location.href = '<?php echo base_url(); ?>FVTTool/index_10';


                } else {
                    $("#form-error").html(data['message']);
                    // $('#btnSaveIt').prop('disabled',false);
                    return false;
                }
            }
        });



    });

    $('#FSE_save').click(function(e) {

        e.preventDefault();
        $("#form-error").html('');


        var formData = new FormData($('#FSE')[0]);


        $.ajax({
            url: '<?php echo base_url(); ?>FVTTool/update_FSE/<?php echo count($detail) > 0 ? $detail[0]->GUID : ''; ?>',
            data: {
                <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
            },
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);

                if (data['status'] == 'true') {

                    bootbox.alert(data['message']);

                    location.href = '<?php echo base_url(); ?>FVTTool/index_10';


                } else {
                    $("#form-error").html(data['message']);
                    // $('#btnSaveIt').prop('disabled',false);
                    return false;
                }
            }
        });



    });

    $('#op_exp_save').click(function(e) {

        e.preventDefault();
        $("#form-error").html('');


        var formData = new FormData($('#op_exp')[0]);


        $.ajax({
            url: '<?php echo base_url(); ?>FVTTool/update_op_exp/<?php echo count($detail) > 0 ? $detail[0]->GUID : ''; ?>',
            data: {
                <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
            },
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);

                if (data['status'] == 'true') {

                    bootbox.alert(data['message']);

                    location.href = '<?php echo base_url(); ?>FVTTool/index_10';


                } else {
                    $("#form-error").html(data['message']);
                    // $('#btnSaveIt').prop('disabled',false);
                    return false;
                }
            }
        });



    });
    (function($) {
          $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
              if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
              } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              }
            });
          };
        }(jQuery));
       $("input").inputFilter(function(value) {
           return /^-?\d*[.,]?\d{0,2}$/.test(value) && (value === "" || parseInt(value) <= 999999999999); });
</script>