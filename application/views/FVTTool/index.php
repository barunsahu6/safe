<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">
  
  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
    <div class="container">


      <form action="" class="form-design-1">
        <div class="row">
          <div class="col-md-12">
            <h4>A Financial Viability analysis for SWEs is just a click away; select the Role and the Tool level
            </h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              <label for="">Select the Role </label>
              <div class="input-pic-chk">
                <div class="pic-rd-wrp">
                  <input type="radio">
                  <div class="pic-radio">
                    <img src="<?php echo base_url(); ?>theme/micro/img/Aggregrators.png" alt="">
                    <p> Aggregators</p>
                  </div>
                </div>

                <div class="pic-rd-wrp">
                  <input type="radio">
                  <div class="pic-radio">
                    <img src="<?php echo base_url(); ?>theme/micro/img/Wash_Entreprenuers.png" alt="">
                    <p>Wash Entrepreneurs</p>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-5">
            <label for="">Select the Tool </label>
            <div class="basic-radio-wrp">
              <div class="basic-radio">
                <input type="radio"> Basic Tool
              </div>
              <div class="basic-radio">
                <input type="radio"> Advanced Tool
              </div>
            </div>
          </div>
          <div class="col-md-2">
          <button class="go-btn"><a href="<?php echo base_url("FVTTool/index_1"); ?>">Go</a></button>
            
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-md-4">
          <a class="fvt-box">
            <img src="<?php echo base_url(); ?>theme/micro/img/About_fvt.png" alt="">
            <h5>About FVT </h5>
          </a>
        </div>


        <div class="col-md-4">
          <a class="fvt-box">
            <img src="<?php echo base_url(); ?>theme/micro/img/About_technology.png" alt="">
            <h5>About Technology </h5>
          </a>
        </div>


        <div class="col-md-4">
          <a class="fvt-box">
            <img src="<?php echo base_url(); ?>theme/micro/img/user_guide.png" alt="">
            <h5>User Guide </h5>
          </a>
        </div>
      </div>
    </div>
  </section>
</div>