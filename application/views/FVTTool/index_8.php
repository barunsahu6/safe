<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">
  
  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
        <div class="common-box">

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Role</label>
                <select name="" id="" class="form-control">
                  <option value="">Breakeven Master Input</option>
                  <option value="">Breakeven Output Chart</option>
                  <option value="">Breakeven Graph</option>
                  <option value="">Per Can Analysis</option>
                </select>
              </div>
            </div>
            
            <div class="col-md-2">
              <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_9"); ?>">Go</a></button>
            </div>

            <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
          </div>
        </div>
        <!-- <div class="row">

          <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
        </div> -->

        <div class="common-box">
          <div class="row">
            <div class="col-md-6">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div style="float:left; width:535px;height:auto;">
          <div style="float:left;width:40px; height:315px; background-color:#08c; color:#fff">
           <div style="float:left;width:100%; height:100%; ">
               <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
                  margin-top: 145px;"><b>DEVOLUTION</b></div>
                </div>
           </div>
           <div style="float:left;width:calc(100% - 85px); height:100%; ">
            <div class="box box-info">
               <div id="chart-container-TrainingRecived"></div>
            </div>
         </div>
         </div>
   </div>
             

              
             

            </div>
          </div>
           
        </div>
        </div>


  </div>
  </div>


  </div>
  </section>
</div>
<script>
    $(window).on("load", function () {
      var range = $(".range").attr("value");
      $(".value-range").html(range);
      $(".slide").css("width", "50%");
      $(document).on('input change', '.range', function () {
        $('.value-range').html($(this).val());
        var slideWidth = $(this).val() * 100 / 50000;

        $(".slide").css("width", slideWidth + "%");
      });
    });
  </script>
  <script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-TrainingRecived',
       width: '450',
        height: '315',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Training Recived ",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
               // "yAxisName": "Training Recived",
                "numberPrefix": "",
                "paletteColors": "#0075c2",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#FFFFFF",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weightInstitutional
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            },

            "data": [
                {
                    "label": "Electicity Safty",
                    "value": "<?php echo $getTrainReciev[0]->Value; ?>",
                    "Color": "#4A8FD0",
                },
                {
                    "label": "Plant Op & Mgnmt.",
                    "value": "<?php echo $getTrainReciev[1]->Value; ?>",
                    "Color": "#F5833C",
                },
                {
                    "label": "Water Qulality",
                    "value": "<?php echo $getTrainReciev[2]->Value; ?>",
                    "Color": "#008080",
                },
                {
                    "label": "Consumer Activation",
                    "value": "<?php echo $getTrainReciev[3]->Value; ?>",
                    "Color": "#FFC501",
                },
                {
                    "label": "Accounts",
                    "value": "<?php echo $getTrainReciev[4]->Value; ?>",
                    "Color": "#4573CB",
                 }
            ]
        }
    }).render();
});
</script>