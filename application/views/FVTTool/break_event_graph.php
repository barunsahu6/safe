<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">


  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
    <div class="common-box">

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Role</label>
            <select name="" id="" class="form-control">
              <option value="">Breakeven Master Input</option>
              <option value="">Breakeven Output Chart</option>
              <option value="">Breakeven Graph</option>
              <option value="">Per Can Analysis</option>
            </select>
          </div>
        </div>

        <div class="col-md-2">
          <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_10"); ?>">Go</a></button>
        </div>

        <div class="col-md-12">
          <ul class="tab-analysis">
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                </span></a>
            </li>
            <li>
              <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
            </li>
          </ul>
        </div>
      </div>
    </div>


    <div class="common-box">
      <div class="col-ms-12">
        <div class="row">
          <div class="col-md-6" style="width:auto;background-color:#12AC38  ;">
            <div class="form-group" style="margin-top: 5px;">
              <button type="button" style="text-align: center;font-weight:bold; width:180px;" onclick="read(this);" value="Community Profile">Community Profile</button>
              <!-- <input type="text" value="Community Profile" background-color:#50AD66 ;"> -->
            </div>
           <div class="form-group">
              <button  style="text-align: center;font-weight:bold; width:180px;" value="Pricing" onclick="read(this);" id="price"><a href="<?php echo base_url("FVTTool/breakeven_analysis"); ?>" >Breakeven Output Chart</a>
</button>

            </div>
            <div class="form-group">
              <button  style="text-align: center;font-weight:bold; width:180px;" value="Variable Operating Cost" onclick="read(this);" id="cost"><a href="<?php echo base_url("FVTTool/break_event_graph"); ?>" >Breakeven Graph</a></button>

            </div>
            <div class="form-group">
              <button style="text-align: center;font-weight:bold; width:180px;" value="Fixed Operating cost" onclick="read(this);">Fixed Operating cost</button>

            </div>

          </div>
          

 
</div>


   <div class="panel panel-default">
  
    <div class="panel-heading">
    <div class="col-md-6"><label>Break even (cans/day)</label></div>
    <label>Cans / day</label>
    </div>
    </div> 
    
    <div class="panel-body">
     
    <div class="col-md-4">
       <input type="checkbox" id="Electricity_bill" name="Electricity_bill" value="Electricity bill"> <b>Only operating cost</b>
   
    </div>
    
      
   <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-2">
    <input type="Number" name="Electricity_can" id="Electricity_can" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div>
    
  </div> 

  <div class="panel-body">
     
    <div class="col-md-4">
       <input type="checkbox" id="Maintenance_reserve" name="Maintenance_reserve" value="Electricity bill"> <b>Operating cost +Maintenance reserve</b>
   
    </div>
    
      
   <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-2">
    <input type="Number" name="Maintenance_reserve_num" id="Maintenance_reserve_num" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div>
    
  </div> 

  <div class="panel-body">
     
    <div class="col-md-4">
       <input type="checkbox" id="Maintenance_reserve_emi" name="Maintenance_reserve_emi" value="Maintenance_reserve_emi"> <b>Operating cost +Bank loan EMI/capital repayment</b>
   
    </div>
    
      
   <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-2">
    <input type="Number" name="Maintenance_reserve_emi_num" id="Maintenance_reserve_emi_num" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div>
    
  </div>  

  <div class="panel-body">
     
    <div class="col-md-4">
       <input type="checkbox" id="Maintenance_reserve_emi" name="Maintenance_reserve_emi_lngo" value="Maintenance_reserve_emi_lngo"> <b>Operating cost +Bank loan EMI/capital repayment + LNGO  cost</b>
   
    </div>
    
      
   <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-2">
    <input type="Number" name="Maintenance_reserve_emi_lngo_num" id="Maintenance_reserve_emi_lngo_num" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div>
    
  </div>  

<div class="panel-body">
     
    <div class="col-md-4">
       <input type="checkbox" id="Maintenance_reserve_emi" name="Maintenance_reserve_emi_lngo" value="Maintenance_reserve_emi_lngo"> <b>Operating cost +Bank loan EMI/capital repayment + LNGO  cost +Field support</b>
   
    </div>
    
      
   <div class="col-md-2">
    <label></label>
    </div>
    <div class="col-md-2">
    <input type="Number" name="Maintenance_reserve_emi_lngo_support" id="Maintenance_reserve_emi_lngo_support" value="<?php  //echo $details->CollectedBy;?>" class="form-control" >
     </div>
    
  </div>       

</div>

 <div class="panel-body">
   
    <div id="chart-container-Regulartory"></div>
    </div>


          </div>




        </div>
      </div>
    </div>
</section></div>

<script type="text/javascript">
   $(function() {
     $("#example1").dataTable();
   });
     

  $(document).ready(function(){
  
FusionCharts.ready(function () {
  
var  Operatorsalary = <?php  echo $details->OperatorSalary; ?>;
var LandRent = <?php  echo $details->LandRent;?>;
var Rawwatersourcerent = <?php  echo $details->RawWaterrSourceRent;?>;
var Servicefeepaid = <?php  echo $details->ServiceFeePaid;?>;
var Spares =  <?php  echo $details->Spares;?>;
var waterCost = <?php  echo $details->TreatedWater;?>;
var Registered_Population = <?php  echo $details->RegHousehold;?>;
var Consumption = <?php  echo $details->ConsumptionPerHouseHoldPerDay;?>;
var can_size = <?php  echo $details->CanSize;?>;
var DistributionHamlets =<?php  echo $details->DistributionHamlets;?>;
var ChemicalsAndConsumables1 = <?php  echo $details->ChemicalsAndConsumables;?>;
var Costofdelivery1 = <?php  echo $details->CostOfDelivery;?>;
var Generatorfuel1 = <?php  echo $details->GeneratorFule;?>;
var OtherOperatorExp1 = <?php  echo $details->OtherOperatorExp;?>;
var ElectricityBill= <?php  echo $details->ElectricityBill;?>;
var EntrepreneurROI = <?php echo $details->EnterpreneurROI ?>;

var Kiosk = parseFloat(Registered_Population) * (parseFloat(Consumption)/parseFloat(can_size));
var electricity = (parseInt(ElectricityBill) / (Kiosk + parseInt(DistributionHamlets))) / 30;
var Chemicalsandconsumables = (parseFloat(ChemicalsAndConsumables1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

var Costofdelivery = ( parseFloat(Costofdelivery1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;
var Generatorfuel = (parseFloat(Generatorfuel1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;

var Otheroperatingexpenses = (parseFloat(OtherOperatorExp1) / (Kiosk + parseFloat(DistributionHamlets))) / 30;


var capitalvalue = <?php  echo $details->Capitalvalueto_bereplaced;?>;
var ElectricityConn= <?php  echo $details->ElectricityConn;?>;
var RMS = <?php  echo $details->RMS;?>;
var AVR = <?php  echo $details->AVR;?>;
var RO = <?php  echo $details->ROPlant;?>;
var Accessories = <?php  echo $details->Other_Accessories;?>;
var Asset_Life = <?php  echo $details->Assetlifeyears;?>;


 var BankLoanEMI = <?php  echo $details->BankLoanEMI;?>;
    var capitalrepayment = <?php   echo (($details->FundedbyDonor / $details->RepaymentToDonor) / 12);?>;
    var FieldSupport = <?php  echo $details->FieldSupport;?>;
    var LNGOCost = <?php echo $details->LocalNGO_month; ?>; 


var Required_MR = ( (parseFloat(capitalvalue)*(parseFloat(RMS))) + (parseFloat(AVR)+parseFloat(ElectricityConn) + parseFloat(RO) + parseFloat(Accessories)) )/100;

var MaintenanceReservecontribution_annum =Required_MR/parseFloat(Asset_Life);

var Maintenancereserve = MaintenanceReservecontribution_annum / 12;



var OperatingCost1 =  ((parseInt(Operatorsalary) + parseInt(LandRent) + parseInt(Rawwatersourcerent) + parseInt(Servicefeepaid) + parseInt(Spares)) / (parseInt(waterCost) - (parseInt(electricity) + parseInt(Chemicalsandconsumables) + parseInt(Costofdelivery) + parseInt(Generatorfuel) + parseInt(Otheroperatingexpenses) + parseInt(EntrepreneurROI)))) / 30;

var OperatingCost2 =  (((Operatorsalary + LandRent + Rawwatersourcerent + Servicefeepaid + Spares + Maintenancereserve) / (waterCost - (electricity + Chemicalsandconsumables + Costofdelivery + Generatorfuel + Otheroperatingexpenses + EntrepreneurROI))) / 30);


var OperatingCost3 =  (((Operatorsalary + LandRent + Rawwatersourcerent + Servicefeepaid + Spares + Maintenancereserve + BankLoanEMI + capitalrepayment) / (waterCost - (electricity + Chemicalsandconsumables + Costofdelivery + Generatorfuel + Otheroperatingexpenses + EntrepreneurROI))) / 30);

var OperatingCost4 =  (((Operatorsalary + LandRent + Rawwatersourcerent + Servicefeepaid + Spares + Maintenancereserve + BankLoanEMI + capitalrepayment + LNGOCost) / (waterCost - (electricity + Chemicalsandconsumables + Costofdelivery + Generatorfuel + Otheroperatingexpenses + EntrepreneurROI))) / 30);

var OperatingCost5 =  (((Operatorsalary + LandRent + Rawwatersourcerent + Servicefeepaid + Spares + Maintenancereserve + BankLoanEMI + capitalrepayment + LNGOCost + FieldSupport) / (waterCost - (electricity + Chemicalsandconsumables + Costofdelivery + Generatorfuel + Otheroperatingexpenses + EntrepreneurROI))) / 30);



$('#Electricity_can').val(OperatingCost1);
$('#Maintenance_reserve_num').val(OperatingCost2);
$('#Maintenance_reserve_emi_num').val(OperatingCost3);
$('#Maintenance_reserve_emi_lngo_num').val(OperatingCost4);
$('#Maintenance_reserve_emi_lngo_support').val(OperatingCost5);

    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Regulartory',
       width: '750',
        height: '415',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Regulartory",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                //"yAxisName": "Regulartory",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                 //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
      
  

            "data": [
                {
                    "label": "Only operating cost",
                    "value": Math.ceil(OperatingCost1),
                    "Color": "#4A8FD0",
                    
                }, 
                {
                    "label": "Operating cost +Maintenance reserve",
                    "value": Math.ceil(OperatingCost2),
                    "Color": "#F5833C",
                    
                }, 
                {
                    "label": "Operating cost +Bank loan EMI/capital repayment",
                    "value": Math.ceil(OperatingCost3),
                    "Color": "#008080",
                   
                }, 
                {
                    "label": "Operating cost +Bank loan EMI/capital repayment + LNGO  cost",
                    "value": Math.ceil(OperatingCost4),
                    "Color": "#FFC501",
                   
                },
                {
                    "label": "Operating cost +Bank loan EMI/capital repayment + LNGO  cost +Field support",
                    "value": Math.ceil(OperatingCost5),
                    "Color": "#008080",
                   
                }
            ]
        }
    }).render();
});


 /*const dataSource = {
  chart: {
    caption: "Treated water price",
    yaxisname: "Cost/Revenue",
    subcaption: "[2005-2016]",
    numbersuffix: " mph",
    rotatelabels: "1",
    setadaptiveymin: "1",
    theme: "fusion"
  },
  data: [
    {
      label: "2005",
      value: "89.45"
    },
    {
      label: "2006",
      value: "89.87"
    }
  ],
  data: [
    {
      label: "2005",
      value: "34.45"
    },
    {
      label: "2006",
      value: "56.87"
    }
  ]
};

FusionCharts.ready(function() {
  var myChart = new FusionCharts({
    type: "line",
    renderAt: "chart-container",
    width: "750",
    height: "450",
    dataFormat: "json",
    dataSource
  }).render();
});*/


  });

 </script>

 <script>
  $(window).on("load", function() {
    $('.range').each(function(index) {
      var range = $(this).attr("value");
      $(".value-range").eq(index).html(range);
      var per = $(this).val() * 100 / 50000;

      $(".slide").eq(index).css("width", per + "%");


    });

    $(document).on('input change', '.range', function() {
      var index = $('.range').index(this);
      
      
      $('.value-range').eq(index).html($(this).val());
      var slideWidth = $(this).val() * 100 / 50000;
      $(".slide").eq(index).css("width", slideWidth + "%");
    });
  });
  $(document).ready(function() {

    $('#profile').show();
    $('#pricing').hide();
    $('#variable').hide();
    $('#fixed').hide();

  });

  function read(elem) {
    var status = $(elem).val();
    if (status == 'Community Profile') {
      $('#profile').show();
      $('#pricing').hide();
      $('#variable').hide();
      $('#fixed').hide();
    } else if (status == 'Pricing') {
      $('#profile').hide();
      $('#pricing').show();
      $('#variable').hide();
      $('#fixed').hide();
    } else if (status == 'Variable Operating Cost') {
      $('#variable').show();
      $('#profile').hide();
      $('#pricing').hide();
      $('#fixed').hide();
    } else if (status == 'Fixed Operating cost') {
      $('#variable').hide();
      $('#profile').hide();
      $('#pricing').hide();
      $('#fixed').show();
    }


  }
</script>