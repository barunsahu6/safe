<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">
  
  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
        <div class="common-box">

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Role</label>
                <select name="" id="" class="form-control">
                  <option value="">Breakeven Master Input</option>
                  <option value="">Breakeven Output Chart</option>
                  <option value="">Breakeven Graph</option>
                  <option value="">Per Can Analysis</option>
                </select>
              </div>
            </div>
            
            <div class="col-md-2">
              <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_6"); ?>">Go</a></button>
            </div>

            <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
          </div>
        </div>
        <!-- <div class="row">

          <div class="col-md-12">
            <ul class="tab-analysis">
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
                  </span></a>
              </li>
              <li>
                <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
              </li>
            </ul>
          </div>
        </div> -->

        <div class="common-box">
            <div class="col-ms-12"> 
              <div class="row">
                <div class="col-md-6">
                  <div class="row list-row">
                    <div class="col-md-5">
                      <h5 class="name-range-label">Per 20 l Can
                      </h5>
                    </div>
                    <div class="col-md-7">
                      <div class="rsilder-wro">
                        <div class="rSlider">
                          <div class="slide-min-max">
                            <span>2</span>
                            <span>20</span>
                          </div>
                          <div class="slide-rge">
                            <span class="slide"></span>
                            <input class="range" type="range" min="0" max="50000">
                          </div>
                        </div>
                        <span class="value-range"></span>
                      </div>
                    </div>
                  </div>



                  <div class="row list-row">
                    <div class="col-md-5">
                      <h5 class="name-range-label">Per 1l
                      </h5>
                    </div>
                    <div class="col-md-7">
                      <div class="rsilder-wro">
                        <div class="rSlider">
                          209
                          
                        </div>
                        <span class="value-range"></span>
                      </div>
                    </div>
                  </div>


                  
                

                </div>
              
              <div class="row">
               <div class="col-md-6" style="width:auto;background-color:#12AC38  ;">
                 <div class="form-group" style="margin-top: 5px;" >
                 <input type="text" value="Community Profile" style="text-align: center;font-weight:bold;">
                 </div>
                 <div class="form-group" >
                 <input type="text" value="Pricing" style="text-align: center;font-weight:bold;background-color:#50AD66 ;">
                 </div>
                 <div class="form-group">
                 <input type="text" value="Variable Operating Cost" style="text-align: center;font-weight:bold;">
                 </div>
                 <div class="form-group">
                 <input type="text" value="Fixed Operating cost" style="text-align: center;font-weight:bold;">
                 </div>
                 
               </div>
              </div>
            </div>   
        </div>
        </div>


  </div>
  </div>


  </div>
  </section>
</div>
<script>
    $(window).on("load", function () {
      var range = $(".range").attr("value");
      $(".value-range").html(range);
      $(".slide").css("width", "50%");
      $(document).on('input change', '.range', function () {
        $('.value-range').html($(this).val());
        var slideWidth = $(this).val() * 100 / 50000;

        $(".slide").css("width", slideWidth + "%");
      });
    });
  </script>