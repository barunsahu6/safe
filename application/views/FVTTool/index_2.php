<script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <script type="text/javascript" src="<?php echo base_url(); ?>/theme/micro/1_files/js"> </script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>html/bootstrap/css/new-style.css">


  <!-- Content Header (Page header) -->

  <section class="inner-wrapper-contetent">
    <div class="common-box">

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Role</label>
            <select name="" id="" class="form-control">
              <option value="">Aggregator</option>
            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Tool
            </label>
            <select name="" id="" class="form-control">
              <option value="">Advanced</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <button class="go-btn no-top"><a href="<?php echo base_url("FVTTool/index_3"); ?>">Go</a></button>
        </div>
      </div>
    </div>
    <div class="row">

      <div class="col-md-12">
        <ul class="tab-analysis">
          <li>
            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/break_even.png" alt=""> <span>Breakeven Analysis </span></a>
          </li>
          <li>
            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/cash_flow.png" alt=""> <span>Cash Flow Analysis
              </span></a>
          </li>
          <li>
            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/sensitivity_analysis.png" alt=""> <span>Sensitivity Analysis </span></a>
          </li>
          <li>
            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/chiller_atm.png" alt=""> <span>Solar/Chiller/ATM Viability Assessment
              </span></a>
          </li>
          <li>
            <a href="#"> <img src="<?php echo base_url(); ?>theme/micro/img/Distribution-Analysis.png" alt=""> <span> Distribution Analysis </span></a>
          </li>
        </ul>
      </div>
    </div>

    <div id="chartContainer" >
    </div>
</div>


</div>
</div>


</div>
</section>
</div>
<script type="text/javascript">
  function CallActivity() {

    JSInterface.ClickActivity();
  }
  // function setlayout(newwidth,newheight){
  // document.getElementById("chartContainer").height=newheight;
  // 	document.getElementById("chartContainer").width=newwidth;
  //window.alert(newwidth);
  // 	}
  function newfun(newwidth, newheight, Cans_sold, op_profit_percan, mov_op_profit) {
    // 	setlayout(newwidth,newheight);

    var cans = [];
    var operating_profit = [];
    var mov_profit = [];
    var newyear = [];

    var cans_sold = Cans_sold.split(",");
    var operate = op_profit_percan.split(",");
    var move = mov_op_profit.split(",");

    for (var i = 0; i < cans_sold.length; i++) {
      var year = i + 1;
      var newcansarray = cans_sold[i];
      var newoperatingarray = operate[i];
      var newmovingarray = move[i];
      var newyearthis = "" + (i + 1);

      newyear.push({
        label: newyearthis
      });

      cans.push({
        label: year,
        value: newcansarray
      });
      operating_profit.push({
        label: year,
        value: newoperatingarray
      });
      mov_profit.push({
        label: year,
        value: newmovingarray
      });


    }
    var data = {
      "chart": {
        "caption": "Operating Profit (per Can)",
        "subCaption": "",
        "xAxisname": "Year",
        "pYAxisName": "Cans Sold",
        "sYAxisName": "Profit %",
        "sYAxisMaxValue": "10",
        "sNumberSuffix": "%",
        "paletteColors": "#0075c2,#1aaf5d,#f2c500",
        "bgColor": "#ffffff",
        "showBorder": "0",
        "showCanvasBorder": "0",
        "usePlotGradientColor": "0",
        "plotBorderAlpha": "10",
        "legendBorderAlpha": "0",
        "legendBgAlpha": "0",
        "legendShadow": "0",
        "showHoverEffect": "1",
        "valueFontColor": "#000000",
        "valueFontSize": "14",
        "rotateValues": "0",
        "formatNumberScale": "1",
        "divlineColor": "#ffffff",
        "divLineIsDashed": "1",
        "divLineDashLen": "1",
        "divLineGapLen": "1",
        "canvasBgColor": "#ffffff",
        "captionFontSize": "14",
        "subcaptionFontSize": "14",
        "showToolTip": "0",
        "decimals": "0",
        "baseFontSize": "14"
      },
      "categories": [{
        "category": newyear
      }],
      "dataset": [{
          "seriesname": "Cans per day",
          "data": cans
        }, ,
        {
          "seriesname": "Operating Profit/Can",
          "renderAs": "line",
          "parentYAxis": "S",
          "showValues": "1",

          "data": operating_profit
        },
        {
          "seriesname": "3 per.Mov.Avg",
          "parentYAxis": "S",

          "renderAs": "Line",
          "data": mov_profit
        }
      ]
    }

    FusionCharts.ready(function() {

      var revenueChart = new FusionCharts({
        type: "mscombidy2d",
        renderAt: "chartContainer",
        width: newwidth,
        height: newheight,
        dataFormat: "json",
        dataSource: data
      });
      revenueChart.render("chartContainer");
    });
  }
  window.onload = CallActivity;
</script>