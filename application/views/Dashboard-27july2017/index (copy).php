 <?php
            echo   $userid       =   $this->session->userdata('USERID');////// Session Userid/////
            echo   $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
            echo   $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////
?>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

 <script type="text/javascript">
        var marker;
        function toShowMap(str, mflag, divID) {
         // var PlantGUID=document.getElementById('hdnPlantGUID').value;
          //var UserID=document.getElementById('hdnSessionUserIDPAT').value;
          //var RoleID=document.getElementById('hdnSessionRoleIDPAT').value;
          
            var mapOptions = {
                center: new google.maps.LatLng(23.1667, 79.9333),
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById(divID), mapOptions);

            $.ajax({
                type: "POST",
                url: "<?php  echo base_url(); ?>Ajax/DatafromOnWebService_Map",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
               // data: "{flag:" + mflag + " ,  flag2:'"+ str +"',PlantGUID:'"+ PlantGUID +"',UserID:'"+ UserID +"',RoleID:'"+RoleID+"'}",
                success: function (data) {
                    var events = [];
                    $.each(data.d, function (i, d) {
                    
                        var description = d.description;
                        var lat = d.Latitude;
                        var long = d.Longitude;
                        var myLatlng = new google.maps.LatLng(lat, long);
                        var pinColor = d.Markercolor;

                        marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: d.description,
                            // icon: new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor),
                            icon: new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + pinColor),
                        });
                        (function (marker, description) {
                            google.maps.event.addListener(marker, "click", function (e) {
                                infoWindow.setContent(description);
                                infoWindow.open(map, marker);
                            });
                        })(marker, description);
                    });
                }
            });
        }
    </script>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'multilevelpie',
    renderAt: 'chart-container-pie',
    id: "myChart",
    width: '450',
    height: '450',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "SOFIE",
            "subcaption": "",
            "showPlotBorder": "2",
            "piefillalpha": "60",
            "pieborderthickness": "2",
            "hoverfillcolor": "#CCCCCC",
            "piebordercolor": "#000000",
            "hoverfillcolor": "#FFFFFF",
            "numberprefix": "$",
            "plottooltext": "$label",
            //Theme
            "theme": "fint"
        },
        "category": [{
            "label": "<?php echo $grandTotal;?>",
            "color": "#fdfa19",
            "value": "<?php echo $grandTotal;?>",
            "category": [{
                "label": "Social ",
                "color": "#ff7857",
                "value": "",
                "tooltext": "Social",
                "category": [{
                    "label": "<?php echo $Social->Coverage;?>",
                    "color": "#ff7857",
					"tooltext": "<?php echo $Social->Coverage;?>",
                    "value": "<?php echo $Social->Coverage;?>",
                }, {
                    "label": "<?php echo $Social->Affordability;?>",
                    "color": "#FFFFFF",
					"tooltext": "<?php echo $Social->Affordability;?>",
                    "value": "<?php echo $Social->Affordability;?>",
                }]
            }, {
                "label": "Operational ",
                "color": "#3669b0",
                "value": "20",
                "tooltext": "Operational",
                "category": [{
                    "label": "<?php echo $totaltab2; ?>",
                    "color": "#3669b0",
                    "value": "<?php echo $totaltab2;?>",
					"tooltext": "<?php echo $totaltab2;?>",
                }, {
                    "label": "<?php echo $remaining;?>",
                    "color": "#FFFFFF",
					"value": "<?php echo $remaining; ?>",
					"tooltext": "<?php echo $remaining; ?>",
                }]
            }, {
                "label": "Institutional",
                "color": "#17a086",
                "value": "20",
                "tooltext": "Institutional",
                "category": [{
                    "label": "<?php echo $totaltab4;?>",
                    "color": "#17a086",
                    "value": "<?php echo $totaltab4;?>",
					 "tooltext": "<?php echo $totaltab4; ?>",
                }, {
                    "label": "<?php echo $remaining4;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $remaining4;?>",
					 "tooltext": "<?php echo $remaining4; ?>",
                }]
            }, {
                "label": "Financial",
                "color": "#cccccc",
                "value": "20",
                "tooltext": "Financial",
                "category": [{
                    "label": "<?php echo $totalfinance; ?>",
                    "color": "#cccccc",
                    "value": "<?php echo $totalfinance; ?>",
                    "tooltext": "<?php echo $totalfinance; ?>",
                },{
                    "label": "<?php echo $remaining3; ?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $remaining3; ?>",
                    "tooltext": "<?php echo $remaining3; ?>",
                }]
            }, {
                "label": "Environmental",
                "color": "#efa232",
                "value": "20",
				 "tooltext": "Environmental",
                "category": [{
                    "label": "<?php echo $totaltab5;?>",
                    "color": "#efa232",
                    "value": "<?php echo $totaltab5;?>"
                }, {
                    "label": "<?php echo $remaining5;?>",
                    "color": "#FFFFFF",
                    "value": "<?php echo $remaining5;?>"
                }]
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>
<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-bar2d',
        width: '200',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Plant and age wise dist...",
                "subCaption": "",
                "yAxisName": " ",
				"xAxisName": "(Year)",
                //"numberPrefix": "$",
                "paletteColors": "#7080a0",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "15",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            
            "data": [
                {
                    "label": "0-3",
                    "value": "880000"
                }, 
                {
                    "label": "3-5 ",
                    "value": "730000"
                }, 
                {
                    "label": "> 5",
                    "value": "10000"
                }
            ]
        }
    })
    .render();
});


</script>
<?php  
$Getgaugedata = $this->model->getguage();
$gaugevalue = $Getgaugedata->adpotion;
 ?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container-gage",
        "width": "210",
        "height": "250",
        "dataFormat": "json",
            "dataSource": {
                   "chart": {
                      "caption": "Adoption in (%)",
                     // "subcaption": "Last week",
                      "lowerLimit": "0",
                      "upperLimit": "100",
					   "bgColor": "#ffffff",
                      "theme": "fint"
                   },
                   "colorRange": {
                      "color": [
                         {
                            "minValue": "0",
                            "maxValue": "<?php echo $gaugevalue;?>",
                            "code": "#efbf60"
                         },
                         {
                            "minValue": "<?php echo $gaugevalue;?>",
                            "maxValue": "100",
                            "code": "#efbf60"
                         }
                      ]
                   },
                   "dials": {
                      "dial": [
                         {
                            "value": "<?php echo $gaugevalue;?>"
                         }
                      ]
                   }
            }
      });

    csatGauge.render();
});
</script>
<?php  
  $PieChartData =  $this->model->CreatePieChart();
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-pie2d',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Split of Visitors by Age Group",
                //"subCaption": "Last year",
                "paletteColors": "#328dca,#2caf67",
                "bgColor": "#c4d8e0",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Walkin",
                    "value": "<?php echo $PieChartData->Walkin;?>"
                }, 
                 {
                    "label": "Distribution",
                    "value": "<?php echo $PieChartData->Distribution;?>"
                }
            ]
        }
    }).render();
});
</script>

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-2d',
        width: '254',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Performance",
                "paletteColors": "#8e0000,#f45b00",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "valueFontColor": "#ffffff",                
               
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Range"
                        }
                      ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "< 60",
                    "data": [
                        {
                            "value": "91"
                        }
                    ]
                },
                {
                    "seriesname": "> 60",
                    "data": [
                        {
                            "value": "75"
                        }
                      ]
                }
            ]
        }
    }).render();    
});
</script>
<?php  
$Getfinandata = $this->model->getfinan($strwhr);
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-financial',
        width: '254',
        height: '400',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Financial",
               // "subCaption": "Harry's SuperMart",
                "xAxisname": "Range",
                //"yAxisName": "Revenue (In USD)",
                //"numberPrefix": "$",
                "paletteColors": "#8e0000,#f45b00,#8bc34a",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Range"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "OpEx",
                    "data": [
                        {
                            "value": "<?php echo $Getfinandata->opex;?>"
                        }
                    ]
                },
                {
                    "seriesname": "OpEx Service Charges",
                    "data": [
                        {
                            "value": "<?php echo $Getfinandata->OPSR;?>"
                        }
                    ]
                },
				{
              "seriesname": "OpEx + Service + Assest Renewal",
                    "data": [
                        {
                            "value": "<?php echo $Getfinandata->OPESRM;?>"
                        }
                    ]
                }
            ]
        }
    }).render();    
});
</script>

<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-Report',
        width: '550',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisName": "",
                //"numberPrefix": "$",
                "paletteColors": "#7080a0",
                "bgColor": "#cfdeea",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            
            "data": [
                {
                    "label": "Total",
                    "value": "9"
                }, 
                {
                    "label": "Active",
                    "value": "0"
                }, 
                {
                    "label": "Partially Active",
                    "value": "0"
                }, 
                {
                    "label": "InActive",
                    "value": "9"
                }
            ]
        }
    })
    .render();
});
</script>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
	   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Country</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ ?>
                  <option value="<?php echo $row->CountryID; ?>" ><?php echo $row->CountryName;?></option>
                    <?php } ?>
                 </select>
                </div>
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
                  <select class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){ ?>
                    <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" name="District" id="District">
                            <option value="">---All ---</option>
                   <?php foreach($District as $row){ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
		 <div class="clearfix visible-sm-block"></div>
        
		 <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            

            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $row){?>
                    <option value="<?php echo $row->PlantGUID;?>" ><?php echo $row->SWNID;?></option>
                   <?php } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
        <!-- /.col -->
      </div>
      <!-- /.row -->
	  
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SWE's</span>
              <span class="info-box-number">85</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Performance</span>
              <span class="info-box-number">56<small>%</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Financial</span>
              <span class="info-box-number">85</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Adoption</span>
              <span class="info-box-number">60</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
   <div class="row">
        <!-- Left col -->
	<section class="col-lg-3 connectedSortable">
       <div class="box box-info">
              <div id="chart-container-bar2d"></div>
    <div class="row" style="background: #545454; padding: 8px; color: #ffffff;">
                <div class="col-md-7" >Indicators</div>
                <div class="col-md-5"  style="text-align:right;">Achievements</div>
    </div>
    <div class="row" style="background: #4463a1;padding: 8px; color: #ffffff;">
                <div class="col-md-7" >Operational</div>
                <div class="col-md-5" style="text-align:right;">0</div>
    </div>
    <div class="row" style="background: #7080a0;padding: 8px; color: #ffffff;">
                <div class="col-md-7">Population Served</div>
                <div class="col-md-5" style="text-align:right;">348664</div>
    </div>
    <div class="row" style="background: #4463a1;padding: 8px; color: #ffffff;">
        <div class="col-md-7">Distribution Point</div>
        <div class="col-md-5" style="text-align:right;">24</div>
    </div>
           </div>
       </section>

<script type="text/javascript">
FusionCharts.ready(function () {
    var topStores = new FusionCharts({
        type: 'bar2d',
        renderAt: 'chart-container-bar2dperformance',
        width: '255',
        height: '200',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": " Sofia Score ",
                "yAxisName": "  ",
                "xAxisName": " ",
                //"numberPrefix": "$",
                "paletteColors": "#ff9800",
                "bgColor": "#cddc39",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#cddc39",
                "showAxisLines": "1",
                "axisLineAlpha": "15",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#cddc39",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            
            "data": [
                {
                    "label": "S",
                    "value": "68%"
                }, 
                {
                    "label": "O ",
                    "value": "56%"
                }, 
                {
                    "label": "F",
                    "value": "88%"
                }, 
                {
                    "label": "I",
                    "value": "33%"
                }, 
                {
                    "label": "E",
                    "value": "38%"
                }
            ]
        }
    })
    .render();
});
</script>



 
<section class="col-lg-3 connectedSortable" style="background-color: #cddc39; color: #fff; padding: 0px; 
border: 2px solid #cddc39;">
       <div id="chart-container-2d"></div>
       <div id="chart-container-bar2dperformance"></div>
</section>
		  
 <div class="clearfix visible-sm-block" ></div>
	<section class="col-lg-3 connectedSortable" style="padding-left: 5px; background-color: #e91e63; color: #fff; padding: 0px; border: 2px solid #e91e63;" >
	<div id="chart-container-financial"></div>
</section>
		 
	 
		<section class="col-lg-3 connectedSortable"  >
        <div class="box box-info">
           <div id="chart-container-gage">FusionCharts will render here</div>
           </div>
		  </section>

	</div>
	        <!-- Middle Chart -->
	
	   
   <div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
               <h3 class="box-title">Sofie</h3>
             </div>
			 <div id="chart-container-pie"></div>
             
		</div>
	</section>
		  
		  
		  <section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Map</h3>
              <div id="dvMap" style="width: 100%; height: 386px; border: 1px #a3c86d; border-style: none solid solid solid;">
            </div>
              </div>
            
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>
	<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Walkin vs distribution</h3>
              </div>
           <div id="chart-container-pie2d"></div>
          
          </div>
       
	</section>
		  
	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">User Report</h3>
              </div>
            <div id="chart-container-Report"></div>
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){

                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

            $("#State").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+$("#Country").val()+'/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#District").html(data);
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });

            $("#District").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getplants/'+$("#Country").val()+'/'+$("#State").val()+'/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#Plant").html(data);
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
   toShowMap('','','dvMap');
});
    </script>
   