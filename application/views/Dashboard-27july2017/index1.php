<!DOCTYPE html>
<html>
<head>
<title>ERP :: ADMIN</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />
<link rel="shortcut icon" href="images/favicon.ico">
<link href="<?php print FCSS; ?>bootstrap.min.css" rel="stylesheet">
<link href="<?php print FCSS; ?>date.css" rel="stylesheet">

<link href="<?php print FCSS; ?>new.css" rel="stylesheet">
<!-- Datepicker CSS -->
<link href="<?php print FTHEME; ?>fonts/font-awesome.min.css" rel="stylesheet">
<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    
  
  
</head>
<body>
<!-- Header Start -->
<header> 

<a href="index.html" class="logo"> <img src="images/logo.jpg" alt="Logo"/> </a>

  <div class="pull-right ">
<ul id="mini-nav" class="clearfix">
      <li class="list-box user-profile"> <a  href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i>&nbsp;&nbsp;Welcome Guest</a>
        <ul class="dropdown-menu server-activity">
          <li>
            <p><i class="fa fa-cog fa-male"></i><a href="profile.html"> My Profile</a></p>
          </li>
          <li>
            <p><i class="fa fa-cog fa-unlock-alt"></i> <a href="#">Change Password</a></p>
          </li>
          <li>
            <div class="demo-btn-group clearfix"> <a href="login.html">
              <button href="#" class="btn btn btn-info"> Logout </button>
              </a> </div>
          </li>
        </ul>
      </li>
     </ul>

  </div>
    
  </div>
</header>
<!-- Header End -->
<!-- Main Container start -->
<div class="dashboard-container">
  <div class="container">
    <!-- Top Nav Start -->
    <div id="cssmenu">
      <ul>
        <li class="active"> <a href="index.html"><i class="fa fa-home"></i>Home</a></li>
        <li> <a href="activate_order.html"><i class="fa fa-archive"></i>Third Parties</a>
          <ul>
            <li><a href="#">New Order</a>
            <ul>
              <li><a href="#">Dispatch</a></li>
             <li><a href="#">Purged Order</a></li>
            </ul>
            </li>
            <li><a href="#">Dispatch</a></li>
            <li><a href="#">Finalized Orders</a></li>
            <li><a href="#">Purged Order</a></li>
            <li><a href="#">Deleted Order</a></li>
            <li><a href="#">Schedule Order</a></li>
          </ul>
        </li>
        <li> <a href="#"> <i class="fa fa-cogs"></i> Products</a> </li>
        <li> <a href="#"> <i class="fa fa-eye"></i> Commercial </a> </li>
        <li> <a href="#"> <i class="fa fa-bullseye"></i> Financial </a> </li>
        <li> <a href="#"> <i class="fa fa-tag"></i> Tools </a> </li>
        <li> <a href="#"> <i class="fa fa-inbox"></i> Members </a> </li>
              </ul>
    </div>
    <!-- Top Nav End -->
    <!-- Sub Nav End -->
    
    <!-- Sub Nav End -->
    <!-- Dashboard Wrapper Start -->
    
   
  
  
    <div class="dashboard-wrapper">
    <div class="right-sidebar">
        <div class="widget">
          <div class="widget-header" style="background:#9B6700">
            <div class="title" style="color:#ffffff"> Home </div>
             </div>
   <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-refresh">
                            </span>&nbsp;Updates</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-eye-open text-primary"></span>&nbsp;<a href="#">View / Search</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span>&nbsp;<a href="#">Publish</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                            </span>&nbsp;Sections</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-eye-open text-primary"></span>&nbsp;<a href="#">View / Search</a> 
                                    </td>
                                </tr>
                                
                                
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-book">
                            </span>&nbsp;Stories</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-edit text-primary"></span>&nbsp;<a   data-toggle="modal" data-target="#myModal" href="#">Add</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-eye-open text-success"></span>&nbsp;<a href="#">View / Search</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-share text-danger"></span>&nbsp;<a href="#">Manage Shortcuts</a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-user">
                            </span>Clients</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-usd"></span>&nbsp;<a href="#">Add</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-user"></span>&nbsp;<a href="#">View / Search</a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-file">
                            </span>Story Sources</a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil text-primary"></span>&nbsp;<a href="#">Add</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="glyphicon glyphicon-eye-open text-success"></span>&nbsp;<a href="#">View / Search</a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-eye-open">
                            </span>&nbsp;News Agency</a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                         <span class="glyphicon glyphicon-pencil text-primary"></span>&nbsp;<a href="#">Add</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-book text-danger"></span>&nbsp;<a href="#">View / Search</a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><span class="glyphicon glyphicon-file">
                            </span>&nbsp;Reports</a>
                        </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-usd"></span><a href="#">News Letter</a>
                                    </td>
                                </tr>
                                
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

        </div>
      </div>
      <!-- Left Sidebar Start -->
      <div class="left-sidebar">
        <!-- Row Start -->
        
       
            
        <div class="row">
          <div class="col-lg-6 col-md-12">
            <div class="widget" id="t1">
              <div class="widget-header">
                <div class="title">  Orders </div>
                <span class="tools">
                  <a href="#" onClick="toggle_visibility('a1');"><i class="fa fa-minus" aria-hidden="true"></i></a>
                <a href="#" onClick="toggle_visibility('t1');"><i class="fa fa-times" aria-hidden="true"></i></a>
              
                 <script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }
//-->
</script>

                </span> 
                </div>
              <div class="widget-body">
                <!-- Row Start -->
                  <div class="tab-pane fade in active" id="a1">
                  <table class="table table-striped table-bordered table-hover no-margin">
                        <thead>
                          <tr>
                           
                            <th> Order No </th>
                            <th> Order Date </th>
                            <th> Ac.No. </th>
                            <th> Client </th>
                             <th> Action </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                          
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                           
                             
                            <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                            
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                          
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                          
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                           
                             
                            <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                           
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                            
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                          
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                           
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                           
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                            
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                            
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          
                        </tbody>
                      </table>
                  
                    <ul class="pagination no-margin">
                      <li class="disabled"><a href="#">«</a></li>
                      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                  </div>
                  
               
                <!-- Row End -->
              </div>
            </div>
          </div>
          
          <div class="col-lg-6 col-md-12">
            <div class="widget" id="t2">
              <div class="widget-header">
                <div class="title">  Orders </div>
                <span class="tools">
                 <a href="#" onClick="toggle_visibility('a2');"><i class="fa fa-minus" aria-hidden="true"></i></a>
                <a href="#" onClick="toggle_visibility('t2');"><i class="fa fa-times" aria-hidden="true"></i></a>
                </span> </div>
              <div class="widget-body">
                <!-- Row Start -->
                
            
      
                  <div class="tab-pane fade in active" id="a2">
                    <div class="sctab">
                      <table class="table table-striped table-bordered table-hover no-margin">
                        <thead>
                          <tr>
                           
                            <th> Order No </th>
                            <th> Order Date </th>
                            <th> Ac.No. </th>
                            <th> Client </th>
                             <th> Action </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                          
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                           
                             
                            <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                            
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                          
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                          
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                           
                             
                            <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                           
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                            
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                          
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                           
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                           
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                            
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          <tr>
                            <td> 619779 </td>
                            <td> 12/15/2015 </td>
                            <td> 42 </td>
                            <td> Nevada Bindery </td>
                            
                             <td align="center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i>

</td>
                          </tr>
                          
                        </tbody>
                      </table>
                    </div>
                    <ul class="pagination no-margin">
                      <li class="disabled"><a href="#">«</a></li>
                      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                  </div>
                  
               
                <!-- Row End -->
              </div>
            </div>
          </div>
          
        </div>
        
        
	
    
    
        <div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12">
          <div class="widget">
              <div class="widget-header">
                <div class="title">  Pie Chart </div>
                <span class="tools">
                
                </span> </div>
              <div class="widget-body">
                <!-- Row Start -->
                <div id="piechart" class="pie_new" >
           
         <script type="text/javascript" src="<?php print FJS; ?>loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    10]
        ]);

        var options = {
          title: '' 
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
          </div>
            
      
                  <div class="tab-pane fade in active" id="Open">
                    
                  </div>
                  
               
                <!-- Row End -->
              </div>
            </div>
           
          </div>
          
          	
    
          <div class="col-lg-6 col-md-12 col-sm-12">
          <div class="widget">
              <div class="widget-header">
                <div class="title">  Bar Chart </div>
                <span class="tools">
                
                </span> </div>
              <div class="widget-body">
                <!-- Row Start -->
                  
         <div id="chartContainer" ></div> 
         <script type="text/javascript">
		window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer", {
				//title: {
//					text: "Multi-Series Column Chart"
//				},
				data: [{
					type: "column",
					dataPoints: [
					  { x: 10, y: 750 },
					  { x: 20, y: 714 },
					  { x: 30, y: 320 },
					  { x: 40, y: 560 },
					  { x: 50, y: 430 },
					  { x: 60, y: 500 },
					  { x: 70, y: 480 },
					  { x: 80, y: 280 },
					  { x: 90, y: 410 },
					  { x: 100, y: 500 },
					]
				}, {
					type: "column",
					dataPoints: [
					  { x: 10, y: 250 },
					  { x: 20, y: 414 },
					  { x: 30, y: 919 },
					  { x: 40, y: 967 },
					  { x: 50, y: 678 },
					  { x: 60, y: 878 },
					  { x: 70, y: 787 },
					  { x: 80, y: 780 },
					  { x: 90, y: 803 },
					  { x: 100, y: 330 },
					]
				}, {
					type: "column",
					dataPoints: [
					  { x: 10, y: 400 },
					  { x: 20, y: 917 },
					  { x: 30, y: 520 },
					  { x: 40, y: 804 },
					  { x: 50, y: 450 },
					  { x: 60, y: 969 },
					  { x: 70, y: 380 },
					  { x: 80, y: 839 },
					  { x: 90, y: 610 },
					  { x: 100, y: 850 },
					]
				}]
			});
			chart.render();
		}
	</script>
	<script src="js/canvasjs.min.js"></script>     
      
                  <div class="tab-pane fade in active" id="Open">
                    
                  </div>
                  
               
                <!-- Row End -->
              </div>
            </div>
             
          
          </div>
        
        <!-- Row End -->
        <!-- Row Start -->
        
        <!-- Row End -->
      </div>
      <!-- Left Sidebar End -->
      <!-- Right Sidebar Start -->
      
      <!-- Right Sidebar End -->
    </div>
    <!-- Dashboard Wrapper End -->
    <footer>
      <p>Copyright © All Right Reserved. 2010. Terms of Use | <a href="#">Privacy Policy</a></p>
    </footer>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Story</h4>
      </div>
      <div class="modal-body">
     
        <form class="form-horizontal no-margin">
        <div class="form-group newrow">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
       
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Client: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="form-control" >
                    <option>Please Choose</option>
                     <option>All</option>
                  </select>
               
        </div>
        </div>
        </div>
        </div>
        
        <div class="form-group newrow">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
       
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Update: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="form-control" >
                    <option>Please Choose</option>
                     <option>All</option>
                  </select>
               
        </div>
        </div>
        </div>
        </div>
        
        <div class="form-group newrow">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
       
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Section: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="form-control" >
                    <option>Please Choose</option>
                     <option>All</option>
                  </select>
               
        </div>
        </div>
        </div>
        </div>
        
        <div class="form-group newrow">
        
        <div class="row">
        
        <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Date/Heading: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                

<input type="text" id="datepicker" class="form-control" > 
<i class="fa fa-calendar" aria-hidden="true"  id="datedesign"></i>

                  </div>
             
  
                  
                        <div class="col-md-8 col-sm-6 col-xs-12">
                   <input type="text" placeholder="Enter Heading"  class="form-control">
                     </div>
               
        </div>

             <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
   <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
        </div>
        </div>
        
        </div>
        
        <div class="form-group newrow">
        
        <div class="row">
        
        <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">New Agency: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                
<select class="form-control" >
                    <option>Please Choose</option>
                     <option>All</option>
                  </select>

                  </div>
             
  
                  
                        <div class="col-md-6 col-sm-6 col-xs-12">
                   <input type="text" placeholder=""  class="form-control">
                     </div>
               
        </div>

            
        </div>
        </div>
        
        </div>
        
        
        <div class="form-group newrow">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
       
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Source: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="form-control" >
                    <option>Please Choose</option>
                     <option>All</option>
                  </select>
               
        </div>
        </div>
        </div>
        </div>
        
        <div class="form-group newrow">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
       
         <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Link: </label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" placeholder="Enter Heading"  class="form-control">
               
        </div>
        </div>
        </div>
        </div>
        
        
        
        
        
        
       
        </form>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" style="margin-right:12px;">Update</button>
      </div>
    </div>
  </div>
</div>
<!-- Main Container end -->
<!--<script src="js/jquery.js"></script>-->


<script src="<?php print FJS; ?>bootstrap.min.js"></script>
<script src="<?php print FJS; ?>jquery.scrollUp.js"></script>
<!-- Tiny Scrollbar JS -->
<script src="<?php print FJS; ?>tiny-scrollbar.js"></script>
<!-- Custom JS -->
<script src="<?php print FJS; ?>menu.js"></script>
<script type="text/javascript">
      //ScrollUp
      $(function () {
        $.scrollUp({
          scrollName: 'scrollUp', // Element ID
          topDistance: '300', // Distance from top before showing element (px)
          topSpeed: 300, // Speed back to top (ms)
          animation: 'fade', // Fade, slide, none
          animationInSpeed: 400, // Animation in speed (ms)
          animationOutSpeed: 400, // Animation out speed (ms)
          scrollText: 'Top', // Text for element
          activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
      });

      //Tooltip
      $('a').tooltip('hide');
      $('i').tooltip('hide');

      //Tiny Scrollbar
      $('#scrollbar').tinyscrollbar();
      $('#scrollbar-one').tinyscrollbar();
      $('#scrollbar-two').tinyscrollbar();
      $('#scrollbar-three').tinyscrollbar();

    </script>
</body>
</html>