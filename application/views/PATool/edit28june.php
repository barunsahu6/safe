  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PATOOL
        <small>preview of PATOOL</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"> PATOOL </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
			  <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
      		  <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>
		   
		   <?php 
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		     $data = $this->model->getDetail($token);
			 $approval=array('0'=>'No', '1'=>'Yes');
			 $Earthing=array('0'=>'Not Done', '1'=>'Done');
			 //echo "<pre>";
			 //print_r($data);
			?>
			
		    				
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			  
			  <!-- Start Assessors details Here ----->
			  <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
			   <input type="hidden" class="form-control"  name="PlantUID" id="PlantUID" 
				  value="<?php  echo $data->PlantUID;?>" >
				 <input type="hidden" class="form-control"  name="PlantPOUID" id="PlantPOUID" 
				  value="<?php  echo $data->PlantPOUID;?>" >

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Assessors details</h3>
			  </div>
			 <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Visit Date</th>
                  <th>
				  <div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
                  </div>
						<input type="text" class="form-control pull-right datepicker" value="<?php  echo date("d-m-Y", strtotime($data->VisitDate));?>" 
name="VisitDate" >
						</div>
					</div>
				  </th>
                  <th>Assessed by</th>
				   <th>
				   <Select name="UserID" id="UserID"class="form-control" >
				  <option value="">Select</option>
				  <?php $Assessedby = $this->model->getAssessedby();
					foreach($Assessedby as $row){
					if($row->UserID==$data->UserID){
				  ?>
				  <option value="<?php echo $row->UserID; ?>" SELECTED><?php echo $row->FirstName." ".$row->LastName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->UserID; ?>" ><?php echo $row->FirstName." ".$row->LastName;?></option>
				  <?php  }} ?>
				  </select>
				  </th>
                  <th col="2">Location:</th>
			    </tr>
                </thead>
                <tbody>
				
                <tr>
                  <th>Assessing agency</th>
                  <th>
				  <Select name="AuditingAgency" id="AuditingAgency"class="form-control" >
				  <option value="">Select</option>
				  <!-- List Agency --->
				  <?php $Agency = $this->model->getAgency();
				  foreach($Agency as $row){
					if($row->AgencyID==$data->AuditingAgency){
				  ?>
				  <option value="<?php echo $row->AgencyID; ?>" SELECTED><?php echo $row->AgencyName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->AgencyID; ?>" ><?php echo $row->AgencyName;?></option>
				  <?php  }} ?>
	
                  </th>
                  <th>Assessing address</th>
                  <th> <input type="text" class="form-control"  name="AuditingAgencyAddress" id="AuditingAgencyAddress"
				  value="<?php echo $data->AuditingAgencyAddress;?>" ></th>
                  <th>Latitude</th>
				  <th><input type="text" class="form-control"  name="Latitude" id="Latitude"  value="<?php echo $data->Latitude;?>" ></th>
                </tr>
                <tr>
                  <th>Contact name</td>
                  <th><input type="text" class="form-control"  name="ContactName1" id="ContactName1" value="<?php echo $data->ContactName;?>" ></th>
                  <th>Email</th>
                  <th><input type="email" class="form-control"  name="Email" id="Email" value="<?php echo $data->Email;?>" ></th>
                  <th>Longitude	</th>
				  <th><input type="text" class="form-control"  name="Longitude" id="Longitude" value="<?php echo $data->Longitude;?>" ></th>
                </tr>
                </tbody>
              </table>
			 </div>
			  <!-- End Assessors details Here ----->
			   <!-- Start Plant Address Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Plant Address</h3>
			</div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>PlantID/Plant name</th>
                  <th><input type="Date" class="form-control"  name="VillageName" id="VillageName" 
				  value="<?php echo $data->VillageName;?>" required></th>
                  <th>Country</th>
                  <th>
				   <Select name="Country" id="Country"class="form-control" >
				    <?php $Country = $this->model->getCountry();
					foreach($Country as $row){
					if($row->CountryID==$data->CountryID){
				  ?>
				  <option value="<?php echo $row->CountryID; ?>" SELECTED><?php echo $row->CountryName;?></option>
					<?php }} ?>
				   
				  </select>
				 </th>
                  <th >State</th>
				  <th>
				   <Select name="State" id="State"class="form-control" >
				    <?php $State = $this->model->getState();
					foreach($State as $row){
					if($row->StateID==$data->StateID){
				  ?>
				  <option value="<?php echo $row->StateID;?>" SELECTED><?php echo $row->StateName;?></option>
					<?php }}?>
					</Select>
				  </th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>District</th>
                  <th>
				  <Select name="District" id="District"class="form-control" >
				    <?php $District = $this->model->getDistrict();
					foreach($District as $row){
					if($row->DistrictID==$data->District){
				  ?>
				  <option value="<?php echo $row->DistrictID; ?>" SELECTED><?php echo $row->DistrictName;?></option>
					<?php }}?>
					</Select>
                  </th>
                  <th>City</th>
                  <th> <input type="text" class="form-control"  name="BlockName" id="BlockName" 
				  value="<?php echo $data->BlockName;?>" required></th>
                  <th>Ward number</th>
				  <th><input type="text" class="form-control"  name="VillageName" id="VillageName" 
				  value="<?php echo $data->VillageName;?>" required></th>
                </tr>
                <tr>
                  <th>Pincode</td>
                  <th><input type="text" class="form-control"  name="PinCode" id="PinCode" 
				  value="<?php echo $data->PinCode;?>" required></th>
                  <th colspan="2">
                         <input type="radio" name="RUrban" id="RUrban" value="Urban" disabled >Urban
						 <input type="radio" name="RUrban" id="RUrban" disabled value="Rural" checked>Rural
                   </th>
                  
                  <th></th>	
				  <th></th>
                </tr>
                </tbody>
              </table>
			 </div>
			 <!-- End Plant Address Here ----->
			 
			 <!-- Start Existing Plant Details Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Existing Plant Details</h3>
			</div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Date of establishment </th>
                  <th>
				  
				   <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" name="EstablishmentDate" 
				  value="<?php echo date("d-m-Y", strtotime($data->EstablishmentDate));?>">
                </div>
				  
				  
				  </th>
                  <th>Age of plant(month)</th>
                  <th><input type="text" class="form-control"  name="AgeOfPlant" id="AgeOfPlant" 
				  value="<?php echo $data->AgeOfPlant;?>" required></th>
                  <th>Plant capacity(lph)</th>
				  <th>
				  <Select name="PlantSpecificationID" id="PlantSpecificationID"class="form-control" >
				  <option value="">All</option>
				  <?php $PlantSpecification = $this->model->getPlantSpecification();
					foreach($PlantSpecification as $row){
					if($row->PlantSpecificationID==$data->PlantSpecificationID){
				  ?>
				  <option value="<?php echo $row->PlantSpecificationID; ?>" SELECTED><?php echo $row->PlantSpecificationName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->PlantSpecificationID; ?>" ><?php echo $row->PlantSpecificationName;?></option>
				  <?php  }} ?>
				  
				  </th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Plant Manufactured by</th>
                  <th>
				  <Select name="PlantManufacturerID" id="PlantManufacturerID"class="form-control" >
				  <option value="">All</option>
				  <?php $PlantManufacturer = $this->model->getPlantManufacturer();
					foreach($PlantManufacturer as $row){
					if($row->PlantManufacturerID==$data->PlantManufacturerID){
				  ?>
				  <option value="<?php echo $row->PlantManufacturerID; ?>" SELECTED><?php echo $row->PlantManufacturerName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->PlantManufacturerID; ?>" ><?php echo $row->PlantManufacturerName;?></option>
				  <?php  }} ?>
                  </th>
                  <th>Remote Monitoring System</th>
                  <th> <Select type="Select" class="form-control"  name="RemoteMonitoringSystem" id="RemoteMonitoringSystem" placeholder=" Enter Remote Monitoring System" required>
				  <option value"0">No</option>
				  <option value"1">Yes</option>
				  </select>
				  </th>
                  <th></th>
				  <th></th>
                </tr>
                <tr>
				  <th colspan="6">
				    <table class="table table-bordered table-hover" >
					<thead>
					<tr>
				   <th colspan="6">
				   <div class="box-header with-border">
						<h3 class="box-title"> Contaminant in raw water</h3>
				</div>
				  </th>
				   </tr>
				   </thead>
				   
				   <?php 
				   $WQC=array();
				   $Contnts = $this->model->getPatWaterContaminants($token);
     			    $WaterQualityChallenge = $this->model->getWaterQualityChallenge();
					
					?>  
					
				   <tr>
				   <?php foreach($WaterQualityChallenge as $key => $val){ 
						//echo $Contnts[$key]->WaterQualityChallengeID."-". $val->WaterQualityChallengeID;
				   
				   
						if($val->WaterQualityChallengeID == $Contnts[$key]->WaterQualityChallengeID){
							
				   ?>
						<td colspan="6">
							<input type="checkbox" value="<?php echo $val->WaterQualityChallengeID;?>" name="WQC[]" id="WQC<?php echo $val->WaterQualityChallengeID;?>" checked > <?php echo $val->WaterQualityChallengeName;?>
						</td>
							<?php }else{ 
							
							?>
							
						<td colspan="6">
							<input type="checkbox" value="<?php echo $val->WaterQualityChallengeID;?>" name="WQC[]" id="WQC<?php echo $val->WaterQualityChallengeID;?>"> <?php echo $val->WaterQualityChallengeName;?>
						</td>
							<?php } }?>
				   
				   </tr>			   
                  </table>
				</th>
				  </tr>
				 <tr>
				  <th colspan="6">
				    <table class="table table-bordered table-hover" >
					<tr>
				   <th  colspan="6">Treatment steps</th>
				   </tr>
				   <?php 
				   $Patplt = $this->model->getPlantPurStep($token);
				   $PlantPurificationStep = $this->model->getPlantPurificationStep();?>  
				   <tr>
				   <?php foreach($PlantPurificationStep as $key=>$row){ 
							if($Patplt[$key]->PlantPurificationstepID==$row->PlantPurificationStepID){
				   ?>
						<td colspan="6">
							<input type="checkbox" value="<?php echo $row->PlantPurificationStepID;?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID;?>" checked > <?php echo $row->PlantPurificationStepName;?>
						</td>
							<?php }else{ ?>
							
						<td colspan="6">
							<input type="checkbox" value="<?php echo $row->PlantPurificationStepID;?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID;?>" > <?php echo $row->PlantPurificationStepName;?>
						</td>
							<?php } }?>
				   
							</tr>
							
							</table>
					</tr>
                  </table>
				</th>
				  </tr>
			   <tr>
				  <th colspan="6">
				    <table class="table table-bordered table-hover" >
					<tr>
				   <th  colspan="6">Plant funded by</th>
				   <tr>
					</tr>
						<?php 
						$Plantfundedby=array();
						$AssestFunder = $this->model->getAssestFunder();
						$plantfunter = $this->model->getPlantFundedBy($token);
						?>
						<tr>
						<?php foreach($AssestFunder as $row){
							if($row->AssestFunderID==$plantfunter->AssetFunderID){
						?>
						<td>
						<input type="checkbox" value="<?php echo $row->AssestFunderID; ?>" name="Plantfundedby[]" id="Plantfundedby" CHECKED> <?php echo $row->AssestFunderName;?>
						</td>
						<?php  }else{ ?>
						<td>
						<input type="checkbox" value="<?php echo $row->AssestFunderID; ?>" name="Plantfundedby[]" id="Plantfundedby"> <?php echo $row->AssestFunderName;?>
						</td>
						<?php } } ?>
						</tr>
                  </table>
				</th>
				  </tr>
				  
                </tbody>
              </table>
			 </div>
			  <!-- End Existing Plant Details Here ----->
			   <!-- Start Asset details Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Asset details</h3>
			</div>
			 <table class="table table-bordered table-hover" >
			<thead>
			<tr>
				<th colspan="6">Machinery</th>
			</tr>
			<?php $AssestFunder = $this->model->getAssestFunder();?> 
			<tr>
			<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->MachineryOther){

			?>
				<td colspan="6">
				 <input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="MachineryOther" id="MachineryOther" CHECKED> <?php echo $row->AssestFunderName;?>
				</td>
			<?php  }else{ ?>
			<td colspan="6">
				 <input type="checkbox"  value="<?php echo $row->AssestFunderID;?>" name="MachineryOther" id="MachineryOther"> <?php echo $row->AssestFunderName;?>
				</td>
			<?php } }?>
			<td>
				<input type="text" name="MachineryCost" id="MachineryCost" class="form-control" value="<?php echo $data->MachineryCost;?>">
			</td>
			</tr>
			<tr>
				<th colspan="6">Land</th>
		   </tr>
		   <!-- Flag 2 List  -->
			<?php $AssestFunder = $this->model->getAssestFunderFlag2();?>
			<tr>
			<?php foreach($AssestFunder as $row){
				if($row->AssestFunderID==$data->LandOther){
			?>
			<td colspan="6">
			<input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="LandOther" id="LandOther" CHECKED> <?php echo $row->AssestFunderName;?>
			</td>
			<?php  }else{ ?>
			<td colspan="6">
			<input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="LandOther" id="LandOther" > <?php echo $row->AssestFunderName;?>
			</td>
			<?php }} ?>
			<td>
				<input type="text" name="LandCost1" id="LandCost1" class="form-control" value="<?php echo $data->LandCost;?>">
			</td>
			</tr>
			<tr>
				<th colspan="6" >Building</th>
		   </tr>
		    <!-- Flag 3 List  -->
			<?php $AssestFunder = $this->model->getAssestFunderFlag3();?>
			<tr>
			<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->BuildingOther){
			?>
				<td colspan="6">
					<input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="BuildingOther" id="BuildingOther" CHECKED> <?php echo $row->AssestFunderName;?>
				</td>
			<?php  }else{ ?>
			<td colspan="6">
					<input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="BuildingOther" id="BuildingOther"> <?php echo $row->AssestFunderName;?>
				</td>
			
			<?php }} ?>
			<td>
				<input type="text" name="BuildingCost1" id="BuildingCost1" class="form-control" value="<?php echo $data->BuildingCost;?>">
			</td>
			</tr>
		   <tr>
				<th colspan="6">Raw water source</th>
		   </tr>
		   <!-- Flag 3 List  -->
			<?php $AssestFunder = $this->model->getAssestFunderFlag4();?>
			<tr>
			<?php foreach($AssestFunder as $row){
				if($row->AssestFunderID==$data->RawWaterSourceOther){
				?>
				<td colspan="6">
					<input type="checkbox" value="<?php echo $row->AssestFunderID;?>"name="RawWaterSourceOther" id="RawWaterSourceOther" CHECKED> <?php echo $row->AssestFunderName;?>
				</td>
			<?php  }else{?>
			<td colspan="6">
					<input type="checkbox" value="<?php echo $row->AssestFunderID;?>"name="RawWaterSourceOther" id="RawWaterSourceOther"> <?php echo $row->AssestFunderName;?>
				</td>
			<?php } }?>
			<td>
					<input type="text" name="RawWaterSourceCost" id="RawWaterSourceCost" class="form-control" value="<?php echo $data->RawWaterSourceCost;?>">
			</td>
			</tr>
		   <tr>
				<th colspan="6">Electricity connection</th>
		   </tr>
		   <!-- Flag 3 List  -->
			<?php $AssestFunder = $this->model->getAssestFunderFlag5();?>
			<tr>
			<?php foreach($AssestFunder as $row){ 
			if($row->AssestFunderID==$data->ElectricityOther){
			?>
				<td colspan="6">
					<input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="ElectricityOther" id="ElectricityOther" CHECKED> <?php echo $row->AssestFunderName;?>
				</td>
			<?php  }else{ ?>
			<td colspan="6">
					<input type="checkbox" value="<?php echo $row->AssestFunderID;?>" name="ElectricityOther" id="ElectricityOther"> <?php echo $row->AssestFunderName;?>
				</td>
			<?php } }?>
			<td>
					<input type="text" name="ElectricityCost1" id="ElectricityCost1" class="form-control" value="<?php echo $data->ElectricityCost;?>">
			</td>
			</tr>
			
			<tr>
				<th colspan="3">Aggregator</th>
				<th colspan="3"><input type="text" name="WaterBrandName" id="WaterBrandName" class="form-control" value="<?php echo $data->WaterBrandName;?>"></th>
		   </tr>
		   </tr>
		     </thead>
		   </table>
	</div>
			 
               <!-- End Asset details Here ----->
		</div><!-- Tab 1 End Here-->
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <!-- Start Asset details Here ----->
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Social sustainability</h3>
			</div>
			
			 <table  class="table table-bordered table-hover">
                <thead>
               <tr>
                  <th>Population </th>
                  <th><input type="text" class="form-control"  name="Population" id="Population"  value="<?php echo $data->Population;?>" required></th>
                  <th>Number of household</th>
                  <th><input type="text" class="form-control"  name="NoOfHousehold" id="NoOfHousehold" value="<?php echo $data->NoOfHousehold;?>" required></th>
                  <th>Number of household within 500m</th>
				  <th><input type="text" class="form-control"  name="NoOfHouseholdWithin2km" id="NoOfHouseholdWithin2km" 
				  value="<?php echo $data->NoOfHouseholdWithin2km;?>" required></th>
			  </tr>
			   <tr>
                  <th>Number of household registered </th>
                  <th><input type="text" class="form-control"  name="NoOfhhregistered" id="NoOfhhregistered" 
				  value="<?php echo $data->NoOfhhregistered;?>" required></th>
                  <th>Average number of monthly water cards/RFID cards</th>
                  <th><input type="text" class="form-control"  name="Average_number_of_monthly_water_cards" id="Average_number_of_monthly_water_cards" 
				  value="<?php echo $data->Population;?>" required></th>
                  <th>Distribution</th>
				  <th>
				  <Select type="Select" class="form-control"  name="Distribution" id="Distribution]">
				  <?php foreach($approval as $key=>$res){ 
				  if($data->Distribution ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
				  </select>
				  
			  </tr>
			    
			   <tr>
                 <td colspan="6">
				 <table  class="table table-bordered" >
				 <tr>
				 <th colspan="6"><div class="box box-primary">Social composition</th>
				 </tr>
				 <tr>
				  <th >
				  <input type="checkbox" value="<?php echo $data->SC;?>" name="SC" id="SC" checked> SC</th>
					<th ><input type="checkbox" value="<?php echo $data->ST;?>" name="ST" id="ST" checked> ST<th>
					
					<th><input type="checkbox" value="<?php echo $data->OBC;?>" name="OBC" id="OBC" checked> OBC</th>
					<th>
					<label><input type="checkbox" value="<?php echo $data->General;?>" name="General" id="General" checked>General</label>
					</th>
					<th >
					<label><input type="checkbox" value="<?php echo $data->AllInclusion;?>" name="AllInclusion" id="AllInclusion" checked>All Inclusion</label>
					</th>
					</tr>
				 </table>
				 <td>
			  </th>
			  </tr>
			  </div>
			  <tr>
                  <th colspan="6">
				  <div class="box box-primary">Distribution/Distance points<div>
					<table  class="table table-bordered table-hover">
					<thead>
					 <tr>
					  <th>Distribution point</th>
					  <th>
					  <input type="hidden"  name="PlantGUID" id="PlantGUID" 
					  value="<?php echo $token;?>" >
					  <input type="text" class="form-control"  name="DPName" id="DPName" 
					  value="<?php echo $data->DPName;?>" ></th>
					  <th>Distance</th>
					  <th><input type="text" class="form-control"  name="Distance" id="Distance" 
					  value="<?php echo $data->Distance;?>" ></th>
					  
					</tr>
					</thead>
					</table>
				  </th>
			</tr>
			 </thead>
           </table>
			   </div>
		 </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
               <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Plant production capacity(litres)</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Avg daily water produced in litres</th>
                  <th><input type="Date" class="form-control"  name="WaterProductionDaily" 
				  id="WaterProductionDaily" value="<?php echo $data->WaterProductionDaily;?>" required></th>
                  
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Avg monthly water production in litres</th>
                  <th><input type="text" class="form-control"  name="WaterProductionMonthly" id="WaterProductionMonthly" value="<?php echo $data->WaterProductionMonthly;?>" required>
                  </th>
                 
                </tr>
                <tr>
                  <th>PeakSale</td>
                  <th><input type="text" class="form-control"  name="PeakSale" id="PeakSale" value="<?php echo $data->PeakSale;?>" required></th>
                  
                </tr>
                </tbody>
              </table>
			 </div>
			 
			 
              <!----  Start Here    -->			 
			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Kiosk operator details</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th><input type="text" class="form-control"  name="ContactName" id="ContactName" value="<?php echo $data->ContactName;?>" required></th>
                  <th>Designation</th>
                  <th>
				  
				  <?php $Designation = $this->model->getDesignation();  ?>
				  <Select type="Select" class="form-control"  name="Designation" id="Designation" required>
				  <?php foreach($Designation as $row) { 
				  if($row->DesignationID==$data->DesignationID){
				  ?>
				  <option value="<?php echo $row->DesignationID;?>" SELECTED><?php echo $row->DesignationName;?></option>
				  <?php }else{ ?>
				  <option value="<?php echo $row->DesignationID;?>"><?php echo $row->DesignationName;?></option>
				  <?php } }?>
				  </select>
				  
				  </th>
				  <th>Contact number</th>
                  <th>
				  <input type="text" class="form-control"  name="ContactNumber" id="ContactNumber" value="<?Php echo $data->ContactNumber;?>" required>
				  </th>
			    </tr>
              </table>
			 </div>
			 <!----- End here -->
			 
			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training and capacity building</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>
				  
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training recieved</h3>
			  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->ElectricalSafety; ?>" name="ElectricalSafety" id="ElectricalSafety"
					  <?php if($data->ElectricalSafety=='0'){ echo "";}else{ echo "CHECKED";}?>>
                     Electrical safety
                    </label>
                  </div>
                   <div class="checkbox">
                    <label>
                       <input type="checkbox" value="<?php echo $data->PlantOM; ?>" name="PlantOM" id="PlantOM" 
					   <?php if($data->PlantOM=='0'){ echo "";}else{ echo "CHECKED";}?>>
                      Plant operation and managment
                    </label>
                  </div>
				<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->WaterQuality; ?>" name="WaterQuality" id="WaterQuality" <?php if($data->WaterQuality=='0'){ echo "";}else{ echo "CHECKED";}?>>
                      Water quality
                    </label>
                  </div>
				<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->ConsumerAwareness; ?>" name="ConsumerAwareness" id="ConsumerAwareness" <?php if($data->ConsumerAwareness=='0'){ echo "";}else{ echo "CHECKED";}?> >
                      Consumer awareness
                    </label>
                  </div>
					<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->ABKeeping; ?>" name="ABKeeping" id="ABKeeping"<?php if($data->ABKeeping=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Accounts and book keeping
                    </label>
                  </div>
				  
			 </div>
			 
			 
			 
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Quality assurance and Hygiene</h3>
			  </div>
			<div class="row">
			  <div class="col-sm-4">					</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Earthing" id="Earthing">
               <?php foreach($Earthing as $key=>$res){ 
				  if($data->Earthing ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
            </select>
				  </div>
			  </div> 

		<div class="row">
			<div class="row">Raw water source</div>
			  <div class="col-sm-4">Open well covered</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="RawWaterOpenwellcovered" id="RawWaterOpenwellcovered">
                <?php foreach($approval as $key=>$res){ 
				  if($data->RawWaterOpenwellcovered ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			  <div class="col-sm-4">Bore well casing</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="RawWaterBoreWellCasing" id="RawWaterBoreWellCasing">
               <?php foreach($approval as $key=>$res){ 
				  if($data->RawWaterBoreWellCasing ==$key){
				?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
				<?php }} ?>
                 </select>
				  </div>
			  </div> 			  
			
			<div class="row">
			<div class="row">Treated water tank</div>
			  <div class="col-sm-4">Inside the plant</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Insidetheplant" id="Insidetheplant">
            <?php foreach($approval as $key=>$res){ 
			  if($data->Insidetheplant ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			  <div class="col-sm-4">Covered</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Covered" id="Covered">
             <?php foreach($approval as $key=>$res){ 
			  if($data->Covered ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 		
			
			
			<div class="row">
			<div class="row">Plant hygiene inside</div>
			  <div class="col-sm-4">Cleanliness in plant</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="CleanlinessinPlant" id="CleanlinessinPlant">
            <?php foreach($approval as $key=>$res){ 
			  if($data->CleanlinessinPlant ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			  <div class="col-sm-4">Leakage</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Leakage" id="Leakage">
            <?php foreach($approval as $key=>$res){ 
			  if($data->Leakage ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 


			<div class="row">
			<div class="row">Plant hygiene outside</div>
			  <div class="col-sm-4">Cleanliness near treatment plant</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="CleanlinessNearTreatmentPlant" id="CleanlinessNearTreatmentPlant">
            <?php foreach($approval as $key=>$res){ 
			  if($data->CleanlinessNearTreatmentPlant ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 

			<div class="row">
			<div class="row">Water fill station</div>
			  <div class="col-sm-4">Moss/algae growth</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Checkmossoralgee" id="Checkmossoralgee">
             <?php foreach($approval as $key=>$res){ 
			  if($data->Checkmossoralgee ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 
			<div class="row">
			  <div class="col-sm-4">Presence of puddle</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="Presenceofpuddle" id="Presenceofpuddle">
            <?php foreach($approval as $key=>$res){ 
			  if($data->Presenceofpuddle ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
				  </div>
			  </div> 			  
			</div>

		</th>
			<th valign="top" style="margin-top: 5px" >
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Water quality test report from accredited laboratory</h3>
			  </div>
			<div class="row">
			  <div class="col-sm-4">
			<div class="form-group">Pre monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PreMonsoonRWTesting;?>" name="PreMonsoonRWTesting" id="PreMonsoonRWTesting"
					   <?php if($data->PreMonsoonRWTesting=='0'){ echo "";}else{ echo "CHECKED";}?>>
                     Raw water
                    </label>
                  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PreMonsoonRWLabAdr;?>" name="PreMonsoonRWLabAdr" id="PreMonsoonRWLabAdr"
					   <?php if($data->PreMonsoonRWLabAdr=='0'){ echo "";}else{ echo "CHECKED";}?>>
                     Treated water
                    </label>
                  </div>
			  
			  </div>
			  </div>
			 <div class="form-group">Post monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PostMonsoonRWTesting;?>" name="PostMonsoonRWTesting" id="PostMonsoonRWTesting"
					 <?php if($data->PostMonsoonRWTesting=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Raw water
                    </label>
                  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PostMonsoonRWLabadr;?>"  name="PostMonsoonRWLabadr" id="PostMonsoonRWLabadr"
					 <?php if($data->PostMonsoonRWLabadr=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Treated water
                    </label>
                  </div>
			      <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->RejectWaterTesting;?>"   name="RejectWaterTesting" id="RejectWaterTesting"
					 <?php if($data->RejectWaterTesting=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Reject water
                    </label>
                  </div>				   
			  </div>
			</div>
			  </div>	
	<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Reliability of operations</h3>
			  </div>
			<div class="row">
			  <div class="col-sm-4">Technical downtime</div>
			  <div class="col-sm-4">
			  <select class="form-control" name="Leakage" id="Leakage">
			  <?php foreach($approval as $key=>$res){ 
				  if($data->TechnicalDowntime ==$key){
				?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
				<?php }} ?>
                 </select>
				  </div>
			  </div> 	
			  
			  <div class="row">
			  <div class="col-sm-4">No of days</div>
			  <div class="col-sm-4">
			  <input type="text" class="form-control" name="NoOfDays" id="NoOfDays" value="<?php echo $data->NoOfDays;?>">
				  </div>
			  </div> 
			  <div class="row">
			  <div class="col-sm-4">No of reason for downtime</div>
			  <div class="col-sm-4">
			  <input type="text" class="form-control" name="TechnicalDowntime" id="TechnicalDowntime" value="<?php echo $data->NoOfFault;?>">
				  </div>
			  </div> 	
			  
			  
			  <div class="row">
			  <div class="col-sm-4">Sales day lost</div>
			  <div class="col-sm-6">
			  <input type="text" class="form-control" name="SalesDayLost" id="SalesDayLost" value="<?php echo $data->SalesDayLost;?>">
			  
				  </div>
			  </div> 	
			
			
			</div>
			 
			
			</div>
		</div>			  
			
          </th>
                   </tr>
              </table>
			 </div>
			 
           </div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_4">
               
			<div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Capital expenditure</h3>
			  </div>
			  <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Machinery cost</th>
                  <th><input type="text" class="form-control"  name="MachineryCost1" id="MachineryCost1" value="<?php echo $data->MachineryCost;?>" ></th>
                  <th>Raw water source cost</th>
                  <th><input type="text" class="form-control"  name="RawWaterSourceCost1" id="RawWaterSourceCost1" value="<?php echo $data->RawWaterSourceCost;?>" ></th>
                  <th>Building cost</th>
				  <th><input type="text" class="form-control"  name="BuildingCost" id="BuildingCost" value="<?php echo $data->BuildingCost;?>" ></th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Land cost</th>
                  <th><input type="text" class="form-control"  name="LandCost" id="LandCost" value="<?php echo $data->LandCost;?>" >
                  </th>
                  <th>Electricity connection cost</th>
                  <th> <input type="text" class="form-control"  name="ElectricityCost" id="ElectricityCost" value="<?php echo $data->ElectricityCost;?>" ></th>
                  <th>Total cost</th>
				  <?php 
					$TotalCost = $data->MachineryCost+$data->RawWaterSourceCost+$data->BuildingCost+$data->LandCost+$data->ElectricityCost;

				  ?>
				  
				  <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" value="<?php echo $TotalCost;?>" ></th>
                </tr>
                </tbody>
              </table>
		 </div>
		 
		 <?php  
			$ProdDetail=$this->model->getPatPlantProdDetail($token);
			//print_r($ProdDetail);
			

		 ?>
		 <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Revenue</h3>
			  </div>
			  <table class="table table-bordered table-hover">
			  <thead>
			  <tr>
				<td></td>
				<th>Price/Ltr</th>
				<th>Vol in Ltr</th>
				<th>Total</th>
			  </tr>
			</thead>
			<?php foreach($ProdDetail as $key=>$val){ 
				$price=$val->Price;
				$vol = $val->Volume;
				$totalpvcost= $price*$vol;
			?>
			<tr>
				<th>At Plant</th>
				<td><?php echo $val->Price;?></td>
				<td><?php echo $val->Volume;?></td>
				<td><?php echo $totalpvcost; ?></td>
			  </tr>
			<?php  } ?>
			<tr>
				<th>Home Delivery</th>
				<td></td>
				<td></td>
				<td></td>
			  </tr>	
			<tr>
				<th>DP1</th>
				<td></td>
				<td></td>
				<td></td>
			  </tr>				  
			</table>
		 </div>
		 
		 
		  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Operating expense (per month)</h3>
			  </div>
			   <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Raw water bill(If any)</th>
                  <th><input type="text" class="form-control"  name="WaterBill" id="WaterBill" value="<?php echo $data->WaterBill; ?>" ></th>
                  <th>Rent of station</th>
                  <th><input type="text" class="form-control"  name="Rent" id="Rent" value="<?php echo $data->Rent;?>" ></th>
                  <th>Transport Expense</th>
				  <th><input type="text" class="form-control"  name="TransportExpense" id="TransportExpense" value="<?php echo $data->TransportExpense;?>" ></th>
				  <th>Chemicals and other consumables</th>
				  <th><input type="text" class="form-control"  name="ChemicalAndOtherConsumable" id="ChemicalAndOtherConsumable" 
				  value="<?php echo $data->ChemicalAndOtherConsumable;?>" ></th>
			    </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Electricity bill</th>
                  <th><input type="text" class="form-control"  name="ElectricityBill" id="ElectricityBill" value="<?php echo $data->ElectricityBill;?>" required></th>
                  <th>Operator salary</th>
                  <th> <input type="text" class="form-control"  name="OperatorSalary" id="OperatorSalary" value="<?php  echo $data->OperatorSalary; ?>" required></th>
                  <th></th>
				  <th></th>
                </tr>
				 <tr>
                  <th>Generator for electricity</th>
                  <th><input type="text" class="form-control"  name="Electricityoutage" id="Electricityoutage" value="<?php echo $data->Electricityoutage;?>" required></th>
                  <th></th>
                  <th></th>
                  <th></th>
				  <th></th>
                </tr>
                </tbody>
              </table>
			  <table  class="table table-bordered table-hover">
			  <thead>
			  <tr>
                  <th colspan="3">
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
						Miscellaneous/Any other
                    </label>
					<table class="table table-bordered table-hover ">
					<tr>
					<td>Service Charge</td>
					<td><input type="text" class="form-control"  name="ServiceCharge" id="ServiceCharge" value="<?php echo $data->ServiceCharge;?>" required></td>
					<td>Sustainability fund</td>
					<td><input type="text" class="form-control"  name="AssestRenewalFund" id="AssestRenewalFund" value="<?php echo $data->AssestRenewalFund;?>" required></td>
					</tr>
					</table>
                  </div>
				  
				  <th>
				 </tr>
                </thead>
                                
               </table>
						  
			  <table  class="table table-bordered table-hover">
			  <thead>
			   
				<tr>
                  <th colspan="3"> Financial sustainability</th>
                  <tr>
                  <th>OpEx</th>
                  <th> <input type="text" class="form-control"  name="OPEx" id="OPEx" value="<?php echo $data->OPEx;?>" ></th>
                  </tr>
				   <tr>
                  <th>OpEx +Service charge</th>
                  <th> <input type="text" class="form-control"  name="OPExservicecharge" id="OPExservicecharge" value="<?php echo $data->OPExservicecharge;?>" ></th>
                  </tr>
				   <tr>
                  <th>OpEx +Service charge maintenance reserve</th>
                  <th> <input type="text" class="form-control"  name="OPExservicechargemaintenance" id="OPExservicechargemaintenance"
				  value="<?php echo $data->OPExservicechargemaintenance;?>" ></th>
                  </tr>
				 </thead>
              </table>
			</div>
	 </div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_5">
                
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Institutional sustainability</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
				<th colspan="2">Approved Yes/No</th>
				<th colspan="2">Approved Yes/No</th>
                 </tr>
				 <tr>
                  <th>Local government approval</th>
				  
                  <th>
				  <select class="form-control" name="GramPanchayatApproval" id="GramPanchayatApproval">
				  <?php foreach($approval as $key=>$res){ 
				  if($data->GramPanchayatApproval ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
				 </th>
				  <th>reject water discharge</th>
                  <th>
				  <select class="form-control" name="reject_water_discharge" id="reject_water_discharge">
                    <?php foreach($approval as $key=>$res){ 
				  if($data->RejectWaterDischargeApproval ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
                  </th>
			    </tr>
				
				<tr>
                  <th>Legal electicity connection</th>
                  <th>
				  <select class="form-control" name="LegalElectricityConnection" id="LegalElectricityConnection">
                    <?php foreach($approval as $key=>$res){ 
				  if($data->LegalElectricityConnection ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
                
				  </th>
				  <th>Trained Operator</th>
                  <th>
				  <select class="form-control" name="Trained_Operator" id="Trained_Operator">
                    <?php foreach($approval as $key=>$res){ 
				  if($data->GramPanchayatApproval ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
				  </th>
			    </tr>
				
				<tr>
                  <th>Premises and land</th>
                  <th>
				  <select class="form-control" name="LandApproval" id="LandApproval">
                   <?php foreach($approval as $key=>$res){ 
				  if($data->LandApproval ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
				  </th>
				  <th>Raw source water</th>
                  <th>
				  <select class="form-control" name="RawWaterSourceApproval" id="RawWaterSourceApproval">
				  <?php foreach($approval as $key=>$res){ 
				  if($data->RawWaterSourceApproval ==$key){
				  ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
					<?php }} ?>
                 </select>
				  </th>
			    </tr>
				
				<tr>
                  <th colspan="1">Document Upload</th>
				  <th colspan="1">
				   <input type="file" name="uploadDocument" id="uploadDocument" >
				  </th>
				  <th colspan="2"></th>
                </tr>
				
				
              </table>
			 </div>
		</div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_6">
			   <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Enviromental sustainability</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
				 <tr>
                  <th>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="checkRWQuantification" id="checkRWQuantification"<?php if($data->checkRWQuantification==0){
					  echo "";}else{ echo "CHECKED";}?>  >
                    Reject water Produced
                    </label>
                  </div>
				  </th>
                  <th><input type="text" class="form-control"  name="WaterProductionDaily" id="WaterProductionDaily" value="<?php echo $data->WaterProductionDaily;?>" ></th>
				  <th></th>
				  <th></th>
			    </tr>
				<tr>
				<th>
				   <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->CheckRWDisposal;?>" name="CheckRWDisposal" id="CheckRWDisposal"<?php if($data->CheckRWDisposal==0){
					  echo "";}else{ echo "CHECKED";}?> >
                   Reject water disposed
                    </label>
                  </div>
				  </th>
                  <th><input type="text" class="form-control"  name="CheckRWDisposal" id="CheckRWDisposal" value="<?php echo $data->CheckRWDisposal;?>" ></th>
				  <th></th>
				  <th></th>
				</tr>
				<tr>
                  <th colspan="4" > 
				  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                   Ground water recharge / rain water harvesting
                    </label>
                  </div>
				  </th>
			    </tr>
				<tr>
                  <th colspan="2">
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->CheckRWUtilisation; ?>" name="CheckRWUtilisation" id="CheckRWUtilisation" <?php if($data->CheckRWUtilisation==0){
					  echo "";}else{ echo "CHECKED";}?>>
                       Reject water utilised
                    </label>
                  </div>			  
				  </th>
				  <th>
				  Activity
				  <input type="text" class="form-control"  name="UtilizationActivity1" id="UtilizationActivity1" value="<?php echo $data->UtilizationActivity1;?>" >
				  <input type="text" class="form-control"  name="UtilizationActivity2" id="UtilizationActivity2" value="<?php echo $data->UtilizationActivity2;?>" >
				  <input type="text" class="form-control"  name="UtilizationActivity3" id="UtilizationActivity3" value="<?php echo $data->UtilizationActivity3;?>" >
				  <input type="text" class="form-control"  name="UtilizationActivity4" id="UtilizationActivity4" value="<?php echo $data->UtilizationActivity4;?>" ></th>
				  </th>
                  <th>
				  Litres/Day
				  <input type="text" class="form-control"  name="UtilizationOther1" id="UtilizationOther1"  value="<?php echo $data->UtilizationOther1; ?>" >
				  <input type="text" class="form-control"  name="UtilizationOther2" id="UtilizationOther2"  value="<?php echo $data->UtilizationOther2; ?>" >
				  <input type="text" class="form-control"  name="UtilizationOther3" id="UtilizationOther3"  value="<?php echo $data->UtilizationOther3; ?>" >
				  <input type="text" class="form-control"  name="UtilizationOther4" id="UtilizationOther4"  value="<?php echo $data->UtilizationOther4; ?>" ></th>
				  </tr>
				</table>
			 </div>
			   
              </div>
			  <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?>
     		<?php print form_close(); ?>
              <!-- /.tab-pane -->
            </div>
			
            <!-- /.tab-content -->
          </div>
		  
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->