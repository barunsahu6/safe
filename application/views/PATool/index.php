  <script type="text/javascript" src="<?php print FJS;?>jquery-1.12.4.js"></script>

  <script type="text/javascript">
  /* Formatting function for row details - modify as you need */
function format (d) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            "<td style='padding-left:50px;'><a href='<?php echo site_url(); ?>/PATool/edit/"+d.PlantGUID+"'>Eval_1</a></td>"+

        '</tr>'+

    '</table>';
}
$(document).ready(function() {
    var table = $('#example').DataTable( {
       "ajax": "<?php echo site_url(); ?>/Patajax/getSWNID",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "SWNID" }

        ],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it*
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );

</script>
<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 525px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
#style-4::-webkit-scrollbar-thumb
{
	background-color: #F5F5F5;
}
</style>
<style type="text/css">
  td.details-control {
    background: url('<?php print FIMAGES;?>details_open.png') no-repeat center center;
    background-size: 20px 20px;
    padding-top: 40px;
}
tr.shown td.details-control {
    background: url('<?php print FIMAGES;?>details_close.png') no-repeat center center;
    background-size: 20px 20px;
    padding-top: 20px;
}
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<div class="row">
		<div class="col-md-3" style="padding-top: 5px;  height: 525px; overflow-y: scroll;>
		<label style="background-color: #674e9e; width: 600px; color: #ffffff; padding-left:10px;" >Operations</label>

          <table id="example" class="display" width="100%" cellspacing="0">
					<thead>
					    <tr>
					        <th style="width: 30px;"></th>
					        <th  style="padding-left: 10px;">SWNID</th>
					    </tr>
					</thead>
					<tfoot>
					    <tr>
					        <th style="width:30px;"></th>
					        <th style="padding-left: 10px;">SWNID</th>
					    </tr>
					</tfoot>
			</table>
		</div>
		<div class="col-md-9" >
		<div class="scrollbar" id="style-3">
		<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
			  <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
      		  <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>

		   <?php
$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
$data = $this->model->getDetail($token);
$approval = array('0' => 'No', '1' => 'Yes');
$Earthing = array('0' => 'Not Done', '1' => 'Done');
?>

            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			  <?php print form_open_multipart($this->router->class . '/' . $this->router->method . '/' . $token, array('name' => $this->router->method . $this->router->class, 'id' => $this->router->method . $this->router->class))?>
			   <input type="hidden" class="form-control"  name="PlantUID" id="PlantUID"
				  value="" >
				 <input type="hidden" class="form-control"  name="PlantPOUID" id="PlantPOUID"
				  value="" >

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Assessors Details</h3>
			  </div>

			<div class="row">
			 <div class="box-header with-border" style="margin-left:10px;">
              <h6 class="box-title">Location:</h6>
			</div>
			<div class="row" style="margin-top:10px; margin-left:5px;">
			<div class="col-lg-12" >
			  <div class="col-lg-6">Latitude</div>
			  <div class="col-lg-6"><input type="text" class="form-control"  name="Latitude" id="Latitude"  value=""></div>
			</div>


			<div class="col-lg-12">
			<div class="col-lg-6">Longitude</div>
				<div class="col-lg-6">
				 <input type="text" class="form-control"  name="Longitude" id="Longitude" value=""></div>
				</div>
			</div>


			  <div class="col-lg-12">
			  <div class="col-lg-6">Date of Visit</div>
			  <div class="col-lg-6" style="margin-top:10px;">
			  <div class="form-group">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
				</div>
				<input type="text" class="form-control pull-right datepicker" value="" name="VisitDate" >
				</div>
			</div>
			</div>
			  </div>

		<div class="col-lg-12">
			<div class="col-lg-6">Assessed By</div>
				<div class="col-lg-6">
				<Select name="UserID" id="UserID"class="form-control" >
					<option value="">Select</option>
					<?php $Assessedby = $this->model->getAssessedby();
foreach ($Assessedby as $row) {
    ?>
					<option value="<?php echo $row->UserID; ?>" ><?php echo $row->FirstName . " " . $row->LastName; ?></option>
					<?php }?>
					</select>
				</div>
			</div>

			  <div class="col-lg-12">
			  <div class="col-lg-6">Assessing Agency</div>
			  <div class="col-lg-6">
			   <Select name="AuditingAgency" id="AuditingAgency"class="form-control" >
				  <option value="">Select</option>
				  <?php $Agency = $this->model->getAgency();
foreach ($Agency as $row) {
    ?>

				   <option value="<?php echo $row->AgencyID; ?>" ><?php echo $row->AgencyName; ?></option>
				 <?php }?>
				  </select>
			  </div>
			  </div>

			  <div class="col-lg-12">
			  <div class="col-lg-6">Assessing Agency Address</div>
			  <div class="col-lg-6">
			  <input type="text" class="form-control"  name="AuditingAgencyAddress" id="AuditingAgencyAddress"
				  value="" >
			  </div>
			  </div>
			   <div class="col-lg-12">
			  <div class="col-lg-6">Contact Name</div>
			  <div class="col-lg-6">
			  <input type="text" class="form-control"  name="ContactName1" id="ContactName1" value="" >
			  </div>
			  </div>
			   <div class="col-lg-12">
			  <div class="col-lg-6">Email</div>
			  <div class="col-lg-6">
			  <input type="email" class="form-control"  name="Email" id="Email" value="" >
			</div>
			</div>
			</div>
			  </div>

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Plant Address</h3>
			</div>

			 <div class="row" style="margin-top:10px;">
			  <div class="col-lg-12">
			  <div class="col-lg-6">Plant ID/Plant Name</div>
			  <div class="col-lg-6">
			<input type="text" class="form-control"  name="VillageName" id="VillageName" value="" >
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">Country</div>
			  <div class="col-lg-6">
			   <Select name="Country" id="Country"class="form-control" >
			   <option value="">Select</option>
				 <?php $Country = $this->model->getCountry();
foreach ($Country as $row) {
    ?>
					<option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName; ?></option>
					<?php }?>

				  </select>
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">State</div>
			  <div class="col-lg-6">
				  <Select name="State" id="State"class="form-control" >
				  <option value="">Select</option>
				    <?php $State = $this->model->getState();
foreach ($State as $row) {?>
					<option value="<?php echo $row->StateID; ?>" ><?php echo $row->StateName; ?></option>
						<?php }?>
					</Select>
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">District</div>
			  <div class="col-lg-6">
				  <Select name="District" id="District"class="form-control" >
				  <option value="">Select</option>
				    <?php $District = $this->model->getDistrict();
foreach ($District as $row) {?>
				  <option value="<?php echo $row->DistrictID; ?>" ><?php echo $row->DistrictName; ?></option>
				<?php }?>
					</Select>
				</div>
				</div>


			<div class="col-lg-12">
			  <div class="col-lg-6">City</div>
			  <div class="col-lg-6">
				 <input type="text" class="form-control"  name="BlockName" id="BlockName"  value="" >
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">Ward Number</div>
			  <div class="col-lg-6">
				 <input type="text" class="form-control"  name="VillageName" id="VillageName"  value="" >
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">Pincode</div>
			  <div class="col-lg-6">
				<input type="text" class="form-control"  name="PinCode" id="PinCode" value="" >
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">Location Type </div>
			  <div class="col-lg-6">
				<input type="radio" name="LocationType" id="RUrban" value="1" checked="CHECKED">Urban
				<input type="radio" name="LocationType" id="RUrban" value="2s" >Rural
				</div>
				</div>
				</div>
			 </div>

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Plant Details</h3>
			</div>

			<div class="col-lg-12">
			  <div class="col-lg-6">Date of Establishment </div>
			  <div class="col-lg-6">
				<div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" name="EstablishmentDate"  value="">
                </div>
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">Age of Plant(Months)</div>
			  <div class="col-lg-6">
				<input type="text" class="form-control"  name="AgeOfPlant" id="AgeOfPlant"
				  value="" >
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">Plant Capacity(LPH)</div>
			  <div class="col-lg-6">
				 <Select name="PlantSpecificationID" id="PlantSpecificationID"class="form-control" >
				  <option value="">All</option>
				  <?php $PlantSpecification = $this->model->getPlantSpecification();
foreach ($PlantSpecification as $row) {
    ?>
				   <option value="<?php echo $row->PlantSpecificationID; ?>" ><?php echo $row->PlantSpecificationName; ?></option>
				  <?php }?>
				  </select>
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">Plant Manufactured By</div>
			  <div class="col-lg-6">
				<Select name="PlantManufacturerID" id="PlantManufacturerID"class="form-control" >
				  <option value="">All</option>
				  <?php $PlantManufacturer = $this->model->getPlantManufacturer();
foreach ($PlantManufacturer as $row) {

    ?>
				   <option value="<?php echo $row->PlantManufacturerID; ?>" ><?php echo $row->PlantManufacturerName; ?></option>
				  <?php }?>
				  </select>
				</div>
				</div>

			<div class="col-lg-12">
			  <div class="col-lg-6">Does Plant Have Remote Monitoring System?</div>
			  <div class="col-lg-6">
				<Select type="Select" class="form-control"  name="RemoteMonitoringSystem" id="RemoteMonitoringSystem" placeholder=" Enter Remote Monitoring System" >
				  <option value"0">No</option>
				  <option value"1">Yes</option>
				  </select>
				</div>
				</div>


				<div class="col-lg-12">
			  	<div class="col-lg-6"><b>Contaminant in Raw Water</b></div>
			  	<div class="col-lg-6"></div>
				</div>
				  <?php
               $WQC = array();
               $Contnts = $this->model->getPatWaterContaminants($token);
                 $WaterQualityChallenge = $this->model->getWaterQualityChallenge();
                    ?>
				<div class="col-lg-12">
				  <?php foreach ($WaterQualityChallenge as $key => $val) {
    ?>
				  	<div class="col-lg-3">	<input type="checkbox" value="<?php echo $val->WaterQualityChallengeID; ?>" name="WQC[]" id="WQC<?php echo $val->WaterQualityChallengeID; ?>"> <?php echo $val->WaterQualityChallengeName; ?></div>
				<?php }?>
				</div>

				<div class="col-lg-12">
			  	<div class="col-lg-6"><b>Treatment Steps</b></div>
			  	<div class="col-lg-6"></div>
				</div>
				  <?php
$Patplt = $this->model->getPlantPurStep($token);
$PlantPurificationStep = $this->model->getPlantPurificationStep();
?>

				<div class="col-lg-12">
				 <?php $i = 0;foreach ($PlantPurificationStep as $key => $row) {
    
    if ($Patplt[$i]->PlantPurificationstepID == $row->PlantPurificationStepID) {
        ?>
			  	<div class="col-lg-2">
				  <input type="checkbox" value="<?php echo $row->PlantPurificationStepID; ?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID; ?>" checked > <?php echo $row->PlantPurificationStepName; ?></div>
			  <?php } else {?>
				  	<input type="checkbox" value="<?php echo $row->PlantPurificationStepID; ?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID; ?>" > <?php echo $row->PlantPurificationStepName; ?>
				<?php }
    $i++;}?>
				</div>


			    <table  class="table table-bordered table-hover">
				<tr>
			  <th colspan="6"></th>
				  </tr>
			   <tr>
				  <th colspan="6"></th>
				  </tr>

 				 </table>
			 </div>
			 <?php //    echo "<pre>";
//print_r($data)
?>
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Assets Details</h3>
			</div>

			<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Machinery</b></td>
								<td></td>
								<td></td>
								<td> </td>
								<td><input type="text" name="MachineryCost" id="MachineryCost" size="5" class="form-control" value=""></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunder6();?>
				<?php foreach ($AssestFunder as $row) {?>
				<td><input type="radio" value="<?php echo $row->AssestFunderID; ?>" name="MachineryOther" id="MachineryOther<?php echo $row->AssestFunderID; ?>"> <b><?php echo $row->AssestFunderName; ?></b></td>
				<?php }?>
        </tr>
      </tbody>
    </table>

	<div id="machinerygrant" style="display:none;">
		<input type="text" name="grant" id="grant" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#MachineryOther2").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrant").show();
            } else {
                $("#machinerygrant").hide();
            }
        });

			$("#MachineryOther3").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrant").show();
            } else {
                $("#machinerygrant").hide();
            }
        });

});
</script>
	<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Land</b></td>
								<td></td>
								<td></td>
								<td><input type="text" name="LandCost1" id="LandCost1" size="5" class="form-control" value=""></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag2();?>
				<?php foreach ($AssestFunder as $row) {?>
<td><input type="radio" value="<?php echo $row->AssestFunderID; ?>" name="LandOther" id="LandOther<?php echo $row->AssestFunderID; ?>" > <b><?php echo $row->AssestFunderName; ?></b>
</td>
				<?php }?>
        </tr>
      </tbody>
    </table>
		<div id="landanyother" style="display:none;">
		<input type="text" name="landother" id="landother" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#LandOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#landanyother").show();
            } else {
                $("#landanyother").hide();
            }
        });
});
</script>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Building</b></td>
								<td></td>
								<td></td>
								<td><input type="text" name="BuildingCost1" id="BuildingCost1" size="5" class="form-control" value=""></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag3();?>
				<?php foreach ($AssestFunder as $row) {?>
         <td><input type="radio" value="<?php echo $row->AssestFunderID; ?>" name="BuildingOther" id="BuildingOther<?php echo $row->AssestFunderID; ?>" > <b><?php echo $row->AssestFunderName; ?></b>
</td>
				<?php }?>
        </tr>
      </tbody>
    </table>
			<div id="buildinganyOther" style="display:none;">
		<input type="text" name="BuildinganOther" id="BuildinganOther" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#BuildingOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#buildinganyOther").show();
            } else {
                $("#buildinganyOther").hide();
            }
        });
});
</script>



<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Raw Water Source</b></td>
								<td></td>
								<td></td>
								<td>
<input type="text" name="RawWaterSourceCost" id="RawWaterSourceCost" size="5" class="form-control" value="">
</td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag4();?>
				<?php foreach ($AssestFunder as $row) {?>
             <td><input type="radio" value="<?php echo $row->AssestFunderID; ?>" name="RawWaterSourceOther" id="RawWaterSourceOther<?php echo $row->AssestFunderID; ?>" > <b><?php echo $row->AssestFunderName; ?></b></td>
				<?php }?>
        </tr>
      </tbody>
    </table>
  	<div id="rawWateranyOther" style="display:none;">
		<input type="text" name="RawWaterOther" id="RawWaterOther" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#RawWaterSourceOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#rawWateranyOther").show();
            } else {
                $("#rawWateranyOther").hide();
            }
        });
});
</script>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Electricity Connection</b></td>
								<td></td>
								<td></td>
								<td>
								<input type="text" name="ElectricityCost1" id="ElectricityCost1" size="5" class="form-control" value="">
								</td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag5();?>
				<?php foreach ($AssestFunder as $row) {
    ?>
            <td>
<input type="radio" value="<?php echo $row->AssestFunderID; ?>" name="ElectricityOther" id="ElectricityOther<?php echo $row->AssestFunderID; ?>" > <b><?php echo $row->AssestFunderName; ?></b></td>
				<?php }?>
        </tr>
      </tbody>
    </table>
  	<div id="electriOther" style="display:none;">
		<input type="text" name="ElectricityanyOther" id="ElectricityanyOther" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#ElectricityOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#electriOther").show();
            } else {
                $("#electriOther").hide();
            }
        });
});
</script>
		 <table class="table table-bordered table-hover" >
			<thead>
			<tr>
				<th colspan="3">Aggregator</th>
				<th colspan="3"><input type="text" name="WaterBrandName" id="WaterBrandName" class="form-control" value=""></th>
		   </tr>
		   </tr>
		     </thead>
		   </table>
	</div>


		</div>
              <div class="tab-pane" id="tab_2">

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Social Sustainability</h3>
			</div>

			<div class="row">
			<div class="col-lg-12">
			<div class="col-lg-6">Population</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="Population" id="Population"  value="" >
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">Number of Household</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="NoOfHousehold" id="NoOfHousehold" value="" >
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">Number of Household Within 500m</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="NoOfHouseholdWithin2km" id="NoOfHouseholdWithin2km"
				  value="" >
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">Number of Household Registered</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="NoOfhhregistered" id="NoOfhhregistered"
				  value="" >
			</div>
			</div>

			<div class="col-lg-12">
			<div class="col-lg-6">Average Number of Monthly Water Cards/RFID Cards</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="Average_number_of_monthly_water_cards" id="Average_number_of_monthly_water_cards"
				  value="" >
			</div>
			</div>

			<div class="col-lg-12">
			<div class="col-lg-6">Distribution</div>
			<div class="col-lg-6">
			<Select type="Select" class="form-control"  name="Distribution" id="Distribution]">
			<?php foreach ($approval as $key => $res) {
    if ($data->Distribution == $key) {
        ?>
			<option value="<?php echo $key; ?>" SELECTED><?php echo $res; ?></option>
			<?php } else {?>
			<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
			<?php }}?>
			</select>
			</div>
			</div>
			</div>
			 <table  class="table table-bordered table-hover">

			   <tr>
          <td colspan="6">
				 <table  class="table table-bordered" >
				 <tr>
				 <th colspan="6"><div class="box box-primary">Social Composition</th>
				 </tr>
				 <tr>
				  <th >

				  <input type="checkbox"  value="1"  name="SC" id="SC"> SC</th>

					<th><input type="checkbox" value="" name="ST" id="ST"> ST<th>

					<th><input type="checkbox" value="" name="OBC" id="OBC" > OBC</th>
					<th>
					<label><input type="checkbox" value="" name="General" id="General" >General</label>
					</th>
					<th>

					<label><input type="checkbox" value="1"  name="AllInclusion" id="AllInclusion" >All Inclusion</label>
					</th>
					</tr>
				 </table>
				 <td>
			  </th>
			  </tr>
			  </div>
			  <tr>
          <th colspan="6">
				  <div class="box box-primary">Distribution/Distance Points<div>
					<table  class="table table-bordered table-hover">
					<thead>
					 <tr>
					  <th>Distribution Point</th>
					  <th>
					  <input type="hidden"  name="PlantGUID" id="PlantGUID"
					  value="" >
					  <input type="text" class="form-control"  name="DPName" id="DPName"
					  value="" ></th>
					  <th>Distance</th>
					  <th><input type="text" class="form-control"  name="Distance" id="Distance"
					  value="" ></th>

					</tr>
					</thead>
					</table>
				  </th>
			</tr>
			 </thead>
           </table>
			   </div>
		 </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
          <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Plant Production Capacity(liters)</h3>
			  </div>

						<div class="row">
							<div class="col-lg-12">
											<div class="col-lg-6">Avg Daily Water Produced in Litres</div>
											<div class="col-lg-6">
													<input type="text" class="form-control"  name="WaterProductionDaily"
				  id="WaterProductionDaily" value="" >
											</div>
							</div>

							<div class="col-lg-12">
											<div class="col-lg-6">Avg Monthly Water Production in Litres</div>
											<div class="col-lg-6">
													<input type="text" class="form-control"  name="WaterProductionMonthly" id="WaterProductionMonthly" value="" >
											</div>
							</div>


								<div class="col-lg-12">
											<div class="col-lg-6">PeakSale</div>
											<div class="col-lg-6">
												<input type="text" class="form-control"  name="PeakSale" id="PeakSale" value="" >
											</div>
							</div>
			 </div>
		</div>

              <!----  Start Here    -->
			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Kiosk Operator Details</h3>
			  </div>

			<div class="row">
<div class="col-lg-12">
        <div class="col-lg-6">Name</div>
        <div class="col-lg-6">
            <input type="text" class="form-control"  name="ContactName" id="ContactName" value="" >
        </div>
</div>


<div class="col-lg-12">
    <div class="col-lg-6">Designation</div>
        <div class="col-lg-6">
            <?php $Designation = $this->model->getDesignation();?>
                <Select type="Select" class="form-control"  name="Designation" id="Designation" >
                <?php foreach ($Designation as $row) {
    if ($row->DesignationID == $data->DesignationID) {
        ?>
                    <option value="<?php echo $row->DesignationID; ?>" SELECTED><?php echo $row->DesignationName; ?></option>
                <?php } else {?>
                    <option value="<?php echo $row->DesignationID; ?>"><?php echo $row->DesignationName; ?></option>
                <?php }}?>
                </select>
        </div>
</div>

<div class="col-lg-12">
    <div class="col-lg-6">Contact Number</div>
    <div class="col-lg-6">
    <input type="text" class="form-control"  name="ContactNumber" id="ContactNumber" value="" >
    </div>
</div>

<div class="col-lg-12">
    <div class="col-lg-6">Education Level</div>
        <div class="col-lg-6">
            <Select type="Select" class="form-control"  name="educationlevel" id="educationlevel" >
                <option value="">SELECT</option>
                <option value="primary">Primary</option>
                <option value="secpndary">Secondary</option>
                <option value="graduate">Graduate</option>
                <option value="any Other">Any Other</option>
								</select>
            </div>
	</div>
	</div>

</div>
			 <!----- End here -->

			 <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Daily Water Quality Monitoring</h3>
			  </div>

		<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">UV(Ultra-violet)</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="UV" id="UV" >

								<option value="0">NO</option>
								<option value="1" SELECTED>Yes</option>

							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">TDS(Total Dissolved Solids)</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="TDS" id="TDS" >
								<option value="0">NO</option>
								<option value="1" >Yes</option>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">pH(Potential of Hydrogen)</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="pH" id="pH" >
								<option value="0" >NO</option>
								<option value="1">Yes</option>
							</select>
							</div>
					</div>
					<div class="col-lg-12">
							<div class="col-lg-6">Residual Chlorine</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Residual_Chlorine" id="Residual_Chlorine" >
								<option value="0">NO</option>
								<option value="1">Yes</option>
							</select>
							</div>
					</div>

						<div class="col-lg-12">
							<div class="col-lg-6">Microbial</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Microbial" id="Microbial" >
								<option value="0" SELECTED>NO</option>
								<option value="1">Yes</option>
							</select>
							</div>
					</div>

				</div>
				</div>


			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training and Capacity Building</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>

			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training Recieved</h3>
			  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->ElectricalSafety; ?>" name="ElectricalSafety" id="ElectricalSafety"
					  <?php if ($data->ElectricalSafety == '0') {echo "";} else {echo "CHECKED";}?>>
                     Electrical Safety
                    </label>
                  </div>
                   <div class="checkbox">
                    <label>
                       <input type="checkbox" value="<?php echo $data->PlantOM; ?>" name="PlantOM" id="PlantOM"
					   <?php if ($data->PlantOM == '0') {echo "";} else {echo "CHECKED";}?>>
                      Plant Operation and Managment
                    </label>
                  </div>
				<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->WaterQuality; ?>" name="WaterQuality" id="WaterQuality" <?php if ($data->WaterQuality == '0') {echo "";} else {echo "CHECKED";}?>>
                      Water Quality
                    </label>
                  </div>
				<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->ConsumerAwareness; ?>" name="ConsumerAwareness" id="ConsumerAwareness" <?php if ($data->ConsumerAwareness == '0') {echo "";} else {echo "CHECKED";}?> >
                      Consumer Awareness
                    </label>
                  </div>
					<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->ABKeeping; ?>" name="ABKeeping" id="ABKeeping"<?php if ($data->ABKeeping == '0') {echo "";} else {echo "CHECKED";}?> >
                     Accounts and Book Keeping
                    </label>
                  </div>

			 </div>



			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Quality Assurance and Hygiene</h3>
			  </div>


					<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">Earthing/Grounding</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Earthing" id="Earthing" >
								<option value="0" >Not Done</option>
								<option value="1">Done</option>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">Appearance</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Appearance" id="Appearance" >
								<option value="0" SELECTED>Not-Agreeable</option>
								<option value="1">Agreeable</option>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">Odour</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Odour" id="Odour" >
								<option value="0" >Not-Agreeable</option>
								<option value="1">Agreeable</option>
							</select>
							</div>
					</div>
					<div class="col-lg-12">
							<div class="col-lg-6">Taste</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Taste" id="Taste" >
								<option value="0" SELECTED>Not-Agreeable</option>
								<option value="1">Agreeable</option>
							</select>
							</div>
					</div>

						<div class="col-lg-12">
							<div class="col-lg-6">TDS VALUE</div>
							<div class="col-lg-6">
							<input type="number" class="form-control" name="TDSValue" id="TDSValue" value="<?php echo number_format($data->TDSValue, 2, '.', ','); ?>">
							</div>
					</div>

					<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Raw Water Source</div>
							<div class="col-lg-6">Open Well Covered</div>
							<div class="col-lg-6">
						<select class="form-control" name="RawWaterOpenwellcovered" id="RawWaterOpenwellcovered">
                <?php foreach ($approval as $key => $res) {?>
                    <option value="<?php echo $key; ?>" ><?php echo $res; ?></option>
            	<?php }?>
                 </select>
							</div>
					</div>

		<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Bore Well Casing</div>
							<div class="col-lg-6">
					 <select class="form-control" name="RawWaterBoreWellCasing" id="RawWaterBoreWellCasing">
       				<?php foreach ($approval as $key => $res) {?>
          			<option value="<?php echo $key; ?>" ><?php echo $res; ?></option>
					   <?php }?>
                 </select>
							</div>
					</div>

	<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Treated water tank</div>
							<div class="col-lg-6">Inside the Plant</div>
							<div class="col-lg-6">
					 <select class="form-control" name="Insidetheplant" id="Insidetheplant">
						<?php foreach ($approval as $key => $res) {?>
						<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
						<?php }?>
                 </select>

							</div>
					</div>

			<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Covered</div>
							<div class="col-lg-6">
				 <select class="form-control" name="Covered" id="Covered">
             		<?php foreach ($approval as $key => $res) {?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
				<?php }?>
                 </select>
				</div>
			</div>


					<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Plant Hygiene Inside</div>
							<div class="col-lg-6">Cleanliness in Plant</div>
							<div class="col-lg-6">
				 <select class="form-control" name="CleanlinessinPlant" id="CleanlinessinPlant">
            <?php foreach ($approval as $key => $res) {?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
			<?php }?>
                 </select>
				</div>
			</div>


				<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Leakage</div>
							<div class="col-lg-6">
			<select class="form-control" name="Leakage" id="Leakage">
            <?php foreach ($approval as $key => $res) {?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
			<?php }?>
                 </select>
							</div>
					</div>

		<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Plant Hygiene Outside</div>
							<div class="col-lg-6">Cleanliness Near Treatment Plant</div>
							<div class="col-lg-6">
		 <select class="form-control" name="CleanlinessNearTreatmentPlant" id="CleanlinessNearTreatmentPlant">
            <?php foreach ($approval as $key => $res) {?>
                  <option value="<?php echo $key; ?>"><?php echo $res; ?></option>
			<?php }?>
                 </select>
							</div>
					</div>

			<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Water Fill Station</div>
							<div class="col-lg-6">Moss/Algae Growth</div>
							<div class="col-lg-6">
		  <select class="form-control" name="Checkmossoralgee" id="Checkmossoralgee">
             <?php foreach ($approval as $key => $res) {?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
			<?php }?>
                 </select>
							</div>
					</div>


			<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Presence of Puddle</div>
							<div class="col-lg-6">
		 <select class="form-control" name="Presenceofpuddle" id="Presenceofpuddle">
            <?php foreach ($approval as $key => $res) {?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
			<?php }?>
                 </select>
							</div>
					</div>
		</th>
</tr>

<tr>
	<th>
<div class="form-group">Pre Monsoon
<div class="checkbox">
<label>
<input type="checkbox"<?php if ($data->PreMonsoonRWTesting == 1) {?>value="<?php echo $data->PreMonsoonRWTesting; ?>" CHECKED <?php } else {?>value="PRERW" <?php }?> name="PreMonsoonRWTesting" id="PreMonsoonRWTesting">
	Raw Water
</label>
</div>
<?php if ($data->PreMonsoonRWTesting == 1) {?>
<div id="dvpreRWTesting">
<?php } else {?>
<div id="dvpreRWTesting" style="display: none">
<?php }?>
<div class="row">
<div class="col-sm-12"><input type="text" class="form-control" Placeholder="Address"  name="PreMonsoonRWLabAdr" id="PreMonsoonRWLabAdr" value="" size="50" ></div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div><input type="text" class="form-control datepicker"  name="PreMonsoonRWTestingdate" id="PreMonsoonRWTestingdate" value="<?php
if ($data->PreMonsoonRWTestingdate != '1970-01-01 00:00:00') {
    echo Date("d-m-Y", strtotime($data->PreMonsoonRWTestingdate));}?>" size="5">
				</div>
		</div>
	</div>
<div class="col-sm-6">
	<input type="file" class="form-control"  name="PreMonsoonRWTestProof" id="PreMonsoonRWTestProof" value="" >
</div>
</div>
<div>
	<?php if ($data->PreMonsoonRWTestProof != '') {?>
					<input type="hidden" class="form-control"  name="oldPreMonsoonRWTestProof" id="PreMonsoonRWTestProof" value="" >
						<img src="<?php echo site_url() . "datafiles/" . $data->PreMonsoonRWTestProof; ?>" hight="100px" width="100px">
						<?php }?>
	</div>
</div>
</div>

	<div class="checkbox">
			<label>
				<input type="checkbox" <?php if ($data->PreMonsoonTWTesting == 1) {?>value="<?php echo $data->PreMonsoonTWTesting; ?>" CHECKED <?php } else {?>value="PRETWT" <?php }?> name="PreMonsoonTWTesting" id="PreMonsoonTWTesting">
				Treated Water
			</label>
		</div>
		<?php if ($data->PreMonsoonTWTesting == 1) {?>
				<div id="dvpretretTWtesting" >
		<?php } else {?>
			<div id="dvpretretTWtesting" style="display: none">
		<?php }?>

<div class="row">
<div class="col-sm-12">
<input type="text" class="form-control" Placeholder="Address"  name="PreMonsoonTWLabAdr" id="PreMonsoonTWLabAdr" value="<?php echo $data->PreMonsoonTWLabAdr; ?>" size="50" >
	</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
								<input type="text" class="form-control datepicker"  name="PreMonsoonTWTestingdate" id="PreMonsoonTWTestingdate" value="<?php
if ($data->PreMonsoonTWTestingdate != '1970-01-01 00:00:00') {
    echo Date("d-m-Y", strtotime($data->PreMonsoonTWTestingdate));}?>" size="10">
				</div>
		</div>
	</div>
<div class="col-sm-6">
	<input type="file" class="form-control "  name="PreMonsoonTWProof" id="PreMonsoonTWProof" value="" size="10">
</div>
</div>
<div>
	<?php if ($data->PreMonsoonTWProof != '') {?>
					<input type="hidden" class="form-control"  name="oldPreMonsoonTWProof" id="PreMonsoonTWProof" value="<?php echo $data->PreMonsoonRWTestProof; ?>" >
						<img src="<?php echo site_url() . "datafiles/" . $data->PreMonsoonTWProof; ?>" hight="100px" width="100px">
						<?php }?>
	</div>
				</div>

 </th>
</tr>

<tr>
	<th>
	<div class="form-group">Post Monsoon
		<div class="checkbox">
		<label>
	<input type="checkbox" <?php if ($data->PostMonsoonRWTesting == 1) {?>value="<?php echo $data->PostMonsoonRWTesting; ?>" CHECKED <?php } else {?>value="POSTRW" <?php }?> name="PostMonsoonRWTesting" id="PostMonsoonRWTesting">
	Raw Water
</label>
</div>
<?php if ($data->PostMonsoonRWTesting == 1) {?>
				<div id="dvRWTesting" >
		<?php } else {?>
			<div id="dvRWTesting" style="display: none">
		<?php }?>

<div class="row">
<div class="col-sm-12">
<input type="text" class="form-control" Placeholder="Address"  name="PostMonsoonRWLabadr" id="PostMonsoonRWLabadr" value="<?php echo $data->PostMonsoonRWLabadr; ?>" size="50" >
	</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
								<input type="text" class="form-control datepicker"  name="PostMonsoonRWtestingdate" id="PostMonsoonRWtestingdate" value="<?php
if ($data->PostMonsoonRWtestingdate != '1970-01-01 00:00:00') {
    echo Date("d-m-Y", strtotime($data->PostMonsoonRWtestingdate));}?>" >
				</div>
		</div>
	</div>
<div class="col-sm-6">
<input type="file" class="form-control "  name="PostMonsoonRWTestProof" id="PostMonsoonRWTestProof" value="" size="10">
</div>
</div>
	<?php if ($data->PostMonsoonRWTestProof != '') {?>
					<input type="hidden" class="form-control"  name="oldPostMonsoonRWTestProof" id="PostMonsoonRWTestProof" value="<?php echo $data->PostMonsoonRWTestProof; ?>" >
						<img src="<?php echo site_url() . "datafiles/" . $data->PostMonsoonRWTestProof; ?>" hight="100px" width="100px">
						<?php }?>
	</div>

		</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" <?php if ($data->PostMonsoonTWtesting == 1) {?> value="<?php echo $data->PostMonsoonTWtesting; ?>" CHECKED <?php } else {?>value="POSTTWT" <?php }?> name="PostMonsoonTWtesting" id="PostMonsoonTWtesting">
			Treated Water
		</label>
	</div>
	<?php if ($data->PostMonsoonTWtesting == 1) {?>
	<div id="dvPostTWtesting">
	<?php } else {?>
	<div id="dvPostTWtesting" style="display: none">
	<?php }?>

<div class="row">
<div class="col-sm-12">
<input type="text" class="form-control" Placeholder="Address"  name="PostMonsoonTWLabAdr" id="PostMonsoonTWLabAdr" value="<?php echo $data->PostMonsoonTWLabAdr; ?>" size="50" >
</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
								<input type="text" class="form-control datepicker"  name="PostMonsoonTWtestingdate" id="PostMonsoonTWtestingdate" value="<?php
if ($data->PostMonsoonTWtestingdate != '1970-01-01 00:00:00') {
    echo Date("d-m-Y", strtotime($data->PostMonsoonTWtestingdate));}?>" size="10">
				</div>
		</div>
	</div>
<div class="col-sm-6">
<input type="file" class="form-control "  name="PostMonsoonTWtestProof" id="PostMonsoonTWtestProof" value="" >
</div>
</div>
<div>
	<?php if ($data->PostMonsoonTWtestProof != '') {?>
					<input type="hidden" class="form-control"  name="oldPostMonsoonTWtestProof" id="PostMonsoonTWtestProof" value="<?php echo $data->PostMonsoonTWtestProof; ?>" >
						<img src="<?php echo site_url() . "datafiles/" . $data->PostMonsoonTWtestProof; ?>" hight="100px" width="100px">
						<?php }?>
	</div>

</div>
<div class="checkbox">
				<label>
					<input type="checkbox" <?php if ($data->RejectWaterTesting == 1) {?> value="<?php echo $data->RejectWaterTesting; ?>" CHECKED<?php } else {?>value="REJWT" <?php }?>name="RejectWaterTesting" id="RejectWaterTesting">
					Reject Water
				</label>
	</div>

<?php if ($data->RejectWaterTesting == 1) {?>
<div id="dvRejectTWtesting" >
<?php } else {?>
<div id="dvRejectTWtesting" style="display: none">
<?php }?>

<div class="row">
<div class="col-sm-12">
	<input type="text" class="form-control" Placeholder="Address"  name="RejectWaterLabAdr" id="RejectWaterLabAdr" value="<?php echo $data->RejectWaterLabAdr; ?>" size="50" >
</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>	<input type="checkbox" <?php if ($data->PostMonsoonTWtesting == 1) {?> value="<?php echo $data->PostMonsoonTWtesting; ?>" CHECKED <?php } else {?>value="POSTTWT" <?php }?> name="PostMonsoonTWtesting" id="PostMonsoonTWtesting">
							<input type="text" class="form-control datepicker"  name="RejectWatertestingdate" id="RejectWatertestingdate" value="<?php
if ($data->RejectWatertestingdate != '1970-01-01 00:00:00') {
    echo Date("d-m-Y", strtotime($data->RejectWatertestingdate));}?>" size="10">
				</div>
		</div>
	</div>
<div class="col-sm-6">
<input type="file" class="form-control "  name="RejectWaterTestProof" id="RejectWaterTestProof" value=""
</div>
</div>
<div>
<?php if ($data->RejectWaterTestProof != '') {?>
	<input type="hidden" class="form-control"  name="oldRejectWaterTestProof" id="RejectWaterTestProof" value="<?php echo $data->RejectWaterTestProof; ?>" >
		<img src="<?php echo site_url() . "datafiles/" . $data->RejectWaterTestProof; ?>" hight="100px" width="100px">
		<?php }?>
	</div>
		</div>

			  		</div>
					</div>
</div>

   </th>
</tr>

<tr>
<th>

<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Reliability of Operations</h3>
			  </div>
			<div class="row">
			<div class="col-lg-12">
			  <div class="col-sm-6">Technical Downtime</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="TechnicalDowntime" id="TechnicalDowntime">
				<option value="0" >NO</option>
				<option value="1">Yes</option>

			   </select>
				  </div>
			  </div>
				</div>

			  <div class="row">
				<div class="col-lg-12">
			  <div class="col-sm-6">No of Days In Last 30 days</div>
			  <div class="col-sm-6">
			  <input type="text" class="form-control" name="NoOfDays" id="NoOfDays" value="">
				  </div>
					</div>
			  </div>

				<div class="row" >
				<div class="col-lg-12">
			  	<div class="col-sm-6"> Reason Of Downtime</div>
				</div>
				</div>
			  <div class="row" style="background-color: #E0E0E0;">
			<div class="col-lg-12">
			  <div class="col-sm-6"> </div>
			  <div class="col-sm-6"></div>
					</div>
				<div class="col-lg-12">
			  <div class="col-sm-6">
					<input type="checkbox" value=""  name="MotorRepair" id="MotorRepair">
				Motor Repair</div>
			  <div class="col-sm-6">
				<input type="checkbox" value="" name="MembraneChoke" id="MembraneChoke">
				Membrane Choke</div>
				</div>
					<div class="col-lg-12">
			  <div class="col-sm-6">
					<input type="checkbox" value=""  name="Rawwaterproblem" id="Rawwaterproblem">
				Row Water Problem</div>
			  <div class="col-sm-6">
				<input type="checkbox" value=""  name="Electricityoutage" id="Electricityoutage">
				Electricity Outage</div>
				</div>
					<div class="col-lg-12">
			  <div class="col-sm-6">
					<input type="checkbox" value=""  name="Anyother" id="Anyother">
				Any Other</div>
			<div class="col-sm-6">
			 <?php if ($data->Anyother == 1) {?>
				<div id="dayAnyother" >
				<?php } else {?>
				<div id="dayAnyother"  style="display:none;">
				<?php }?>
				<input type="text" name="rowAnyotherEdit" id="rowAnyotherEdit" class="form-control" value="">
		</div>
				</div>
				</div>
			  </div>

			<script type="text/javascript">
				$(function () {
						$("#Anyother").click(function () {
						if ($(this).is(":checked")) {
							$("#dayAnyother").show();
						} else {
							$("#dayAnyother").hide();
						}
					});
			});
			</script>
			  <div class="row" >
					<div class="col-lg-12">
			  <div class="col-sm-6" style="margin-top:10px;">Sales day Lost</div>
			  <div class="col-sm-6" style="margin-top:10px;">
			  <input type="text" class="form-control" name="SalesDayLost" id="SalesDayLost" value="">

				  </div>
					<div>
			  </div>
				</div>
			</div>
		</div>

</th>
	</tr>

    </table>
			 </div>


           </div>

<script type="text/javascript">
    $(function () {
			$("#PreMonsoonRWTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvpreRWTesting").show();
            } else {
                $("#dvpreRWTesting").hide();
            }
        });

			$("#PreMonsoonTWTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvpretretTWtesting").show();
            } else {
                $("#dvpretretTWtesting").hide();
            }
        });

        $("#PostMonsoonRWTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvRWTesting").show();
            } else {
                $("#dvRWTesting").hide();
            }
        });

				$("#PostMonsoonTWtesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPostTWtesting").show();
            } else {
                $("#dvPostTWtesting").hide();
            }
        });
					$("#RejectWaterTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvRejectTWtesting").show();
            } else {
                $("#dvRejectTWtesting").hide();
            }
        });

 });
</script>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_4">

			<div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Capital Expenditure</h3>
			  </div>

			<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6">Machinery Cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="MachineryCost1" id="MachineryCost1" value="" ></div>
			</div>

			<div class="col-lg-12">
			<div class="col-sm-6">Raw water Source cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="RawWaterSourceCost1" id="RawWaterSourceCost1" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Building Cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="BuildingCost" id="BuildingCost" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Land Cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="LandCost" id="LandCost" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Electricity Connection Cost</div>
			<div class="col-sm-6"> <input type="text" class="form-control"  name="ElectricityCost" id="ElectricityCost" value="" ></div>
			</div>

			<div class="col-lg-12">
			<div class="col-sm-6">Total CapEx</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="Total_cost" id="Total_cost" value="" ></div>
			</div>

		 </div>


			</div>
		 <?php
$ProdDetail = $this->model->getPatPlantProdDetail($token);
?>
		 <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Revenue</h3>
			  </div>
			  <table class="table table-bordered table-hover">
			  <thead>
			  <tr>
				<td></td>
				<th>Price/Ltr</th>
				<th>Vol in Ltr</th>
				<th>Total</th>
			  </tr>
			</thead>

			<tr>
				<th>At Plant</th>
				<td></td>
				<td></td>
				<td>	</td>
			  </tr>

			<tr>
				<th>Home Delivery</th>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			<tr>
				<th>DP1</th>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			</table>
		 </div>


		  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Operating Expense (Per Months)</h3>
			  </div>
				<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6">Raw Water Bill(If Any)</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="WaterBill" id="WaterBill" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Rent of Station</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="Rent" id="Rent" value=""></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Transport Expense</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="TransportExpense" id="TransportExpense" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Chemicals and Other Consumables</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="ChemicalAndOtherConsumable" id="ChemicalAndOtherConsumable"
				  value="" ></div>
			</div>
				<div class="col-lg-12">
			<div class="col-sm-6">Electricity Bill</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="ElectricityBill" id="ElectricityBill" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Operator Salary</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OperatorSalary" id="OperatorSalary" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Generator Expense</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="Electricityoutage" id="Electricityoutage" value="" ></div>
			</div>
			</div>



			<div class="box box-primary">
			<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6"><input type="checkbox" name="Miscellaneous" id="Miscellaneous" value="">
									Miscellaneous/Any other</div>
			<div class="col-sm-6"></div>
			</div>
			<div id="Miscellanyoth" style="display:none">
			<div class="col-lg-12">
			<div class="col-sm-2">S No.</div>
			<div class="col-sm-5">Expense</div>
			<div class="col-sm-5">Amount</div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-2">1</div>
			<div class="col-sm-5"><input type="text" name="Expense1" id="Expense1" value=""></div>
			<div class="col-sm-5"><input type="number" name="Amount1" id="Amount1" value=""></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-2">2</div>
			<div class="col-sm-5"><input type="text" name="Expense2" id="Expense2" value=""></div>
			<div class="col-sm-5"><input type="number" name="Amount2" id="Amount2" value=""></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-2">3</div>
			<div class="col-sm-5"><input type="text" name="Expense3" id="Expense3" value=""></div>
			<div class="col-sm-5"><input type="number" name="Amount3" id="Amount3" value=""></div>
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Monthly Service/ Maintenance Charge</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="ServiceCharge" id="ServiceCharge" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Asset Renewal Fund</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="AssestRenewalFund" id="AssestRenewalFund" value="" ></div>
			</div>
			</div>
			</div>
			<script type="text/javascript">
				$(function () {
						$("#Miscellaneous").click(function () {
							if ($(this).is(":checked")) {
							$("#Miscellanyoth").show();
						} else {
							$("#Miscellanyoth").hide();
						}
					});
					});
			</script>

			<div class="box box-primary">
			<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6">  Financial Sustainability</div>
			<div class="col-sm-6"></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">OpEx</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OPEx" id="OPEx" value="" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">OpEx + Service/Maintenance Charge</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OPExservicecharge" id="OPExservicecharge" value=""></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">OpEx + Service / Maintenance Charge + Asset Renewal Fund</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OPExservicechargemaintenance" id="OPExservicechargemaintenance" value="" ></div>
			</div>
			</div>
			</div>

						</div>
				</div>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_5">

			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Institutional Sustainability</h3>
			  </div>

					<div class="row">
					<div class="col-lg-12">
									<div class="col-lg-6"><b>Approved Yes/No</b></div>
									<div class="col-lg-6"><b>Approved Yes/No</b></div>
					</div>


						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Local Government Approval</div>
									<div class="col-lg-6">
									<select class="form-control" name="GramPanchayatApproval" id="GramPanchayatApproval">
										<?php foreach ($approval as $key => $res) {?>
										<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
										<?php }?>
                					 </select>
									</div>
								</div>


						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Reject Water Discharge</div>
									<div class="col-lg-6">
								 <select class="form-control" name="reject_water_discharge" id="reject_water_discharge">
                    <?php foreach ($approval as $key => $res) {
    ?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
					<?php }?>
                 </select>
									</div>
					</div>


							<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Legal Electricity Connection</div>
									<div class="col-lg-6">
								 <select class="form-control" name="LegalElectricityConnection" id="LegalElectricityConnection">
                   				 <?php foreach ($approval as $key => $res) {?>
									<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
								<?php }?>
                 			</select>
									</div>
					</div>

						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Trained Operator</div>
									<div class="col-lg-6">
								 <select class="form-control" name="Trained_Operator" id="Trained_Operator">
								<?php foreach ($approval as $key => $res) {?>
								<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
								<?php }?>
							</select>
							</div>
					</div>

					<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Premises and Land</div>
									<div class="col-lg-6">
								 <select class="form-control" name="LandApproval" id="LandApproval">
                   <?php foreach ($approval as $key => $res) {?>
					<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
					<?php }?>
                 	</select>
					</div>
					</div>


						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Raw Source Water</div>
									<div class="col-lg-6">
								  <select class="form-control" name="RawWaterSourceApproval" id="RawWaterSourceApproval">
										<?php foreach ($approval as $key => $res) {?>
										<option value="<?php echo $key; ?>"><?php echo $res; ?></option>
										<?php }?>
                 </select>
									</div>
					</div>

						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Document Upload</div>
									<div class="col-lg-6">
								 <div class="form-group">
						<input type="hidden" name="oldmainimage" id="oldmainimage" value="">
				    <input type="file" name="fileupload" id="exampleInputFile" value="amit" class="form-control"> </div>
									</div>
					</div>


						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6"></div>
									<div class="col-lg-6">

				<img src="<?php echo base_url() . "datafiles/noimage.jpg"; ?>" width="150px" height="150px" alt="">

						</div>
					</div>
					</div>

			 </div>
		</div>
			  <!-- /.tab-pane -->
	<div class="tab-pane" id="tab_6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Enviromental Sustainability</h3>
			</div>

				<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">
							 <input type="checkbox" name="checkRWQuantification" id="checkRWQuantification"<?php if ($data->checkRWQuantification == 0) {
    echo "";} else {echo "CHECKED";}?>  >
                    Reject water Produced
							</div>
							<div class="col-lg-6">
							<input type="text" class="form-control"  name="WaterProductionDaily" id="WaterProductionDaily" value="" ></div>
					</div>
				</div>
		</div>


		<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">
							 <input type="checkbox" value="<?php echo $data->CheckRWUtilisation; ?>" name="CheckRWUtilisation" id="CheckRWUtilisation">
                Reject Water Utilised
							</div>
							<div class="col-lg-6"></div>
					</div>

				<div id="dvUtilizActive" style="display:none;">
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"> <b>Activity</b></div>
				<div class="col-lg-4"> <b>Litres/Day</b></div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity1" id="UtilizationActivity1" value="" ></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationOther1" id="UtilizationOther1"  value="" >	</div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity2" id="UtilizationActivity2" value="" ></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationOther2" id="UtilizationOther2"  value="" >	</div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity3" id="UtilizationActivity3" value="" ></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationOther3" id="UtilizationOther3"  value="" >	</div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity4" id="UtilizationActivity4" value="" ></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationOther4" id="UtilizationOther4"  value="" >	</div>
				</div>

				</div>


					<div class="col-lg-12">
							<div class="col-lg-6" style="margin-top:10px;">
							<input type="checkbox" value="<?php echo $data->CheckRWDisposal; ?>" name="CheckRWDisposal" id="CheckRWDisposal"<?php if ($data->CheckRWDisposal == 0) {
    echo "";} else {echo "CHECKED";}?>>
                   Reject water Disposed
							</div>
							<div class="col-lg-6" style="margin-top:10px;">
							<input type="text" class="form-control"  name="CheckRWDisposal" id="CheckRWDisposal" value="<?php echo $data->CheckRWDisposal; ?>" ></div>
					</div>
				</div>


		<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">

							<input type="checkbox" value="<?php echo $data->CheckRWDisposal; ?>" name="CheckRWDisposal" id="CheckRWDisposal"<?php if ($data->CheckRWDisposal == 0) {
    echo "";} else {echo "CHECKED";}?>>Ground Water Recharge / Rain Water Harvesting</div>
							<div class="col-lg-6"> </div>
					</div>
				</div>
		</div>

  <script type="text/javascript">
    $(function () {
			$("#CheckRWUtilisation").click(function () {
			      if ($(this).is(":checked")) {
                $("#dvUtilizActive").show();
            } else {
                $("#dvUtilizActive").hide();
            }
        });
		});
</script>

	</div>
	</div>
             <div class="row">
              	<div class="col-lg-12">

 <?php print form_submit($this->router->method, 'Submit', array('id' => $this->router->method, 'class' => 'btn btn-info pull-right'))?>
     		<?php print form_close();?>
              	</div>
              </div>
			  </div>
              <!-- /.tab-pane -->
            </div>

            <!-- /.tab-content -->
          </div>

          <!-- nav-tabs-custom -->
        </div>

		</div>

</div>
</div>






    <!-- /.content -->