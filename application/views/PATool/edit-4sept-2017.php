
  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

  <script type="text/javascript">
  /* Formatting function for row details - modify as you need */
function format (d) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            "<td style='padding-left:50px;'><a href='<?php echo site_url(); ?>PATool/edit/"+d.PlantGUID+"'>Eval_1</a></td>"+
            
        '</tr>'+
      
    '</table>';
} 
$(document).ready(function() {
    var table = $('#example').DataTable( {
       "ajax": "<?php echo site_url();?>/Patajax/getSWNID",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "SWNID" }
           
        ],
        "order": [[1, 'asc']]
    } );
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it*
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );

</script>
<style type="text/css">
  td.details-control {
    background: url('<?php print FIMAGES;?>details_open.png') no-repeat center center;
    background-size: 20px 20px;
    padding-top: 40px;    
}
tr.shown td.details-control {
    background: url('<?php print FIMAGES;?>details_close.png') no-repeat center center;
    background-size: 20px 20px;
    padding-top: 20px;
}  
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<div class="row">
		<div class="col-md-3" style="padding-top: 5px;  height: 500px; overflow-y: scroll;">
		<label style="background-color: #674e9e; width: 600px; color: #ffffff; padding-left:10px;" >Operations</label>

          <table id="example" class="display" width="100%" cellspacing="0">
					<thead>
					    <tr>
					        <th style="width: 30px;"></th>
					        <th  style="padding-left: 10px;">SWNID</th>
					    </tr>
					</thead>
					<tfoot>
					    <tr>
					        <th style="width:30px;"></th>
					        <th style="padding-left: 10px;">SWNID</th>
					    </tr>
					</tfoot>
			</table>
		</div>
		<div class="col-md-9" style="padding-top: 5px;  height: 500px; overflow-y: scroll;">
		 <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
			   <input type="hidden" class="form-control"  name="PlantUID" id="PlantUID" 
				  value="<?php  echo $data->PlantUID;?>" >
				  <input type="hidden" class="form-control"  name="PlantGUID" id="PlantGUID" 
				  value="<?php echo $token;?>" >
				 <input type="hidden" class="form-control"  name="PlantPOUID" id="PlantPOUID" 
				  value="<?php  echo $data->PlantPOUID;?>" >	
		<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
			  <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
      		  <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>
		   
		   <?php 
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		     $data = $this->model->getDetail($token);
		  	 $approval=array('0'=>'No', '1'=>'Yes');
			 $Earthing=array('0'=>'Not Done', '1'=>'Done');
			// echo "<pre>";
			 //print_r($data);
		?>
			
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			 

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Assessors details</h3>
			  </div>

			<div class="row">
			 <div class="box-header with-border">
              <h5 class="box-title"><b>Location:</b></h5>
			</div>
			<div class="row" style="margin-top:10px;">
			<div class="col-lg-12" >
			  <div class="col-lg-6">Latitude</div>
			  <div class="col-lg-6"><input type="text" class="form-control"  name="Latitude" id="Latitude"  value="<?php echo $data->Latitude;?>"></div>
			</div>
				
			<div class="col-lg-12" style="margin-top:10px;">
			 <div class="col-lg-6">Longitude</div>
     		 <div class="col-lg-6">
			 <input type="text" class="form-control"  name="Longitude" id="Longitude" value="<?php echo $data->Longitude;?>"></div>
			</div>
			</div>
			  
			  <div class="col-lg-12">
			  <div class="col-lg-6">Visit Date</div>
			  <div class="col-lg-6" style="margin-top:10px;">
			  <div class="form-group">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
				</div>
				<input type="text" class="form-control pull-right datepicker" value="<?php  echo date("d-m-Y", strtotime($data->VisitDate));?>" 
				name="VisitDate" >
				</div>
			</div>
			</div>
			  </div>
			
		<div class="col-lg-12">
			<div class="col-lg-6">Assessed by</div>
				<div class="col-lg-6">
				<Select name="UserID" id="UserID"class="form-control" >
					<option value="">Select</option>
					<?php $Assessedby = $this->model->getAssessedby();
					foreach($Assessedby as $row){
					if($row->UserID==$data->UserID){
					?>
					<option value="<?php echo $row->UserID; ?>" SELECTED><?php echo $row->UserName;?></option>
					<?php }else{ ?>
					<option value="<?php echo $row->UserID; ?>" ><?php echo $row->UserName; ?></option>
					<?php  }} ?>
					</select>
				</div>
			</div>
			   
			  <div class="col-lg-12">
			  <div class="col-lg-6">Assessing agency</div>
			  <div class="col-lg-6">
			   <Select name="AuditingAgency" id="AuditingAgency"class="form-control" >
				  <option value="">Select</option>
				  <?php $Agency = $this->model->getAgency();
				  foreach($Agency as $row){
					if($row->AgencyID==$data->AuditingAgency){
				  ?>
				  <option value="<?php echo $row->AgencyID; ?>" SELECTED><?php echo $row->AgencyName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->AgencyID; ?>" ><?php echo $row->AgencyName;?></option>
				  <?php  }} ?>
				  </select>
			  </div>
			  </div>
			  
			  <div class="col-lg-12">
			  <div class="col-lg-6">Assessing address</div>
			  <div class="col-lg-6">
			  <input type="text" class="form-control"  name="AuditingAgencyAddress" id="AuditingAgencyAddress"
				  value="<?php echo $data->AuditingAgencyAddress;?>" >
			  </div>
			  </div>
			   <div class="col-lg-12">
			  <div class="col-lg-6">Contact name</div>
			  <div class="col-lg-6">
			  <input type="text" class="form-control"  name="ContactName" id="ContactName" value="<?php echo $data->ContactName;?>" >
			  </div>
			  </div>
			   <div class="col-lg-12">
			  <div class="col-lg-6">Email</div>
			  <div class="col-lg-6">
			  <input type="email" class="form-control"  name="Email" id="Email" value="<?php echo $data->Email;?>" >
			</div>
			</div>
			</div>
			  </div>

			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Plant Address</h3>
			</div>

			 <div class="row" style="margin-top:10px;">
			  <div class="col-lg-12">
			  <div class="col-lg-6">PlantID/Plant name</div>
			  <div class="col-lg-6">
				<input type="text" class="form-control"  name="SWNID" id="SWNID" value="<?php echo $data->SWNID;?>" >
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">Country</div>
			  <div class="col-lg-6">
			   <Select name="Country" id="Country"class="form-control" >
				 <?php $Country = $this->model->getCountry();
					foreach($Country as $row){
					if($row->CountryID==$data->CountryID){
				  ?>
				  <option value="<?php echo $row->CountryID; ?>" SELECTED><?php echo $row->CountryName;?></option>
					<?php }else{ ?>
					<option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>
					<?php }} ?>
				   
				  </select>
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">State</div>
			  <div class="col-lg-6">
				  <Select name="State" id="State"class="form-control" >
				    <?php $State = $this->model->getState();
						foreach($State as $row){
						if($row->StateID == $data->StateID){
					?>
				  <option value="<?php echo $row->StateID;?>" selected= "SELECTED"><?php echo $row->StateName;?></option>
					<?php }else{?>
					<option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
					<?php } } ?>
					</Select>
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">District</div>
			  <div class="col-lg-6">
				  <Select name="District" id="District"class="form-control" >
				    <?php $District = $this->model->getDistrict();
					foreach($District as $row){
					if($row->DistrictID==$data->District){
				  ?>
				  <option value="<?php echo $row->DistrictID; ?>" SELECTED><?php echo $row->DistrictName;?></option>
				<?php }else{ ?>
				<option value="<?php echo $row->DistrictID; ?>"><?php echo $row->DistrictName;?></option>
				<?php } } ?>
					</Select>
				</div>
				</div>


			<div class="col-lg-12">
			  <div class="col-lg-6">City</div>
			  <div class="col-lg-6">
				 <input type="text" class="form-control"  name="BlockName" id="BlockName" 
				  value="<?php echo $data->BlockName;?>" >
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">Ward number</div>
			  <div class="col-lg-6">
				 <input type="text" class="form-control"  name="VillageName" id="VillageName" 
				  value="<?php echo $data->VillageName;?>" >
				</div>
				</div>

				<div class="col-lg-12">
			  <div class="col-lg-6">Pincode</div>
			  <div class="col-lg-6">
				<input type="text" class="form-control"  name="PinCode" id="PinCode" 
				  value="<?php echo $data->PinCode;?>" >
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">Location Type </div>
			  <div class="col-lg-6">
				<?php if($data->LocationType ==2){ ?>
				 <input type="radio" name="LocationType" id="RUrban" value="1"  >Urban
				 <input type="radio" name="LocationType" id="RUrban" value="2" checked="CHECKED">Rural
				<?php }else{ ?>
				<input type="radio" name="LocationType" id="RUrban" value="1" checked="CHECKED">Urban
				<input type="radio" name="LocationType" id="RUrban" value="2">Rural
				<?php } ?>
				</div>
				</div>
				</div>
			 </div>
			 
			  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Existing Plant Details</h3>
			</div>

			<div class="col-lg-12">
			  <div class="col-lg-6">Date of establishment </div>
			  <div class="col-lg-6">
				<div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" name="EstablishmentDate" 
				  value="<?php echo date("d-m-Y", strtotime($data->EstablishmentDate));?>">
                </div>
				</div>
				</div>
				
				<div class="col-lg-12">
			  <div class="col-lg-6">Age of plant(month)</div>
			  <div class="col-lg-6">
				<input type="text" class="form-control"  name="AgeOfPlant" id="AgeOfPlant" 
				  value="<?php echo $data->AgeOfPlant;?>">
				</div>
				</div>
				<div class="col-lg-12">
			  <div class="col-lg-6">Plant capacity(lph)</div>
			  <div class="col-lg-6">
				 <Select name="PlantSpecificationID" id="PlantSpecificationID"class="form-control" >
				  <option value="">All</option>
				  <?php $PlantSpecification = $this->model->getPlantSpecification();
					foreach($PlantSpecification as $row){
					if($row->PlantSpecificationID==$data->PlantSpecificationID){
				  ?>
				  <option value="<?php echo $row->PlantSpecificationID; ?>" SELECTED><?php echo $row->PlantSpecificationName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->PlantSpecificationID; ?>" ><?php echo $row->PlantSpecificationName;?></option>
				  <?php  }} ?>
				  </select>
				</div>
				</div>
				
			
			<?php if(empty($data->PlantSpecificationAnyOther)){
				$vstyle = "style='display:none;'";
			} else{
				$vstyle = "style='display:block;'";
			} 
			?>
			<div id="ActivePlantSpecification" <?php echo $vstyle; ?> >
			<div class="col-lg-12">
			  <div class="col-lg-6"></div>
			  <div class="col-lg-6"><input type="text" class="form-control"  name="PlantSpecificationAnyOther" id="PlantSpecificationAnyOther" value="<?php echo $data->PlantSpecificationAnyOther;?>" ></div>
			</div>
			</div>

		<script type="text/javascript">

		$("#PlantSpecificationID").change(function(){
			var select=  $(this).val();
			if(select=='99'){
				$("#ActivePlantSpecification").show();
				}else {
						$("#ActivePlantSpecification").hide();
					}
			}); 
		
		</script>


				<div class="col-lg-12">
			  <div class="col-lg-6">Plant Manufactured by</div>
			  <div class="col-lg-6">
				<Select name="PlantManufacturerID" id="PlantManufacturerID"class="form-control" >
				  <option value="">All</option>
				  <?php $PlantManufacturer = $this->model->getPlantManufacturer();
					foreach($PlantManufacturer as $row){
					if($row->PlantManufacturerID==$data->PlantManufacturerID){
				  ?>
				  <option value="<?php echo $row->PlantManufacturerID; ?>" SELECTED><?php echo $row->PlantManufacturerName;?></option>
				  <?php }else{ ?>
				   <option value="<?php echo $row->PlantManufacturerID; ?>" ><?php echo $row->PlantManufacturerName;?></option>
				  <?php  }} ?>
				  </select>
				</div>
				</div>
			<?php if(empty($data->ManufacturerName)){
				$vstyle = "style='display:none;'";
			} else{
				$vstyle = "style='display:block;'";
			} 
			?>
			<div id="ActiveManufacturername" <?php echo $vstyle; ?> >
			<div class="col-lg-12">
			  <div class="col-lg-6"></div>
			  <div class="col-lg-6">
				<input type="text" class="form-control"  name="ManufacturerName" id="ManufacturerName" value="<?php echo $data->ManufacturerName;?>" >
				</div>
				</div>
			</div>


<script type="text/javascript">

 $("#PlantManufacturerID").change(function(){
     var select=  $(this).val();
       if(select=='2'){
          $("#ActiveManufacturername").show();
         }else {
                $("#ActiveManufacturername").hide();
            }
    }); 
  
</script>

			<div class="col-lg-12">
			  <div class="col-lg-6">Remote Monitoring System</div>
			  <div class="col-lg-6">
				<Select type="Select" class="form-control"  name="RemoteMonitoringSystem" id="RemoteMonitoringSystem" >
				<?php if($data->RemoteMonitoringSystem =='1'){ ?> 
				  <option value"0">No</option>
				  <option value"1" SELECTED="SELECTED">Yes</option>
				  <?php }else{?>
				   <option value"0" SELECTED="SELECTED">No</option>
				  <option value"1" >Yes</option>
				  <?php } ?>
				  </select>
				</div>
				</div>


				<div class="col-lg-12">
			  	<div class="col-lg-6"><b>Contaminant in raw water</b></div>
			  	<div class="col-lg-6"></div>
				</div>
				<input type="hidden" class="form-control"  name="PlantGUID" id="PlantGUID" 
				  value="<?php echo $token;?>" >
			  <?php 
				   $WQC=array();
				   $WaterQualityChallenge = $this->model->getWaterQualityChallenge();
				   $Contnts = $this->model->getPatWaterContaminants($token);
				   //echo "<pre>";
				   //print_r($Contnts);
				?>  
				<div class="col-lg-12">
				  <?php $i=0;foreach($WaterQualityChallenge as $row){ ?>
			  	<div class="col-lg-3"><input type="checkbox" value="<?php echo $row->WaterQualityChallengeName;?>" name="WQC[]" id="WQC<?php echo $row->WaterQualityChallengeID;?>" 
				<?php if($Contnts[$i]->WaterQualityChallengeID !='') { echo "checked"; }  ?>> <?php echo $row->WaterQualityChallengeName;?></div>
				  <?php $i++; } ?>
				</div>

				<div class="col-lg-12">
			  	<div class="col-lg-6"><b>Treatment steps</b></div>
			  	<div class="col-lg-6"></div>
				</div>
				  <?php 
				 
				   $PlantPurificationStep = $this->model->getPlantPurificationStep();
				    $Patplt = $this->model->getPlantPurStep($token);
					//echo "<pre>";
					//print_r($Patplt);
				?>  
					
				<div class="col-lg-12" style="margin-left:15px;" >
				 <?php $i=0; foreach($PlantPurificationStep as $row){ 
					// echo $Patplt[$i]->PlantPurificationstepID;
						if($row->PlantPurificationStepID == $Patplt[$i]->PlantPurificationstepID){
				   ?>
					<div class="col-lg-3">
					<input type="checkbox" value="<?php echo $row->PlantPurificationStepName;?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID;?>" checked="CHECKED" > <?php echo $row->PlantPurificationStepName;?></div>
					<?php }else{  ?>
						<input type="checkbox" value="<?php echo $row->PlantPurificationStepName;?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID;?>" > <?php echo $row->PlantPurificationStepName;?>
					<?php } $i++;}?>
				</div>

			    <table  class="table table-bordered table-hover">
				<tr>
			  <th colspan="6"></th>
				  </tr>
			   <tr>
				  <th colspan="6"></th>
				  </tr>
			
 				 </table>
			 </div>
		  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Asset details</h3>
			</div>

			<div class="box-body">
			 <input type="hidden" name="POGUID" id="POGUID" value="<?php echo $data->POGUID;?>"> 
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Machinery</b></td>
								<td></td>
								<td></td>
								<td> </td>
								<td><input type="text" name="MachineryCost" id="MachineryCost" size="5" class="form-control" value="<?php echo $data->MachineryCost;?>"></td>
							</tr>
						</tbody>
						 <tbody>
				 <tr>
				<?php $AssestFunder = $this->model->getAssestFunder6();
				  $sqlflag6 = "SELECT AssetFunderID FROM tblpatplantassetfunder WHERE `PlantGUID` ='".$token."' AND FlagID ='6'";
				  $result = $this->Common_model->query_data_array($sqlflag6);
				?> 
				 <input type="hidden" name="MachineryFlag" id="Flag" value="6"> 
				<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$result[0]['AssetFunderID']){
				?>
      			 <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="MachineryOther" id="MachineryOther<?php echo $row->AssestFunderID;?>" checked="checked"> <b><?php echo $row->AssestFunderName;?></b>
				  
				   
				</td>
				<?php }else{ ?>
				
				<td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="MachineryOther" id="MachineryOther<?php echo $row->AssestFunderID;?>"> <b><?php echo $row->AssestFunderName;?></b></td>
				<?php  } }?>
        </tr>
      </tbody>
    </table>
	<?php if($result[0]['AssetFunderID'] ==2){ ?>
	<div id="machinerygrantf" style="display:block;">
	<input type="text" name="grant" id="grant" size="5" class="form-control" value="<?php echo $data->MachineryOther;?>">
	<?php  }else{ ?>
	<div id="machinerygrantf" style="display:none;" >
	<input type="text" name="grant" id="grant" size="5" class="form-control" value="">
	<?php } ?>
	</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#MachineryOther2").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrantf").show();
            } else {
                $("#machinerygrantf").hide();
            }
        });

			$("#MachineryOther3").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrantf").show();
            } else {
                $("#machinerygrantf").hide();
            }
        });

			$("#MachineryOther4").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrantf").hide();
            } 
        });
			$("#MachineryOther5").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrantf").hide();
            } 
        });

			$("#MachineryOther1").click(function () {
            if ($(this).is(":checked")) {
                $("#machinerygrantf").hide();
            } 
        });
				
});
</script>
	<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Land</b></td>
								<td></td>
								<td></td>
								<td><input type="text" name="LandCost1" id="LandCost1" size="5" class="form-control" value="<?php echo $data->LandCost;?>"></td>
							</tr>
						</tbody>
						 <tbody>
					<tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag2();
			 $sqlflag6 = "SELECT AssetFunderID FROM tblpatplantassetfunder WHERE `PlantGUID` ='".$token."' AND FlagID ='2'";
			 $result = $this->Common_model->query_data_array($sqlflag6);
			?> 
			 <input type="hidden" name="LandFlag" id="Flag" value="2">
				<?php foreach($AssestFunder as $row){
				 
				 if($row->AssestFunderID==$result[0]['AssetFunderID']){
				?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="LandOther" id="LandOther<?php echo $row->AssestFunderID;?>" checked> <b><?php echo $row->AssestFunderName;?></b>
</td>
				<?php }else{ ?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="LandOther" id="LandOther<?php echo $row->AssestFunderID;?>" > <b><?php echo $row->AssestFunderName;?></b>
</b>
</td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
		<div id="landanyother" style="display:none;">
		<input type="text" name="landother" id="landother" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#LandOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#landanyother").show();
            } else {
                $("#landanyother").hide();
            }
        });

		$("#LandOther7").click(function () {
            if ($(this).is(":checked")) {
                $("#landanyother").hide();
            } 
        });

		$("#LandOther8").click(function () {
            if ($(this).is(":checked")) {
                $("#landanyother").hide();
            } 
        });
		$("#LandOther9").click(function () {
            if ($(this).is(":checked")) {
                $("#landanyother").hide();
            } AuditingAgencyAddress
        });


});
</script>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Building</b></td>
								<td></td>
								<td></td>
								<td><input type="text" name="BuildingCost1" id="BuildingCost1" size="5" class="form-control" value="<?php echo $data->BuildingCost;?>"></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag3();
			 $sqlflag6 = "SELECT AssetFunderID FROM tblpatplantassetfunder WHERE `PlantGUID` ='".$token."' AND FlagID ='3'";
			 $result = $this->Common_model->query_data_array($sqlflag6);
			 //print_r($result);
			 
			?> 
			<input type="hidden" name="BuildingFlag" id="Flag" value="3">
	<?php foreach($AssestFunder as $row){
		if($row->AssestFunderID==$result[0]['AssetFunderID']){
	?>
       <td>
	   
	   <input type="radio" value="<?php echo $row->AssestFunderID;?>" name="BuildingOther" id="BuildingOther<?php echo $row->AssestFunderID;?>" checked> <b><?php echo $row->AssestFunderName;?></b>
</td>
				<?php }else{ ?>
       <td>
	    
	   <input type="radio" value="<?php echo $row->AssestFunderID;?>" name="BuildingOther" id="BuildingOther<?php echo $row->AssestFunderID;?>" > <b><?php echo $row->AssestFunderName;?></b>
</td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
			<div id="buildinganyOther" style="display:none;">
		<input type="text" name="BuildinganOther" id="BuildinganOther" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#BuildingOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#buildinganyOther").show();
            } else {
                $("#buildinganyOther").hide();
            }
        });
		$("#BuildingOther11").click(function () {
            if ($(this).is(":checked")) {
                $("#buildinganyOther").hide();
            } 
        });
		$("#BuildingOther12").click(function () {
            if ($(this).is(":checked")) {
                $("#buildinganyOther").hide();
            } 
        });
		$("#BuildingOther13").click(function () {
            if ($(this).is(":checked")) {
                $("#buildinganyOther").hide();
            } 
        });
});
</script>

  

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Raw water source</b></td>
								<td></td>
								<td></td>
								<td>
								<input type="text" name="RawWaterSourceCost" id="RawWaterSourceCost" size="5" class="form-control" value="<?php echo $data->RawWaterSourceCost;?>">
								</td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag4();
			 $sqlflag6 = "SELECT AssetFunderID FROM tblpatplantassetfunder WHERE `PlantGUID` ='".$token."' AND FlagID ='4'";
			 $result = $this->Common_model->query_data_array($sqlflag6);
			?> 
			 <input type="hidden" name="RawWaterSourceFlag" id="Flag" value="4">
				<?php foreach($AssestFunder as $row){
				if($row->AssestFunderID==$result[0]['AssetFunderID']){
				?>
       <td>
	   
	<input type="radio" value="<?php echo $row->AssestFunderID;?>" name="RawWaterSourceOther" id="RawWaterSourceOther<?php echo $row->AssestFunderID;?>" checked> <b><?php echo $row->AssestFunderName;?></b>
	</td>
				<?php }else{ ?>
       <td>
	    
	   <input type="radio" value="<?php echo $row->AssestFunderID;?>" name="RawWaterSourceOther" id="RawWaterSourceOther<?php echo $row->AssestFunderID;?>" > <b><?php echo $row->AssestFunderName;?></b></td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
  	<div id="rawWateranyOther" style="display:none;">
		<input type="text" name="RawWaterOther" id="RawWaterOther" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#RawWaterSourceOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#rawWateranyOther").show();
            } else {
                $("#rawWateranyOther").hide(); 
			}
        });

			$("#RawWaterSourceOther15").click(function () {
            if ($(this).is(":checked")) {
                $("#rawWateranyOther").hide();
            } 
        });
		$("#RawWaterSourceOther16").click(function () {
            if ($(this).is(":checked")) {
                $("#rawWateranyOther").hide();
            } 
        });
		$("#RawWaterSourceOther17").click(function () {
            if ($(this).is(":checked")) {
                $("#rawWateranyOther").hide();
            } 
        });

});
</script>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Electricity connection</b></td>
								<td></td>
								<td></td>
								<td>
								<input type="text" name="ElectricityCost1" id="ElectricityCost1" size="5" class="form-control" value="<?php echo $data->ElectricityCost;?>">
								</td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag5();
			 $sqlflag5 = "SELECT AssetFunderID FROM tblpatplantassetfunder WHERE `PlantGUID` ='".$token."' AND FlagID ='5'";
			 $result = $this->Common_model->query_data_array($sqlflag5);
			?> 
			 <input type="hidden" name="ElectricityFlag" id="Flag" value="5">
				<?php $i=0; foreach($AssestFunder as $row){
					if($row->AssestFunderID== $result[0]['AssetFunderID']){
				?>
       <td>
	   
	<input type="radio" value="<?php echo $row->AssestFunderID;?>" name="ElectricityOther" id="ElectricityOther<?php  echo $row->AssestFunderID;?>" checked> <b><?php echo $row->AssestFunderName;?></b>
	</td>
				<?php }else{ ?>
       <td>
	   
<input type="radio" value="<?php echo $row->AssestFunderID;?>" name="ElectricityOther" id="ElectricityOther<?php echo $row->AssestFunderID;?>" > <b><?php echo $row->AssestFunderName;?></b></td>
				<?php } $i++; }?>
        </tr>
      </tbody>
    </table>
  	<div id="electriOther" style="display:none;">
		<input type="text" name="ElectricityanyOther" id="ElectricityanyOther" size="5" class="form-control" value="">
		</div>

  </div>
<script type="text/javascript">
    $(function () {
			$("#ElectricityOther99").click(function () {
            if ($(this).is(":checked")) {
                $("#electriOther").show();
            } else {
                $("#electriOther").hide();
            }
        });
	
	$("#ElectricityOther19").click(function () {
            if ($(this).is(":checked")) {
                $("#electriOther").hide();
            } 
        });
	$("#ElectricityOther20").click(function () {
            if ($(this).is(":checked")) {
                $("#electriOther").hide();
            } 
        });
	$("#ElectricityOther21").click(function () {
            if ($(this).is(":checked")) {
                $("#electriOther").hide();
            } 
        });
		
});
</script>
		 <table class="table table-bordered table-hover">
			<thead>
			<tr>
				<th colspan="3">Aggregator</th>
				<th colspan="3"><input type="text" name="WaterBrandName" id="WaterBrandName" class="form-control" value="<?php echo $data->WaterBrandName;?>"></th>
		   </tr>
		   </tr>
		     </thead>
		   </table>
	</div>
	           
		</div>
	<?php  
	$distpoint = $this->model->getDistributionPointFlag11();
	$dpdistance = $this->model->getDistance();
	?>
<script src="http://code.jquery.com/jquery-2.1.1.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var counter = 2;

    $("#addButton").click(function () {

	if(counter>15){
            alert("Only 5 Distribution/Distance points Allow");
            return false;
	}

	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);

	var template1 = '<div class="col-lg-12"><div class="col-lg-3">Distribution point : </div>' + '<div class="col-lg-4"><select name="DPName[]" class="form-control" id="DPName' + counter + '">';

	var part1 = '<option value="">Select</option>';

	<?php foreach($distpoint as $drow){ ?>
		part1 += '<option value="<?php echo $drow->AssestFunderID;?>"><?php echo $drow->AssestFunderName;?></option>';
	<?php } ?>
	
	template1 += part1;
	
	template1 += '</select></div>';

	var template2 = '';

	template2 += '<lable class="col-lg-2">Distance : </lable>' + '<div class="col-lg-3"><select name="Distance[]" class="form-control" id="Distance' + counter + '">';
	
	var part2 = '<option value="">Select</option>';

	<?php foreach($dpdistance as $drow){ ?>
		part2 += '<option value="<?php echo $drow->DistanceID;?>"><?php echo $drow->DistanceValue;?></option>';
	<?php } ?>
	
	template2 += part2;
	template2 += '</select></div>';
	template2 += '</div>';

	console.log(template1+template2);

	// newTextBoxDiv.after().html(template1 + template2);
	newTextBoxDiv.append(template1 + template2);
	// );

	newTextBoxDiv.appendTo("#TextBoxesGroup");

	counter++;
     });

     $("#removeButton").click(function (name) {
	if(counter==1){
          alert("No more textbox to remove");
          return false;
       }

	counter--;

        $("#TextBoxDiv" + counter).remove(AssestFunderName);

     });

   
  });
</script>

              <div class="tab-pane" id="tab_2">

			  <div class="box box-primary">
			   <div class="box-header with-border">
              <h3 class="box-title">Social sustainability</h3>
			</div>
			<div class="row">
			<div class="col-lg-12">
			<div class="col-lg-6">Population</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="Population" id="Population"  value="<?php echo $data->Population;?>" >
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">Number of households</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="NoOfHousehold" id="NoOfHousehold" value="<?php echo $data->NoOfHousehold;?>" >
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">Number of households within 500m</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="NoOfHouseholdWithin2km" id="NoOfHouseholdWithin2km" 
				  value="<?php echo $data->NoOfHouseholdWithin2km;?>" >
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">Number of households registered</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="NoOfhhregistered" id="NoOfhhregistered" 
				  value="<?php echo $data->NoOfhhregistered;?>" >
			</div>
			</div>

			<div class="col-lg-12">
			<div class="col-lg-6">Average no. of prepaid cards used /month</div>
			<div class="col-lg-6">
			<input type="text" class="form-control"  name="Average_number_of_monthly_water_cards" id="Average_number_of_monthly_water_cards" 
				  value="<?php echo $data->Population;?>" >
			</div>
			</div>

			<div class="col-lg-12">
			<div class="col-lg-6">Distribution</div>
			<div class="col-lg-6">
			<select type="Select" class="form-control"  name="Distribution" id="Distribution">
			<?php foreach($approval as $key=>$res){ 
			if($data->Distribution ==$key){
			?>
			<option value="<?php echo $key;?>" SELECTED="SELECTED"><?php echo $res;?></option>
			<?php }else{ ?>
			<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
			</select>
			</div>
		</div>
			<div class="row" style="margin-left:25px;"><b>Social composition</b></div>

			<div class="col-lg-12">
			<div class="col-lg-6"> <input type="checkbox"  value="SC"  name="SC" id="SC" <?php if($data->SC ==1){ echo "checked";}else{ echo " ";} ?>> Scheduled Caste (SC)</div>
			<div class="col-lg-6"><input type="checkbox" value="ST"  <?php if($data->ST ==1){ echo "checked";}else{ echo "";}?> name="ST" id="ST">Scheduled Tribes (ST)</div>
			</div>
			<div class="col-lg-12">
				<div class="col-lg-6"><input type="checkbox" value="OBC" <?php if($data->OBC ==1){ echo "checked";}else{ echo " ";}?> name="OBC" id="OBC" >Other Backward Castes (OBC)</div>
				<div class="col-lg-6"><input type="checkbox" value="General" <?php if($data->General ==1){ echo "checked";}else{ echo " ";}?> name="General" id="General" >General</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6"><input type="checkbox" value="BPL" <?php if($data->BPL ==1){ echo "checked";}else{ echo " ";}?> name="BPL" id="BPL">Below Povirty Line (BPL)</div>
			<?php if($data->AllInclusion ==1){ $checked = "checked";} ?>
			<div class="col-lg-6"><input type="checkbox" value="AllInclusion"  name="AllInclusion" id="AllInclusion" <?php echo $checked; ?>>All Inclusion</div>
			</div>
		
				
			<div class="row" style="margin-left:25px;"><b>Distribution/Distance points</b></div>
			
			<div class="col-lg-12">
				<div class="col-lg-3">Distribution point</div>
				<div class="col-lg-4">
				<input type="hidden"  name="PlantGUID" id="PlantGUID" value="<?php echo $token;?>">
				<select name="DPName[]" class="form-control" id="DPName1">
				 <option value="">Select </option>
				<?php foreach($distpoint as $row){  ?>
				  <option value="<?php echo $row->AssestFunderID; ?>"><?php echo $row->AssestFunderName; ?></option>
				<?php } ?>
				</select>
			</div>
				<div class="col-lg-1">Distance</div>
				<div class="col-lg-2">
				<select name="Distance[]"  class="form-control" id="Distance1">
				<option value="">Select </option>
				 <?php foreach($dpdistance as $row){?>
				  <option value="<?php echo $row->DistanceID;?>"><?php echo $row->DistanceValue;?></option>
				<?php } ?>
				</select>
				</div>
				<div class="col-lg-2"><input type='button' value='Add' id='addButton' Class="btn btn-info pull-right"></div>
			</div>
			<div id='TextBoxesGroup'>
			<div id="TextBoxDiv1">
			</div>
			</div>
	
			 </div>
		 </div>
		 </div>

<script type="text/javascript">
    $(function () {
			$('#Distribution').change(function(){
				if($('#Distribution').val() == '1') {
					$('#DPName').removeAttr('disabled');   
					$('#Distance').removeAttr('disabled');  
				} else {
					$('#DPName').attr('disabled','disabled');   
					$('#Distance').attr('disabled','disabled');  
				} 
			});
		
		});
</script> 

      <div class="tab-pane" id="tab_3">
          <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Plant production capacity (Litres)</h3>
			  </div>

						<div class="row">
							<div class="col-lg-12">
											<div class="col-lg-6">Daily water produced (Average)</div>
											<div class="col-lg-6">
				<input type="text" class="form-control"  name="WaterProductionDaily" id="WaterProductionDaily" value="<?php echo $data->WaterProductionDaily;?>" >
											</div>
							</div>

							<div class="col-lg-12">
											<div class="col-lg-6">Monthly water produced (Average)</div>
											<div class="col-lg-6">
													<input type="text" class="form-control"  name="WaterProductionMonthly" id="WaterProductionMonthly" value="<?php echo $data->WaterProductionMonthly;?>" >
											</div>
							</div>

							<div class="col-lg-12">
											<div class="col-lg-6">Peak Sale</div>
											<div class="col-lg-6">
												<input type="text" class="form-control"  name="PeakSale" id="PeakSale" value="<?php echo $data->PeakSale;?>" >
											</div>
							</div>
			 </div>
		</div>	 
			 
              <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Kiosk Operated By</h3>
			  </div>

			<div class="row">
<div class="col-lg-12">
        <div class="col-lg-6">Name</div>
        <div class="col-lg-6">
            <input type="text" class="form-control"  name="OperatorName" id="OperatorName" value="<?php echo $data->OperatorName;?>" >
        </div>
</div>

				
<div class="col-lg-12">
    <div class="col-lg-6">Designation</div>
        <div class="col-lg-6">
            <?php $Designation = $this->model->getDesignation();?>
                <Select type="Select" class="form-control"  name="Designation" id="Designation" >
                <?php foreach($Designation as $row) { 
                if($row->DesignationID==$data->DesignationID){
                ?>
                    <option value="<?php echo $row->DesignationID;?>" SELECTED><?php echo $row->DesignationName;?></option>
                <?php }else{ ?>
                    <option value="<?php echo $row->DesignationID;?>"><?php echo $row->DesignationName;?></option>
                <?php } }?>
                </select>
        </div>
</div>

<div class="col-lg-12">
    <div class="col-lg-6">Contact number</div>
    <div class="col-lg-6">
    <input type="text" class="form-control"  name="ContactNumber" id="ContactNumber" value="<?Php echo $data->ContactNumber;?>" >
    </div>
</div>

<div class="col-lg-12">
    <div class="col-lg-6">Education Level</div>
        <div class="col-lg-6">
		<?php $literacy = $this->model->getLiteracy();?>
            <Select type="Select" class="form-control"  name="LiteracyID" id="LiteracyID" >
                <option value="">SELECT</option>
				<?php foreach($literacy as $row) {
				if($row->LiteracyID == $data->LiteracyID){
				?>
                <option value="<?php echo $row->LiteracyID; ?>" SELECTED="SELECTED"><?php echo $row->LiteracyName; ?></option>
				<?php }else{ ?>
				 <option value="<?php echo $row->LiteracyID; ?>"><?php echo $row->LiteracyName; ?></option>
				<?php } } ?>
                </select>
            </div>
	</div>
	</div>		  

</div>
			 <!----- End here -->
			 
			 <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Daily Water Quality Monitoring</h3>
			  </div>

		<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">UV (Ultra-violet)</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="UV" id="UV" >
						<?php if($data->UV==0){ ?>
								<option value="0" SELECTED>NO</option>
								<option value="1">Yes</option>
						<?php }else{ ?>
								<option value="0">NO</option>
								<option value="1" SELECTED>Yes</option>
						<?php } ?>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">TDS (Total Dissolved Solids)</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="TDS" id="TDS" >
							<?php if($data->TDS==0){ ?>
								<option value="0" SELECTED>NO</option>
								<option value="1">Yes</option>
							<?php } else{?>
								<option value="0">NO</option>
								<option value="1" SELECTED>Yes</option>
							<?php } ?>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">pH (Potential of Hydrogen)</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="pH" id="pH" >
								<?php if($data->pH==0){ ?>
								<option value="0" SELECTED>NO</option>
								<option value="1">Yes</option>
								<?php } else{?>
									<option value="0">NO</option>
								<option value="1" SELECTED>Yes</option>
								<?php } ?>
							</select>
							</div>
					</div>
					<div class="col-lg-12">
							<div class="col-lg-6">Residual Chlorine</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Residual_Chlorine" id="Residual_Chlorine" >
							<?php if($data->ResidualChlorine==0){ ?>
								<option value="0" SELECTED>NO</option>
								<option value="1">Yes</option>
								<?php } else{?>
									<option value="0" >NO</option>
								<option value="1" SELECTED>Yes</option>
								<?php } ?>
							</select>
							</div>
					</div>

						<div class="col-lg-12">
							<div class="col-lg-6">Microbial</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Microbial" id="Microbial" >
								<?php if($data->Microbial==0){ ?>
								<option value="0" SELECTED>NO</option>
								<option value="1">Yes</option>
								<?php }else{ ?>
									<option value="0">NO</option>
								<option value="1" SELECTED>Yes</option>
								<?php } ?>
							</select>
							</div>
					</div>

				</div>
				</div>


			  <div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training and capacity building</h3>
			  </div>
			    <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>
				  
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Training recieved</h3>
			  </div>
				  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="ElectricalSafety" name="ElectricalSafety" id="ElectricalSafety"
					  <?php if($data->ElectricalSafety==1){ echo "CHECKED";}?>>
                     Electrical safety
                    </label>
                  </div>
                   <div class="checkbox">
                    <label>
                       <input type="checkbox" value="PlantOM" name="PlantOM" id="PlantOM" 
					   <?php if($data->PlantOM==1){ echo "CHECKED";}?>>
                      Plant operation and managment
                    </label>
                  </div>
				<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="WaterQuality" name="WaterQuality" id="WaterQuality" <?php if($data->WaterQuality==1){ echo "CHECKED";}?>>
                      Water quality
                    </label>
                  </div>
				<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="ConsumerAwareness" name="ConsumerAwareness" id="ConsumerAwareness" <?php if($data->ConsumerAwareness==1){ echo "CHECKED";}?> >
                      Consumer awareness
                    </label>
                  </div>
					<div class="checkbox" >
                    <label>
                      <input type="checkbox" value="ABKeeping" name="ABKeeping" id="ABKeeping"<?php if($data->ABKeeping==1){ echo "CHECKED";}?> >
                     Accounts and book keeping
                    </label>
                  </div>
				  
			 </div>
			 
			 
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Quality assurance and Hygiene</h3>
			  </div>
				

					<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">Earthing/Grounding</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Earthing" id="Earthing" >
							<?php if($data->Earthing ==0) { ?>
								<option value="0" SELECTED>Not Done</option>
								<option value="1">Done</option>
								<?php } else{?>
								<option value="0" >Not Done</option>
								<option value="1" SELECTED>Done</option>
								<?php } ?>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">Appearance</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Appearance" id="Appearance" >
								<?php if($data->Appearance ==0) { ?>
								<option value="0" SELECTED>Not-Agreeable</option>
								<option value="1">Agreeable</option>
								<?php } else{ ?>
									<option value="0">Not-Agreeable</option>
								<option value="1" SELECTED>Agreeable</option>
								<?php } ?>
							</select>
							</div>
					</div>

					<div class="col-lg-12">
							<div class="col-lg-6">Odour</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Odour" id="Odour" >
									<?php if($data->Odour ==0) { ?>
								<option value="0" SELECTED>Not-Agreeable</option>
								<option value="1">Agreeable</option>
								<?php } else{ ?>
									<option value="0">Not-Agreeable</option>
								<option value="1" SELECTED>Agreeable</option>
								<?php } ?>
							</select>
							</div>
					</div>
					<div class="col-lg-12">
							<div class="col-lg-6">Taste</div>
							<div class="col-lg-6">
							<Select type="Select" class="form-control"  name="Taste" id="Taste" >
								<?php if($data->Taste ==0) { ?>
								<option value="0" SELECTED>Not-Agreeable</option>
								<option value="1">Agreeable</option>
								<?php } else{ ?>
									<option value="0">Not-Agreeable</option>
								<option value="1" SELECTED>Agreeable</option>
								<?php } ?>
							</select>
							</div>
					</div>

						<div class="col-lg-12">
							<div class="col-lg-6">TDS Value</div>
							<div class="col-lg-6">
							<input type="number" class="form-control" name="TDSValue" id="TDSValue" value="<?php echo number_format($data->TDSValue, 2, '.', ','); ?>">
							</div>
					</div>

					<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Raw water source</div>
							<div class="col-lg-6">Well covered</div>
							<div class="col-lg-6">
						<select class="form-control" name="RawWaterOpenwellcovered" id="RawWaterOpenwellcovered">
                <?php foreach($approval as $key=>$res){ 
				  			if($data->RawWaterOpenwellcovered ==$key){?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                <?php }else{ ?>
								<option value="<?php echo $key;?>"><?php echo $res;?></option>
								<?php }} ?>
                 </select>
							</div>
					</div>

		<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Borewell casing done</div>
							<div class="col-lg-6">
					 <select class="form-control" name="RawWaterBoreWellCasing" id="RawWaterBoreWellCasing">
       <?php foreach($approval as $key=>$res){ 
				  if($data->RawWaterBoreWellCasing ==$key){
				?>
          <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
         <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
				<?php }} ?>
                 </select>
							</div>
					</div>
			
	<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Treated water tank</div>
							<div class="col-lg-6">Inside the plant</div>
							<div class="col-lg-6">
					 <select class="form-control" name="Insidetheplant" id="Insidetheplant">
            <?php foreach($approval as $key=>$res){ 
							if($data->Insidetheplant ==$key){?>
								<option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
									<?php }else{ ?>
								<option value="<?php echo $key;?>"><?php echo $res;?></option>
						<?php }} ?>
                 </select>

							</div>
					</div>

			<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Covered</div>
							<div class="col-lg-6">
				 <select class="form-control" name="Covered" id="Covered">
             <?php foreach($approval as $key=>$res){ 
			  if($data->Covered ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>

							</div>
					</div>
			
		
					<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Hygiene - inside plant  </div>
							<div class="col-lg-6">Cleanliness in plant</div>
							<div class="col-lg-6">
				 <select class="form-control" name="CleanlinessinPlant" id="CleanlinessinPlant">
            <?php foreach($approval as $key=>$res){ 
			  if($data->CleanlinessinPlant ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
							</div>
					</div>
		
			
				<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Leakage</div>
							<div class="col-lg-6">
			<select class="form-control" name="Leakage" id="Leakage">
            <?php foreach($approval as $key=>$res){ 
			  if($data->Leakage ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
							</div>
					</div>
		
		<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Hygiene - outside plant</div>
							<div class="col-lg-6">Cleanliness near treatment plant</div>
							<div class="col-lg-6">
		 <select class="form-control" name="CleanlinessNearTreatmentPlant" id="CleanlinessNearTreatmentPlant">
            <?php foreach($approval as $key=>$res){ 
			  if($data->CleanlinessNearTreatmentPlant ==$key){
			?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
							</div>
					</div>

			<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:30px;">Water fill station</div>
							<div class="col-lg-6">Moss/algae growth</div>
							<div class="col-lg-6">
		  <select class="form-control" name="Checkmossoralgee" id="Checkmossoralgee">
             <?php foreach($approval as $key=>$res){ 
			  if($data->Checkmossoralgee ==$key){
			?>
          <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
							</div>
					</div>

			
			<div class="col-lg-12">
					<div class="row text-left" style="margin-left:15px;font-size:15px;height:10px;"></div>
							<div class="col-lg-6">Presence of puddle</div>
							<div class="col-lg-6">
		 <select class="form-control" name="Presenceofpuddle" id="Presenceofpuddle">
            <?php foreach($approval as $key=>$res){ 
			  if($data->Presenceofpuddle ==$key){
			?>
           <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
					<option value="<?php echo $key;?>"><?php echo $res;?></option>
			<?php }} ?>
                 </select>
							</div>
					</div>
		</th>
</tr>

<tr>
<th>
<div class="col-lg-12">
Water Quality Test Report From Accredited Laboratory
</div>
<div class="form-group">Before rainy season: 
<div class="checkbox">
<label>
<input type="checkbox"<?php if($data->PreMonsoonRWTesting ==1){ ?>value="<?php echo $data->PreMonsoonRWTesting;?>" CHECKED <?php } else{?>value="PRERW" <?php } ?> name="PreMonsoonRWTesting" id="PreMonsoonRWTesting">
	Raw water
</label>
</div>
<?php  if($data->PreMonsoonRWTesting ==1){ ?>
<div id="dvpreRWTesting">
<?php } else{ ?>
<div id="dvpreRWTesting" style="display: none">
<?php } ?>
<div class="row">
<div class="col-sm-12"><input type="text" class="form-control" Placeholder="Address"  name="PreMonsoonRWLabAdr" id="PreMonsoonRWLabAdr" value="<?php echo $data->PreMonsoonRWLabAdr;?>" size="50" ></div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div><input type="text" class="form-control datepicker"  name="PreMonsoonRWTestingdate" id="PreMonsoonRWTestingdate" value="<?php 
					if($data->PreMonsoonRWTestingdate !='1970-01-01 00:00:00'){	
					echo Date("d-m-Y",strtotime($data->PreMonsoonRWTestingdate)); } ?>" size="5">
				</div>
		</div>
	</div>
<div class="col-sm-6">
	<input type="file" class="form-control"  name="PreMonsoonRWTestProof" id="PreMonsoonRWTestProof" value="" >
</div>
</div>
<div>
	<?php if($data->PreMonsoonRWTestProof !=''){ ?>
					<input type="hidden" class="form-control"  name="oldPreMonsoonRWTestProof" id="PreMonsoonRWTestProof" value="<?php echo $data->PreMonsoonRWTestProof;?>" >
						<img src="<?php echo site_url()."datafiles/".$data->PreMonsoonRWTestProof; ?>" hight="100px" width="100px">
						<?php } ?>
	</div>
</div>
</div>

	<div class="checkbox">
			<label>
				<input type="checkbox" <?php  if($data->PreMonsoonTWTesting ==1 ){ ?>value="<?php echo $data->PreMonsoonTWTesting;?>" CHECKED <?php }else{?>value="PRETWT" <?php } ?> name="PreMonsoonTWTesting" id="PreMonsoonTWTesting">
				Treated water
			</label>
		</div>
		<?php  if($data->PreMonsoonTWTesting ==1 ){ ?>
				<div id="dvpretretTWtesting" >
		<?php  }else{ ?>
			<div id="dvpretretTWtesting" style="display: none">
		<?php } ?>

<div class="row">
<div class="col-sm-12">
<input type="text" class="form-control" Placeholder="Address"  name="PreMonsoonTWLabAdr" id="PreMonsoonTWLabAdr" value="<?php echo $data->PreMonsoonTWLabAdr;?>" size="50" >
	</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
								<input type="text" class="form-control datepicker"  name="PreMonsoonTWTestingdate" id="PreMonsoonTWTestingdate" value="<?php 
									if($data->PreMonsoonTWTestingdate !='1970-01-01 00:00:00'){	
									echo Date("d-m-Y", strtotime($data->PreMonsoonTWTestingdate)); } ?>" size="10">
				</div>
		</div>
	</div>
<div class="col-sm-6">
	<input type="file" class="form-control "  name="PreMonsoonTWProof" id="PreMonsoonTWProof" value="" size="10">
</div>
</div>
<div>
	<?php if($data->PreMonsoonTWProof !=''){ ?>
					<input type="hidden" class="form-control"  name="oldPreMonsoonTWProof" id="PreMonsoonTWProof" value="<?php echo $data->PreMonsoonRWTestProof;?>" >
						<img src="<?php echo site_url()."datafiles/".$data->PreMonsoonTWProof; ?>" hight="100px" width="100px">
						<?php } ?>
	</div>
				</div>

 </th>
</tr>

<tr>
	<th>
	<div class="form-group">After rainy season:
		<div class="checkbox">
		<label>
	<input type="checkbox" <?php if($data->PostMonsoonRWTesting==1){ ?>value="<?php echo $data->PostMonsoonRWTesting;?>" CHECKED <?php }else{?>value="POSTRW" <?php } ?> name="PostMonsoonRWTesting" id="PostMonsoonRWTesting">
	Raw water
</label>
</div>
<?php  if($data->PostMonsoonRWTesting ==1 ){ ?>
				<div id="dvRWTesting" >
		<?php  }else{ ?>
			<div id="dvRWTesting" style="display: none">
		<?php } ?>
					
<div class="row">
<div class="col-sm-12">
<input type="text" class="form-control" Placeholder="Address"  name="PostMonsoonRWLabadr" id="PostMonsoonRWLabadr" value="<?php echo $data->PostMonsoonRWLabadr;?>" size="50" >
	</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
								<input type="text" class="form-control datepicker"  name="PostMonsoonRWtestingdate" id="PostMonsoonRWtestingdate" value="<?php 
					if($data->PostMonsoonRWtestingdate !='1970-01-01 00:00:00'){	
					echo Date("d-m-Y", strtotime($data->PostMonsoonRWtestingdate));} ?>" >
				</div>
		</div>
	</div>
<div class="col-sm-6">
<input type="file" class="form-control "  name="PostMonsoonRWTestProof" id="PostMonsoonRWTestProof" value="" size="10">
</div>
</div>
	<?php if($data->PostMonsoonRWTestProof !=''){ ?>
					<input type="hidden" class="form-control"  name="oldPostMonsoonRWTestProof" id="PostMonsoonRWTestProof" value="<?php echo $data->PostMonsoonRWTestProof;?>" >
						<img src="<?php echo site_url()."datafiles/".$data->PostMonsoonRWTestProof; ?>" hight="100px" width="100px">
						<?php } ?>
	</div>

		</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" <?php if($data->PostMonsoonTWtesting==1){ ?> value="<?php echo $data->PostMonsoonTWtesting;?>" CHECKED <?php }else{?>value="POSTTWT" <?php } ?> name="PostMonsoonTWtesting" id="PostMonsoonTWtesting">
			Treated water
		</label>
	</div>
	<?php if($data->PostMonsoonTWtesting==1){ ?>
	<div id="dvPostTWtesting">
	<?php }else{ ?>
	<div id="dvPostTWtesting" style="display: none">
	<?php } ?>

<div class="row">
<div class="col-sm-12">
<input type="text" class="form-control" Placeholder="Address"  name="PostMonsoonTWLabAdr" id="PostMonsoonTWLabAdr" value="<?php echo $data->PostMonsoonTWLabAdr;?>" size="50" >
</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
								<input type="text" class="form-control datepicker"  name="PostMonsoonTWtestingdate" id="PostMonsoonTWtestingdate" value="<?php 
						if($data->PostMonsoonTWtestingdate !='1970-01-01 00:00:00'){	
						echo Date("d-m-Y", strtotime($data->PostMonsoonTWtestingdate)); } ?>" size="10">
				</div>
		</div>
	</div>
<div class="col-sm-6">
<input type="file" class="form-control "  name="PostMonsoonTWtestProof" id="PostMonsoonTWtestProof" value="" >
</div>
</div>
<div>
	<?php if($data->PostMonsoonTWtestProof !=''){ ?>
					<input type="hidden" class="form-control"  name="oldPostMonsoonTWtestProof" id="PostMonsoonTWtestProof" value="<?php echo $data->PostMonsoonTWtestProof;?>" >
						<img src="<?php echo site_url()."datafiles/".$data->PostMonsoonTWtestProof; ?>" hight="100px" width="100px">
						<?php } ?>
	</div>

</div>
<div class="checkbox">
				<label>
					<input type="checkbox" <?php if($data->RejectWaterTesting==1){ ?> value="<?php echo $data->RejectWaterTesting; ?>" CHECKED<?php }else{?>value="REJWT" <?php } ?>name="RejectWaterTesting" id="RejectWaterTesting">
					Reject water
				</label>
	</div>	

<?php if($data->RejectWaterTesting==1){ ?>
<div id="dvRejectTWtesting" >
<?php }else{ ?>	
<div id="dvRejectTWtesting" style="display: none">
<?php } ?>

<div class="row">
<div class="col-sm-12">
	<input type="text" class="form-control" Placeholder="Address"  name="RejectWaterLabAdr" id="RejectWaterLabAdr" value="<?php echo $data->RejectWaterLabAdr;?>" size="50" >
</div>
<div class="col-sm-6">
	<div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>	<input type="checkbox" <?php if($data->PostMonsoonTWtesting==1){ ?> value="<?php echo $data->PostMonsoonTWtesting;?>" CHECKED <?php }else{?>value="POSTTWT" <?php } ?> name="PostMonsoonTWtesting" id="PostMonsoonTWtesting">
							<input type="text" class="form-control datepicker"  name="RejectWatertestingdate" id="RejectWatertestingdate" value="<?php
					if($data->RejectWatertestingdate !='1970-01-01 00:00:00'){			
						 echo Date("d-m-Y", strtotime($data->RejectWatertestingdate));}?>" size="10">
				</div>
		</div>
	</div>
<div class="col-sm-6">
<input type="file" class="form-control "  name="RejectWaterTestProof" id="RejectWaterTestProof" value="">
</div>
</div>
<div>
<?php if($data->RejectWaterTestProof !=''){ ?>
	<input type="hidden" class="form-control"  name="oldRejectWaterTestProof" id="RejectWaterTestProof" value="<?php echo $data->RejectWaterTestProof;?>" >
		<img src="<?php echo site_url()."datafiles/".$data->RejectWaterTestProof; ?>" hight="100px" width="100px">
		<?php } ?>
	</div>
		</div>
							   
	</div>
	</div>
</div>			  
			
   </th>
</tr>

<tr>
<th>

<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Reliability of operations</h3>
			  </div>
			<div class="row">
			<div class="col-lg-12">
			  <div class="col-sm-6">Technical downtime</div>
			  <div class="col-sm-6">
			  <select class="form-control" name="TechnicalDowntime" id="TechnicalDowntime">
				<?php if($data->TechnicalDowntime==0){  ?>
				<option value="0" selected="selected" >NO</option>
				<option value="1">Yes</option>
				<?php }else{ ?>
				<option value="0" >NO</option>
				<option value="1"  selected="selected">Yes</option>
				<?php } ?>
			   </select>
				  </div>
			  </div> 	
				</div>
			  
			  <div class="row">
				<div class="col-lg-12">
			  <div class="col-sm-6">No of days in last 30 days</div>
			  <div class="col-sm-6">
			  <input type="text" class="form-control" name="NoOfDays" id="NoOfDays" value="<?php echo $data->NoOfDays;?>">
				  </div>
					</div>
			  </div> 

				<div class="row" >
				<div class="col-lg-12">
			  	<div class="col-sm-6"> Reason Of downtime</div>
				</div>
				</div>
			  <div class="row" style="background-color: #E0E0E0;">
			<div class="col-lg-12">
			  <div class="col-sm-6"> </div>
			  <div class="col-sm-6"></div>
					</div>
				<div class="col-lg-12">
			  <div class="col-sm-6">
					<input type="checkbox" value="MotorRepair" <?php if($data->MotorRepair==1){ echo "CHECKED";} ?> name="MotorRepair" id="MotorRepair">
				Motor repair</div>
			  <div class="col-sm-6">	
				<input type="checkbox" value="MembraneChoke" <?php if($data->MotorRepair==1){ echo "CHECKED";} ?> name="MembraneChoke" id="MembraneChoke">
				Membrane choke</div>
				</div>
					<div class="col-lg-12">
			  <div class="col-sm-6">
					<input type="checkbox" value="Rawwaterproblem" <?php if($data->Rawwaterproblem==1){ echo "CHECKED";} ?> name="Rawwaterproblem" id="Rawwaterproblem">
				Row water problem</div>
			  <div class="col-sm-6">
				<input type="checkbox" value="Electricityoutage" <?php if($data->Electricityoutage==1){ echo "CHECKED";} ?> name="Electricityoutage" id="Electricityoutage">
				Electricity outage</div>
				</div>
					<div class="col-lg-12">
			  <div class="col-sm-6">
					<input type="checkbox" value="Anyother"  name="Anyother" id="Anyother" <?php if($data->Anyother==1){ echo "CHECKED";} ?>>
				Any other</div>
			<div class="col-sm-6">
			 <?php if($data->Anyother==1){ ?>
				<div id="dayAnyother" >
				<?php } else{ ?>
				<div id="dayAnyother"  style="display:none;">
				<?php } ?>
				<input type="text" name="rowAnyotherEdit" id="rowAnyotherEdit" class="form-control" value="<?php echo $data->AnyotherEdit;?>">
		</div>
				</div>
				</div>
			  </div> 	

			<script type="text/javascript">
				$(function () {
						$("#Anyother").click(function () {
						if ($(this).is(":checked")) {
							$("#dayAnyother").show();
						} else {
							$("#dayAnyother").hide();
						}
					});
			});
			</script>
			  <div class="row" >
					<div class="col-lg-12">
			  <div class="col-sm-6" style="margin-top:10px;">Sales days lost</div>
			  <div class="col-sm-6" style="margin-top:10px;">
			  <input type="text" class="form-control" name="SalesDayLost" id="SalesDayLost" value="<?php echo $data->SalesDayLost;?>">
			  
				  </div>
					<div>
			  </div> 	
				</div>
			</div>
		</div>	

</th>
	</tr>

    </table>
			 </div>
	
			 
           </div>

<script type="text/javascript">
    $(function () {
			$("#PreMonsoonRWTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvpreRWTesting").show();
            } else {
                $("#dvpreRWTesting").hide();
            }
        });
			
			$("#PreMonsoonTWTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvpretretTWtesting").show();
            } else {
                $("#dvpretretTWtesting").hide();
            }
        });

        $("#PostMonsoonRWTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvRWTesting").show();
            } else {
                $("#dvRWTesting").hide();
            }
        });

				$("#PostMonsoonTWtesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPostTWtesting").show();
            } else {
                $("#dvPostTWtesting").hide();
            }
        });
					$("#RejectWaterTesting").click(function () {
            if ($(this).is(":checked")) {
                $("#dvRejectTWtesting").show();
            } else {
                $("#dvRejectTWtesting").hide();
            }
        });
				
 });
</script>
			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_4">
               
			<div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Capital expenditure</h3>
			  </div>

			<div class="row">
			 <?php 
					$TotalCost = $data->MachineryCost+$data->RawWaterSourceCost+$data->BuildingCost+$data->LandCost+$data->ElectricityCost;
			  ?>
			<div class="col-lg-12">
			<div class="col-sm-6">Total CapEx</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="Total_cost" id="Total_cost" value="<?php echo $TotalCost;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Machinery cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="MachineryCost1" id="MachineryCost1" value="<?php echo $data->MachineryCost;?>" ></div>
			</div>

			<div class="col-lg-12">
			<div class="col-sm-6">Raw water source cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="RawWaterSourceCost1" id="RawWaterSourceCost1" value="<?php echo $data->RawWaterSourceCost;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Building cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="BuildingCost" id="BuildingCost" value="<?php echo $data->BuildingCost;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Land cost</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="LandCost" id="LandCost" value="<?php echo $data->LandCost;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Electricity connection cost</div>
			<div class="col-sm-6"> <input type="text" class="form-control"  name="ElectricityCost" id="ElectricityCost" value="<?php echo $data->ElectricityCost;?>" ></div>
			</div>
		 </div>
			</div>
		 	<?php  $ProdDetail=$this->model->getPatPlantProdDetail($token); 
			 ?>
		 <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Revenue</h3>
			  </div>
			  <table class="table table-bordered table-hover">
			  <thead>
			  <tr>
				<td></td>
				<th>Price/Ltr</th>
				<th>Vol in Ltr</th>
				<th>Total</th>
			  </tr>
			</thead>
						
			<tr>
				<th>At Plant</th>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
					
				<tr>
				<th>Home Delivery</th>
				<td></td>
				<td></td>
				<td></td>
			  </tr>	
			 <?php  
			 foreach($ProdDetail as $key=>$val){ 
				$price=$val->Price;
				$vol = $val->Volume;
				$totalpvcost= $price*$vol;
			if($val->DPName!=''){ ?>
			<tr>
				<th><?php echo $val->DPName;?></th>
				<td><?php echo $val->Price;?></td>
				<td><?php echo $val->Volume;?></td>
				<td><?php echo $totalpvcost; ?></td>
			  </tr>	
			<?php } } ?>			  
			</table>
		 </div>
		 
		 
		  <div class="box box-primary">
			  <div class="box-header with-border">
              <h3 class="box-title">Operating expense (per month)</h3>
			  </div>
				<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6">Raw water bill(If any)</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="WaterBill" id="WaterBill" value="<?php echo $data->WaterBill; ?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Rent of station</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="Rent" id="Rent" value="<?php echo $data->Rent;?>"></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Transport Expense</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="TransportExpense" id="TransportExpense" value="<?php echo $data->TransportExpense;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Chemicals and other consumables</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="ChemicalAndOtherConsumable" id="ChemicalAndOtherConsumable" 
				  value="<?php echo $data->ChemicalAndOtherConsumable;?>" ></div>
			</div>
				<div class="col-lg-12">
			<div class="col-sm-6">Electricity bill</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="ElectricityBill" id="ElectricityBill" value="<?php echo $data->ElectricityBill;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Operator salary</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OperatorSalary" id="OperatorSalary" value="<?php  echo $data->OperatorSalary; ?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Generator expense </div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="Electricityoutage" id="Electricityoutage" value="<?php echo $data->GeneratorMaintainance;?>" ></div>
			</div>
			</div>
			
			<div class="box box-primary">			  
			<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6"><input type="checkbox" value="MiscellaneousChk" name="MiscellaneousChk" id="MiscellaneousChk" <?php 
			if($data->MiscellaneousChk ==1){
				echo "CHECKED";
			}?>>Miscellaneous/Any other</div>
			<div class="col-sm-6"></div>
			</div>
			<?php 
			if($data->MiscellaneousChk ==1){ ?>
				<div id="Miscellanyoth" style="display:block">
			<?php }else{ ?>
				<div id="Miscellanyoth" style="display:none">
			<?php 	}?>
			
			<div class="col-lg-12">
			<div class="col-sm-2">S No.</div>
			<div class="col-sm-5">Expense</div>
			<div class="col-sm-5">Amount</div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-2">1</div>
			<div class="col-sm-5"><input type="text" name="Expense1" id="Expense1" value="<?php echo $data->MiscFirstText;?>"></div>
			<div class="col-sm-5"><input type="number" name="Amount1" id="Amount1" value="<?php echo $data->MiscFirstAmount;?>"></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-2">2</div>
			<div class="col-sm-5"><input type="text" name="Expense2" id="Expense2" value="<?php echo $data->MiscSecondText;?>"></div>
			<div class="col-sm-5"><input type="number" name="Amount2" id="Amount2" value="<?php echo $data->MiscSecondAmount;;?>"></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-2">3</div>
			<div class="col-sm-5"><input type="text" name="Expense3" id="Expense3" value="<?php echo $data->MiscThirdText;?>"></div>
			<div class="col-sm-5"><input type="number" name="Amount3" id="Amount3" value="<?php echo $data->MiscThirdAmount;?>"></div>
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Monthly Service/ maintenance charge</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="ServiceCharge" id="ServiceCharge" value="<?php echo $data->ServiceCharge;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">Asset renewal fund</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="AssestRenewalFund" id="AssestRenewalFund" value="<?php echo $data->AssestRenewalFund;?>" ></div>
			</div>
			</div>
			</div>	
			<script type="text/javascript">
				$(function () {
						$("#MiscellaneousChk").click(function () {
							if ($(this).is(":checked")) {
							$("#Miscellanyoth").show();
						} else {
							$("#Miscellanyoth").hide();
						}
					});
					});
			</script>    

			<div class="box box-primary">			  
			<div class="row">
			<div class="col-lg-12">
			<div class="col-sm-6">  Financial sustainability</div>
			<div class="col-sm-6"></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">OpEx</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OPEx" id="OPEx" value="<?php echo $data->OPEx;?>" ></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">OpEx + Service/maintenance charge</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OPExservicecharge" id="OPExservicecharge" value="<?php echo $data->OPExservicecharge;?>"></div>
			</div>
			<div class="col-lg-12">
			<div class="col-sm-6">OpEx + Service / maintenance charge + Asset renewal fund</div>
			<div class="col-sm-6"><input type="text" class="form-control"  name="OPExservicechargemaintenance" id="OPExservicechargemaintenance"
							value="<?php echo $data->OPExservicechargemaintenance;?>" ></div>
			</div>
			</div>
			</div>			  
				</div>
				</div>


<script type="text/javascript">
$(document).ready(function(){
    var counter = 2;

    $("#addupload").click(function () {

	if(counter>15){
            alert("Only 15 Distribution/Distance points Allow");
            return false;
	}

	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDivupload' + counter);

	var template1 = '<div class="col-lg-12"><div class="col-lg-6"></div>' + '<div class="col-lg-4"><input type="file" name="fileupload[]" id="exampleInputFile' + counter + '" value="" class="form-control"></div>';
	
	template1 += '</div>';

	console.log(template1);

	// newTextBoxDiv.after().html(template1 + template2);
	newTextBoxDiv.append(template1);
	// );

	newTextBoxDiv.appendTo("#TextBoxesGroupupload");

	counter++;
     });

     $("#removeButton").click(function () {
	if(counter==1){
          alert("No more textbox to remove");
          return false;
       }

	counter--;

        $("#TextBoxDivupload" + counter).remove();

     });

   
  });
</script>

			  <!-- /.tab-pane -->
			   <div class="tab-pane" id="tab_5">
                
			<div class="box box-primary">
			  <div class="box-header with-border">
               <h3 class="box-title">Institutional sustainability</h3>
			  </div>

					<div class="row">
					<div class="col-lg-12">
									<div class="col-lg-6"><b>Approved Yes/No</b></div>
									<div class="col-lg-6"><b>Approved Yes/No</b></div>
					</div>


						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Local government approval</div>
									<div class="col-lg-6">
									<select class="form-control" name="GramPanchayatApproval" id="GramPanchayatApproval">
										<?php foreach($approval as $key=>$res){ 
										if($data->GramPanchayatApproval ==$key){
										?>
															<option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
															<?php }else{ ?>
										<option value="<?php echo $key;?>"><?php echo $res;?></option>
										<?php }} ?>
                 </select>
									</div>
					</div>

						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Legal electicity connection</div>
									<div class="col-lg-6">
								 <select class="form-control" name="LegalElectricityConnection" id="LegalElectricityConnection">
                    <?php foreach($approval as $key=>$res){ 
										if($data->LegalElectricityConnection ==$key){
										?>
															<option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
															<?php }else{ ?>
										<option value="<?php echo $key;?>"><?php echo $res;?></option>
										<?php }} ?>
                 </select>
									</div>
					</div>

						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Premises and land</div>
									<div class="col-lg-6">
								 <select class="form-control" name="LandApproval" id="LandApproval">
                   <?php foreach($approval as $key=>$res){ 
										if($data->LandApproval ==$key){
										?>
															<option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
															<?php }else{ ?>
										<option value="<?php echo $key;?>"><?php echo $res;?></option>
										<?php }} ?>
                 </select>
									</div>
					</div>


						<div class="col-lg-12" style="margin-top: 10px;">
									<div class="col-lg-6">Raw source water</div>
									<div class="col-lg-6">
								  <select class="form-control" name="RawWaterSourceApproval" id="RawWaterSourceApproval">
										<?php foreach($approval as $key=>$res){ 
										if($data->RawWaterSourceApproval ==$key){
										?>
										<option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
										<?php }else{ ?>
										<option value="<?php echo $key;?>"><?php echo $res;?></option>
										<?php }} ?>
                 						</select>
									</div>
								</div>

				

		<div class="col-lg-12" style="margin-top: 10px;">
					<div class="col-lg-6">Document Upload</div>
					<div class="col-lg-4">
					<div class="form-group">
						<input type="file" name="fileupload[]" id="exampleInputFile" value="" class="form-control"> </div>
					</div>
				<div class="col-lg-2"><input type='button' value='Add' id='addupload' Class="btn btn-info pull-right"></div>
	</div>
				
				<div id='TextBoxesGroupupload'>
				<div id="TextBoxDivupload1">
				</div>
				</div>
				<div class="col-lg-12" style="margin-top: 10px;">
				<div class="col-lg-6"></div>
				<?php 
				$documentstr = $this->model->getDocument($token);
				$documentstr1 = Count($documentstr);
				for($i=0; $i< $documentstr1; $i++){
				 $PlantInstitutionalid = $documentstr[$i]->PlantInstitutionalDocumentID;
				?>
				<div class="col-lg-6">
				<a href="#" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Image<?php echo $i;?> </a>
				<div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
					
						<div class="modal-dialog modal-md ">
						 <?php print form_open_multipart($this->router->class.'/'.uploadDocument.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
							<div class="modal-content">
								<div class="modal-body">
								<img src="<?php echo base_url()."datafiles/".$token.$data->POGUID.'Report'.$i.'.jpg';?>" width="250px" hight="250px">
								</div>
								<div class="modal-footer">
								 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
						<?php print form_close(); ?>
					</div>
					
				</div>
				<?php } ?>
		
				
			</div>
		</div>
		    
			 </div>
		</div>
			  <!-- /.tab-pane -->
	<div class="tab-pane" id="tab_6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Enviromental sustainability</h3>
			</div>
					
				<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">
				<input type="checkbox" name="checkRWQuantification" id="checkRWQuantification"<?php if($data->checkRWQuantification==0){
					  echo "";}else{ echo "CHECKED";}?>  >
                    Reject water Produced
							</div>
							<div class="col-lg-6">
							<input type="text" class="form-control"  name="WaterProductionDaily" id="WaterProductionDaily" value="<?php echo $data->WQuantification;?>" ></div>
					</div>
				</div>
		</div>

		<div class="row">
				<?php if($data->CheckRWUtilisation==1){ ?>
					<div class="col-lg-12">
							<div class="col-lg-6" >
							 <input type="checkbox" value="<?php echo $data->CheckRWUtilisation; ?>" name="CheckRWUtilisation" id="CheckRWUtilisation" checked >
                Reject water utilised
							</div>
							<div class="col-lg-6">
							<div class="col-lg-6" id="showtotalCount"></div>
							
							</div>
					</div>
				<?php }else{ ?>
					<div class="col-lg-12">
							<div class="col-lg-6">
							 <input type="checkbox" value="<?php echo $data->CheckRWUtilisation; ?>" name="CheckRWUtilisation" id="CheckRWUtilisation" >
                Reject water utilised
							</div>
							<div id="dvUtilizshow" style="display:none;">	
							<div class="col-lg-6" id="showtotalCount">
							</div>
							
							</div>
					</div>
				<?php } ?>
					<?php if($data->CheckRWUtilisation==1){ ?>
					<div id="dvUtilizActive" style="display:block;">	
					<?php } else{ ?>
					<div id="dvUtilizActive" style="display:none;">	
					<?php } ?>

				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"> <b>Activity</b></div>
				<div class="col-lg-4"> <b>Litres/Day</b></div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity1" id="UtilizationActivity1" value="<?php echo $data->UtilizationActivity1;?>" ></div>
				<div class="col-lg-4"><input type="number" class="form-control"  name="UtilizationOther1" id="UtilizationOther1" onkeydown="Utilizationadd()" onkeyup="Utilizationadd()" onclick="Utilizationadd()"  value="<?php echo $data->UtilizationOther1; ?>" >	</div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity2" id="UtilizationActivity2" value="<?php echo $data->UtilizationActivity2;?>" ></div>
				<div class="col-lg-4"><input type="number" class="form-control"  name="UtilizationOther2" id="UtilizationOther2"  onkeyup="Utilizationadd()" onclick="Utilizationadd()"  value="<?php echo $data->UtilizationOther2;?>" >	</div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity3" id="UtilizationActivity3" value="<?php echo $data->UtilizationActivity3;?>" ></div>
				<div class="col-lg-4"><input type="number" class="form-control"  name="UtilizationOther3" id="UtilizationOther3" onkeydown="Utilizationadd()" onkeyup="Utilizationadd()" onclick="Utilizationadd()"  value="<?php echo $data->UtilizationOther3;?>" >	</div>
				</div>
				<div class="col-lg-12">
				<div class="col-lg-4"></div>
				<div class="col-lg-4"><input type="text" class="form-control"  name="UtilizationActivity4" id="UtilizationActivity4" value="<?php echo $data->UtilizationActivity4;?>" ></div>
				<div class="col-lg-4"><input type="number" class="form-control"  name="UtilizationOther4" id="UtilizationOther4" onkeydown="Utilizationadd()" onkeyup="Utilizationadd()" onclick="Utilizationadd()"  value="<?php echo $data->UtilizationOther4;?>" >	</div>
				</div>
				</div>
					<div class="col-lg-12">
							<div class="col-lg-6" style="margin-top:10px;">
							<input type="checkbox" value="<?php echo $data->CheckRWDisposal;?>" name="CheckRWDisposal" id="CheckRWDisposal"<?php if($data->CheckRWDisposal==0){
					  echo "";}else{ echo "CHECKED";}?>>
                   Reject water disposed
							</div>
							<div class="col-lg-6" style="margin-top:10px;">
							<input type="text" class="form-control"  name="CheckRWDisposal" id="CheckRWDisposal" value="<?php echo $data->WDisposal;?>" ></div>
					</div>
				</div>
	

		<div class="row">
					<div class="col-lg-12">
							<div class="col-lg-6">
							<input type="checkbox" value="<?php echo $data->CheckRWDisposal;?>" name="CheckRWDisposal" id="CheckRWDisposal"<?php if($data->CheckRWDisposal==0){
					  echo "";}else{ echo "CHECKED";}?>>Ground water recharge / rain water harvesting</div>
							<div class="col-lg-6"> </div>
					</div>
				</div>
		</div>
 								
  <script type="text/javascript">
    $(function () {
			$("#CheckRWUtilisation").click(function () {
			      if ($(this).is(":checked")) {
                $("#dvUtilizActive").show();
				 $("#dvUtilizshow").show();
				
            } else {
                $("#dvUtilizActive").hide();
				 $("#dvUtilizshow").hide();
            }
        });

		
		});
</script>       	

	</div>
	</div>

              <div class="row">
              	<div class="col-lg-12">
              		
 <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?>
     		<?php print form_close(); ?>
              	</div>
              </div>
              <!-- /.tab-pane -->
            </div>
			
            <!-- /.tab-content -->
          </div>
		  
          <!-- nav-tabs-custom -->
        </div>

		</div>
		
</div>
</div>

 <script type="text/javascript">
function Utilizationadd() {
	
	var Checkutlization = parseInt(document.getElementById("CheckRWUtilisation").value);
	var utilization1 = parseInt(document.getElementById("UtilizationOther1").value);
	if (isNaN(utilization1) == true) {
         utilization1 = 0;
     }
	 var utilization2 = parseInt(document.getElementById("UtilizationOther2").value);
	if (isNaN(utilization2) == true) {
         utilization2 = 0;
     }
	  var utilization3 = parseInt(document.getElementById("UtilizationOther3").value);
	if (isNaN(utilization3) == true) {
         utilization3 = 0;
     }
	  var utilization4 = parseInt(document.getElementById("UtilizationOther4").value);
	if (isNaN(utilization4) == true) {
         utilization4 = 0;
     }
	
	document.getElementById("showtotalCount").innerHTML = utilization1 + utilization2 + utilization3 + utilization4;

}
</script>  
    

   
    <!-- /.content -->