  <script type="text/javascript" src="<?php print FJS; ?>jquery-1.12.4.js"></script>

  <script type="text/javascript">
  /* Formatting function for row details - modify as you need */
function format (d) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            
            "<td style='padding-left:50px;'><a href='<?php echo site_url(); ?>/PATool/edit/"+d.PlantGUID+"'>Eval_1</a></td>"+
        '</tr>'+
      
    '</table>';
}
 
$(document).ready(function() {
    var table = $('#example').DataTable( {
       "ajax": "<?php echo site_url();?>/Patajax/getSWNID",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "SWNID" }
             
                      
        ],
        "order": [[1, 'asc']]
    } );
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it*
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );

</script>
<style type="text/css">
  td.details-control {
    background: url('<?php print FIMAGES;?>details_open.png') no-repeat center center;
    background-size: 20px 20px;
    padding-top: 40px;    
}
tr.shown td.details-control {
    background: url('<?php print FIMAGES;?>details_close.png') no-repeat center center;
    background-size: 20px 20px;
    padding-top: 20px;
}  
</style>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PAT Tool Management
        <small>preview of PAT Management</small>
      </h1>
	  
	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
       </a>-->
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">PAT Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          
		<div class="col-xs-12">
         
          
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>

        </div>
			
       
 <div class="col-xs-3" style="padding-top: 5px;  height: 500px; overflow-y: scroll;">
    <label style="background-color: #674e9e; width: 600px; color: #ffffff; padding-left:10px;" >Operations</label>

          <table id="example" class="display" width="100%" cellspacing="0">
          <thead>
              <tr>
                  <th style="width: 30px;"></th>
                  <th  style="padding-left: 10px;">SWNID</th>
              </tr>
          </thead>
          <!-- <tfoot>
              <tr>
                  <th style="width:30px;"></th>
                  <th style="padding-left: 10px;">SWNID</th>
              </tr>
          </tfoot> -->
      </table>
    </div>

  
<div class="col-xs-9" style="padding-top: 5px;  height: 500px; overflow-y: scroll;">
  <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Social <br/> Sustainability</a></li>
              <li><a href="#tab_3" data-toggle="tab">Operational <br/> Sustainability</a></li>
        <li><a href="#tab_4" data-toggle="tab">Financial <br/> Sustainability</a></li>
              <li><a href="#tab_5" data-toggle="tab">Institutional <br/> Sustainability</a></li>
            <li><a href="#tab_6" data-toggle="tab">Environmental <br/> Sustainability</a></li>
           </ul>
       
      
      
                
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
        <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
         <input type="hidden" class="form-control"  name="PlantUID" id="PlantUID" 
          value="" >
         <input type="hidden" class="form-control"  name="PlantPOUID" id="PlantPOUID" 
          value="" >

        <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Assessors details</h3>
        </div>
       <table  class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Visit Date</th>
            <th>
          <div class="form-group">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
                  </div>
            <input type="text" class="form-control pull-right datepicker" value="" name="VisitDate" >
            </div>
          </div>
          </th>
                  <th>Assessed by</th>
           <th>
           <Select name="UserID" id="UserID"class="form-control" >
          <option value="">Select</option>
          <?php $Assessedby = $this->model->getAssessedby();
          foreach($Assessedby as $row){
          if($row->UserID==$data->UserID){
          ?>
          <option value="<?php echo $row->UserID; ?>" SELECTED><?php echo $row->FirstName." ".$row->LastName;?></option>
          <?php }else{ ?>
           <option value="<?php echo $row->UserID; ?>" ><?php echo $row->FirstName." ".$row->LastName;?></option>
          <?php  }} ?>
          </select>
          </th>
                  <th col="2">Location:</th>
          </tr>
                </thead>
                <tbody>
        
                <tr>
                  <th>Assessing agency</th>
                  <th>
          <Select name="AuditingAgency" id="AuditingAgency"class="form-control" >
          <option value="">Select</option>
          
          <?php $Agency = $this->model->getAgency();
          foreach($Agency as $row){
          if($row->AgencyID==$data->AuditingAgency){
          ?>
          <option value="<?php echo $row->AgencyID; ?>" SELECTED><?php echo $row->AgencyName;?></option>
          <?php }else{ ?>
           <option value="<?php echo $row->AgencyID; ?>" ><?php echo $row->AgencyName;?></option>
          <?php  }} ?>
  
                  </th>
                  <th>Assessing address</th>
                  <th> <input type="text" class="form-control"  name="AuditingAgencyAddress" id="AuditingAgencyAddress"
          value="<?php echo $data->AuditingAgencyAddress;?>" ></th>
                  <th>Latitude</th>
          <th><input type="text" class="form-control"  name="Latitude" id="Latitude"  value="<?php echo $data->Latitude;?>" ></th>
                </tr>
                <tr>
                  <th>Contact name</td>
                  <th><input type="text" class="form-control"  name="ContactName1" id="ContactName1" value="<?php echo $data->ContactName;?>" ></th>
                  <th>Email</th>
                  <th><input type="email" class="form-control"  name="Email" id="Email" value="<?php echo $data->Email;?>" ></th>
                  <th>Longitude </th>
          <th><input type="text" class="form-control"  name="Longitude" id="Longitude" value="<?php echo $data->Longitude;?>" ></th>
                </tr>
                </tbody>
              </table>
       </div>
        
        <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Plant Address</h3>
      </div>
          <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>PlantID/Plant name</th>
                  <th><input type="text" class="form-control"  name="VillageName" id="VillageName" 
          value="<?php echo $data->VillageName;?>" required></th>
                  <th>Country</th>
                  <th>
           <Select name="Country" id="Country"class="form-control" >
            <?php $Country = $this->model->getCountry();
          foreach($Country as $row){
          if($row->CountryID==$data->CountryID){
          ?>
          <option value="<?php echo $row->CountryID; ?>" SELECTED><?php echo $row->CountryName;?></option>
          <?php }} ?>
           
          </select>
         </th>
                  <th >State</th>
          <th>
           <Select name="State" id="State"class="form-control" >
            <?php $State = $this->model->getState();
          foreach($State as $row){
          if($row->StateID==$data->StateID){
          ?>
          <option value="<?php echo $row->StateID;?>" SELECTED><?php echo $row->StateName;?></option>
          <?php }}?>
          </Select>
          </th>
          </tr>
                </thead>
                <tbody>
                <tr>
                  <th>District</th>
                  <th>
          <Select name="District" id="District"class="form-control" >
            <?php $District = $this->model->getDistrict();
          foreach($District as $row){
          if($row->DistrictID==$data->District){
          ?>
          <option value="<?php echo $row->DistrictID; ?>" SELECTED><?php echo $row->DistrictName;?></option>
          <?php }}?>
          </Select>
                  </th>
                  <th>City</th>
                  <th> <input type="text" class="form-control"  name="BlockName" id="BlockName" 
          value="<?php echo $data->BlockName;?>" required></th>
                  <th>Ward number</th>
          <th><input type="text" class="form-control"  name="VillageName" id="VillageName" 
          value="<?php echo $data->VillageName;?>" required></th>
                </tr>
                <tr>
                  <th>Pincode</td>
                  <th><input type="text" class="form-control"  name="PinCode" id="PinCode" 
          value="<?php echo $data->PinCode;?>" required></th>
                  <th colspan="2">
                         <input type="radio" name="RUrban" id="RUrban" value="Urban" disabled >Urban
             <input type="radio" name="RUrban" id="RUrban" disabled value="Rural" checked>Rural
                   </th>
                  
                  <th></th> 
          <th></th>
                </tr>
                </tbody>
              </table>
       </div>
       
        <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Existing Plant Details</h3>
      </div>
          <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Date of establishment </th>
                  <th>
          
           <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
            <input type="text" class="form-control pull-right datepicker" name="EstablishmentDate" value="">
                </div>
          
          
          </th>
                  <th>Age of plant(month)</th>
                  <th><input type="text" class="form-control"  name="AgeOfPlant" id="AgeOfPlant" 
          value="<?php echo $data->AgeOfPlant;?>" required></th>
                  <th>Plant capacity(lph)</th>
          <th>
          <Select name="PlantSpecificationID" id="PlantSpecificationID"class="form-control" >
          <option value="">All</option>
          <?php $PlantSpecification = $this->model->getPlantSpecification();
          foreach($PlantSpecification as $row){
          if($row->PlantSpecificationID==$data->PlantSpecificationID){
          ?>
          <option value="<?php echo $row->PlantSpecificationID; ?>" SELECTED><?php echo $row->PlantSpecificationName;?></option>
          <?php }else{ ?>
           <option value="<?php echo $row->PlantSpecificationID; ?>" ><?php echo $row->PlantSpecificationName;?></option>
          <?php  }} ?>
          
          </th>
          </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Plant Manufactured by</th>
                  <th>
          <Select name="PlantManufacturerID" id="PlantManufacturerID"class="form-control" >
          <option value="">All</option>
          <?php $PlantManufacturer = $this->model->getPlantManufacturer();
          foreach($PlantManufacturer as $row){
          if($row->PlantManufacturerID==$data->PlantManufacturerID){
          ?>
          <option value="<?php echo $row->PlantManufacturerID; ?>" SELECTED><?php echo $row->PlantManufacturerName;?></option>
          <?php }else{ ?>
           <option value="<?php echo $row->PlantManufacturerID; ?>" ><?php echo $row->PlantManufacturerName;?></option>
          <?php  }} ?>
                  </th>
                  <th>Remote Monitoring System</th>
                  <th> <Select type="Select" class="form-control"  name="RemoteMonitoringSystem" id="RemoteMonitoringSystem" placeholder=" Enter Remote Monitoring System" required>
          <option value"0">No</option>
          <option value"1">Yes</option>
          </select>
          </th>
                  <th></th>
          <th></th>
                </tr>
                <tr>
          <th colspan="6">
            <table class="table table-bordered table-hover" >
          <thead>
          <tr>
           <th colspan="6">
           <div class="box-header with-border">
            <h3 class="box-title"> Contaminant in raw water</h3>
        </div>
          </th>
           </tr>
           </thead>
           
           <?php 
           $WQC=array();
           $Contnts = $this->model->getPatWaterContaminants($token);
              $WaterQualityChallenge = $this->model->getWaterQualityChallenge();
          
          ?>  
          
           <tr>
           <?php foreach($WaterQualityChallenge as $key => $val){ 
            //echo $Contnts[$key]->WaterQualityChallengeID."-". $val->WaterQualityChallengeID;
           
           
            if($val->WaterQualityChallengeID == $Contnts[$key]->WaterQualityChallengeID){
              
           ?>
            <td colspan="6">
              <input type="checkbox" value="<?php echo $val->WaterQualityChallengeID;?>" name="WQC[]" id="WQC<?php echo $val->WaterQualityChallengeID;?>" checked > <?php echo $val->WaterQualityChallengeName;?>
            </td>
              <?php }else{ 
              
              ?>
              
            <td colspan="6">
              <input type="checkbox" value="<?php echo $val->WaterQualityChallengeID;?>" name="WQC[]" id="WQC<?php echo $val->WaterQualityChallengeID;?>"> <?php echo $val->WaterQualityChallengeName;?>
            </td>
              <?php } }?>
           
           </tr>         
                  </table>
        </th>
          </tr>
         <tr>
          <th colspan="6">
            <table class="table table-bordered table-hover" >
          <tr>
           <th  colspan="6">Treatment steps</th>
           </tr>
           <?php 
           $Patplt = $this->model->getPlantPurStep($token);
           $PlantPurificationStep = $this->model->getPlantPurificationStep();?>  
           <tr>
           <?php foreach($PlantPurificationStep as $key=>$row){ 
              if($Patplt[$key]->PlantPurificationstepID==$row->PlantPurificationStepID){
           ?>
            <td colspan="6">
              <input type="checkbox" value="<?php echo $row->PlantPurificationStepID;?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID;?>" checked > <?php echo $row->PlantPurificationStepName;?>
            </td>
              <?php }else{ ?>
              
            <td colspan="6">
              <input type="checkbox" value="<?php echo $row->PlantPurificationStepID;?>" name="PlantPurificationStep[]" id="PlantPurificationStep<?php echo $row->PlantPurificationStepID;?>" > <?php echo $row->PlantPurificationStepName;?>
            </td>
              <?php } }?>
           
              </tr>
              
              </table>
          </tr>
                  </table>
        </th>
          </tr>
          </tbody>
        </table>
       </div>
        
        <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Asset details</h3>
      </div>
       	<div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Machinery</b></td>
								<td></td>
								<td></td>
								<td> </td>
								<td><input type="text" name="MachineryCost" id="MachineryCost" size="5" class="form-control" value="<?php echo $data->MachineryCost;?>"></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunder();?> 
				<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->MachineryOther){
				?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="MachineryOther" id="MachineryOther" checked> <b><?php echo $row->AssestFunderName;?></b></td>
				<?php }else{ ?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="MachineryOther" id="MachineryOther"> <b><?php echo $row->AssestFunderName;?></b></td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
  </div>

	<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Land</b></td>
								<td></td>
								<td></td>
								<td><input type="text" name="LandCost1" id="LandCost1" size="5" class="form-control" value="<?php echo $data->LandCost;?>"></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag2();?> 
				<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->LandOther){
				?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="LandOther" id="LandOther" checked> <b><?php echo $row->AssestFunderName;?></b>
</td>
				<?php }else{ ?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="LandOther" id="LandOther" > <b><?php echo $row->AssestFunderName;?></b>
</b></td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
  </div>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Building</b></td>
								<td></td>
								<td></td>
								<td><input type="text" name="BuildingCost1" id="BuildingCost1" size="5" class="form-control" value="<?php echo $data->BuildingCost;?>"></td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag3();?> 
				<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->BuildingOther){
				?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="BuildingOther" id="BuildingOther" checked> <b><?php echo $row->AssestFunderName;?></b>
</td>
				<?php }else{ ?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="BuildingOther" id="BuildingOther" > <b><?php echo $row->AssestFunderName;?></b>
</td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
  </div>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Raw water source</b></td>
								<td></td>
								<td></td>
								<td>
<input type="text" name="RawWaterSourceCost" id="RawWaterSourceCost" size="5" class="form-control" value="<?php echo $data->RawWaterSourceCost;?>">
</td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag4();?> 
				<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->RawWaterSourceOther){
				?>
       <td>
	<input type="radio" value="<?php echo $row->AssestFunderID;?>" name="RawWaterSourceOther" id="RawWaterSourceOther" checked> <b><?php echo $row->AssestFunderName;?></b>
	</td>
				<?php }else{ ?>
       <td><input type="radio" value="<?php echo $row->AssestFunderID;?>" name="RawWaterSourceOther" id="RawWaterSourceOther" > <b><?php echo $row->AssestFunderName;?></b></td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
  </div>

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td><b>Electricity connection</b></td>
								<td></td>
								<td></td>
								<td>
<input type="text" name="ElectricityCost1" id="ElectricityCost1" size="5" class="form-control" value="<?php echo $data->ElectricityCost;?>">
</td>
							</tr>
						</tbody>
						 <tbody>
							  <tr>
			<?php $AssestFunder = $this->model->getAssestFunderFlag5();?> 
				<?php foreach($AssestFunder as $row){
					if($row->AssestFunderID==$data->ElectricityOther){
				?>
       <td>
	<input type="radio" value="<?php echo $row->AssestFunderID;?>" name="ElectricityOther" id="ElectricityOther" checked> <b><?php echo $row->AssestFunderName;?></b>
	</td>
				<?php }else{ ?>
       <td>
<input type="radio" value="<?php echo $row->AssestFunderID;?>" name="ElectricityOther" id="ElectricityOther" > <b><?php echo $row->AssestFunderName;?></b></td>
				<?php } }?>
        </tr>
      </tbody>
    </table>
  </div>

   <table class="table table-bordered table-hover" >
			<thead>
      <tr>
        <th colspan="3">Aggregator</th>
        <th colspan="3"><input type="text" name="WaterBrandName" id="WaterBrandName" class="form-control" value="<?php echo $data->WaterBrandName;?>"></th>
       </tr>
       </tr>
         </thead>
       </table>
  </div>
       
               
    </div>
              <div class="tab-pane" id="tab_2">
                
        <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Social sustainability</h3>
      </div>
      
       <table  class="table table-bordered table-hover">
                <thead>
               <tr>
                  <th>Population </th>
                  <th><input type="text" class="form-control"  name="Population" id="Population"  value="<?php echo $data->Population;?>" required></th>
                  <th>Number of household</th>
                  <th><input type="text" class="form-control"  name="NoOfHousehold" id="NoOfHousehold" value="<?php echo $data->NoOfHousehold;?>" required></th>
                  <th>Number of household within 500m</th>
          <th><input type="text" class="form-control"  name="NoOfHouseholdWithin2km" id="NoOfHouseholdWithin2km" 
          value="<?php echo $data->NoOfHouseholdWithin2km;?>" required></th>
        </tr>
         <tr>
                  <th>Number of household registered </th>
                  <th><input type="text" class="form-control"  name="NoOfhhregistered" id="NoOfhhregistered" 
          value="<?php echo $data->NoOfhhregistered;?>" required></th>
                  <th>Average number of monthly water cards/RFID cards</th>
                  <th><input type="text" class="form-control"  name="Average_number_of_monthly_water_cards" id="Average_number_of_monthly_water_cards" 
          value="<?php echo $data->Population;?>" required></th>
                  <th>Distribution</th>
          <th>
          <Select type="Select" class="form-control"  name="Distribution" id="Distribution]">
          <?php foreach($approval as $key=>$res){ 
          if($data->Distribution ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
          </select>
          
        </tr>
          
         <tr>
                 <td colspan="6">
         <table  class="table table-bordered" >
         <tr>
         <th colspan="6"><div class="box box-primary">Social composition</th>
         </tr>
         <tr>
          <th >
          <input type="checkbox" value="<?php echo $data->SC;?>" name="SC" id="SC" checked> SC</th>
          <th ><input type="checkbox" value="<?php echo $data->ST;?>" name="ST" id="ST" checked> ST<th>
          
          <th><input type="checkbox" value="<?php echo $data->OBC;?>" name="OBC" id="OBC" checked> OBC</th>
          <th>
          <label><input type="checkbox" value="<?php echo $data->General;?>" name="General" id="General" checked>General</label>
          </th>
          <th >
          <label><input type="checkbox" value="<?php echo $data->AllInclusion;?>" name="AllInclusion" id="AllInclusion" checked>All Inclusion</label>
          </th>
          </tr>
         </table>
         <td>
        </th>
        </tr>
        </div>
        <tr>
                  <th colspan="6">
          <div class="box box-primary">Distribution/Distance points<div>
          <table  class="table table-bordered table-hover">
          <thead>
           <tr>
            <th>Distribution point</th>
            <th>
            <input type="hidden"  name="PlantGUID" id="PlantGUID" 
            value="<?php echo $token;?>" >
            <input type="text" class="form-control"  name="DPName" id="DPName" 
            value="<?php echo $data->DPName;?>" ></th>
            <th>Distance</th>
            <th><input type="text" class="form-control"  name="Distance" id="Distance" 
            value="<?php echo $data->Distance;?>" ></th>
            
          </tr>
          </thead>
          </table>
          </th>
      </tr>
       </thead>
           </table>
         </div>
     </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
               <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Plant production capacity(litres)</h3>
        </div>
          <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Avg daily water produced in litres</th>
                  <th><input type="text" class="form-control"  name="WaterProductionDaily" 
          id="WaterProductionDaily" value="<?php echo $data->WaterProductionDaily;?>" required></th>
                  
          </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Avg monthly water production in litres</th>
                  <th><input type="text" class="form-control"  name="WaterProductionMonthly" id="WaterProductionMonthly" value="<?php echo $data->WaterProductionMonthly;?>" required>
                  </th>
                 
                </tr>
                <tr>
                  <th>PeakSale</td>
                  <th><input type="text" class="form-control"  name="PeakSale" id="PeakSale" value="<?php echo $data->PeakSale;?>" required></th>
                  
                </tr>
                </tbody>
              </table>
       </div>
       
       
              <!----  Start Here    -->      
        <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Kiosk operator details</h3>
        </div>
          <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th><input type="text" class="form-control"  name="ContactName" id="ContactName" value="<?php echo $data->ContactName;?>" required></th>
                  <th>Designation</th>
                  <th>
          
          <?php $Designation = $this->model->getDesignation();  ?>
          <Select type="Select" class="form-control"  name="Designation" id="Designation" required>
          <?php foreach($Designation as $row) { 
          if($row->DesignationID==$data->DesignationID){
          ?>
          <option value="<?php echo $row->DesignationID;?>" SELECTED><?php echo $row->DesignationName;?></option>
          <?php }else{ ?>
          <option value="<?php echo $row->DesignationID;?>"><?php echo $row->DesignationName;?></option>
          <?php } }?>
          </select>
          
          </th>
          <th>Contact number</th>
                  <th>
          <input type="text" class="form-control"  name="ContactNumber" id="ContactNumber" value="<?Php echo $data->ContactNumber;?>" required>
          </th>
          </tr>
              </table>
       </div>
       <!----- End here -->
       
        <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Training and capacity building</h3>
        </div>
          <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>
          
      <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Training recieved</h3>
        </div>
          <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->ElectricalSafety; ?>" name="ElectricalSafety" id="ElectricalSafety"
            <?php if($data->ElectricalSafety=='0'){ echo "";}else{ echo "CHECKED";}?>>
                     Electrical safety
                    </label>
                  </div>
                   <div class="checkbox">
                    <label>
                       <input type="checkbox" value="<?php echo $data->PlantOM; ?>" name="PlantOM" id="PlantOM" 
             <?php if($data->PlantOM=='0'){ echo "";}else{ echo "CHECKED";}?>>
                      Plant operation and managment
                    </label>
                  </div>
        <div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->WaterQuality; ?>" name="WaterQuality" id="WaterQuality" <?php if($data->WaterQuality=='0'){ echo "";}else{ echo "CHECKED";}?>>
                      Water quality
                    </label>
                  </div>
        <div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->ConsumerAwareness; ?>" name="ConsumerAwareness" id="ConsumerAwareness" <?php if($data->ConsumerAwareness=='0'){ echo "";}else{ echo "CHECKED";}?> >
                      Consumer awareness
                    </label>
                  </div>
          <div class="checkbox" >
                    <label>
                      <input type="checkbox" value="<?php echo $data->ABKeeping; ?>" name="ABKeeping" id="ABKeeping"<?php if($data->ABKeeping=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Accounts and book keeping
                    </label>
                  </div>
          
       </div>
       
       
       
      <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Quality assurance and Hygiene</h3>
        </div>
      <div class="row">
        <div class="col-sm-4">          </div>
        <div class="col-sm-6">
        <select class="form-control" name="Earthing" id="Earthing">
               <?php foreach($Earthing as $key=>$res){ 
          if($data->Earthing ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
            </select>
          </div>
        </div> 

    <div class="row">
      <div class="row text-center">Raw water source</div>
        <div class="col-sm-4">Open well covered</div>
        <div class="col-sm-6">
        <select class="form-control" name="RawWaterOpenwellcovered" id="RawWaterOpenwellcovered">
                <?php foreach($approval as $key=>$res){ 
          if($data->RawWaterOpenwellcovered ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
          </div>
        </div> 

      <div class="row">
        <div class="col-sm-4">Bore well casing</div>
        <div class="col-sm-6">
        <select class="form-control" name="RawWaterBoreWellCasing" id="RawWaterBoreWellCasing">
               <?php foreach($approval as $key=>$res){ 
          if($data->RawWaterBoreWellCasing ==$key){
        ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
        <?php }} ?>
                 </select>
          </div>
        </div>        
      
      <div class="row">
      <div class="row text-center">Treated water tank</div>
        <div class="col-sm-4">Inside the plant</div>
        <div class="col-sm-6">
        <select class="form-control" name="Insidetheplant" id="Insidetheplant">
            <?php foreach($approval as $key=>$res){ 
        if($data->Insidetheplant ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div> 

      <div class="row">
        <div class="col-sm-4">Covered</div>
        <div class="col-sm-6">
        <select class="form-control" name="Covered" id="Covered">
             <?php foreach($approval as $key=>$res){ 
        if($data->Covered ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div>    
      
      
      <div class="row">
      <div class="row text-center">Plant hygiene inside</div>
        <div class="col-sm-4">Cleanliness in plant</div>
        <div class="col-sm-6">
        <select class="form-control" name="CleanlinessinPlant" id="CleanlinessinPlant">
            <?php foreach($approval as $key=>$res){ 
        if($data->CleanlinessinPlant ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div> 

      <div class="row">
        <div class="col-sm-4">Leakage</div>
        <div class="col-sm-6">
        <select class="form-control" name="Leakage" id="Leakage">
            <?php foreach($approval as $key=>$res){ 
        if($data->Leakage ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div> 


      <div class="row">
      <div class="row text-center">Plant hygiene outside</div>
        <div class="col-sm-4">Cleanliness near treatment plant</div>
        <div class="col-sm-6">
        <select class="form-control" name="CleanlinessNearTreatmentPlant" id="CleanlinessNearTreatmentPlant">
            <?php foreach($approval as $key=>$res){ 
        if($data->CleanlinessNearTreatmentPlant ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div> 

      <div class="row">
      <div class="row text-center">Water fill station</div>
        <div class="col-sm-4">Moss/algae growth</div>
        <div class="col-sm-6">
        <select class="form-control" name="Checkmossoralgee" id="Checkmossoralgee">
             <?php foreach($approval as $key=>$res){ 
        if($data->Checkmossoralgee ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div> 
      <div class="row">
        <div class="col-sm-4">Presence of puddle</div>
        <div class="col-sm-6">
        <select class="form-control" name="Presenceofpuddle" id="Presenceofpuddle">
            <?php foreach($approval as $key=>$res){ 
        if($data->Presenceofpuddle ==$key){
      ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
            <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
      <?php }} ?>
                 </select>
          </div>
        </div>        
      </div>

    </th>
      <th valign="top" style="margin-top: 5px" >
      <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Water quality test report from accredited laboratory</h3>
        </div>
      <div class="row">
        <div class="col-sm-4">
      <div class="form-group">Pre monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PreMonsoonRWTesting;?>" name="PreMonsoonRWTesting" id="PreMonsoonRWTesting"
             <?php if($data->PreMonsoonRWTesting=='0'){ echo "";}else{ echo "CHECKED";}?>>
                     Raw water
                    </label>
                  </div>
          <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PreMonsoonRWLabAdr;?>" name="PreMonsoonRWLabAdr" id="PreMonsoonRWLabAdr"
             <?php if($data->PreMonsoonRWLabAdr=='0'){ echo "";}else{ echo "CHECKED";}?>>
                     Treated water
                    </label>
                  </div>
        
        </div>
        </div>
       <div class="form-group">Post monsoon
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PostMonsoonRWTesting;?>" name="PostMonsoonRWTesting" id="PostMonsoonRWTesting"
           <?php if($data->PostMonsoonRWTesting=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Raw water
                    </label>
                  </div>
          <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->PostMonsoonRWLabadr;?>"  name="PostMonsoonRWLabadr" id="PostMonsoonRWLabadr"
           <?php if($data->PostMonsoonRWLabadr=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Treated water
                    </label>
                  </div>
            <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->RejectWaterTesting;?>"   name="RejectWaterTesting" id="RejectWaterTesting"
           <?php if($data->RejectWaterTesting=='0'){ echo "";}else{ echo "CHECKED";}?> >
                     Reject water
                    </label>
                  </div>           
        </div>
      </div>
        </div>  
  <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Reliability of operations</h3>
        </div>
      <div class="row">
        <div class="col-sm-4">Technical downtime</div>
        <div class="col-sm-4">
        <select class="form-control" name="Leakage" id="Leakage">
        <?php foreach($approval as $key=>$res){ 
          if($data->TechnicalDowntime ==$key){
        ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
        <?php }} ?>
                 </select>
          </div>
        </div>  
        
        <div class="row">
        <div class="col-sm-4">No of days</div>
        <div class="col-sm-4">
        <input type="text" class="form-control" name="NoOfDays" id="NoOfDays" value="<?php echo $data->NoOfDays;?>">
          </div>
        </div> 
        <div class="row">
        <div class="col-sm-4">No of reason for downtime</div>
        <div class="col-sm-4">
        <input type="text" class="form-control" name="TechnicalDowntime" id="TechnicalDowntime" value="<?php echo $data->NoOfFault;?>">
          </div>
        </div>  
        
        
        <div class="row">
        <div class="col-sm-4">Sales day lost</div>
        <div class="col-sm-6">
        <input type="text" class="form-control" name="SalesDayLost" id="SalesDayLost" value="<?php echo $data->SalesDayLost;?>">
        
          </div>
        </div>  
      
      
      </div>
       
      
      </div>
    </div>        
      
          </th>
                   </tr>
              </table>
       </div>
       
           </div>
        <!-- /.tab-pane -->
         <div class="tab-pane" id="tab_4">
               
      <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Capital expenditure</h3>
        </div>
        <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Machinery cost</th>
                  <th><input type="text" class="form-control"  name="MachineryCost1" id="MachineryCost1" value="<?php echo $data->MachineryCost;?>" ></th>
                  <th>Raw water source cost</th>
                  <th><input type="text" class="form-control"  name="RawWaterSourceCost1" id="RawWaterSourceCost1" value="<?php echo $data->RawWaterSourceCost;?>" ></th>
                  <th>Building cost</th>
          <th><input type="text" class="form-control"  name="BuildingCost" id="BuildingCost" value="<?php echo $data->BuildingCost;?>" ></th>
          </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Land cost</th>
                  <th><input type="text" class="form-control"  name="LandCost" id="LandCost" value="<?php echo $data->LandCost;?>" >
                  </th>
                  <th>Electricity connection cost</th>
                  <th> <input type="text" class="form-control"  name="ElectricityCost" id="ElectricityCost" value="<?php echo $data->ElectricityCost;?>" ></th>
                  <th>Total cost</th>
          <?php 
          $TotalCost = $data->MachineryCost+$data->RawWaterSourceCost+$data->BuildingCost+$data->LandCost+$data->ElectricityCost;

          ?>
          
          <th><input type="text" class="form-control"  name="Total_cost" id="Total_cost" value="<?php echo $TotalCost;?>" ></th>
                </tr>
                </tbody>
              </table>
     </div>
     
     <?php  
      $ProdDetail=$this->model->getPatPlantProdDetail($token);
   ?>
     <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Revenue</h3>
        </div>
        <table class="table table-bordered table-hover">
        <thead>
        <tr>
        <td></td>
        <th>Price/Ltr</th>
        <th>Vol in Ltr</th>
        <th>Total</th>
        </tr>
      </thead>
      <?php foreach($ProdDetail as $key=>$val){ 
        $price=$val->Price;
        $vol = $val->Volume;
        $totalpvcost= $price*$vol;
      ?>
      <tr>
        <th>At Plant</th>
        <td><?php echo $val->Price;?></td>
        <td><?php echo $val->Volume;?></td>
        <td><?php echo $totalpvcost; ?></td>
        </tr>
      <?php  } ?>
      <tr>
        <th>Home Delivery</th>
        <td></td>
        <td></td>
        <td></td>
        </tr> 
      <tr>
        <th>DP1</th>
        <td></td>
        <td></td>
        <td></td>
        </tr>         
      </table>
     </div>
     
     
      <div class="box box-primary">
        <div class="box-header with-border">
              <h3 class="box-title">Operating expense (per month)</h3>
        </div>
         <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Raw water bill(If any)</th>
                  <th><input type="text" class="form-control"  name="WaterBill" id="WaterBill" value="<?php echo $data->WaterBill; ?>" ></th>
                  <th>Rent of station</th>
                  <th><input type="text" class="form-control"  name="Rent" id="Rent" value="<?php echo $data->Rent;?>" ></th>
                  <th>Transport Expense</th>
          <th><input type="text" class="form-control"  name="TransportExpense" id="TransportExpense" value="<?php echo $data->TransportExpense;?>" ></th>
          <th>Chemicals and other consumables</th>
          <th><input type="text" class="form-control"  name="ChemicalAndOtherConsumable" id="ChemicalAndOtherConsumable" 
          value="<?php echo $data->ChemicalAndOtherConsumable;?>" ></th>
          </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Electricity bill</th>
                  <th><input type="text" class="form-control"  name="ElectricityBill" id="ElectricityBill" value="<?php echo $data->ElectricityBill;?>" required></th>
                  <th>Operator salary</th>
                  <th> <input type="text" class="form-control"  name="OperatorSalary" id="OperatorSalary" value="<?php  echo $data->OperatorSalary; ?>" required></th>
                  <th></th>
          <th></th>
                </tr>
         <tr>
                  <th>Generator for electricity</th>
                  <th><input type="text" class="form-control"  name="Electricityoutage" id="Electricityoutage" value="<?php echo $data->Electricityoutage;?>" required></th>
                  <th></th>
                  <th></th>
                  <th></th>
          <th></th>
                </tr>
                </tbody>
              </table>
        <table  class="table table-bordered table-hover">
        <thead>
        <tr>
                  <th colspan="3">
          <div class="checkbox">
                    <label>
                      <input type="checkbox">
            Miscellaneous/Any other
                    </label>
          <table class="table table-bordered table-hover ">
          <tr>
          <td>Service Charge</td>
          <td><input type="text" class="form-control"  name="ServiceCharge" id="ServiceCharge" value="<?php echo $data->ServiceCharge;?>" required></td>
          <td>Sustainability fund</td>
          <td><input type="text" class="form-control"  name="AssestRenewalFund" id="AssestRenewalFund" value="<?php echo $data->AssestRenewalFund;?>" required></td>
          </tr>
          </table>
                  </div>
          
          <th>
         </tr>
                </thead>
                                
               </table>
              
        <table  class="table table-bordered table-hover">
        <thead>
         
        <tr>
                  <th colspan="3"> Financial sustainability</th>
                  <tr>
                  <th>OpEx</th>
                  <th> <input type="text" class="form-control"  name="OPEx" id="OPEx" value="<?php echo $data->OPEx;?>" ></th>
                  </tr>
           <tr>
                  <th>OpEx +Service charge</th>
                  <th> <input type="text" class="form-control"  name="OPExservicecharge" id="OPExservicecharge" value="<?php echo $data->OPExservicecharge;?>" ></th>
                  </tr>
           <tr>
                  <th>OpEx +Service charge maintenance reserve</th>
                  <th> <input type="text" class="form-control"  name="OPExservicechargemaintenance" id="OPExservicechargemaintenance"
          value="<?php echo $data->OPExservicechargemaintenance;?>" ></th>
                  </tr>
         </thead>
              </table>
      </div>
   </div>
        <!-- /.tab-pane -->
         <div class="tab-pane" id="tab_5">
                
      <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Institutional sustainability</h3>
        </div>
          <table  class="table table-bordered table-hover">
                <thead>
                <tr>
        <th colspan="2">Approved Yes/No</th>
        <th colspan="2">Approved Yes/No</th>
                 </tr>
         <tr>
                  <th>Local government approval</th>
          
                  <th>
          <select class="form-control" name="GramPanchayatApproval" id="GramPanchayatApproval">
          <?php foreach($approval as $key=>$res){ 
          if($data->GramPanchayatApproval ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
         </th>
          <th>reject water discharge</th>
                  <th>
          <select class="form-control" name="reject_water_discharge" id="reject_water_discharge">
                    <?php foreach($approval as $key=>$res){ 
          if($data->RejectWaterDischargeApproval ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
                  </th>
          </tr>
        
        <tr>
                  <th>Legal electicity connection</th>
                  <th>
          <select class="form-control" name="LegalElectricityConnection" id="LegalElectricityConnection">
                    <?php foreach($approval as $key=>$res){ 
          if($data->LegalElectricityConnection ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
                
          </th>
          <th>Trained Operator</th>
                  <th>
          <select class="form-control" name="Trained_Operator" id="Trained_Operator">
                    <?php foreach($approval as $key=>$res){ 
          if($data->GramPanchayatApproval ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
          </th>
          </tr>
        
        <tr>
                  <th>Premises and land</th>
                  <th>
          <select class="form-control" name="LandApproval" id="LandApproval">
                   <?php foreach($approval as $key=>$res){ 
          if($data->LandApproval ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
          </th>
          <th>Raw source water</th>
                  <th>
          <select class="form-control" name="RawWaterSourceApproval" id="RawWaterSourceApproval">
          <?php foreach($approval as $key=>$res){ 
          if($data->RawWaterSourceApproval ==$key){
          ?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $res;?></option>
                    <?php }else{ ?>
          <option value="<?php echo $key;?>"><?php echo $res;?></option>
          <?php }} ?>
                 </select>
          </th>
          </tr>
        
        <tr>
                  <th colspan="1">Document Upload</th>
          <th colspan="1">
           <input type="file" name="uploadDocument" id="uploadDocument" >
          </th>
          <th colspan="2"></th>
                </tr>
        
        
              </table>
       </div>
    </div>
        <!-- /.tab-pane -->
         <div class="tab-pane" id="tab_6">
         <div class="box box-primary">
        <div class="box-header with-border">
               <h3 class="box-title">Enviromental sustainability</h3>
        </div>
          <table  class="table table-bordered table-hover">
                <thead>
         <tr>
                  <th>
          <div class="checkbox">
                    <label>
                      <input type="checkbox" name="checkRWQuantification" id="checkRWQuantification"<?php if($data->checkRWQuantification==0){
            echo "";}else{ echo "CHECKED";}?>  >
                    Reject water Produced
                    </label>
                  </div>
          </th>
                  <th><input type="text" class="form-control"  name="WaterProductionDaily" id="WaterProductionDaily" value="<?php echo $data->WaterProductionDaily;?>" ></th>
          <th></th>
          <th></th>
          </tr>
        <tr>
        <th>
           <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->CheckRWDisposal;?>" name="CheckRWDisposal" id="CheckRWDisposal"<?php if($data->CheckRWDisposal==0){
            echo "";}else{ echo "CHECKED";}?> >
                   Reject water disposed
                    </label>
                  </div>
          </th>
                  <th><input type="text" class="form-control"  name="CheckRWDisposal" id="CheckRWDisposal" value="<?php echo $data->CheckRWDisposal;?>" ></th>
          <th></th>
          <th></th>
        </tr>
        <tr>
                  <th colspan="4" > 
          <div class="checkbox">
                    <label>
                      <input type="checkbox">
                   Ground water recharge / rain water harvesting
                    </label>
                  </div>
          </th>
          </tr>
        <tr>
                  <th colspan="2">
          <div class="checkbox">
                    <label>
                      <input type="checkbox" value="<?php echo $data->CheckRWUtilisation; ?>" name="CheckRWUtilisation" id="CheckRWUtilisation" <?php if($data->CheckRWUtilisation==0){
            echo "";}else{ echo "CHECKED";}?>>
                       Reject water utilised
                    </label>
                  </div>        
          </th>
          <th>
          Activity
          <input type="text" class="form-control"  name="UtilizationActivity1" id="UtilizationActivity1" value="<?php echo $data->UtilizationActivity1;?>" >
          <input type="text" class="form-control"  name="UtilizationActivity2" id="UtilizationActivity2" value="<?php echo $data->UtilizationActivity2;?>" >
          <input type="text" class="form-control"  name="UtilizationActivity3" id="UtilizationActivity3" value="<?php echo $data->UtilizationActivity3;?>" >
          <input type="text" class="form-control"  name="UtilizationActivity4" id="UtilizationActivity4" value="<?php echo $data->UtilizationActivity4;?>" ></th>
          </th>
                  <th>
          Litres/Day
          <input type="text" class="form-control"  name="UtilizationOther1" id="UtilizationOther1"  value="<?php echo $data->UtilizationOther1; ?>" >
          <input type="text" class="form-control"  name="UtilizationOther2" id="UtilizationOther2"  value="<?php echo $data->UtilizationOther2; ?>" >
          <input type="text" class="form-control"  name="UtilizationOther3" id="UtilizationOther3"  value="<?php echo $data->UtilizationOther3; ?>" >
          <input type="text" class="form-control"  name="UtilizationOther4" id="UtilizationOther4"  value="<?php echo $data->UtilizationOther4; ?>" ></th>
          </tr>
        </table>
       </div>
         
              </div>
              <div class="row">
                <div class="col-lg-12">
        <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?>
        <?php print form_close(); ?>
                  
                </div>
              </div>  
              <!-- /.tab-pane -->
            </div>
      
            <!-- /.tab-content -->
          </div>
      
          <!-- nav-tabs-custom -->
        </div>


        


</div>
         
      </div>
 </section>
    <!-- /.content -->
	 <!-- page script -->
	 
	 
	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   // $("#example1").dataTable();
   });
	   
 </script>