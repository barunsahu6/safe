 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Institutional Sustainability
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Institutional Sustainability</li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-xs-1"><b>Country :</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($_POST['Country']==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($_POST['Country']==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
          </div>
            <div class="col-xs-1"><b>State :</b></div>
          <?php if($_POST['State'] ==''){ ?>
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
          <?php } else{ ?>
           <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
           <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
          <?php } ?>

        <!-- /.col -->
      <div class="col-xs-1"><b>Dirstict :</b></div>
           <?php if($_POST['District'] !=''){ ?>
         <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>
           <?php }else{ ?>
            <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
             <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ echo "select";?>

                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  
                </div>		  
           <?php } ?>
       
	     <div class="col-xs-1"><b>Plant :</b></div>
          <div class="col-xs-2">
     <?php  if($_POST['Plant'] !=''){
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
     <?php }else{ 
      $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']);
     ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
             <?php } ?>
         
        </div>
      <!-- /.row -->
	  </form>
<!-- /.row -->
  	</div>

 <div class="row">
 <script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-TrainingRecived',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Training Recived ",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Training Recived",
                "numberPrefix": "",
                "paletteColors": "#0075c2",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#FFFFFF",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Electicity Safty",
                    "value": "<?php echo $getTrainReciev[0]->Value;?>",
                    "Color": "#4A8FD0",
                   
                }, 
                {
                    "label": "Plant Op & Mgnmt.",
                    "value": "<?php echo $getTrainReciev[1]->Value;?>",
                    "Color": "#F5833C",
                    
                }, 
                {
                    "label": "Water Qulality",
                    "value": "<?php echo $getTrainReciev[2]->Value;?>",
                    "Color": "#9E9E9E",
                    
                }, 
                {
                    "label": "Consumer Acts",
                    "value": "<?php echo $getTrainReciev[3]->Value;?>",
                    "Color": "#FFC501",
                   
                }, 
                {
                    "label": "Accounts",
                    "value": "<?php echo $getTrainReciev[4]->Value;?>",
                    "Color": "#4573CB",
                   
                }
            ]
        }
    }).render();
});
</script>
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
    
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">  Training Recived</h3>
              </div>
           <div id="chart-container-TrainingRecived"></div>
          
          </div>
	</section>

<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Regulartory',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Regulartory",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Regulartory",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                 //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Govt. Approved",
                    "value": "<?php echo $getRegulartories[0]->Value;?>",
                    "Color": "#4A8FD0",
                    
                }, 
                {
                    "label": "Elec. Connection",
                    "value": "<?php echo $getRegulartories[1]->Value;?>",
                    "Color": "#F5833C",
                    
                }, 
                {
                    "label": "Land & Premises",
                    "value": "<?php echo $getRegulartories[2]->Value;?>",
                    "Color": "#9E9E9E",
                   
                }, 
                {
                    "label": "Raw Water Source",
                    "value": "<?php echo $getRegulartories[3]->Value;?>",
                    "Color": "#FFC501",
                   
                }
            ]
        }
    }).render();
});
</script>
   	<section class="col-lg-6 connectedSortable">
      
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Regulartory</h3>
              </div>
            <div id="chart-container-Regulartory"></div>
          
          </div>
		  </section>


</div>
 </section>
 </div>
  </div>
 
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexInstitutional').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexInstitutional').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
           
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexInstitutional').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   