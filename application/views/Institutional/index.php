 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>
<style>
/*.dropdownmenu ul, .dropdownmenu li {
	margin: 0;
	padding: 0;
}
.dropdownmenu ul {
	background: gray;
	list-style: none;
	width: 100%;
}
.dropdownmenu li {
	float: left;
	position: relative;
	width:auto;
}
.dropdownmenu a {
	background: #3c8dbc;
	color: #FFFFFF;
	display: block;
	font: bold 11px/20px sans-serif;
	padding: 8px 15px;
	text-align: center;
	text-decoration: none;
	-webkit-transition: all .25s ease;
	-moz-transition: all .25s ease;
	-ms-transition: all .25s ease;
	-o-transition: all .25s ease;
	transition: all .25s ease;
}
.dropdownmenu li:hover a {
	background: #000000;
}*/
#submenu {
	left: 0;
	opacity: 0;
	position: absolute;
	top: 35px;
	visibility: hidden;
	z-index: 1;
}
li:hover ul#submenu {
	opacity: 1;
	top: 40px;	/* adjust this as per top nav padding top & bottom comes */
	visibility: visible;
}
#submenu li {
	float: none;
	width: 100%;
}
#submenu a:hover {
	background: #DF4B05;
}
#submenu a {
	background-color:#000000;
}
</style>
<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class ="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <nav class="dropdownmenu">
  <ul>
    <li><a href="<?php echo site_url().'Dashboard/index';?>" " >PAT Dashboard</a></li>
    <li><a href="<?php echo site_url().'Social/index';?>" ">Social Sustainability</a></li>
    <li><a href="<?php echo site_url().'Operational/index';?>" ">Operational Sustainability</a> </li>
    <li><a href="<?php echo site_url().'Financial/index';?>" ">Financial Sustainability</a></li>
    <li><a href="<?php echo site_url().'Institutional/index';?>" style="background-color:#000000;">Institutional Sustainability</a></li>
    <li><a href="<?php echo site_url().'Environmental/index';?>" ">Environmental Sustainability</a></li>
  </ul>
</nav>

</div>
</div>
     
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="0">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($this->session->userdata("Country")==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($this->session->userdata("Country")==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
          </div>
            <div class="col-xs-1" style="top:7px;font-size:12px;"><b>State/Province:</b></div>
         
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($this->session->userdata("Country"));  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="0">---All ---</option>
                     <?php foreach($State as $row){
                         if($this->session->userdata("State")==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($this->session->userdata("State")==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
         

        <!-- /.col -->
      <div class="col-xs-1" style="top:7px;font-size:12px;"><b>District/Zone:</b></div>
         
         <?php $District = $this->model->getDistricts($this->session->userdata("Country"),$this->session->userdata("State")); ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="0">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($this->session->userdata("District")==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($this->session->userdata("District")==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>
           
       
	     <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Plant:</b></div>
          <div class="col-xs-2">
     <?php 
        $Plant = $this->model->getPlants($this->session->userdata("Country"),$this->session->userdata("State"),$this->session->userdata("District")); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="0">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($this->session->userdata("Plant")==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($this->session->userdata("Plant") == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
  
        </div>
      <!-- /.row -->
	  </form>
<!-- /.row -->
  	</div>
 <script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-TrainingRecived',
       width: '450',
        height: '315',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Training Recived ",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
               // "yAxisName": "Training Recived",
                "numberPrefix": "",
                "paletteColors": "#0075c2",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#FFFFFF",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weightInstitutional
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Electicity Safty",
                    "value": "<?php echo $getTrainReciev[0]->Value;?>",
                    "Color": "#4A8FD0",
                }, 
                {
                    "label": "Plant Op & Mgnmt.",
                    "value": "<?php echo $getTrainReciev[1]->Value;?>",
                    "Color": "#F5833C",
                }, 
                {
                    "label": "Water Qulality",
                    "value": "<?php echo $getTrainReciev[2]->Value;?>",
                    "Color": "#008080",
                }, 
                {
                    "label": "Consumer Activation",
                    "value": "<?php echo $getTrainReciev[3]->Value;?>",
                    "Color": "#FFC501",
                }, 
                {
                    "label": "Accounts",
                    "value": "<?php echo $getTrainReciev[4]->Value;?>",
                    "Color": "#4573CB",
                 }
            ]
        }
    }).render();
});
</script>


<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 405px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}

.rotate {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */}
</style>
<div class="scrollbar" id="style-3">

<div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:315px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 145px;"><b>DEVOLUTION</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
               <div id="chart-container-TrainingRecived"></div>
        </div>
    </div>
   </div>
   </div>
<script type="text/javascript">
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-Regulartory',
       width: '450',
        height: '315',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Regulartory",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                //"yAxisName": "Regulartory",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "rotateValues": "0",
                 //Changing font
                "valueFont": "Arial",
                //Changing font color
                "valueFontColor": "#FFFFFF",
                //Changing font size
                "valueFontSize": "12",
                //Changing font weight
                "valueFontBold": "1",
                //Changing font style
                "valueFontItalic": "0"
            }, 
        
            "data": [
                {
                    "label": "Govt. Approved",
                    "value": "<?php echo $getRegulartories[0]->Value;?>",
                    "Color": "#4A8FD0",
                    
                }, 
                {
                    "label": "Elec. Connection",
                    "value": "<?php echo $getRegulartories[1]->Value;?>",
                    "Color": "#F5833C",
                    
                }, 
                {
                    "label": "Land & Premises",
                    "value": "<?php echo $getRegulartories[2]->Value;?>",
                    "Color": "#008080",
                   
                }, 
                {
                    "label": "Raw Water Source",
                    "value": "<?php echo $getRegulartories[3]->Value;?>",
                    "Color": "#FFC501",
                   
                }
            ]
        }
    }).render();
});
</script>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:315px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 145px;"><b>REGULATORY APPROVALS </b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
               <div id="chart-container-Regulartory"></div>
        </div>
    </div>
   </div>
   </div>
	
</div>

 
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                     $('#indexInstitutional').submit();
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexInstitutional').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexInstitutional').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
           $('#indexInstitutional').submit();    
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   