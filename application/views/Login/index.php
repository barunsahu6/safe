<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SafeWaterNetwork| Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo FCSS;?>bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo FCSS;?>/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><img src="<?php  echo FIMAGES;?>swn-logo.gif"></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
   <?php print form_open_multipart($this->router->class.'/'.$this->router->method,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class,'class'=>'form-horizontal'))?>
         <?php 
		        $log_err = $this->session->flashdata('message'); 
               if($log_err){ 
                   ?>
                   <div class="alert alert-dismissable alert-danger" style="margin-left: 0;">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>Sorry !</strong>  <?php echo $log_err; ?> :).
                  </div>
                  <?php } ?> 

	 <div class="form-group has-feedback">
  
 <?php if(form_error('form[username]')){ ?><div class="col-sm-12 form-group has-error"><?php } ?>
<?php print form_input(array('type'=>'text','name'=>'form[username]','id'=>'username','value'=>set_value('form[username]'),'class'=>'form-control','placeholder'=>'Username','autocomplete'=>'off','required'=>'required')); ?>	  
      
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
	  <?php if(form_error('form[password]')){ ?><div class="col-sm-12 form-group has-error"><?php } ?>
									<?php print form_input(array('type'=>'password','name'=>'form[password]','id'=>'password','value'=>set_value('form[password]'),'class'=>'form-control','placeholder'=>'Password','autocomplete'=>'off','required'=>'required')); ?>
       
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
             <!--  <input type="checkbox"> Remember Me-->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
		<?php print form_submit('sigin', 'Sign In', 'class="btn btn-primary btn-block btn-flat"'); ?>          
         
        </div>
        <!-- /.col -->
      </div>
    </form>

   
    <!-- /.social-auth-links -->

   </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php print FJS; ?>bootstrap.min.js"></script>
 <script src="<?php print FJS; ?>jquery.form-validator.min.js" type="text/javascript"></script>

    <script> $.validate(); </script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
