  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Standard Parameter Management
      </h1>
	  
	   <a href="<?php echo site_url()."/StandardParameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add Standard Parameter
       </a>
	  
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url().'Dashboard/index';?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Standard Parameter Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Standard Parameter </h3>
            </div>
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
			?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			<div class="box-body">
			
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Parameter</th>
				          <th>Standard</th>
                  <th>Parameter Value  </th>
				           <th>Unit </th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
				<?php 	
				$i=1;
				$getdata =$this->db->query("Select mstsd.StandardParameterID, mstsd.ParameterID, mstsd.StandardID,mstsd.AcceptableLimit,
				mstp.Name,mstp.uoM,msts.StandardName 
				FROM mststandardparameterdetail as mstsd INNER JOIN mstparameter as mstp ON mstp.ParameterID=mstsd.ParameterID
				INNER JOIN mststandard as msts ON msts.StandardID=mstsd.StandardID WHERE mstsd.IsDeleted=0 
				ORDER BY mstsd.StandardParameterID DESC")->result();
				foreach($getdata as $detail){
				?>
                <td><?php echo $i;?></td>
				 <td><?php echo $detail->Name;?></td>
                  <td><?php echo $detail->StandardName;?></td>
                  <td><?php echo $detail->AcceptableLimit;?></td>
				  <td><?php echo $detail->uoM;?></td>
				  <td><a href="<?php print base_url().$this->router->class.'/edit/'.$detail->StandardParameterID; ?>" ><span class="fa fa-fw fa-edit"> </span></a></td>
                   <td><a href="<?php print base_url().$this->router->class.'/delete/'.$detail->StandardParameterID; ?>" onclick="return confirm_delete()" ><span class="fa fa-fw fa-remove"> </span></a></td>
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No.</th>
                  <th>Parameter</th>
				  <th>Standard</th>
                  <th>Parameter Value  </th>
				  <th>Unit </th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
           
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
         
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 
<script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
      <script>
		function confirm_delete() {
			var r = confirm("Are you sure to delete record?");
			if (r == true) {
				return true;
			} else {
				return false;
			}
		}

   </script>	