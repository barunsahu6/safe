<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Safe Water Network| Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php print FCSS; ?>bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <!-- jvectormap -->
  
  <link rel="stylesheet" href="<?php print FCSS; ?>bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php print FCSS; ?>AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php print FCSS; ?>skins/_all-skins.min.css">
  
  <script src="<?php print FPLUGINS;?>jQuery/jquery-2.2.3.min.js"></script>
   <!--<script src="<?php print FJS; ?>pages/dashboard2.js"></script>-->
   <script src="<?php print FJS; ?>fusioncharts.js"></script>
<script src="<?php print FJS; ?>fusioncharts.theme.fint.js?cacheBust=56"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">


    <!-- Logo -->
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
	  <a href="#<?php //echo site_url('admin/Dashboard');?>" >
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
				<img src="<?php print FIMAGES;?>swn-logo.gif" width="150px" height="40px" alt="logo">
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
		  <?php  
			//echo $Count = $this->db->count_Country();
		  ?>
		 <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             Country
              <?php $Country = $this->Common_model->getCountCountry();?>
              <span class="label label-success"><?php echo count($Country);?></span>
            </a>
           
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             District
             <?php $District = $this->Common_model->getCountDistricts(); ?>
              <span class="label label-warning"><?php echo count($District);?></span>
            </a>
       </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             State
             <?php $State = $this->Common_model->getCountStates(); ?>
              <span class="label label-danger"><?php echo count($State); ?></span>
            </a>
              
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo FIMAGES;?>user2-160x160.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata['NAME'];?></span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="<?php echo site_url("login/logout"); ?>">Logout</a>
          </li>
        </ul>
      </div>

    </nav>
  </header>

<aside class="main-sidebar">