  <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo FIMAGES;?>user2-160x160.png" class="img-circle" alt="User Image">
        </div>

        <div class="pull-left info">
          <p><?php echo $this->session->userdata['NAME'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url(); ?>/Dashboard/index"><i class="fa fa-circle-o"></i> PATDashboard</a></li>
			      <li><a href="<?php echo site_url(); ?>/TSTDashboard/index"><i class="fa fa-circle-o"></i> TSTDashboard</a></li>
          </ul>
        </li>
		

  <!--   <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Role Management</span>
            <span class="pull-right-container">
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php //echo site_url("RoleManagement/add"); ?>"><i class="fa fa-circle-o"></i> Add Permission</a></li>
         </li>
            </ul>
        </li> -->
		
		  <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Masters</span>
            <span class="pull-right-container">
              
            </span>
          </a>
          <ul class="treeview-menu">
        <li><a href="<?php echo site_url("State/index"); ?>"><i class="fa fa-circle-o"></i> State</a></li>
			<li><a href="<?php echo site_url("District/index"); ?>"><i class="fa fa-circle-o"></i> District</a></li>
			
			 <li><a href="<?php echo site_url("Standard/index"); ?>"><i class="fa fa-circle-o"></i> Standard</a></li>
			<li><a href="<?php echo site_url("Parameter/index"); ?>"><i class="fa fa-circle-o"></i> Parameter</a></li>
			
			 <li><a href="<?php echo site_url("StandardParameter/index"); ?>"><i class="fa fa-circle-o"></i> Standard Parameter</a></li>
			<li><a href="<?php echo site_url("PlantSpecification/index"); ?>"><i class="fa fa-circle-o"></i> Plant Specification</a></li>
			<li><a href="<?php echo site_url("Aggregator/index"); ?>"><i class="fa fa-circle-o"></i> Aggregator</a></li>
			 </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>User Management</span>
            <span class="pull-right-container">
              
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url("Users/index"); ?>"><i class="fa fa-circle-o"></i> Existing Users</a></li>
			<li><a href="<?php echo site_url("ChangePassword/index"); ?>"><i class="fa fa-circle-o"></i> Change Password</a></li>
            </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Data Entry Form</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url("PATool/index"); ?>"><i class="fa fa-circle-o"></i> PAT Form</a></li>
			<li><a href="<?php echo site_url("TSTForm/index"); ?>"><i class="fa fa-circle-o"></i> TST Form</a></li>
          </ul>
        </li>
		
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Scoring</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url("Scoring/index"); ?>"><i class="fa fa-circle-o"></i> Scoring</a></li>
          </ul>
        </li>
    </ul>
                 
    </section>
    <!-- /.sidebar -->
  </aside>


