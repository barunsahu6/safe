<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Safe Water Network| Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php print FCSS; ?>bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <!-- jvectormap -->
  
  <link rel="stylesheet" href="<?php print FCSS; ?>bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php print FPLUGINS;?>select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php print FCSS; ?>AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php print FCSS; ?>skins/_all-skins.min.css">

  <style>
    .box.box-info {
        border-top-color: #0088cc !important;
    }
  </style>
  
  <script src="<?php print FPLUGINS;?>jQuery/jquery-2.2.3.min.js"></script>
   <!--<script src="<?php print FJS; ?>pages/dashboard2.js"></script>-->
   <script src="<?php print FJS; ?>fusioncharts.js"></script>
<script src="<?php print FJS; ?>fusioncharts.theme.fint.js?cacheBust=56"></script>

</head>

<body class="hold-transition skin-blue sidebar-mini" onload="FillTextboxValue(); FVTParameters();">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
	  <a href="#<?php //echo site_url('admin/Dashboard');?>" >
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
				<img src="<?php print FIMAGES;?>SWN_CHIEF_LOGO_FINAL_WHITE.png" width="auto" height="40px" alt="logo">
      </a>
      <?php //echo  $roleID = $this->session->userdata['login_data']['ROLE_ID'];?>
      <!-- Navbar Right Menu -->

      
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo FIMAGES;?>user2-160x160.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata['login_data']['NAME'];?></span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="<?php echo site_url("login/logout"); ?>">Logout</a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
<style>
.dropdownmenu ul, .dropdownmenu li {
  margin: 0;
  padding: 0;
}
.dropdownmenu ul {
  background: gray;
    
  list-style: none;
  width: 100%;
}
.dropdownmenu li {
  float: left;
    
  position: relative;
  width:auto;
}
.dropdownmenu a {
  background: #3c8dbc;
  color: #FFFFFF;
  display: block;
  font: bold 10px/20px sans-serif;
  padding: 8px 15px;
  text-align: center;
  text-decoration: none;
  -webkit-transition: all .25s ease;
  -moz-transition: all .25s ease;
  -ms-transition: all .25s ease;
  -o-transition: all .25s ease;
  transition: all .25s ease;
}
.dropdownmenu li:hover a {
  background: #000000;
   
}
</style>
<aside class="main-sidebar">
