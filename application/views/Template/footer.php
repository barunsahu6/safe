
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<footer class="main-footer" style="margin-left: 0 !important;">
    <strong>Copyright &copy; 2016 - 2017 <a href="#">SafeWaterNetwork </a>.</strong> All rights
    reserved.
  </footer>
  </div>
</div>
</body>	
 <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
 


<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.7 -->

<script src="<?php print FJS; ?>TreeMenu.js"></script>

<script src="<?php print FJS; ?>bootstrap.min.js"></script>
<!-- FastClick -->

<!-- AdminLTE App -->
<script src="<?php print FJS; ?>app.min.js"></script>
<!-- Sparkline -->
<!-- ChartJS 1.0.1 -->
<script src="<?php print FJS; ?>plugins/chartjs/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php print FJS; ?>plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php print FJS; ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script src="<?php print FJS; ?>demo.js"></script>
<script src="<?php print FPLUGINS;?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php print FPLUGINS;?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php print FPLUGINS;?>slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php print FPLUGINS;?>fastclick/fastclick.js"></script>
<script>
$(document).ready(function(){
    //Initialize Select2 Elements
    // $(".select2").select2();

    //Datemask dd/mm/yyyy
    // $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    // $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    // $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
   // $('.datepicker').datepicker({
	// format: 'dd-mm-yyyy',
    // startDate: '-3d',
    // autoclose: true
    //});

	 $(function() {
          $( ".datepicker" ).datepicker({
				todayBtn: "linked",
				format: 'dd-mm-yyyy',
				startDate: '-3d',
				todayHighlight: true,
				autoclose: true
			});
      });

});	
</script>

</html>


