   <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Bordered Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Task</th>
                  <th>Progress</th>
                  <th style="width: 40px">Label</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Update software</td>
                  <td>
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-red">55%</span></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Clean database</td>
                  <td>
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-yellow">70%</span></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Cron job running</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-light-blue">30%</span></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Fix and squish bugs</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-success" style="width: 90%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-green">90%</span></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->








<div class="left-sidebar">
<style type="text/css">
table > tbody > tr > th{
border:none !important;
}

table > tbody > tr > td{
border:none !important;
}
</style>
	<!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst($this->router->method).' '.ucfirst(str_replace('_',' ',$this->router->class)); ?></div>
                	<span class="tools"><?php print form_button('goback','<i class="fa fa-arrow-circle-left"> Back to Lists</i>',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
						<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <table class="table table-striped" 
                            <?php if(count($detail)>0) { foreach($detail as $label=>$value) {?>                            
                            <tr>
                              <th width="20%"><?php print ucwords(str_replace('_',' ',$label)); ?> :</th>
                              <td width="80%"><?php print $value; ?></td>
                            </tr>
                            <?php } } ?>
                            
                            <tr>
                              <td><?php print form_button('goback','Cancel',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></td>
                            </tr>
                          </table>
                        <?php print form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>