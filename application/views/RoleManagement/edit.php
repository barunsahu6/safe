  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Role Management
        <small>preview of Role Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"> Role Management </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
          <!-- Custom Tabs -->
<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
    <div class="col-md-12">

        <div class="panel panel-default">
          <div class="panel-heading"><label>Edit Role Management </label></div>
          <div class="panel-body">
                     
                     
                      <table class="table table-striped">
                      <tr>
                          <td class="panel-heading"><label>Role Name </lable></td>
                          <td ></td>
                          <td colspan="7">

                          <input type="hidden" name="flag" value="">
                           <select name="role_id" id="role_id" class="form-control select" required="required" onchange="showRolePermissions()" >
                          <option value="">Select Role Name</option>
                          <?php  foreach($role as $row){?>
                          <option value="<?php echo $row->RoleID;?>" <?php echo (set_value('role_id') == $row->RoleID? 'selected':'');?>><?php echo $row->RoleName;?></option>
                          <?php } ?>
                          </select>
                          </td>
                        </tr>
                       <?php 
                       echo "<pre>";
                      print_r($permission_list);
                      // print_r($permission_list[1]);
                        //  echo $permission_list[0]->controller;
                          // echo $permission_list[0]->method;

                       ?>
                        <tr>
                          <td class="panel-heading"><label>Dashboard</lable></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>PAT Dashboard</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[patdashboard][view]" id="permission[patdashboard][view]" value="index"
                          <?php
                          	if ($permission_list[0]->controller == 'patdashboard' && $permission_list[0]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>TST Dashboard</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[tstdashboard][view]" id="permission[tstdashboard][view]" value="index" 
                           <?php
                          	if ($permission_list[0]->controller == 'tstdashboard' && $permission_list[0]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                          	
                           ?>
                          ><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td class="panel-heading"><label>Scoring</lable></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>PAT Scoring</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[scoring][view]" id="permission[scoring][view]" value="index"
                           <?php
                          	if ($permission_list[1]->controller == 'scoring' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td class="panel-heading"><label>Data Entry Form</lable></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>PAT Form</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[patform][view]" id="permission[patform][view]" value="index"
                          <?php
                          	if ($permission_list[2]->controller == 'patform' && $permission_list[2]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[patform][update]" id="permission[patform][update]" value="edit"
                           <?php
                          	if ($permission_list[3]->controller == 'patform' && $permission_list[3]->method == 'update') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td> </td>
                           <!--<td><input type="checkbox" name="permission[patform][delete]" id="permission[patform][delete]" value="delete"><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                        <tr>
                          <td ><label>TST Form</lable></td>
                          <!-- <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[tstform][view]" id="permission[tstform][view]" value="index"
                           <?php
                          	if ($permission_list[0]->controller == 'tstform' && $permission_list[0]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[tstform][update]" id="permission[tstform][update]" value="edit"
                          <?php
                          	if ($permission_list[1]->controller == 'tstform' && $permission_list[1]->method == 'update') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td> </td>
                           <!--<td><input type="checkbox" name="permission[tstform][delete]" id="permission[tstform][delete]" value="delete"><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                        <tr >
                          <td class="panel-heading"><label>Masters</lable></td>
                          <td colspan="8"></td>
                          </tr>
                          <div id="showrole">
                        <tr>
                          <td><label>State</lable></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[state][add]" id="permission[state][add]" value="add" 
                          <?php
                          			if ($permission_list[0]->controller == 'state' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                           >
                          <label style="padding-left: 5px;">Add</label></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[state][view]" id="permission[state][view]" value="index"
                         <?php
                          			if ($permission_list[1]->controller == 'state' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>>
                          <label style="padding-left: 5px;">View</label>
                          </td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[state][edit]" id="permission[state][edit]" value="edit"
                           <?php
                          			if ($permission_list[2]->controller == 'state' && $permission_list[2]->method == 'edit') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td> </td>
                           
                           <!--<td><input type="checkbox" name="permission[state][delete]" id="permission[state][delete]" value="delete"><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                         <tr>
                          <td ><label>District</lable></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[district][add]" id="permission[district][add]" 
                          value="add"
                          <?php
                          			if ($permission_list[0]->controller == 'district' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Add</label></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[district][view]" id="permission[district][view]" value="index"
                           <?php
                          			if ($permission_list[1]->controller == 'district' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          > <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[district][edit]" id="permission[district][edit]" value="edit"
                           <?php
                          			if ($permission_list[2]->controller == 'district' && $permission_list[2]->method == 'edit') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          > <label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                            <td> </td>
                           <!--<td><input type="checkbox" name="permission[district][delete]" id="permission[district][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                        <tr>
                          <td ><label>Standard</lable></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[standard][add]" id="permission[standard][add]" 
                          value="add"
                           <?php
                          			if ($permission_list[0]->controller == 'standard' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                        ><label style="padding-left: 5px;">Add</label></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[standard][view]" id="permission[standard][view]" value="index"
                           <?php
                          			if ($permission_list[1]->controller == 'standard' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          > <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standard][update]" id="permission[standard][update]" value="edit"
                           <?php
                          			if ($permission_list[2]->controller == 'standard' && $permission_list[2]->method == 'update') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td></td>
                           <td></td>
                           <!--<td><input type="checkbox" name="permission[standard][delete]" id="permission[standard][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>
                         <tr>
                          <td ><label>Parameter</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[parameter][add]" id="permission[parameter][add]" 
                          value="add"
                          <?php
                          			if ($permission_list[0]->controller == 'parameter' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[parameter][view]" id="permission[parameter][view]" value="index"
                          <?php
                          			if ($permission_list[1]->controller == 'parameter' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          > <label style="padding-left: 5px;">View</label></td>
                          <td></td>
                          <td><input type="checkbox" name="permission[parameter][update]" id="permission[parameter][update]" value="edit"
                          <?php
                          			if ($permission_list[2]->controller == 'parameter' && $permission_list[2]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>                         
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td></td>
                           <td></td>
                           <!--<td><input type="checkbox" name="permission[parameter][delete]" id="permission[parameter][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                         <tr>
                          <td ><label>Standard Parameter</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standardparameter][add]" id="permission[standardparameter][add]" 
                          value="add"
                           <?php
                          			if ($permission_list[0]->controller == 'standardparameter' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?> ><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standardparameter][view]" id="permission[standardparameter][view]" value="index"
                           <?php
                          			if ($permission_list[1]->controller == 'standardparameter' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>> <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standardparameter][update]" id="permission[standardparameter][update]" value="edit"
                          <?php
                          			if ($permission_list[2]->controller == 'standardparameter' && $permission_list[2]->method == 'update') {
                          				echo 'checked="checked"';
                          			}
                           ?>
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td> </td>
                           <!--<td><input type="checkbox" name="permission[standardparameter][delete]" id="permission[standardparameter][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                         <tr>
                          <td ><label>Plant Specification</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[plantspecification][add]" id="permission[plantSpecification][add]" 
                          value="add"
                            <?php
                          			if ($permission_list[0]->controller == 'plantspecification' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?>                         
                          ><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[plantspecification][view]" id="permission[plantSpecification][view]" value="index"
                          <?php
                          			if ($permission_list[1]->controller == 'plantspecification' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?> > <label style="padding-left: 5px;">View</label></td>
                          <td></td>
                          <td><input type="checkbox" name="permission[plantspecification][update]" id="permission[plantSpecification][update]" value="edit"
                          <?php
                          			if ($permission_list[2]->controller == 'plantspecification' && $permission_list[2]->method == 'update') {
                          				echo 'checked="checked"';
                          			}
                           ?>   
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td></td>
                           <td></td>
                           <!--<td><input type="checkbox" name="permission[plantspecification][delete]" id="permission[plantSpecification][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                         <tr>
                          <td ><label>Aggregator</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[aggregator][add]" id="permission[aggregator][add]" value="add"
                          <?php
                          			if ($permission_list[0]->controller == 'aggregator' && $permission_list[0]->method == 'add') {
                          				echo 'checked="checked"';
                          			}
                           ?>   
                          ><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[aggregator][view]" id="permission[aggregator][view]" value="index"
                          <?php
                          			if ($permission_list[1]->controller == 'aggregator' && $permission_list[1]->method == 'view') {
                          				echo 'checked="checked"';
                          			}
                           ?>  
                          
                          > <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[aggregator][update]" id="permission[aggregator][update]" value="edit"
                          <?php
                          			if ($permission_list[2]->controller == 'aggregator' && $permission_list[2]->method == 'update') {
                          				echo 'checked="checked"';
                          			}
                           ?>                           
                          ><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                            <td> </td>
                           <!--<td><input type="checkbox" name="permission[aggregator][delete]" id="permission[aggregator][delete]" value=""> <label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>
                     
                        </div>
                         <tr>
                          <td class="panel-heading"></td>
                          <td> <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?> </td>
                           <td colspan="6">
                           </td>
                        </tr>
                       
                      </table>

                      </div>
                      </div>
                      </div>
                      <?php print form_close(); ?>
 
</div>
</section>
<script type="text/javascript">
	      
        $(document).ready(function(){
            $("#role_id").change(function(){

                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getRoleManagement/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    console.log(data);
                    $("#showrole").html(data);
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

        });

        function showRolePermissions() {
        	$('#flag').val("switcher");
        	$('#<?php echo $this->router->method.$this->router->class;?>').submit();
        }
</script>