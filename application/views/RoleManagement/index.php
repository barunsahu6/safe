    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Role Management
          <small>preview of Role Management</small>
        </h1>

	  <!-- <a href="<?php //echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add PAT
              </a>-->

              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Role Management</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">

                <div class="col-xs-12">
                 <!-- /.box-header -->
                 <?php 
                 $tr_msg= $this->session->flashdata('tr_msg');
                 $er_msg= $this->session->flashdata('er_msg');

                 if(!empty($tr_msg))
                 {
                  ?>
                  <div class="alert alert-success alert-dismissible"> 
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('tr_msg');?>. </div>
                    <?php } else if(!empty($er_msg)){?>
                    <div class="alert alert-danger alert-dismissible"> 
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('er_msg');?>. </div>  
                      <?php } ?>

                    </div>


                    

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Role Name</th>
                  <th>User Name</th>
                  <th>Permission</th>
                </tr>
                </thead>
                <tbody>
                <td>&nbps;  </td>
                <td>&nbps;</td>
                <td>&nbps;</td>
                </tbody>
              </table>
             </div>

                  </div>
                </section>
                <!-- /.content -->
                <!-- page script -->


                <!-- page script -->
                <script type="text/javascript">
                  $(function() {
                    $("#example1").dataTable();
                  });

                </script>