  
  <style type="text/css">
    #label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;
    padding-left: 5px;
}

  </style> <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Role Management
        </h1>
     </section>
    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
          <!-- Custom Tabs -->
<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
    <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading"><label>Role Management </label></div>
                      <div class="panel-body">
                      <table class="table table-striped">
                      <tr>
                          <td class="panel-heading"><label>Role Name </lable></td>
                          <td ></td>
                          <td colspan="7">
                           <select name="role_id" id="role_id" class="form-control select" required="required">
                          <option value="">Select Role Name</option>
                          <?php  foreach($role as $row){?>
                          <option value="<?php echo $row->RoleID;?>" <?php echo (set_value('role_id') == $row->RoleID? 'selected':'');?>><?php echo $row->RoleName;?></option>
                          <?php } ?>
                          </select>
                          </td>
                        </tr>
                         <tr>
                          <td class="panel-heading"><label>Dashboard</lable></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>PAT Dashboard</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[patdashboard][view]" id="permission[patdashboard][view]" value="index"><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>TST Dashboard</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[tstdashboard][view]" id="permission[tstdashboard][view]" value="index"><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>

                        <tr>
                          <td class="panel-heading"><label>Scoring</lable></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                         <tr>
                          <td ><label>PAT Scoring</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[scoring][view]" id="permission[scoring][view]" value="index"><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>

                         <tr>
                          <td class="panel-heading"><label>Data Entry Form</lable></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                        
                        <tr>
                          <td ><label>PAT Form</lable></td>
                         <!--  <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[patform][view]" id="permission[patform][view]" value="index"><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[patform][update]" id="permission[patform][update]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                            <td> </td>
                             <td> </td>
                              <td> </td>
                           <!--<td><input type="checkbox" name="permission[patform][delete]" id="permission[patform][delete]" value="delete"><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>

                        <tr>
                          <td ><label>TST Form</lable></td>
                          <!-- <td> Add </td>
                          <td><input type="checkbox" name="PATForm[]" id="Aggregator_1" value="add"></td> -->
                          <td>  </td>
                          <td><input type="checkbox" name="permission[tstform][view]" id="permission[tstform][view]" value="index"><label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[tstform][update]" id="permission[tstform][update]" value="edit "><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                            <td> </td>
                             <td> </td>
                              <td> </td>
                           <!--<td><input type="checkbox" name="permission[tstform][delete]" id="permission[tstform][delete]" value="delete"><label style="padding-left: 5px;">Delete</label></td>-->
                        </tr>


                        <tr >
                          <td class="panel-heading"><label>Masters</lable></td>
                          <td colspan="7"></td>
                          </tr>
                        <tr>
                          <td><label>State</lable></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[state][add]" id="permission[state][add]" value="add">
                          <label style="padding-left: 5px;">Add</label></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[state][view]" id="permission[state][view]" value="index">
                          <label style="padding-left: 5px;">View</label>
                          </td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[state][edit]" id="permission[state][edit]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td><input type="checkbox" name="permission[state][delete]" id="permission[state][delete]" value="delete"><label style="padding-left: 5px;">Delete</label></td>
                        </tr>

                         <tr>
                          <td ><label>District</lable></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[district][add]" id="permission[district][add]" 
                          value="add"><label style="padding-left: 5px;">Add</label></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[district][view]" id="permission[district][view]" value="index"> <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[district][edit]" id="permission[district][edit]" value="edit"> <label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td><input type="checkbox" name="permission[district][delete]" id="permission[district][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>
                        </tr>

                        <tr>
                          <td ><label>Standard</lable></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[standard][add]" id="permission[standard][add]" 
                          value="add"><label style="padding-left: 5px;">Add</label></td>
                          <td>  </td>
                          <td><input type="checkbox" name="permission[standard][view]" id="permission[standard][view]" value="index"> <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standard][update]" id="permission[standard][update]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td></td>
                           <td><input type="checkbox" name="permission[standard][delete]" id="permission[standard][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>
                        </tr>
                         <tr>
                          <td ><label>Parameter</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[parameter][add]" id="permission[parameter][add]" 
                          value="add"><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[parameter][view]" id="permission[parameter][view]" value="index"> <label style="padding-left: 5px;">View</label></td>
                          <td></td>
                          <td><input type="checkbox" name="permission[parameter][update]" id="permission[parameter][update]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td></td>
                           <td><input type="checkbox" name="permission[parameter][delete]" id="permission[parameter][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>
                        </tr>

                         <tr>
                          <td ><label>Standard Parameter</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standardparameter][add]" id="permission[standardparameter][add]" 
                          value="add"><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standardparameter][view]" id="permission[standardparameter][view]" value="index"> <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[standardparameter][update]" id="permission[standardparameter][update]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td><input type="checkbox" name="permission[standardparameter][delete]" id="permission[standardparameter][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>
                        </tr>

                         <tr>
                          <td ><label>Plant Specification</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[plantspecification][add]" id="permission[plantSpecification][add]" 
                          value="add"><label style="padding-left: 5px;">Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[plantspecification][view]" id="permission[plantSpecification][view]" value="index"> <label style="padding-left: 5px;">View</label></td>
                          <td></td>
                          <td><input type="checkbox" name="permission[plantspecification][update]" id="permission[plantSpecification][update]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td></td>
                           <td><input type="checkbox" name="permission[plantspecification][delete]" id="permission[plantSpecification][delete]" value=""><label style="padding-left: 5px;">Delete</label></td>
                        </tr>

                         <tr>
                          <td ><label>Aggregator</lable></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[aggregator][add]" id="permission[aggregator][add]" 
                          value="add"><label style="padding-left: 5px;" >Add</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[aggregator][view]" id="permission[aggregator][view]" value="index"> <label style="padding-left: 5px;">View</label></td>
                          <td> </td>
                          <td><input type="checkbox" name="permission[aggregator][update]" id="permission[aggregator][update]" value="edit"><label style="padding-left: 5px;">Edit</label></td>
                           <td> </td>
                           <td><input type="checkbox" name="permission[aggregator][delete]" id="permission[aggregator][delete]" value=""> <label style="padding-left: 5px;">Delete</label></td>
                        </tr>

                         <tr>
                          <td class="panel-heading"></td>
                          <td> <?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?> </td>
                           <td colspan="6"><a href="<?php echo site_url("RoleManagement/");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> </td>
                        </tr>
                       
                      </table>

                      </div>
                      </div>
                      </div>
                      <?php print form_close(); ?>
 
</div>
</section>