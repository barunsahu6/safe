<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 450px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

.scrollbar1
{
	margin-left: 0px;
	float: left;
	height: 250px;
	width: 450px;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}
#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
#style-4::-webkit-scrollbar-thumb
{
	background-color: #F5F5F5;
}
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TST Dashboard
    </h1>
     
    </section>
	 
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
	    <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>

<div class="row">
          <div class="col-xs-1" style="top:7px; font-size:16px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="0">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($this->session->userdata("Country")==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($this->session->userdata("Country")==$row->CountryID) { echo 'selected="selected"';} ?>><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>
                    <?php } }?>
                 </select>
                </div>
          </div>
          
          <div class="col-xs-2"style="top:7px;font-size:16px;"><b>State/Province:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($this->session->userdata("Country"));  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="0">---All ---</option>
                     <?php foreach($State as $row){
                         if($this->session->userdata("State")==$row->StateID){ ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($this->session->userdata("State")==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>

          <div class="col-xs-2" style="top:7px;font-size:16px;"><b>District/Zone:</b></div>
         
         <?php $District = $this->model->getDistricts($this->session->userdata("Country"),$this->session->userdata("State")); ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="0">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($this->session->userdata("District")==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($this->session->userdata("District")==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>         
     
 
 </form>
<div class="scrollbar" id="style-3">     
<div class="row">
        <!-- Left col -->
	<div class="col-lg-12 col-sm-12 col-xs-12" style="width:1030px; margin-top:10px;margin-left:15px;" >
   <div class="box box-info">
           <div id="map" style="width: 1000px; height: 500px; border: 1px #a3c86d; border-style: none solid solid solid;"></div>
        </div>
	</div>
</div>
<?php $resparameter = $this->model->DashBoard_Contamination(); ?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-Contamination',
        width: '1000',
        height: '500',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "WHO Guidelines",
                "subCaption": "",
                "xAxisname": "",
                "numbersuffix": "",
                "paletteColors": "#008080,#FF8C00",
                "borderColor": "#e0d9d9",
                "borderThickness": "4",
                "borderAlpha": "80",
                "bgColor": "#e0d9d9",
                "borderAlpha": "20",
                "labeldisplay": "rotate",
                "slantlabels": "1",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#FFFFFF",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "<?php echo $resparameter[0]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[1]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[2]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[3]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[4]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[5]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[6]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[7]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[8]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[9]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[10]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[11]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[12]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[13]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[14]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[15]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[16]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[17]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[18]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[19]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[20]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[21]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[22]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[23]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[24]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[25]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[26]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[27]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[28]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[29]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[30]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[31]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[32]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[33]->Name; ?>"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Within",
                    "data": [
                       {
                            "value": "<?php echo $resparameter[0]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[1]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[2]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[3]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[4]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[5]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[6]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[7]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[8]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[9]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[10]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[11]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[12]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[13]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[14]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[15]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[16]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[17]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[18]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[19]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[20]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[21]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[22]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[23]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[24]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[25]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[26]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[27]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[28]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[29]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[30]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[31]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[32]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[33]->within_limit; ?>"
                        }
                    ]
                },
                {
                    "seriesname": "Outside",
                    "data": [
                         {
                            "value": "<?php echo $resparameter[0]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[1]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[2]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[3]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[4]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[5]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[6]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[7]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[8]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[9]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[10]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[11]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[12]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[13]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[14]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[15]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[16]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[17]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[18]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[19]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[20]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[21]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[22]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[23]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[24]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[25]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[26]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[27]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[28]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[29]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[30]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[31]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[32]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[33]->not_within_limit; ?>"
                        }
                    ]
                }
            ]
        }
    }).render();    
});
</script>
	<div class="row">
      
	<div class="col-lg-12 col-sm-12 col-xs-12" style="margin-left:15px;">
   
    <div id="chart-container-Contamination" ></div>
       
	</div>
    </div>

    <?php $resparameter = $this->model->DashBoard_India_Contamination(); ?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-ContaminationIndia',
        width: '1000',
        height: '500',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Selected Country Standrads ",
                "subCaption": "",
                "xAxisname": "",
                "numbersuffix": "",
                "paletteColors": "#008080,#FF8C00",
                "borderColor": "#e0d9d9",
                "borderThickness": "4",
                "borderAlpha": "80",
                "bgColor": "#e0d9d9",
                "borderAlpha": "20",
                "labeldisplay": "rotate",
                "slantlabels": "1",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#FFFFFF",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "<?php echo $resparameter[0]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[1]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[2]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[3]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[4]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[5]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[6]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[7]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[8]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[9]->Name; ?>"
                        },
                        {
                            "label":  "<?php echo $resparameter[10]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[11]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[12]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[13]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[14]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[15]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[16]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[17]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[18]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[19]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[20]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[21]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[22]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[23]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[24]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[25]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[26]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[27]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[28]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[29]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[30]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[31]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[32]->Name; ?>"
                        },
                        {
                            "label": "<?php echo $resparameter[33]->Name; ?>"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Within",
                    "data": [
                       {
                            "value": "<?php echo $resparameter[0]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[1]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[2]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[3]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[4]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[5]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[6]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[7]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[8]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[9]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[10]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[11]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[12]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[13]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[14]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[15]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[16]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[17]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[18]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[19]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[20]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[21]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[22]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[23]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[24]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[25]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[26]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[27]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[28]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[29]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[30]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[31]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[32]->within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[33]->within_limit; ?>"
                        }
                    ]
                },
                {
                    "seriesname": "Outside",
                    "data": [
                         {
                            "value": "<?php echo $resparameter[0]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[1]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[2]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[3]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[4]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[5]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[6]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[7]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[8]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[9]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[10]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[11]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[12]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[13]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[14]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[15]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[16]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[17]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[18]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[19]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[20]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[21]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[22]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[23]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[24]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[25]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[26]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[27]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[28]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[29]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[30]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[31]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[32]->not_within_limit; ?>"
                        },
                        {
                            "value": "<?php echo $resparameter[33]->not_within_limit; ?>"
                        }
                    ]
                }
            ]
        }
    }).render();    
});
</script>


<div class="row">
      
	<div class="col-lg-12 col-sm-12 col-xs-12" style="margin-left:15px;">
   
    <div id="chart-container-ContaminationIndia" ></div>
       
	</div>
    </div>

    </div>

	</div>

    <script type="text/javascript">
          $(document).ready(function(){
            $("#Country").change(function(){
             
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getStates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                    //alert(data);
                    console.log(data);
                    $("#State").html(data);
                     $('#indexTSTDashboard').submit();
                    //$("#District").html("<option value= '0'>--All--</option>");
                   
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     

        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').su9bmit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexTSTDashboard').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexTSTDashboard').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                      
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
            
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                // alert(data);
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexTSTDashboard').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        }

    </script>
<?php   
        $strwhr = $this->session->userdata["Country"]; 
        $strstatewhr = $this->session->userdata["State"];
        $districtwhr =  $this->session->userdata["District"];
        $plantwhr = $this->session->userdata["Plant"]; 

    $getLocation = $this->model->DatafromOnWebService_Map($strwhr,$strstatewhr,$districtwhr,$plantwhr);
    foreach($getLocation as $row){
                $nama_kabkot  = $row->description;
                $longitude    = $row->Longitude;                              
                $latitude     = $row->Latitude;
                $Markercolor  = $row->Markercolor;
                /* Each row is added as a new array */
                $locations[]=array($nama_kabkot, $latitude, $longitude);
    }
    /* Convert data to json */
            $markers = json_encode($locations);
        // print_r($markers);
?>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>