 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>

<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>






 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Operational Sustainability 
       </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Operational Sustainability </li>
      </ol>
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Country</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" name="Country" id="Country">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($_POST['Country']==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($_POST['Country']==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <?php if($_POST['State'] ==''){ ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <?php } else{ ?>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
           <div class="info-box-content">
              <span class="info-box-text">State</span>
              <span class="info-box-text">
			   <div class="form-group">
               <?php $State = $this->model->getCountryStates($_POST['Country']);  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="">---All ---</option>
                     <?php foreach($State as $row){
                         if($_POST['State']==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($_POST['State']==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <?php } ?>

        <!-- /.col -->
         <?php if($_POST['District'] !=''){ ?>
         <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
       
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <?php } else{ ?>
          <?php $District = $this->model->getDistricts($_POST['Country'],$_POST['State']); ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">District</span>
              <span class="info-box-text">
			  <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($_POST['District']==$row->DistrictID){ echo "select";?>

                    <option value="<?php echo $row->DistrictID;?>" <?php if($_POST['District']==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <?php } ?>
       
	   <?php
     
       if($_POST['Plant'] !=''){
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']); ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                  if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant']==$prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
        <!-- /.col -->
      </div>
       <?php }else{ 
        $Plant = $this->model->getPlants($_POST['Country'],$_POST['State'],$_POST['District']);
         ?>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box-select">
            <div class="info-box-content">
              <span class="info-box-text">Plant</span>
              <span class="info-box-text">
			    <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($_POST['Plant']==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($_POST['Plant'] == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
			  
			  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

       <?php } ?>
      <!-- /.row -->
	  </form>
            <!-- /.row -->

   <div class="row">
 <?php  
  if(!empty($this->input->post())){
		 	 $countryid 	= $this->input->post('Country');
		 	 $stateid 	    = $this->input->post('State');
		 	 $districtid    = $this->input->post('District');
		 	 $plantid 	    = $this->input->post('Plant');

		 if(!empty($countryid)){
			 $strwhr = $countryid;
		 }
		  if(!empty($stateid)){
			 $strstatewhr = $stateid;
		 }
		 if(!empty($districtid)){
			 $districtwhr = $districtid;
		 }
		 if(!empty($plantid)){
			 $plantwhr = $plantid;
		 }
	}  
  
 $premonsoontwt = $getPieChartPre[0]->value;
 $postemonsoontwt = $getPieChartPre[1]->value;
 $TotalpremonsoonTWT = $premonsoontwt + $postemonsoontwt;
 $totalq = 100 - $TotalpremonsoonTWT;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-pie2d',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Quality Report Availability",
                "subCaption": "",
                "paletteColors": "#5889DA,#F97000",
                "bgColor": "#c4d8e0",
                 "yAxisName": "Report Availability",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Report Availability",
                    "value": "<?php echo $TotalpremonsoonTWT;?>"
                }, 
                 {
                    "label": "Not Availability",
                    "value": "<?php echo $totalq;?>"
                    
                }
            ]
        }
    }).render();
});
</script>

       


	<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title" style="margin-left:10px;" >Quality Report Availability</h3>
              </div>
           <div id="chart-container-pie2d"></div>
          
          </div>
	</section>

<?php  
  if(!empty($this->input->post())){
		 	 $countryid 	= $this->input->post('Country');
		 	 $stateid 	    = $this->input->post('State');
		 	 $districtid    = $this->input->post('District');
		 	 $plantid 	    = $this->input->post('Plant');

		 if(!empty($countryid)){
			 $strwhr = $countryid;
		 }
		  if(!empty($stateid)){
			 $strstatewhr = $stateid;
		 }
		 if(!empty($districtid)){
			 $districtwhr = $districtid;
		 }
		 if(!empty($plantid)){
			 $plantwhr = $plantid;
		 }
	}  
  
  //$PieChartData =  $this->model->CreatePieChartPost($strwhr,$strstatewhr,$districtid,$plantwhr);
  //$SumQuality = $PieChartData->Quality;
  //$totalq = 100 - $SumQuality;
  //print_r($getPieChartTDS);

  $Appearance   = $getPieChartTDS[0]->value;
  $Odour        = $getPieChartTDS[1]->value;
  $Taste        = $getPieChartTDS[2]->value;
  $TDSValue     = $getPieChartTDS[3]->value;
  $SUMAOTT      = $Appearance+$Odour+$Taste+$TDSValue;
  $totalaott    = 100-$SUMAOTT;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-plantandage',
        width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "A-O-T & TDS",
                "subCaption": "",
                "paletteColors": "#5889DA,#F97000",
                "bgColor": "#c4d8e0",
                 "yAxisName": "A-O-T & TDS",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Acceptable",
                    "value": "<?php echo $SUMAOTT;?>"
                }, 
                 {
                    "label": "Not Acceptable",
                    "value": "<?php echo $totalaott;?>"
                    
                }
            ]
        }
    }).render();
});
</script>




	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">A-O-T & TDS</h3>
              </div>
            <div id="chart-container-plantandage"></div>
          
          </div>
		  </section>
		  <!-- /.nav-tabs-custom -->
	</div>
<?php 
$tdowntime1 =array();
$techdowntime2 =array();
$technidowntime3 =array();
foreach($getTechDowntime as $row){

if($row->technical_downtime <= 10){
    $tdowntime1[] = $row->technical_downtime;
}
if(($row->technical_downtime < 10) && ($row->technical_downtime <= 25)){
    $techdowntime2[] = $row->technical_downtime;
}

if(($row->technical_downtime > 25)){
    $technidowntime3[] = $row->technical_downtime;
}

}
$tdowntime      = count($tdowntime1);
$techdowntime   = count($techdowntime2);
$technidowntime = count($technidowntime3);

?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-RuralAfford',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Technical Downtime",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Technical Downtime",
                "numberPrefix": "",
                "paletteColors": "#0075c2",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            }, 
        
            "data": [
                {
                    "label": "<=10%",
                    "value": "<?php echo $tdowntime;?>",
                    "Color": "#599DDC",
                    "seriesname": "<=10%"
                }, 
                {
                    "label": ">10% and <=25%",
                    "value": "<?php echo $techdowntime;?>",
                    "Color": "#ED7320",
                    "seriesname": ">10 and <=25%"
                }, 
                {
                    "label": ">25%",
                    "value": "<?php echo $technidowntime;?>",
                    "Color": "#A1A1A1",
                    "seriesname": ">25%"
                }
            ]
        }
    }).render();
});
</script>


<div class="row">
        <!-- Left col -->
	<section class="col-lg-6 connectedSortable">
	 <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title" style="margin-left:10px;">Technical Downtime  </h3>
              </div>
           <div id="chart-container-RuralAfford"></div>
          
          </div>
	</section>
<?php 
$Sdowntime1 = array();
$Saldowntime2 = array();
$Saltdowntime3 = array();
foreach($getSalDowntime as $row){

if($row->sales_downtime <= 10){
   $Sdowntime1[] = $row->sales_downtime;
}
if(($row->sales_downtime < 10) && ($row->technical_downtime <= 25)){
     $Saldowntime2[] = $row->sales_downtime;
}

if(($row->sales_downtime > 25)){
     $Saltdowntime3[] = $row->sales_downtime;
}

}
 $Sdowntime = count($Sdowntime1);
 $Saldowntime = count($Saldowntime2);
 $Saltdowntime = count($Saltdowntime3);
//print_r($Sdowntime);
?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-UrbanAfford',
       width: '500',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Sales Downtime",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
                "yAxisName": "Sales Downtime",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#272727",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#272727",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            }, 
        
            "data": [
                {
                    "label": "<=10%",
                    "value": "<?php echo $Sdowntime;?>",
                    "Color": "#599DDC",
                    "seriesname": "<=10%"
                }, 
                {
                    "label": ">10% and <=25%",
                    "value": "<?php echo $Saldowntime;?>",
                    "Color": "#ED7320",
                    "seriesname": ">10% and <=25%"
                }, 
                {
                    "label": ">25%",
                    "value": "<?php echo $Saltdowntime;?>",
                    "Color": "#A1A1A1",
                    "seriesname": ">25%     "
                }
            ]
        }
    }).render();
});
</script>

   	<section class="col-lg-6 connectedSortable">
       <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Sales Downtime</h3>
              </div>
            <div id="chart-container-UrbanAfford"></div>
          
          </div>
		  </section>


</div>


	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexOperational').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexOperational').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            var id = $('#Plant').val();
             var districtid = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
           
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getSingelPlants/'+ Countryid +'/'+ Stateid +'/'+ districtid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                  
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexOperational').submit();
                     
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   