 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////

            //echo $State;
?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>
<style>
/*.dropdownmenu ul, .dropdownmenu li {
	margin: 0;
	padding: 0;
}
.dropdownmenu ul {
	background: gray;
	list-style: none;
	width: 100%;
}
.dropdownmenu li {
	float: left;
	position: relative;
	width:auto;
}
.dropdownmenu a {
	background: #3c8dbc;
	color: #FFFFFF;
	display: block;
	font: bold 11px/20px sans-serif;
	padding: 8px 15px;
	text-align: center;
	text-decoration: none;
	-webkit-transition: all .25s ease;
	-moz-transition: all .25s ease;
	-ms-transition: all .25s ease;
	-o-transition: all .25s ease;
	transition: all .25s ease;
}
.dropdownmenu li:hover a {
	background: #000000;
}*/
#submenu {
	left: 0;
	opacity: 0;
	position: absolute;
	top: 35px;
	visibility: hidden;
	z-index: 1;
}
li:hover ul#submenu {
	opacity: 1;
	top: 40px;	/* adjust this as per top nav padding top & bottom comes */
	visibility: visible;
}
#submenu li {
	float: none;
	width: 100%;
}
#submenu a:hover {
	background: #DF4B05;
}
#submenu a {
	background-color:#000000;
}
</style>
<?php 
  //$getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
 // print_r($getPlantAge); die;
  $jsonplantaged = json_encode($getPlantAge); 
 ?>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class ="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <nav class="dropdownmenu">
  <ul>
    <li><a href="<?php echo site_url().'Dashboard/index';?>" >PAT Dashboard</a></li>
    <li><a href="<?php echo site_url().'Social/index';?>" >Social Sustainability</a></li>
    <li><a href="<?php echo site_url().'Operational/index';?>"  style="background-color:#000000;">Operational Sustainability</a> </li>
    <li><a href="<?php echo site_url().'Financial/index';?>" >Financial Sustainability</a></li>
    <li><a href="<?php echo site_url().'Institutional/index';?>" >Institutional Sustainability</a></li>
    <li><a href="<?php echo site_url().'Environmental/index';?>" >Environmental Sustainability</a></li>
  </ul>
</nav>

</div>
</div>
    </section>
	 <?php  //print_r($this->session); ?>
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	    <div class="row">
          <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="0">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($this->session->userdata("Country")==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($this->session->userdata("Country")==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
          </div>
       
         <div class="col-xs-1" style="top:7px;font-size:12px;"><b>State/Province:</b></div>
         
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($this->session->userdata("Country"));  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="0">---All ---</option>
                     <?php foreach($State as $row){
                         if($this->session->userdata("State")==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($this->session->userdata("State")==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select> 
                </div>  	 
          </div>        
          
      <!-- /.col -->
       <div class="col-xs-1" style="top:7px;font-size:12px;"><b>District/Zone:</b></div>
         
         <?php $District = $this->model->getDistricts($this->session->userdata("Country"),$this->session->userdata("State")); 
             ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="0">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($this->session->userdata("District") == $row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($this->session->userdata("District") == $row->DistrictID) { echo 'selected="selected"'; } ?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>
           
       
	   <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Plant:</b></div>
          <div class="col-xs-2">
     <?php 
        $Plant = $this->model->getPlants($this->session->userdata("Country"),$this->session->userdata("State"),$this->session->userdata("District")); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="0">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($this->session->userdata("Plant")==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($this->session->userdata("Plant")== $prow->PlantGUID) { echo 'selected="selected"'; }?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
            </div>
    </div>
      <!-- /.row -->
	  </form>
 <?php  
 $premonsoontwt = $getPieChartPre[0]->value;
 $postemonsoontwt = $getPieChartPre[1]->value;
 $TotalpremonsoonTWT = $premonsoontwt + $postemonsoontwt;
 $totalq = 100 - $TotalpremonsoonTWT;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-pie2d',
        width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Quality Report Availability",
                "subCaption": "",
                "paletteColors": "#008080,#F97000",
                "bgColor": "#FFFFFF",
                 "yAxisName": "Report Availability",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFoTechnical DowntimentSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Report Availability",
                    "value": "<?php echo $TotalpremonsoonTWT;?>"
                }, 
                 {
                    "label": "Not Availability",
                    "value": "<?php echo $totalq;?>"
                    
                }
            ]
        }
    }).render();
});
</script>
<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 405px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}

.rotate {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */}
</style>
<div class="scrollbar" id="style-3">
 <div class="row">
    <div class="col-lg-12"  style="padding-left: 15px;">
        <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color:#0EBCE4;" ><b>QUALITY</b></div>
    </div>
    </div>
    
  </div>
       

<div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b>QUALITY REPORT AVAILABILITY</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
             <div id="chart-container-pie2d"></div>
        </div>
    </div>
   </div>
   </div>
<?php  
  $Appearance   = $getPieChartTDS[0]->value;
  $Odour        = $getPieChartTDS[1]->value;
  $Taste        = $getPieChartTDS[2]->value;
  $TDSValue     = $getPieChartTDS[3]->value;
  $SUMAOTT      = $Appearance+$Odour+$Taste+$TDSValue;
  $totalaott    = 100-$SUMAOTT;
?>

<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-plantandage',
        width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "A-O-T & TDS",
                "subCaption": "",
                "paletteColors": "#008080,#F97000",
                "bgColor": "#FFFFFF",
                // "yAxisName": "A-O-T & TDS",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "data": [
                {
                    "label": "Acceptable",
                    "value": "<?php echo $SUMAOTT;?>"
                }, 
                 {
                    "label": "Not Acceptable",
                    "value": "<?php echo $totalaott;?>"
                    
                }
            ]
        }
    }).render();
});
</script>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b>A-O-T & TDS</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
              <div id="chart-container-plantandage"></div>
        </div>
    </div>
   </div>
   </div>
	</div>



<div class="row"></div>

  <div class="row">
    <div class="col-lg-12"  style="padding-left: 15px;">
        <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color:#0EBCE4;" ><b>DOWNTIME</b></div>
    </div>
    </div>
    
</div>

<?php 
$tdowntime1 =array();
$techdowntime2 =array();
$technidowntime3 =array();
foreach($getTechDowntime as $row){

if($row->technical_downtime <= 10){
    $tdowntime1[] = $row->technical_downtime;
}
if(($row->technical_downtime > 10) && ($row->technical_downtime <= 25)){
    $techdowntime2[] = $row->technical_downtime;
}

if(($row->technical_downtime > 25)){
    $technidowntime3[] = $row->technical_downtime;
}

}
$tdowntime      = count($tdowntime1);
$techdowntime   = count($techdowntime2);
$technidowntime = count($technidowntime3);

?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-RuralAfford',
       width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Technical Downtime",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
               // "yAxisName": "Technical Downtime",
                "numberPrefix": "",
                "paletteColors": "#0075c2",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderATechnical Downtimelpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#FFFFFF",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            }, 
        
            "data": [
                {
                    "label": "<=10%",
                    "value": "<?php echo $tdowntime;?>",
                    "Color": "#599DDC",
                    "seriesname": "<=10%"
                }, 
                {
                    "label": ">10% and <=25%",
                    "value": "<?php echo $techdowntime;?>",
                    "Color": "#ED7320",
                    "seriesname": ">10 and <=25%"
                }, 
                {
                    "label": ">25%",
                    "value": "<?php echo $technidowntime;?>",
                    "Color": "#008080",
                    "seriesname": ">25%"
                }
            ]
        }
    }).render();
});
</script>

 <div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 15px;">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:RURAL100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b>TECHNICAL DOWNTIME</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
             <div id="chart-container-RuralAfford"></div>
        </div>
    </div>
   </div>
   </div>


<?php 
$Sdowntime1 = array();
$Saldowntime2 = array();
$Saltdowntime3 = array();
foreach($getSalDowntime as $row){

if($row->sales_downtime <= 10){
   $Sdowntime1[] = $row->sales_downtime;
}
if(($row->sales_downtime > 10) && ($row->technical_downtime <= 25)){
     $Saldowntime2[] = $row->sales_downtime;
}

if(($row->sales_downtime > 25)){
     $Saltdowntime3[] = $row->sales_downtime;
}

}
 $Sdowntime = count($Sdowntime1);
 $Saldowntime = count($Saldowntime2);
 $Saltdowntime = count($Saltdowntime3);
//print_r($Sdowntime);
?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'column2d',
        renderAt: 'chart-container-UrbanAfford',
       width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Sales Downtime",
                //"subCaption": "Harry's SuperMart",
                "xAxisName": " ",
               // "yAxisName": "Sales Downtime",
                "numberPrefix": "",
                "paletteColors": "#353535",
                "bgColor": "#FFFFFF",
                "borderAlpha": "20",
                "canvasBorderAlpha": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placevaluesInside": "1",
                "rotatevalues": "1",
                "valueFontColor": "#FFFFFF",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14"
            }, 
        
            "data": [
                {
                    "label": "<=10%",
                    "value": "<?php echo $Sdowntime;?>",
                    "Color": "#599DDC",
                    "seriesname": "<=10%"
                }, 
                {
                    "label": ">10% and <=25%",
                    "value": "<?php echo $Saldowntime;?>",
                    "Color": "#ED7320",
                    "seriesname": ">10% and <=25%"
                }, 
                {
                    "label": ">25%",
                    "value": "<?php echo $Saltdowntime;?>",
                    "Color": "#008080",
                    "seriesname": ">25%     "
                }
            ]
        }
    }).render();
});
</script>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:550px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b> 
   SALES DOWNTIME</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
       <div class="box box-info">
      <div id="chart-container-UrbanAfford"></div>
   </div>
   </div>

   </div>
  </div>
	

   </div>

   
	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexOperational').submit();
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexOperational').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexOperational').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
            $('#indexOperational').submit();
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   