
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Parameter Management
        <small>preview of Parameter Management</small>
      </h1>
	  
	   <a href="<?php echo site_url()."/Parameter/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add Parameter
            </a>
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Parameter Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
          <div class="box">
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
            
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
			
			  <div class="widget-body">
                    <div class="tab-pane" >
				<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
              <div class="box-body">
                <div class="form-group">
              	  
				  <label for="exampleInputEmail1">Name</label>
				  <?php if(form_error('form[Name]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[Name]','id'=>'Name','value'=>set_value('form[Name]',$data->Name),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Name','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[Name]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[Name]')?></label></div>
				<?php } ?>
				
				 <label for="exampleInputEmail1">Unit</label>
				  <?php if(form_error('form[uoM]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[uoM]','id'=>'uoM','value'=>set_value('form[uoM]',$data->uoM),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Unit','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[uoM]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[uoM]')?></label></div>
				<?php } ?>
				  
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            <?php print form_close(); ?>
					  
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
</div>	 

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	







