 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Management
        <small>preview of User Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">User Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Listing </h3>

             
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
             <?php 
			 $token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		     $data = $this->model->getDetail($token);
			
			 ?>
			  <div class="widget-body">
                    <div class="tab-pane" >
				<?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">First Name</label>
				  
				<?php if(form_error('form[FirstName]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[FirstName]','id'=>'FirstName','value'=>set_value('form[FirstName]',$data->FirstName),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' FirstName','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[FirstName]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[FirstName]')?></label></div>
				<?php } ?>
	  
				  <label for="exampleInputEmail1">Middle Name</label>
				  <?php if(form_error('form[MiddleName]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[MiddleName]','id'=>'MiddleName','value'=>set_value('form[MiddleName]',$data->MiddleName),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' MiddleName','autocomplete'=>'off')); ?>
				<?php if(form_error('form[MiddleName]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[MiddleName]')?></label></div>
				<?php } ?>
				  
				  <label for="exampleInputEmail1">Last Name</label>
				<?php if(form_error('form[LastName]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[LastName]','id'=>'LastName','value'=>set_value('form[LastName]',$data->LastName),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' LastName','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[LastName]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[LastName]')?></label></div>
				<?php } ?>
				  		  				  
				  <label for="exampleInputEmail1">User Name</label>
				  <?php if(form_error('form[UserName]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[UserName]','id'=>'UserName','value'=>set_value('form[UserName]',$data->UserName),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' UserName','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[UserName]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[UserName]')?></label></div>
				<?php } ?>
				 
                </div>
                <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
				<?php if(form_error('form[Password]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[Password]','id'=>'Password','value'=>set_value('form[Password]',$data->Password),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Password','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[Password]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[Password]')?></label></div>
				<?php } ?>
				</div>
				 <div class="form-group">
                  <label for="exampleInputPassword1">Confirm  Password</label>
				  <input type="Password" class="form-control" id="ConfrimPassword" value="<?php echo $data->Password;?>" name="ConfrimPassword" class="form-control" placeholder="Enter Confrim Password" Required>
				</div>
				<div class="form-group">
                  <label for="exampleInputPassword1">Email</label>
	     		  <?php if(form_error('form[Email]')){ ?><div class="form-group has-error"><?php } ?>
				<?php print form_input(array('name'=>'form[Email]','id'=>'Email','value'=>set_value('form[Email]',$data->Email),'class'=>'form-control','placeholder'=>ucfirst(str_replace('_',' ',$this->router->class)).' Email ','autocomplete'=>'off','required'=>'required')); ?>
				<?php if(form_error('form[Email]')){?>
				<label class="control-label" for="inputError"><?php print form_error('form[Email]')?></label></div>
				<?php } ?>
				</div>
				
				<div class="form-group">
                  <label for="exampleInputPassword1">Status</label>
	     		  <?php $status = array(0=>'Pending',1=>'Active'); ?>
				  <select class="form-control" name="form[IsActive]" id="IsActive" required>
                    <option value="">Select </option>
					<?php foreach($status as $key => $val){ 
					if($key ==$data->IsActive){
					?>
                    <option value="<?php echo $key;?>" SELECTED><?php echo $val;?></option>
                    <?php }else{ ?>
					 <option value="<?php echo $key;?>" ><?php echo $val;?></option>
					<?php } } ?>
                  </select></div>
				
				<?php  $Role = $this->model->getLevel(); ?>
				<div class="form-group">
                  <label for="exampleInputPassword1">User Level:</label>
					<select class="form-control" name="form[RoleID]" id="RoleID" required>
                    <option value="">Select </option>
					<?php foreach($Role as $key => $val){  
					if($val->RoleID == $data->RoleID){
					?>
					<option value="<?php echo $val->RoleID;?>"SELECTED ><?php echo $val->RoleName;?></option>
					<?php  }else{ ?>
					
                    <option value="<?php echo $val->RoleID;?>"><?php echo $val->RoleName;?></option>
                    <?php }} ?>
                  </select>
				  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            <?php print form_close(); ?>
					  
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
</div>	  
    </section>
    <!-- /.content -->
	
