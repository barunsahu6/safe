  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Management
        <small>preview of User Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">User Management</li>
      </ol>
    </section>

    <!-- Main content -->
  <section class="content">
      <div class="row">
          
		<div class="col-xs-12">
         
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List State</h3>
            </div>
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>User Name</th>
                  <th>Staff Name</th>
				  <th>Email</th>
                  <th>User Level</th>
                  <th>Status</th>
                  <th>Update</th>
				  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
				<?php 	
				$i=1;
				$getdata =$this->db->query("SELECT mstu.`UserID`,mstu.UserName,mstu.`FirstName`, mstu.LastName, mstu.Email,mstr.RoleName,mstu.IsActive FROM mstuser as mstu 
											INNER Join mstrolebkupbkup as mstr on mstr.RoleID=mstu.RoleID")->result();
				
				foreach($getdata as $detail){
				?>
                <td><?php echo $i;?></td>
                  <td><?php echo $detail->UserName;?></td>
                  <td><?php echo $detail->FirstName ." ".$detail->LastName;?></td>
				  <td><?php echo $detail->Email;?></td>
				  <td><?php echo $detail->RoleName;?></td>
                  <td>
				  <?php if($detail->IsActive==1){?>
				  <span class="label label-success">Approved</span>
				  <?php } else{ ?>
				  <span class="label label-warning">Pending</span>
				  <?php } ?>
				  </td>
				  <td><a href="<?php print base_url().$this->router->class.'/edit/'.$detail->UserID; ?>" ><span class="fa fa-fw fa-edit"> </span></a></td>
                   <td><a href="<?php print base_url().$this->router->class.'/delete/'.$detail->UserID; ?>" onclick="return confirm_delete()" ><span class="fa fa-fw fa-remove"> </span></a></td>
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>User Name</th>
                  <th>Staff Name</th>
				  <th>Email</th>
                  <th>User Level</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->		
 
 
 
 
          </div>
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 
	 
	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
	 
