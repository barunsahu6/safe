 <?php
               $userid       =   $this->session->userdata('USERID');////// Session Userid/////
               $roleid       =   $this->session->userdata('LEVEL'); ////// Session Role Id /////
               $strwhr       = $this->session->userdata('COUNTRYID'); ////// Session Role Id /////
            //echo $State;
?>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD1wOf9ISBejnZhC-umNwWdaQbuSk3mMwg&sensor=false"> </script>
<style>

#submenu {
	left: 0;
	opacity: 0;
	position: absolute;
	top: 35px;
	visibility: hidden;
	z-index: 1;
}
li:hover ul#submenu {
	opacity: 1;
	top: 40px;	/* adjust this as per top nav padding top & bottom comes */
	visibility: visible;
}
#submenu li {
	float: none;
	width: 100%;
}
#submenu a:hover {
	background: #DF4B05;
}
#submenu a {
	background-color:#000000;
}
</style>
<?php 
  // $getPlantAge   = $this->model->getDashboardPlantAndAge($strwhr);
  $getPlantAge   = $this->model->getDashboardYearWiseFunder($strwhr);
 // print_r($getPlantAge); die;
  // $jsonplantaged = json_encode($getPlantAge); 
 ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class ="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <nav class="dropdownmenu">
  <ul>
    <li><a href="<?php echo site_url().'Dashboard/index';?>" >PAT Dashboard</a></li>
    <li><a href="<?php echo site_url().'Social/index';?>" >Social Sustainability</a></li>
    <li><a href="<?php echo site_url().'Operational/index';?>" >Operational Sustainability</a> </li>
    <li><a href="<?php echo site_url().'Financial/index';?>"  style="background-color:#000000;">Financial Sustainability</a></li>
    <li><a href="<?php echo site_url().'Institutional/index';?>" >Institutional Sustainability</a></li>
    <li><a href="<?php echo site_url().'Environmental/index';?>">Environmental Sustainability</a></li>
  </ul>
</nav>

</div>
</div>
     
    </section>
	 
   <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
       <?php print form_open_multipart($this->router->class.'/'.$this->router->method.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
	   <div class="row">
          <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Country:</b></div>
          <div class="col-xs-2">
               <div class="form-group">
                  <select class="form-control" name="Country" id="Country" onchange="getAllCountry();">
                  <option value="">--All--</option>
                   <?php foreach($Country as $row){ 
                   if($this->session->userdata("Country")==$row->CountryID){
                   ?>
                  <option value="<?php echo $row->CountryID; ?>" <?php if($this->session->userdata("Country")==$row->CountryID) echo 'selected="selected"';?>  ><?php echo $row->CountryName;?></option>
                    <?php }else{ ?>
                        <option value="<?php echo $row->CountryID; ?>"><?php echo $row->CountryName;?></option>

                    <?php } }?>
                 </select>
                </div>
          </div>
        <!-- /.col -->
      <div class="col-xs-1" style="top:7px;font-size:12px;"><b>State/Province:</b></div>
         
          <div class="col-xs-2">
               <div class="form-group">
               <?php $State = $this->model->getCountryStates($this->session->userdata("Country"));  ?>
                  <select onchange="getAllState()" class="form-control" Name="State" id="State">
                    <option value="0">---All ---</option>
                     <?php foreach($State as $row){
                         if($this->session->userdata("State")==$row->StateID){     
                     ?>
                    <option value="<?php echo $row->StateID;?>" <?php if($this->session->userdata("State")==$row->StateID) echo 'selected="selected"';?>><?php echo $row->StateName;?></option>
                     <?php } else{ ?>
                      <option value="<?php echo $row->StateID;?>" ><?php echo $row->StateName;?></option>
                     <?php } } ?>
                  </select>
                </div>  	 
          </div>
        
        <!-- /.col -->
       <div class="col-xs-1" style="top:7px;font-size:12px;"><b>District/Zone:</b></div>
          
         <?php $District = $this->model->getDistricts($this->session->userdata("Country"),$this->session->userdata("State")); ?>
          <div class="col-xs-2">
             <div class="form-group">
                  <select class="form-control" onchange="getAllDistrict()" name="District" id="District">
                    <option value="0">---All ---</option>
                   <?php foreach($District as $row){ 
                   if($this->session->userdata("District")==$row->DistrictID){ 
                   ?>
                    <option value="<?php echo $row->DistrictID;?>" <?php if($this->session->userdata("District")==$row->DistrictID) echo 'selected="selected"';?>><?php echo $row->DistrictName;?></option>
                   <?php }else{ ?>
                    <option value="<?php echo $row->DistrictID;?>" ><?php echo $row->DistrictName;?></option>
                   <?php } } ?>
                  </select>
                </div>  		
          </div>
           
       <div class="col-xs-1" style="top:7px;font-size:12px;"><b>Plant:</b></div>
          <div class="col-xs-2">
     <?php 
        $Plant = $this->model->getPlants($this->session->userdata("Country"),$this->session->userdata("State"),$this->session->userdata("District")); ?>
      <div class="form-group">
                  <select class="form-control" onchange="getAllPlant()" id="Plant" name="Plant">
                  <option value="">--All--</option>
                  <?php foreach($Plant as $prow){
                     if($this->session->userdata("Plant")==$prow->PlantGUID){ 
                  ?>
                    <option value="<?php echo $prow->PlantGUID;?>" <?php if($this->session->userdata("Plant") == $prow->PlantGUID) echo 'selected="selected"';?> ><?php echo $prow->SWNID;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $prow->PlantGUID;?>" ><?php echo $prow->SWNID;?></option>
                   <?php }  } ?>
                  </select>
                </div>
   
          </div>
    </div>
</form>
<?php 
//echo "<pre>";
//print_r($getYearWiseFunder); ?>
<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-plantandage',
        width: '450',
        height: '250',
        dataFormat: 'json',           
        dataSource: {
            "chart": {
                "caption": "Year wise Funder data",
                "subCaption": "",
                "xAxisname": "",
                //"yAxisName": "Funder data",
                //"numbersuffix": "%",
                "paletteColors": "#3E719E,#84B961,#FDC226,#EA8E4F,#008080,#6EA3D5",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "categories": [
                {
                    "category": [
                        {
                            "label":  "<?php echo $getYearWiseFunder[0]->label;?> "
                        },
                        {
                            "label":  "<?php echo $getYearWiseFunder[1]->label;?>  "
                        },
                        {
                            "label": "<?php echo $getYearWiseFunder[2]->label;?>  "
                        },
                        {
                            "label": "<?php echo $getYearWiseFunder[3]->label;?>  "
                        },
                        {
                            "label": "<?php echo $getYearWiseFunder[4]->label;?>  "
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "HONEYWELL",
                    "data": [
                        {
                            "value": "<?php echo $getYearWiseFunder[0]->HONEYWELL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[1]->HONEYWELL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[2]->HONEYWELL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[3]->HONEYWELL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[4]->HONEYWELL;?>"
                        }
                    ]
                },
                {
                    "seriesname": "PEPSICO",
                    "data": [
                        {
                            "value": "<?php echo $getYearWiseFunder[0]->PEPSICO;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[1]->PEPSICO;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[2]->PEPSICO;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[3]->PEPSICO;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[4]->PEPSICO;?>"
                        }
                    ]
                },
                {
                    "seriesname": "TATA",
                    "data": [
                        {
                            "value": "<?php echo $getYearWiseFunder[0]->TATA;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[1]->TATA;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[2]->TATA;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[3]->TATA;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[4]->TATA;?>"
                        }
                    ]
                },
                {
                    "seriesname": "BHEL",
                    "data": [
                        {
                            "value": "<?php echo $getYearWiseFunder[0]->BHEL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[1]->BHEL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[2]->BHEL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[3]->BHEL;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[4]->BHEL;?>"
                        }
                    ]
                },
                {
                    "seriesname": "NRTT",
                    "data": [
                        {
                            "value": "<?php echo $getYearWiseFunder[0]->NRTT;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[1]->NRTT;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[2]->NRTT;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[3]->NRTT;?>"
                        },
                        {
                            "value": "<?php echo $getYearWiseFunder[4]->NRTT;?>"
                        }
                    ]
                }
            ]
        }
    }).render();    
});

</script>
<style>
.scrollbar
{
	margin-left: 0px;
	float: left;
	height: 405px;
	width: auto;
	background: #F5F5F5;
	overflow-y: scroll;
    overflow-x: hidden;
	margin-bottom: 25px;
}

#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
.rotate {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */}
</style>
<div class="scrollbar" id="style-3">
  <div class="row">
    <div class="col-lg-12"  style="padding-left: 15px;">
        <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color:#67899A;"><b>SOURCES OF FUNDS</b></div>
    </div>
    </div>
    
  </div> 
       

<div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b>FUNDING TRENDS</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
              <div id="chart-container-plantandage"></div>
        </div>
    </div>
   </div>
   </div>
<?php 
//echo "<pre>";
//print_r($GetPiechart);
$txtsum =  $GetPiechart[0]->VALUE + $GetPiechart[1]->VALUE+$GetPiechart[2]->VALUE+$GetPiechart[3]->VALUE+$GetPiechart[4]->VALUE;
?>
<script type="text/javascript">
FusionCharts.ready(function () {
    var ageGroupChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'chart-container-Funderwishbreadup',
        width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Funder wise break -up",
                "subCaption": "Total Number of Plants assessed <?php echo $PieChartData->TotalHouseholds ?>",
                "paletteColors": "#FBC412,#4C92D0,#F07B2C,#4876C4,",
                "bgColor": "#FFFFFF",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666',
               // "plottooltext": "<div id='nameDiv'>$label :<b>$value</div>",
            },
            "data": [
                {
                    "label"   : "Government",
                    "value"   : "<?php echo ($GetPiechart[0]->VALUE/$txtsum)*100;?>",
                    "tooltext": " Government : <?php echo $GetPiechart[0]->VALUE;?> "
                }, 
                 {
                    "label": "Grant",
                    "value": "<?php echo ($GetPiechart[1]->VALUE/$txtsum)*100;?>",
                     "tooltext": " Grant : <?php echo $GetPiechart[1]->VALUE;?> "
                }, 
                 {
                    "label": "Self Funded (Individual/Group)",
                    "value": "<?php echo ($GetPiechart[2]->VALUE/$txtsum)*100;?>",
                     "tooltext": " Self Funded : <?php echo $GetPiechart[2]->VALUE;?> "
                }, 
                 {
                    "label": "SWE Franchisor",
                    "value": "<?php echo ($GetPiechart[3]->VALUE/$txtsum)*100;?>",
                     "tooltext": " SWE Franchisor : <?php echo $GetPiechart[3]->VALUE;?> "
                }, 
                 {
                    "label": "Bank loan",
                    "value": "<?php echo ($GetPiechart[4]->VALUE/$txtsum)*100;?>",
                    "tooltext": " Bank loan : <?php echo $GetPiechart[4]->VALUE;?> "
                }
            ]
        }
    }).render();
});
</script>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b>FUNDER WISE </b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
               <div id="chart-container-Funderwishbreadup"></div>
        </div>
    </div>
   </div>
   </div>
	</div> 

<script>
FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container-Financialmance',
        width: '450',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                //"caption": "Financial Performance",
                "subCaption": "",
                "xAxisname": "",
                //"yAxisName": "Financial Performance",
                "formatNumber" :"0",
                "formatNumberScale" :"0",
                "xFormatNumberScale":"0",
                "paletteColors": "#62A0DB,#EF7522,#A9A9AB",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                //"plotToolText": "Opex:  <br> Sales (YTD): $dataValue <br> $displayValue",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect":"1",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "rotateValues": "0",
                "valueFont": "Arial",
                "valueFontColor": "#FFFFFF",
                "valueFontSize": "12",
                "valueFontBold": "1",
                "valueFontItalic": "0",
                "legendPosition": "right",
            },
            "categories": [
                {
                    "category": [
                        
                        {
                            "label":  ""
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Opex",
                    "data": [
                        {
                            "value": "<?php echo $GetCOPEX->opex;?>",
                            "Color": "#62A0DB"
                        }
                       
                    ]
                }, 
                {
                    "seriesname": "Service",
                    "data": [
                        {
                            "value": "<?php echo $GetCOPEX->service;?>",
                             "Color": "#EF7522"
                        }
                    ]
                }, 
                {
                    "seriesname": "ARF",
                    "data": [
                        {
                            "value": "<?php echo $GetCOPEX->ARF;?>",
                             "Color": "#A9A9AB"
                        }
                    ]
                }
                
            ]
        }
    }).render();    
});

</script>

<div class="row"></div>

  <div class="row">
    <div class="col-lg-12"  style="padding-left: 15px;">
        <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color:#67899A;" ><b>FINANCIAL PERFORMANCE</b></div>
    </div>
    </div>
    
</div>

<div class="row">
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 15px;">
   <div style="float:left; width:535px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:RURAL100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b>REVENUE BREAK-UP</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
   <div class="box box-info">
             <div id="chart-container-Financialmance"></div>
        </div>
    </div>
   </div>
   </div>

<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'mscolumn2d',
    renderAt: 'chart-container-Financial-Performance',
    width: '450',
    height: '250',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            //"caption": "Financial Performance",
            "subCaption": "",
            "xAxisname": "",
            "yAxisName": "",
            //"numberPrefix": "$",
            "plotHighlightEffect": "fadeout",
            "theme": "fint",
            "canvasBgColor":"#FFFFFF",
            "paletteColors": "#5196D7,#F67D2E,#008080,#FEC502",
            "bgColor": "#FFFFFF",
            "borderAlpha": "20",
            "canvasBorderAlpha": "0",
            "usePlotGradientColor": "0",
            "plotBorderAlpha": "10",
            "placevaluesInside": "1",
            "rotatevalues": "1",
            "valueFontColor": "#272727",                
            "showXAxisLine": "1",
            "xAxisLineColor": "#272727",
            "divlineColor": "#272727",               
            "divLineIsDashed": "1",
            "showAlternateHGridColor": "0",
            "subcaptionFontBold": "0",
            "subcaptionFontSize": "14",
            "rotateValues": "0",
            "valueFont": "Arial",
            "valueFontColor": "#FFFFFF",
            "valueFontSize": "12",
            "valueFontBold": "1",
            "valueFontItalic": "0",
            "legendPosition": "right"
        },
        "categories": [{
            "category": [{
                "label": " "
            }]
        }],
        "dataset": [{
            "seriesname": "Revenue<OpEx",
            "data": [{
                "value": "<?php echo $GetfourthOpexServic[0]->value;?>",
                "Color": "#5196D7"
				
            }]
        }, {
            "seriesname": "OpEx<Revenue<OpEx+Services",
            "data": [{
                "value": "<?php echo $GetfourthOpexServic[1]->value;?>",
                 "Color": "#F67D2E"
				
            }]
        }, {
            "seriesname": "OpEx+Services<Revenue<OpEx+Services+ARF ",
            "data": [{
                "value": "<?php echo $GetfourthOpexServic[2]->value;?>",
                 "Color": "#008080"
				
            }]
        }, {
            "seriesname": "Revenue>OpEx+Services+ARF<OpEx",
            "data": [{
                "value": "<?php echo $GetfourthOpexServic[3]->value;?>",
                 "Color": "#FEC502"
        }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <div style="float:left; width:550px;height:auto;">
   <div style="float:left;width:40px; height:250px; background-color:#08c; color:#fff">
   <div style="float:left;width:100%; height:100%; ">
   <div class="rotate text-center" style="width: 300px;height: 40px;margin-left: -120px;
    margin-top: 108px;"><b> 
   VIABILITY</b></div>
   </div>
   </div>
   <div style="float:left;width:calc(100% - 85px); height:100%; ">
       <div class="box box-info">
      <div id="chart-container-Financial-Performance"></div>
   </div>
   </div>
   </div>
   </div>
  </div>
 </div>

	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
 <script type="text/javascript">
        
        $(document).ready(function(){
            $("#Country").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getstates/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   // alert(data);
                    console.log(data);
                    $("#State").html(data);
                    $("#District").html("<option value= ''>--All--</option>");
                    $("#Plant").html("<option value= ''>--All--</option>");
                     $('#indexFinancial').submit();
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     


        function getAllState(){
             var id = $('#State').val();
              var Countryid = $('#Country').val();
             if(id !=''){
           
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getdistricts/'+ Countryid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                    $("#District").html(data);
                   // $("#Plant").html("<option value= ''>--All--</option>");
                    $('#indexFinancial').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
         function getAllDistrict(){
             var id = $('#District').val();
             var Stateid = $('#State').val();
              var Countryid = $('#Country').val();
              if(id !=''){
              $.ajax({
                    url: '<?php echo site_url(); ?>/ajax/getPlants/'+ Countryid +'/'+ Stateid +'/'+id,
                     type: 'POST',
                     dataType: 'text',
                 })
             .done(function(data) {
                    console.log(data);
                    //$('#indexDashboard').submit();
                  //  $("#District").html(data);
                    $("#Plant").html(data);
                    $('#indexFinancial').submit();
                     return false;
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

             }
                       
        }
       
        function getAllPlant(){
             $('#indexFinancial').submit();                      
        }

    </script>

   <script type="text/javascript">
     var locations = <?php echo $markers;?>;
     var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(23.1667, 79.9333),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
   
    for (i = 0; i < locations.length; i++) { 
       marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


   
   