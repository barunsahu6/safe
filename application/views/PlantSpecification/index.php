  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Plant Specification Management
        <small>preview of Plant Specification Management</small>
      </h1>
	  
	   <a href="<?php echo site_url()."/PlantSpecification/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add Plant Specification
       </a>
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Plant Specification Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          
		<div class="col-xs-12">
         
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Plant Specification</h3>
            </div>
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Plant Specification</th>
                   <th>Update</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
				<?php 	
				$i=1;
				$getdata =$this->db->query("SELECT mstsp.PlantSpecificationID, mstsp.PlantSpecificationName
											FROM `mstplantspecification` AS mstsp  WHERE mstsp.IsDeleted=0 
											ORDER BY mstsp.PlantSpecificationID DESC")->result();
				
				foreach($getdata as $detail){
				?>
                <td><?php echo $i;?></td>
                  <td><?php echo $detail->PlantSpecificationName;?></td>
                 
				  <td><a href="<?php print base_url().$this->router->class.'/edit/'.$detail->PlantSpecificationID; ?>" ><span class="fa fa-fw fa-edit"> </span></a></td>
                   <td><a href="<?php print base_url().$this->router->class.'/delete/'.$detail->PlantSpecificationID; ?>" onclick="return confirm_delete()" ><span class="fa fa-fw fa-remove"> </span></a></td>
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Plant Specification</th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->		
 
 
 
 
          </div>
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 
	 
	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
	 
    <!-- /.content -->
	 <!-- page script -->
      <script>
		function confirm_delete() {
			var r = confirm("Are you sure to delete record?");
			if (r == true) {
				return true;
			} else {
				return false;
			}
		}

   </script>	
