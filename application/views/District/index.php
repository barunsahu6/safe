  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        District Management
        <small>preview of District Management</small>
      </h1>
	  
	   <a href="<?php echo site_url()."/District/add/"; ?>" class="btn btn-app">
                <i class="fa fa-edit"></i> Add District
       </a>
	  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">District Management</li>
      </ol>
    </section>

    <!-- Main content -->
  <!-- Main content -->
    <section class="content">
      <div class="row">
          
		<div class="col-xs-12">
         
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List District </h3>
            </div>
            <!-- /.box-header -->
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Country</th>
                  <th>State</th>
				  <th>District</th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
				<?php 	
				$i=1;
				$getdata =$this->db->query("SELECT mstc.CountryID, mstc.CountryName,  msts.`StateID`, msts.`StateName`,  mstd.`DistrictID`, mstd.`DistrictName` FROM `mstdistrict` AS mstd 
											INNER JOIN mststate AS msts ON msts.StateID=mstd.`StateID`
											INNER JOIN mstcountry AS mstc ON mstc.CountryID=mstd.`CountryID` WHERE mstd.IsDeleted=0 
											ORDER BY mstd.DistrictID DESC")->result();
				
				foreach($getdata as $detail){
				?>
                <td><?php echo $i;?></td>
                  <td><?php echo $detail->CountryName;?></td>
                  <td><?php echo $detail->StateName;?></td>
				  <td><?php echo $detail->DistrictName;?></td>
				  <td><a href="<?php print base_url().$this->router->class.'/edit/'.$detail->DistrictID; ?>" ><span class="fa fa-fw fa-edit"> </span></a></td>
                   <td><a href="<?php print base_url().$this->router->class.'/delete/'.$detail->DistrictID; ?>" onclick="return confirm_delete()" ><span class="fa fa-fw fa-remove"> </span></a></td>
                </tr>
                <?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Country</th>
                  <th>State</th>
				   <th>District</th>
                  <th>Update</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->		
 
 
 
 
          </div>
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
	 <!-- page script -->
	 <script src="http://localhost/swnrms/theme/micro/plugins/datatables/jquery.dataTables.min.js"></script>

	 
	  <!-- page script -->
 <script type="text/javascript">
	 $(function() {
	   $("#example1").dataTable();
   });
	   
 </script>
    <!-- /.content -->
	 <!-- page script -->
      <script>
		function confirm_delete() {
			var r = confirm("Are you sure to delete record?");
			if (r == true) {
				return true;
			} else {
				return false;
			}
		}

   </script>	
