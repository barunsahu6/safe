  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
        <small>preview of  Change Password</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"> Change Password </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
          <div class="box">
           
            <!-- /.box-header -->
			
			
		 <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Change Password</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');
		
			if(!empty($tr_msg))
			{
				?>
				<div class="alert alert-success alert-dismissible"> 
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
                <?php } else if(!empty($er_msg)){?>
                <div class="alert alert-danger alert-dismissible"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>  
                <?php } ?>
			
            <?php print form_open_multipart($this->router->class.'/'.edit.'/'.$token,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
             <div class="box-body">
               <div class="form-group">
                  <label for="exampleInputPassword1">Old Password</label>
				  <input type="Password" class="form-control" id="OldPassword" name="OldPassword" class="form-control" placeholder="Enter Old Password" Required>
                </div>
				<div class="form-group">
                  <label for="exampleInputPassword1">New Password</label>
				   <?php if(form_error('form[Password]')){ ?><div class="form-group has-error"><?php } ?>
					<?php print form_input(array('type'=>'password','name'=>'form[Password]','id'=>'Password','value'=>set_value('form[Password]'),'class'=>'form-control','placeholder'=>'Enter New Password','autocomplete'=>'off','required'=>'required')); ?>
					<?php if(form_error('form[Password]')){ ?>
					<label class="control-label" for="inputError"><?php print form_error('form[Password]')?></label></div>
					<?php } ?>
				                 
                </div>
				<div class="form-group">
                  <label for="exampleInputPassword1">Confrim Password</label>
				  <input type="Password" class="form-control" id="ConfrimPassword" name="ConfrimPassword" class="form-control" placeholder="Enter Confrim Password" Required>
				  </div>
           
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="Submit"  class="btn btn-primary"  >Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
		  
			
			
          
          </div>
          <!-- /.box -->
        </div>
      </div>  
</div>	
        </div>
      </div>  
</div>  
    </section>
    <!-- /.content -->
 <script>
        function validatePassword() {
            var validator = $("#indexChangePassword").validate({
                rules: {
					OldPassword: "required",
                    NewPassword: "required",
                    ConfrimPassword: {
                        equalTo: "#NewPassword"
                    }
					
                },
                messages: {
					OldPassword: " Enter Old Password",
                    NewPassword: " Enter  New Password",
                    ConfrimPassword: " Enter Confirm Password Same as New Password"
                }
            });
            if (validator.form()) {
                alert('Sucess');
            }
        }
     
        </script>