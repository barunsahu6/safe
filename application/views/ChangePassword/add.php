<div class="left-sidebar">
<style type="text/css">
table > tbody > tr > th{
border:none !important;
}

table > tbody > tr > td{
border:none !important;
}
</style>
	<!--Data Lists-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="widget">
                <div class="widget-header">
                	<div class="title"><?php print ucfirst($this->router->method).' '.ucfirst(str_replace('_',' ',$this->router->class)); ?></div>
                	<span class="tools"><?php print form_button('goback','<i class="fa fa-arrow-circle-left"> Back to Lists</i>',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></span>
                </div>
                <div class="widget-body">
                    <div class="tab-pane" >
						<?php print form_open_multipart($this->router->class.'/'.$this->router->method,array('name'=>$this->router->method.$this->router->class,'id'=>$this->router->method.$this->router->class))?>
                        <table class="table table-striped" 
                            <tr>
                              <td colspan="2"></td>
                            </tr>
                            
                            <tr>
                              <th width="20%"> Type :</th>
                              <td width="80%">
                                <?php if(form_error('form[level_id]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_dropdown('form[level_id]',$types,set_value('form[level_id]'),'id="level_id" required="required" class="form-control"')?>
                                <?php if(form_error('form[level_id]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[level_id]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Full Name :</th>
                              <td>
                                <?php if(form_error('form[first_name]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[first_name]','id'=>'first_name','value'=>set_value('form[first_name]'),'class'=>'form-control','placeholder'=>'First Name','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[first_name]')){?>
                                <label class="control-label" for="inputError"><?php print form_error('form[first_name]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Username :</th>
                              <td>
                                <?php if(form_error('form[access_text]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('name'=>'form[access_text]','id'=>'access_text','value'=>set_value('form[access_text]'),'class'=>'form-control','placeholder'=>'Username','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[access_text]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[access_text]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
							
							
							<tr>
                              <th>Password :</th>
                              <td>
                                <?php if(form_error('form[access_key]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('type'=>'password','name'=>'form[access_key]','id'=>'access_key','value'=>set_value('form[access_key]'),'class'=>'form-control','placeholder'=>'Password','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[access_key]')){ ?>
                                <label class="control-label" for="inputError"><?php print form_error('form[access_key]')?></label></div>
                                <?php } ?>
                              </td>
                            </tr>
                            
                            <tr>
                              <th>Email Address  :</th>
                              <td>
                                <?php if(form_error('form[email]')){ ?><div class="form-group has-error"><?php } ?>
                                <?php print form_input(array('type'=>'text','name'=>'form[email]','id'=>'email','value'=>set_value('form[email]'),'class'=>'form-control','placeholder'=>'Email','autocomplete'=>'off','required'=>'required')); ?>
                                <?php if(form_error('form[email]')) {?><label class="control-label" for="inputError"><?php print form_error('form[email]')?></label></div><?php }?>
                              </td>
                            </tr>
                            
                            <tr>
                              <td><?php print form_button('goback','Cancel',array('onclick'=>'goBack()','class'=>'btn btn-default'))?></td>
                              <td><?php print form_submit($this->router->method,'Submit',array('id'=>$this->router->method,'class'=>'btn btn-info pull-right'))?></td>
                            </tr>
                          </table>
                        <?php print form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>