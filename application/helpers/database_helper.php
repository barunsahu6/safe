<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

define('FTHEME',base_url().'theme/micro/');
define('FCSS',FTHEME.'css/');
define('FJS',FTHEME.'js/');
define('FPLUGINS',FTHEME.'plugins/');
define('FIMAGES',FTHEME.'img/');
define('FTOP','Template/header');
define('NAVTOP','Template/navigation');
define('FBOTTOM','Template/footer');
define('IMAGE_MANAGER','pichub/uploads/');
define('IMAGE_MANAGER_THUMB','pichub/uploads/thumbs/');
define('PER_PAGE',10);
define('URI_SEGMENT',3);

// Table Prefix and Suffix 
define('TABLE_PRE','mst');
define('TABLE_SUF','');


// System Settings
define('USERS',TABLE_PRE.'user'.TABLE_SUF);
define('LEVEL',TABLE_PRE.'rolebkupbkup'.TABLE_SUF);
define('STATE',TABLE_PRE.'state'.TABLE_SUF);
define('COUNTRY',TABLE_PRE.'country'.TABLE_SUF);
define('DISTRICT',TABLE_PRE.'district'.TABLE_SUF);
define('STANDARD',TABLE_PRE.'standard'.TABLE_SUF);
define('PARAMETER',TABLE_PRE.'parameter'.TABLE_SUF);
define('AGGREGATOR',TABLE_PRE.'aggregator'.TABLE_SUF);
define('CONTAINERTYPE',TABLE_PRE.'containertype'.TABLE_SUF);

define('PLANSPECIFICATION',TABLE_PRE.'plantspecification'.TABLE_SUF);
define('PLANSD',TABLE_PRE.'standardparameterdetail'.TABLE_SUF);
define('AGENCY',TABLE_PRE.'agency'.TABLE_SUF);
define('PATPLANDETAIL','tblpatplantdetail'.TABLE_SUF);
define('PLANTMANUFACTURER',TABLE_PRE.'plantmanufacturer'.TABLE_SUF);
define('PLANTPUR',TABLE_PRE.'plantpurificationstep'.TABLE_SUF);
define('ASSETFUNDER',TABLE_PRE.'assetfunder'.TABLE_SUF);
define('DISTRIBUTION','tblpatplantdistribution'.TABLE_SUF);
define('WATERQTCE',TABLE_PRE.'waterqualitychallenge'.TABLE_SUF);
define('PATPOD','tblpatpodetail'.TABLE_SUF);
define('DESIG',TABLE_PRE.'designation'.TABLE_SUF);
define('PATWCS','tblpatwatercontaminants'.TABLE_SUF);
define('PATPFCTS','tblpatplantpurificationstep'.TABLE_SUF);
define('PATPPD','tblpatplantproddetail'.TABLE_SUF);
define('TPPAF','tblpatplantassetfunder'.TABLE_SUF);
define('LOCATION','tbltstlocation'.TABLE_SUF);


















//define('STATUS',TABLE_PRE.'isstatus'.TABLE_SUF);
