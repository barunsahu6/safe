<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

define('FTHEME',base_url().'theme/uveous/front/');
define('FCSS',FTHEME.'css/');
define('FJS',FTHEME.'js/');
define('FIMAGES',FTHEME.'images/');
define('FTOP','Template/header');
define('FBOTTOM','Template/footer');
define('IMAGE_MANAGER','pichub/uploads/');
define('IMAGE_MANAGER_THUMB','pichub/uploads/thumbs/');
define('PER_PAGE',50);
define('URI_SEGMENT',3);

// Table Prefix and Suffix 
define('TABLE_PRE','nurc_');
define('TABLE_SUF','');

// System Settings
define('STATUS',TABLE_PRE.'isstatus'.TABLE_SUF);
define('CLIENT',TABLE_PRE.'clients'.TABLE_SUF);

define('USERS',TABLE_PRE.'users'.TABLE_SUF);
define('TYPES',TABLE_PRE.'userlevels'.TABLE_SUF);
define('CONTACT',TABLE_PRE.'contacts'.TABLE_SUF);
define('CONTACT_EMAIL',TABLE_PRE.'contact_email_type'.TABLE_SUF);
define('CONTACT_UPD',TABLE_PRE.'contact_updates'.TABLE_SUF);
define('ETYPE',TABLE_PRE.'email_types'.TABLE_SUF);
define('UPDATES',TABLE_PRE.'updates'.TABLE_SUF);
define('AGENCY',TABLE_PRE.'news_agencies'.TABLE_SUF);
define('STUPDATES',TABLE_PRE.'story_updates'.TABLE_SUF);
define('SOURCES',TABLE_PRE.'story_sources'.TABLE_SUF);
define('SECTION',TABLE_PRE.'update_sections'.TABLE_SUF);
	
define('STORY',TABLE_PRE.'newstory'.TABLE_SUF);



define('SU_UPDATE',TABLE_PRE.'story_updates'.TABLE_SUF);

//define('SU_UPDATE',TABLE_PRE.'story_updates'.TABLE_SUF);

define('SST_CLIENT',TABLE_PRE.'story_clients'.TABLE_SUF);

define('TEMPLATES',TABLE_PRE.'update_templates'.TABLE_SUF);
define('PLAN',TABLE_PRE.'plan'.TABLE_SUF);
define('SHORTCUT',TABLE_PRE.'shortcuts'.TABLE_SUF);
define('NEWS_LETT',TABLE_PRE.'newsletters'.TABLE_SUF);
