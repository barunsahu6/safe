<?php
  class Ajaxcontroller_model extends CI_Model
 {
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		
	}
	

	
	/**
	 * Method getSections() get all section.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUpdateSections($updateid)
	{
		$this->db->select('section_id,section_name');
		$this->db->where('status_id','1');
		$this->db->where('update_id',$updateid);
		$this->db->order_by('section_id','asc');
		return $this->db->get(SECTION)->result();
	}
	
	
}