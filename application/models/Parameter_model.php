<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Parameter_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(PARAMETER);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(PARAMETER)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			//print_R($form); die;
			return ($this->db->insert(PARAMETER,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function getDetail($token)
	{
		try {
			$this->db->select('*');
			$this->db->where('ParameterID',(int)$token);
			return $this->db->get(PARAMETER)->row(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(PARAMETER)->result(); 
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			$form = $this->input->post();
			//echo "<pre";
			//print_r($form);
			//die;
			
			$this->db->where('ParameterID',(int)$token);
			return ($this->db->update(PARAMETER,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['IsDeleted']  = '1';
			$this->db->where('ParameterID',(int)$token);
			//$this->db->update(DISTRICT,$form); //echo $this->db->last_query(); die;
			return ($this->db->update(PARAMETER,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method DISTRICTName_validation() check duplicate DISTRICTName.
	 * @access	public
	 * @param	Null
	 * @return	array
	 */
	public function Name_validation()
	{
		try {
			$form  = $this->input->post('form');
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
			$user  = isset($form['ParameterID']) ? $form['ParameterID'] : '0'; //print_r($form);die;
			$user  = isset($form['Name']) ? $form['Name'] : ''; 
			
			$this->db->select("COUNT(1) AS CNT");
			$this->db->where('Name',$user);
			if($token>0) {
				$this->db->where('ParameterID!='.(int)$token);	
			}
			$row = $this->db->get(PARAMETER)->row();//echo $this->db->last_query();die;
			if ($row->CNT > 0){
				$this->form_validation->set_message('Name_validation', '{field} should be unique.');
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	
}