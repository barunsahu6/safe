<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Institutional_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	

/**
	 * Method getDashboardPlantAndAge() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getRegulartory($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = "SELECT 'GovApproved' AS label,  COUNT(`GramPanchayatApproval`) AS `Value`  
			FROM `tblpatplantdetail` INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `GramPanchayatApproval`=1 ";

			$sql2 = "SELECT  'ElectricityConnection' AS label, COUNT(`LegalElectricityConnection`) AS `Value` 
			From `tblpatplantdetail` INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `LegalElectricityConnection`=1"; 

			$sql3 = "SELECT 'LandApproval' as label, COUNT(`LandApproval`) AS `Value` 
			FROM `tblpatplantdetail` INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `LandApproval`=1 ";

			$sql4 ="SELECT 'RawWaterSource' as label, COUNT(`RawWaterSourceApproval`) AS `Value`
			FROM `tblpatplantdetail` INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `RawWaterSourceApproval`=1 "; 
			}
			else
			{
				$sql1 = "SELECT 'GovApproved' AS label,  COUNT(`GramPanchayatApproval`) AS `Value`  
			FROM `tblpatplantdetail` WHERE `GramPanchayatApproval`=1 ";

			$sql2 = "SELECT  'ElectricityConnection' AS label, COUNT(`LegalElectricityConnection`) AS `Value` 
			From `tblpatplantdetail` WHERE `LegalElectricityConnection`=1"; 

			$sql3 = "SELECT 'LandApproval' as label, COUNT(`LandApproval`) AS `Value` 
			FROM `tblpatplantdetail` WHERE `LandApproval`=1 ";

			$sql4 ="SELECT 'RawWaterSource' as label, COUNT(`RawWaterSourceApproval`) AS `Value`
			FROM `tblpatplantdetail` WHERE `RawWaterSourceApproval`=1 "; 
			}
			
			
			if($strwhr != "")
			{
				$sql1 .= " AND tblpatplantdetail.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql1 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql1 .= " AND tblpatplantdetail.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql1 .= " AND tblpatplantdetail.`PlantGUID` = '".$plantwhr."' "; 
			}
			
			
			
			if($strwhr != "")
			{
				$sql2 .= " AND tblpatplantdetail.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql2 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql2 .= " AND tblpatplantdetail.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql2 .= " AND tblpatplantdetail.`PlantGUID` = '".$plantwhr."' "; 
			}
			
			
			
			if($strwhr != "")
			{
				$sql3 .= " AND tblpatplantdetail.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql3 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql3 .= " AND tblpatplantdetail.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql3 .= " AND tblpatplantdetail.`PlantGUID` = '".$plantwhr."' "; 
			}
			
			
			if($strwhr != "")
			{
				$sql4 .= " AND tblpatplantdetail.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql4 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql4 .= " AND tblpatplantdetail.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql4 .= " AND tblpatplantdetail.`PlantGUID` = '".$plantwhr."' "; 
			}
			
			$sql = "$sql1 union $sql2 union $sql3 union $sql4 ";
			
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}



/**
	 * Method getTrainingRecieved() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getTrainingRecieved($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = " SELECT 'Electrical Safety' AS label,  COUNT(`ElectricalSafety`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`ElectricalSafety` AS `ElectricalSafety` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			 INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID WHERE ppod.`ElectricalSafety`=1 ";

			$sql2 ="SELECT 'PlantOM' AS label,  COUNT(`PlantOM`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`PlantOM` AS `PlantOM` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			 INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID WHERE ppod.`PlantOM`=1 ";

			$sql3 ="SELECT 'WaterQuality' AS label,  COUNT(`WaterQuality`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`WaterQuality` AS `WaterQuality` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			 INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID WHERE ppod.`WaterQuality`=1 ";

			$sql4 ="SELECT 'ConsumerAwareness' AS label,  COUNT(`ConsumerAwareness`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`ConsumerAwareness` AS `ConsumerAwareness` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			 INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID WHERE ppod.`ConsumerAwareness`=1 ";

			$sql5 ="SELECT 'ABKeeping' AS label,  COUNT(`ABKeeping`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`ABKeeping` AS `ABKeeping` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			 INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID WHERE ppod.`ABKeeping`=1 ";
			}
			else
			{
				$sql1 = " SELECT 'Electrical Safety' AS label,  COUNT(`ElectricalSafety`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`ElectricalSafety` AS `ElectricalSafety` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			WHERE ppod.`ElectricalSafety`=1 ";

			$sql2 ="SELECT 'PlantOM' AS label,  COUNT(`PlantOM`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`PlantOM` AS `PlantOM` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			WHERE ppod.`PlantOM`=1 ";

			$sql3 ="SELECT 'WaterQuality' AS label,  COUNT(`WaterQuality`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`WaterQuality` AS `WaterQuality` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			WHERE ppod.`WaterQuality`=1 ";

			$sql4 ="SELECT 'ConsumerAwareness' AS label,  COUNT(`ConsumerAwareness`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`ConsumerAwareness` AS `ConsumerAwareness` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			WHERE ppod.`ConsumerAwareness`=1 ";

			$sql5 ="SELECT 'ABKeeping' AS label,  COUNT(`ABKeeping`) AS `Value`  FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`ABKeeping` AS `ABKeeping` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS ppod INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=ppod.PlantGUID
			WHERE ppod.`ABKeeping`=1 ";
			}
			
			
				if($strwhr != "")
					{
						$sql1 .= " AND ppd.CountryID ='".$strwhr."' "; 
					}
					if($strstatewhr != ""){
						$sql1 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
					}
					if($districtwhr != ""){
						$sql1 .= " AND ppd.District = '".$districtwhr."' "; 
					}
					if($plantwhr != ""){
						$sql1 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
					}
	
			
			
			if($strwhr != "")
			{
				$sql2 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql2 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql2 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql2 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
			}

			
			if($strwhr != "")
			{
				$sql3 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql3 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql3 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql3 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
			}

			
			
			
			if($strwhr != "")
			{
				$sql4 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql4 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql4 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql4 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
			}
			
			
			
			if($strwhr != "")
			{
				$sql5 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql5 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql5 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql5 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
			}
			

			$sql = "$sql1 union $sql2 union $sql3 union $sql4 union $sql5 ";
			//echo $sql; //die;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}





/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->where('IsDeleted','0');
				$this->db->where('b.CountryID','1');
				$this->db->group_by('b.CountryID');
				$this->db->order_by('b.CountryID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			}

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('c.StateID,c.StateName');
				$this->db->from('mststate c');
				$this->db->where('c.CountryID',$id);
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateName','asc');
				
				return $this->db->get()->result();
			}
			else
			{
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}