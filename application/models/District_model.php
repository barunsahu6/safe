<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class District_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(DISTRICT);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(DISTRICT)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			//print_R($form); die;
			return ($this->db->insert(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function getDetail($token)
	{
		try {
			$this->db->select('*');
			$this->db->where('DistrictID',(int)$token);
			return $this->db->get(DISTRICT)->row(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(DISTRICT)->result(); 
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			$form = $this->input->post('form');
			//print_r($form);die;
			
			$this->db->where('DistrictID',(int)$token);
			return ($this->db->update(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['IsDeleted']  = '1';
			$this->db->where('DistrictID',(int)$token);
			//$this->db->update(DISTRICT,$form); //echo $this->db->last_query(); die;
			return ($this->db->update(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method name_validation() check duplicate name.
	 * @access	public
	 * @param	Null
	 * @return	array
	 */
	public function name_validation()
	{
		try {
			$form = $this->input->post('form');
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
			$user = isset($form['user_id']) ? $form['user_id'] : '0'; //print_r($form);die;
			$email = isset($form['email']) ? $form['email'] : ''; 
			
			$this->db->select("COUNT(1) AS CNT");
			$this->db->where('email',$email);
			if($token>0) {
				$this->db->where('user_id!='.(int)$token);	
			}
			$row = $this->db->get(USERS)->row();//print_r($row->CNT);die;//echo $this->db->last_query();die;
			if ($row->CNT > 0){
				$this->form_validation->set_message('name_validation', '{field} should be unique.');
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method DISTRICTName_validation() check duplicate DISTRICTName.
	 * @access	public
	 * @param	Null
	 * @return	array
	 */
	public function DistrictName_validation()
	{
		try {
			$form  = $this->input->post('form');
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
			$user  = isset($form['DistrictID']) ? $form['DistrictID'] : '0'; //print_r($form);die;
			$user  = isset($form['DistrictName']) ? $form['DistrictName'] : ''; 
			
			$this->db->select("COUNT(1) AS CNT");
			$this->db->where('DistrictName',$user);
			if($token>0) {
				$this->db->where('DistrictID!='.(int)$token);	
			}
			$row = $this->db->get(DISTRICT)->row();//echo $this->db->last_query();die;
			if ($row->CNT > 0){
				$this->form_validation->set_message('DistrictName_validation', '{field} should be unique.');
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		$this->db->select('CountryID,CountryName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('CountryID','asc');
		return $this->db->get(COUNTRY)->result();
	}
	
	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getState()
	{
		$this->db->select('StateID,StateName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('StateID','asc');
		return $this->db->get(STATE)->result();
	}
}