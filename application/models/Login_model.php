<?php
class Login_model extends CI_Model
{
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
	}
	
	public function removeAction(){
		$where = $this->input->post();
		array_pop($where);			
		return $where;
	}
	
	/**
	 * Method authenticate()check user authentication.
	 * @access	public
	 * @param	null
	 * @return	string.
	 */
	public function authenticate()
	{
		$this->db->select('*');
		$this->db->where('UserName',$this->input->post('form[username]'));
		$this->db->where('Password',$this->input->post('form[password]'));
		$data  =  $this->db->get('mstuser')->row();
	
		if(count($data) > 0) {
					
			$datasession = $this->GetData($data->UserID);/// Get CountryID 
			
			$this->session->set_userdata("login_data",	array(
				'USERID' 	=> $data->UserID,
				'ROLE_ID' 	=> $data->RoleID,
				'NAME'		=> $data->FirstName.' '.$data->LastName,
				'USERTYPE'	=> $data->RoleID,
				'STATEID'	=> $data->StateID,
				'COUNTRYID'	=> $datasession->CountryID
				)
			);
			return 1;
		}
		else {
			return -1;
		}
	}


	public function GetData($userid)
	{

		try{
			$this->db->select('tbltstlocation.CountryID,tbltstlocation.StateID,tbltstlocation.DistrictID');
			$this->db->from('mstuser');
			$this->db->join('tbltstlocation ','tbltstlocation.UserID =mstuser.UserID');
			$this->db->where('mstuser.UserID', $userid);
			return $this->db->get()->row(); //echo $this->db->last_query(); die;
		
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/*public function logout()
   {
    $user_data = $this->session->all_userdata();
     foreach ($user_data as $key => $value) {
      if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
       $this->session->unset_userdata($key);
      }
     }
    $this->session->sess_destroy();
    redirect('login');
}*/
}