<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	public $plantguid = "";
	public $plantpoguid = "";
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
	}
	
	


	public function getCountStates($country_id)
	{
		$sql = "select * from mststate where CountryID = ".$country_id;
		$res = $this->Common_model->query_data($sql);

		$options_state = '<option value="">All</option>';
		foreach($res as $state)
		{
			$options_state .= '<option value="'.$state->StateID.'">'.$state->StateName.'</option>';
		}

		echo $options_state;
	}

	public function getDistricts($country_id, $state_id)
	{
		$sql = "select * from mstdistrict where CountryID = ".$country_id." and StateID = ".$state_id;
		$res = $this->Common_model->query_data($sql);

		$options_district = '<option value="">All</option>';
		foreach($res as $district)
		{
			$options_district .= '<option value="'.$district->DistrictID.'">'.$district->DistrictName.'</option>';
		}

		echo $options_district;
	}
}
