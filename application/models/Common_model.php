<?php 
class Common_Model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->file_path = realpath(APPPATH . '../datafiles');
		$this->banner_path = realpath(APPPATH . '../banners');
		$this->gallery_path_url = base_url().'datafiles/';
	}

	public function insert_data($table,$data1)
	{   
		$this->db->insert($table,$data1);
		//echo $this->db->last_query();die;
	}	
	
	public function update_data($table,$data1,$col,$val)
	{   
		
		$this->db->where($col, $val);
		$this->db->update($table,$data1);
		//echo $this->db->last_query();die;
	}	
	
	
	public function delete_row($table,$col,$val)
	{   
		
		$this->db->where($col, $val);
		$this->db->delete($table);
	}			

	
	public function exist_data($table,$compare)
	{   
		try{
			 $query = $this->db->get_where($table, $compare);
 
		     $count = $query->num_rows(); //echo $this->db->last_query(); die;//counting result from query
			return $count;
		}
		catch(Exception $e){
			print_r($e->getMessage());die;
		}

      }

      public function query_data($sql){
      	$query = $this->db->query($sql);
      	return $query->result();

      }

      public function query_data_array($sql){
      	$query = $this->db->query($sql);
      	return $query->result_array();
      }
 
      public function get_data($table,$fld = "*",$col=NULL,$val=NULL){
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}		
      	$query = $this->db->get($table);
		//echo $this->db->last_query();die;
      	$row  = $query->result();
      	return $row;
      }
	  
	  public function get_data_notDeleted($table,$fld = "*",$col=NULL,$val=NULL,$col1=NULL,$val1=NULL){
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}
		if($col1 != NULL){ 
      		$this->db->where($col1,$val1); 
      	}		
      	$query = $this->db->get($table);
		//echo $this->db->last_query();die;
      	$row  = $query->result();
      	return $row;
      }
	  
	  public function get_data_condition($table,$fld = "*",$col=NULL,$val=NULL,$col1=NULL,$val1=NULL,$col2=NULL,$val2=NULL){
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}	
		if($col1 != NULL){ 
      		$this->db->where($col1,$val1); 
      	}		
		if($col2 != NULL){ 
      		$this->db->where($col2,$val2); 
      	}
      	$query = $this->db->get($table);
		//echo $this->db->last_query();die;
      	$row  = $query->result();
      	return $row;
      }
     /////////////////////////////// Statr Count User With Where Condation /////////////////////////////
	  public function get_count_user($table,$fld = "*",$col=NULL,$val=NULL,$col1=NULL,$val1=NULL){
      	$this->db->select($fld);
      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}	
		if($col1 != NULL){ 
      		$this->db->where($col1,$val1); 
      	}
				
      	$query = $this->db->get($table);
		//echo $this->db->last_query();die;
      	$row  = $query->num_rows();
		return $row;
      }
	  ////////////////////// End Here /////////////////////////////////////////
	  
	  /////////////////////////////// List User With Current Date /////////////////////////////
	  public function get_list_user($table,$fld = "*",$col=NULL,$val=NULL,$col1=NULL,$val1=NULL,$col2=NULL,$val2=NULL){
      	$this->db->select($fld);
      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}	
		if($col1 != NULL){ 
      		$this->db->where($col1,$val1); 
      	}
		if($col2 != NULL){ 
      		$this->db->where($col2,$val2); 
      	}	
		$this->db->limit(4, 0);		
      	$query = $this->db->get($table);
		
		//echo $this->db->last_query();die;
      	$row  = $query->result();
		return $row;
      }
	 ////////////////////////////////////// End Here //////////////////////////////////// 
	  
	  
      public function get_data_multiple_where($table,$fld = "*",$where){
      	$this->db->select($fld);
      	$this->db->where($where); 
      	$query = $this->db->get($table);
				echo $this->db->last_query();die;
      	$row  = $query->result();
      	return $row;
		
      }

      public function get_data_array($table,$fld = "*",$col=NULL,$val=NULL){
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}
      	$query = $this->db->get($table);
      	$row  = $query->result_array();
      	return $row;
      }

      public function get_data_like($table,$fld = "*",$col=NULL,$val=NULL){
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->like($col,$val,'both'); 
      	}		
      	$query = $this->db->get($table);
		//echo $this->db->last_query();die;
      	$row  = $query->result();
      	return $row;
      }	

      function do_upload($new_name = null) {

		//$this->load->helper(array('form', 'url'));

      	$config = array(
      		'allowed_types' => 'png|jpeg|jpg',
      		'upload_path' => $this->file_path,
      		'max_size' => 20000,
      		'file_name' => $new_name
      		);

      	$this->load->library('upload', $config);
      	$this->upload->do_upload();
      	$file_data = $this->upload->data();


      	return $file_data;
      }
//


      function do_upload_banner() {
		//$this->load->helper(array('form', 'url'));
      	$config = array(
      		'allowed_types' => 'png|jpeg|jpg',
      		'upload_path' => $this->banner_path,
      		'max_size' => 200000
      		);

      	$this->load->library('upload', $config);
      	$this->upload->do_upload();
      	$file_data = $this->upload->data();


      	return $file_data;
      }


      function do_upload_thumbnail()
      {
		// Get configuartion data (we fill up 2 arrays - $config and $conf)

      	$config['img_path']			= $this->config->item('img_path');
      	$config['allow_resize']		= $this->config->item('allow_resize');

      	$config['allowed_types']	= $this->config->item('allowed_types');
      	$config['max_size']			= $this->config->item('max_size');
      	$config['encrypt_name']		= $this->config->item('encrypt_name');
      	$config['overwrite']		= $this->config->item('overwrite');
      	$config['upload_path']		= $this->config->item('upload_path');

		// Load uploader
      	$this->load->library('upload', $config);

		if ($this->upload->do_upload()) // Success
		{
			// General result data
			$result = $this->upload->data();

			$config['max_width']	= $this->config->item('max_width');
			$config['max_height']	= $this->config->item('max_height');
			

			if ($config['allow_resize'] and $config['max_width'] > 0 and $config['max_height'] > 0 and (($result['image_width'] > $config['max_width']) or ($result['image_height'] > $config['max_height'])))
			{			
				// Resizing parameters
				$resizeParams = array
				(
					'source_image'	=> $result['full_path'],
					'new_image'		=> $result['full_path'],
					'width'			=> $config['max_width'],
					'height'		=> $config['max_height']
					);
				
				// Load resize library
				$this->load->library('image_lib', $resizeParams);
				
				// Do resize
				if ( ! $this->image_lib->resize())
				{
					echo "errors in resize";
					echo $this->image_lib->display_errors();
				}
			}
			
			// Add our stuff
			$result['result']		= "file_uploaded";
			$result['resultcode']	= 'ok';
			$result['file_name']	= $config['img_path'] . '/' . $result['file_name'];
		}
		else // Failure
		{
			// Compile data for output
			$result['result']		= $this->upload->display_errors(' ', ' ');
			$result['resultcode']	= 'failure';
		}
		return $result;
	}
	


	public function get_language_phrase($key = null){

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == ''){
			$languageId = 1;
		}

		$query = "select * from mstlanguage 
		where LanguageId =	$languageId 
		and Lang_Key = '".trim($key)."'";
		// echo $query; die();
		$langRow = $this->Common_Model->query_data($query);
		if(count($langRow) < 1){
			return '';
		}else{
			return $langRow[0]->Value;
		}
	}

	public function get_indicator_heading_phrase($headingId=null)
	{

		$query 	=	 "select * from tblmstindicatorheading where HeadingID	=	$headingId";
		$headingDetails	=	$this->Common_Model->query_data($query);
		if(count($headingDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == ''){
			$languageId = 1;
		}

		if($languageId == 1) return $headingDetails[0]->HeadingName;
		else if($languageId == 2) return $headingDetails[0]->HeadingNameTamil;

	}

	public function get_indicator_phrase($indicatorId=null)
	{
		$query = "select * from tblmstindicators where indicatorID	=	$indicatorId";
		$indicatorDetails	=	$this->Common_Model->query_data($query);

		if(count($indicatorDetails) <1) return '';
		
		$loginData	=	$this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == ''){
			$languageId = 1;
		}

		if($languageId == 1) return $indicatorDetails[0]->IndicatorName;
		else if($languageId == 2) return $indicatorDetails[0]->IndicatorNameTamil;
	}

	public function get_graph_phrase($id=null)
	{
		$query 	=	 "select * from tblmstgraphs where Id	=	$id";
		$graphDetails	=	$this->Common_Model->query_data($query);

		if(count($graphDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == '')
		{
			$languageId = 1;
		}
		if($languageId == 1) return $graphDetails[0]->GraphName;
		else if($languageId == 2) return $graphDetails[0]->GraphNameTamil;
	}

	public function get_subheading_phrase($indicatorID=null)
	{
		$query =	"select * from tblmstindicators where indicatorID	=	$indicatorID";
		$subheadingDetails	=	$this->Common_Model->query_data($query);

		if(count($subheadingDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == '')
		{
			$languageId = 1;
		}
		if($languageId == 1) return $subheadingDetails[0]->SubHeading;
		else if($languageId == 2) return $subheadingDetails[0]->SubHeadingTamil;
	}

	public function get_graph_heading_phrase($headingId=null)
	{
		$query =	"select * from tblmstgraphgroup where GroupId	=	$headingId";
		$subheadingDetails	=	$this->Common_Model->query_data($query);

		if(count($subheadingDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == '')
		{
			$languageId = 1;
		}
		if($languageId == 1) return $subheadingDetails[0]->Name;
		else if($languageId == 2) return $subheadingDetails[0]->NameTamil;
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountCountry()
	{
		try {
            $syscountryid = $_SESSION['login_data']['COUNTRYID'];//die;
			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('b.CountryID',$syscountryid);
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();// echo $this->db->last_query(); die; 
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountStates($strwhr)
	{
		try {
			 $syscountryid = $_SESSION['login_data']['COUNTRYID'];
			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			if(!empty($strwhr)){
			$this->db->where('b.CountryID',$syscountryid);
			}
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountDistricts($strstatewhr)
	{
		try {
			
			$syscountryid = $_SESSION['login_data']['COUNTRYID'];
		    $this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
			$this->db->where('b.CountryID',$syscountryid);
			if(!empty($strstatewhr)){
			$this->db->where('m.StateID',$strstatewhr);
			}
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictID','asc');
		 return $this->db->get()->result();   //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function getCountDistricts1()
	{
		try {
			$syscountryid = $_SESSION['login_data']['COUNTRYID'];
		    $this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
			$this->db->where('b.CountryID',$syscountryid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictID','asc');
		 return $this->db->get()->result();  // echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}





	public function send_email($subject, $body, $to_email= null, $to_name=null, $cc)
	{

		//load SMTP details
		// $loginDetails = $this->Common_model->get_data('settings');

		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		// date_default_timezone_set('Etc/UTC');
		date_default_timezone_set('Asia/Calcutta');

		//Create a new PHPMailer instance
		$mail = new Phpmailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();
		  // telling the class to use SMTP
           $mail->SMTPDebug = 0;

		$mail->CharSet = "UTF-8";


		$mail->SMTPAuth = true; // authentication enabled
		$mail->SMTPSecure = 'ssl'; 


		//Set the hostname of the mail server
		//$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
		$mail->Host = 'smtp.gmail.com';

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = 465;

		//Whether to use SMTP authentication
		// $mail->SMTPAuth = TRUE;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = 'microwareconsulting@gmail.com';

		//Password to use for SMTP authentication
		$mail->Password = 'Dev@#1234';

		//Set who the message is to be sent from
		$mail->setFrom('barunkumarsahun@gmail.com', 'Safe Water Network Admin');

		$mail->addAddress($to_email, $to_name);

		//Set the subject line
		$mail->Subject = $subject;

		$content['email_body'] = $body;
		$body = $this->load->view('email/email_template', $content, TRUE);
		$mail->msgHTML($body);

		//Attach an image file
		/*if(count($attachments)>0){
			foreach($attachments as $attachment){
				if(file_exists(FCPATH  . 'datafiles/' . $attachment)){
					$mail->addAttachment(FCPATH  . 'datafiles/' . $attachment);
				}else{
					return "ERROR: attachment file ". FCPATH  . 'datafiles/' . $attachment . " does not exists";
				}
			}
		}
*/
		// add CC 
				foreach($cc as $email){
					$mail->addCC($email);
				}

		//send the message, check for errors
				//print_r($mail->send());
				//print_r($mail->ErrorInfo);exit();
		if (!$mail->send()) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "SUCCESS: Message sent!";
		}
	}
	

	
}
