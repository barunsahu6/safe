<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Environmental_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
/**
	 * Method getDashboardPlantAndAge() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getEnvirmentalWaterReject($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			 // $sql = "SELECT (SUM(ppod.`WaterProductionDaily`)) as WaterRejected, 
				// 	(SUM(ppod.`WUtilisation`)+Sum(ppod.`UtilizationOther1`)+Sum(ppod.`UtilizationOther2`)+SUM(ppod.`UtilizationOther3`)+SUM(ppod.`UtilizationOther4`)) as RejectWaterUtilisation FROM 
				// 	(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
				// 	AS `PlantGUID`,
				// 	max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
				// 	`tblpatpodetail`.`WaterProductionDaily` AS `WaterProductionDaily`,
				// 	`tblpatpodetail`.`WUtilisation` AS `WUtilisation`,
				// 	`tblpatpodetail`.`UtilizationOther1` AS `UtilizationOther1`,
				// 	`tblpatpodetail`.`UtilizationOther2` AS `UtilizationOther2`,
				// 	`tblpatpodetail`.`UtilizationOther3` AS `UtilizationOther3`,
				// 	`tblpatpodetail`.`UtilizationOther4` AS `UtilizationOther4`
				// 	from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as ppod
				// 	INNER JOIN  tblpatplantdetail as ppd ON ppd.PlantGUID=ppod.PlantGUID WHERE 1=1 ";

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql = "SELECT (SUM(ppod.`WaterProductionDaily`)) as WaterRejected, 
					(Sum(ppod.`UtilizationOther1`)+Sum(ppod.`UtilizationOther2`)+SUM(ppod.`UtilizationOther3`)+SUM(ppod.`UtilizationOther4`)) as RejectWaterUtilisation FROM 
					(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`WaterProductionDaily` AS `WaterProductionDaily`,
					`tblpatpodetail`.`WUtilisation` AS `WUtilisation`,
					`tblpatpodetail`.`UtilizationOther1` AS `UtilizationOther1`,
					`tblpatpodetail`.`UtilizationOther2` AS `UtilizationOther2`,
					`tblpatpodetail`.`UtilizationOther3` AS `UtilizationOther3`,
					`tblpatpodetail`.`UtilizationOther4` AS `UtilizationOther4`
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as ppod
					INNER JOIN  tblpatplantdetail as ppd ON ppd.PlantGUID=ppod.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE 1=1 ";
			}

			else
			{
				$sql = "SELECT (SUM(ppod.`WaterProductionDaily`)) as WaterRejected, 
					(Sum(ppod.`UtilizationOther1`)+Sum(ppod.`UtilizationOther2`)+SUM(ppod.`UtilizationOther3`)+SUM(ppod.`UtilizationOther4`)) as RejectWaterUtilisation FROM 
					(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`WaterProductionDaily` AS `WaterProductionDaily`,
					`tblpatpodetail`.`WUtilisation` AS `WUtilisation`,
					`tblpatpodetail`.`UtilizationOther1` AS `UtilizationOther1`,
					`tblpatpodetail`.`UtilizationOther2` AS `UtilizationOther2`,
					`tblpatpodetail`.`UtilizationOther3` AS `UtilizationOther3`,
					`tblpatpodetail`.`UtilizationOther4` AS `UtilizationOther4`
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as ppod
					INNER JOIN  tblpatplantdetail as ppd ON ppd.PlantGUID=ppod.PlantGUID WHERE 1=1 ";
			}

			 

					if($strwhr != "")
					{
						$sql .= " AND ppd.CountryID ='".$strwhr."' "; 
					}
					if($strstatewhr != ""){
						$sql .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
					}
					if($districtwhr != ""){
						$sql .= " AND ppd.District = '".$districtwhr."' "; 
					}
					if($plantwhr != ""){
						$sql .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
					}
			// echo $sql; die();
			$data = $this->db->query($sql)->result()[0];
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}






//////////////////////// Get Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChartRejectWater($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql  = "SELECT ppod.PlantGUID, IFNULL((SUM(ppod.`WaterProductionDaily`) /(SUM(ppod.`UtilizationOther1`) + SUM(ppod.`UtilizationOther2`) + SUM(ppod.`UtilizationOther3`) + SUM(ppod.`UtilizationOther4`))) * 100,0) AS Utilizewater 
						FROM 
						(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`WaterProductionDaily` AS `WaterProductionDaily`,
					`tblpatpodetail`.`UtilizationOther1` AS `UtilizationOther1`,
					`tblpatpodetail`.`UtilizationOther2` AS `UtilizationOther2`,
					`tblpatpodetail`.`UtilizationOther3` AS `UtilizationOther3`,
					`tblpatpodetail`.`UtilizationOther4` AS `UtilizationOther4`
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) AS ppod 
						INNER JOIN `tblpatplantdetail` AS ppd ON ppd.PlantGUID= ppod.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  
						WHERE 1=1  ";
			}

			else
			{
				$sql  = "SELECT ppod.PlantGUID, IFNULL((SUM(ppod.`WaterProductionDaily`) /(SUM(ppod.`UtilizationOther1`) + SUM(ppod.`UtilizationOther2`) + SUM(ppod.`UtilizationOther3`) + SUM(ppod.`UtilizationOther4`))) * 100,0) AS Utilizewater 
						FROM 
						(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`WaterProductionDaily` AS `WaterProductionDaily`,
					`tblpatpodetail`.`UtilizationOther1` AS `UtilizationOther1`,
					`tblpatpodetail`.`UtilizationOther2` AS `UtilizationOther2`,
					`tblpatpodetail`.`UtilizationOther3` AS `UtilizationOther3`,
					`tblpatpodetail`.`UtilizationOther4` AS `UtilizationOther4`
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) AS ppod 
						INNER JOIN `tblpatplantdetail` AS ppd ON ppd.PlantGUID= ppod.PlantGUID  
						WHERE 1=1  ";
			}

						if($strwhr != "")
						{
							$sql .= " AND ppd.CountryID ='".$strwhr."' "; 
						}
						if($strstatewhr != ""){
							$sql .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
						}
						if($districtwhr != ""){
							$sql .= " AND ppd.District = '".$districtwhr."' "; 
						}
						if($plantwhr != ""){
							$sql .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
						}
						$sql .= " group by PlantGUID ";


				$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->where('IsDeleted','0');
				$this->db->where('b.CountryID','1');
				$this->db->group_by('b.CountryID');
				$this->db->order_by('b.CountryID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			}

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('c.StateID,c.StateName');
				$this->db->from('mststate c');
				$this->db->where('c.CountryID',$id);
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateName','asc');
				
				return $this->db->get()->result();
			}
			else
			{
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}