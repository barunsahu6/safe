<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Social_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$strwhr = $this->session->userdata("Country");
		$strstatewhr = $this->session->userdata("State");
		$districtwhr = $this->session->userdata("District");
		$plantwhr = $this->session->userdata("Plant");
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	

/**
	 * Method getDashboardPlantAndAge() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardPlantAndAge($strwhr=NULL,$strstatewhr=NULL,$districtwhr=NULL,$plantwhr=NULL){
	
		try{
			//echo $plantwhr;
			//echo "dsfds"; die;
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = "SELECT SUM(NoOfHousehold) AS Household,
							 	SUM(NoOfhhregistered) AS registered,
								(SUM(NoOfhhregistered) + SUM(NoOfhhregistered)) as total,
								ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
								ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
							FROM
								tblpatplantdetail
INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID
							WHERE AgeOfPlant < 1 ";

				$sql2=	"SELECT
									SUM(NoOfHousehold) AS Household,
									SUM(NoOfhhregistered) AS registered,
									(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
									ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
									ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
								FROM
									tblpatplantdetail
INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID
								WHERE
									AgeOfPlant between 1 and 3 ";

				$sql3 =	"SELECT
									SUM(NoOfHousehold) AS Household,
									SUM(NoOfhhregistered) AS registered,
									(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
									ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
									ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
								FROM
									tblpatplantdetail
INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID
								WHERE
									AgeOfPlant between 3 and 5 ";

				$sql4 = "SELECT
										SUM(NoOfHousehold) AS Household,
										SUM(NoOfhhregistered) AS registered,
										(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
										ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
										ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
									FROM
										tblpatplantdetail
INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID
									WHERE
										AgeOfPlant > 5 ";

			}
			else
			{

				$sql1 = "SELECT SUM(NoOfHousehold) AS Household,
							 	SUM(NoOfhhregistered) AS registered,
								(SUM(NoOfhhregistered) + SUM(NoOfhhregistered)) as total,
								ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
								ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
							FROM
								tblpatplantdetail
							WHERE AgeOfPlant < 1 ";


					$sql2=	"SELECT
									SUM(NoOfHousehold) AS Household,
									SUM(NoOfhhregistered) AS registered,
									(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
									ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
									ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
								FROM
									tblpatplantdetail
								WHERE
									AgeOfPlant between 1 and 3 ";


								$sql3 =	"SELECT
									SUM(NoOfHousehold) AS Household,
									SUM(NoOfhhregistered) AS registered,
									(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
									ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
									ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
								FROM
									tblpatplantdetail
								WHERE
									AgeOfPlant between 3 and 5 ";

								$sql4 = "SELECT
										SUM(NoOfHousehold) AS Household,
										SUM(NoOfhhregistered) AS registered,
										(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
										ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
										ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
									FROM
										tblpatplantdetail
									WHERE
										AgeOfPlant > 5 ";
			}

							
							if($strwhr != ""){
								$sql1 .= " AND tblpatplantdetail.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql1 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql1 .= " AND tblpatplantdetail.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql1 .= " AND tblpatplantdetail.PlantGUID = '".$plantwhr."' "; 
							}
							

					
									
									if($strwhr != ""){
										$sql2 .= " AND tblpatplantdetail.CountryID ='".$strwhr."' "; 
									}
									if($strstatewhr != ""){
										$sql2 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
									}
									if($districtwhr != ""){
										$sql2 .= " AND tblpatplantdetail.District = '".$districtwhr."' "; 
									}
									if($plantwhr != ""){
										$sql2 .= " AND tblpatplantdetail.PlantGUID = '".$plantwhr."' "; 
									}
			
									
									
									if($strwhr != ""){
										$sql3 .= " AND tblpatplantdetail.CountryID ='".$strwhr."' "; 
									}
									if($strstatewhr != ""){
										$sql3 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
									}
									if($districtwhr != ""){
										$sql3 .= " AND tblpatplantdetail.District = '".$districtwhr."' "; 
									}
									if($plantwhr != ""){
										$sql3 .= " AND tblpatplantdetail.PlantGUID = '".$plantwhr."' "; 
									}
									
									
										
										if($strwhr != ""){
											$sql4 .= " AND tblpatplantdetail.CountryID ='".$strwhr."' "; 
										}
										if($strstatewhr != ""){
										$sql4 .= " AND tblpatplantdetail.`StateID` = '".$strstatewhr."' "; 
										}
										if($districtwhr != ""){
											$sql4 .= " AND tblpatplantdetail.District = '".$districtwhr."' "; 
										}
										if($plantwhr != ""){
											$sql4 .= " AND tblpatplantdetail.PlantGUID = '".$plantwhr."' "; 
										}
			

			

			$sql = "$sql1 
			union 
			$sql2 
			union 
			$sql3 
			union 
			$sql4";

			//die($sql);
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}



/**
	 * Method getDashboardRuralAffordability() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardRuralAffordability($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{ 
		$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = " select '<=0.1' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  where ppd.LocationType=2 AND pppd.`Price` <= 0.1";

			$sql2 = "select '>0.1 and <=0.25' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  where ppd.LocationType=2 AND  pppd.`Price` BETWEEN 0.1 AND 0.25 ";

			$sql3 = "select '> 0.25' as label, count(*) as VALUE from tblpatplantproddetail AS pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  where ppd.LocationType=2 AND  pppd.`Price` > 0.25 ";
			}
			else
			{
				$sql1 = " select '<=0.1' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=2 AND pppd.`Price` <= 0.1";

			$sql2 = "select '>0.1 and <=0.25' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=2 AND  pppd.`Price` BETWEEN 0.1 AND 0.25 ";

			$sql3 = "select '> 0.25' as label, count(*) as VALUE from tblpatplantproddetail AS pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=2 AND  pppd.`Price` > 0.25 ";
			}

			
			if($strwhr != ""){
				$sql1 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql1 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql1 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql1 .= " AND pppd.PlantGUID = '".$plantwhr."' "; 
			}

			
			
			if($strwhr != ""){
				$sql2 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql2 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql2 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql2 .= " AND pppd.PlantGUID = '".$plantwhr."' "; 
			}
			
			
			if($strwhr != ""){
				$sql3 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql3 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql3 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql3 .= " AND pppd.PlantGUID = '".$plantwhr."' "; 
			}

			$sql = "$sql1 
			union 
			$sql2 
			union 
			$sql3";
			//echo $sql;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


/**
	 * Method getDashboardUrbanAffordability() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardUrbanAffordability($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = " select '<=0.25' as label, count(*) as VALUE from tblpatplantproddetail as pppd
				INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  where ppd.LocationType=1 AND pppd.`Price` <= 0.25 ";

				$sql2 = "select '>0.25 and <=0.5' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  where ppd.LocationType=1 AND  pppd.`Price` BETWEEN 0.25 AND 0.5 ";

			$sql3 = "select '> 0.5' as label, count(*) as VALUE from tblpatplantproddetail AS pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  where ppd.LocationType=1 AND  pppd.`Price` > 0.5 ";
			}
			else
			{
				$sql1 = " select '<=0.25' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=1 AND pppd.`Price` <= 0.25 ";

			$sql2 = "select '>0.25 and <=0.5' as label, count(*) as VALUE from tblpatplantproddetail as pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=1 AND  pppd.`Price` BETWEEN 0.25 AND 0.5 ";

			$sql3 = "select '> 0.5' as label, count(*) as VALUE from tblpatplantproddetail AS pppd
			INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=1 AND  pppd.`Price` > 0.5 ";
			}



			if($strwhr != ""){
				$sql1 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql1 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql1 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql1 .= " AND pppd.PlantGUID = '".$plantwhr."' "; 
			}

			
			
			if($strwhr != ""){
				$sql2 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql2 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql2 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql2 .= " AND pppd.PlantGUID = '".$plantwhr."' "; 
			}
			
			
			if($strwhr != ""){
				$sql3 .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql3 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql3 .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql3 .= " AND pppd.PlantGUID = '".$plantwhr."' "; 
			}

			$sql = "$sql1 
					union
					$sql2
					union
					$sql3";
			//echo $sql;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

  
	//////////////////////// Get Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChart($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{


		try{
			$strwhr      = $this->session->userdata("Country");
			$strstatewhr = $this->session->userdata("State");
			$districtwhr = $this->session->userdata("District");
			$plantwhr    = $this->session->userdata("Plant");

			$login_data = $this->session->userdata('login_data');

			if($login_data['ROLE_ID'] == 9)
			{
				$sql  = "SELECT
						    SUM(NoOfHousehold) AS TotalHouseholds,
						    SUM(NoOfhhregistered) AS reg,
						    (
						        SUM(NoOfhhregistered) + SUM(NoOfhhregistered)
						    ) AS total,
						    (
						        SUM(NoOfHousehold) /(
						            SUM(NoOfHousehold) + SUM(NoOfhhregistered)
						        )
						    ) * 100 AS perc,
						    (
						        SUM(NoOfhhregistered) /(
						            SUM(NoOfHousehold) + SUM(NoOfhhregistered)
						        )
						    ) * 100 AS perc2
						FROM
						    `tblpatplantdetail`
						INNER JOIN honeywell_patplantdetail ON tblpatplantdetail.PlantGUID = honeywell_patplantdetail.PlantGUID
						WHERE
						    1 = 1 ";
			}
			else
			{
				
				$sql  = "SELECT
				SUM(NoOfHousehold) AS TotalHouseholds,
				SUM(NoOfhhregistered) AS reg,
				(
					SUM(NoOfhhregistered) + SUM(NoOfhhregistered)
				) AS total,
				(
					SUM(NoOfHousehold) /(
						SUM(NoOfHousehold) + SUM(NoOfhhregistered)
					)
				) * 100 AS perc,
				
				(
					SUM(NoOfhhregistered) /(
						SUM(NoOfHousehold) + SUM(NoOfhhregistered)
					)
				) * 100 AS perc2
				
			FROM
				`tblpatplantdetail` WHERE 1=1  "; //die; 
			}



				if(!empty($strwhr)){
				   $sql .= " AND tblpatplantdetail.CountryID = '".$strwhr."'";
				}
				if(!empty($strstatewhr)){
					$sql .= " AND tblpatplantdetail.StateID = '".$strstatewhr."'";
				}
				if(!empty($districtwhr)){
					$sql .= " AND tblpatplantdetail.District = '".$districtwhr."'";
				}
				if(!empty($plantwhr)){
					$sql .= " AND tblpatplantdetail.PlantGUID = '".$plantwhr."'";
				}
				$sql .= " ORDER BY tblpatplantdetail.`PlantUID` ASC";

				//echo $sql;
			$data = $this->db->query($sql)->result()[0]; //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}


	/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->where('IsDeleted','0');
				$this->db->where('b.CountryID','1');
				$this->db->group_by('b.CountryID');
				$this->db->order_by('b.CountryID','asc');
				return $this->db->get()->result();
			}
			else
			{

				$this->db->select('b.CountryID, b.CountryName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->where('IsDeleted','0');
				$this->db->group_by('b.CountryID');
				$this->db->order_by('b.CountryID','asc');
				return $this->db->get()->result();
			}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**getDashboardRuralAffordability
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			}

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('c.StateID,c.StateName');
				$this->db->from('mststate c');
				$this->db->where('c.CountryID',$id);
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateName','asc');
				
				return $this->db->get()->result();
			}
			else
			{
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}