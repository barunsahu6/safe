<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Financial_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	

/**
	 * Method getDashboardPlantAndAge() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardYearWiseFunder($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

				if(!empty($strwhr) && empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){
						$sql = "SELECT '2013' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2013) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2013) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2013) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2013) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2013) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2013
						AND `CountryID`='".$strwhr."'
						UNION
						SELECT '2014' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2014 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2014 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2014  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2014 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2014) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2014
						AND `CountryID`='".$strwhr."'
						UNION
						SELECT '2015' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2015 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2015 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2015  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2015 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2015) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2015
						AND `CountryID`='".$strwhr."'
						UNION
						SELECT '2016' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2016 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2016 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2016) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2016) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2016) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2016
						AND `CountryID`='".$strwhr."'
						UNION
						SELECT '2017' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2017 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2017) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2017) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2017) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2017) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2017
						AND `CountryID`='".$strwhr."' GROUP BY YEAR(`VisitDate`) ";
					}elseif(!empty($strwhr) && !empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){

						$sql = "SELECT '2013' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2013) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2013) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2013) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2013) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2013) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2013
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."'
						UNION
						SELECT '2014' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2014 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2014 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2014  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2014 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2014) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2014
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."'
						UNION
						SELECT '2015' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2015 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2015 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2015  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2015 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2015) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2015
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."'
						UNION
						SELECT '2016' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2016 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2016 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2016) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2016) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2016) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2016
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."'
						UNION
						SELECT '2017' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2017 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2017) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2017) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2017) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2017) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2017
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' GROUP BY YEAR(`VisitDate`) ";
			
				}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && empty($plantwhr)){
				
					$sql = "SELECT '2013' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2013) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2013) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2013) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2013) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2013) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2013
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."'
						UNION
						SELECT '2014' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2014 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2014 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2014  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2014 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2014) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2014
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."'
						UNION
						SELECT '2015' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2015 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2015 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2015  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2015 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2015) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2015
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."'
						UNION
						SELECT '2016' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2016 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2016 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2016) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2016) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2016) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2016
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."'
						UNION
						SELECT '2017' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2017 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2017) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2017) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2017) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2017) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2017
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."' GROUP BY YEAR(`VisitDate`) ";

				}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && !empty($plantwhr)){
			
			$sql = "SELECT '2013' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2013) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2013) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2013) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2013) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2013) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2013
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."' AND `PlantGUID`='".$plantwhr."'
						UNION
						SELECT '2014' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2014 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2014 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2014  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2014 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2014) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2014
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."' AND `PlantGUID`='".$plantwhr."'
						UNION
						SELECT '2015' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2015 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2015 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2015  ) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2015 ) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2015) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2015
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."' AND `PlantGUID`='".$plantwhr."'
						UNION
						SELECT '2016' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2016 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2016 ) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2016) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2016) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2016) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2016
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."' AND `PlantGUID`='".$plantwhr."'
						UNION
						SELECT '2017' AS label, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND YEAR(`VisitDate`)=2017 ) AS Government, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND YEAR(`VisitDate`)=2017) AS Grants, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND YEAR(`VisitDate`)=2017) AS SelfFunded, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND YEAR(`VisitDate`)=2017) AS SWEFranchisor, (SELECT COUNT(`MachineryOther`) FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND YEAR(`VisitDate`)=2017) AS Bankloan FROM `tblpatplantdetail` WHERE YEAR(`VisitDate`)=2017
						AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District`='".$districtwhr."' AND `PlantGUID`='".$plantwhr."' GROUP BY YEAR(`VisitDate`) ";
		}
				
			//echo $sql;
			
			$data = $this->db->query($sql)->result(); //echo last_query(); die;
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	public function getFinancialCOPEX($strwhr,$strstatewhr,$districtwhr,$plantwhr){

		
		try{
		
			if(!empty($strwhr) && empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){

				$sql = "SELECT sum(b.OPEx) as opex, sum(b.ServiceCharge) as service,  sum(b.AssestRepaymentFund) as ARF
				FROM tblpatplantproddetail a 
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`OPEx` AS `OPEx`,
					`tblpatpodetail`.`ServiceCharge` AS `ServiceCharge`,
					`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`				
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as b ON a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON b.PlantGUID = c.PlantGUID WHERE c.CountryID='".$strwhr."'";

			}elseif(!empty($strwhr) && !empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){
				
				
				$sql = "SELECT sum(b.OPEx) as opex, sum(b.ServiceCharge) as service,  sum(b.AssestRepaymentFund) as ARF
				FROM tblpatplantproddetail a 
				
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`OPEx` AS `OPEx`,
					`tblpatpodetail`.`ServiceCharge` AS `ServiceCharge`,
					`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`				
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as b ON a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON b.PlantGUID = c.PlantGUID WHERE c.CountryID='".$strwhr."' AND c.StateID='".$strstatewhr."'";

			}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && empty($plantwhr)){

				$sql = "SELECT sum(b.OPEx) as opex, sum(b.ServiceCharge) as service,  sum(b.AssestRepaymentFund) as ARF
				FROM tblpatplantproddetail a 
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`OPEx` AS `OPEx`,
					`tblpatpodetail`.`ServiceCharge` AS `ServiceCharge`,
					`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`				
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as				
				b ON a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON b.PlantGUID = c.PlantGUID WHERE c.CountryID='".$strwhr."' AND c.StateID='".$strstatewhr."' AND c.District = '".$districtwhr."' ";

			}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && !empty($plantwhr)){
				
				$sql = "SELECT sum(b.OPEx) as opex, sum(b.ServiceCharge) as service,  sum(b.AssestRepaymentFund) as ARF
				FROM tblpatplantproddetail a 
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`OPEx` AS `OPEx`,
					`tblpatpodetail`.`ServiceCharge` AS `ServiceCharge`,
					`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`				
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as	b ON a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON b.PlantGUID = c.PlantGUID WHERE c.CountryID='".$strwhr."' AND c.StateID='".$strstatewhr."' AND c.District = '".$districtwhr."' AND c.PlantGUID = '".$plantwhr."' ";

			}
			//echo $sql;
			$data = $this->db->query($sql)->result()[0]; //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
            

		}catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

	//////////////////////// 	 Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChart($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		try{
	if(!empty($strwhr) && empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){

$sql = "SELECT 'Machinery' as label, COUNT(`MachineryOther`) AS VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND `CountryID`='".$strwhr."' 
		UNION
		SELECT 'Gran' as label, COUNT(`MachineryOther`) AS VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND `CountryID`='".$strwhr."' 
		UNION
		SELECT 'SelfFunded' as label, COUNT(`MachineryOther`) AS  VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND `CountryID`='".$strwhr."' 
		UNION
		SELECT 'SWEFranchisor' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND `CountryID`='".$strwhr."' 
		UNION
		SELECT 'Bankloan' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND `CountryID`='".$strwhr."' "; 

	}elseif(!empty($strwhr) && !empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){

$sql = "SELECT 'Machinery' as label, COUNT(`MachineryOther`) AS VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=1 AND `CountryID`='".$strwhr."'  AND `StateID`='".$strstatewhr."'
		UNION
		SELECT 'Gran' as label, COUNT(`MachineryOther`) AS VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."'
		UNION
		SELECT 'SelfFunded' as label, COUNT(`MachineryOther`) AS  VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' 
		UNION
		SELECT 'SWEFranchisor' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND `CountryID`='".$strwhr."'  AND `StateID`='".$strstatewhr."'
		UNION
		SELECT 'Bankloan' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' ";

	}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && empty($plantwhr)){
$sql = "SELECT 'Machinery' as label, COUNT(`MachineryOther`) AS VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=1
		UNION
		SELECT 'Gran' as label, COUNT(`MachineryOther`) AS VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=2  AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."'
		UNION
		SELECT 'SelfFunded' as label, COUNT(`MachineryOther`) AS  VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND  `District` = '".$districtwhr."'
		UNION
		SELECT 'SWEFranchisor' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=4  AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."'
		UNION
		SELECT 'Bankloan' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=5  AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."'";

	}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && !empty($plantwhr)){
$sql = "SELECT 'Machinery' as label, COUNT(`MachineryOther`) AS VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=1  AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."' AND `PlantGUID` = '".$plantwhr."'
		UNION
		SELECT 'Gran' as label, COUNT(`MachineryOther`) AS VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=2 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."' AND `PlantGUID` = '".$plantwhr."'
		UNION
		SELECT 'SelfFunded' as label, COUNT(`MachineryOther`) AS  VALUE FROM `tblpatplantdetail` WHERE `MachineryOther`=3 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."' AND `PlantGUID` = '".$plantwhr."'
		UNION
		SELECT 'SWEFranchisor' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=4 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."' AND `PlantGUID` = '".$plantwhr."'
		UNION
		SELECT 'Bankloan' as label, COUNT(`MachineryOther`) AS  VALUE  FROM `tblpatplantdetail` WHERE `MachineryOther`=5 AND `CountryID`='".$strwhr."' AND `StateID`='".$strstatewhr."' AND `District` = '".$districtwhr."' AND `PlantGUID` = '".$plantwhr."'";
	}
		$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); die;
		return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	

public function getfourthOpex($strwhr,$strstatewhr,$districtwhr,$plantwhr){

	try{
		if(!empty($strwhr) && empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){
		$sql = "SELECT
					'revenueOpex' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
					round((a.Price * a.Volume)) < b.OPEx AND c.CountryID='".$strwhr."'
				UNION
				SELECT
					'OpexRevenueOPExservicecharge' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPEx` AS `OPEx`,
`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				b.OPEx<round((a.Price * a.Volume)) < b.OPExservicecharge AND c.CountryID='".$strwhr."'
				
				UNION

				SELECT
					'OpexOPExserviceRevenueARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPEx` AS `OPEx`,
`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`,
`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				(b.OPEx + b.OPExservicecharge)<round((a.Price * a.Volume)) < (b.OPExservicecharge + b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."'
				
				UNION

				SELECT
					'RevenueOpexOPExserviceARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPEx` AS `OPEx`,
`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`,
`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				round((a.Price * a.Volume))>(b.OPEx + b.OPExservicecharge+b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' ";
		}elseif(!empty($strwhr) && !empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){
			$sql = "SELECT
					'revenueOpex' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
					round((a.Price * a.Volume)) < b.OPEx AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."'
				UNION
				SELECT
					'OpexRevenueOPExservicecharge' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				b.OPEx< round((a.Price * a.Volume)) < b.OPExservicecharge AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."'
				
				UNION

				SELECT
					'OpexOPExserviceRevenueARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				(b.OPEx + b.OPExservicecharge)<round((a.Price * a.Volume)) < (b.OPExservicecharge + b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."'
				
				UNION

				SELECT
					'RevenueOpexOPExserviceARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				round((a.Price * a.Volume))>(b.OPEx + b.OPExservicecharge+ b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' ";
		}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && empty($plantwhr)){

			$sql = "SELECT
					'revenueOpex' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
					round((a.Price * a.Volume)) < b.OPEx AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."'
				UNION
				SELECT
					'OpexRevenueOPExservicecharge' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				b.OPEx<round((a.Price * a.Volume)) < b.OPExservicecharge AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."'
				
				UNION

				SELECT
					'OpexOPExserviceRevenueARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				(b.OPEx + b.OPExservicecharge)<round((a.Price * a.Volume)) < (b.OPExservicecharge + b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."'
				
				UNION

				SELECT
					'RevenueOpexOPExserviceARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				round((a.Price * a.Volume))>(b.OPEx + b.OPExservicecharge+b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."' ";

		}elseif(!empty($strwhr) && !empty($strstatewhr) && !empty($districtwhr) && !empty($plantwhr)){
			$sql = "SELECT
					'revenueOpex' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
					round((a.Price * a.Volume)) < b.OPEx AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."' AND c.PlantGUID='".$plantwhr."'
				UNION
				SELECT
					'OpexRevenueOPExservicecharge' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				b.OPEx<round((a.Price * a.Volume)) < b.OPExservicecharge AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."' AND c.PlantGUID='".$plantwhr."'
				
				UNION

				SELECT
					'OpexOPExserviceRevenueARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				(b.OPEx + b.OPExservicecharge)<round((a.Price * a.Volume)) < (b.OPExservicecharge + b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."' AND c.PlantGUID='".$plantwhr."'
				
				UNION

				SELECT
					'RevenueOpexOPExserviceARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)			
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				round((a.Price * a.Volume))>(b.OPEx + b.OPExservicecharge+b.AssestRepaymentFund)
				AND c.CountryID='".$strwhr."' AND c.StateID = '".$strstatewhr."' AND c.District='".$districtwhr."' AND c.PlantGUID='".$plantwhr."' ";
		}
			//echo $sql;
			$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
	}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

}
   

/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}