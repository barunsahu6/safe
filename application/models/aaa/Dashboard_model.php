<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			$this->db->limit($limit,$start);
			return $this->db->get(DISTRICT)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	

/**
	 * Method getPATSofiescore() get detail Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATSofiescore($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			$this->db->close();
			$this->db->initialize();
			//$this->db->select("round(sum(`ss`)/count(*)) AS 'Social', round(sum(`FS`)/count(*)) AS 'financial',round(sum(`OS`)/count(*)) AS 'operational', round(sum(`IS_t`)/count(*)) AS 'Institutional',round(sum(`ES`)/count(*)) AS 'Environmental'");
			//$this->db->where('CountryID',$strwhr);
			//return($this->db->get(vw_pat_score)->result()[0]); 
			$sql = "SELECT round(sum(`ss`)/count(*)) AS 'Social', round(sum(`FS`)/count(*)) AS 'financial',round(sum(`OS`)/count(*)) AS 'operational', round(sum(`IS_t`)/count(*)) AS 'Institutional',round(sum(`ES`)/count(*)) AS 'Environmental' 
			FROM `vw_pat_score` WHERE 1=1";

			if(!empty($strwhr)){
				$sql .= " AND `CountryID` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `StateID` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `District` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";

		   $data = $this->db->query($sql)->result()[0];
		   return $data;
		   }
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getPerformanceSofieScore() get detail Performance Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPerformanceSofieScore($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
						
			$sql = "SELECT ROUND(AVG(`SoS`)) as 'social', ROUND(AVG(`OpS`)) as 'operational', ROUND(AVG(`FsS`)) AS 'financial', ROUND(AVG(`InS`)) as 'Institutional',ROUND(AVG(`EnS`)) as 'Environmental' 
			FROM `vw_plant_sofiescore` WHERE 1=1";

			if(!empty($strwhr)){
				$sql .= " AND `CountryID` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `StateID` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `District` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";
			//echo $sql; die;
		   $data = $this->db->query($sql)->result()[0];
		   return $data;
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

//////////// Count District /////////////////////////////////

public function getStateSofieScore($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		
		try{
			
		$sql = "SELECT `ps`.PlantGUID,`ps`.`stateid`,`ms`.`DistrictName` AS 'DistrictName', `ms`.`DistrictID` AS 'DistrictID',
		 
		ROUND(`SoS`) as social, ROUND(`OpS`) as operational, 
		ROUND(`FsS`) AS financial, ROUND(`InS`) as Institutional, 
		ROUND(`EnS`) as Environmental 
		FROM `vw_plant_sofiescore` as `ps` 
		LEFT JOIN `mstdistrict` as`ms` ON `ms`.DistrictID = `ps`.`District` WHERE 1=1 ";

			if(!empty($strwhr)){
				$sql .= " AND `ps`.`countryid` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `ps`.`stateid` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `ps`.`district` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `ps`.`PlantGUID` = '".$plantwhr."'";
			}
			
			$sql .= " GROUP BY ms.`DistrictName`,ms.`DistrictID` ORDER BY `DistrictName` ASC";

			//echo $sql; //die;

		   $data = $this->db->query($sql)->result();
		   return $data;
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}




/**
	 * Method getPerformanceLimit() get detail Performance Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPerformanceLimit($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			$sql = "SELECT (SELECT COUNT(*) FROM `vw_plantsofiescore` WHERE `TS` < 60)AS 'lessthan60', (SELECT COUNT(*) FROM `vw_plantsofiescore` WHERE `TS` > 60)AS 'morethan60',
(SELECT COUNT(*) FROM `vw_plantsofiescore` WHERE `TS`=60)AS 'eql60'
FROM `vw_plant_sofiescore` WHERE 1=1 ";

			if(!empty($strwhr)){
				$sql .= " AND `countryid` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `stateid` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `district` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";

			$data = $this->db->query($sql)->result()[0];
		return $data;
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	/**
	 * Method getPerformance() get detail Performance Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPerformance($strwhr,$strstatewhr,$districtwhr,$plantwhr){
		try{
			//$this->db->select('round(((AVG(`SoS`)+AVG(`OpS`)+AVG(`FsS`)+AVG(`InS`)+AVG(`EnS`))/5)) as PERFORMANCE');
			//$this->db->where('countryid',$strwhr);
		//	$this->db->get(vw_plantsofiescore)->result()[0]; echo $this->db->last_query(); die; 

		$sql = " SELECT round(((AVG(`SoS`)+AVG(`OpS`)+AVG(`FsS`)+AVG(`InS`)+AVG(`EnS`))/5)) as 'PERFORMANCE' 
			FROM `vw_plant_sofiescore` WHERE 1=1";

			if(!empty($strwhr)){
				$sql .= " AND `CountryID` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `StateID` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `District` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";
			//echo $sql;
		   $data = $this->db->query($sql)->result()[0];

		   return $data;
			//return($this->db->get(vw_plant_sofiescore)->result()[0]); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}





/**
	 * Method DatafromOnWebService_Map() get Location Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function DatafromOnWebService_Map($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		// $sql = "SELECT DISTINCT 
        // 		tbltstlocation.VillageName AS description,
		// 		tbltstlocation.XValue AS Latitude,
		// 		tbltstlocation.YValue AS Longitude
		// 		FROM tbltstlocation WHERE 1=1";

		$sql = "SELECT DISTINCT tbt.VillageName AS description, tbt.XValue AS Latitude, 
		tbt.YValue AS Longitude FROM tbltstlocation AS tbt 
		LEFT JOIN tblpatpodetail AS tppd ON tppd.PlantGUID = tbt.GUID 
		WHERE 1=1";

		// $sql = "SELECT DISTINCT tppd.VillageName AS description, tbt.Latitude AS Latitude, 
		// 		tbt.Longitude AS Longitude FROM tblpatpodetail AS tbt 
		// 		LEFT JOIN tbltstlocation AS tppd ON tppd.GUID = tbt.PlantGUID 
		// 		WHERE 1=1 ";

		if(!empty($strwhr)){
			 $sql .= " AND tbt.`CountryID` = '".$strwhr."'";
		}
		if(!empty($strstatewhr)){
			 $sql .= " AND tbt.`StateID` = '".$strstatewhr."'";
		}
		if(!empty($districtwhr)){
			 $sql .= " AND tbt.`DistrictID` = '".$districtwhr."'";
		}
		if(!empty($plantwhr)){
			$sql .= " AND tppd.PlantGUID = '".$plantwhr."'";
		}
		$sql .= " ORDER BY tbt.`LocationUID` DESC";
		//echo $sql; //die;
		return($data = $this->db->query($sql)->result());
	}


/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}