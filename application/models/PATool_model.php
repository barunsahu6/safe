<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PATool_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(PATPD);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(PATPD)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
		
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			$password = $this->input->post('access_key', true);
		   
			return ($this->db->insert(PATPD,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	
	public function getDetail($token)
	{
		try {
			$this->db->select('*');
			//$this->db->where(PATPLANDETAIL.'.PlantGUID',$token);
			$this->db->where(PATPLANDETAIL.'.PlantGUID',$token);
			$this->db->from(PATPLANDETAIL);
			$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID','INNER');
			$this->db->join(USERS, USERS.'.UserID ='.PATPLANDETAIL.'.UserID','INNER');
			$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID','INNER');
			$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID','INNER');
			$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District','LEFT');
			$this->db->join(tblpatplantinstitutionaldocument, tblpatplantinstitutionaldocument.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID','LEFT');
			$this->db->join(DISTRIBUTION, DISTRIBUTION.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID','LEFT');
		    $result = $this->db->get()->row(); //echo $this->db->last_query(); die;
			return $result;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(PATPD)->result(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			//echo $token;
			//echo "<pre>";
			//print_r($_FILES);
			//echo "<pre>";
			//print_r($_POST); 
		 //
			//die;
		
		

		//die;	
			

			    $dpval = $this->input->post('DPName');
				 $dpcount = count($dpval);
				$dpdistance = $this->input->post('Distance');

				$deletedatadistribution=array(
								'PlantGUID'  =>  $this->input->post('PlantGUID'),
					);
				$this->db->delete('tblpatplantdistribution', $deletedatadistribution);
				
			for($i=0; $i < $dpcount; $i++){
				if($dpval[$i]==1){
					$dpname = "Distribution Point1";
				}elseif($dpval[$i]==2){
					$dpname = "Distribution Point2";
				}elseif($dpval[$i]==3){
					$dpname = "Distribution Point3";
				}elseif($dpval[$i]==4){
					$dpname = "Distribution Point4";
				}elseif($dpval[$i]==5){
					$dpname = "Distribution Point5";
				}elseif($dpval[$i]==6){
					$dpname = "Distribution Point6";
				}elseif($dpval[$i]==7){
					$dpname = "Distribution Point7";
				}elseif($dpval[$i]==8){
					$dpname = "Distribution Point8";
				}elseif($dpval[$i]==9){
					$dpname = "Distribution Point9";
				}elseif($dpval[$i]==10){
					$dpname = "Distribution Point10";
				}

				$Insertdp=array(
								'PlantGUID' 	=>  $this->input->post('PlantGUID'),
								'DPID'    		=>  $dpval[$i],
								'Distance'      =>  $dpdistance[$i],
								'DPName'    	=>  $dpname,
								'IsDeleted'		=>  0,
							);

							
			$this->db->insert('tblpatplantdistribution', $Insertdp);
		
			}
		

				$form = $this->input->post();
				$POGUID 					=  $this->input->post('POGUID');
				$PlantGUID					=  $this->input->post('PlantGUID');
				$MachineryOther 			= $this->input->post('MachineryOther');
				$LandOther 					= $this->input->post('LandOther');
				$BuildingOther 				= $this->input->post('BuildingOther');
				$RawWaterSourceOther 		= $this->input->post('RawWaterSourceOther');
				$ElectricityOther 			= $this->input->post('ElectricityOther');
			
				$MachineryFlagID    		=  $this->input->post('MachineryFlag');
				$LandFlagID    				=  $this->input->post('LandFlag');
				$BuildingFlagID    			=  $this->input->post('BuildingFlag');
				$RawWaterSourceFlagID  		=  $this->input->post('RawWaterSourceFlag');
				$ElectricityFlagID    		=  $this->input->post('ElectricityFlag');

		 	$deletedataMFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'FlagID'    				=>  $MachineryFlagID,
							);
			$this->db->delete('tblpatplantassetfunder', $deletedataMFunder);

			$InsertMachinerydataFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'AssetFunderID'    			=>  $MachineryOther,
								'FlagID'    				=>  $MachineryFlagID,
							);

			$this->db->insert('tblpatplantassetfunder', $InsertMachinerydataFunder);

			$deletedataLandFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'FlagID'    				=>  $LandFlagID,
							);
			$this->db->delete('tblpatplantassetfunder', $deletedataLandFunder);

			$LanddataFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'AssetFunderID'    			=>  $LandOther,
								'FlagID'    				=>  $LandFlagID,
							);

			$this->db->insert('tblpatplantassetfunder', $LanddataFunder);

			$deletedataBuildingFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'FlagID'    				=>  $BuildingFlagID,
							);
			$this->db->delete('tblpatplantassetfunder', $deletedataBuildingFunder);

			$BuildingdataFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'AssetFunderID'    			=>  $BuildingOther,
								'FlagID'    				=>  $BuildingFlagID,
							);
							
			$this->db->insert('tblpatplantassetfunder', $BuildingdataFunder);

			$deletedataRawWaterFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'FlagID'    				=>  $RawWaterSourceFlagID,
							);
			$this->db->delete('tblpatplantassetfunder', $deletedataRawWaterFunder);

			
			$RawWaterdataFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'AssetFunderID'    			=>  $RawWaterSourceOther,
								'FlagID'    				=>  $RawWaterSourceFlagID,
				);
			$this->db->insert('tblpatplantassetfunder', $RawWaterdataFunder);

			$deletedataElectricitFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'FlagID'    				=>  $ElectricityFlagID,
				);
			$this->db->delete('tblpatplantassetfunder', $deletedataElectricitFunder);

		   $ElectricitydataFunder=array(
								'POGUID' 					=>  $this->input->post('POGUID'),
								'PlantGUID'    				=>  $this->input->post('PlantGUID'),
								'AssetFunderID'    			=>  $ElectricityOther,
								'FlagID'    				=>  $ElectricityFlagID,
				);

			$this->db->insert('tblpatplantassetfunder', $ElectricitydataFunder);
						 
			
			if(!empty($this->input->post('SC'))){
				$SC = 1;
			}else{
				$SC = 0;
			}
			if(!empty($this->input->post('ST'))){ 
				$ST = 1;
			}else{
				$ST = 0;
			}
			if(!empty($this->input->post('OBC'))){
				$OBC = 1;
			}else{
				$OBC = 0;
			}
			if(!empty($this->input->post('BPL'))){
				$BPL = 1;
			}else{
				$BPL = 0;
			}
			if(!empty($this->input->post('BPL'))){
				$BPL = 1;
			}else{
				$BPL = 0;
			}
			if(!empty($this->input->post('General'))){
				$General = 1;
			}else{
				$General = 0;
			}

			if(!empty($this->input->post('AllInclusion'))){
				$AllInclusion = 1;
			}else{
				$AllInclusion = 0;
			}
			if(!empty($this->input->post('Designation'))){
				$DesignationID = $this->input->post('Designation');
			}else{
				$DesigManufacturerNamenationID = 0;
			}
			if($this->input->post('RemoteMonitoringSystem')=='YES'){
				$RemoteMonitoringSystem = 1;
			}else{
				$RemoteMonitoringSystem = 0;
			}
			if(!empty($this->input->post('UserID'))){
				$UserID = $this->input->post('UserID');
			}else{
				$UserID = 0;
			}
			if(!empty($this->input->post('LiteracyID'))){
				$LiteracyID = $this->input->post('LiteracyID');
			}else{
				$LiteracyID = 0;
			}
		
			if(!empty($this->input->post('RejectWaterDischargeApproval'))){
				$RejectWaterDischargeApproval = $this->input->post('RejectWaterDischargeApproval');
			}else{
				$RejectWaterDischargeApproval = 0;
			}
		

			if(!empty($this->input->post('ServiceProviderApproval'))){
				$ServiceProviderApproval = $this->input->post('ServiceProviderApproval');
			}else{
				$ServiceProviderApproval = 0;
			}
			if(!empty($this->input->post('OperatorQualification'))){
				$OperatorQualification = $this->input->post('OperatorQualification');
			}else{
				$OperatorQualification = 0;
			}
			if(!empty($this->input->post('OperatorQualification'))){
				$OperatorQualification = $this->input->post('OperatorQualification');
			}else{
				$OperatorQualification = 0;
			}
////// Second Table ////////////////

			if(!empty($this->input->post('Operatorcertificate'))){
				$Operatorcertificate = $this->input->post('Operatorcertificate');
			}else{
				$Operatorcertificate = 0;
			}
			if(!empty($this->input->post('LastWQtestreport'))){
				$LastWQtestreport = $thitblpatplantpurificationsteps->input->post('LastWQtestreport');
			}else{
				$LastWQtestreport = 0;
			}
			if(!empty($this->input->post('CheckforIECmaterial'))){
				$CheckforIECmaterial = $this->input->post('CheckforIECmaterial');
			}else{
				$CheckforIECmaterial = 0;
			}
			if(!empty($this->input->post('ResidualChlorine'))){
				$ResidualChlorine = $this->input->post('ResidualChlorine');
			}else{
				$ResidualChlorine = 0;
			}

			if(!empty($this->input->post('CheckforReceipts'))){
				$CheckforReceipts = $this->input->post('CheckforReceipts');
			}else{
				$CheckforReceipts = 0;
			}
			
			if(!empty($this->input->post('RejectWaterTesting'))){
				$RejectWaterTesting = $this->input->post('RejectWaterTesting');
			}else{
				$RejectWaterTesting = 0;
			}
			if($this->input->post('MotorRepair') =='MotorRepair'){
				$MotorRepair = 1;
			}else{
				$MotorRepair = 0;
			}
			if($this->input->post('MembraneChoke'=='MembraneChoke')){
				$MembraneChoke = 1;
			}else{
				$MembraneChoke = 0;
			}
			if($this->input->post('Rawwaterproblem')=='Rawwaterproblem'){
				$Rawwaterproblem = 1;
			}else{
				$RawwaManufacturerNameterproblem = 0;
			}
			if($this->input->post('Anyother')=='Anyother'){
				$Anyother = 1;
			}else{
				$Anyother = 0;
			}
			if($this->input->post('ElectricalSafety') =='ElectricalSafety'){
				$ElectricalSafety = 1;
			}else{
				$ElectricalSafety = 0;
			}
			if($this->input->post('PlantOM'=='PlantOM')){
				$PlantManufacturerNameOM = 1;
			}else{
				$PlantOM = 0;
			}
			if($this->input->post('WaterQuality')=='WaterQuality'){
				$WaterQuality = 1;
			}else{
				$WaterQuality = 0;
			}
			if($this->input->post('ConsumerAwareness')=='ConsumerAwareness'){
				$ConsumerAwareness = 1;
			}else{
				$ConsumerAwareness = 0;
			}
			if($this->input->post('ABKeeping')=='ABKeeping'){
				$ABKeeping = 1;
			}else{
				$ABKeeping = 0;
			}
			if($this->input->post('MiscellaneousChk')=='MiscellaneousChk'){
				$MiscellaneousChk = 1;
			}else{
				$MiscellaneousChk = 0;
			}

			

			if(!empty($this->input->post('AnyotherEdit'))){
				$AnyotherEdit = $this->input->post('AnyotherEdit');
			}else{
				$AnyotherEdit = 0;
			}
			if(!empty($this->input->post('checkRWQuantification'))){
				$checkRWQuantification = $this->input->post('checkRWQuantification');
			}else{
				$checkRWQuantification = 0;
			}
			if(!empty($this->input->post('CheckWharvesting'))){
				$CheckWharvesting = $this->input->post('CheckWharvesting');
			}else{
				$CheckWharvesting = 0;
			}
			if($this->input->post('Electricityoutage')=='Electricityoutage'){
				$Electricityoutage = 1;
			}else{
				$Electricityoutage = 0;
			}

	
		 	$visitDateDBConvert =  date("Y-m-d", strtotime($this->input->post('VisitDate')));
				
			$EstablishmentDateDBConvert =  date("Y-m-d", strtotime($this->input->post('EstablishmentDate')));
		 	$count = count($this->input->post('WQC'));

			$WQC = $this->input->post('WQC');

			if($count >0){
				$this->db->delete('tblpatwatercontaminants', array('PlantGUID' => $this->input->post('PlantGUID'))); //echo  $this->db->last_query();die;
			}

		     for($i=0; $i < $count; $i++){
				 if($WQC[$i] =='TDS (Total Dissolved Solids)'){
					$WQCValues = 1;
				 }
				 if($WQC[$i] =='As (Arsenic)'){
					$WQCValues = 2;
				 }
				 if($WQC[$i] =='Fe (Iron)'){
					$WQCValues = 3;
				 }
				 if($WQC[$i] =='F ¯ (Fluoride)'){
					$WQCValues = 4;
				 }
				 if($WQC[$i] =='NO3¯ (Nitrate)'){
					$WQCValues = 5;
				 }
				 if($WQC[$i] =='Microbial'){
					$WQCValues = 6;
				 }
				 if($WQC[$i] =='Any Other'){
					$WQCValues = 7;
				 }
				$data_array[$i]=array(
				'WaterContaminantsID' => 0,
				'WaterQualityChallengeID' => $WQCValues,
				'PlantGUID' => $this->input->post('PlantGUID'),
				'IsDeleted' => 0,
				);
				($this->db->insert('tblpatwatercontaminants',$data_array[$i])); //echo $this->db->last_query();//die;
			}
			//die;
			
			 $PlantPurificationStepcount = count($this->input->post('PlantPurificationStep'));
			$PlantPurificationStep =$this->input->post('PlantPurificationStep');
			
			if($PlantPurificationStepcount > 0){
				$this->db->delete('tblpatplantpurificationstep', array('PlantGUID' => $this->input->post('PlantGUID'))); //secho $this->db->last_query();die;
			}
			//echo "delete";
		//	die;
		     for($i=0; $i < $PlantPurificationStepcount; $i++){
				
				 if($PlantPurificationStep[$i]=='Sand filter'){
					 $PlantPurificationStepValues = 1;
				 }
				  if($PlantPurificationStep[$i]=='Activated Charcoal Filter'){
					 $PlantPurificationStepValues = 3;
				 }
				  if($PlantPurificationStep[$i]=='Micron Filter'){
					 $PlantPurificationStepValues = 4;
				 }
				  if($PlantPurificationStep[$i]=='RO Membrane'){
					 $PlantPurificationStepValues = 5;
				 }
				  if($PlantPurificationStep[$i]=='UV'){
					 $PlantPurificationStepValues = 6;
				 }
				  if($PlantPurificationStep[$i]=='Residual Chlorination'){
					 $PlantPurificationStepValues = 7;
				 }

				$data_array=array(
				'PlantPurificationstepID' => $PlantPurificationStepValues,
				'PlantGUID' => $this->input->post('PlantGUID'),
				'IsDeleted' => 0,
				);
				$this->db->insert('tblpatplantpurificationstep',$data_array); //echo $this->db->last_query();
			}
			//die;
			//echo $this->db->last_query();
		

			if(!empty($this->input->post('grant'))){
				 $MachineryOthergrant1 = $this->input->post('grant');
			}else{
				 $MachineryOthergrant1 = 'Null';
			}
			//die;

			if(!empty($this->input->post('landother'))){
				$LandOther_anyother = $this->input->post('landother');
			}else{
				$LandOther_anyother = $this->input->post('LandOther');
			}

			if(!empty($this->input->post('BuildinganOther'))){
				$BuildinganOther_anyother = $this->input->post('BuildinganOther');
			}else{
				$BuildinganOther_anyother = $this->input->post('BuildingOther');
			}
			if(!empty($this->input->post('RawWaterOther'))){
				$RawWaterOther_anyother = $this->input->post('RawWaterOther');
			}else{
				$RawWaterOther_anyother = $this->input->post('RawWaterSourceOther');
			}
			if(!empty($this->input->post('ElectricityanyOther'))){
				$Electricity_anyother = $this->input->post('ElectricityanyOther');
			}else{
				$Electricity_anyother = $this->input->post('ElectricityOther');
			}
						
			$data2=array(
								'VisitDate' 					=> $visitDateDBConvert,
								'SWNID'    						=>  $this->input->post('SWNID'),
								'MailingAddress'    			=>  $this->input->post('MailingAddress'),
								'PlantLocation'     			=>  $this->input->post('PlantLocation'),
								'PlantLatitude'     			=>  $this->input->post('PlantLatitude'),
								'PlantPhotoLink'    			=>  $this->input->post('PlantPhotoLink'),
								'UserID'  						=>  $UserID,
								'AgencyID' 						=>  $this->input->post('AgencyID'),
								'LocationType'  				=>  $this->input->post('LocationType'),
								'CountryID'  					=>  $this->input->post('Country'),
								'StateID' 						=>  $this->input->post('State'),
								'District'  					=>  $this->input->post('District'),
								'BlockName'  					=>  $this->input->post('BlockName'),	
								'VillageName' 					=>  $this->input->post('VillageName'),
								'PinCode'  						=>  $this->input->post('PinCode'),
								'SC' 							=>  $SC,
								'ST'  							=>  $ST,
								'OBC'  							=>  $OBC,
								'General' 						=>  $General,
								'AllInclusion' 					=>  $AllInclusion,
								'OperatorName'  				=>  $this->input->post('OperatorName'),
								'DesignationID' 				=>  $DesignationID,
								'ContactNumber'  				=>  $this->input->post('ContactNumber'),
								'LiteracyID'  					=>  $LiteracyID,
								'PlantSpecificationID' 			=>  $this->input->post('PlantSpecificationID'),
								'PlantManufacturerID'  			=>  $this->input->post('PlantManufacturerID'),
								'ManufacturerName'  			=>  $this->input->post('ManufacturerName'),	
								'EstablishmentDate'	 			=>  $EstablishmentDateDBConvert,
								'AgeOfPlant'  					=>  $this->input->post('AgeOfPlant'),
								'RemoteMonitoringSystem' 		=>  $RemoteMonitoringSystem,
								'LandCost' 		 				=>  $this->input->post('LandCost1'),
								'LandOther'  					=>  $this->input->post('LandOther'),
								'MachineryCost' 				=>  $this->input->post('MachineryCost'),
								'MachineryOther'  				=>  $MachineryOthergrant1,
								'BuildingCost' 	 				=>  $this->input->post('BuildingCost1'),
								'BuildingOther' 				=>  $BuildinganOther_anyother,
								'RawWaterSourceCost'  			=>  $this->input->post('RawWaterSourceCost'),
								'RawWaterSourceOther'  			=>  $RawWaterOther_anyother,
								'PlantManufacturerID'  			=>  $this->input->post('PlantManufacturerID'),
								'ElectricityCost'  				=>  $this->input->post('ElectricityCost1'),	
								'ElectricityOther' 				=>  $Electricity_anyother,
								'Distribution'  				=>  $this->input->post('Distribution'),
								'NoOfhhregistered' 				=>  $this->input->post('NoOfhhregistered'),
								'AvgMonthlyCards'  				=>  $this->input->post('AvgMonthlyCards'),
								'GramPanchayatApproval'  		=>  $this->input->post('GramPanchayatApproval'),
								'LegalElectricityConnection' 	=>  $this->input->post('LegalElectricityConnection'),
								'LandApproval'  				=>  $this->input->post('LandApproval'),
								'RawWaterSourceApproval'  		=>  $this->input->post('RawWaterSourceApproval'),
								'RejectWaterDischargeApproval'  =>  $RejectWaterDischargeApproval,
								'ServiceProviderApproval' 		=>  $ServiceProviderApproval,
								//'OperatorQualification'  		=>  $peratorQualification,
								'WaterBrandName' 				=>  $this->input->post('WaterBrandName'),
								'PlantSpecificationAnyOther'  	=>  $this->input->post('PlantSpecificationAnyOther'),
								'PAssetSourceFundedBy' 	 		=>  $this->input->post('PAssetSourceFundedBy'),	
								'NoOfHousehold' 				=>  $this->input->post('NoOfHousehold'),
								'NoOfHouseholdWithin2km'  		=>  $this->input->post('NoOfHouseholdWithin2km'),
								'BPL'  							=>  $BPL,
								'auditAgencyOther'  			=>  $this->input->post('auditAgencyOther'),
								'EducationAnyOther'  			=>  $this->input->post('EducationAnyOther'),
								'ContaminantAnyOther' 			=>  $this->input->post('ContaminantAnyOther'),
								'Population'  					=>  $this->input->post('Population'),
						);
						//echo "<pre>";
						//print_r($data2); //die;

			  $this->db->where('PlantGUID',$this->input->post('PlantGUID'));
			  $resultDetail =($this->db->update(PATPLANDETAIL,$data2)) ? 1 : -1;
			
			//echo $this->db->last_query(); die;
			
			//if($resultDetail==1){

					if($this->input->post('PreMonsoonRWTesting')=='PRERW'){
							$PreMonsoonRWTestingcheck =1;
					}else{
							$PreMonsoonRWTestingcheck =0;
					}
					if($this->input->post('PreMonsoonTWTesting')=='PRETWT'){
							$PreMonsoonTWTestingcheck =1;
					}else{
							$PreMonsoonTWTestingcheck =0;
					}
					if($this->input->post('PostMonsoonRWTesting')=='POSTRW'){
							$PostMonsoonRWTestingcheck =1;
					}else{
						$PostMonsoonRWTestingcheck =0;
					}
					if($this->input->post('PostMonsoonTWtesting')=='POSTTWT'){
							$PostMonsoonTWtestingcheck =1;
					}else{
							$PostMonsoonTWtestingcheck =0;
					}
					if($this->input->post('RejectWaterTesting')=='REJWT'){
							$RejectWaterTestingcheck =1;
					}else{
						$RejectWaterTestingcheck =0;
					}
					
				$PreMonsoonRWTestingdateDBConvert 	=  date("Y-m-d", strtotime($this->input->post('PreMonsoonRWTestingdate')));
				$PreMonsoonTWTestingdateDBConvert 	=  date("Y-m-d", strtotime($this->input->post('PreMonsoonTWTestingdate')));
				$PostMonsoonRWtestingdateDBConvert  =  date("Y-m-d", strtotime($this->input->post('PostMonsoonRWtestingdate')));
				$PostMonsoonTWtestingdateDBConvert  =  date("Y-m-d", strtotime($this->input->post('PostMonsoonTWtestingdate')));
				$RejectWatertestingdateDBConvert    =  date("Y-m-d", strtotime($this->input->post('RejectWatertestingdate')));
			
				//$PreMonsoonRWTestProof = $_FILES['PreMonsoonRWTestProof']['name'];	//die;
				if(!empty($_FILES['PreMonsoonRWTestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PreMonsoonRWTestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$PreMonsoonRWTestProofimage = $encryptedName;
					}
				}else{
				    $PreMonsoonRWTestProofimage = $this->input->post('oldPreMonsoonRWTestProof');
				}

				//die;

				//$PreMonsoonTWProof = $_FILESS['PreMonsoonTWProof']['name'];
				
				if(!empty($_FILES['PreMonsoonTWProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PreMonsoonTWProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$PreMonsoonTWProofimage = $encryptedName;
					}

				}else{
					$PreMonsoonTWProofimage = $this->input->post('oldPreMonsoonTWProof');
				}

				//echo $_FILESS['PostMonsoonRWTestProof']['name']; die;
				// $PostMonsoonRWTestProof = $_FILESS['PostMonsoonRWTestProof']['name'];	//die;
				if(!empty($_FILES['PostMonsoonRWTestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PostMonsoonRWTestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						 $PostMonsoonRWTestProofimage = $encryptedName;
					}
				}else{
					 $PostMonsoonRWTestProofimage = $this->input->post('oldPostMonsoonRWTestProof');
				}
				//die;
				//$PostMonsoonTWtestProof = $_FILESS['PostMonsoonTWtestProof']['name'];	
				if(!empty($_FILES['PostMonsoonTWtestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PostMonsoonTWtestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$PostMonsoonTWtestProofimage = $encryptedName;
					}
				}else{
					$PostMonsoonTWtestProofimage = $this->input->post('oldPostMonsoonTWtestProof');
				}
			
				//$RejectWaterTestProof = $_FILESS['RejectWaterTestProof']['name'];	
				if(!empty($_FILES['RejectWaterTestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['RejectWaterTestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$RejectWaterTestProofimage = $encryptedName;
					}
				}else{
					$RejectWaterTestProofimage = $this->input->post('oldRejectWaterTestProof');
				}

				$data=array(
						'AuditedBy'  				=>  $this->input->post('AuditedBy'),
                        'Latitude'  				=>  $this->input->post('Latitude'),
						'Longitude' 				=> $this->input->post('Longitude'),
						'AgencyAnyOther'  			=>  $this->input->post('AgencyAnyOther'),
                        'AuditingAgency'  			=>  $this->input->post('AuditingAgency'),
     					'AuditingAgencyAddress' 	=> $this->input->post('AuditingAgencyAddress'),
						'ContactName'  				=>  $this->input->post('ContactName'),
                        'Email'  					=>  $this->input->post('Email'),
						'ElectricalSafety' 			=>  $ElectricalSafety,
						'Earthing'  				=>  $Earthing,
                        'PlantOM'  					=>  $PlantOM,	
						'Operatorcertificate' 		=> $Operatorcertificate,
						'WaterQuality'  			=>  $WaterQuality,
                        'LastWQtestreport'  		=>  $LastWQtestreport,
						'ConsumerAwareness' 		=> $ConsumerAwareness,
						'CheckforIECmaterial'  		=>  $CheckforIECmaterial,
                        'ABKeeping'  				=>  $ABKeeping,
     					'CheckforReceipts' 			=> $CheckforReceipts,
						'UV'  						=>  $this->input->post('UV'),
                        'TDS'  						=>  $this->input->post('TDS'),
						'pH' 						=> $this->input->post('pH'),
						'ResidualChlorine'  		=>  $ResidualChlorine,
                        'Microbial'  				=>  $this->input->post('Microbial'),	
						'PreMonsoonRWTesting' 		=> $PreMonsoonRWTestingcheck,
						'PreMonsoonRWLabAdr'  		=>  $this->input->post('PreMonsoonRWLabAdr'),
                        'PreMonsoonRWTestingdate'   => $PreMonsoonRWTestingdateDBConvert,
						'PreMonsoonRWTestProof' 	=> $PreMonsoonRWTestProofimage,
						'PreMonsoonTWTesting' 	 	=>  $PreMonsoonTWTestingcheck,
                        'PreMonsoonTWLabAdr'  		=>  $this->input->post('PreMonsoonTWLabAdr'),
     					'PreMonsoonTWTestingdate' 	=> $PreMonsoonTWTestingdateDBConvert,
						'PreMonsoonTWProof' 		=> $PreMonsoonTWProofimage,
                        'PostMonsoonRWTesting'  	=>  $PostMonsoonRWTestingcheck,
						'PostMonsoonRWLabadr' 		=> $this->input->post('PostMonsoonRWLabadr'),
						'PostMonsoonRWtestingdate'  =>  $PostMonsoonRWtestingdateDBConvert,
                        'PostMonsoonRWTestProof'  	=>  $PostMonsoonRWTestProofimage,	
						'PostMonsoonTWtesting' 		=> $PostMonsoonTWtestingcheck,
						'PostMonsoonTWLabAdr'  		=>  $this->input->post('PostMonsoonTWLabAdr'),
                        'PostMonsoonTWtestingdate'  => $PostMonsoonTWtestingdateDBConvert,
						'PostMonsoonTWtestProof' 	=> $PostMonsoonRWTestProofimage,
						'RejectWaterTesting'  		=>  $RejectWaterTestingcheck,
                        'RejectWaterLabAdr'  		=>  $this->input->post('RejectWaterLabAdr'),
     					'RejectWatertestingdate' 	=> $RejectWatertestingdateDBConvert,
						'RejectWaterTestProof'  	=>  $RejectWaterTestProofimage,
                        'RawWaterOpenwellcovered'   =>  $this->input->post('RawWaterOpenwellcovered'),
						'RawWaterBoreWellCasing' 	=> $this->input->post('RawWaterBoreWellCasing'),
						'Insidetheplant'  			=>  $this->input->post('Insidetheplant'),
                        'Covered'  					=>  $this->input->post('Covered'),
						'CleanlinessinPlant' 		=> $this->input->post('CleanlinessinPlant'),
						'Leakage'  					=>  $this->input->post('Leakage'),
                        'CleanlinessNearTreatmentPlant'  =>  $this->input->post('CleanlinessNearTreatmentPlant'),
						'Checkmossoralgee' 			=> $this->input->post('Checkmossoralgee'),
						'TechnicalDowntime'  		=>  $this->input->post('TechnicalDowntime'),
                        'NoOfDays'  				=>  $this->input->post('NoOfDays'),
     					'NoOfFault' 				=> $this->input->post('NoOfFault'),
						'MotorRepair'  				=>  $MotorRepair,
                        'MembraneChoke'  			=>  $MembraneChoke,
						'Rawwaterproblem' 			=> $Rawwaterproblem,
						'Electricityoutage'  		=>  $Electricityoutage,
                        'Anyother'  				=>  $Anyother,	
						'AnyotherEdit' 				=> $AnyotherEdit,
						'SalesDayLost'  			=>  $this->input->post('SalesDayLost'),
                        'WaterProductionDaily'  	=>  $this->input->post('WaterProductionDaily'),
						'WaterProductionMonthly' 	=> $this->input->post('WaterProductionMonthly'),
						'SamplingOrGift'  			=>  $this->input->post('SamplingOrGift'),
                        'LeakageEdit'  				=>  $this->input->post('LeakageEdit'),
     					'WaterTreatmentPlantCost' 	=> $this->input->post('WaterTreatmentPlantCost'),
						'InfrastructureCost'  		=>  $this->input->post('InfrastructureCost'),
                        'WaterBill'  				=>  $this->input->post('WaterBill'),
						'ElectricityBill' 			=> $this->input->post('ElectricityBill'),
						'GeneratorMaintainance'  	=>  $this->input->post('GeneratorMaintainance'),
                        'Rent'  					=>  $this->input->post('Rent'),		
                        'OperatorSalary' 			=> $this->input->post('OperatorSalary'),
						'ChemicalAndOtherConsumable'  =>  $this->input->post('ChemicalAndOtherConsumable'),
                        'MiscellaneousChk'  		=>  $MiscellaneousChk,
						'MiscFirstText' 			=> $this->input->post('Expense1'),
						'MiscFirstAmount' 			 =>  $this->input->post('Amount1'),
                        'MiscSecondText'  			=>  $this->input->post('Expense2'),
     					'MiscSecondAmount'			 => $this->input->post('Amount2'),
						'MiscThirdText'  			=>  $this->input->post('Expense3'),
                        'MiscThirdAmount'  			=>  $this->input->post('Amount3'),
						'ServiceCharge' 			=> $this->input->post('ServiceCharge'),
						'AssestRenewalFund'  		=>  $this->input->post('AssestRenewalFund'),
                        'AssestRepaymentFund'  		=>  $this->input->post('AssestRepaymentFund'),	
						'OPEx' 						=> $this->input->post('OPEx'),
						'OPExservicecharge'  		=>  $this->input->post('OPExservicecharge'),
                        'OPExservicechargemaintenance'  =>  $this->input->post('OPExservicechargemaintenance'),
						'opExSCMRAssetRepayment' 	=> $this->input->post('opExSCMRAssetRepayment'),
						'checkRWQuantification'  	=>  $checkRWQuantification,
                        'CheckRWUtilisation'  		=>  $this->input->post('CheckRWUtilisation'),
     					'CheckRWDisposal' 			=> $this->input->post('CheckRWDisposal'),
						'CheckWharvesting' 		 	=>  $CheckWharvesting,
                        //'WQuantification'  =>  $this->input->post('WQuantification'),
						//'WUtilisation' => $this->input->post('WUtilisation'),
						//'WDisposal'  =>  $this->input->post('WDisposal'),
                        'Presenceofpuddle'  		=>  $this->input->post('Presenceofpuddle'),		
						'UtilizationActivity1'		=> $this->input->post('UtilizationActivity1'),
						'UtilizationActivity2'  	=>  $this->input->post('UtilizationActivity2'),
                        'UtilizationActivity3'  	=>  $this->input->post('UtilizationActivity3'),
						'UtilizationActivity4' 		=> $this->input->post('UtilizationActivity4'),
						'UtilizationOther1'  		=>  $this->input->post('UtilizationOther1'),
                        'UtilizationOther2'  		=>  $this->input->post('UtilizationOther2'),
     					'UtilizationOther3'			=> $this->input->post('UtilizationOther3'),
						'UtilizationOther4'  		=>  $this->input->post('UtilizationOther4'),
                        'OperatorName'  			=>  $this->input->post('OperatorName'),
						'DesignationID' 			=> $this->input->post('DesignationID'),
						'ContactNumber'  			=>  $this->input->post('ContactNumber'),
                        'LiteracyID'  				=>  $this->input->post('LiteracyID'),	
						'EducationAnyOther' 		=> $this->input->post('EducationAnyOther'),
						'TransportExpense'  		=>  $this->input->post('TransportExpense'),
                        'PeakSale'  				=>  $this->input->post('PeakSale'),
						'Appearance'  				=>  $this->input->post('Appearance'),
						'Odour'  					=>  $this->input->post('Odour'),
						'Taste'  					=>  $this->input->post('Taste'),
						'TDSValue'  				=>  $this->input->post('TDSValue'),
					);	
					//echo "<pre>";
				//	print_r($data);
				
					//$this->db->where('PlantPOUID',$this->input->post('PlantPOUID'));
					$this->db->where('PlantGUID',$this->input->post('PlantGUID'));
					
					$this->db->update(PATPOD,$data); //echo $this->db->last_query();die;
					//echo $this->db->last_query(); die;
			if(!empty($_FILES['fileupload']['name'])){
			$POGUID 	=  $this->input->post('POGUID');
		    $PlantGUID	=  $this->input->post('PlantGUID');
		    $filesCount = count($_FILES['fileupload']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['fileupload']['name'] = $_FILES['fileupload']['name'][$i];
			
					$encrypted = $PlantGUID.$POGUID.'Report'.($i+1).".jpg"; 
				 	$tempFile = $_FILES['fileupload']['tmp_name'][$i];
					$targetPath = FCPATH . "datafiles/";
				 	$targetFile = $targetPath.$encrypted;

					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$uploadImage = $encrypted;
						$dataFileupload = array(
							'PlantGUID' => $PlantGUID,
							'DocumentName' => $uploadImage,
							'IsDeleted'  => 0,
						);

						$this->db->where('PlantGUID',$this->input->post('PlantGUID'));
						$this->db->insert('tblpatplantinstitutionaldocument',$dataFileupload); 
					}
			}
	}
		
			return 1;
			

		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	

	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['status_id']  = '2';
			$form['modify_by'] = 1;//$this->loggedIn;
			$form['modify_ip'] = $this->fromIP;
			$this->db->set('modify_date', 'NOW()', FALSE);
			$this->db->where('user_id',(int)$token);
			return ($this->db->update(PATPD,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	public function getPatAssestFunder($id)
	{
		$this->db->select('AssetFunderID');
		$this->db->where('PlantGUID',$id);
		return $this->db->get('tblpatplantassetfunder')->result();
	}
	
	
	/**
	 * Method getAgency() get all Agency.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAgency()
	{
		$this->db->select('AgencyID,AgencyName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AgencyID','asc');
		return $this->db->get(AGENCY)->result();
	}
		/**
	 * Method getAssessedby() get all USER.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssessedby()
	{
		$this->db->select('UserID,UserName,FirstName,LastName');
		$this->db->where('IsActive','1');
		$this->db->where('Isdeleted','0');
		$this->db->order_by('UserID','asc');
		return $this->db->get(USERS)->result();
	}
	
	
	/**
	 * Method getPlantSpecification() get all Plant Specification.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantSpecification()
	{
		$this->db->select('PlantSpecificationID,PlantSpecificationName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('PlantSpecificationID','asc');
		return $this->db->get(PLANSPECIFICATION)->result();
	}
	
	/**
	 * Method getPlantManufacturer() get all Plant Manufacturer.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantManufacturer()
	{
		$this->db->select('PlantManufacturerID,	PlantManufacturerName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('PlantManufacturerID','asc');
		return $this->db->get(PLANTMANUFACTURER)->result();
	}


		
	/**
	 * Method getPlantPurificationStep() get all Plant Purification Step.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantPurificationStep()
	{
		$this->db->select('PlantPurificationStepID,	PlantPurificationStepName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('PlantPurificationStepID','asc');
		return $this->db->get(PLANTPUR)->result();
	}
	
	
	
	public function getPlantPurStep($token)
	{
		$this->db->select('PlantPurificationstepID');
		$this->db->where('PlantGUID',$token);
	    $this->db->where('IsDeleted','0');
		$res =  $this->db->get('tblpatplantpurificationstep')->result();//echo $this->db->last_query();
		$PtPurstep = array();
		foreach($res as $row){
			$PtPurstep[]=$row->PlantPurificationstepID;  
		}

		return $PtPurstep;
	}
	
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 1.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunder6()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','6');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
		
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 1.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantFundedBy($token)
	{
		$this->db->select('AssetFunderID,FlagID');
		$this->db->where('PlantGUID',$token);
		return $this->db->get('tblpatplantassetfunder')->row(); //echo $this->db->last_query(); //die;
		
	}
	
	
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 2.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag2()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','2');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 3.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag3()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','3');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}

	/**
	 * Method getDistributionPointFlag11() get all Plant Purification Assest Funder Flag 3.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistributionPointFlag11()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','11');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}

	/**
	 * Method getDistance() get all Plant Purification Assest Funder Flag 3.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistance()
	{
		$this->db->select('DistanceID,DistanceValue');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('DistanceID','ASC');
		return $this->db->get('mstdpdistance')->result();
	}
	

	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 4.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag4()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','4');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 4.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag5()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','5');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getWaterQualityChallenge() get all Plant Purification Water Quality Challenge.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getWaterQualityChallenge()
	{
		$this->db->select('WaterQualityChallengeID,WaterQualityChallengeName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('WaterQualityChallengeID','asc');
		return $this->db->get(WATERQTCE)->result();
	}
	
	
	
	/**
	 * Method getPatPlantProdDetail() get all Plant Prod Detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPatPlantProdDetail($token)
	{
		
	  $sql = "Select `pppod`.`DistributionplantID`,`pppod`.`Price`,`pppod`.`Volume`,`pppod`.`HomeDeliveryPrice`,`ppd`.DPID, `ppd`.Distance, `ppd`.DPName from `tblpatplantproddetail` AS `pppod`
inner JOIN `tblpatplantdistribution` AS `ppd` ON `ppd`.`DPID`=`pppod`.`DistributionplantID`
				WHERE `pppod`.`PlantGUID` = '".$token."' GROUP BY `pppod`.`DistributionplantID`";
		return $this->Common_model->query_data($sql);
	}

	/**
	 * Method getPatPlantProdDetail() get all Plant Prod Detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistributionpoint($token)
	{
		
	 $sql = "Select DPID,Distance,PlantGUID from `tblpatplantdistribution` WHERE `PlantGUID` = '".$token."' ";
		return $this->Common_model->query_data($sql);
	}	
	
	/**
	 * Method getWaterQualityChallenge() get all Plant Purification Water Quality Challenge.
	 * @access	public
	 * @param	
	 * @return	array  tblpatwatercontaminants
	 */
	public function getPatWaterContaminants($token)
	{
		$this->db->select('WaterContaminantsID,WaterQualityChallengeID');
		
		$this->db->where('PlantGUID',$token);
		
	    $this->db->where('IsDeleted','0');
		
		$res = $this->db->get('tblpatwatercontaminants')->result(); 
		// echo $this->db->last_query();

		$ptwatercontm = array();
		foreach($res as $row){
			$ptwatercontm[] = $row->WaterQualityChallengeID;
		}

		return $ptwatercontm;
	}
	
	
	/**
	 * Method getDesignation() get all designation.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDesignation()
	{
		$this->db->select('DesignationID,DesignationName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('DesignationID','asc');
		return $this->db->get(DESIG)->result();
	}


	/**
	 * Method getDesignation() get all designation.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getLiteracy()
	{
		$this->db->select('LiteracyID,LiteracyName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('LiteracyID','asc');
		return $this->db->get('mstliteracy')->result();
	}
	
	
	/**
	 * Method getCountry() get all Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		$this->db->select('CountryID,CountryName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('CountryID','asc');
		//$this->db->get(COUNTRY)->result(); //echo  $this->db->last_query(); die;
		return $this->db->get(COUNTRY)->result();
	}
	
	/**
	 * Method getState() get all State.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getState()
	{
		$this->db->select('StateID,StateName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('StateID','asc');
		return $this->db->get(STATE)->result();
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistrict()
	{
		$this->db->select('DistrictID,DistrictName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('DistrictID','asc');
		return $this->db->get(DISTRICT)->result();
	}


	public function getDocument($token)
	{
		$this->db->select('PlantGUID,DocumentName,PlantInstitutionalDocumentID');
		$this->db->where('IsDeleted','0');
		$this->db->where('PlantGUID',$token);
		return $this->db->get('tblpatplantinstitutionaldocument')->result();
	}

	public function getAtPlantRevenue($token)
	{
		$query = "SELECT b.AssestFunderName as point, a.Price as price, a.Volume as volume, (a.Price * a.Volume) as total FROM `tblpatplantproddetail` a inner join mstassetfunder b on a.DistributionplantID = b.AssestFunderID where b.Flag = 10 and a.PlantGUID = '".$token."' and b.AssestFunderID = 1";

		$result = $this->db->query($query)->result();

		return $result;
	}

	public function getHomeDeliverRevenue($token)
	{
		$query = "SELECT b.AssestFunderName as point, a.Price as price, a.Volume as volume, (a.Price * a.Volume) as total FROM `tblpatplantproddetail` a inner join mstassetfunder b on a.DistributionplantID = b.AssestFunderID where b.Flag = 10 and a.PlantGUID = '".$token."' and b.AssestFunderID = 2";

		$result = $this->db->query($query)->result();

		return $result;
	}

	public function getDistributionPointRevenue($token)
	{
		$query = "SELECT CASE
					    c.DistributionplantID WHEN 99 THEN a.DPName ELSE b.AssestFunderName
					END AS point,
					c.Price AS price,
					c.Volume AS volume,
					(c.Price * c.Volume) AS total
					FROM
					    `tblpatplantdistribution` a
					INNER JOIN mstassetfunder b ON
					    a.DPID = b.AssestFunderID
					INNER JOIN tblpatplantproddetail c ON
					    a.PlantGUID = c.PlantGUID AND a.DPID AND c.DistributionplantID
					WHERE
					    a.PlantGUID = '".$token."' AND b.Flag = 11";

		$result = $this->db->query($query)->result();

		return $result;
	}
	
	
}