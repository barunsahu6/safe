<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(USERS);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(USERS)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/*public function listsold($limit,$start)
	{
		try {
			$this->db->select('CT.*');
			$this->db->order_by('CT.first_name','asc');
			$this->db->limit($limit,$start);
			return $this->db->get(USERS.' AS CT')->result();
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}*/
	
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			$password = $this->input->post('access_key', true);
		    $form['access_key'] = md5($form['access_key']);
			$form['added_by'] = $this->loggedIn;
			$form['added_ip'] = $this->fromIP;
			return ($this->db->insert(USERS,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function getDetail($token)
	{
		try {
			$this->db->select('*');
			$this->db->where('UserID',(int)$token);
			return $this->db->get(USERS)->row(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(USERS)->result(); 
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			$form = $this->input->post('form');
			//print_r($form);die;
			$form['ModifiedBy'] = $this->loggedIn;
			$form['ModifiedOn'] = $this->loggedDate;
			$this->db->set('ModifiedOn', 'NOW()', TRUE);
			$this->db->where('UserID',(int)$token);
			return ($this->db->update(USERS,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['IsActive']  = '0';
			$this->db->where('UserID',(int)$token);
			return ($this->db->update(USERS,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method name_validation() check duplicate name.
	 * @access	public
	 * @param	Null
	 * @return	array
	 */
	public function name_validation()
	{
		try {
			$form = $this->input->post('form');
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
			$user = isset($form['user_id']) ? $form['user_id'] : '0'; //print_r($form);die;
			$email = isset($form['email']) ? $form['email'] : ''; 
			
			$this->db->select("COUNT(1) AS CNT");
			$this->db->where('email',$email);
			if($token>0) {
				$this->db->where('user_id!='.(int)$token);	
			}
			$row = $this->db->get(USERS)->row();//print_r($row->CNT);die;//echo $this->db->last_query();die;
			if ($row->CNT > 0){
				$this->form_validation->set_message('name_validation', '{field} should be unique.');
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method username_validation() check duplicate username.
	 * @access	public
	 * @param	Null
	 * @return	array
	 */
	public function username_validation()
	{
		try {
			$form  = $this->input->post('form');
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
			$user  = isset($form['user_id']) ? $form['user_id'] : '0'; //print_r($form);die;
			$user  = isset($form['access_text']) ? $form['access_text'] : ''; 
			
			$this->db->select("COUNT(1) AS CNT");
			$this->db->where('access_text',$user);
			if($token>0) {
				$this->db->where('user_id!='.(int)$token);	
			}
			$row = $this->db->get(USERS)->row();//print_r($row->CNT);die;//echo $this->db->last_query();die;
			if ($row->CNT > 0){
				$this->form_validation->set_message('username_validation', '{field} should be unique.');
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getTypes() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getLevel()
	{
		$this->db->select('RoleID,RoleName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('RoleID','asc');
		return $this->db->get(LEVEL)->result();
	}
}