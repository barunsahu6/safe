<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class FVTForm_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(PATPD);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(PATPD)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
		
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			$password = $this->input->post('access_key', true);
		   
			return ($this->db->insert(PATPD,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	
	public function getDetail($token)
	{
		try {
	$this->db->select('tbltstsampletestresult.*,tbltstsampleinfo.CollectedBy,tbltstsampleinfo.CollectedDate,
		tbltstsampleinfo.CollectedTime,tbltstsampleinfo.Address,tbltstsampleinfo.Mobile,tbltstsampleinfo.ContainerType,
		tbltstsampleinfo.Quantity,tbltstsampleinfo.AgencyName,tbltstsampleinfo.CourieredDate,tbltstsampleinfo.CourieredTime,tbltstsampleinfo.Contact,tbltstlaboratorydetails.LabName,tbltstlaboratorydetails.Address,tbltstlaboratorydetails.ReportID,tbltstlaboratorydetails.ReportDate,tbltstlaboratorydetails.Contact,tbltstlaboratorydetails.LabTestReport,
		tbltstlaboratorydetails.UserType,tbltstlocation.CountryID,tbltstlocation.StateID,tbltstlocation.DistrictID,
		tbltstlocation.BlockName,tbltstlocation.VillageName,tbltstlocation.SourceID,tbltstlocation.WeatherID,
		tbltstlocation.GroundWaterDepth,tbltstlocation.Xvalue,tbltstlocation.Yvalue,tbltstlocation.PhotoLink,
		tbltstlocation.IsWasteHeatAvailable,tbltstlocation.IsElectricityAvailable,tbltstlocation.UserID,
		tbltstlocation.CityName,tbltstlocation.SeasonID');
	$this->db->where(tbltstsampletestresult.'.LocationGUID',$token);
	$this->db->join(tbltstlocation, tbltstlocation.'.GUID ='.tbltstsampletestresult.'.LocationGUID');
	$this->db->join(tbltstsampleinfo, tbltstsampleinfo.'.SampleGUID ='.tbltstsampletestresult.'.SampleGUID');
	$this->db->join(tbltstlaboratorydetails, tbltstlaboratorydetails.'.LabGUID ='.tbltstsampletestresult.'.LabGUID');
	 return $this->db->get(tbltstsampletestresult)->row();   //echo $this->db->last_query(); die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('tbltstlocation.GUID,tbltstlocation.BlockName,tbltstlocation.VillageName,mststate.StateName,mstdistrict.DistrictName');
			$this->db->join(tbltstsampleinfo, tbltstsampleinfo.'.LocationGUID ='.tbltstlocation.'.GUID');
			$this->db->join(mstcountry, mstcountry.'.CountryID ='.tbltstlocation.'.CountryID');
			$this->db->join(mststate, mststate.'.StateID ='.tbltstlocation.'.StateID');
			$this->db->join(mstdistrict, mstdistrict.'.DistrictID ='.tbltstlocation.'.DistrictID');
			return $this->db->get(tbltstlocation)->result(); // echo $this->db->last_query(); die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			//echo $token;
			$form = $this->input->post();
			//echo "<pre>";
			//print_r($this->input->post()); 
			
			$CourieredDateDBConvert =  date("Y-m-d", strtotime($this->input->post('Coriered_CourieredDate')));
			$CollectedDateDBConvert =  date("Y-m-d", strtotime($this->input->post('WSL_CollectedDate')));
			$testresultdateDBConvert =  date("Y-m-d", strtotime($this->input->post('testresultdate')));

		$data_sampleinfo=array(
								'CollectedBy' 			=> $this->input->post('CollectedBy'),
								'CollectedDate'  		=>  $CollectedDateDBConvert,
								'CollectedTime'  		=>  $this->input->post('CollectedTime'),
								'Address'  				=>  $this->input->post('WSL_Address'),
								'Mobile'				=> $this->input->post('Collected_Mobile'),
								'ContainerType'  		=>  $this->input->post('Sent_ContainerType'),
								'Quantity'  			=>  $this->input->post('Sent_Quantity'),
								'AgencyName' 			=> $this->input->post('Coriered_AgencyName'),
								'CourieredDate'  		=>  $this->input->post('Coriered_CourieredDate'),
								'CourieredTime'  		=>  $this->input->post('Coriered_CourieredTime'),	
								'Contact' 				=> $this->input->post('Coriered_Contact'),
						);

			$this->db->where('SampleGUID',$this->input->post('SampleGUID'));
			$this->db->update(tbltstsampleinfo,$data_sampleinfo);

////////////////////////////  Upload  Document //////////////
			
			if(isset($_FILES["fileupload"]) && $_FILES["fileupload"]!='' && $_FILES["fileupload"]["error"] == 0){
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
			 	$filename = $_FILES["fileupload"]["name"];
			 	$filetype = $_FILES["fileupload"]["type"];
				$filesize = $_FILES["fileupload"]["size"];
				$target   =	$this->input->post('LabGUID');	
				$uptarget = $target."_".$filename;		
				// Verify file extension
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
			
				// Verify MYME type of the file
				if(in_array($filetype, $allowed)){
					// Check whether file exists before uploading it
					if(file_exists("datafiles/" . $_FILES["fileupload"]["name"])){
						echo $_FILES["fileupload"]["name"] . " is already exists.";

	$this->session->set_flashdata('er_msg', $_FILES["fileupload"]["name"].' is already exists!');
					} else{
					move_uploaded_file($uptarget, "datafiles/" . $uptarget);
					} 
				} else{
					
			$this->session->set_flashdata('er_msg', ' There was a problem uploading your file. Please try again!');
					//echo "Error: There was a problem uploading your file. Please try again."; 
				}
			} else{
				
				$uptarget = $this->input->post('OldImage');
			}
			$dataFileupload = array(
				'PlantGUID' => $token,
				'DocumentName' => $filename,
				'IsDeleted'  => 0,
			 );

			$data_location=array(
								'CountryID' 			 => $this->input->post('WSL_CountryID'),
								'StateID'  				 =>  $this->input->post('WSL_StateID'),
								'DistrictID'  			 =>  $this->input->post('WSL_DistrictID'),
								'BlockName'  			 =>  $this->input->post('WSL_BlockName'),
								'VillageName' 			 => $this->input->post('WSL_VillageName'),
								'SourceID'  		 	 =>  $this->input->post('WSC_SourceID'),
								'WeatherID'  			 =>  $this->input->post('WSC_Weather'),
								'GroundWaterDepth' 		 => $this->input->post('WSC_GWDft'),
								'Xvalue'  				 =>  $this->input->post('Xvalue'),
								'Yvalue'  				 =>  $this->input->post('Yvalue'),
								'PhotoLink'				 =>  $uptarget,
								'IsWasteHeatAvailable'   => $this->input->post('IsWasteHeatAvailable'),
								'IsElectricityAvailable' => $this->input->post('Electricity'),
     							'SeasonID' 				 => $this->input->post('WSC_Season'),
						
						);

			$this->db->where('GUID',$this->input->post('LocationGUID'));
		    $this->db->update(tbltstlocation,$data_location);
		  // echo $this->db->last_query(); 

			$data_laboratorydetails=array(
								'LabName' 			 	 => $this->input->post('Conducted_LaboratoryName'),
								'Address'  				 => $this->input->post('Conducted_Address'),
								'ReportID'  			 => $this->input->post('Conducted_ReportID'),
								'ReportDate'  			 => $testresultdateDBConvert,
								'Contact' 			     => $this->input->post('Coriered_Contact'),
								'UserType'  			 => $this->input->post('Conducted_UserType'),	
						);

			$this->db->where('LabGUID',$this->input->post('LabGUID'));
			$this->db->update(tbltstlaboratorydetails,$data_laboratorydetails);
			//echo $this->db->last_query(); die;
			//echo "AMIT";
			//die;
		return 1;	 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['status_id']  = '2';
			$form['modify_by'] = 1;//$this->loggedIn;
			$form['modify_ip'] = $this->fromIP;
			$this->db->set('modify_date', 'NOW()', FALSE);
			$this->db->where('user_id',(int)$token);
			return ($this->db->update(PATPD,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	


/**
	 * Method getArray() get all Array.
	 * @access	public
	 * @param	
	 * @return	array CONTAINERTYPE
	 */
	public function getArray()
	{
		try {
			return array(1=>'Available',2=>'not Available');
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getSeason() get all Season.
	 * @access	public
	 * @param	
	 * @return	array CONTAINERTYPE
	 */
	public function getSeason()
	{
		try {
			$this->db->select('SeasonID,SeasonName');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('SeasonID','asc');
			return $this->db->get(mstseason)->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	/**
	 * Method getWeather() get all Weather.
	 * @access	public
	 * @param	
	 * @return	array CONTAINERTYPE
	 */
	public function getWeather()
	{
		try {
				$this->db->select('WeatherID,WeatherName');
				$this->db->where('IsDeleted','0');
				$this->db->order_by('WeatherID','asc');
				return $this->db->get(mstweather)->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
/**
	 * Method getSourceType() get all SourceType.
	 * @access	public
	 * @param	
	 * @return	array CONTAINERTYPE
	 */
	public function getSourceType()
	{
			try {
					$this->db->select('SourceID,SourceName');
					$this->db->where('IsDeleted','0');
					$this->db->order_by('SourceID','asc');
					return $this->db->get(mstsource)->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}


	/**
	 * Method getContainertype() get all Containertype.
	 * @access	public
	 * @param	
	 * @return	array CONTAINERTYPE
	 */
	public function getContainertype()
	{
	try {
			$this->db->select('ContainerID,ContainerType');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('ContainerID','asc');
			return $this->db->get(mstcontainertype)->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	
	/**
	 * Method getCountry() get all Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try{
			$this->db->select('CountryID,CountryName');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('CountryID','asc');
			return $this->db->get(COUNTRY)->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getState() get all State.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getState()
	{
		try {
			$this->db->select('StateID,StateName');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('StateID','asc');
			return $this->db->get(STATE)->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistrict()
	{
		try{
			$this->db->select('DistrictID,DistrictName');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('DistrictID','asc');
			return $this->db->get(DISTRICT)->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	
}