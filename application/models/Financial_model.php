<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Financial_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	/**
	 * Method getDashboardPlantAndAge() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardYearWiseFunder($strwhr=Null,$strstatewhr=Null,$districtwhr=Null,$plantwhr=Null){
	
		try{
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = "SELECT
    '2013' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE
    YEAR(`tpp`.`VisitDate`) = 2013 GROUP BY YEAR(`tpp`.`VisitDate`) ";

    $sql2 = "SELECT
    '2014' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE
    YEAR(`tpp`.`VisitDate`) = 2014 GROUP BY YEAR(`tpp`.`VisitDate`)";

    $sql3 ="SELECT
    '2015' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE
    YEAR(`tpp`.`VisitDate`) = 2015 GROUP BY YEAR(`tpp`.`VisitDate`)";


    $sql4 = "SELECT
    '2016' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE
    YEAR(`tpp`.`VisitDate`) = 2016 GROUP BY YEAR(`tpp`.`VisitDate`)";


    $sql5 ="SELECT
    '2017' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE
    YEAR(`tpp`.`VisitDate`) = 2017 GROUP BY YEAR(`tpp`.`VisitDate`)";


			}
			else
			{
				$sql1 = "SELECT
    '2013' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2013
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE
    YEAR(`tpp`.`VisitDate`) = 2013 GROUP BY YEAR(`tpp`.`VisitDate`) ";

    $sql2 = "SELECT
    '2014' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2014
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE
    YEAR(`tpp`.`VisitDate`) = 2014 GROUP BY YEAR(`tpp`.`VisitDate`)";

    $sql3 ="SELECT
    '2015' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2015
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE
    YEAR(`tpp`.`VisitDate`) = 2015 GROUP BY YEAR(`tpp`.`VisitDate`)";


    $sql4 = "SELECT
    '2016' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2016
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE
    YEAR(`tpp`.`VisitDate`) = 2016 GROUP BY YEAR(`tpp`.`VisitDate`)";



    $sql5 ="SELECT
    '2017' AS label,
    (
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
        ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'HONEYWELL' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS HONEYWELL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
        `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'PEPSICO' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS PEPSICO,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'TATA' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS TATA,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
       `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
       `ppd`.`MachineryOther` = 'BHEL' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS BHEL,
(
    SELECT
        COUNT(`ppd`.`MachineryOther`)
    FROM
          `tblpatplantdetail` as `ppd`
        INNER JOIN 	`tblpatpodetail` as `tpp`
      ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
    WHERE
        `ppd`.`MachineryOther` = 'NRTT' AND YEAR(`tpp`.`VisitDate`) = 2017
) AS NRTT
FROM
    `tblpatplantdetail` AS `ppd`
     INNER JOIN `tblpatpodetail` as `tpp`
       ON `tpp`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE
    YEAR(`tpp`.`VisitDate`) = 2017 GROUP BY YEAR(`tpp`.`VisitDate`)";
			}
			
			
						
	if($strwhr != "")
	{
		$sql1 .= " AND  `ppd`.CountryID ='".$strwhr."' "; 
	}
	if($strstatewhr != ""){
		$sql1 .= " AND  `ppd`.`StateID` = '".$strstatewhr."' "; 
	}
	if($districtwhr != ""){
		$sql1 .= " AND  `ppd`.`District` = '".$districtwhr."' "; 
	}
	if($plantwhr != ""){
		$sql1 .= " AND  `ppd`.`PlantGUID` = '".$plantwhr."' "; 
	}

	//echo $sql1;					
						
		

			if($strwhr != "")
			{
				$sql2 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql2 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql2 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql2 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}
					
		
			if($strwhr != "")
			{
				$sql3 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql3 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql3 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql3 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}

			
			if($strwhr != "")
			{
				$sql4 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql4 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql4 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql4 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}

		
			if($strwhr != "")
			{
				$sql5 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql5 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql5 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql5 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 

			}
			
		 $sql ="$sql1 union $sql2 union $sql3 union $sql4 union $sql5 ";
		// echo $sql; //die();
		 $data = $this->db->query($sql)->result(); //echo last_query(); die;
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	public function getFinancialCOPEX($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql = "SELECT count(b.OPEx) as opex, count(b.ServiceCharge) as service,  count(b.AssestRepaymentFund) as ARF
				FROM tblpatplantproddetail a 
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`OPEx` AS `OPEx`,
					`tblpatpodetail`.`ServiceCharge` AS `ServiceCharge`,
					`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`				
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as	b ON a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON b.PlantGUID = c.PlantGUID INNER JOIN honeywell_patplantdetail ON c.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE 1=1";
			}
			else
			{
				$sql = "SELECT count(b.OPEx) as opex, count(b.ServiceCharge) as service,  count(b.AssestRepaymentFund) as ARF
				FROM tblpatplantproddetail a 
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` 
					AS `PlantGUID`,
					max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,
					`tblpatpodetail`.`OPEx` AS `OPEx`,
					`tblpatpodetail`.`ServiceCharge` AS `ServiceCharge`,
					`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`				
					from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as	b ON a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON b.PlantGUID = c.PlantGUID WHERE 1=1";
			}
		
				
					if($strwhr != "")
					{
						$sql .= " AND c.CountryID ='".$strwhr."' "; 
					}
					if($strstatewhr != ""){
						$sql .= " AND c.`StateID` = '".$strstatewhr."' "; 
					}
					if($districtwhr != ""){
						$sql .= " AND c.District = '".$districtwhr."' "; 
					}
					if($plantwhr != ""){
						$sql .= " AND c.PlantGUID = '".$plantwhr."' "; 
					}
						

			// echo $sql; die();
			$data = $this->db->query($sql)->result()[0]; //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
            

		}catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

	//////////////////////// 	 Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChart($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = "SELECT 'Government' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE `ppa`.`AssetFunderID`=1 AND `ppa`.`FlagID`=6";

				$sql2 = "SELECT 'Grant' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE `ppa`.`AssetFunderID`=2 AND `ppa`.`FlagID`=6"; 

				$sql3 = "SELECT 'Self Funded (Individual/Group)' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE `ppa`.`AssetFunderID`=3 AND `ppa`.`FlagID`=6";
				
				$sql4 = "SELECT 'SWE Franchisor' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE `ppa`.`AssetFunderID`=4 AND `ppa`.`FlagID`=6";

				$sql5 = "SELECT 'Bank loan' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID` INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
WHERE `ppa`.`AssetFunderID`=5 AND `ppa`.`FlagID`=6 ";
			}
			else
			{
				$sql1 = "SELECT 'Government' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE `ppa`.`AssetFunderID`=1 AND `ppa`.`FlagID`=6";

				$sql2 = "SELECT 'Grant' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE `ppa`.`AssetFunderID`=2 AND `ppa`.`FlagID`=6"; 

				$sql3 = "SELECT 'Self Funded (Individual/Group)' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE `ppa`.`AssetFunderID`=3 AND `ppa`.`FlagID`=6";

				$sql4 = "SELECT 'SWE Franchisor' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE `ppa`.`AssetFunderID`=4 AND `ppa`.`FlagID`=6";

				$sql5 = "SELECT 'Bank loan' as label, COUNT(`ppa`.`AssetFunderID`) AS VALUE FROM `tblpatplantdetail` AS `ppd`
inner JOIN `tblpatplantassetfunder` as `ppa` ON `ppa`.`PlantGUID` = `ppd`.`PlantGUID`
WHERE `ppa`.`AssetFunderID`=5 AND `ppa`.`FlagID`=6 ";
			}
		
		
			
			if($strwhr != "")
			{
			$sql1 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql1 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql1 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql1 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}
	
		
		if($strwhr != "")
			{
			$sql2 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql2 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql2 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql2 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}

	
		
		
			if($strwhr != "")
			{
			$sql3 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql3 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql3 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql3 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}
		
			
		
		if($strwhr != "")
			{
			$sql4 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql4 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql4 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql4 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}
		
		
		  if($strwhr != "")
			{
			$sql5 .= " AND `ppd`.`CountryID` ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql5 .= " AND `ppd`.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql5 .= " AND `ppd`.`District` = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql5 .= " AND `ppd`.`PlantGUID` = '".$plantwhr."' "; 
			}

		$sql = "$sql1 union $sql2 union $sql3 union $sql4 union $sql5 ";	
		// echo $sql; die();
		$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); die;
		return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	

public function getfourthOpex($strwhr,$strstatewhr,$districtwhr,$plantwhr){

	try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = "SELECT
					'revenueOpex' as lable, count(c.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID INNER JOIN honeywell_patplantdetail ON c.PlantGUID = honeywell_patplantdetail.PlantGUID 
				WHERE
					round((a.Price * a.Volume)) < b.OPEx ";

				$sql2 = "SELECT
					'OpexRevenueOPExservicecharge' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID INNER JOIN honeywell_patplantdetail ON c.PlantGUID = honeywell_patplantdetail.PlantGUID 
				WHERE
				b.OPEx<round((a.Price * a.Volume)) < b.OPExservicecharge ";

				$sql3="	SELECT
					'OpexOPExserviceRevenueARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID INNER JOIN honeywell_patplantdetail ON c.PlantGUID = honeywell_patplantdetail.PlantGUID 
				WHERE
				(b.OPEx + b.OPExservicecharge)<round((a.Price * a.Volume)) < (b.OPExservicecharge + b.AssestRepaymentFund) ";

				$sql4 = "SELECT
					'RevenueOpexOPExserviceARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)			
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID INNER JOIN honeywell_patplantdetail ON c.PlantGUID = honeywell_patplantdetail.PlantGUID 
				WHERE
				round((a.Price * a.Volume))>(b.OPEx + b.OPExservicecharge+b.AssestRepaymentFund) ";
			}
			else
			{
				$sql1 = "SELECT
					'revenueOpex' as lable, count(c.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
					round((a.Price * a.Volume)) < b.OPEx ";

				$sql2 = "SELECT
					'OpexRevenueOPExservicecharge' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				b.OPEx<round((a.Price * a.Volume)) < b.OPExservicecharge ";

				$sql3="	SELECT
					'OpexOPExserviceRevenueARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)				
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				(b.OPEx + b.OPExservicecharge)<round((a.Price * a.Volume)) < (b.OPExservicecharge + b.AssestRepaymentFund) ";

				$sql4 = "SELECT
					'RevenueOpexOPExserviceARF' as lable, count(a.PlantGUID) as 'value'
				FROM
					tblpatplantproddetail a
				INNER JOIN 
				(select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`OPExservicecharge` AS `OPExservicecharge`
,`tblpatpodetail`.`AssestRepaymentFund` AS `AssestRepaymentFund`
,`tblpatpodetail`.`OPEx` AS `OPEx` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`)			
				as b ON
					a.PlantGUID = b.PlantGUID AND a.POGUID = b.POGUID
				INNER JOIN tblpatplantdetail c ON
					b.PlantGUID = c.PlantGUID
				WHERE
				round((a.Price * a.Volume))>(b.OPEx + b.OPExservicecharge+b.AssestRepaymentFund) ";
			}
		
		
		
					 if($strwhr != "")
						{
						$sql1 .= " AND c.CountryID ='".$strwhr."' "; 
						}
						if($strstatewhr != ""){
							$sql1 .= " AND c.`StateID` = '".$strstatewhr."' "; 
						}
						if($districtwhr != ""){
							$sql1 .= " AND c.District = '".$districtwhr."' "; 
						}
						if($plantwhr != ""){
							$sql1 .= " AND c.PlantGUID = '".$plantwhr."' "; 
						}
					
					
					
				
				
				if($strwhr != "")
				{
					$sql2 .= " AND c.CountryID ='".$strwhr."' "; 
				}
				if($strstatewhr != ""){
					$sql2 .= " AND c.`StateID` = '".$strstatewhr."' "; 
				}
				if($districtwhr != ""){
					$sql2 .= " AND c.District = '".$districtwhr."' "; 
				}
				if($plantwhr != ""){
					$sql2 .= " AND c.PlantGUID = '".$plantwhr."' "; 
				}
					
			

			
				if($strwhr != "")
				{
					$sql3 .= " AND c.CountryID ='".$strwhr."' "; 
				}
				if($strstatewhr != ""){
					$sql3 .= " AND c.`StateID` = '".$strstatewhr."' "; 
				}
				if($districtwhr != ""){
					$sql3 .= " AND c.District = '".$districtwhr."' "; 
				}
				if($plantwhr != ""){
					$sql3 .= " AND c.PlantGUID = '".$plantwhr."' "; 
				}

				
				
				if($strwhr != "")
				{
					$sql4 .= " AND c.CountryID ='".$strwhr."' "; 
				}
				if($strstatewhr != ""){
					$sql4 .= " AND c.`StateID` = '".$strstatewhr."' "; 
				}
				if($districtwhr != ""){
					$sql4 .= " AND c.District = '".$districtwhr."' "; 
				}
				if($plantwhr != ""){
					$sql4 .= " AND c.PlantGUID = '".$plantwhr."' "; 
				}
			$sql = "$sql1 union $sql2 union $sql3 union $sql4";
			//echo $sql;
			$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
	}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

}
   

/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {

      $login_data = $this->session->userdata('login_data');
      if($login_data['ROLE_ID'] == 9 )
      {
        $this->db->select('b.CountryID, b.CountryName');
        $this->db->from('tblpatplantdetail a');
        $this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
        $this->db->where('IsDeleted','0');
        $this->db->where('b.CountryID','1');
        $this->db->group_by('b.CountryID');
        $this->db->order_by('b.CountryID','asc');
        return $this->db->get()->result();
      }
      else
      {

			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
    }
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
      $login_data = $this->session->userdata('login_data');
      if($login_data['ROLE_ID'] == 9 )
      {
        $this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
        $this->db->from('tblpatplantdetail a');
        $this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
        $this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
        $this->db->where('c.StateID', 216);
        $this->db->group_by('c.StateID');
        $this->db->order_by('c.StateID','asc');
        return $this->db->get()->result();
      }
      else
      {

			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			}

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
      $login_data = $this->session->userdata('login_data');
      if($login_data['ROLE_ID'] == 9 )
      {
        $this->db->select('c.StateID,c.StateName');
        $this->db->from('mststate c');
        $this->db->where('c.CountryID',$id);
        $this->db->where('c.StateID', 216);
        $this->db->group_by('c.StateID');
        $this->db->order_by('c.StateName','asc');
        
        return $this->db->get()->result();
      }
      else
      {

			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
    }
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}