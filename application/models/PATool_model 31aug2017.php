<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PATool_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(PATPD);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(PATPD)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
		
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			$password = $this->input->post('access_key', true);
		   
			return ($this->db->insert(PATPD,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	
	public function getDetail($token)
	{
		try {
			$this->db->select('*');
			//$this->db->where(PATPLANDETAIL.'.PlantGUID',$token);
			$this->db->where(PATPLANDETAIL.'.PlantGUID',$token);
			$this->db->from(PATPLANDETAIL);
			$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID','INNER');
			$this->db->join(USERS, USERS.'.UserID ='.PATPLANDETAIL.'.UserID','INNER');
			$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID','INNER');
			$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID','INNER');
			$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District','LEFT');
			$this->db->join(tblpatplantinstitutionaldocument, tblpatplantinstitutionaldocument.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID','LEFT');
			$this->db->join(DISTRIBUTION, DISTRIBUTION.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID','LEFT');
		    $result = $this->db->get()->row(); //echo $this->db->last_query(); die;
			return $result;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(PATPD)->result(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			//echo $token;
			//echo "<pre>";
			//print_r($_POST); 
			//die;
			
			$form = $this->input->post();
		
			$visitDateDBConvert =  date("Y-m-d", strtotime($this->input->post('VisitDate1')));
			$EstablishmentDateDBConvert =  date("Y-m-d", strtotime($this->input->post('EstablishmentDate')));
			$count = count($this->input->post('WQC'));
			$WQC = $this->input->post('WQC');
			if($count >0){
				$this->db->delete('tblpatwatercontaminants', array('PlantGUID' => $token)); //$this->db->last_query();die;
			}
		     for($i=0; $i < $count; $i++){
				 
				$data_array[$i]=array(
				'WaterQualityChallengeID' => $WQC[$i],
				'PlantGUID' => $token,
				);
				 ($this->db->insert(PATWCS,$data_array[$i])) ? 1 : -1; //echo $this->db->last_query();//die;

			}
			$PlantPurificationStepcount = count($this->input->post('PlantPurificationStep'));
			$PlantPurificationStep =$this->input->post('PlantPurificationStep');
			if($PlantPurificationStepcount >0){
				$this->db->delete('tblpatplantpurificationstep', array('PlantGUID' => $token)); //$this->db->last_query();die;
			}
		     for($i=0; $i < $PlantPurificationStepcount; $i++){
				 
				$data_array[$i]=array(
				'PlantPurificationstepID' => $PlantPurificationStep[$i],
				'PlantGUID' => $token,
				);
				 ($this->db->insert(PATPFCTS,$data_array[$i])) ? 1 : -1; 
			}
			//echo $this->db->last_query();die;

			if(!empty($this->input->post('grant'))){
				$MachineryOthergrant = $this->input->post('grant');
			}else{
				$MachineryOthergrant = $this->input->post('MachineryOther');
			}

			if(!empty($this->input->post('landother'))){
				$LandOther_anyother = $this->input->post('landother');
			}else{
				$LandOther_anyother = $this->input->post('LandOther');
			}

			if(!empty($this->input->post('BuildinganOther'))){
				$BuildinganOther_anyother = $this->input->post('BuildinganOther');
			}else{
				$BuildinganOther_anyother = $this->input->post('BuildingOther');
			}
			if(!empty($this->input->post('RawWaterOther'))){
				$RawWaterOther_anyother = $this->input->post('RawWaterOther');
			}else{
				$RawWaterOther_anyother = $this->input->post('RawWaterSourceOther');
			}
			if(!empty($this->input->post('ElectricityanyOther'))){
				$Electricity_anyother = $this->input->post('ElectricityanyOther');
			}else{
				$Electricity_anyother = $this->input->post('ElectricityOther');
			}


		
			$data2=array(
								'VisitDate' 		=> $visitDateDBConvert,
								'MailingAddress'    =>  $this->input->post('MailingAddress'),
								'PlantLocation'     =>  $this->input->post('PlantLocation'),
								'PlantLatitude'     => $this->input->post('PlantLatitude'),
								'PlantPhotoLink'    =>  $this->input->post('PlantPhotoLink'),
								'UserID'  			=>  $this->input->post('UserID'),
								'AgencyID' 			=> $this->input->post('AgencyID'),
								'LocationType'  	=>  $this->input->post('LocationType'),
								'CountryID'  		=>  $this->input->post('Country'),
								'StateID' 			=> $this->input->post('State'),
								'District'  		=>  $this->input->post('District'),
								'BlockName'  		=>  $this->input->post('BlockName'),	
								'VillageName' 		=> $this->input->post('VillageName'),
								'PinCode'  			=>  $this->input->post('PinCode'),
								'SC' 				=> $this->input->post('SC'),
								'ST'  				=>  $this->input->post('ST'),
								'OBC'  				=>  $this->input->post('OBC'),
								'General' 			=> $this->input->post('General'),
								'AllInclusion' 		=>  $this->input->post('AllInclusion'),
								'OperatorName'  	=>  $this->input->post('OperatorName'),
								'DesignationID' 	=> $this->input->post('DesignationID'),
								'ContactNumber'  	=>  $this->input->post('ContactNumber'),
								'LiteracyID'  		=>  $this->input->post('LiteracyID'),
								'PlantSpecificationID' => $this->input->post('PlantSpecificationID'),
								'PlantManufacturerID'  =>  $this->input->post('PlantManufacturerID'),
								'ManufacturerName'  =>  $this->input->post('ManufacturerName'),	
								'EstablishmentDate' => $EstablishmentDateDBConvert,
								'AgeOfPlant'  		=>  $this->input->post('AgeOfPlant'),
								'RemoteMonitoringSystem' => $this->input->post('RemoteMonitoringSystem'),
								'LandCost' 		 	=>  $this->input->post('LandCost1'),
								'LandOther'  		=> $this->input->post('LandOther'),
								'MachineryCost' 	=> $this->input->post('MachineryCost'),
								'MachineryOther'  	=>  $MachineryOthergrant,
								'BuildingCost' 	 	=>  $this->input->post('BuildingCost1'),
								'BuildingOther' 	=> $BuildinganOther_anyother,
								'RawWaterSourceCost'  =>  $this->input->post('RawWaterSourceCost'),
								'RawWaterSourceOther'  =>  $RawWaterOther_anyother,
								'PlantSpecificationID' => $this->input->post('PlantSpecificationID'),
								'PlantManufacturerID'  =>  $this->input->post('PlantManufacturerID'),
								'ElectricityCost'  		=>  $this->input->post('ElectricityCost1'),	
								'ElectricityOther' 		=> $Electricity_anyother,
								'Distribution'  		=>  $this->input->post('Distribution'),
								'NoOfhhregistered' 		=> $this->input->post('NoOfhhregistered'),
								'AvgMonthlyCards'  		=>  $this->input->post('AvgMonthlyCards'),
								'GramPanchayatApproval'  =>  $this->input->post('GramPanchayatApproval'),
								'LegalElectricityConnection' => $this->input->post('LegalElectricityConnection'),
								'LandApproval'  			=>  $this->input->post('LandApproval'),
								'RawWaterSourceApproval'  	=>  $this->input->post('RawWaterSourceApproval'),
								'RejectWaterDischargeApproval' => $this->input->post('RejectWaterDischargeApproval'),
								'ServiceProviderApproval' 	=>  $this->input->post('ServiceProviderApproval'),
								'OperatorQualification'  	=>  $this->input->post('OperatorQualification'),
								'WaterBrandName' 			=> $this->input->post('WaterBrandName'),
								'PlantSpecificationAnyOther'  =>  $this->input->post('PlantSpecificationAnyOther'),
								'PAssetSourceFundedBy' 	 	=>  $this->input->post('PAssetSourceFundedBy'),	
								'NoOfHousehold' 			=> $this->input->post('NoOfHousehold'),
								'NoOfHouseholdWithin2km'  	=>  $this->input->post('NoOfHouseholdWithin2km'),
								'BPL'  						=>  $this->input->post('BPL'),
								'auditAgencyOther'  		=>  $this->input->post('auditAgencyOther'),
								'EducationAnyOther'  		=>  $this->input->post('EducationAnyOther'),
								'ContaminantAnyOther' 		=> $this->input->post('ContaminantAnyOther'),
								'Population'  				=>  $this->input->post('Population'),
						);
						//echo "<pre>";
						//print_r($data2); die;

			$this->db->where('PlantGUID',$this->input->post('PlantGUID'));
			//$this->db->where('PlantUID',$this->input->post('PlantUID'));


			$resultDetail =($this->db->update(PATPLANDETAIL,$data2)) ? 1 : -1;
			//echo $this->db->last_query(); die;
			
			//if($resultDetail==1){

					if($this->input->post('PreMonsoonRWTesting')=='PRERW'){
							$PreMonsoonRWTestingcheck =1;
					}
					if($this->input->post('PreMonsoonTWTesting')=='PRETWT'){
							$PreMonsoonTWTestingcheck =1;
					}
					if($this->input->post('PostMonsoonRWTesting')=='POSTRW'){
							$PostMonsoonRWTestingcheck =1;
					}
					if($this->input->post('PostMonsoonTWtesting')=='POSTTWT'){
							$PostMonsoonTWtestingcheck =1;
					}
					if($this->input->post('RejectWaterTesting')=='REJWT'){
							$RejectWaterTestingcheck =1;
					}
				$PreMonsoonRWTestingdateDBConvert 	=  date("Y-m-d", strtotime($this->input->post('PreMonsoonRWTestingdate')));
				$PreMonsoonTWTestingdateDBConvert 	=  date("Y-m-d", strtotime($this->input->post('PreMonsoonTWTestingdate')));
				$PostMonsoonRWtestingdateDBConvert  =  date("Y-m-d", strtotime($this->input->post('PostMonsoonRWtestingdate')));
				$PostMonsoonTWtestingdateDBConvert  =  date("Y-m-d", strtotime($this->input->post('PostMonsoonTWtestingdate')));
				$RejectWatertestingdateDBConvert    =  date("Y-m-d", strtotime($this->input->post('RejectWatertestingdate')));
			
				//$PreMonsoonRWTestProof = $_FILES['PreMonsoonRWTestProof']['name'];	//die;
				if(!empty($_FILES['PreMonsoonRWTestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PreMonsoonRWTestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$PreMonsoonRWTestProofimage = $encryptedName;
					}
				}else{
				    $PreMonsoonRWTestProofimage = $this->input->post('oldPreMonsoonRWTestProof');
				}

				//die;

				//$PreMonsoonTWProof = $_FILESS['PreMonsoonTWProof']['name'];	
				if(!empty($_FILES['PreMonsoonTWProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PreMonsoonTWProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$PreMonsoonTWProofimage = $encryptedName;
					}

				}else{
					$PreMonsoonTWProofimage = $this->input->post('oldPreMonsoonTWProof');
				}

				//echo $_FILESS['PostMonsoonRWTestProof']['name']; die;
				// $PostMonsoonRWTestProof = $_FILESS['PostMonsoonRWTestProof']['name'];	//die;
				if(!empty($_FILES['PostMonsoonRWTestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PostMonsoonRWTestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						 $PostMonsoonRWTestProofimage = $encryptedName;
					}
				}else{
					 $PostMonsoonRWTestProofimage = $this->input->post('oldPostMonsoonRWTestProof');
				}
				//die;
				//$PostMonsoonTWtestProof = $_FILESS['PostMonsoonTWtestProof']['name'];	
				if(!empty($_FILES['PostMonsoonTWtestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['PostMonsoonTWtestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$PostMonsoonTWtestProofimage = $encryptedName;
					}
				}else{
					$PostMonsoonTWtestProofimage = $this->input->post('oldPostMonsoonTWtestProof');
				}
			
				//$RejectWaterTestProof = $_FILESS['RejectWaterTestProof']['name'];	
				if(!empty($_FILES['RejectWaterTestProof']['name'])){
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)); 
					$tempFile = $_FILES['RejectWaterTestProof']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile). ".jpg";
					if($uploadResult == true){
						$RejectWaterTestProofimage = $encryptedName;
					}
				}else{
					$RejectWaterTestProofimage = $this->input->post('oldRejectWaterTestProof');
				}

				$data=array(
						'AuditedBy'  				=>  $this->input->post('AuditedBy'),
                        'Latitude'  				=>  $this->input->post('Latitude'),
						'Longitude' 				=> $this->input->post('Longitude'),
						'AgencyAnyOther'  			=>  $this->input->post('AgencyAnyOther'),
                        'AuditingAgency'  			=>  $this->input->post('AuditingAgency'),
     					'AuditingAgencyAddress' 	=> $this->input->post('AuditingAgencyAddress'),
						'ContactName'  				=>  $this->input->post('ContactName1'),
                        'Email'  					=>  $this->input->post('Email'),
						'ElectricalSafety' 			=> $this->input->post('ElectricalSafety'),
						'Earthing'  				=>  $this->input->post('Earthing'),
                        'PlantOM'  					=>  $this->input->post('PlantOM'),	
						'Operatorcertificate' 		=> $this->input->post('Operatorcertificate'),
						'WaterQuality'  			=>  $this->input->post('WaterQuality'),
                        'LastWQtestreport'  		=>  $this->input->post('LastWQtestreport'),
						'ConsumerAwareness' 		=> $this->input->post('ConsumerAwareness'),
						'CheckforIECmaterial'  		=>  $this->input->post('CheckforIECmaterial'),
                        'ABKeeping'  				=>  $this->input->post('ABKeeping'),
     					'CheckforReceipts' 			=> $this->input->post('CheckforReceipts'),
						'UV'  						=>  $this->input->post('UV'),
                        'TDS'  						=>  $this->input->post('TDS'),
						'pH' 						=> $this->input->post('pH'),
						'ResidualChlorine'  		=>  $this->input->post('ResidualChlorine'),
                        'Microbial'  				=>  $this->input->post('Microbial'),	
						'PreMonsoonRWTesting' 		=> $PreMonsoonRWTestingcheck,
						'PreMonsoonRWLabAdr'  		=>  $this->input->post('PreMonsoonRWLabAdr'),
                        'PreMonsoonRWTestingdate'   => $PreMonsoonRWTestingdateDBConvert,
						'PreMonsoonRWTestProof' 	=> $PreMonsoonRWTestProofimage,
						'PreMonsoonTWTesting' 	 	=>  $PreMonsoonTWTestingcheck,
                        'PreMonsoonTWLabAdr'  		=>  $this->input->post('PreMonsoonTWLabAdr'),
     					'PreMonsoonTWTestingdate' 	=> $PreMonsoonTWTestingdateDBConvert,
						'PreMonsoonTWProof' 		=> $PreMonsoonTWProofimage,
                        'PostMonsoonRWTesting'  	=>  $PostMonsoonRWTestingcheck,
						'PostMonsoonRWLabadr' 		=> $this->input->post('PostMonsoonRWLabadr'),
						'PostMonsoonRWtestingdate'  =>  $PostMonsoonRWtestingdateDBConvert,
                        'PostMonsoonRWTestProof'  	=>  $PostMonsoonRWTestProofimage,	
						'PostMonsoonTWtesting' 		=> $PostMonsoonTWtestingcheck,
						'PostMonsoonTWLabAdr'  		=>  $this->input->post('PostMonsoonTWLabAdr'),
                        'PostMonsoonTWtestingdate'  => $PostMonsoonTWtestingdateDBConvert,
						'PostMonsoonTWtestProof' 	=> $PostMonsoonRWTestProofimage,
						'RejectWaterTesting'  		=>  $RejectWaterTestingcheck,
                        'RejectWaterLabAdr'  		=>  $this->input->post('RejectWaterLabAdr'),
     					'RejectWatertestingdate' 	=> $RejectWatertestingdateDBConvert,
						'RejectWaterTestProof'  	=>  $RejectWaterTestProofimage,
                        'RawWaterOpenwellcovered'   =>  $this->input->post('RawWaterOpenwellcovered'),
						'RawWaterBoreWellCasing' 	=> $this->input->post('RawWaterBoreWellCasing'),
						'Insidetheplant'  			=>  $this->input->post('Insidetheplant'),
                        'Covered'  					=>  $this->input->post('Covered'),
						'CleanlinessinPlant' 		=> $this->input->post('CleanlinessinPlant'),
						'Leakage'  					=>  $this->input->post('Leakage'),
                        'CleanlinessNearTreatmentPlant'  =>  $this->input->post('CleanlinessNearTreatmentPlant'),
						'Checkmossoralgee' 			=> $this->input->post('Checkmossoralgee'),
						'TechnicalDowntime'  		=>  $this->input->post('TechnicalDowntime'),
                        'NoOfDays'  				=>  $this->input->post('NoOfDays'),
     					'NoOfFault' 				=> $this->input->post('NoOfFault'),
						'MotorRepair'  				=>  $this->input->post('MotorRepair'),
                        'MembraneChoke'  			=>  $this->input->post('MembraneChoke'),
						'Rawwaterproblem' 			=> $this->input->post('Rawwaterproblem'),
						'Electricityoutage'  		=>  $this->input->post('Electricityoutage'),
                        'Anyother'  				=>  $this->input->post('Anyother'),	
						'AnyotherEdit' 				=> $this->input->post('AnyotherEdit'),
						'SalesDayLost'  			=>  $this->input->post('SalesDayLost'),
                        'WaterProductionDaily'  	=>  $this->input->post('WaterProductionDaily'),
						'WaterProductionMonthly' 	=> $this->input->post('WaterProductionMonthly'),
						'SamplingOrGift'  			=>  $this->input->post('SamplingOrGift'),
                        'LeakageEdit'  				=>  $this->input->post('LeakageEdit'),
     					'WaterTreatmentPlantCost' 	=> $this->input->post('WaterTreatmentPlantCost'),
						'InfrastructureCost'  		=>  $this->input->post('InfrastructureCost'),
                        'WaterBill'  				=>  $this->input->post('WaterBill'),
						'ElectricityBill' 			=> $this->input->post('ElectricityBill'),
						'GeneratorMaintainance'  	=>  $this->input->post('GeneratorMaintainance'),
                        'Rent'  					=>  $this->input->post('Rent'),		
                        'OperatorSalary' 			=> $this->input->post('OperatorSalary'),
						'ChemicalAndOtherConsumable'  =>  $this->input->post('ChemicalAndOtherConsumable'),
                        'MiscellaneousChk'  		=>  $this->input->post('MiscellaneousChk'),
						'MiscFirstText' 			=> $this->input->post('MiscFirstText'),
						'MiscFirstAmount' 			 =>  $this->input->post('MiscFirstAmount'),
                        'MiscSecondText'  			=>  $this->input->post('MiscSecondText'),
     					'MiscSecondAmount'			 => $this->input->post('MiscSecondAmount'),
						'MiscThirdText'  			=>  $this->input->post('MiscThirdText'),
                        'MiscThirdAmount'  			=>  $this->input->post('MiscThirdAmount'),
						'ServiceCharge' 			=> $this->input->post('ServiceCharge'),
						'AssestRenewalFund'  		=>  $this->input->post('AssestRenewalFund'),
                        'AssestRepaymentFund'  		=>  $this->input->post('AssestRepaymentFund'),	
						'OPEx' 						=> $this->input->post('OPEx'),
						'OPExservicecharge'  		=>  $this->input->post('OPExservicecharge'),
                        'OPExservicechargemaintenance'  =>  $this->input->post('OPExservicechargemaintenance'),
						'opExSCMRAssetRepayment' 	=> $this->input->post('opExSCMRAssetRepayment'),
						'checkRWQuantification'  	=>  $this->input->post('checkRWQuantification'),
                        'CheckRWUtilisation'  		=>  $this->input->post('CheckRWUtilisation'),
     					'CheckRWDisposal' 			=> $this->input->post('CheckRWDisposal'),
						'CheckWharvesting' 		 	=>  $this->input->post('CheckWharvesting'),
                        //'WQuantification'  =>  $this->input->post('WQuantification'),
						//'WUtilisation' => $this->input->post('WUtilisation'),
						//'WDisposal'  =>  $this->input->post('WDisposal'),
                        'Presenceofpuddle'  		=>  $this->input->post('Presenceofpuddle'),		
						'UtilizationActivity1'		=> $this->input->post('UtilizationActivity1'),
						'UtilizationActivity2'  	=>  $this->input->post('UtilizationActivity2'),
                        'UtilizationActivity3'  	=>  $this->input->post('UtilizationActivity3'),
						'UtilizationActivity4' 		=> $this->input->post('UtilizationActivity4'),
						'UtilizationOther1'  		=>  $this->input->post('UtilizationOther1'),
                        'UtilizationOther2'  		=>  $this->input->post('UtilizationOther2'),
     					'UtilizationOther3'			=> $this->input->post('UtilizationOther3'),
						'UtilizationOther4'  		=>  $this->input->post('UtilizationOther4'),
                        'OperatorName'  			=>  $this->input->post('OperatorName'),
						'DesignationID' 			=> $this->input->post('DesignationID'),
						'ContactNumber'  			=>  $this->input->post('ContactNumber'),
                        'LiteracyID'  				=>  $this->input->post('LiteracyID'),	
						'EducationAnyOther' 		=> $this->input->post('EducationAnyOther'),
						'TransportExpense'  		=>  $this->input->post('TransportExpense'),
                        'PeakSale'  				=>  $this->input->post('PeakSale'),
					);	
					
					
					//$this->db->where('PlantPOUID',$this->input->post('PlantPOUID'));
					$this->db->where('PlantGUID',$this->input->post('PlantGUID'));
					
					$this->db->update(PATPOD,$data); //echo $this->db->last_query();die;


			////////////////////////// Exist  Upload  Document or not //////////////
			$condition = 'PlantGUID="'.$this->input->post('PlantGUID').'"';
			$Imagecount = $this->Common_model->exist_data('tblpatplantinstitutionaldocument',$condition);
			if($Imagecount ==0){
			////////////////////////////  Upload  Document //////////////
				$OldImage = $this->input->post('oldmainimage');	
				$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100));
				$tempFile = $_FILES['fileupload']['tmp_name'];
				$targetPath = FCPATH . "datafiles/";
				$targetFile = $targetPath . $encryptedName . ".jpg";
				$uploadResult = move_uploaded_file($tempFile,$targetFile);
				if($uploadResult == true){
					$mainimage = $encryptedName;
				}
				
				if(!empty($mainimage)){
					$uploadImage = $mainimage;			
				}else{
					$uploadImage = $OldImage;
				}
	
			$dataFileupload = array(
				'PlantInstitutionalDocumentID' =>1,
				'PlantGUID' => $token,
				'DocumentName' => $uploadImage,
				'IsDeleted'  => 0,
			 );

			$this->db->where('PlantGUID',$this->input->post('PlantGUID'));
			return $this->db->insert(tblpatplantinstitutionaldocument,$dataFileupload) ? 1: -1;
			}else{

			    $OldImage = $this->input->post('oldmainimage');	
				$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100));
				$tempFile = $_FILES['fileupload']['tmp_name'];
				$targetPath = FCPATH . "datafiles/";
				$targetFile = $targetPath . $encryptedName . ".jpg";
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$mainimage = $encryptedName;
				}
									
			if(!empty($mainimage)){
				$uploadImage = $mainimage;			
			}else{
				$uploadImage = $OldImage;
			}

				$dataFileupload = array(
				'PlantInstitutionalDocumentID' =>1,
				'PlantGUID' => $token,
				'DocumentName' => $uploadImage,
				'IsDeleted'  => 0,
			 );

			$this->db->where('PlantGUID',$this->input->post('PlantGUID'));
			return $this->db->update(tblpatplantinstitutionaldocument,$dataFileupload) ? 1: -1;
			}
			
			return 1;
			

		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['status_id']  = '2';
			$form['modify_by'] = 1;//$this->loggedIn;
			$form['modify_ip'] = $this->fromIP;
			$this->db->set('modify_date', 'NOW()', FALSE);
			$this->db->where('user_id',(int)$token);
			return ($this->db->update(PATPD,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	public function getPatAssestFunder($id)
	{
		$this->db->select('AssetFunderID');
		$this->db->where('PlantGUID',$id);
		return $this->db->get('tblpatplantassetfunder')->result();
	}
	
	
	/**
	 * Method getAgency() get all Agency.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAgency()
	{
		$this->db->select('AgencyID,AgencyName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AgencyID','asc');
		return $this->db->get(AGENCY)->result();
	}
	
	/**
	 * Method getAssessedby() get all USER.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssessedby()
	{
		$this->db->select('UserID,UserName,FirstName,LastName');
		$this->db->where('IsActive','1');
		$this->db->where('Isdeleted','0');
		$this->db->order_by('UserID','asc');
		return $this->db->get(USERS)->result();
	}
	
	
	/**
	 * Method getPlantSpecification() get all Plant Specification.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantSpecification()
	{
		$this->db->select('PlantSpecificationID,PlantSpecificationName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('PlantSpecificationID','asc');
		return $this->db->get(PLANSPECIFICATION)->result();
	}
	
	/**
	 * Method getPlantManufacturer() get all Plant Manufacturer.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantManufacturer()
	{
		$this->db->select('PlantManufacturerID,	PlantManufacturerName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('PlantManufacturerID','asc');
		return $this->db->get(PLANTMANUFACTURER)->result();
	}
	
	/**
	 * Method getPlantPurificationStep() get all Plant Purification Step.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantPurificationStep()
	{
		$this->db->select('PlantPurificationStepID,	PlantPurificationStepName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('PlantPurificationStepID','asc');
		return $this->db->get(PLANTPUR)->result();
	}
	
	
	
	public function getPlantPurStep($token)
	{
		$this->db->select('PlantPurificationstepID');
		$this->db->where('PlantGUID',$token);
	    $this->db->where('IsDeleted','0');
		return $this->db->get(PATPFCTS)->result();
	}
	
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 1.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunder6()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','6');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
		
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 1.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPlantFundedBy($token)
	{
		$this->db->select('AssetFunderID,FlagID');
		$this->db->where('PlantGUID',$token);
		return $this->db->get('tblpatplantassetfunder')->row(); //echo $this->db->last_query(); //die;
		
	}
	
	
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 2.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag2()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','2');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 3.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag3()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','3');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 4.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag4()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','4');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getAssestFunder() get all Plant Purification Assest Funder Flag 4.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAssestFunderFlag5()
	{
		$this->db->select('AssestFunderID,AssestFunderName');
		$this->db->where('Flag','5');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('AssestFunderID','asc');
		return $this->db->get(ASSETFUNDER)->result();
	}
	
	/**
	 * Method getWaterQualityChallenge() get all Plant Purification Water Quality Challenge.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getWaterQualityChallenge()
	{
		$this->db->select('WaterQualityChallengeID,WaterQualityChallengeName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('WaterQualityChallengeID','asc');
		return $this->db->get(WATERQTCE)->result();
	}
	
	
	
	/**
	 * Method getPatPlantProdDetail() get all Plant Prod Detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getPatPlantProdDetail($token)
	{
		$this->db->select('PlantGUID,DistributionplantID,Price,Volume,HomeDeliveryPrice');
		$this->db->where('IsDeleted','0');
		$this->db->where('PlantGUID',$token);
		return $this->db->get(PATPPD)->result();
	}
	
	
	/**
	 * Method getWaterQualityChallenge() get all Plant Purification Water Quality Challenge.
	 * @access	public
	 * @param	
	 * @return	array  tblpatwatercontaminants
	 */
	public function getPatWaterContaminants($token)
	{
		$this->db->select('WaterContaminantsID,WaterQualityChallengeID');
		
		$this->db->where('PlantGUID',$token);
		
	    $this->db->where('IsDeleted','0');
		
		return $this->db->get('tblpatwatercontaminants')->result(); //echo $this->db->last_query();
	}
	
	
	/**
	 * Method getDesignation() get all designation.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDesignation()
	{
		$this->db->select('DesignationID,DesignationName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('DesignationID','asc');
		return $this->db->get(DESIG)->result();
	}


	/**
	 * Method getDesignation() get all designation.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getLiteracy()
	{
		$this->db->select('LiteracyID,LiteracyName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('LiteracyID','asc');
		return $this->db->get('mstliteracy')->result();
	}
	
	
	/**
	 * Method getCountry() get all Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		$this->db->select('CountryID,CountryName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('CountryID','asc');
		//$this->db->get(COUNTRY)->result(); //echo  $this->db->last_query(); die;
		return $this->db->get(COUNTRY)->result();
	}
	
	/**
	 * Method getState() get all State.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getState()
	{
		$this->db->select('StateID,StateName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('StateID','asc');
		return $this->db->get(STATE)->result();
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistrict()
	{
		$this->db->select('DistrictID,DistrictName');
		$this->db->where('IsDeleted','0');
		$this->db->order_by('DistrictID','asc');
		return $this->db->get(DISTRICT)->result();
	}
	
	
}