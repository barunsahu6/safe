<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TSTDashboard_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			$this->db->limit($limit,$start);
			return $this->db->get(DISTRICT)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
/***
List HorizontalBar Graph

*/
	public function CreateHorizontalBarPAT($strwhr,$roleid,$userid)
	{

		try { 
			$procedure = 'CALL SP_GraphdashboardUserdetailForPAT('.$strwhr.','.$userid.','.$roleid.')';
			$data = $this->db->query($procedure)->result();
			//print_r($data); die;
			 return $data;
			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}

//////////////////////// Get Guage Graph data ///////////////////////
	public function getguage()
	{
		try{
			$this->db->close();
			$this->db->initialize();
		    $procedure  = 'SELECT AVG((NoOfhhregistered)*100/NoOfHousehold)  AS adpotion FROM tblpatplantdetail';
			$data = $this->db->query($procedure)->result()[0]; //die;
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	
//////////////////////// Get finan Graph data ///////////////////////
	
	public function getfinan($strwhr)
	{
		try{
			
			$sql = 'SELECT (SELECT COUNT(t.opex) FROM  (SELECT tblPATPODetail.plantguid,opex, OpexServiceCharge, OpexServiceChargemaintenance, OpexSCMRAssetRepayment,SUM(Price*Volume) AS revenue FROM tblPATPODetail INNER JOIN tblPATPlantProdDetail ON tblPATPlantProdDetail.plantguid=tblPATPODetail.PlantGUID
			INNER JOIN tblpatplantdetail ON tblpatplantdetail.PlantGUID=tblPATPlantProdDetail.PlantGUID
			WHERE tblpatplantdetail.countryid="'.$strwhr.'"
			GROUP BY 
			tblPATPODetail.PlantGUID, opex, OpexServiceCharge, OpexServiceChargemaintenance, OpexSCMRAssetRepayment) t
			WHERE t.OPEx>=t.revenue) AS opex,
			(SELECT COUNT(t.OpexServiceCharge) FROM  (SELECT tblPATPODetail.plantguid,opex, OpexServiceCharge, OpexServiceChargemaintenance, OpexSCMRAssetRepayment,SUM(Price*Volume) AS revenue FROM tblPATPODetail INNER JOIN tblPATPlantProdDetail ON tblPATPlantProdDetail.plantguid=tblPATPODetail.PlantGUID
			INNER JOIN tblpatplantdetail ON tblpatplantdetail.PlantGUID=tblPATPlantProdDetail.PlantGUID
			WHERE tblpatplantdetail.countryid="'.$strwhr.'"
			GROUP BY 
			tblPATPODetail.PlantGUID, opex, OpexServiceCharge, OpexServiceChargemaintenance, OpexSCMRAssetRepayment) t
			WHERE t.OpexServiceCharge>=t.revenue) AS OPSR,
			(SELECT COUNT(t.OpexServiceChargemaintenance) FROM 
			 (SELECT tblPATPODetail.plantguid,opex, OpexServiceCharge, OpexServiceChargemaintenance, OpexSCMRAssetRepayment,SUM(Price*Volume) AS revenue FROM tblPATPODetail INNER JOIN tblPATPlantProdDetail ON tblPATPlantProdDetail.plantguid=tblPATPODetail.PlantGUID
			INNER JOIN tblpatplantdetail ON tblpatplantdetail.PlantGUID=tblPATPlantProdDetail.PlantGUID
			WHERE tblpatplantdetail.countryid="'.$strwhr.'"
			GROUP BY 
			tblPATPODetail.PlantGUID, opex, OpexServiceCharge, OpexServiceChargemaintenance, OpexSCMRAssetRepayment) t
			WHERE t.OpexServiceChargemaintenance>=t.revenue) AS OPESRM';
			$result = $this->db->query($sql)->result()[0];
			return $result; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	
	//////////////////////// Get Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChart()
	{
		try{
			$this->db->close();
			$this->db->initialize();
			$procedure  = 'SELECT COUNT(POGUID) AS Walkin,(SELECT COUNT(POGUID) FROM tblPATPlantprodDetail WHERE distributionplantid NOT IN(0,99)) AS Distribution FROM tblPATPlantprodDetail 
		    INNER JOIN  tblpatPlantDetail ON tblPATPlantprodDetail.PlantGUID=tblpatPlantDetail.PlantGUID 
		    INNER JOIN MstState ON MstState.StateID =tblpatPlantDetail.StateID 
		    INNER JOIN mstdistrict ON mstdistrict.DistrictID=tblPATPlantDetail.District ';
			$data = $this->db->query($procedure)->result()[0];
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	//////////////////////// Get Bar Graph User data Bar2dUserBarChart ///////////////////////

	public function CreateBar2dUserBarChart()
	{
		try{
			$this->db->close();
			$this->db->initialize();
			 $procedure  = 'SELECT COUNT(UserID) AS Total FROM userTemp UNION ALL SELECT COUNT(UserID) AS Active FROM userTemp WHERE VisitDate>SUBDATE(NOW(), 20)
							UNION ALL SELECT COUNT(UserID) AS PartiallyActive FROM userTemp WHERE VisitDate<SUBDATE(NOW(), 7) AND VisitDate>SUBDATE(NOW(), 20)
							UNION ALL SELECT COUNT(UserID) AS Inactive FROM userTemp WHERE VisitDate<SUBDATE(NOW(), 7); ';
			$data = $this->db->query($procedure)->result();
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	 /**
	 * Method DashBoard_Contamination() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoard_Contamination()
	{
		
		$procedure = "SELECT  mstparameter.Name AS label, 
		tbltstsampletestresult.ParameterID AS value
		FROM tbltstlocation INNER JOIN tbltstsampleInfo ON tbltstlocation.GUID = tbltstsampleInfo.LocationGUID 
		INNER JOIN tbltstlaboratorydetails ON tbltstsampleInfo.SampleGUID=tbltstlaboratorydetails.SampleGUID 
		INNER JOIN tbltstsampletestresult ON tbltstsampletestresult.LabGUID = tbltstlaboratorydetails.LabGUID 
		INNER JOIN mstparameter ON mstparameter.ParameterID=tbltstsampletestresult.ParameterID
		GROUP BY tbltstsampletestresult.ParameterID, mstparameter.Name ORDER BY mstparameter.Name";
		$data = $this->db->query($procedure)->result();
		return json_encode($data);
	}


	/**
	 * Method DashBoard_WaterTreatmentTechnology() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoard_WaterTreatmentTechnology()
	{
		
		 $procedure = "SELECT WTechnologyName as label,OperationCost AS Value FROM MstWaterTreatmentTechnology 
 					WHERE WtechnologyID IN(1, 2, 3, 4, 5, 6, 7, 10)"; //die;
		$data = $this->db->query($procedure)->result();
		return json_encode($data);
	}
	

	
    /**
	 * Method DashBoard_Report() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoard_Report()
	{

		try { 

			$this->db->close();
			$this->db->initialize();
				
			$procedure = 'CALL DashBoard_Report()';
			$data = $this->db->query($procedure)->result();
			return $data; /////print_r($data); die;
			}
			catch (Exception $e) {
				print_r($e->getMessage());die;
			}

	}


 /**
	 * Method DashBoard_Report() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoardGraphs_BarChart_Data()
	{

		try { 
				
			$procedure = 'CALL Get_DashBoardGraphs_BarChart_Data()';
			$data = $this->db->query($procedure)->result();
			return $data; /////print_r($data); die;
			}
			catch (Exception $e) {
				print_r($e->getMessage());die;
			}

	}




	
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			//print_R($form); die;
			return ($this->db->insert(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function getDetail($token)
	{
		try {
			$this->db->select('*');
			$this->db->where('DistrictID',(int)$token);
			return $this->db->get(DISTRICT)->row(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(DISTRICT)->result(); 
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			$form = $this->input->post('form');
			//print_r($form);die;
			
			$this->db->where('DistrictID',(int)$token);
			return ($this->db->update(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['IsDeleted']  = '1';
			$this->db->where('DistrictID',(int)$token);
			//$this->db->update(DISTRICT,$form); //echo $this->db->last_query(); die;
			return ($this->db->update(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
/**
	 * Method Count_Country_All() get Count Country .
	 * @access	public
	 * @param	
	 * @return	array
	 */
/**	public function Count_Country_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT CountryID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
			print_r($e->getMessage());die;
		}
	} */
/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result(); //echo $this->db->last_query(); die;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			return $this->db->get()->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts()
	{
		try {
		$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictID','asc');
		return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	/**public function getPlants()
	{
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.PlantGUID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}*/
	
	
}