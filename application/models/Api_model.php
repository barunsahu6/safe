<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	// public $plantguid = "";
	// public $plantpoguid = "";
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
	}

	public function response($response)
	{
	  header("Content-Type: application/json");
	  die(json_encode(["response"=>[0=>$response]]));
	  
	}
	
	public function uploaddata()
	{
		// $this->plantguid = uniqid();
		// $this->plantpoguid = uniqid();

		$data = $this->input->post('data');
		
		$data_array = json_decode($data);

		foreach ($data_array as $tablename => $tabledata) {

			//echo "<pre>";
			//print_r($tablename.'-'.$tabledata);
			//print_r($tabledata);
			
			switch($tablename)
			{
				
				case "tblPATPlantDetail" : $res['Table'][0]['PlantGUID'] = $this->tblPATPlantDetail($tabledata);
				//case "tblPATPlantDetail" : $this->tblPATPlantDetail($tabledata);
				break;
			 	case "tblPATWaterContaminants" : $this->tblPATWaterContaminants($tabledata);
				break;
				case "tblPATPlantAssetFunder" : $this->tblPATPlantAssetFunder($tabledata);
				break;
				case "tblPATPlantProdDetail" : $this->tblPATPlantProdDetail($tabledata);
				break;
				case "tblPATPlantPurificationstep" : $this->tblPATPlantPurificationstep($tabledata);
				break;
				case "tblPATPlantInstitutionalDocument" : $res['Table1'][] = $this->tblPATPlantInstitutionalDocument($tabledata);
				// case "tblPATPlantInstitutionalDocument" : $this->tblPATPlantInstitutionalDocument($tabledata);
				break;
				case "tblPATPODetail" : $res['Table'][0]['PlantPOGUID'] = $this->tblPATPODetail($tabledata);
				 //case "tblPATPODetail" : $this->tblPATPODetail($tabledata);
				break;
				case "tblPATPlantDistribution" : $this->tblPATPlantDistribution($tabledata);
				break;


			}


		}
		// print_r($res);
		// echo json_encode($res);
		// echo json_encode("success");
		//return $res;
		return NULL;

	}

	public function uploaddata_tst()
	{
		$data = $this->input->post('data');
		
		$data_array = json_decode($data);

		foreach ($data_array as $tablename => $tabledata) {

			//echo "<pre>";
			//print_r($tablename.'-'.$tabledata);
			//print_r($tabledata);exit();
			
			switch($tablename)
			{
			 	case "tblTSTLaboratoryDetails" : $this->tbltstlaboratorydetails($tabledata);
				break;
				case "tblTSTLocation" : $this->tbltstlocation($tabledata);
				break;
				case "tblTSTSampleInfo" : $this->tbltstsampleinfo($tabledata);
				break;
				case "tblTSTSampleTestResult" : $this->tbltstsampletestresult($tabledata);
				break;
			}


		}
		//print_r($res);
		// echo json_encode($res);
		 //echo json_encode("success");
		//return $res;
		return NULL;

	}

	public function uploaddata_fvt()
	{
		$data = $this->input->post('data');
		
		$data_array = json_decode($data);

		foreach ($data_array as $tablename => $tabledata) {

			//echo "<pre>";
			//print_r($tablename.'-'.$tabledata);
			//print_r($tabledata);
			
			switch($tablename)
			{
			 	case "tblTSTLaboratoryDetails" : $this->tbltstlaboratorydetails($tabledata);
				break;
				case "tblTSTLocation" : $this->tbltstlocation($tabledata);
				break;
				case "tblTSTSampleInfo" : $this->tbltstsampleinfo($tabledata);
				break;
				case "tblTSTSampleTestResult" : $this->tbltstsampletestresult($tabledata);
				break;
			}


		}
		// print_r($res);
		// echo json_encode($res);
		// echo json_encode("success");
		//return $res;
		return NULL;

	}

	private function tbltstlaboratorydetails($tabledata)
	{

		foreach ($tabledata as $row) 
		{									
			$LabGUID = $row->LabGUID;
			$this->db->select('*');
			$this->db->where(LabGUID,$LabGUID);
			$sql_count = $this->db->get(tbltstlaboratorydetails)->num_rows();

			if($sql_count > 0)
			{
				$query = "delete FROM `tbltstlaboratorydetails` where LabGUID = '$row->LabGUID'";
				$result = $this->db->query($query);
				$this->db->insert('tbltstlaboratorydetails', $row);
				// $this->db->update('tbltstlaboratorydetails', $row, 'LabGUID',$row->LabGUID);
			}
			else
			{
				$this->db->insert('tbltstlaboratorydetails', $row);
			}
		}
	}

	private function tblTSTSampleInfo($tabledata)
	{
        // echo "<pre>";
		// 	print_r($tabledata);exit();
		foreach ($tabledata as $row) 
		{									
			$SampleGUID = $row->SampleGUID;
			$this->db->select('*');
			$this->db->where(SampleGUID,$SampleGUID);
			$sql_count = $this->db->get(tbltstsampleinfo)->num_rows();
           
			if($sql_count > 0)
			{
				$query = "delete FROM `tbltstsampleinfo` where SampleGUID = '$row->SampleGUID'";
				$result = $this->db->query($query);
				$this->db->insert('tbltstsampleinfo', $row);
			
			// $result = 	$this->db->update('tbltstsampleinfo', $row, 'SampleGUID',$row->SampleGUID);
				
			}
			else
			{
				
				$this->db->insert('tbltstsampleinfo', $row);
			}
		}
	}

	private function tblTSTLocation($tabledata)
	{
        
		foreach ($tabledata as $row) 
		{									
			$GUID = $row->GUID;
			$this->db->select('*');
			$this->db->where(GUID,$GUID);
			$sql_count = $this->db->get(tbltstlocation)->num_rows();

			if($sql_count > 0)
			{
				$query = "delete FROM `tbltstlocation` where GUID = '$row->GUID'";
				$result = $this->db->query($query);
				$this->db->insert('tbltstlocation', $row);
				// $this->db->update('tbltstlocation', $row, 'GUID',$row->GUID);
			}
			else
			{
				$this->db->insert('tbltstlocation', $row);
			}
		}
	}

	private function tblTSTSampleTestResult($tabledata)
	{
          
		foreach ($tabledata as $row) 
		{		
									
			$SampleGUID = $row->SampleGUID;
			$this->db->select('*');
			$this->db->where(SampleGUID,$SampleGUID);
			$sql_count = $this->db->get(tbltstsampletestresult)->num_rows();
			
			if($sql_count > 0)
			{
				$query = "delete FROM `tbltstsampletestresult` where SampleGUID = '$row->SampleGUID'";
				$result = $this->db->query($query);
				$this->db->insert('tbltstsampletestresult', $row);
				// $this->db->update('tbltstsampletestresult', $row, 'SampleGUID',$row->SampleGUID);
			}
			else
			{
				
				$this->db->insert('tbltstsampletestresult', $row);
			}
		}
	}


private function tblPATPODetail($tabledata)
{

		foreach ($tabledata as $row) 
		{

			if($row->SamplingOrGift == "")
			{
				$row->SamplingOrGift = 0;	
			}
			if($row->LastWQtestreport == "")
			{
				$row->LastWQtestreport = 0;	
			}
			if($row->UpdatedOn == "")
			{
				$row->UpdatedOn = 0;	
			}
			if($row->Anyother == "")
			{
				$row->Anyother = 0;	
			}
			if($row->Operatorcertificate == "")
			{
				$row->Operatorcertificate = 0;	
			}
			if($row->Microbial == "")
			{
				$row->Microbial = 0;	
			}

    		if($row->ResidualChlorine == "")
			{
				$row->ResidualChlorine = 0;	
			}
			if($row->OperatorSalary == "")
			{
				$row->OperatorSalary = NULL;	
			}
			if($row->PostMonsoonTWLabAdr == "")
			{
				$row->PostMonsoonTWLabAdr = Null;	
			}
			if($row->AssestRepaymentFund == "")
			{
				$row->AssestRepaymentFund = 0;	
			}
			if($row->ConsumerAwareness == "")
			{
				$row->ConsumerAwareness = 0;	
			}
			if($row->pH == "")
			{
				$row->pH = 0;	
			}

			if($row->UV == "")
			{
				$row->UV = 0;	
			}
			if($row->RejectWaterTesting == "")
			{
				$row->RejectWaterTesting = 0;	
			}
			if($row->ContactNumber == "")
			{
				$row->ContactNumber = 0;	
			}
			if($row->ElectricalSafety == "")
			{
				$row->ElectricalSafety = 0;	
			}
			if($row->AuditingAgencyAddress == "")
			{
				$row->AuditingAgencyAddress = NULL;	
			}
			if($row->PostMonsoonRWtestingdate == "")
			{
				$row->PostMonsoonRWtestingdate = Null;	
			}

    		if($row->MiscSecondAmount == "")
			{
				$row->MiscSecondAmount = 0;	
			}
			if($row->Email == "")
			{
				$row->Email = NULL;	
			}
			if($row->PreMonsoonRWLabAdr == "")
			{
				$row->PreMonsoonRWLabAdr = Null;	
			}
			if($row->WaterQuality == "")
			{
				$row->WaterQuality = 0;	
			}
			if($row->PostMonsoonTWtestProof == "")
			{
				$row->PostMonsoonTWtestProof = NULL;	
			}
			if($row->ServiceCharge == "")
			{
				$row->ServiceCharge = 0;	
			}

			if($row->PostMonsoonRWLabadr == "")
			{
				$row->PostMonsoonRWLabadr = Null;	
			}
			if($row->PreMonsoonTWTestingdate == "")
			{
				$row->PreMonsoonTWTestingdate = NULL;	
			}
			if($row->PlantOM == "")
			{
				$row->PlantOM = 0;	
			}
			if($row->PreMonsoonTWProof == "")
			{
				$row->PreMonsoonTWProof = 0;	
			}
			if($row->TDS == "")
			{
				$row->TDS = 0;	
			}
			if($row->UtilizationOther3 == "")
			{
				$row->UtilizationOther3 = 0;	
			}

    		if($row->PostMonsoonTWtestingdate == "")
			{
				$row->PostMonsoonTWtestingdate = 0;	
			}
			if($row->UtilizationOther2 == "")
			{
				$row->UtilizationOther2 = 0;	
			}
			if($row->UtilizationOther4 == "")
			{
				$row->UtilizationOther4 = 0;	
			}
			if($row->UtilizationOther1 == "")
			{
				$row->UtilizationOther1 = 0;	
			}
			if($row->OperatorName == "")
			{
				$row->OperatorName = NULL;	
			}
			if($row->Latitude == "")
			{
				$row->Latitude = 0;	
			}

			if($row->RawWaterOpenwellcovered == "")
			{
				$row->RawWaterOpenwellcovered = 0;	
			}
			if($row->InfrastructureCost == "")
			{
				$row->InfrastructureCost = NULL;	
			}
			if($row->Checkmossoralgee == "")
			{
				$row->Checkmossoralgee = 0;	
			}
			if($row->LiteracyID == "")
			{
				$row->LiteracyID = 0;	
			}
			if($row->CheckRWUtilisation == "")
			{
				$row->CheckRWUtilisation = 0;	
			}
			if($row->IsSynched == "")
			{
				$row->IsSynched = 0;	
			}

    		if($row->ABKeeping == "")
			{
				$row->ABKeeping = 0;	
			}
			if($row->PeakSale == "")
			{
				$row->PeakSale = NULL;	
			}
			if($row->WaterProductionMonthly == "")
			{
				$row->WaterProductionMonthly = Null;	
			}
			if($row->ContactName == "")
			{
				$row->ContactName = 0;	
			}
			if($row->PreMonsoonRWTesting == "")
			{
				$row->PreMonsoonRWTesting = 0;	
			}
			if($row->AgencyAnyOther == "")
			{
				$row->AgencyAnyOther = 0;	
			}
			if($row->WaterBill == "")
			{
				$row->WaterBill = 0;	
			}
			if($row->MiscFirstText == "")
			{
				$row->MiscFirstText = NULL;	
			}
			if($row->PostMonsoonTWtesting == "")
			{
				$row->PostMonsoonTWtesting = 0;	
			}
			if($row->RejectWaterTestProof == "")
			{
				$row->RejectWaterTestProof = 0;	
			}
			if($row->VisitDate == "")
			{
				$row->VisitDate = NULL;	
			}

			if($row->CheckWharvesting == "")
			{
				$row->CheckWharvesting = 0;	
			}

    		if($row->NoOfDays == "")
			{
				$row->NoOfDays = 0;	
			}
			if($row->MiscThirdText == "")
			{
				$row->MiscThirdText = NULL;	
			}
			if($row->WQuantification == "")
			{
				$row->WQuantification = Null;	
			}
			if($row->CheckforReceipts == "")
			{
				$row->CheckforReceipts = 0;	
			}
			if($row->CreatedBy == "")
			{
				$row->CreatedBy = 0;	
			}
			if($row->AuditedBy == "")
			{
				$row->AuditedBy = 0;	
			}

			if($row->Rent == "")
			{
				$row->Rent = 0;	
			}
			if($row->UtilizationActivity4 == "")
			{
				$row->UtilizationActivity4 = NULL;	
			}
			if($row->UtilizationActivity3 == "")
			{
				$row->UtilizationActivity3 = 0;	
			}
			if($row->checkRWQuantification == "")
			{
				$row->checkRWQuantification = 0;	
			}
			if($row->UtilizationActivity2 == "")
			{
				$row->UtilizationActivity2 = NULL;	
			}
			if($row->CleanlinessNearTreatmentPlant == "")
			{
				$row->CleanlinessNearTreatmentPlant = 0;	
			}
    		if($row->UtilizationActivity1 == "")
			{
				$row->UtilizationActivity1 = 0;	
			}
			if($row->Electricityoutage == "")
			{
				$row->Electricityoutage = 0;	
			}
			if($row->ElectricityBill == "")
			{
				$row->ElectricityBill = Null;	
			}
			if($row->MiscellaneousChk == "")
			{
				$row->MiscellaneousChk = 0;	
			}
			if($row->WUtilisation == "")
			{
				$row->WUtilisation = NULL;	
			}
			if($row->Rawwaterproblem == "")
			{
				$row->Rawwaterproblem = 0;	
			}

			if($row->WaterProductionDaily == "")
			{
				$row->WaterProductionDaily = Null;	
			}
			if($row->UpdatedBy == "")
			{
				$row->UpdatedBy = NULL;	
			}
			if($row->CheckRWDisposal == "")
			{
				$row->CheckRWDisposal = 0;	
			}
			if($row->Earthing == "")
			{
				$row->Earthing = 0;	
			}
			if($row->CheckforIECmaterial == "")
			{
				$row->CheckforIECmaterial = 0;	
			}
			if($row->NoOfFault == "")
			{
				$row->NoOfFault = 0;	
			}

    		if($row->Leakage == "")
			{
				$row->Leakage = 0;	
			}
			if($row->CleanlinessinPlant == "")
			{
				$row->CleanlinessinPlant = 0;	
			}
			if($row->TechnicalDowntime == "")
			{
				$row->TechnicalDowntime = Null;	
			}
			if($row->OPExservicecharge == "")
			{
				$row->OPExservicecharge = 0;	
			}
			if($row->MiscFirstAmount == "")
			{
				$row->MiscFirstAmount = NULL;	
			}
			if($row->AuditingAgency == "")
			{
				$row->AuditingAgency = 0;	
			}

			if($row->Presenceofpuddle == "")
			{
				$row->Presenceofpuddle = 0;	
			}
			if($row->MiscSecondText == "")
			{
				$row->MiscSecondText = NULL;	
			}
			if($row->CreatedOn == "")
			{
				$row->CreatedOn = 0;	
			}
			if($row->WDisposal == "")
			{
				$row->WDisposal = 0;	
			}
			if($row->Longitude == "")
			{
				$row->Longitude = NULL;	
			}
			if($row->EducationAnyOther == "")
			{
				$row->EducationAnyOther = Null;	
			}

    		if($row->POGUID == "")
			{
				$row->POGUID = 0;	
			}
			if($row->PreMonsoonRWTestProof == "")
			{
				$row->PreMonsoonRWTestProof = NULL;	
			}
			if($row->IsComplete == "")
			{
				$row->IsComplete = 0;	
			}
			if($row->WaterTreatmentPlantCost == "")
			{
				$row->WaterTreatmentPlantCost = 0;	
			}
			if($row->PostMonsoonRWTesting == "")
			{
				$row->PostMonsoonRWTesting = 0;	
			}
			if($row->DesignationID == "")
			{
				$row->DesignationID = 0;	
			}
			if($row->TransportExpense == "")
			{
				$row->TransportExpense = 0;	
			}
			if($row->PreMonsoonRWTestingdate == "")
			{
				$row->PreMonsoonRWTestingdate = NULL;	
			}
			if($row->LeakageEdit == "")
			{
				$row->LeakageEdit = 0;	
			}
			if($row->ChemicalAndOtherConsumable == "")
			{
				$row->ChemicalAndOtherConsumable = 0;	
			}
			if($row->AssestRenewalFund == "")
			{
				$row->AssestRenewalFund = NULL;	
			}
			if($row->IsTablet == "")
			{
				$row->IsTablet = 0;	
			}

    		if($row->Covered == "")
			{
				$row->Covered = 0;	
			}
			if($row->MiscThirdAmount == "")
			{
				$row->MiscThirdAmount = NULL;	
			}
			if($row->OPEx == "")
			{
				$row->OPEx = Null;	
			}
			if($row->MembraneChoke == "")
			{
				$row->MembraneChoke = 0;	
			}
			if($row->Insidetheplant == "")
			{
				$row->Insidetheplant = 0;	
			}
			if($row->opExSCMRAssetRepayment == "")
			{
				$row->opExSCMRAssetRepayment = 0;	
			}
			if($row->GeneratorMaintainance == "")
			{
				$row->GeneratorMaintainance = 0;	
			}
			if($row->SalesDayLost == "")
			{
				$row->SalesDayLost = NULL;	
			}
			if($row->PreMonsoonTWTesting == "")
			{
				$row->PreMonsoonTWTesting = 0;	
			}
			if($row->RawWaterBoreWellCasing == "")
			{
				$row->RawWaterBoreWellCasing = 0;	
			}
			if($row->AnyotherEdit == "")
			{
				$row->AnyotherEdit = NULL;	
			}
			if($row->OPExservicechargemaintenance == "")
			{
				$row->OPExservicechargemaintenance = Null;	
			}
    		if($row->PreMonsoonTWLabAdr == "")
			{
				$row->PreMonsoonTWLabAdr = 0;	
			}
			if($row->RejectWatertestingdate == "")
			{
				$row->RejectWatertestingdate = NULL;	
			}
			if($row->PostMonsoonRWTestProof == "")
			{
				$row->PostMonsoonRWTestProof = Null;	
			}
			
			if($row->RejectWaterLabAdr == "")
			{
				$row->RejectWaterLabAdr = NULL;	
			}
			if($row->MotorRepair== "")
			{
				$row->MotorRepair= 0;	
			}

												
			$plantGUID = $row->PlantGUID;

			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatpodetail)->num_rows();

			// echo "asdasd".$sql_count; die();


			if(count($tabledata) > 0)
				{

			foreach ($tabledata as $row) 
			{
				
				$res_patientguids_array[] = "'".$row->PlantGUID."'";

			}
			$patientguids = implode($res_patientguids_array,',');

			//$this->db->update('	tblpatwatercontaminants', $row, 'PlantGUID',$row->PlantGUID);

			$query = "delete FROM `tblpatpodetail` where PlantGUID in (".$patientguids.")";

			$result = $this->db->query($query);
		
			foreach ($tabledata as $row) {
			// $row->PlantGUID = $this->plantguid;
			$this->db->insert('tblpatpodetail', $row);

		}
		}

			/*if($sql_count > 0)
			{
				
				$this->db->update('tblpatpodetail', $row, 'PlantGUID',$row->PlantGUID);
				return $row->POGUID;
			}
			else
			{
				
				$this->db->insert('tblpatpodetail', $row);
				
				return $row->POGUID;
			}*/

		}
}



private function tblPATPlantInstitutionalDocument($tabledata)
{

	$docs = array();
		foreach ($tabledata as $row) 
		{
			if($row->PlantInstitutionalDocumentID == "")
			{
				$row->PlantInstitutionalDocumentID = 0;	
			}
			if($row->DocumentName == "")
			{
				$row->DocumentName = NULL;	
			}
			if($row->IsDeleted == "")
			{
				$row->IsDeleted = 0;	
			}
									
			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantinstitutionaldocument)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantinstitutionaldocument', $row, 'PlantGUID',$row->PlantGUID);
			}
			else
			{
				// $row->PlantGUID = $this->plantguid;
				$this->db->insert('tblpatplantinstitutionaldocument', $row);
			}

			$docs = ['DOCUMENTNAME'=>$row->DocumentName, 'PLANTINSTITUTIONALDOCUMENTID'=>$row->PlantInstitutionalDocumentID];
		}
		return $docs;
	}


	private function tblPATPlantPurificationstep($tabledata)
	{

		foreach ($tabledata as $row) 
		{
			if($row->PlantPurificationstepID == "")
			{
				$row->PlantPurificationstepID = 0;	
			}
			if($row->PlantGUID == "")
			{
				$row->PlantGUID = 0;	
			}
			if($row->IsDeleted == "")
			{
				$row->IsDeleted = 0;	
			}
									
			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantpurificationstep)->num_rows();

			if(count($tabledata) > 0)
			{
		

				foreach ($tabledata as $row) 
			{
				
				$res_patientguids_array[] = "'".$row->PlantGUID."'";

			}
			$patientguids = implode($res_patientguids_array,',');

			
			$query = "delete FROM `tblpatplantpurificationstep` where PlantGUID in (".$patientguids.")";

			$result = $this->db->query($query);
		
			foreach ($tabledata as $row) {

				// $row->PlantGUID = $this->plantguid;
				$this->db->insert('tblpatplantpurificationstep', $row);
			}
		}
			
		}
	}


private function tblPATPlantProdDetail($tabledata)
{

		foreach ($tabledata as $row) 
		{
			if($row->POGUID == "")
			{
				$row->POGUID = 0;	
			}
			if($row->DistributionplantID == "")
			{
				$row->DistributionplantID = 0;	
			}
			if($row->Price == "")
			{
				$row->Price = 0;	
			}
			if($row->Volume == "")
			{
				$row->Volume = 0;	
			}
			if($row->HomeDeliveryPrice == "")
			{
				$row->HomeDeliveryPrice = 0;	
			}
			
			$this->db->select('*');
			$this->db->where('PlantGUID',$row->PlantGUID);
			$this->db->where('POGUID',$row->POGUID);
			$this->db->where('DistributionplantID',$row->DistributionplantID);
			$sql_count = $this->db->get(tblpatplantproddetail)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantproddetail', $row, array('PlantGUID' => $row->PlantGUID, 'POGUID' => $row->POGUID, 'DistributionplantID' => $row->DistributionplantID));
			}
			else
			{
				$this->db->insert('tblpatplantproddetail', $row);
			}
		}
}
	


private function tblPATPlantAssetFunder($tabledata)
{

	foreach ($tabledata as $row) 
	{
		if($row->AssetFunderID == "")
		{
			$row->AssetFunderID = 0;	
		}
		if($row->FlagID == "")
		{
			$row->FlagID = 0;	
		}
		if($row->POGUID == "")
		{
			$row->POGUID = 0;	
		}
					

		$AssetFunderID 	= $row->AssetFunderID;
		$PlantGUID 		= $row->PlantGUID;
		$FlagID 		= $row->FlagID;
		$id_tblpatplantassetfunderAssetFunderID = $row->id_tblpatplantassetfunder;

		$this->db->select('*');
		$this->db->where(PlantGUID,$PlantGUID);
		$this->db->where(AssetFunderID,$AssetFunderID);
		$this->db->where(FlagID,$FlagID); 
		$sql_count = $this->db->get(tblpatplantassetfunder)->num_rows();

		if($sql_count > 0)
		{
			$this->db->update('tblpatplantassetfunder', $row, 'id_tblpatplantassetfunder',$row->id_tblpatplantassetfunder);
			
		}
		else
		{
			// $row->PlantGUID = $this->plantguid;
			// $row->POGUID = $this->plantpoguid;
			$this->db->insert('tblpatplantassetfunder', $row);
		}
	}
}

private function tblPATWaterContaminants($tabledata)
{
	foreach ($tabledata as $row) 
	{
		   if($row->WaterContaminantsID == "")
		{
			$row->WaterContaminantsID = 0;	
		}
		if($row->PlantGUID == "")
		{
			$row->PlantGUID = 0;	
		}
		if($row->WaterQualityChallengeID == "")
		{
			$row->WaterQualityChallengeID = 0;	
		}
		if($row->IsDeleted == "")
		{
			$row->IsDeleted = 0;	
		}
		
		$plantGUID = $row->PlantGUID;

		$this->db->select('*');
		$this->db->where('PlantGUID',$plantGUID);

		$sql_count = $this->db->get('tblpatwatercontaminants')->num_rows();

		if(count($tabledata) > 0)
		{

			foreach ($tabledata as $row) 
			{
				
				$res_patientguids_array[] = "'".$row->PlantGUID."'";

			}
			$patientguids = implode($res_patientguids_array,',');

			//$this->db->update('	tblpatwatercontaminants', $row, 'PlantGUID',$row->PlantGUID);

			$query = "delete FROM `tblpatwatercontaminants` where PlantGUID in (".$patientguids.")";

			$result = $this->db->query($query);
		
			foreach ($tabledata as $row) {
			// $row->PlantGUID = $this->plantguid;
			$this->db->insert('tblpatwatercontaminants', $row);

		}
		}
	}

}

private function tblPATPlantDetail($tabledata)
{

	foreach ($tabledata as $row) 
		{	
			// if($row->SWNID == "")
			// {
			// 	$row->SWNID = null;	
			// }
			// if($row->VisitDate == "")
			// {
			// 	$row->VisitDate = null;	
			// }
			// if($row->Email == "")
			// {
			// 	$row->Email = null;	
			// }
			// if($row->MailingAddress == "")
			// {
			// 	$row->MailingAddress = null;	
			// }
			// if($row->PlantLocation == "")
			// {
			// 	$row->PlantLocation = null;	
			// }
			// if($row->PlantLatitude == "")
			// {
			// 	$row->PlantLatitude = null;	
			// }
			// if($row->PlantLongitude == "")
			// {
			// 	$row->PlantLongitude = null;	
			// }
			// if($row->PlantPhotoLink == "")
			// {
			// 	$row->PlantPhotoLink = null;	
			// }
			// // if($row->UserID == "")
			// // {
			// // 	$row->UserID = null;	
			// // }
			// if($row->AgencyID == "")
			// {
			// 	$row->AgencyID = null;	
			// }
			// if($row->LocationType == "")
			// {
			// 	$row->LocationType = null;	
			// }
			// if($row->CountryID == "")
			// {
			// 	$row->CountryID = null;	
			// }
			// if($row->StateID == "")
			// {
			// 	$row->StateID = null;	
			// }
			// if($row->District == "")
			// {
			// 	$row->District = null;	
			// }
			// if($row->BlockName == "")
			// {
			// 	$row->BlockName = null;	
			// }
			// if($row->VillageName == "")
			// {
			// 	$row->VillageName = null;	
			// }
			// if($row->PinCode == "")
			// {
			// 	$row->PinCode = 0;	
			// }
			// if($row->SC == "")
			// {
			// 	$row->SC = 0;	
			// }
			// if($row->ST == "")
			// {
			// 	$row->ST = 0;	
			// }
			// if($row->OBC == "")
			// {
			// 	$row->OBC = 0;	
			// }
			// if($row->General == "")
			// {
			// 	$row->General = 0;	
			// }
			// if($row->AllInclusion == "")
			// {
			// 	$row->AllInclusion = 0;	
			// }
			// if($row->OperatorName == "")
			// {
			// 	$row->OperatorName = null;	
			// }
			// if($row->DesignationID == "")
			// {
			// 	$row->DesignationID = 0;	
			// }
			// if($row->ContactNumber == "")
			// {
			// 	$row->ContactNumber = 0;	
			// }
			// if($row->LiteracyID == "")
			// {
			// 	$row->LiteracyID = 0;	
			// }
			// if($row->PlantSpecificationID == "")
			// {
			// 	$row->PlantSpecificationID = 0;	
			// }
			// if($row->PlantManufacturerID == "")
			// {
			// 	$row->PlantManufacturerID = 0;	
			// }
			// if($row->ManufacturerName == "")
			// {
			// 	$row->ManufacturerName = null;	
			// }
			// if($row->EstablishmentDate == "")
			// {
			// 	$row->EstablishmentDate = null;	
			// }
			// if($row->AgeOfPlant == "")
			// {
			// 	$row->AgeOfPlant = null;	
			// }
			// // if($row->RemoteMonitoringSystem == "")
			// // {
			// // 	$row->RemoteMonitoringSystem = null;	
			// // }
			// if($row->LandCost == "")
			// {
			// 	$row->LandCost = null;	
			// }
			// if($row->LandOther == "")
			// {
			// 	$row->LandOther = null;	
			// }
			// if($row->MachineryCost == "")
			// {
			// 	$row->MachineryCost = null;	
			// }
			// if($row->MachineryOther == "")
			// {
			// 	$row->MachineryOther = null;	
			// }
			// if($row->BuildingCost == "")
			// {
			// 	$row->BuildingCost = null;	
			// }
			// if($row->BuildingOther == "")
			// {
			// 	$row->BuildingOther = null;	
			// }
			// if($row->RawWaterSourceCost == "")
			// {
			// 	$row->RawWaterSourceCost = null;	
			// }
			// if($row->RawWaterSourceOther == "")
			// {
			// 	$row->RawWaterSourceOther = null;	
			// }
			// if($row->ElectricityCost == "")
			// {
			// 	$row->ElectricityCost = null;	
			// }
			// if($row->ElectricityOther == "")
			// {
			// 	$row->ElectricityOther = null;	
			// }
			// if($row->Distribution == "")
			// {
			// 	$row->Distribution = 0;	
			// }
			// if($row->NoOfhhregistered == "")
			// {
			// 	$row->NoOfhhregistered = null;	
			// }
			// if($row->AvgMonthlyCards == "")
			// {
			// 	$row->AvgMonthlyCards = null;	
			// }
			// // if($row->GramPanchayatApproval == "")
			// // {
			// // 	$row->GramPanchayatApproval = null;	
			// // }
			// // if($row->LegalElectricityConnection == "")
			// // {
			// // 	$row->LegalElectricityConnection = null;	
			// // }
			// if($row->LandApproval == "")
			// {
			// 	$row->LandApproval = null;	
			// }
			// if($row->RawWaterSourceApproval == "")
			// {
			// 	$row->RawWaterSourceApproval = null;	
			// }
			// if($row->RejectWaterDischargeApproval == "")
			// {
			// 	$row->RejectWaterDischargeApproval = null;	
			// }
			// if($row->ServiceProviderApproval == "")
			// {
			// 	$row->ServiceProviderApproval = null;	
			// }
			// if($row->OperatorQualification == "")
			// {
			// 	$row->OperatorQualification = null;	
			// }
			// if($row->CreatedBy == "")
			// {
			// 	$row->CreatedBy = 0;	
			// }
			// if($row->CreatedOn == "")
			// {
			// 	$row->CreatedOn = null;	
			// }
			// if($row->UpdatedBy == "")
			// {
			// 	$row->UpdatedBy = 0;	
			// }
			// if($row->UpdatedOn == "")
			// {
			// 	$row->UpdatedOn = null;	
			// }
			// if($row->IsTablet == "")
			// {
			// 	$row->IsTablet = 0;	
			// }
			// if($row->WaterBrandName == "")
			// {
			// 	$row->WaterBrandName = null;	
			// }
			// if($row->PlantSpecificationAnyOther == "")
			// {
			// 	$row->PlantSpecificationAnyOther = null;	
			// }
			// if($row->PAssetSourceFundedBy == "")
			// {
			// 	$row->PAssetSourceFundedBy = null;	
			// }
			// if($row->auditAgencyOther == "")
			// {
			// 	$row->auditAgencyOther = null;	
			// }
			// if($row->EducationAnyOther == "")
			// {
			// 	$row->EducationAnyOther = null;	
			// }
			// if($row->ContaminantAnyOther == "")
			// {
			// 	$row->ContaminantAnyOther = null;	
			// }
			// if($row->Population == "")
			// {
			// 	$row->Population = null;	
			// }
			// if($row->NoOfHousehold == "")
			// {
			// 	$row->NoOfHousehold = null;	
			// }
			// if($row->NoOfHouseholdWithin2km == "")
			// {
			// 	$row->NoOfHouseholdWithin2km = null;	
			// }
			// if($row->BPL == "")
			// {
			// 	$row->BPL = null;	
			// }
			

			$plantGUID = $row->PlantGUID;
			$this->db->select('*');
			$this->db->where(PlantGUID,$plantGUID);
			$sql_count = $this->db->get(tblpatplantdetail)->num_rows();

			if($sql_count > 0)
			{
				$this->db->update('tblpatplantdetail', $row, 'PlantGUID',$row->PlantGUID);
				return $row->PlantGUID;
				

			}
			else
			{
				// print_r($row);
				// $this->plantguid = uniqid(); 
				// $row->PlantGUID = $this->plantguid;
				// echo $this->plantguid; 
				// die();
				$this->db->insert('tblpatplantdetail', $row);
				// return array("PlantGUID"=>$row->PlantGUID);
				return $row->PlantGUID;
			}
		}

}

private function tblPATPlantDistribution($tabledata)
{			
	foreach ($tabledata as $row) 
	{
		if($row->DPID == "")
		{
			$row->DPID = 0;	
		}
		if($row->Distance == "")
		{
			$row->Distance = 0;	
		}
		if($row->DPName == "")
		{
			$row->DPName = NULL;	
		}
		if($row->PlantGUID == "")
		{
			$row->PlantGUID = 0;	
		}
	
		$plantGUID = $row->PlantGUID;
		$this->db->select('*');
		$this->db->where(PlantGUID,$plantGUID); 
		$sql_count = $this->db->get(tblpatplantdistribution)->num_rows();

		if($sql_count > 0)
		{
			$this->db->update('tblpatplantdistribution', $row, 'PlantGUID',$row->PlantGUID);
		}
		else
		{
			// $row->PlantGUID = $this->plantguid;
			// $row->POGUID = $this->plantpoguid;
			$this->db->insert('tblpatplantdistribution', $row);
		}
	}
}

public function download_masters($username, $password)
{
	$data = array();

	$sql = "select * from mstuser where UserName = '".$username."' and Password = '".$password."'";
	$data['MstUser'] = $this->db->query($sql)->result();

	$sql = "SELECT * FROM mstcountry";
	$data['MstCountry'] = $this->db->query($sql)->result();
	
	$sql = "select * from mststate";
	$data['MstState'] = $this->db->query($sql)->result();

	$sql = "SELECT `DistrictID`, `StateID`, `CountryID`, `DistrictCode`, replace(`DistrictName`,'\'','`') as DistrictName, `IsDeleted` FROM `mstdistrict`";
	$data['MstDistrict'] = $this->db->query($sql)->result();
	
	$sql = "select * from mstblock";
	$data['MstBlock'] = $this->db->query($sql)->result();

	$sql = "select * from mstvillage";
	$data['MstVillage'] = $this->db->query($sql)->result();

	$sql = "select * from mstsource";
	$data['MSTSource'] = $this->db->query($sql)->result();

	$sql = "select * from mstseason";
	$data['MstSeason'] = $this->db->query($sql)->result();

	$sql = "select * from mstweather";
	$data['MstWeather'] = $this->db->query($sql)->result();

	$sql = "select * from mststandard";
	$data['MSTstandard'] = $this->db->query($sql)->result();

	$sql = "select * from mststandardparameterdetail";
	$data['MSTStandardParameterDetail'] = $this->db->query($sql)->result();

	$sql = "select * from msttechnology";
	$data['MSTTechnology'] = $this->db->query($sql)->result();

	$sql = "select * from msttreatment";
	$data['MSTTreatment'] = $this->db->query($sql)->result();

	$sql = "select * from msttreatmentpercent";
	$data['MstTreatmentPercent'] = $this->db->query($sql)->result();

	$sql = "select * from msttreatmenttechnology";
	$data['MstTreatmentTechnology'] = $this->db->query($sql)->result();

	$sql = "select * from mstwatertreatment";
	$data['MstWaterTreatment'] = $this->db->query($sql)->result();

	$sql = "select * from mstwatertreatmenttechnology";
	$data['MstWaterTreatmentTechnology'] = $this->db->query($sql)->result();

	$sql = "select * from mstcontainertype";
	$data['MstContainerType'] = $this->db->query($sql)->result();
	
	$sql = "SELECT * from mstparameter where  IsDeleted=0";
	$data['mstparameter'] = $this->db->query($sql)->result();

	$sql = "select * from mstplantspecification ";
	$data['MstPlantSpecification'] = $this->db->query($sql)->result();

	$sql = "select * from mstagency";
	$data['MstAgency'] = $this->db->query($sql)->result();

	$sql = "select * from mstassetfunder where IsDeleted = 0";
	$data['MstAssetFunder'] = $this->db->query($sql)->result();

	$sql = "select * from mstdpdistance";
	$data['MstDPDistance'] = $this->db->query($sql)->result();

	$sql = "select * from mstdesignation";
	$data['MstDesignation'] = $this->db->query($sql)->result();

	$sql = "select * from mstliteracy";
	$data['MstLiteracy'] = $this->db->query($sql)->result();

	$sql = "select * from mstplantmanufacturer";
	$data['MstPlantManufacturer'] = $this->db->query($sql)->result();

	$sql = "select * from mstplantpurificationstep";
	$data['MstPlantPurificationStep'] = $this->db->query($sql)->result();

	$sql = "select * from mstwaterqualitychallenge";
	$data['MstWaterQualityChallenge'] = $this->db->query($sql)->result();

	$sql = "select * from mstrole";
	$data['MstRole'] = $this->db->query($sql)->result();

	return $data;
}

public function download_masters_PAT($username, $password)
{
	$data = array();

	$sql = "select * from mstuser where UserName = '".$username."' and Password = '".$password."'";
	$data['MstUser'] = $this->db->query($sql)->result();

	$sql = "SELECT * FROM mstcountry";
	$data['MstCountry'] = $this->db->query($sql)->result();
	
	$sql = "select * from mststate";
	$data['MstState'] = $this->db->query($sql)->result();

	$sql = "SELECT `DistrictID`, `StateID`, `CountryID`, `DistrictCode`, replace(`DistrictName`,'\'','`') as DistrictName, `IsDeleted` FROM `mstdistrict`";
	$data['MstDistrict'] = $this->db->query($sql)->result();
	
	$sql = "select * from mstblock";
	$data['MstBlock'] = $this->db->query($sql)->result();

	$sql = "select * from mstvillage";
	$data['MstVillage'] = $this->db->query($sql)->result();

	$sql = "select * from mstsource";
	$data['MSTSource'] = $this->db->query($sql)->result();

	$sql = "select * from mstseason";
	$data['MstSeason'] = $this->db->query($sql)->result();

	$sql = "select * from mstweather";
	$data['MstWeather'] = $this->db->query($sql)->result();

	$sql = "select * from mststandard";
	$data['MSTstandard'] = $this->db->query($sql)->result();

	$sql = "select * from mststandardparameterdetail";
	$data['MSTStandardParameterDetail'] = $this->db->query($sql)->result();

	$sql = "select * from msttechnology";
	$data['MSTTechnology'] = $this->db->query($sql)->result();

	$sql = "select * from msttreatment";
	$data['MSTTreatment'] = $this->db->query($sql)->result();

	$sql = "select * from msttreatmentpercent";
	$data['MstTreatmentPercent'] = $this->db->query($sql)->result();

	$sql = "select * from msttreatmenttechnology";
	$data['MstTreatmentTechnology'] = $this->db->query($sql)->result();

	$sql = "select * from mstwatertreatment";
	$data['MstWaterTreatment'] = $this->db->query($sql)->result();

	$sql = "select * from mstwatertreatmenttechnology";
	$data['MstWaterTreatmentTechnology'] = $this->db->query($sql)->result();

	$sql = "select * from mstcontainertype";
	$data['MstContainerType'] = $this->db->query($sql)->result();
	
	$sql = "select * from mstparameter";
	$data['mstparameter'] = $this->db->query($sql)->result();

	$sql = "select * from mstplantspecification ";
	$data['MstPlantSpecification'] = $this->db->query($sql)->result();

	$sql = "select * from mstagency";
	$data['MstAgency'] = $this->db->query($sql)->result();

	$sql = "select * from mstassetfunder where IsDeleted = 0";
	$data['MstAssetFunder'] = $this->db->query($sql)->result();

	$sql = "select * from mstdpdistance";
	$data['MstDPDistance'] = $this->db->query($sql)->result();

	$sql = "select * from mstdesignation";
	$data['MstDesignation'] = $this->db->query($sql)->result();

	$sql = "select * from mstliteracy";
	$data['MstLiteracy'] = $this->db->query($sql)->result();

	$sql = "select * from mstplantmanufacturer";
	$data['MstPlantManufacturer'] = $this->db->query($sql)->result();

	$sql = "select * from mstplantpurificationstep";
	$data['MstPlantPurificationStep'] = $this->db->query($sql)->result();

	$sql = "select * from mstwaterqualitychallenge";
	$data['MstWaterQualityChallenge'] = $this->db->query($sql)->result();

	$sql = "select * from mstrole";
	$data['MstRole'] = $this->db->query($sql)->result();

	return $data;
}

public function download_masters_FVT($username, $password)
{
	$data = array();

	$sql = "select * from mstuser where UserName = '".$username."' and Password = '".$password."'";
	$data['MstUser'] = $this->db->query($sql)->result();

	$sql = "SELECT * FROM mstcountry";
	$data['MstCountry'] = $this->db->query($sql)->result();
	
	$sql = "select * from mststate";
	$data['MstState'] = $this->db->query($sql)->result();

	$sql = "SELECT `DistrictID`, `StateID`, `CountryID`, `DistrictCode`, replace(`DistrictName`,'\'','`') as DistrictName, `IsDeleted` FROM `mstdistrict`";
	$data['MstDistrict'] = $this->db->query($sql)->result();
	
	$sql = "select * from mstblock";
	$data['MstBlock'] = $this->db->query($sql)->result();

	$sql = "select * from mstvillage";
	$data['MstVillage'] = $this->db->query($sql)->result();

	$sql = "select * from mstsource";
	$data['MSTSource'] = $this->db->query($sql)->result();

	$sql = "select * from masterinputrecords";
	$data['MasterInputRecords'] = $this->db->query($sql)->result();

	$sql = "select * from inputrecords";
	$data['InputRecords'] = $this->db->query($sql)->result();

	$sql = "select * from masterplantoutput";
	$data['MasterPlantOutput'] = $this->db->query($sql)->result();

	$sql = "select * from aggregatorploutput";
	$data['AggregatorPLOutput'] = $this->db->query($sql)->result();

	$sql = "select * from aggregator_plinput";
	$data['Aggregator_PLInput'] = $this->db->query($sql)->result();

	$sql = "select * from masteraggregator_plinput";
	$data['MasterAggregator_PLInput'] = $this->db->query($sql)->result();

	$sql = "select * from solarchiller_output";
	$data['SolarChiller_Output'] = $this->db->query($sql)->result();

	$sql = "select * from tblploutput";
	$data['TblPLOutput'] = $this->db->query($sql)->result();

	$sql = "select * from tbl_distributionoutput";
	$data['Tbl_DistributionOutput'] = $this->db->query($sql)->result();

	$sql = "select * from distribution_plinput";
	$data['Distribution_PLInput'] = $this->db->query($sql)->result();

	$sql = "select * from tblloanrepaymentsolarchiller";
	$data['TblLoanRepaymentSolarChiller'] = $this->db->query($sql)->result();
	
	$sql = "select * from recordsolarchiller_plinput";
	$data['RecordSolarChiller_PLInput'] = $this->db->query($sql)->result();

	$sql = "select * from mastersolarchiller_plinput";
	$data['MasterSolarChiller_PLInput'] = $this->db->query($sql)->result();

	$sql = "select * from recordpl_input";
	$data['RecordPL_Input'] = $this->db->query($sql)->result();

	$sql = "select * from masterpl_input";
	$data['MasterPL_Input'] = $this->db->query($sql)->result();

	$sql = "select * from msttechnology_type";
	$data['MstTechnology_Type'] = $this->db->query($sql)->result();

	$sql = "select * from mstfunders";
	$data['MstFunders'] = $this->db->query($sql)->result();

	$sql = "select * from mstownership";
	$data['MstOwnership'] = $this->db->query($sql)->result();

	$sql = "select * from tblloanrepaymentdata";
	$data['TblLoanRepaymentData'] = $this->db->query($sql)->result();

	return $data;
}

public function register()
{
	$data = $this->security->xss_clean($this->input->post('data'));
      $data = json_decode($data);
	// print_r($data->User->UserName); die();

      if($data->User->UserName == '')
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Username not found! Please send username request'
      	);
      	$this->response($response);
      }
      
      if($data->User->Email == '')
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Email not found! Please email username request'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(UserName) = '".strtolower($data->User->UserName)."' and lcase(Email) = '".$data->User->Email."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Both'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(UserName) = '".strtolower($data->User->UserName)."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Username'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(Email) = '".$data->User->Email."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Email'
      	);
      	$this->response($response);
      }

      $OTP = rand(100000,999999);

      $this->db->trans_start();

      $subject = "OTP for Verification";
      $body = 'Dear '.$data->User->FirstName.' '.$data->User->LastName.', <br><br>';

      $body .= 'Please find new users OTP is '. $OTP;
      $to_email = $data->User->Email;
      $to_name = $data->User->UserName;
      $cc='';
      $email_result = $this->Common_model->send_email($subject, $body, $to_email, $to_name, $cc);

      $insert_array = array(
        // "UserID"       => null,
        "UserName"     => $data->User->UserName,
        "FirstName"    => $data->User->FirstName,
        "MiddleName"   => $data->User->MiddleName,
        "LastName"     => $data->User->LastName,
        "Password"     => $data->User->Password,
        "RoleID"       => $data->User->RoleID,
        "CreatedBy"    => $data->User->CreatedBy,
        "CreatedOn"    => $data->User->CreatedOn,
        "ModifiedBy"   => $data->User->ModifiedBy,
        "ModifiedOn"   => $data->User->ModifiedOn,
        "IsActive"     => $data->User->IsActive,
        "Isdeleted"    => $data->User->Isdeleted,
        "Email"        => $data->User->Email,
        "AggregatorID" => null,
        "CountryID"    => null,
        "StateID"      => null,
        "DistrictID"   => null,
        "PlantID"      => null,
        "ToolID"       => null,
        "OTP"     	   => $OTP,
      );

      $this->db->insert('mstuser', $insert_array);

      $this->db->trans_complete();

      if($this->db->trans_status() === FALSE)
      {
      	return ["status"=>1,"message"=>"ERROR:error in transaction"];
      }
      return ["status"=>0,"message"=>"Registration Successful Please Verify OTP !!"];
}

public function register_PAT()
{
	$data = $this->security->xss_clean($this->input->post('data'));
      $data = json_decode($data);
	// print_r($data->User->UserName); die();

      if($data->User->UserName == '')
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Username not found! Please send username request'
      	);
      	$this->response($response);
      }
      
      if($data->User->Email == '')
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Email not found! Please email username request'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(UserName) = '".strtolower($data->User->UserName)."' and lcase(Email) = '".$data->User->Email."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Both'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(UserName) = '".strtolower($data->User->UserName)."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Username'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(Email) = '".$data->User->Email."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Email'
      	);
      	$this->response($response);
      }

      $OTP = rand(100000,999999);

      $this->db->trans_start();

      $subject = "OTP for Verification";
      $body = 'Dear '.$data->User->FirstName.' '.$data->User->LastName.', <br><br>';

      $body .= 'Please find new users OTP is '. $OTP;
      $to_email = $data->User->Email;
      $to_name = $data->User->UserName;

      $email_result = $this->Common_model->send_email($subject, $body, $to_email, $to_name, $cc);

      $insert_array = array(
        // "UserID"       => null,
        "UserName"     => $data->User->UserName,
        "FirstName"    => $data->User->FirstName,
        "MiddleName"   => $data->User->MiddleName,
        "LastName"     => $data->User->LastName,
        "Password"     => $data->User->Password,
        "RoleID"       => $data->User->RoleID,
        "CreatedBy"    => $data->User->CreatedBy,
        "CreatedOn"    => $data->User->CreatedOn,
        "ModifiedBy"   => $data->User->ModifiedBy,
        "ModifiedOn"   => $data->User->ModifiedOn,
        "IsActive"     => $data->User->IsActive,
        "Isdeleted"    => $data->User->Isdeleted,
        "Email"        => $data->User->Email,
        "AggregatorID" => null,
        "CountryID"    => null,
        "StateID"      => null,
        "DistrictID"   => null,
        "PlantID"      => null,
        "ToolID"       => null,
        "OTP"     	   => $OTP,
      );

      $this->db->insert('mstuser', $insert_array);

      $this->db->trans_complete();

      if($this->db->trans_status() === FALSE)
      {
      	return ["status"=>1,"message"=>"ERROR:error in transaction"];
      }
      return ["status"=>0,"message"=>"Registration Successful Please Verify OTP !!"];
}

public function register_FVT()
{
	$data = $this->security->xss_clean($this->input->post('data'));
      $data = json_decode($data);
	// print_r($data->User->UserName); die();

      if($data->User->UserName == '')
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Username not found! Please send username request'
      	);
      	$this->response($response);
      }
      
      if($data->User->Email == '')
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Email not found! Please email username request'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(UserName) = '".strtolower($data->User->UserName)."' and lcase(Email) = '".$data->User->Email."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Both'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(UserName) = '".strtolower($data->User->UserName)."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Username'
      	);
      	$this->response($response);
      }

      $query = "select * from mstuser where lcase(Email) = '".$data->User->Email."'";
      $result = $this->db->query($query)->result();

      if(count($result) > 0)
      {
      	$response = array(
      	  'status' => 1,
      	  'message'=> 'Email'
      	);
      	$this->response($response);
      }

      $OTP = rand(100000,999999);

      $this->db->trans_start();

      $subject = "OTP for Verification";
      $body = 'Dear '.$data->User->FirstName.' '.$data->User->LastName.', <br><br>';

      $body .= 'Please find new users OTP is '. $OTP;
      $to_email = $data->User->Email;
      $to_name = $data->User->UserName;

      $email_result = $this->Common_model->send_email($subject, $body, $to_email, $to_name, $cc);

      $insert_array = array(
        // "UserID"       => null,
        "UserName"     => $data->User->UserName,
        "FirstName"    => $data->User->FirstName,
        "MiddleName"   => $data->User->MiddleName,
        "LastName"     => $data->User->LastName,
        "Password"     => $data->User->Password,
        "RoleID"       => $data->User->RoleID,
        "CreatedBy"    => $data->User->CreatedBy,
        "CreatedOn"    => $data->User->CreatedOn,
        "ModifiedBy"   => $data->User->ModifiedBy,
        "ModifiedOn"   => $data->User->ModifiedOn,
        "IsActive"     => $data->User->IsActive,
        "Isdeleted"    => $data->User->Isdeleted,
        "Email"        => $data->User->Email,
        "AggregatorID" => null,
        "CountryID"    => null,
        "StateID"      => null,
        "DistrictID"   => null,
        "PlantID"      => null,
        "ToolID"       => null,
        "OTP"     	   => $OTP,
      );

      $this->db->insert('mstuser', $insert_array);

      $this->db->trans_complete();

      if($this->db->trans_status() === FALSE)
      {
      	return ["status"=>1,"message"=>"ERROR:error in transaction"];
      }
      return ["status"=>0,"message"=>"Registration Successful Please Verify OTP !!"];
}

}