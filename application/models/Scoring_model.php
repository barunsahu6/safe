<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Scoring_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(PATPD);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(PATPD)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	
	public function getDetail()
	{
		$ROLE_ID = $this->session->userdata('login_data')['ROLE_ID'];
		if ($ROLE_ID == 1) {

			$this->db->select('*');
			$this->db->from(PATPLANDETAIL);
			$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID');
			$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID');
			$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID');
			$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District');
			$this->db->order_by(PATPOD.'.VisitDate', 'DESC');
			// $this->db->join('honeywell_patplantdetail' ,'iJal_Stations =SWNID');

			$result = $this->db->get()->result(); //echo $this->db->last_query(); die;
			// print_r($result); die();
			return $result;

		} else if ($ROLE_ID == 9) {
			$this->db->select('*');
			$this->db->from(PATPLANDETAIL);
			$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID');
			$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID');
			$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID');
			$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District');
			$this->db->join('honeywell_patplantdetail' ,'honeywell_patplantdetail.PlantGUID ='.PATPLANDETAIL.'.PlantGUID');
			$this->db->order_by(PATPOD.'.VisitDate', 'DESC');

			$result = $this->db->get()->result(); 
			// echo $this->db->last_query(); die;
			// print_r($result); die();
			return $result;# code...
		}else{
			$this->db->select('*');
			$this->db->from(PATPLANDETAIL);
			$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID');
			$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID');
			$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID');
			$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District');
			$this->db->order_by(PATPOD.'.VisitDate', 'DESC');
			// $this->db->join('honeywell_patplantdetail' ,'iJal_Stations =SWNID');

			$result = $this->db->get()->result(); //echo $this->db->last_query(); die;
			// print_r($result); die();
			return $result;
		}

	}

	// date 7/5/2018 public function getDetail()
	// {
	// 	try {
	// 		$this->db->select('*');
	// 		$this->db->from(PATPLANDETAIL);
	// 		$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID');
	// 		$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID');
	// 		$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID');
	// 		$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District');
	// 		$result = $this->db->get()->result(); //echo $this->db->last_query(); die;
	// 		return $result;
	// 	}
	// 	catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }
	
	
	/**
	 * Method get view Detail() get detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function viewDetail($tab,$token,$POGUID)	
	{
		try {
			$this->db->close();
			$this->db->initialize();
			$procedure = 'CALL spGraph_PATScoring("'.$tab.'","'.$token.'","'.$POGUID.'")';
			$data = $this->db->query($procedure)->result()[0];
			return $data;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	/**
	 * Method get PatPodDetail() get detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function getPatPodDetail($token)	
	{
		
		try {
			$this->db->close();
			$this->db->initialize();
			$this->db->select('POGUID');
			$this->db->where('PlantGUID',$token);
			return($this->db->get(PATPOD)->row()); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	/**
	 * Method getPATSocial() get detail Social Sustainability.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATSocial($token){

		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('Coverage,Affordability');
			$this->db->where('PlantGUID',$token);
			return($this->db->get(vw_patsocial)->row()); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

	/**
	 * Method getPATOperational() get detail Operational.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATOperational($token){

		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('Quality,Reliability');
			$this->db->where('PlantGUID',$token);
			return($this->db->get(vw_patoperational)->row()); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

	/**
	 * Method getPATfinancial() get detail Financial Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATfinancial($token){

		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('CapitalSource,OpExandRevenue');
			$this->db->where('PlantGUID',$token);
			//$this->db->get(vw_patfinancial)->row(); echo $this->db->last_query(); die;
			return($this->db->get(vw_patfinancial)->row()); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	/**
	 * Method getPATInstitutional() get detail Institutional Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATInstitutional($token){

		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('Regulatory,Training');
			$this->db->where('PlantGUID',$token);
			return($this->db->get(vw_patinstitutional)->row()); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}



	/**
	 * Method getPATInstitutional() get detail Institutional Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATEnvironmental($token,$poguid){

		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('REJECTWATERUTILIZATION');
			$this->db->where('PlantGUID',$token);
			$this->db->where('POGUID',$poguid);
			//$this->db->get(vw_patenvironmental)->row();
			//echo $this->db->last_query(); die;
			return($this->db->get(vw_patenvironmental)->row()); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	/**
	 * Method getPATSofiescore() get detail Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATSofiescore($token){

		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('SS,OS,FS,IS_t,ES,sum(SS+OS+FS+IS_t+ES) as total');
			$this->db->where('PlantGUID',$token);
			//$this->db->get(vw_plantsofiescore)->row(); 
			$result = $this->db->get(vw_patscore)->row();
			// echo $this->db->last_query(); die;
			return $result; 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

	public function getTotalScore($token = null)
	{

		$sql = "SELECT
    `tblpatplantdetail`.`PlantGUID` AS `PlantGUID`,
    `tblpatplantdetail`.`CountryID` AS `CountryID`,
    (
        IFNULL(`social`.`Affordability`, 0) + IFNULL(`social`.`Coverage`, 0)
    ) AS `SS`,
    (IFNULL(
        `finance`.`CapitalSource`,
        0
    ) + ifnull(finance.OpExandRevenue, 0)) AS `FS`,
    (
        IFNULL(insti.Regulatory,0)+IFNULL(insti.Training,0)
    ) AS `IS_t`,
    (
        IFNULL(`opera`.`Quality`, 0) + IFNULL(`opera`.`Reliability`, 0)
    ) AS `OS`,
    (
        CASE WHEN(`environ`.`total` = 100) THEN 20 WHEN(
            (`environ`.`total` < 100) AND(`environ`.`total` > 75)
        ) THEN 15 WHEN(
            (`environ`.`total` < 75) AND(`environ`.`total` > 50)
        ) THEN 10 WHEN(
            (`environ`.`total` < 50) AND(`environ`.`total` > 0)
        ) THEN 5 WHEN(`environ`.`total` = 0) THEN 0
    END
) AS `ES`
FROM
    (
        (
            (
                (
                    (
                        (select * from `tblpatplantdetail` where PlantGUID = '".$token."') as tblpatplantdetail
                    JOIN(
                        SELECT
    `a`.`PlantGUID` AS `PlantGUID`,
    (
        (
            (
                (
                    CASE `a`.`GramPanchayatApproval` WHEN 1 THEN 4 ELSE 0
                END
            ) +(
                CASE `a`.`LegalElectricityConnection` WHEN 1 THEN 2 ELSE 0
            END
        )
    ) +(
        CASE `a`.`LandApproval` WHEN 1 THEN 2 ELSE 0
    END
)
) +(
    CASE `a`.`RawWaterSourceApproval` WHEN 1 THEN 2 ELSE 0
END
)
) AS `Regulatory`,
(
    (
        (
            (
                (
                    CASE `b`.`ElectricalSafety` WHEN 1 THEN 2 ELSE 0
                END
            ) +(
                CASE `b`.`PlantOM` WHEN 1 THEN 2 ELSE 0
            END
        )
    ) +(
        CASE `b`.`WaterQuality` WHEN 1 THEN 2 ELSE 0
    END
)
) +(
    CASE `b`.`ConsumerAwareness` WHEN 1 THEN 2 ELSE 0
END
)
) +(
    CASE `b`.`ABKeeping` WHEN 1 THEN 2 ELSE 0
END
)
) AS `Training`
FROM
    (
        `tblpatplantdetail` `a`
    JOIN `tblpatpodetail` `b`
    ON
        ((`a`.`PlantGUID` = `b`.`PlantGUID`))
    )
) `insti`
ON
    (
        (
            `insti`.`PlantGUID` = `tblpatplantdetail`.`PlantGUID`
        )
    )
)
JOIN(
    SELECT
    `c`.`PlantGUID` AS `PlantGUID`,
    (`c`.`q1` + `c`.`q2`) AS `Quality`,
    (
        `c`.`technical_downtime` + `c`.`sales_downtime`
    ) AS `Reliability`
FROM
    (
    SELECT
        `tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
        (
            CASE WHEN(
                (
                    `tblpatpodetail`.`PreMonsoonTWTesting` = 1
                ) OR(
                    `tblpatpodetail`.`PostMonsoonTWtesting` = 1
                )
            ) THEN 5 ELSE 0
        END
) AS `q1`,
(
    CASE WHEN(
        (
            `tblpatpodetail`.`Appearance` = 1
        ) AND(
            `tblpatpodetail`.`Odour` = 1
        ) AND(
            `tblpatpodetail`.`Taste` = 1
        ) AND(
            `tblpatpodetail`.`TDSValue` > 0
        ) AND(
            `tblpatpodetail`.`TDSValue` < 200
        )
    ) THEN 5 WHEN(
        (
            `tblpatpodetail`.`Appearance` <> 1
        ) OR(
            `tblpatpodetail`.`Odour` <> 1
        ) OR(
            `tblpatpodetail`.`Taste` <> 1
        )
    ) THEN 0 else 0
END
) AS `q2`,
(
    CASE WHEN(
        (
            (
                `tblpatpodetail`.`NoOfDays` * 100
            ) / 30
        ) <= 10
    ) THEN 5 WHEN(
        (
            (
                (
                    `tblpatpodetail`.`NoOfDays` * 100
                ) / 30
            ) > 10
        ) AND(
            (
                (
                    `tblpatpodetail`.`NoOfDays` * 100
                ) / 30
            ) <= 25
        )
    ) THEN 3 WHEN(
        (
            (
                `tblpatpodetail`.`NoOfDays` * 100
            ) / 30
        ) > 25
    ) THEN 0
END
) AS `technical_downtime`,
(
    CASE WHEN(
        (
            (
                `tblpatpodetail`.`SalesDayLost` * 100
            ) / 30
        ) <= 5
    ) THEN 5 WHEN(
        (
            (
                (
                    `tblpatpodetail`.`SalesDayLost` * 100
                ) / 30
            ) > 5
        ) AND(
            (
                (
                    `tblpatpodetail`.`SalesDayLost` * 100
                ) / 30
            ) <= 10
        )
    ) THEN 2 WHEN(
        (
            (
                `tblpatpodetail`.`SalesDayLost` * 100
            ) / 30
        ) > 10
    ) THEN 0
END
) AS `sales_downtime`
FROM
    `tblpatpodetail`
) `c`
) `opera`
ON
    (
        (
            `opera`.`PlantGUID` = `tblpatplantdetail`.`PlantGUID`
        )
    )
)
left JOIN(
    SELECT
    `a`.`PlantGUID` AS `PlantGUID`,
    (`a`.`score1` + `b`.`score2`) AS `FinancialSustainability`,
    `a`.`score1` AS `CapitalSource`,
    `b`.`score2` AS `OpExandRevenue`,
    `b`.`countryid` AS `countryid`
FROM
    (
        (
            (
            SELECT
                `c`.`PlantGUID` AS `PlantGUID`,
                `c`.`POGUID` AS `poguid`,
                (
                    CASE `c`.`AssetFunderID` WHEN 1 THEN 10 WHEN 2 THEN 8 WHEN 3 THEN 6 WHEN 4 THEN 6 WHEN 5 THEN 4 ELSE 0
                END
        ) AS `score1`
    FROM
        `tblpatplantassetfunder` `c`
    WHERE
        (`c`.`FlagID` = 6)
    GROUP BY
        `c`.`PlantGUID`
    ORDER BY
        `c`.`PlantGUID`,
        `c`.`AssetFunderID`
        )
    ) `a`
JOIN(
    SELECT
        `a`.`PlantGUID` AS `PlantGUID`,
        `a`.`POGUID` AS `POGUID`,
        (
            CASE WHEN(
                SUM((`a`.`Price` * `a`.`Volume`)) <= `b`.`OPEx`
            ) THEN 0 WHEN(
                (
                    `b`.`OPEx` < SUM((`a`.`Price` * `a`.`Volume`))
                ) AND(
                    SUM((`a`.`Price` * `a`.`Volume`)) <=(`b`.`OPEx` + `b`.`OPExservicecharge`)
                )
            ) THEN 2 WHEN(
                (
                    (`b`.`OPEx` + `b`.`OPExservicecharge`) < SUM((`a`.`Price` * `a`.`Volume`))
                ) AND(
                    SUM((`a`.`Price` * `a`.`Volume`)) <=(
                        (`b`.`OPEx` + `b`.`OPExservicecharge`) +(0.5 * `b`.`AssestRepaymentFund`)
                    )
                )
            ) THEN 6 WHEN(
                (
                    (
                        (`b`.`OPEx` + `b`.`OPExservicecharge`) +(0.5 * `b`.`AssestRepaymentFund`)
                    ) < SUM((`a`.`Price` * `a`.`Volume`))
                ) AND(
                    SUM((`a`.`Price` * `a`.`Volume`)) <=(
                        (`b`.`OPEx` + `b`.`OPExservicecharge`) + `b`.`AssestRepaymentFund`
                    )
                )
            ) THEN 8 WHEN(
                SUM((`a`.`Price` * `a`.`Volume`)) >=(
                    (`b`.`OPEx` + `b`.`OPExservicecharge`) + `b`.`AssestRepaymentFund`
                )
            ) THEN 10
        END
) AS `score2`,
`p`.`CountryID` AS `countryid`
FROM
    (
        (
            `tblpatplantproddetail` `a`
        JOIN `tblpatpodetail` `b`
        ON
            (
                (
                    (`a`.`PlantGUID` = `b`.`PlantGUID`) AND(`b`.`POGUID` = `b`.`POGUID`)
                )
            )
        )
    JOIN `tblpatplantdetail` `p`
    ON
        ((`b`.`PlantGUID` = `p`.`PlantGUID`))
    )
GROUP BY
    `a`.`PlantGUID`,
    `a`.`POGUID`
ORDER BY
    `a`.`PlantGUID`,
    `a`.`POGUID`
) `b`
ON
    (
        (
            (`a`.`PlantGUID` = `b`.`PlantGUID`) AND(`a`.`poguid` = `b`.`POGUID`)
        )
    )
)
) `finance`
ON
    (
        (
            `finance`.`PlantGUID` = `tblpatplantdetail`.`PlantGUID`
        )
    )
)
JOIN(
    SELECT
        `tblpatpodetail`.`PlantGUID` AS `PlantgUID`,
        `tblpatpodetail`.`POGUID` AS `POGUID`,
        (
            CASE WHEN(
                `tblpatpodetail`.`WQuantification` = 0
            ) THEN 100 WHEN(
                `tblpatpodetail`.`WQuantification` > 0
            ) THEN(
                (
                    (
                        (
                            (
                                `tblpatpodetail`.`UtilizationOther1` + `tblpatpodetail`.`UtilizationOther2`
                            ) + `tblpatpodetail`.`UtilizationOther3`
                        ) + `tblpatpodetail`.`UtilizationOther4`
                    ) * 100
                ) / `tblpatpodetail`.`WQuantification`
            )
        END
) AS `total`
FROM
(select * from `tblpatpodetail` where PlantGUID = '".$token."') as tblpatpodetail
) `environ`
ON
    (
        (
            `environ`.`PlantGUID` = `tblpatplantdetail`.`PlantGUID`
        )
    )
)
LEFT JOIN(
    SELECT
    t.PlantGUID AS PlantGUID,
    t.Coverage AS Coverage,
    t.WIC AS Affordability
FROM
    (
    SELECT
        safewaternetwork.tblpatplantdetail.PlantGUID AS PlantGUID,
        (
            CASE WHEN(
                (
                    (
                        safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                    ) *100
                ) > 80
            ) THEN 10 WHEN(
                (
                    (
                        (
                            safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                        ) *100
                    ) <= 80
                ) AND(
                    (
                        (
                            safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                        ) *100
                    ) > 60
                )
            ) THEN 8 WHEN(
                (
                    (
                        (
                            safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                        ) *100
                    ) <= 60
                ) AND(
                    (
                        (
                            safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                        ) *100
                    ) > 40
                )
            ) THEN 6 WHEN(
                (
                    (
                        (
                            safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                        ) *100
                    ) <= 40
                ) AND(
                    (
                        (
                            safewaternetwork.tblpatplantdetail.NoOfhhregistered / safewaternetwork.tblpatplantdetail.NoOfHousehold
                        ) * 100
                    ) > 20
                )
            ) THEN 4 ELSE 0
        END
) AS Coverage,
IFNULL(
    (
        CASE WHEN(
            (
                safewaternetwork.tblpatplantdetail.LocationType = 1
            ) AND(atplant.Price <= 0.25)
        ) THEN 10 WHEN(
            (
                safewaternetwork.tblpatplantdetail.LocationType = 1
            ) AND(atplant.Price > 0.25) AND(atplant.Price < 0.5)
        ) THEN 6 WHEN(
            (
                safewaternetwork.tblpatplantdetail.LocationType = 1
            ) AND(atplant.Price > 0.5)
        ) THEN 0 WHEN(
            (
                safewaternetwork.tblpatplantdetail.LocationType = 2
            ) AND(atplant.Price <= 0.10)
        ) THEN 10 WHEN(
            (
                safewaternetwork.tblpatplantdetail.LocationType = 2
            ) AND(atplant.Price > 0.10) AND(atplant.Price < 0.25)
        ) THEN 6 WHEN(
            (
                safewaternetwork.tblpatplantdetail.LocationType = 2
            ) AND (atplant.Price > 0.25)
        ) THEN 0 ELSE 0
    END
),
0
) AS WIC,
IFNULL(
    (
        CASE WHEN (atdp.Price < 4) THEN 4 WHEN(
            (atdp.Price >= 4) AND (atdp.Price < 6)
        ) THEN 3 WHEN(
            (atdp.Price >= 6) AND (atdp.Price < 8)
        ) THEN 2 WHEN(
            
                atdp.Price >=8
            ) THEN 1 ELSE 0
        END
    ),
    0
) AS DPATM,
IFNULL(
    (
        CASE WHEN (hd.Price < 15) THEN 2 WHEN (hd.Price = 15) THEN 1 ELSE 0
    END
),
0
) AS HomeDeliveryPrice
FROM
    (
        (
            (
                (
                    (
                        safewaternetwork.tblpatplantdetail
                    JOIN safewaternetwork.tblpatpodetail ON
                        (
                            (
                                safewaternetwork.tblpatpodetail.PlantGUID = safewaternetwork.tblpatplantdetail.PlantGUID
                            )
                        )
                    )
                JOIN(
                    SELECT
                        safewaternetwork.tblpatpodetail.PlantGUID AS PlantGUID,
                        safewaternetwork.tblpatpodetail.POGUID AS POGUID,
                        safewaternetwork.tblpatpodetail.VisitDate AS VisitDate
                    FROM
                        (
                            (
                                safewaternetwork.tblpatpodetail
                            JOIN safewaternetwork.tblpatplantdetail ON
                                (
                                    (
                                        safewaternetwork.tblpatpodetail.PlantGUID = safewaternetwork.tblpatplantdetail.PlantGUID
                                    )
                                )
                            )
                        JOIN(
                            SELECT
                                safewaternetwork.tblpatpodetail.POGUID AS POGUID,
                                safewaternetwork.tblpatpodetail.PlantGUID AS PlantGUID,
                                MAX(
                                    safewaternetwork.tblpatpodetail.VisitDate
                                ) AS VisitDate
                            FROM
                                safewaternetwork.tblpatpodetail
                            GROUP BY
                                safewaternetwork.tblpatpodetail.PlantGUID
                        ) t
                    ON
                        (
                            (
                                safewaternetwork.tblpatpodetail.POGUID = t.POGUID
                            )
                        )
                        )
                    ORDER BY
                        safewaternetwork.tblpatplantdetail.PlantGUID,
                        safewaternetwork.tblpatpodetail.VisitDate
                    DESC
                ) currentplantpo
            ON
                (
                    (
                        (
                            safewaternetwork.tblpatpodetail.PlantGUID = currentplantpo.PlantGUID
                        ) AND(
                            safewaternetwork.tblpatpodetail.POGUID = currentplantpo.POGUID
                        )
                    )
                )
                )
            LEFT JOIN(
                SELECT
                    safewaternetwork.tblpatplantproddetail.PlantGUID AS PlantGUID,
                    safewaternetwork.tblpatplantproddetail.POGUID AS POGUID,
                    safewaternetwork.tblpatplantproddetail.Price AS Price,
                    safewaternetwork.tblpatplantproddetail.HomeDeliveryPrice AS HomeDeliveryPrice
                FROM
                    safewaternetwork.tblpatplantproddetail
                WHERE
                    (
                        safewaternetwork.tblpatplantproddetail.DistributionplantID = 0
                    )
            ) atplant
        ON
            (
                (
                    (
                        atplant.PlantGUID = currentplantpo.PlantGUID
                    ) AND(
                        atplant.POGUID = currentplantpo.POGUID
                    )
                )
            )
            )
        LEFT JOIN(
            SELECT
                safewaternetwork.tblpatplantproddetail.PlantGUID AS PlantGUID,
                safewaternetwork.tblpatplantproddetail.POGUID AS POGUID,
                safewaternetwork.tblpatplantproddetail.Price AS Price,
                safewaternetwork.tblpatplantproddetail.HomeDeliveryPrice AS HomeDeliveryPrice
            FROM
                safewaternetwork.tblpatplantproddetail
            WHERE
                (
                    safewaternetwork.tblpatplantproddetail.DistributionplantID = 99
                )
        ) hd
    ON
        (
            (
                (
                    hd.PlantGUID = currentplantpo.PlantGUID
                ) AND(
                    hd.POGUID = currentplantpo.POGUID
                )
            )
        )
        )
    LEFT JOIN(
        SELECT
            safewaternetwork.tblpatplantproddetail.PlantGUID AS PlantGUID,
            safewaternetwork.tblpatplantproddetail.POGUID AS POGUID,
            AVG(
                safewaternetwork.tblpatplantproddetail.Price
            ) AS Price,
            AVG(
                safewaternetwork.tblpatplantproddetail.HomeDeliveryPrice
            ) AS HomeDeliveryPrice
        FROM
            safewaternetwork.tblpatplantproddetail
        WHERE
            (
                safewaternetwork.tblpatplantproddetail.DistributionplantID > 0
            )
        GROUP BY
            safewaternetwork.tblpatplantproddetail.PlantGUID,
            safewaternetwork.tblpatplantproddetail.POGUID
    ) atdp
ON
    (
        (
            (
                atdp.PlantGUID = currentplantpo.PlantGUID
            ) AND(
                atdp.POGUID = currentplantpo.POGUID
            )
        )
    )
    )
) t
) `social`
ON
    (
        (
            `social`.`PlantGUID` = `tblpatplantdetail`.`PlantGUID`
        )
    )
)";

$result = $this->db->query($sql)->result();
// print_r($result); die();
// echo $this->db->last_query(); die();

return $result[0];

	}



}