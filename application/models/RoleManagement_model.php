<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RoleManagement_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all('user_role_mapping');
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			
			$this->db->limit($limit,$start);
			return $this->db->get(PATPD)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
		
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {

			$form = $this->input->post();
			//print_r($form); die;

			$roleID = $this->input->post('role_id');

			$query = "delete from user_role_mapping where role_id = ?";
			$this->db->query($query, [$roleID]);
			///echo "dvgdg"; die;
			$permission = $this->input->post('permission');
			//print_r($permission);

			foreach ($permission as $key => $value) {
		   		foreach ($value as $Mkey => $Mvalue) {
		   			$insertquery="Insert into user_role_mapping(`role_id`,`controller`,`method`) 
		   			value('".$roleID."','".$key."','".$Mkey."')";
		   			$data = $this->db->query($insertquery);
				}
	   		}
//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	
	
	
	public function viewDetail()
	{
		try {
			$this->db->select('*');
			return $this->db->get(user_role_mapping)->result(); 
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			//echo $token;
			$form = $this->input->post();
			print_r($form);
			die;
					
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['status_id']  = '2';
			$form['modify_by'] = 1;//$this->loggedIn;
			$form['modify_ip'] = $this->fromIP;
			$this->db->set('modify_date', 'NOW()', FALSE);
			$this->db->where('user_id',(int)$token);
			return ($this->db->update(user_role_mapping,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getRole() get all Role.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getRole()
	{
		$this->db->select('RoleID,RoleName');
		$this->db->where('IsDeleted','0');
		$this->db->where('RoleID !=','1');
		$this->db->order_by('RoleID','asc');
		return $this->db->get(mstrole)->result();
	}

}