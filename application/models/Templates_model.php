<?php
class Templates_model extends CI_Model
{
	public $loggedIn = 0;
	public $fromIP = '';
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('ACCESS');
		$this->fromIP   = $_SERVER['REMOTE_ADDR'];
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows()
	{
		try {
			return $this->db->count_all(TEMPLATES);
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword='0',$status='0',$Order_by='0',$template='0')
	{
		try {
		//echo "<pre>";
		    //$status = $_POST['status_id']; 
			$this->db->select('CT.template_type,CT.added_date,UP.title,CT.modify_date,ST.status,CT.template_id as token');
			$this->db->join(STATUS.' AS ST','ST.status_id=CT.status_id','left');
			$this->db->join(UPDATES.' AS UP','UP.update_id=CT.update_id','left');
			  if(isset($status) && $status !='') {
			   $this->db->where('ST.status_id',$_POST['status_id']);
			   }
			   if(isset($keyword) && $keyword !='') {
			   $this->db->like('CT.html_header',$keyword);
			   $this->db->or_like('CT.html_footer',$keyword);
			   $this->db->or_like('CT.template_type',$keyword);
			   }
			   if($template ==''){
				 $this->db->where('1=1');
			}else{
			     $this->db->or_like('CT.template_type',$template);
			}
			if(isset($Order_by) && $Order_by !=''){
			if($Order_by == "Template Type"){
			$this->db->order_by('CT.template_id','asc');
			}elseif($Order_by == "Html Header"){
			$this->db->order_by('CT.html_header','asc');
			}elseif($Order_by == "Html Footer"){
			$this->db->order_by('CT.html_footer','asc');
			}elseif($Order_by == "Word Header"){
			$this->db->order_by('CT.word_header','asc');
			}elseif($Order_by == "Word Footer"){
			$this->db->order_by('CT.word_footer','asc');
			}elseif($Order_by == "Add Date"){
			$this->db->order_by('CT.added_date','asc');
			}else
			{
			$this->db->order_by('CT.modify_date','asc');
			}
			}
			$this->db->limit($limit,$start);
			$return = $this->db->get(TEMPLATES.' AS CT')->result();  //echo $this->db->last_query();
			
			return $return;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			$form['added_by'] = $this->loggedIn;
			$form['added_ip'] = $this->fromIP;
			return ($this->db->insert(TEMPLATES,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function getDetail($token)
	{
		try {
			$this->db->select('CT.*');
			$this->db->where('CT.template_id',(int)$token);
			return $this->db->get(TEMPLATES.' AS CT')->row();
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function viewDetail($token)
	{
		try {
			$this->db->select('CT.template_type,CT.html_header,CT.html_footer,CT.word_header,CT.word_footer,CT.added_date,CT.modify_date,ST.status');
			$this->db->join(UPDATES.' AS UP','UP.update_id=CT.update_id','left');
			$this->db->join(STATUS.' AS ST','ST.status_id=CT.status_id','inner');
			$this->db->where('CT.template_id',(int)$token);
			return $this->db->get(TEMPLATES.' AS CT')->row();
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method edit() update detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function edit($token)
	{
		try {
			$form = $this->input->post('form');
			$form['modify_by'] = $this->loggedIn;
			$form['modify_ip'] = $this->fromIP;
			$this->db->set('modify_date', 'NOW()', FALSE);
			$this->db->where('template_id',(int)$token);
			return ($this->db->update(TEMPLATES,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method delete() delete detail.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */
	public function delete($token)
	{
		try {
			$form['status_id']  = '2';
			$form['modify_by'] = 1;//$this->loggedIn;
			$form['modify_ip'] = $this->fromIP;
			$this->db->set('modify_date', 'NOW()', FALSE);
			$this->db->where('template_id',(int)$token);
			return ($this->db->update(TEMPLATES,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	
	
	/**
	 * Method getUpdates() get all update.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUpdates()
	{
		$this->db->select('update_id,title');
		$this->db->where('status_id','1');
		$this->db->order_by('title','asc');
		return $this->db->get(UPDATES)->result();
	}
	
	/**
	 * Method name_validation() check duplicate name.
	 * @access	public
	 * @param	Null
	 * @return	array
	 */
	/*public function name_validation()
	{
		try {
			$form = $this->input->post('form');
			$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
			$name = isset($form['template_type']) ? $form['template_type'] : ''; //print_r($form);die;
			
			$this->db->select("COUNT(1) AS CNT");
			$this->db->where('template_type',$name);
			if($token>0) {
				$this->db->where('template_id!='.(int)$token);	
			}
			$row = $this->db->get(TEMPLATES)->row();//print_r($row->CNT);die;//echo $this->db->last_query();die;
			if ($row->CNT > 0){
				$this->form_validation->set_message('name_validation', '{field} should be unique.');
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}*/
}