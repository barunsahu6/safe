<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TSTDashboard_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			$this->db->limit($limit,$start);
			return $this->db->get(DISTRICT)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	 /**
	 * Method DashBoard_Contamination() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoard_Contamination()
	{
		
		$procedure = "SELECT
					p.ParameterID,
					p.Name,
					IFNULL(b.within_limit, 0) AS within_limit,
					IFNULL(c.not_within_limit, 0) AS not_within_limit
				FROM
					`mstparameter` p
				LEFT JOIN(
					SELECT b.Name,
						COUNT(*) AS within_limit,
						b.ParameterID
					FROM
						(
						SELECT
							p.LocationGUID,
							p.ParameterValue,
							sp.ParameterID,
							sp.Name,
							sp.AcceptableLimit
						FROM
							`tbltstsampletestresult` p
						INNER JOIN(
							SELECT
								sp.ParameterID,
								p.Name,
								sp.AcceptableLimit
							FROM
								`mststandardparameterdetail` sp
							INNER JOIN mstparameter p ON
								sp.ParameterID = p.ParameterID
							WHERE
								sp.StandardID = 2
						) sp
					ON
						p.ParameterID = sp.ParameterID
					) b
				WHERE
					b.ParameterValue <= b.AcceptableLimit
				GROUP BY
					b.ParameterID
				) b
				ON
					p.ParameterID = b.ParameterID
				LEFT JOIN(
					SELECT b.Name,
						COUNT(*) AS not_within_limit,
						b.ParameterID
					FROM
						(
						SELECT
							p.LocationGUID,
							p.ParameterValue,
							sp.ParameterID,
							sp.Name,
							sp.AcceptableLimit
						FROM
							`tbltstsampletestresult` p
						INNER JOIN(
							SELECT
								sp.ParameterID,
								p.Name,
								sp.AcceptableLimit
							FROM
								`mststandardparameterdetail` sp
							INNER JOIN mstparameter p ON
								sp.ParameterID = p.ParameterID
							WHERE
								sp.StandardID = 2
						) sp
					ON
						p.ParameterID = sp.ParameterID
					) b
				WHERE
					b.ParameterValue > b.AcceptableLimit
				GROUP BY
					b.ParameterID
				) c
				ON
					p.ParameterID = c.ParameterID";
		
			$data = $this->db->query($procedure)->result(); 
		//return json_encode($data);
		return $data;
	}





	 /**
	 * Method DashBoard_Contamination() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoard_India_Contamination()
	{
		
		$procedure = "SELECT
					p.ParameterID,
					p.Name,
					IFNULL(b.within_limit, 0) AS within_limit,
					IFNULL(c.not_within_limit, 0) AS not_within_limit
				FROM
					`mstparameter` p
				LEFT JOIN(
					SELECT b.Name,
						COUNT(*) AS within_limit,
						b.ParameterID
					FROM
						(
						SELECT
							p.LocationGUID,
							p.ParameterValue,
							sp.ParameterID,
							sp.Name,
							sp.AcceptableLimit
						FROM
							`tbltstsampletestresult` p
						INNER JOIN(
							SELECT
								sp.ParameterID,
								p.Name,
								sp.AcceptableLimit
							FROM
								`mststandardparameterdetail` sp
							INNER JOIN mstparameter p ON
								sp.ParameterID = p.ParameterID
							WHERE
								sp.StandardID = 1
						) sp
					ON
						p.ParameterID = sp.ParameterID
					) b
				WHERE
					b.ParameterValue <= b.AcceptableLimit
				GROUP BY
					b.ParameterID
				) b
				ON
					p.ParameterID = b.ParameterID
				LEFT JOIN(
					SELECT b.Name,
						COUNT(*) AS not_within_limit,
						b.ParameterID
					FROM
						(
						SELECT
							p.LocationGUID,
							p.ParameterValue,
							sp.ParameterID,
							sp.Name,
							sp.AcceptableLimit
						FROM
							`tbltstsampletestresult` p
						INNER JOIN(
							SELECT
								sp.ParameterID,
								p.Name,
								sp.AcceptableLimit
							FROM
								`mststandardparameterdetail` sp
							INNER JOIN mstparameter p ON
								sp.ParameterID = p.ParameterID
							WHERE
								sp.StandardID = 1
						) sp
					ON
						p.ParameterID = sp.ParameterID
					) b
				WHERE
					b.ParameterValue > b.AcceptableLimit
				GROUP BY
					b.ParameterID
				) c
				ON
					p.ParameterID = c.ParameterID";
		
			$data = $this->db->query($procedure)->result(); 
		//return json_encode($data);
		return $data;
	}

	/**
	 * Method add() add detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */
	public function add()
	{
		try {
			$form = $this->input->post('form');
			//print_R($form); die;
			return ($this->db->insert(DISTRICT,$form)) ? 1 : -1;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
/**
	 * Method DatafromOnWebService_Map() get Location Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function DatafromOnWebService_Map($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		// $sql = "SELECT DISTINCT 
        // 		tbltstlocation.VillageName AS description,
		// 		tbltstlocation.XValue AS Latitude,
		// 		tbltstlocation.YValue AS Longitude
		// 		FROM tbltstlocation WHERE 1=1";

		$sql = "SELECT DISTINCT tbt.VillageName AS description, tbt.XValue AS Latitude, 
		tbt.YValue AS Longitude FROM tbltstlocation AS tbt 
		LEFT JOIN tblpatpodetail AS tppd ON tppd.PlantGUID = tbt.GUID 
		WHERE 1=1";
		if(!empty($strwhr)){
			 $sql .= " AND tbt.`CountryID` = '".$strwhr."'";
		}
		if(!empty($strstatewhr)){
			 $sql .= " AND tbt.`StateID` = '".$strstatewhr."'";
		}
		if(!empty($districtwhr)){
			 $sql .= " AND tbt.`DistrictID` = '".$districtwhr."'";
		}
		if(!empty($plantwhr)){
			$sql .= " AND tbt.PlantGUID = '".$plantwhr."'";
		}
		$sql .= " ORDER BY tbt.`LocationUID` ASC";
		echo $sql; //die;
		return($data = $this->db->query($sql)->result());
	}
	
	
	/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}