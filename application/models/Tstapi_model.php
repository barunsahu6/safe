<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		ini_set('display_errors', 1);
class Tstapi_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	public $Approval = '';
	public $plantguid = "";
	public $plantpoguid = "";
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('Common_Model');
	}
	
	public function uploaddata()
	{
		$this->plantguid = uniqid();
		$this->plantpoguid = uniqid();

		$data = $this->input->post('data');
		// echo $data; die();
		$data_array = json_decode($data);
		//echo "amit";
		//print_r($data_array);
		
		
		foreach ($data_array as $tablename => $tabledata) {
			
			switch($tablename)
			{
				case "tblTSTLocation" : $this->tblTSTLocationType($tabledata);
				break;
				case "tblTSTSampleInfo" : $this->tblTSTSampleInfoType($tabledata);
				break;
				case "tblTSTSampleTestResult" : $this->tblTSTSampleTestResultType($tabledata);
				break;
				case "tblTSTLaboratoryDetails" : $this->tblTSTLaboratoryDetailsType($tabledata);
				break;
			}

		}
		echo json_encode('success');

	}

	public function tblTSTSampleInfoType($tabledata)
	{	
		
		foreach($tabledata as $row){
			 $row->CourieredDate = date("Y-m-d", strtotime($row->CourieredDate));
			 $row->CollectedDate = date("Y-m-d", strtotime($row->CollectedDate));
			 $row->UpdatedOn = date("Y-m-d", strtotime($row->UpdatedOn));
		 	 $row->CreatedOn = date("Y-m-d", strtotime($row->CreatedOn));
			 $CollectedTime = substr($row->CollectedTime, 0, 10);
			 $row->CollectedTime = date("G:i", strtotime($CollectedTime));
			 $CourieredTime = substr($row->CourieredTime, 0, 10);
			 $row->CourieredTime = date("G:i", strtotime($CourieredTime));
			
			  unset($row->IsEdited);
			  unset($row->IsUploaded);
			
			$sql_record_exists = "SELECT * FROM `tbltstsampleinfo` where SampleGUID = '".$row->SampleGUID."'";

			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			if(count($res_record_exists) > 0)
			{

				$this->Common_Model->update_data('tbltstsampleinfo', $row, 'SampleGUID',$row->SampleGUID);
			}
			else
			{
				$this->Common_Model->insert_data('tbltstsampleinfo', $row); //echo $this->db->last_query(); die;
			}
		}
	}

	public function tblTSTSampleTestResultType($tabledata)
	{	
	
		foreach ($tabledata as $row) {
			 $row->UpdatedOn = date("Y-m-d", strtotime($row->UpdatedOn));
		 	 $row->CreatedOn = date("Y-m-d", strtotime($row->CreatedOn));
			unset($row->IsEdited);
			unset($row->IsUploaded);
			unset($row->TestUID);
			# code...
			$sql_record_exists = "SELECT * FROM `tbltstsampletestresult` where TestGUID = '".$row->TestGUID."'";

			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			if(count($res_record_exists) > 0)
			{

				$this->Common_Model->update_data('tbltstsampletestresult', $row, 'TestGUID',$row->TestGUID);
			}
			else
			{
				$this->Common_Model->insert_data('tbltstsampletestresult', $row); //echo $this->db->last_query(); die;
			}
		}
	}

	public function tblTSTLaboratoryDetailsType($row)
	{	
		foreach ($row as $res) {
			 $res->ReportDate = date("Y-m-d", strtotime($res->ReportDate));
			 $res->UpdatedOn = date("Y-m-d", strtotime($res->UpdatedOn));
			 $res->CreatedOn = date("Y-m-d", strtotime($res->CreatedOn));

			unset($row[0]->IsUploaded); 
			unset($row[0]->IsEdited); 
			$sql_record_exists = "SELECT * FROM `tbltstlaboratorydetails` where LabGUID = '".$res->LabGUID."'";

			//echo $sql_record_exists; die();
			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			// print_r($res_record_exists); die();
			if(count($res_record_exists) > 0)
			{

				$this->Common_Model->update_data('tbltstlaboratorydetails', $res, 'LabGUID',$res->LabGUID);
			}
			else
			{
				$this->Common_Model->insert_data('tbltstlaboratorydetails', $res); //echo $this->db->last_query(); die;
			}
		}
	}

	public function tblTSTLocationType($tabledata)
	{	
		foreach($tabledata as $row){
					
			 $row->UpdatedOn = date("Y-m-d", strtotime($row->UpdatedOn));
			 $row->CreatedOn = date("Y-m-d", strtotime($row->CreatedOn));

			unset($row->IsUploaded); 
			unset($row->IsEdited);
			unset($row->UID); 
			$sql_record_exists = "SELECT * FROM `tbltstlocation` where GUID = '".$row->GUID."'";

			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			if(count($res_record_exists) > 0)
			{

				$this->Common_Model->update_data('tbltstlocation', $row, 'GUID',$row->GUID);
			}
			else
			{
				$this->Common_Model->insert_data('tbltstlocation', $row);
			}
		}
	
	}

	
}