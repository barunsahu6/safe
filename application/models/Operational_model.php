<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Operational_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
/**
	 * Method getDashboardRuralAffordability() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getTechnicalDowntime($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql = " SELECT ROUND(((`c`.`NoOfDays` * 100) / 30)) AS `technical_downtime` FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`NoOfDays` AS `NoOfDays` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS c 
		INNER JOIN `tblpatplantdetail` AS ppd ON ppd.PlantGUID = c.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID 
		WHERE 1=1";
			}
			else
			{
				$sql = " SELECT ROUND(((`c`.`NoOfDays` * 100) / 30)) AS `technical_downtime` FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`NoOfDays` AS `NoOfDays` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS c 
		INNER JOIN `tblpatplantdetail` AS ppd ON ppd.PlantGUID = c.PlantGUID 
		WHERE 1=1";
			}

		

			if($strwhr != "")
			{
			$sql .= " AND ppd.CountryID ='".$strwhr."' "; 
			}
			if($strstatewhr != ""){
				$sql .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
			}
			if($districtwhr != ""){
				$sql .= " AND ppd.District = '".$districtwhr."' "; 
			}
			if($plantwhr != ""){
				$sql .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
			}
			//echo $sql;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getDashboardUrbanAffordability() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getSalesDowntime($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql = " SELECT ROUND(((`c`.`SalesDayLost` * 100) / 30)) AS `sales_downtime` FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`SalesDayLost` AS `SalesDayLost` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS c 
					INNER JOIN `tblpatplantdetail` AS ppd ON ppd.PlantGUID = c.PlantGUID  INNER JOIN honeywell_patplantdetail ON ppd.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE 1=1 ";
			}
			else
			{
				$sql = " SELECT ROUND(((`c`.`SalesDayLost` * 100) / 30)) AS `sales_downtime` FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`SalesDayLost` AS `SalesDayLost` from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) AS c 
					INNER JOIN `tblpatplantdetail` AS ppd ON ppd.PlantGUID = c.PlantGUID WHERE 1=1 ";
			}

				
				    
						if($strwhr != "")
						{
						$sql1 .= " AND ppd.CountryID ='".$strwhr."' "; 
						}
						if($strstatewhr != ""){
							$sql1 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
						}
						if($districtwhr != ""){
							$sql1 .= " AND ppd.District = '".$districtwhr."' "; 
						}
						if($plantwhr != ""){
							$sql1 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
						}
						
			//echo $sql;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	//////////////////////// Get Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChartPre($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = "SELECT 'premonsoon' as label, COUNT(pod.`PreMonsoonTWTesting`) as 'value'
						FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`PreMonsoonTWTesting` 
AS `PreMonsoonTWTesting`,`tblpatpodetail`.`PreMonsoonTWTestingdate` AS `PreMonsoonTWTestingdate` 
from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) as pod LEFT JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID  INNER JOIN honeywell_patplantdetail ON pod.PlantGUID = honeywell_patplantdetail.PlantGUID 
						WHERE pod.PreMonsoonTWTesting=1 AND pod.`PreMonsoonTWTestingdate` BETWEEN (CURRENT_DATE - INTERVAL 6 MONTH) and CURRENT_DATE ";

						$sql2 = "SELECT 'postmonsoon' as label, COUNT(pod.`PostMonsoonTWtesting`) as 'value'
						FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`PostMonsoonRWTesting` 
AS `PostMonsoonRWTesting`,`tblpatpodetail`.`PostMonsoonTWTestingdate` AS `PostMonsoonTWTestingdate`,
`tblpatpodetail`.`PostMonsoonTWtesting` AS `PostMonsoonTWtesting`  
from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) as pod LEFT JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID INNER JOIN honeywell_patplantdetail ON pod.PlantGUID = honeywell_patplantdetail.PlantGUID 
						WHERE pod.PostMonsoonRWTesting=1 AND pod.`PostMonsoonTWTestingdate` BETWEEN (CURRENT_DATE - INTERVAL 6 MONTH) and CURRENT_DATE ";
			}
			else
			{
				$sql1 = "SELECT 'premonsoon' as label, COUNT(pod.`PreMonsoonTWTesting`) as 'value'
						FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`PreMonsoonTWTesting` 
AS `PreMonsoonTWTesting`,`tblpatpodetail`.`PreMonsoonTWTestingdate` AS `PreMonsoonTWTestingdate` 
from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) as pod LEFT JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID
						WHERE pod.PreMonsoonTWTesting=1 AND pod.`PreMonsoonTWTestingdate` BETWEEN (CURRENT_DATE - INTERVAL 6 MONTH) and CURRENT_DATE ";

						$sql2 = "SELECT 'postmonsoon' as label, COUNT(pod.`PostMonsoonTWtesting`) as 'value'
						FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`PostMonsoonRWTesting` 
AS `PostMonsoonRWTesting`,`tblpatpodetail`.`PostMonsoonTWTestingdate` AS `PostMonsoonTWTestingdate`,
`tblpatpodetail`.`PostMonsoonTWtesting` AS `PostMonsoonTWtesting`  
from `tblpatpodetail` 
group by `tblpatpodetail`.`PlantGUID`) as pod LEFT JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID
						WHERE pod.PostMonsoonRWTesting=1 AND pod.`PostMonsoonTWTestingdate` BETWEEN (CURRENT_DATE - INTERVAL 6 MONTH) and CURRENT_DATE ";
			}

			
						
							if($strwhr != ""){
								$sql1 .= " AND ppd.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql1 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql1 .= " AND ppd.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql1 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
							}
						
            
						
							if($strwhr != ""){
								$sql2 .= " AND ppd.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql2 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql2 .= " AND ppd.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql2 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
							}
					
				$sql = "$sql1 union $sql2 ";		
				//echo $sql;
				$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	

	public function CreatePieChartTDS($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		try{

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$sql1 = " SELECT 'Appearance' as lable, COUNT(`Appearance`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`Appearance` 
AS `Appearance`from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID INNER JOIN honeywell_patplantdetail ON pod.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `Appearance`=1 ";

						$sql2 = "SELECT 'Odour' as lable, COUNT(`Odour`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`Odour` AS `Odour` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID INNER JOIN honeywell_patplantdetail ON pod.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `Odour`=1 ";

						$sql3 = "SELECT 'Taste' as lable, COUNT(`Taste`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`Taste` AS `Taste` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID INNER JOIN honeywell_patplantdetail ON pod.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `Taste`=1"; 

						$sql4 = "SELECT 'TDSValue' as lable, COUNT(`TDSValue`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`TDSValue` AS `TDSValue` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID INNER JOIN honeywell_patplantdetail ON pod.PlantGUID = honeywell_patplantdetail.PlantGUID  WHERE `TDSValue` > 0 AND `TDSValue` < 200 ";
			}
			else
			{
				$sql1 = " SELECT 'Appearance' as lable, COUNT(`Appearance`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`Appearance` 
AS `Appearance`from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID WHERE `Appearance`=1 ";

						$sql2 = "SELECT 'Odour' as lable, COUNT(`Odour`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`Odour` AS `Odour` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID WHERE `Odour`=1 ";

						$sql3 = "SELECT 'Taste' as lable, COUNT(`Taste`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`Taste` AS `Taste` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID WHERE `Taste`=1"; 

						$sql4 = "SELECT 'TDSValue' as lable, COUNT(`TDSValue`) as 'value' FROM (select `tblpatpodetail`.`POGUID` AS `POGUID`,`tblpatpodetail`.`PlantGUID` AS `PlantGUID`,
max(`tblpatpodetail`.`VisitDate`) AS `maxVisitDate`,`tblpatpodetail`.`TDSValue` AS `TDSValue` from `tblpatpodetail` group by `tblpatpodetail`.`PlantGUID`) as pod 
						INNER JOIN `tblpatplantdetail` as ppd ON ppd.PlantGUID=pod.PlantGUID WHERE `TDSValue` > 0 AND `TDSValue` < 200 ";
			}
			
			
						
						if($strwhr != ""){
								$sql1 .= " AND ppd.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql1 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql1 .= " AND ppd.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql1 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
							}
						
						
						
						if($strwhr != ""){
								$sql2 .= " AND ppd.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql2 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql2 .= " AND ppd.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql2 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
							}

						
						
						
						if($strwhr != ""){
								$sq3 .= " AND ppd.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql3 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql3 .= " AND ppd.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql3 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
							}
						
						
						
						
						  if($strwhr != ""){
								$sq4 .= " AND ppd.CountryID ='".$strwhr."' "; 
							}
							if($strstatewhr != ""){
								$sql4 .= " AND ppd.`StateID` = '".$strstatewhr."' "; 
							}
							if($districtwhr != ""){
								$sql4 .= " AND ppd.District = '".$districtwhr."' "; 
							}
							if($plantwhr != ""){
								$sql5 .= " AND ppd.PlantGUID = '".$plantwhr."' "; 
							}
						
				$sql = "$sql1 union $sql2 union $sql3 union $sql4 ";		
		
			//echo $sql;
			$data = $this->db->query($sql)->result(); //echo $this->db->last_query(); //die;

			//print_r($data); die;
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	

/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {

			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->where('IsDeleted','0');
				$this->db->where('b.CountryID','1');
				$this->db->group_by('b.CountryID');
				$this->db->order_by('b.CountryID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
				$this->db->from('tblpatplantdetail a');
				$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
				$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateID','asc');
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			}

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$login_data = $this->session->userdata('login_data');
			if($login_data['ROLE_ID'] == 9 )
			{
				$this->db->select('c.StateID,c.StateName');
				$this->db->from('mststate c');
				$this->db->where('c.CountryID',$id);
				$this->db->where('c.StateID', 216);
				$this->db->group_by('c.StateID');
				$this->db->order_by('c.StateName','asc');
				
				return $this->db->get()->result();
			}
			else
			{

			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}