<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
	public $loggedIn = 0;
	public $loggedDate = '';
	
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}


	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function count_rows($UserID)
	{
		try {

			$this->db->where('CountryID',$UserID);
			 $this->db->count_all(COUNTRY); //echo $this->db->last_query(); die;
			
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method lists() get product data which are active and delete status is false.
	 * @access	public
	 * @param	$limit hold number of record displayed per page & $start hold starting row number
	 * @return	array
	 */
	public function lists($limit,$start,$keyword=0,$level=0,$Order_by=0)
	{
		try { 
			//print $Order_by ; die;
			$this->db->select('*');
			$this->db->limit($limit,$start);
			return $this->db->get(DISTRICT)->result(); //echo $this->db->last_query();//die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	

/**
	 * Method getPATSofiescore() get detail Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPATSofiescore($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			$this->db->close();
			$this->db->initialize();
			//$this->db->select("round(sum(`ss`)/count(*)) AS 'Social', round(sum(`FS`)/count(*)) AS 'financial',round(sum(`OS`)/count(*)) AS 'operational', round(sum(`IS_t`)/count(*)) AS 'Institutional',round(sum(`ES`)/count(*)) AS 'Environmental'");
			//$this->db->where('CountryID',$strwhr);
			//return($this->db->get(vw_pat_score)->result()[0]); 
			$sql = "SELECT round(sum(`ss`)/count(*)) AS 'Social', round(sum(`FS`)/count(*)) AS 'financial',round(sum(`OS`)/count(*)) AS 'operational', round(sum(`IS_t`)/count(*)) AS 'Institutional',round(sum(`ES`)/count(*)) AS 'Environmental' 
			FROM `vw_pat_score` WHERE 1=1";

			if(!empty($strwhr)){
				$sql .= " AND `CountryID` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `StateID` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `District` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";

		   $data = $this->db->query($sql)->result()[0];
		   return $data;
		   }
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getPerformanceSofieScore() get detail Performance Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPerformanceSofieScore($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
					
			$sql = "SELECT ROUND(AVG(`SoS`)) as 'social', ROUND(AVG(`OpS`)) as 'operational', ROUND(AVG(`FsS`)) AS 'financial', ROUND(AVG(`InS`)) as 'Institutional',ROUND(AVG(`EnS`)) as 'Environmental' 
			FROM `vw_plant_sofiescore` WHERE 1=1";

			if(!empty($strwhr)){
				$sql .= " AND `CountryID` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `StateID` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `District` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";
			//echo $sql; die;
		   $data = $this->db->query($sql)->result()[0];
		   return $data;
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


public function getStateSofieScore($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		
		try{
			
		$sql = "SELECT `ms`.`DistrictName` AS 'DistrictName', `ms`.`DistrictID` AS 'DistrictID', ROUND(`SoS`) as 'social', ROUND(`OpS`) as 'operational', ROUND(`FsS`) AS 'financial', ROUND(`InS`) as 'Institutional', 
		ROUND(`EnS`) as 'Environmental'
		FROM `vw_plant_sofiescore` as `ps` LEFT JOIN `mstdistrict` as`ms` ON `ms`.DistrictID = `ps`.`District` WHERE 1=1 ";

			if(!empty($strwhr)){
				$sql .= " AND `ps`.`countryid` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `ps`.`stateid` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `ps`.`district` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `ps`.`PlantGUID` = '".$plantwhr."'";
			}
			
			$sql .= " GROUP BY ms.`DistrictName`,ms.`DistrictID` ORDER BY `DistrictName` ASC";

			//echo $sql; //die;

		   $data = $this->db->query($sql)->result();
		   return $data;
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}




/**
	 * Method getPerformanceLimit() get detail Performance Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPerformanceLimit($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			$sql = "SELECT (SELECT COUNT(*) FROM `vw_plantsofiescore` WHERE `TS` < 60)AS 'lessthan60', (SELECT COUNT(*) FROM `vw_plantsofiescore` WHERE `TS` > 60)AS 'morethan60',
(SELECT COUNT(*) FROM `vw_plantsofiescore` WHERE `TS`=60)AS 'eql60'
FROM `vw_plant_sofiescore` WHERE 1=1 ";

			if(!empty($strwhr)){
				$sql .= " AND `countryid` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `stateid` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `district` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";

			$data = $this->db->query($sql)->result()[0];
		return $data;
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	/**
	 * Method getPerformance() get detail Performance Sofie Score Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getPerformance($strwhr,$strstatewhr,$districtwhr,$plantwhr){
		try{
			//$this->db->select('round(((AVG(`SoS`)+AVG(`OpS`)+AVG(`FsS`)+AVG(`InS`)+AVG(`EnS`))/5)) as PERFORMANCE');
			//$this->db->where('countryid',$strwhr);
		//	$this->db->get(vw_plantsofiescore)->result()[0]; echo $this->db->last_query(); die; 

		$sql = " SELECT round(((AVG(`SoS`)+AVG(`OpS`)+AVG(`FsS`)+AVG(`InS`)+AVG(`EnS`))/5)) as 'PERFORMANCE' 
			FROM `vw_plant_sofiescore` WHERE 1=1";

			if(!empty($strwhr)){
				$sql .= " AND `CountryID` = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND `StateID` = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND `District` = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND `PlantGUID` = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantGUID` DESC";
			//echo $sql;
		   $data = $this->db->query($sql)->result()[0];

		   return $data;
			//return($this->db->get(vw_plant_sofiescore)->result()[0]); 
		}
		catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getDashboardfinancial() get detail Financial Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardfinancial($strwhr){
	
		try{
			$this->db->close();
			$this->db->initialize();
			$this->db->select('CapitalSource,OpExandRevenue');
			$this->db->where('CountryID',$strwhr);
			//$this->db->get(vw_patfinancial)->row(); echo $this->db->last_query(); die;
			return($this->db->get(vw_patfinancial)->result()[0]); 
		}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getDashboardPlantAndAge() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardPlantAndAge($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
				$sql = "SELECT SUM(NoOfHousehold) AS Household,
								SUM(NoOfhhregistered) AS registered,
								(SUM(NoOfhhregistered) + SUM(NoOfhhregistered)) as total,
								ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
								ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
							FROM
								tblpatplantdetail
							WHERE
								AgeOfPlant < 1 
							union
							SELECT
									SUM(NoOfHousehold) AS Household,
									SUM(NoOfhhregistered) AS registered,
									(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
									ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
									ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
								FROM
									tblpatplantdetail
								WHERE
									AgeOfPlant between 1 and 3 
			
									UNION
									SELECT
									SUM(NoOfHousehold) AS Household,
									SUM(NoOfhhregistered) AS registered,
									(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
									ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
									ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
								FROM
									tblpatplantdetail
								WHERE
									AgeOfPlant between 3 and 5 
									UNION
										SELECT
										SUM(NoOfHousehold) AS Household,
										SUM(NoOfhhregistered) AS registered,
										(SUM(NoOfHousehold) + SUM(NoOfhhregistered)) as total,
										ROUND((SUM(NoOfhhregistered)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as RegisteredHH,
										ROUND((SUM(NoOfHousehold)/(SUM(NoOfHousehold) + SUM(NoOfhhregistered)))*100) as UncoveredHH
									FROM
										tblpatplantdetail
									WHERE
										AgeOfPlant > 5 ";
				
			//echo $sql;
			
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}



/**
	 * Method getDashboardRuralAffordability() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardRuralAffordability($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
				$sql = " select '<=0.1' as label, count(*) as VALUE from tblpatplantproddetail as pppd
						INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=0 AND pppd.`Price` <= 0.1
						union
						select '>0.1 and <=0.25' as label, count(*) as VALUE from tblpatplantproddetail as pppd
						INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=0 AND  pppd.`Price` BETWEEN 0.1 AND 0.25
						union
						select '> 0.25' as label, count(*) as VALUE from tblpatplantproddetail AS pppd
						INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=0 AND  pppd.`Price` > 0.25 ";
			//echo $sql;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}




	
/**
	 * Method getDashboardUrbanAffordability() get detail Age of Plant Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardUrbanAffordability($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
				$sql = " select '<=0.1' as label, count(*) as VALUE from tblpatplantproddetail as pppd
						INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=1 AND pppd.`Price` <= 0.1
						union
						select '>0.1 and <=0.25' as label, count(*) as VALUE from tblpatplantproddetail as pppd
						INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=1 AND  pppd.`Price` BETWEEN 0.1 AND 0.25
						union
						select '> 0.25' as label, count(*) as VALUE from tblpatplantproddetail AS pppd
						INNER JOIN tblpatplantdetail AS ppd ON ppd.PlantGUID=pppd.PlantGUID  where ppd.LocationType=1 AND  pppd.`Price` > 0.25 ";
			//echo $sql;
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


	/**
	 * Method getDashboardUserActiveAndInactive() get detail of Active user and In Active user Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardUserActiveAndInactive($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
				if(!empty($strwhr)){
				$sql .= " AND ppd.`CountryID` = '".$strwhr."'";
				}
				if(!empty($strstatewhr)){
					$sql .= " AND ppd.`StateID` = '".$strstatewhr."'";
				}
				if(!empty($districtwhr)){
					$sql .= " AND ppd.`District` = '".$districtwhr."'";
				}
				if(!empty($plantwhr)){
					$sql .= " AND ppd.`PlantGUID` = '".$plantwhr."'";
				}
			
				$sql = "SELECT Count(*) As Total,(select count(*) AS VALUE from `mstuser` as u 
				LEFT JOIN `tblpatplantdetail` as ppd on ppd.UserID = u.UserID
				WHERE u.`IsActive`='1' AND 
				ppd.`UpdatedOn` BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW() ".$sql."
				) as Active, 
				(select count(*) AS VALUE from `mstuser` as u 
				LEFT JOIN `tblpatplantdetail` as ppd on ppd.UserID = u.UserID
				WHERE u.`IsActive`='1' AND ppd.`UpdatedOn` BETWEEN DATE_SUB(NOW(), INTERVAL 20 DAY) AND NOW()
				".$sql.") as Partially,
				(select count(*) AS VALUE from `mstuser` as u 
				LEFT JOIN `tblpatplantdetail` as ppd on ppd.UserID = u.UserID
				WHERE u.`IsActive`='0' ".$sql.") as InActive
				FROM `mstuser`";
			    //echo $sql; die;


			$data = $this->db->query($sql)->result()[0];
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getDashboardDistributionPoint() get Count of Distribution Point.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardDistributionPoint($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
		
		$sql = "SELECT COUNT(ppd.DPName) as DistributionPoint 
		FROM `tblpatplantdetail` as ppdt
		INNER JOIN `tblpatplantdistribution` as ppd ON ppd.PlantGUID = ppdt.PlantGUID
		WHERE 1=1 ";

		if(!empty($strwhr)){
			 $sql .= " AND ppdt.CountryID = '".$strwhr."'";
		}
		if(!empty($strstatewhr)){
			 $sql .= " AND ppdt.StateID = '".$strstatewhr."'";
		}
		if(!empty($districtwhr)){
			 $sql .= " AND ppdt.District = '".$districtwhr."'";
		}
		if(!empty($plantwhr)){
			$sql .= " AND ppdt.PlantGUID = '".$plantwhr."'";
		}
		$sql .= " ORDER BY ppdt.`PlantUID` ASC";
		$data = $this->db->query($sql)->result();
  		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}


/**
	 * Method getCountPlants() get Count of Plants.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getCountPlants($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			$sql = "SELECT COUNT(`PlantGUID`) as count FROM `tblpatplantdetail` WHERE 1=1";
			if(!empty($strwhr)){
				$sql .= " AND CountryID = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND StateID = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND District = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND PlantGUID = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantUID` ASC";
			//echo $sql;
			$data = $this->db->query($sql)->result()[0];
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

	/**
	 * Method getDashboardPopulation() get Count of Population .
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardPopulation($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
			
		 if(!empty($strstatewhr) && empty($districtwhr) && empty($plantwhr)){
				$sql = "SELECT SUM(Population) as 'Population'  FROM `tblpatplantdetail` WHERE CountryID='".$strwhr."' AND StateID ='".$strstatewhr."' ";
			}elseif(!empty($districtwhr) && !empty($strstatewhr) && empty($plantwhr)){
				$sql = "SELECT SUM(Population) as 'Population'  FROM `tblpatplantdetail` WHERE CountryID='".$strwhr."' AND StateID ='".$strstatewhr."' AND District ='".$districtwhr."'";
			}elseif(!empty($plantwhr) && !empty($districtwhr) && !empty($strstatewhr)){
				$sql = "SELECT SUM(Population) as 'Population'  FROM `tblpatplantdetail` WHERE CountryID='".$strwhr."' AND StateID ='".$strstatewhr."'  AND District ='".$districtwhr."' AND PlantGUID ='".$plantwhr."'";
			}else{
				$sql = "SELECT SUM(Population) as 'Population'  FROM `tblpatplantdetail` WHERE CountryID='".$strwhr."'";
			}
			$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}
	
	/**
	 * Method getDashboardPopulation() get Count of Population .
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardPlantEvl($strwhr,$strstatewhr,$districtwhr,$plantwhr){
	
		try{
		$sql = "SELECT COUNT(SWNID) as 'Plant' FROM tblpatplantdetail where UpdatedOn >= now() AND CountryID='".$strwhr."'";
		
		if(!empty($strstatewhr)){
			 $sql .= " AND StateID = '".$strstatewhr."'";
		}
		if(!empty($districtwhr)){
			 $sql .= " AND District = '".$districtwhr."'";
		}
		if(!empty($plantwhr)){
			$sql .= " AND PlantGUID = '".$plantwhr."'";
		}
		$sql .= " ORDER BY `PlantUID` DESC";
		//echo $sql; die;
		$data = $this->db->query($sql)->result();
       	return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method getDashboardSofePerformance() get detail Sofe score in persantage and performance Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function getDashboardSofePerformance($strwhr){
	
		try{
		$sql = "SELECT 'Total' as label, Count(*)/20 AS VALUE FROM `mstuser` WHERE IsActive='1' 
				union
				select 'Active' as label, count(*)/20 AS VALUE from `mstuser` WHERE IsActive='1' AND `ModifiedOn` BETWEEN NOW() AND DATE_SUB(NOW(), INTERVAL 7 DAY)
				union
				select 'Partially Active' as label, count(*)/20 AS VALUE from `mstuser` WHERE IsActive='1' AND `ModifiedOn` BETWEEN NOW() AND DATE_SUB(NOW(), INTERVAL 20 DAY)
				union
				select 'InActive' as label, count(*)/20 AS VALUE from `mstuser` where IsActive='0'";
				$data = $this->db->query($sql)->result();
       		return $data;
	}
	catch(Exception $e){
			print_r($e->getMessage()); die;

		}

	}

/**
	 * Method DatafromOnWebService_Map() get Location Details.
	 * @access	public
	 * @param	$token
	 * @return	string.
	 */

	public function DatafromOnWebService_Map($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		// $sql = "SELECT DISTINCT 
        // 		tbltstlocation.VillageName AS description,
		// 		tbltstlocation.XValue AS Latitude,
		// 		tbltstlocation.YValue AS Longitude
		// 		FROM tbltstlocation WHERE 1=1";

		$sql = "SELECT DISTINCT tbt.VillageName AS description, tbt.XValue AS Latitude, 
		tbt.YValue AS Longitude FROM tbltstlocation AS tbt 
		LEFT JOIN tblpatpodetail AS tppd ON tppd.PlantGUID = tbt.GUID 
		WHERE 1=1";

		// $sql = "SELECT DISTINCT tppd.VillageName AS description, tbt.Latitude AS Latitude, 
		// 		tbt.Longitude AS Longitude FROM tblpatpodetail AS tbt 
		// 		LEFT JOIN tbltstlocation AS tppd ON tppd.GUID = tbt.PlantGUID 
		// 		WHERE 1=1 ";

		if(!empty($strwhr)){
			 $sql .= " AND tbt.`CountryID` = '".$strwhr."'";
		}
		if(!empty($strstatewhr)){
			 $sql .= " AND tbt.`StateID` = '".$strstatewhr."'";
		}
		if(!empty($districtwhr)){
			 $sql .= " AND tbt.`DistrictID` = '".$districtwhr."'";
		}
		if(!empty($plantwhr)){
			$sql .= " AND tppd.PlantGUID = '".$plantwhr."'";
		}
		$sql .= " ORDER BY tbt.`LocationUID` DESC";
		//echo $sql; //die;
		return($data = $this->db->query($sql)->result());
	}


/***
List HorizontalBar Graph

*/
	public function CreateHorizontalBarPAT($strwhr,$roleid,$userid)
	{

		try { 
			$procedure = 'CALL SP_GraphdashboardUserdetailForPAT('.$strwhr.','.$userid.','.$roleid.')';
			$data = $this->db->query($procedure)->result();
			//print_r($data); die;
			 return $data;
			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}

//////////////////////// Get Guage Graph data ///////////////////////
	public function getguage($strwhr,$strstatewhr,$districtwhr,$plantwhr)
	{
		try{
			$this->db->close();
			$this->db->initialize();
		    $sql  = 'SELECT AVG((NoOfhhregistered)*100/NoOfHousehold)  AS adpotion 
			FROM tblpatplantdetail WHERE 1=1'; 
			if(!empty($strwhr)){
			 $sql .= " AND CountryID = '".$strwhr."'";
			}
			if(!empty($strstatewhr)){
				$sql .= " AND StateID = '".$strstatewhr."'";
			}
			if(!empty($districtwhr)){
				$sql .= " AND District = '".$districtwhr."'";
			}
			if(!empty($plantwhr)){
				$sql .= " AND PlantGUID = '".$plantwhr."'";
			}
			$sql .= " ORDER BY `PlantUID` DESC";
			//echo $sql;
			$data = $this->db->query($sql)->result()[0];
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	

	//////////////////////// Get Pie Distributionplantid Graph data ///////////////////////
	public function CreatePieChart()
	{
		try{
				$sql  = "SELECT
				SUM(NoOfHousehold) AS TotalHouseholds,
				SUM(NoOfhhregistered) AS reg,
				(
					SUM(NoOfHousehold) + SUM(NoOfhhregistered)
				) AS total,
				(
					SUM(NoOfHousehold) /(
						SUM(NoOfHousehold) + SUM(NoOfhhregistered)
					)
				) * 100 AS perc,
				
				(
					SUM(NoOfhhregistered) /(
						SUM(NoOfHousehold) + SUM(NoOfhhregistered)
					)
				) * 100 AS perc2
				
			FROM
				`tblpatplantdetail` WHERE 1=1  "; //die; 
				if(!empty($strwhr)){
				 $sql .= " AND CountryID = '".$strwhr."'";
				}
				if(!empty($strstatewhr)){
					$sql .= " AND StateID = '".$strstatewhr."'";
				}
				if(!empty($districtwhr)){
					$sql .= " AND District = '".$districtwhr."'";
				}
				if(!empty($plantwhr)){
					$sql .= " AND PlantGUID = '".$plantwhr."'";
				}
				$sql .= " ORDER BY `PlantUID` ASC";


			$data = $this->db->query($sql)->result()[0]; //echo $this->db->last_query(); die;

			//print_r($data); die;
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	//////////////////////// Get Bar Graph User data Bar2dUserBarChart ///////////////////////

	public function CreateBar2dUserBarChart()
	{
		try{
			$this->db->close();
			$this->db->initialize();
			$procedure  = 'SELECT COUNT(UserID) AS Total FROM userTemp UNION ALL SELECT COUNT(UserID) AS Active FROM userTemp WHERE VisitDate>SUBDATE(NOW(), 20)
							UNION ALL SELECT COUNT(UserID) AS PartiallyActive FROM userTemp WHERE VisitDate<SUBDATE(NOW(), 7) AND VisitDate>SUBDATE(NOW(), 20)
							UNION ALL SELECT COUNT(UserID) AS Inactive FROM userTemp WHERE VisitDate<SUBDATE(NOW(), 7); ';
			$data = $this->db->query($procedure)->result();
			return $data; 
            
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	
	
    /**
	 * Method DashBoard_Report() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoard_Report()
	{

		try { 

			$this->db->close();
			$this->db->initialize();
				
			$procedure = 'CALL DashBoard_Report()';
			$data = $this->db->query($procedure)->result();
			return $data; /////print_r($data); die;
			}
			catch (Exception $e) {
				print_r($e->getMessage());die;
			}

	}


 /**
	 * Method DashBoard_Report() Get detail.
	 * @access	public
	 * @param	$data , hold all bin data
	 * @return	string.
	 */

	public function DashBoardGraphs_BarChart_Data()
	{

		try { 
				
			$procedure = 'CALL Get_DashBoardGraphs_BarChart_Data()';
			$data = $this->db->query($procedure)->result();
			return $data; /////print_r($data); die;
			}
			catch (Exception $e) {
				print_r($e->getMessage());die;
			}

	}


/**
	 * Method Count_State_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_State_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT StateID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->num_rows();
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method Count_District_All() get Count State .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function Count_District_All($UserID)	
	{
		try {
			$query = $this->db->query("SELECT count(DISTINCT DistrictID) FROM `tbltstlocation` WHERE UserID = '".$UserID."'");
			$row = $query->row(); 
			return $row;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDashboard($UserID)	
	{
		try {

			$this->db->select('*');
			//$this->db->where('mstuser.UserID', $userid);
			$this->db->where('IsDeleted', '0');
			return $this->db->get('mstmenu')->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserCountry($UserID)	
	{
		try {

			$this->db->close();
			$this->db->initialize();

			$query = $this->db->query("SELECT STL.CountryID,STL.StateID,STL.DistrictID FROM `mstuser`  AS MSTU 
INNER JOIN `tbltstlocation` AS STL ON STL.UserID = MSTU.UserID WHERE MSTU.UserID = '".$UserID."'");
			$row = $query->row();
		    //echo $this->db->last_query(); die;
			return $row;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

/**
	 * Method getUserState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getUserState($SelectedCountryID, $UserID)	
	{
		try {
           $query = $this->db->query("SELECT DISTINCT(StateID) FROM `tbltstlocation` 
										WHERE tbltstlocation.CountryID='".$SelectedCountryID."' 
										AND  tbltstlocation.UserID = '".$UserID."'");
			  return $row = $query->result();
				//print_r($row);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCountry() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountry()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->where('IsDeleted','0');
			$this->db->group_by('b.CountryID');
			$this->db->order_by('b.CountryID','asc');
			return $this->db->get()->result();
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	/**
	 * Method getState() get all types.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStates()
	{
		try {
			$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName');
			$this->db->from('tblpatplantdetail a');
			$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
			$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateID','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			//$this->db->
			return $this->db->get()->result();
			

		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	

	/**
	 * Method getCountryStates() get State According to Country.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountryStates($id)
	{
		try {
			$this->db->select('c.StateID,c.StateName');
			$this->db->from('mststate c');
			$this->db->where('c.CountryID',$id);
			$this->db->group_by('c.StateID');
			$this->db->order_by('c.StateName','asc');
			//$this->db->get()->result(); //echo $this->db->last_query(); die;
			return $this->db->get()->result();
		}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
	}
	
	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getDistricts()
	// {
	// 	try {
	// 	$this->db->select('b.CountryID, b.CountryName,c.StateID,c.StateName,m.DistrictID,m.DistrictName');
	// 		$this->db->from('tblpatplantdetail a');
	// 		$this->db->join('mstcountry b','b.CountryID = a.CountryID','INNER');
	// 		$this->db->join('mststate c', 'a.StateID = c.StateID','INNER'); 
	// 		$this->db->join('mstdistrict m', 'a.StateID = m.StateID','INNER'); 
	// 		$this->db->group_by('m.DistrictID');
	// 		$this->db->order_by('m.DistrictID','asc');
	// 	return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
	// }

	/**
	 * Method getDistrict() get all District.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDistricts($countryid, $stateid)
	{
		try {
			$this->db->select('m.DistrictID,m.DistrictName');
			$this->db->from('mstdistrict m');
			$this->db->where('m.CountryID', $countryid);
			$this->db->where('m.StateID', $stateid);
			$this->db->group_by('m.DistrictID');
			$this->db->order_by('m.DistrictName','asc');
		return $this->db->get()->result(); //echo $this->db->last_query(); die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getPlants() get all Plant Name.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	
	public function getPlants($countryid, $stateid,$district)
	{
		
		try {
		    $this->db->select('a.SWNID,a.PlantGUID');
			$this->db->from('tblpatplantdetail a');
			$this->db->where('a.CountryID', $countryid);
			$this->db->where('a.StateID', $stateid);
			$this->db->where('a.District', $district);
			$this->db->group_by('a.PlantGUID');
			$this->db->order_by('a.SWNID','asc');
		    return $this->db->get()->result();   //echo $this->db->last_query();// die;
								
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	} 
	
}