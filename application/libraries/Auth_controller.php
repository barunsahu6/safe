<?php 

/**
 * Auto_controller class
 * to extend all the controllers
 */

/**
* Auth controller
*/
class Auth_controller extends Ci_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$check_result = $this->check_is_authorized();
		if ($check_result == FALSE) {
			redirect('login');
		}
	}

	private function check_is_authorized()
	{
		$controller = strtolower($this->uri->segment(1));
		$method = strtolower($this->uri->segment(2));

		if ($method == NULL) {
			$method = 'index';
		}

		$this->db->where('controller', $controller);
		$this->db->where('method', $method);
		$this->db->where('role_id', $this->login_data['ROLE_ID']);
		//echo $this->db->get('user_role_mapping'); echo $this->db->last_query();
		 $result = $this->db->get('user_role_mapping')->result(); 
		
		//echo count($result);
		//die;

		if (count($result) < 1) {
			return false;
		}

		return true;
	}


/**
 * List All Manu at Database
*/
public function list_menu(){

	$this->db->where('Menu', 'Dashboard');
	$data = $this->db->get('mstmenu')->row; 
	print_r($data); die;
	if ($data >0) {
		return $data;
	}else{

		return 1;
	}

}




}