<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;	
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		//SET VALIDATION RULES
		
		$this->valid = array(
				   array(
						 'field'   => 'form[username]', 
						 'label'   => ' username', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[password]', 
						 'label'   => ' password', 
						 'rules'   => 'trim|required'
				   )
				 );
        
		$this->form_validation->set_rules($this->valid);
		 
		if($this->input->post('sigin'))
		{

			if($this->form_validation->run())
			{
						
				if($this->model->authenticate() == '1'){
					redirect('Dashboard');
				}
				else
				{
					$this->session->set_flashdata('message', 'Username and password did not match!!');
					redirect($this->router->class);
				}
			}
		}
		$this->load->view($this->router->class.'/'.$this->router->method);
	}
	
	

 public function logout()
   {
	   
    //$data = $this->session->set_userdata();
   //  foreach ($data as $key => $value) {
     // if ($key != 'session_id' && $key != 'ip_address') {
      // $this->session->unset_data($key);
      //}
    // }
    $this->session->sess_destroy();
    redirect(base_url());
   }
}