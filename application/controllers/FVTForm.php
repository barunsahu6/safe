<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FVTForm extends Ci_controller 
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function FVTForm()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
	    $this->model = $this->$mod;
		$this->view['title']  = ucfirst($this->router->method).' '. ucfirst(str_replace('_',' ',$this->router->class));
		$content['status'] = array(1=>'Available',2=>'not Available');
		//SET VALIDATION RULES
		$this->valid = array(
				   
				   array(
						 'field'   => 'form[OldPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' OldPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				  array(
						 'field'   => 'form[NewPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' NewPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				  array(
						 'field'   => 'form[ConfrimPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' ConfrimPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				 );
		 $this->form_validation->set_rules($this->valid);
		//$this->form_validation->set_rules('form[email]', ucfirst(str_replace('_',' ',$this->router->class)).' email', 'callback_name_validation');
		$this->form_validation->set_rules('form[UserName]', ' username', 'callback_username_validation');
		
		
		$this->load->view(FTOP, $this->view);
		$this->load->view(NAVTOP, $this->view);		
	}
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$content['details']         = $this->model->getDetail(1);  

		$loginData = $this->session->userdata('USERID');
		//echo "<pre>"; print_r($loginData);
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		 $sql1 = "SELECT max(UID) as uidval FROM `inputrecords`";
		$inuidval = $this->db->query($sql1)->result();
		 $rec_uid = $inuidval[0]->uidval+1;
		//print_r($inputdata);
		$sqlinput = "SELECT *  FROM `masterinputrecords`";
		$content['details'] = $this->db->query($sqlinput)->result()[0];
		//echo "<pre>";print_r($inputval['details']);
		if($RequestMethod == 'POST')
		{
		$insertarray = array("UID" =>$rec_uid,
							"GUID" =>$A,
							"DateOfEstablishment" =>$A,
							"Distribution_DateOfEstablishment" =>$A,
							"Population" =>$A,
							"PopulationGrothRate" =>$A,
							"Household_Size" =>$A,
							"RegHousehold" =>$A,
							"Ownership" =>$A,
							"ConsumptionPerHouseHoldPerDay" =>$A,
							"CanSize" =>$A,
							"Growthrate_volume" =>$A,
							"Auto" =>$A,
							"TechnologyType" =>$A,
							"Borewell" =>$A,
							"Land" =>$A,
							"Building" =>$A,
							"ConstructionCost" =>$A,
							"RMS" =>$A,
							"AVR" =>$A,
							"ElectricityConn" =>$A,
							"ROPlant" =>$A,
							"Other_Accessories" =>$A,
							"FundedbyEnterpreCommunity" =>$A,
							"FundedbyDonor" =>$A,
							"RepaymentToDonor" =>$A,
							"Moratorium" =>$A,
							"FundedbyBank" =>$A,
							"Loan" =>$A,
							"LoanTenor" =>$A,
							"InterestRate" =>$A,
							"CapacityOfPlant" =>$A,
							"OperatingHours" =>$A,
							"Backwash" =>$A,
							"Seasonality" =>$A,
							"DistributionHamlets" =>$A,
							"OperatorSalary" =>$A,
							"DriverSalary" =>$A,
							"AutoFuel" =>$A,
							"Helpersalary" =>$A,
							"VehicleMaintenance" =>$A,
							"Rental" =>$A,
							"LandRent" =>$A,
							"RawWaterrSourceRent" =>$A,
							"ServiceFeePaid" =>$A,
							"Spares" =>$A,
							"ElectricityBill" =>$A,
							"GeneratorFule" =>$A,
							"CostOfDelivery" =>$A,
							"ChemicalsAndConsumables" =>$A,
							"OtherOperatorExp" =>$A,
							"EnterpreneurROI" =>$A,
							"OperatorIncentive" =>$A,
							"MembraneReplacement" =>$A,
							"MembraneReplacement_after_years" =>$A,
							"MembraneReplacementFundedBy" =>$A,
							"MotorReplacement" =>$A,
							"MotorReplacement_after_years" =>$A,
							"MotorReplacementFundedBy" =>$A,
							"TreatedWater" =>$A,
							"ChildWater" =>$A,
							"Capitalvalueto_bereplaced" =>$A,
							"Assetlifeyears" =>$A,
							"Field_Supervision_month" =>$A,
							"LocalNGO_month" =>$A,
							"MoratoriumforFieldsupervision_Years" =>$A,
							"Moratoriumfor_LocalNGO_Years" =>$A,
							"CreatedOn" =>$A,
							"UpdatedOn" =>$A,
							"DonorMoratorium" =>$A,
							"CommunityFlag" =>$A,
							"CapexFlag" =>$A,
							"FundingFlag" =>$A,
							"OperationalFlag" =>$A,
							"OperationalExpensesFlag" =>$A,
							"NonRecurringFlag" =>$A,
							"PricingFlag" =>$A,
							"TampletID" =>$A,
							"CopyTemplet" =>$A,
							"CapitalRepayment" =>$A,
							"LNGOCost" =>$A,
							"FieldSupport" =>$A,
							"SC_VolumeCansSoldPerDay" =>$A,
							"SC_BaselinePenetration" =>$A,
							"SC_IncrInPenetrationATMYear1" =>$A,
							"SC_IncrInPenetrationChillerYear1" =>$A,
							"SC_IncrInPenetrationATMChillerYear1" =>$A,
							"SC_IncrInPenetrationBeyondYear1" =>$A,
							"SC_SolarPanel" =>$A,
							"SC_ATM" =>$A,
							"SC_ChillerMachine" =>$A,
							"SC_CansForChillerPlant" =>$A,
							"SC_ATMCards" =>$A,
							"SC_ATMmaintenance" =>$A,
							"SC_SolarPanelMaintenance" =>$A,
							"SC_ChillerMachineMaintenance" =>$A,
							"SC_SolarBatteryReplacementInYears" =>$A,
							"SC_SolarBatteryReplacementCost" =>$A,
							"Distribution_Population" =>$A,
							"Distribution_Expected_Population_Growth_Rate" =>$A,
							"Distribution_Household_Size" =>$A,
							"Distribution_Registered_Households" =>$A,
							"Distribution_Consumption_household_Perday" =>$A,
							"Distribution_Cansize" =>$A,
							"Distribution_Growthrate_Volume" =>$A,
							"Distribution_Auto" =>$A,
							"Distribution_Loan" =>$A,
							"Distribution_LoanTenor" =>$A,
							"Distribution_Interest_Rate" =>$A,
							"Distribution_BankLoanEMI_Monthly" =>$A,
							"Distribution_Driver_Salary" =>$A,
							"Distribution_Auto_Fuel" =>$A,
							"Distribution_Helper_Salary" =>$A,
							"Distribution_Vehicle_Maintenance" =>$A,
							"Distribution_Rental" =>$A,
							"Distribution_TreatedWater" =>$A,
							"SC_RegisteredHousehold" =>$A,
							"SC_CanSize" =>$A,
							"SC_FundedbyDonor" =>$A,
							"SC_RepaymentToDonor" =>$A,
							"SC_MoratoriumDonor" =>$A,
							"SC_FundedByBank" =>$A,
							"SC_LoanTenor" =>$A,
							"SC_BankLoanEMI" =>$A,
							"SC_ChilledWater" =>$A,
							"SC_UnchilledWater" =>$A,
							"SC_EntreprenuerROI" =>$A,
							"SC_ChillerElectricityCostperCan" =>$A,
							"SC_Cost_NormalWater" =>$A,
							"SC_Cost_ChilledWater" =>$A,
							"SC_FundedBy_Entreprenure_Community" =>$A,
							"SC_Moratorium_Bank" =>$A,
							"SC_InterestRate" =>$A,
							"BreakEven_CostPerLitre" =>$A,
							"BreakEven_FieldSupport" =>$A,
							"SC_SolarEvaluation" =>$A,
							"SC_ChillerEvaluation" =>$A,
							"SC_ATMEvaluation" =>$A,
							"Distribution_AutoLife" =>$A,
							"NofStationsInstalled" =>$A,
							"FOOCanSubsidy" =>$A,
							"OperatorSalaryOverAboveEntrepreneurModel" =>$A,
							"LNGOExpenses" =>$A,
							"FieldSupportExpenses" =>$A,
							"DGCAExpenses" =>$A,
							"AdminExpensesPerMonthPerStation" =>$A,
							"LabTestsPerQuarterPerStation" =>$A,
							"SimChargesPerMonthPerStation" =>$A,
							"Sectionflag" =>$A,
							"COO" =>$A,
							"FOO" =>$A,
							"Cansubsidy" =>$A,
							"Operator_ovrsalary" =>$A,
							"Aggregator_Entreprenure" =>$A);

					}


		$this->load->view('FVTForm/index',$content);
		$this->load->view(FBOTTOM, $this->view);
	}


	public function break_event_output_chart(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$sqlinput = "SELECT *  FROM `masterinputrecords`";
		$content['details'] = $this->db->query($sqlinput)->result()[0];
		$this->load->view('FVTForm/break_event_output_chart',$content);
		$this->load->view(FBOTTOM, $this->view);

	}

	public function break_event_graph(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$sqlinput = "SELECT *  FROM `masterinputrecords`";
		$content['details'] = $this->db->query($sqlinput)->result()[0];
		$this->load->view('FVTForm/break_event_graph',$content);
		$this->load->view(FBOTTOM, $this->view);	

	}

	public function per_can_analysis(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$this->load->view('FVTForm/pre_can_analysis',$content);
		$this->load->view(FBOTTOM, $this->view);

	}

	public function cash_flow_master_input(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$sqlinput = "SELECT *  FROM `masterinputrecords`";
		$content['details'] = $this->db->query($sqlinput)->result()[0];
		$this->load->view('FVTForm/cash_flow_master_input',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function cash_flow_output(){
		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$this->load->view('FVTForm/cash_flow_output',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function volume_analysis_graph(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$this->load->view('FVTForm/volume_analysis_graph',$content);
		$this->load->view(FBOTTOM, $this->view);

	}
	public function operating_profitcan(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$this->load->view('FVTForm/operating_profitcan',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function sensitivity_graph(){

		$this->load->helper(array('form','url'));
		$content['getdata'] = $this->model->viewDetail();
		$this->load->view('FVTForm/sensitivity_graph',$content);
		$this->load->view(FBOTTOM, $this->view);
		
	}
	
	/**
	 * Method add() add new accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function add()
	{
		//SET VALIDATION RULES
		$this->valid[] = array(
						 'field'   => 'form[FirstName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' FirstName', 
						 'rules'   => 'trim|required'
				   		);

		$this->valid[] = array(
				    	 'field'   => 'form[MiddleName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' MiddleName', 
						 'rules'   => 'trim|required|min_length[8]|max_length[20]'
				 		);
				 
		if($this->input->post($this->router->method))
		{
			if($this->form_validation->run())
			{
				if($this->model->add() == '1'){
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' added successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not added!!');
					redirect($this->router->class.'/'.$this->router->method);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method edit() update accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function edit()
	{
		
		$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$form = $this->input->post();

		$content = array();
		$content['details']         = $this->model->getDetail($token);  //// Get All detail GUID info
		$content['ContinerType'] 	= $this->model->getContainertype(); //// Get All  ContinerType info
		$content['country'] 		= $this->model->getCountry();		//// Get All Country 
		$content['state'] 			= $this->model->getState();			//// Get All State
		$content['district'] 		= $this->model->getDistrict();		//// Get All District
		$content['source'] 			= $this->model->getSourceType();	//// Get All Source Type
		$content['weather'] 		= $this->model->getWeather();       //// Get All Weather
		$content['season'] 			= $this->model->getSeason();		//// Get All Season
		$content['status'] 		    = $this->model->getArray();			//// Get All Status
		$content['getdata'] 		= $this->model->viewDetail();
		

		if(!empty($form))
		{
			if($this->model->edit($token)==1){
					$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)). ' updated successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('err_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not updated!!');
					redirect($this->router->class.'/'.$this->router->method.'/'.$this->view['token']);
				}
		}
			
		
		$this->load->view('FVTForm/edit', $content);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->getDetail($this->view['token']);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		if($this->model->delete($this->view['token']) == '1'){
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}
	 }
	 
	 /**
	 * Method view() view accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function view()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->viewDetail($this->view['token']);//print_r($this->view['detail']);die;
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method name_validation() check unique email.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function name_validation()
	{
		return $this->model->name_validation();
	}
	
	
	/**
	 * Method username_validation() check unique username.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function username_validation()
	{
		return $this->model->username_validation();
	}
}