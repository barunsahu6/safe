<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
	}

	public function getStates($country_id)
	{
	  $login_data = $this->session->userdata('login_data');

		if($login_data['ROLE_ID'] == 9 )
		{
			$sql = "select * from mststate where StateID = 216";
		}
		else
		{
		  	$sql = "select * from mststate where 1=1";
			if(isset($country_id)&& !empty($country_id)){
			$sql .= " AND CountryID ='".$country_id."'";
			}else{
				$sql .= " ";
			}   
		  	$sql .= "ORDER BY StateName ASC";
		}

	 //  	$sql = "select * from mststate where 1=1";
		// if(isset($country_id)&& !empty($country_id)){
		// $sql .= " AND CountryID ='".$country_id."'";
		// }else{
		// 	$sql .= " ";
		// }   
	 //  	$sql .= "ORDER BY StateName ASC";
		
		$res = $this->Common_model->query_data($sql);
		$options_state = '<option value="">All</option>';
		foreach($res as $state)
		{
			$options_state .= '<option value="'.$state->StateID.'">'.$state->StateName.'</option>';
		}

		echo $options_state;
	}

	public function getAllDistrict($country_id, $state_id)
	{
		
		$sql = "select StateID,DistrictID from  mstdistrict where StateID = ".$state_id;
		$res = $this->Common_model->query_data($sql);
		print_r($res);
	}



	public function getDistricts($country_id, $state_id)
	{
		$sql = "select * from mstdistrict where 1=1";
		if(isset($country_id) && !empty($country_id)){
			$sql .= " AND CountryID = '".$country_id."'";
		}
		if(isset($state_id) && !empty($state_id)){
			$sql .= "AND StateID = '".$state_id."'";
		}
		$sql .= "ORDER BY StateID ASC ";
		
		$res = $this->Common_model->query_data($sql);

		$options_district = '<option value="">All</option>';
		foreach($res as $district)
		{
			$options_district .= '<option value="'.$district->DistrictID.'">'.$district->DistrictName.'</option>';
		}

		echo $options_district;
	}

	public function getPlants($country_id, $state_id, $district_id)
	{
		$sql = "select PlantGUID, SWNID from tblpatplantdetail where 1=1";

		if(isset($country_id) && !empty($country_id)){
			$sql .= " AND CountryID = '".$country_id."'";
		}
		if(isset($state_id) && !empty($state_id)){
			$sql .= "AND StateID = '".$state_id."'";
		}
		if(isset($district_id) && !empty($district_id)){
			$sql .= "AND District = '".$district_id."'";
		}
		$sql .= "ORDER BY District ASC ";
		
		
		$res = $this->Common_model->query_data($sql);
		print_r($res);

		$options_plants = '<option value="">All</option>';
		foreach($res as $plant)
		{
			$options_plants .= '<option value="'.$plant->PlantGUID.'">'.$plant->SWNID.'</option>';
		}

		echo $options_plants;
	}

	public function getSingelPlants($country_id, $state_id, $district_id, $plantguid)
	{
	    $sql = "select PlantGUID, SWNID from tblpatplantdetail where 1=1";
	   	if(isset($country_id) && !empty($country_id)){
			$sql.= " AND CountryID = '".$country_id."'";
		   }
		if(isset($state_id) && !empty($state_id)){
			$sql.= "AND StateID = '".$state_id."'";
		}
			if(isset($district_id) && !empty($district_id)){ 
			$sql.= "AND District = '".$district_id."'";
		}
			if(isset($plantguid) && !empty($plantguid)){
			$sql.= "AND PlantGUID= '".$plantguid."'";
		}
		$sql.= "ORDER BY SWNID ASC";

		//echo $sql; die;
	
		$res = $this->Common_model->query_data($sql);
		print_r($res); 

		$options_plants = '<option value="">All</option>';
		foreach($res as $plant)
		{
			$options_plants .= '<option value="'.$plant->PlantGUID.'" SELECTED>'.$plant->SWNID.'</option>';
		}

		echo $options_plants;
	}


	public function DatafromOnWebService_Map()
	{
		$sql = "SELECT DISTINCT 
				tbltstlocation.XValue AS Latitude,
				tbltstlocation.YValue AS Longitude
				FROM tbltstlocation 
				INNER JOIN tblTSTSampleTestResult ON tbltstlocation.GUID=tblTSTSampleTestResult.LocationGUID
				INNER JOIN mstuser ON tbltstlocation.userid=mstuser.userid
				WHERE tblTSTSampleTestResult.ImpurityFlag=1";
		$res = $this->Common_model->query_data($sql);
		echo json_encode($res);
	}


	/**
	 * Method getRoleManagement() get All Role Management.
	 *
	 * @access	public
	 * @param	Null
	 * @return	Array
	 */
	public function getRoleManagement($roleid)
	{
		$sql = "select * from user_role_mapping where role_id = ".$roleid;
		$res = $this->db->query($sql)->result()[0];
		print_r($res);
	}


}