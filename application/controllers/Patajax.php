<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patajax extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
/********
	//////// getSWNID all SWNID /////////////////
*/
public function getSWNID()
{
	$ROLE_ID = $this->session->userdata('login_data')['ROLE_ID']; 
	//print_r($ROLE_ID);
	if ($ROLE_ID == 1) {
		$this->db->select('PlantGUID,SWNID');
		$this->db->FROM('tblpatplantdetail');
		$this->db->group_by('PlantGUID');
		$this->db->order_by('SWNID');
		$data = $this->db->get()->result();
	//echo $data;
	//return json_encode(["data"=>$data]);
		die(json_encode(["data"=>$data]));
	}
	 else if($ROLE_ID == 9){

		// $this->db->select('PlantGUID,SWNID');
		// $this->db->FROM('tblpatplantdetail');
		// $this->db->group_by('PlantGUID');
		// $this->db->where('StateID', $this->session->userdata('login_data')['STATEID']);
		// $this->db->order_by('SWNID');

		$this->db->select('*');    
		$this->db->from('honeywell_patplantdetail a');
		$this->db->join('tblpatplantdetail b', 'a.PlantGUID = b.PlantGUID');
		// $this->db->where('StateID', $this->session->userdata('login_data')['STATEID']);
		$this->db->order_by('SWNID');

// $query= "SELECT a.iJal_Stations, b.SWNID, b.PlantGUID FROM `honeywell_patplantdetail` a inner join tblpatplantdetail b on a.iJal_Stations = b.SWNID";

		$data = $this->db->get()->result();
		// echo $this->db->last_query(); die();
	// echo "SELECT * FROM credit WHERE pid='.$_GET['payment_id'].'";;
	//return json_encode(["data"=>$data]);
		die(json_encode(["data"=>$data]));
	}else{
		$this->db->select('PlantGUID,SWNID');
		$this->db->FROM('tblpatplantdetail');
		$this->db->group_by('PlantGUID');
		$this->db->order_by('SWNID');
		$data = $this->db->get()->result();
	//echo $data;
	//return json_encode(["data"=>$data]);
		die(json_encode(["data"=>$data]));
	}

}


	/********
	//////// getLocations all Location /////////////////
*/
	public function getLocations()
	{
		try {
			$this->db->select('*');
			$this->db->from(PATPLANDETAIL);
			$this->db->join(PATPOD, PATPOD.'.PlantGUID ='.PATPLANDETAIL.'.PlantGUID');
			$this->db->join(COUNTRY, COUNTRY.'.CountryID ='.PATPLANDETAIL.'.CountryID');
			$this->db->join(STATE, STATE.'.StateID ='.PATPLANDETAIL.'.StateID');
			$this->db->join(DISTRICT, DISTRICT.'.DistrictID ='.PATPLANDETAIL.'.District');
			$data = $this->db->get()->result(); //echo $this->db->last_query(); die;
			die(json_encode(["data"=>$data]));
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

}