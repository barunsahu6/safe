<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financial extends CI_Controller
{
	public $model = '';
	public $view = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
				
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
	}
	
	public function index($id = NULL)
	{
	 
		 $userid       =   $this->session->userdata['login_data']['USERID'];////// Session Userid/////
         $roleid       =   $this->session->userdata['login_data']['LEVEL']; ////// Session Role Id /////
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
		 	 $strwhr     	= $this->input->post('Country');
		 	 $stateid 	    = $this->input->post('State');
		 	 $districtid    = $this->input->post('District');
		 	 $plantid 	    = $this->input->post('Plant');

			$this->session->set_userdata("Country",$this->input->post('Country'));
		 	$this->session->set_userdata("State",$this->input->post('State'));
		 	$this->session->set_userdata("District",$this->input->post('District'));
		 	$this->session->set_userdata("Plant",$this->input->post('Plant'));
			}

			
	

		 if(!empty($this->session->userdata("Country"))){
			 $strwhr = $this->session->userdata("Country");
		 }else{
			 $strwhr = $this->session->userdata['login_data']['COUNTRYID'];
		 }
		if(!empty($this->session->userdata("State"))){
			 $strstatewhr = $this->session->userdata("State");
		 }
		 if(!empty($this->session->userdata("District"))){
			 $districtwhr = $this->session->userdata("District");
		 }
		 if(!empty($this->session->userdata("Plant"))){
			 $plantwhr = $this->session->userdata("Plant");
		 }
 
		$content = array();
        $content['Country']   			 		= $this->model->getCountry();
        $content['State']     			 		= $this->model->getStates();
		$content['District']             		= $this->model->getDistricts($strwhr,$strstatewhr);
		$content['getYearWiseFunder']           = $this->model->getDashboardYearWiseFunder($strwhr,$strstatewhr,$districtwhr,$plantwhr);
		$content['GetPiechart']                 = $this->model->CreatePieChart($strwhr,$strstatewhr,$districtwhr,$plantwhr);
	    ///////////// latest Dashboard ///////////////													
		$content['GetCOPEX']       			    = $this->model->getFinancialCOPEX($strwhr,$strstatewhr,$districtwhr,$plantwhr);
		$content['GetfourthOpexServic']         = $this->model->getfourthOpex($strwhr,$strstatewhr,$districtwhr,$plantwhr);

		$this->load->view(FTOP, $this->view);
		$this->load->view(NAVTOP, $this->view);
		$this->load->view('Financial/index', $content);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function countCountry()
	{
		try {
			 $this->db->count_all(DISTRICT); //echo $this->db->last_query(); die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
}