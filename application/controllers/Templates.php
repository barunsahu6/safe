<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends CI_Controller 
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function Templates()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$this->view['title']  = ucfirst($this->router->method).' '. ucfirst(str_replace('_',' ',$this->router->class));
		$this->view['status'] = array(0=>'Inactive',1=>'Active',2=>'Deleted');
		
	/*	
		//Get Template
		$templates = $this->model->getTemplates();
		$this->view['Templates'][''] = 'All';
		foreach($templates as $template){
			$this->view['Templates'][$template->template_type_id] = $template->template_type;
		}*/
		
		
		//Get Updates
		$updates = $this->model->getUpdates();
		$this->view['updates'][''] = ' All ';
		foreach($updates as $update){
		$this->view['updates'][$update->update_id] = $update->title;
		}
		
		//SET VALIDATION RULES
		$this->valid = array(
				   array(
						 'field'   => 'form[template_type]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' template_type', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[update_id]', 
						 'label'   => 'Update', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[html_header]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' html_header', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[html_footer]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' html_footer', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[word_header]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' word_header', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[word_footer]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' word_footer', 
						 'rules'   => 'trim|required'
				   )
				 );
		 $this->form_validation->set_rules($this->valid);
		//$this->form_validation->set_rules('form[template_type]', ucfirst(str_replace('_',' ',$this->router->class)).' template_type', 'callback_name_validation');
		
		
		//Ckeditor's configuration
		$this->view['ck_html_header'] = array(			
			//ID of the textarea that will be replaced
			'id' 	=> 	'form[html_header]',
			'path'	=>	'theme/uveous/front/js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", 	//Using the Full toolbar
				'width' 	=> 	"780px",	//Setting a custom width
				'height' 	=> 	'150px',	//Setting a custom height						
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(				
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Blue',
						'font-weight' 		=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 		=> 	'Red Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Red',
						'font-weight' 		=> 	'bold',
						'text-decoration'	=> 	'underline'
					)
				)				
			)
			
		);
		
		$this->view['ck_html_footer'] = array(			
			//ID of the textarea that will be replaced
			'id' 	=> 	'form[html_footer]',
			'path'	=>	'theme/uveous/front/js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", 	//Using the Full toolbar
				'width' 	=> 	"780px",	//Setting a custom width
				'height' 	=> 	'150px',	//Setting a custom height						
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(				
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Blue',
						'font-weight' 		=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 		=> 	'Red Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Red',
						'font-weight' 		=> 	'bold',
						'text-decoration'	=> 	'underline'
					)
				)				
			)
		);
		
		
		$this->view['ck_word_header'] = array(			
			//ID of the textarea that will be replaced
			'id' 	=> 	'form[word_header]',
			'path'	=>	'theme/uveous/front/js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", 	//Using the Full toolbar
				'width' 	=> 	"780px",	//Setting a custom width
				'height' 	=> 	'150px',	//Setting a custom height						
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(				
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Blue',
						'font-weight' 		=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 		=> 	'Red Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Red',
						'font-weight' 		=> 	'bold',
						'text-decoration'	=> 	'underline'
					)
				)				
			)
		);
		
		
		
		
		
		$this->view['ck_word_footer'] = array(			
			//ID of the textarea that will be replaced
			'id' 	=> 	'form[word_footer]',
			'path'	=>	'theme/uveous/front/js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", 	//Using the Full toolbar
				'width' 	=> 	"780px",	//Setting a custom width
				'height' 	=> 	'150px',	//Setting a custom height						
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(				
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Blue',
						'font-weight' 		=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 		=> 	'Red Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Red',
						'font-weight' 		=> 	'bold',
						'text-decoration'	=> 	'underline'
					)
				)				
			)
		);
		$this->load->view(FTOP, $this->view);		
	}
	
	
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
	/*echo'<pre>';
		print_r($this->input->post());die;*/
		  $keyword=$this->input->post(keyword);
		  $status=$this->input->post(status_id);
		  $Order_by=$this->input->post(Order_by);
		  $form=$this->input->post('form');
		  $template=$form['template_type'];
			
		$config["base_url"] 	= base_url().$this->router->class.'/'.$this->router->method;
		$config["total_rows"]	= $this->model->count_rows();
		$config["per_page"]		= PER_PAGE;
		$config["uri_segment"]	= URI_SEGMENT;
		$choice	= $config["total_rows"] / $config["per_page"];	
		$this->pagination->initialize($config);
	
		$this->view['page'] = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['rows'] = $this->model->lists($config["per_page"],$this->view['page'],$keyword,$status,$Order_by,$template);
		
		$links	= $this->pagination->create_links();
		$startRecord	= ($config["total_rows"]>0) ? $this->view['page']+1 : $this->view['page'];
		$endRecord		= $this->view['page']+$config["per_page"];
		$endRecords		= ($config["total_rows"] < $endRecord) ? $config["total_rows"] : $endRecord;
		
		$this->view['paging'] = "Displaying (".$startRecord." - ".$endRecords.") of ".$config["total_rows"]." <b>|</b> ".$links.' of '.ceil($choice)." pages";
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method add() add new accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function add()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		if($this->input->post($this->router->method))
		{
			if($this->form_validation->run())
			{
				if($this->model->add() == '1'){
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' added successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not added!!');
					redirect($this->router->class.'/'.$this->router->method);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method edit() update accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function edit()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->getDetail($this->view['token']);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		if($this->input->post($this->router->method))
		{
			if($this->form_validation->run())
			{
				if($this->model->edit($this->view['token']) == '1'){
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' updated successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not updated!!');
					redirect($this->router->class.'/'.$this->router->method.'/'.$this->view['token']);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->getDetail($this->view['token']);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		if($this->model->delete($this->view['token']) == '1'){
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}
	 }
	 
	 /**
	 * Method view() view accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function view()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->viewDetail($this->view['token']);//print_r($this->view['detail']);die;
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method name_validation() check unique name.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	/*public function name_validation()
	{
		return $this->model->name_validation();
	}*/
}