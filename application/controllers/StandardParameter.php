<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StandardParameter extends CI_Controller
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function StandardParameter()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		//print_r($this->model);
		$this->view['title']  = ucfirst($this->router->method).' '. ucfirst(str_replace('_',' ',$this->router->class));
		$this->view['status'] = array(0=>'Pending',1=>'Active');
				
		
		//SET VALIDATION RULES
		$this->valid = array(
				   array(
						 'field'   => 'form[ParameterID]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' ParameterID', 
						 'rules'   => 'trim|required'
				   ),
				
				    array(
						 'field'   => 'form[StandardID]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' StandardID', 
						 'rules'   => 'trim|required'
				   ),
				   array(
						 'field'   => 'form[AcceptableLimit]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' AcceptableLimit', 
						 'rules'   => 'trim|required'
				   ));
		 $this->form_validation->set_rules($this->valid);
		//$this->form_validation->set_rules('form[email]', ucfirst(str_replace('_',' ',$this->router->class)).' email', 'callback_name_validation');
		//$this->form_validation->set_rules('form[AcceptableLimit]', ' AcceptableLimit', 'callback_AcceptableLimit_validation');
		
		
		$this->load->view(FTOP, $this->view);
		$this->load->view(NAVTOP, $this->view);		
	}
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
	//echo "dscdsf";
		
		$config["base_url"] 	= base_url().$this->router->class.'/'.$this->router->method;
		$config["total_rows"]	= $this->model->count_rows();
		$config["per_page"]		= PER_PAGE;
		$config["uri_segment"]	= URI_SEGMENT;
		$choice	= $config["total_rows"] / $config["per_page"];	
		$this->pagination->initialize($config);
	
		$this->view['page'] = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['rows'] = $this->model->lists($config["per_page"],$this->view['page'],$keyword,$level,$Order_by);
		$links	            = $this->pagination->create_links();
		$startRecord	= ($config["total_rows"]>0) ? $this->view['page']+1 : $this->view['page'];
		$endRecord		= $this->view['page']+$config["per_page"];
		$endRecords		= ($config["total_rows"] < $endRecord) ? $config["total_rows"] : $endRecord;
		
		$this->view['paging'] = "Displaying (".$startRecord." - ".$endRecords.") of ".$config["total_rows"]." <b>|</b> ".$links.' of '.ceil($choice)." pages";
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
			
	}
	
	/**
	 * Method add() add new accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function add()
	{
			
		//SET VALIDATION RULES
		$this->valid[] = array(
						 'field'   => 'form[ParameterID]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' ParameterID', 
						 'rules'   => 'trim|required'
				   		);

							
		$this->valid[] = array(
				    	 'field'   => 'form[StandardID]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' StandardID', 
						 'rules'   => 'trim|required'
				 		);
						
		$this->valid[] = array(
				    	 'field'   => 'form[AcceptableLimit]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' AcceptableLimit', 
						 'rules'   => 'trim|required'
				 		);
						//echo $test = $this->form_validation->run();
						
		$form = $this->input->post('form');	
		//print_r($form); die;
		if(!empty($form))
		{
			if($this->form_validation->run())
			{
				//echo "Amit"; die;
				if($this->model->add() == '1'){
					$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' added successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('err_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not added!!');
					redirect($this->router->class.'/'.$this->router->method);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method edit() update accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function edit()
	{
		$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$data = $this->model->getDetail($token);
				
		//echo $this->router->method;
		//print_r($this->input->post($this->router->method));  //die;
		$form = $this->input->post('form');
		if(!empty($form ))
		{
			if($this->model->edit($token)==1){
					$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' updated successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not updated!!');
					redirect($this->router->class.'/'.$this->router->method.'/'.$this->view['token']);
				}
			
		}
		//echo "test2"; die;
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete()
	{
		$token  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->getDetail($token);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		if($this->model->delete($token) == '1'){
			$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}
	 }
	 
	 /**
	 * Method view() view accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function view()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->viewDetail($this->view['token']);//print_r($this->view['detail']);die;
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method name_validation() check unique email.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function name_validation()
	{
		return $this->model->name_validation();
	}
	
	
	/**
	 * Method username_validation() check unique username.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function StandradName_validation()
	{
		return $this->model->AcceptableLimit_validation();
	}
}