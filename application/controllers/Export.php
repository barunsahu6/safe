<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller 
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function __construct() {
        parent::__construct();
        // Load the Library
        $this->load->library("excel");
        // Load the Model
        $this->load->model("Your_model_name");
    }
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
		//echo "dscdsf";
		
		$config["base_url"] 	= base_url().$this->router->class.'/'.$this->router->method;
		$config["total_rows"]	= $this->model->count_rows();
		$config["per_page"]		= PER_PAGE;
		$config["uri_segment"]	= URI_SEGMENT;
		$choice	= $config["total_rows"] / $config["per_page"];	
		$this->pagination->initialize($config);
	
		$this->view['page'] = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['rows'] = $this->model->lists($config["per_page"],$this->view['page'],$keyword,$level,$Order_by);
		$links	            = $this->pagination->create_links();
		$startRecord	= ($config["total_rows"]>0) ? $this->view['page']+1 : $this->view['page'];
		$endRecord		= $this->view['page']+$config["per_page"];
		$endRecords		= ($config["total_rows"] < $endRecord) ? $config["total_rows"] : $endRecord;
		
		$this->view['paging'] = "Displaying (".$startRecord." - ".$endRecords.") of ".$config["total_rows"]." <b>|</b> ".$links.' of '.ceil($choice)." pages";
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
			
	}
	
	/**
	 * Method add() add new accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function add()
	{
			
		//SET VALIDATION RULES
		$this->valid[] = array(
						 'field'   => 'form[CountryID]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' CountryID', 
						 'rules'   => 'trim|required'
				   		);

		$this->valid[] = array(
				    	 'field'   => 'form[StateID]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' StateID', 
						 'rules'   => 'trim|required'
				 		);
						
		$this->valid[] = array(
				    	 'field'   => 'form[DistrictName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' DistrictName', 
						 'rules'   => 'trim|required'
				 		);
						//echo $test = $this->form_validation->run();
						
		$form = $this->input->post('form');	
		//print_r($form); die;
		if(!empty($form))
		{
			if($this->form_validation->run())
			{
				//echo "Amit"; die;
				if($this->model->add() == '1'){
					$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' added successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('err_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not added!!');
					redirect($this->router->class.'/'.$this->router->method);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	


	
	
	
	
	
}