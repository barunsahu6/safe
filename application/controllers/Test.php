<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller 
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function Api()
	{
		parent::__construct();
		
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$this->view['title']  = ucfirst($this->router->method).' '. ucfirst(str_replace('_',' ',$this->router->class));

		//$this->load->view(FTOP, $this->view);
		//$this->load->view(NAVTOP, $this->view);		
	}
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
		
		Echo "Api ";
		
		
		
	}

	private function authenticate($username, $password)
	{	
		$sql = "select * from mstuser where UserName = '".$username."' limit 1";
		$res = $this->db->query($sql);
		$res_row = $res->result();

		if (count($res_row) < 1) {
			return false;
		}

		if($res_row[0]->UserName === $username && $res_row[0]->Password === $password)
		{
			return true;
		}
		return false;
	}

	
	private function getuserid($username,$password)
	{
				
		$sql = "select * from mstuser where UserName = '".$username."' and Password = '".$password."'";
		$res = $this->model->query_data($sql);
		//print_r($res);
		return $res[0]->UserID;
	}

	private function getplantid($userid)
	{
				
		$sql = "select PlantGUID from tblpatplantdetail where UserID = '".$userid."'";
		$res = $this->model->query_data($sql);
		//print_r($res); 
		//echo $res[0]->PlantGUID;
		//die('Amit Plant ID');
		return $res;
	}

	public function getMasters()
	{	

		
		if($this->input->post('UserName') === null || $this->input->post('Password') === null)
		{
			echo json_encode("ERROR : You must send username and password with the request");
			die();
		}
		$username = $this->security->xss_clean($this->input->post('UserName'));
		$password = $this->security->xss_clean($this->input->post('Password'));

		$authlogin = $this->authenticate($username, $password);

		if(!$authlogin)
		{
			echo json_encode("Incorrect username and/or password");
		}
		else
		{
			$data = array();

			$sql = "select * from mstuser where UserName = '".$username."'";
			$tblusers = $this->model->query_data($sql);

			
			$sql = "select s.StateID, s.StateName from mststate s INNER JOIN mstcountry c on c.CountryID = s.CountryID ";
			$data['mststates'] = $this->db->query($sql)->result();
			

			$sql = "select d.*, c.CountryID, s.stateID from mstdistrict d 
					inner join mststate s on s.stateID=d.stateID 
					inner join mstcountry c on c.CountryID = d.CountryID ";
			$data['mstdistrict'] = $this->db->query($sql)->result();

			$sql = "select * from mststandard s INNER JOIN mstcountry c on c.CountryID=s.CountryID";
			$data['mststandard'] = $this->db->query($sql)->result();
			
			$sql = "select * from mstparameter";
			$data['mstparameter'] = $this->db->query($sql)->result();
			

			$sql = "select * from mststandardparameterdetail stp 
					inner join mstparameter p on p.ParameterID = stp.ParameterID 
					inner join mststandard  s on s.StandardID = stp.StandardID ";
			$data['mststandardparameterdetail'] = $this->db->query($sql)->result();
			

			$sql = "select * from mstplantspecification ";
			$data['mstplantspecification'] = $this->db->query($sql)->result();
			
			$sql = "select * from mstaggregator";
			$data['mstaggregator'] = $this->db->query($sql)->result();
			echo json_encode($data);

			}
	}

	public function GetUserData()
	{
		//header("Content-type : application/json");

	if($this->input->post('UserName') === null || $this->input->post('Password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('UserName'));
		$password = $this->security->xss_clean($this->input->post('Password'));

		$authlogin = $this->authenticate($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

			$sql = "select * from mstuser where UserName = '".$username."'";
			//$tblusers = $this->model->query_data($sql);

			$current_pass = $this->security->xss_clean($this->input->post('CurrentPass'));

			$offset = $current_pass * 500;

			$userid = $this->getuserid($username, $password);
			//echo json_encode($userid); 

			$plantguid = $this->getplantid($userid);
			//echo json_encode($plantguid); 

			$data = array();
			$tblpatplantdetail = array();
			$tblpatpodetail = array();
			$tblpatplantassetfunder = array();
			$tblpatplantdistribution = array();
			$tblpatplantproddetail = array();
			$tblpatplantpurificationstep = array();
			$tblpatwatercontaminants = array();
			
		$res_patplantdetail_array = array();
		foreach ($plantguid as $row) {
					$guids .= "'".$row->PlantGUID."',";
			}

			$guids = substr($guids, 0,-1);
			//echo $guids; //die();

			  $sql_patplantdetail = 'select `PlantGUID`,`SWNID`,`AgencyID`,`LocationType`,`CountryID`,`StateID`,`District`,`BlockName`,
								   `VillageName`,`PinCode`,`SC`,`ST`,`OBC`,`General`,`AllInclusion`,`OperatorName`,`DesignationID`,
								   `ContactNumber`,`LiteracyID`,`PlantSpecificationID`,`PlantManufacturerID`,`ManufacturerName`,
								   `EstablishmentDate`,`AgeOfPlant`,`RemoteMonitoringSystem`,`LandCost`,`LandOther`,`MachineryCost`,
								   `MachineryOther`,`BuildingCost`,`BuildingOther`,`RawWaterSourceCost`,`RawWaterSourceOther`,
								   `ElectricityCost`,`ElectricityOther`,`Distribution`,`NoOfhhregistered`,`AvgMonthlyCards`,
								   `GramPanchayatApproval`,`LegalElectricityConnection`,`LandApproval`,`RawWaterSourceApproval`,`RejectWaterDischargeApproval`,
								   `ServiceProviderApproval`,`OperatorQualification`,`WaterBrandName`,`PlantSpecificationAnyOther`,`PAssetSourceFundedBy`,
								   `auditAgencyOther`,`EducationAnyOther`,`ContaminantAnyOther`,`Population`,`NoOfHousehold`,`NoOfHouseholdWithin2km`,`BPL`
			 					    from tblpatplantdetail where PlantGUID in ('.$guids.')';
								    $data['tblpatplantdetail'] = $this->db->query($sql_patplantdetail)->result();

			
     		 $sql_patpodetail = ' select `PlantPOUID`, `POGUID`, `PlantGUID`, `VisitDate`, `AuditedBy`, `Latitude`, `Longitude`,
								`AgencyAnyOther`, `AuditingAgency`, `AuditingAgencyAddress`, `ContactName`, `Email`, `ElectricalSafety`, `Earthing`,
								`PlantOM`, `Operatorcertificate`, `WaterQuality`, `LastWQtestreport`, `ConsumerAwareness`, `CheckforIECmaterial`, 
								`ABKeeping`, `CheckforReceipts`, `UV`, `TDS`, `pH`, `ResidualChlorine`, `Microbial`, `PreMonsoonRWTesting`, 
								`PreMonsoonRWLabAdr`, `PreMonsoonRWTestingdate`, `PreMonsoonRWTestProof`, `PreMonsoonTWTesting`, `PreMonsoonTWLabAdr`, 
								`PreMonsoonTWTestingdate`, `PreMonsoonTWProof`, `PostMonsoonRWTesting`, `PostMonsoonRWLabadr`, `PostMonsoonRWtestingdate`, 
								`PostMonsoonRWTestProof`, `PostMonsoonTWtesting`, `PostMonsoonTWLabAdr`, `PostMonsoonTWtestingdate`, `PostMonsoonTWtestProof`, 
								`RejectWaterTesting`, `RejectWaterLabAdr`, `RejectWatertestingdate`, `RejectWaterTestProof`, `RawWaterOpenwellcovered`, 
								`RawWaterBoreWellCasing`, `Insidetheplant`, `Covered`, `CleanlinessinPlant`, `Leakage`, `CleanlinessNearTreatmentPlant`,
								`Checkmossoralgee`, `TechnicalDowntime`, `NoOfDays`, `NoOfFault`, `MotorRepair`, `MembraneChoke`, `Rawwaterproblem`,
								`Electricityoutage`, `Anyother`, `AnyotherEdit`, `SalesDayLost`, `WaterProductionDaily`, `WaterProductionMonthly`, 
								`SamplingOrGift`, `LeakageEdit`, `WaterTreatmentPlantCost`, `InfrastructureCost`, `WaterBill`, `ElectricityBill`, 
								`GeneratorMaintainance`, `Rent`, `OperatorSalary`, `ChemicalAndOtherConsumable`, `MiscellaneousChk`, `MiscFirstText`,
								`MiscFirstAmount`, `MiscSecondText`, `MiscSecondAmount`, `MiscThirdText`, `MiscThirdAmount`, `ServiceCharge`, 
								`AssestRenewalFund`, `AssestRepaymentFund`, `OPEx`, `OPExservicecharge`, `OPExservicechargemaintenance`, 
								`opExSCMRAssetRepayment`, `checkRWQuantification`, `CheckRWUtilisation`, `CheckRWDisposal`, `CheckWharvesting`, 
								`WQuantification`, `WUtilisation`, `WDisposal`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsTablet`,
								`IsSynched`, `IsComplete`, `Presenceofpuddle`, `UtilizationActivity1`, `UtilizationActivity2`, `UtilizationActivity3`, 
								`UtilizationActivity4`, `UtilizationOther1`, `UtilizationOther2`, `UtilizationOther3`, `UtilizationOther4`, `OperatorName`,
								`DesignationID`, `ContactNumber`, `LiteracyID`, `EducationAnyOther`, `TransportExpense`, `PeakSale`
     							FROM tblpatpodetail where PlantGUID in ('.$guids.')';

								$data['tblpatpodetail'] = $this->db->query($sql_patpodetail)->result();

								 $sql_patplantassetfunder = ' select * FROM tblpatplantassetfunder where PlantGUID in ('.$guids.')';
				 				$data['tblpatplantassetfunder'] = $this->db->query($sql_patplantassetfunder)->result();	

								$sql_patplantdistribution = ' select * FROM tblpatplantdistribution where PlantGUID in ('.$guids.')';
								$data['tblpatplantdistribution'] = $this->db->query($sql_patplantdistribution)->result();	

								$sql_patplantproddetail = ' select * FROM tblpatplantproddetail where PlantGUID in ('.$guids.')';
								$data['tblpatplantproddetail'] = $this->db->query($sql_patplantproddetail)->result();	

								$sql_patplantpurificationstep = ' select * FROM tblpatplantpurificationstep where PlantGUID in ('.$guids.')';
								$data['tblpatplantpurificationstep'] = $this->db->query($sql_patplantpurificationstep)->result();	

								$sql_patwatercontaminants = ' select * FROM tblpatwatercontaminants where PlantGUID in ('.$guids.')';
								$data['tblpatwatercontaminants'] = $this->db->query($sql_patwatercontaminants)->result();	

			echo json_encode($data);

		}
	}

	public function uploadData()
	{
		

		$data = $this->input->post('data');
		$data_array = json_decode($data);

		if($this->input->post('UserName') === null || $this->input->post('Password') === null)
		{
			echo json_encode("ERROR : You must send username and password with the request");
			die();
		}
		$username = $this->security->xss_clean($this->input->post('UserName'));
		$password = $this->security->xss_clean($this->input->post('Password'));

		$authlogin = $this->authenticate($username, $password);

		if(!$authlogin)
		{
			echo json_encode("Incorrect username and/or password");
		}
		else
		{
		
		if($data_array == null)
		{
			echo json_encode("ERROR: Please send properly formatted json");	
		}
		else
		{
			$res = $this->api_model->uploaddata();
			if($res === null)
			{
				echo json_encode("success");
			}
			else
			{
				echo json_encode("error");
			}
		}

		}




	}





}