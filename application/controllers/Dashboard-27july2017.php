<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public $model = '';
	public $view = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
				
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
	}
	
	public function index($id = NULL)
	{
		//print_r($this->input->post());
		//die;
		if(!empty($this->input->post())){
		 	 $countryid 	= $this->input->post('Country');
		 	 $stateid 	    = $this->input->post('State');
		 	 $districtid    = $this->input->post('District');
		 	 $plantid 	    = $this->input->post('Plant');

		 if(!empty($countryid)){
			 $strwhr = $countryid;
		 }
		  if(!empty($stateid)){
			 $strstatewhr = $stateid;
		 }
		 if(!empty($districtid)){
			 $districtwhr = $districtid;
		 }
		 if(!empty($plantid)){
			 $plantwhr = $plantid;
		 }


		}
	
		//die;
		 $userid       =   $this->session->userdata['login_data']['USERID'];////// Session Userid/////
         $roleid       =   $this->session->userdata['login_data']['LEVEL']; ////// Session Role Id /////
		 $strwhr       =   $this->session->userdata['login_data']['COUNTRYID']; 
		 
		$content = array();
		
        $content['Country']   			 = $this->model->getCountry();
        $content['State']     			 = $this->model->getStates();
		//$content['District']  			 = $this->model->getDistrict();
		//$content['Plant']     			 = $this->model->getPlants();
		$content['CountPlants']     	 = $this->model->getCountPlants($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['CountPerformance']     = $this->model->getPerformance($strwhr,$strstatewhr,$districtid,$plantwhr);
					
		//$content['District']  = $this->model->getDistrict();
       // $content['Plant']     = $this->model->getPlants();
	    $content['Tab_TotalScore'] 				= $this->model->getPATSofiescore($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['Getfinandata']   				= $this->model->getDashboardfinancial($strwhr);
		$content['getPerformanceSofiScore']     = $this->model->getPerformanceSofieScore($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['getPlantAge']                 = $this->model->getDashboardPlantAndAge($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['getDistributionPoint']        = $this->model->getDashboardDistributionPoint($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['getPopulation']               = $this->model->getDashboardPopulation($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['getPlantEvl']                 = $this->model->getDashboardPlantEvl($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['getPerformanceWithLimit']     = $this->model->getPerformanceLimit($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['getuser']                     = $this->model->getDashboardUserActiveAndInactive($strwhr,$strstatewhr,$districtid,$plantwhr);
		$content['Getgaugedata']                = $this->model->getguage($strwhr,$strstatewhr,$districtid,$plantwhr);

		$this->load->view(FTOP, $this->view);
		$this->load->view(NAVTOP, $this->view);
		$this->load->view('Dashboard/index', $content);
		$this->load->view(FBOTTOM, $this->view);
	}
	
/**
	 * Method count_rows() get total number of products.
	 * used for pagination.
	 * @access	public
	 * @param	Null
	 * @return	integer number
	 */
	public function countCountry()
	{
		try {
			 $this->db->count_all(DISTRICT); //echo $this->db->last_query(); die;
		}
		catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
}