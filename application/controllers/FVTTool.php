<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FVTTool extends CI_controller 
{
	public function FVTTool()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->library('form_validation');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
	    $this->model = $this->$mod;
		$this->view['title']  = ucfirst($this->router->method).' '. ucfirst(str_replace('_',' ',$this->router->class));
		$content['status'] = array(1=>'Available',2=>'not Available');
		//SET VALIDATION RULES
		$this->valid = array(
				   
				   array(
						 'field'   => 'form[OldPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' OldPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				  array(
						 'field'   => 'form[NewPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' NewPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				  array(
						 'field'   => 'form[ConfrimPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' ConfrimPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				 );
		 $this->form_validation->set_rules($this->valid);
		//$this->form_validation->set_rules('form[email]', ucfirst(str_replace('_',' ',$this->router->class)).' email', 'callback_name_validation');
		$this->form_validation->set_rules('form[UserName]', ' username', 'callback_username_validation');
		
		
		$this->load->view(FTOP, $this->view);
		$this->load->view(NAVTOP, $this->view);		
	}
	
	
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
	    
		$this->load->helper(array('form','url'));
		$content['getdata'] ='';
		$this->load->view('FVTTool/index',$content);
		$this->load->view(FBOTTOM, $this->view);
	}

    public function index_1()
	{
	    
		$this->load->helper(array('form','url'));
		$data['detail'] =$this->model->masterinputrecords();
		$this->load->view('FVTTool/index_1',$data);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function breakeven_analysis()
	{
	    
		$this->load->helper(array('form','url'));
		//$content['getdata'] ='';
		$sqlinput = "SELECT *  FROM `masterinputrecords`";
		$content['details'] = $this->db->query($sqlinput)->result()[0];
		$this->load->view('FVTTool/breakeven_analysis',$content);
		$this->load->view(FBOTTOM, $this->view);
	}

		public function break_event_graph()
	{
	    
		$this->load->helper(array('form','url'));
		//$content['getdata'] ='';
		$sqlinput = "SELECT *  FROM `masterinputrecords`";
		$content['details'] = $this->db->query($sqlinput)->result()[0];
		$this->load->view('FVTTool/break_event_graph',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function index_2()
	{
	    
		$this->load->helper(array('form','url'));
		$content['getdata'] ='';
		$this->load->view('FVTTool/index_2',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function index_3()
	{
	    
		$this->load->helper(array('form','url'));
		$content['getdata'] ='';
		$this->load->view('FVTTool/index_3',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function index_4()
	{
	    
		$this->load->helper(array('form','url'));
		$data['detail'] =$this->model->getdata();
		// print_r($data['detail'][0]->Population);
		// exit;
		$this->load->view('FVTTool/index_4',$data);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function index_9()
	{
	    
		$this->load->helper(array('form','url'));
		$content['getdata'] ='';
		
		$this->load->view('FVTTool/index_9',$content);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function index_10()
	{
	    
		$this->load->helper(array('form','url'));
		$RequestMethod = $this->input->server('REQUEST_METHOD');

        // if ($RequestMethod == 'POST') {
		// 	print_r($_POST);
        // }
		$data['detail'] =$this->model->masterinputrecords();
		$query = "SELECT *  FROM `tblploutput`";
		$data['output'] = $this->db->query($query)->result();
		// print_r($data['output']);
		// exit;
		$this->load->view('FVTTool/index_10',$data);
		$this->load->view(FBOTTOM, $this->view);
	}
	public function update_masterdata($detailguid = null) {
			
		if($this->input->post()){
			// print_r($detailguid);
			// exit;
			
			$arr = array();
			$this->form_validation->set_rules('resonval', 'Reson', 'trim|required|xss_clean');
		
				$data = array(
					'Household_Size' => ($this->input->post('Household_Size')),
					'RegHousehold' => ($this->input->post('RegHousehold')),
					'ConsumptionPerHouseHoldPerDay' => ($this->input->post('ConsumptionPerHouseHoldPerDay')),
					'CanSize' => ($this->input->post('CanSize')),
					'DistributionHamlets' => ($this->input->post('DistributionHamlets')),
					'CapacityOfPlant' =>($this->input->post('CapacityOfPlant')),
					'OperatingHours' =>($this->input->post('OperatingHours')),
					'Backwash' =>($this->input->post('Backwash')),
					"Updated_on"=> date('Y-m-d')
				);

				$this->db->where('GUID', $detailguid);
				$unitid = $this->db->update('masterinputrecords', $data);
                

				// $arr['interruptstage'] = $this->security->xss_clean($this->input->post('interruption_stage'));

				if ($unitid > 0) {
					$arr['status'] = 'true';
					$arr['result'] = 1;
					$arr['message'] = 'Data Saved Succesfully.';
				}else{
                    $arr['result'] = 0;
					$arr['status'] = 'false';
					$arr['message'] = 'Please fill out all the required fields.';
				}
			

			echo json_encode($arr);

		}
	}	

	public function update_Entrepreneur($detailguid = null) {
			
		if($this->input->post()){
			// print_r($_POST);
			// exit;
			
			$arr = array();
			$this->form_validation->set_rules('resonval', 'Reson', 'trim|required|xss_clean');
		
				$data = array(
					'TreatedWater' => ($this->input->post('TreatedWater')),
					'EnterpreneurROI' => ($this->input->post('EnterpreneurROI')),
					
					"Updated_on"=> date('Y-m-d')
				);

				$this->db->where('GUID', $detailguid);
				$unitid = $this->db->update('masterinputrecords', $data);
                

				// $arr['interruptstage'] = $this->security->xss_clean($this->input->post('interruption_stage'));

				if ($unitid > 0) {
					$arr['status'] = 'true';
					$arr['result'] = 1;
					$arr['message'] = 'Data Saved Succesfully.';
				}else{
                    $arr['result'] = 0;
					$arr['status'] = 'false';
					$arr['message'] = 'Please fill out all the required fields.';
				}
			

			echo json_encode($arr);

		}
	}
	
	public function update_FSE($detailguid = null) {
			
		if($this->input->post()){
			// print_r($_POST);
			// exit;
			
			$arr = array();
			$this->form_validation->set_rules('resonval', 'Reson', 'trim|required|xss_clean');
		
				$data = array(
					'ServiceFeePaid' => ($this->input->post('ServiceFeePaid')),
					'Spares' => ($this->input->post('Spares')),
					
					"Updated_on"=> date('Y-m-d')
				);

				$this->db->where('GUID', $detailguid);
				$unitid = $this->db->update('masterinputrecords', $data);
                

				// $arr['interruptstage'] = $this->security->xss_clean($this->input->post('interruption_stage'));

				if ($unitid > 0) {
					$arr['status'] = 'true';
					$arr['result'] = 1;
					$arr['message'] = 'Data Saved Succesfully.';
				}else{
                    $arr['result'] = 0;
					$arr['status'] = 'false';
					$arr['message'] = 'Please fill out all the required fields.';
				}
			

			echo json_encode($arr);

		}
	}

	public function update_op_exp($detailguid = null) {
			
		if($this->input->post()){
		// 	print_r($_POST);
		//    exit;
			
			$arr = array();
			$this->form_validation->set_rules('resonval', 'Reson', 'trim|required|xss_clean');
		
				$data = array(
					'OperatorSalary' => ($this->input->post('OperatorSalary')),
					'LandRent' => ($this->input->post('LandRent')),
					'RawWaterrSourceRent' => ($this->input->post('RawWaterrSourceRent')),
					'ElectricityBill' => ($this->input->post('ElectricityBill')),
					'GeneratorFule' => ($this->input->post('GeneratorFule')),
					'CostOfDelivery' => ($this->input->post('CostOfDelivery')),
					'ChemicalsAndConsumables' => ($this->input->post('ChemicalsAndConsumables')),
					'OtherOperatorExp' => ($this->input->post('OtherOperatorExp')),
					'OperatorIncentive' => ($this->input->post('OperatorIncentive')),
					
					"Updated_on"=> date('Y-m-d')
				);

				$this->db->where('GUID', $detailguid);
				$unitid = $this->db->update('masterinputrecords', $data);
                

				// $arr['interruptstage'] = $this->security->xss_clean($this->input->post('interruption_stage'));

				if ($unitid > 0) {
					$arr['status'] = 'true';
					$arr['result'] = 1;
					$arr['message'] = 'Data Saved Succesfully.';
				}else{
                    $arr['result'] = 0;
					$arr['status'] = 'false';
					$arr['message'] = 'Please fill out all the required fields.';
				}
			

			echo json_encode($arr);

		}
	}
	
	
	/**
	 * Method add() add new accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function add()
	{
		//SET VALIDATION RULES
		$this->valid[] = array(
						 'field'   => 'form[FirstName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' FirstName', 
						 'rules'   => 'trim|required'
				   		);

		$this->valid[] = array(
				    	 'field'   => 'form[MiddleName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' MiddleName', 
						 'rules'   => 'trim|required|min_length[8]|max_length[20]'
				 		);
				 
		if($this->input->post($this->router->method))
		{
			if($this->form_validation->run())
			{
				if($this->model->add() == '1'){
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' added successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not added!!');
					redirect($this->router->class.'/'.$this->router->method);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method edit() update accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function edit()
	{
		
		$token = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$form = $this->input->post();

		$content = array();
		$content['details']         = $this->model->getDetail($token);  //// Get All detail GUID info
		$content['ContinerType'] 	= $this->model->getContainertype(); //// Get All  ContinerType info
		$content['country'] 		= $this->model->getCountry();		//// Get All Country 
		$content['state'] 			= $this->model->getState();			//// Get All State
		$content['district'] 		= $this->model->getDistrict();		//// Get All District
		$content['source'] 			= $this->model->getSourceType();	//// Get All Source Type
		$content['weather'] 		= $this->model->getWeather();       //// Get All Weather
		$content['season'] 			= $this->model->getSeason();		//// Get All Season
		$content['status'] 		    = $this->model->getArray();			//// Get All Status
		$content['getdata'] 		= $this->model->viewDetail();
		
        // echo '<pre>';
		// print_r($content['details']);
		// die();
		if(!empty($form))
		{
			if($this->model->edit($token)==1){
					$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)). ' updated successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('err_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not updated!!');
					redirect($this->router->class.'/'.$this->router->method.'/'.$this->view['token']);
				}
		}
			
		
		$this->load->view('TSTForm/edit', $content);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->getDetail($this->view['token']);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		if($this->model->delete($this->view['token']) == '1'){
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}
	 }
	 
	 /**
	 * Method view() view accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function view()
	{
		$this->view['token']  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$this->view['detail'] = $this->model->viewDetail($this->view['token']);//print_r($this->view['detail']);die;
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method name_validation() check unique email.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function name_validation()
	{
		return $this->model->name_validation();
	}
	
	
	/**
	 * Method username_validation() check unique username.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function username_validation()
	{
		return $this->model->username_validation();
	}
}