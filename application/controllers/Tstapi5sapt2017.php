<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tstapi extends CI_Controller 
{
	
  public function __construct()
  {
		parent::__construct();
		$this->load->model('Common_model');
	}

	public function index()
	{
		
		echo "TST Api";
	}

	private function authenticate($username, $password)
	{	
		$sql = "select * from mstuser where UserName = '".$username."' limit 1";
		$res = $this->db->query($sql);
		$res_row = $res->result();

		if (count($res_row) < 1) {
			return false;
		}

		if($res_row[0]->UserName === $username && $res_row[0]->Password === $password)
		{
			return true;
		}
		return false;
	}

	
	private function getuserid($username,$password)
	{
				
		$sql = "select * from mstuser where UserName = '".$username."' and Password = '".$password."'";
		$res = $this->Common_model->query_data($sql);
		//print_r($res);
		return $res[0]->UserID;
	}

	private function getplantid($userid)
	{
				
		$sql = "select PlantGUID from tblpatplantdetail where UserID = '".$userid."'";
		$res = $this->Common_model->query_data($sql);
		return $res;
	}

	public function getMasters()
	{	

		// print_r($_POST); die();

		header("Content-Type: application/json");

		if($this->input->post('UserName') === null || $this->input->post('Password') === null)
		{
			echo json_encode("ERROR : You must send username and password with the request");
			die();
		}
		$username = $this->security->xss_clean($this->input->post('UserName'));
		$password = $this->security->xss_clean($this->input->post('Password'));

		$authlogin = $this->authenticate($username, $password);

    if(!$authlogin)
    {
      echo json_encode("Incorrect username and/or password");
    }
    else
    {
      $data = array();

      $sql = "SELECT
  `UserID`,
  `UserName`,
  `FirstName`,
  `MiddleName`,
  `LastName`,
  `Password`,
  `RoleID`,
  `CreatedBy`,
  date_format(`CreatedOn`,'%Y-%m-%d') as CreatedOn,
  `ModifiedBy`,
  date_format(`ModifiedOn`,'%Y-%m-%d') as ModifiedOn,
  ifnull(`IsActive`, 0) as IsActive,
  ifnull(`Isdeleted`, 0) as Isdeleted,
  `Email`,
  `AggregatorID`,
  `CountryID`,getmaster
  `StateID`,
  `DistrictID`,
  `PlantID`,
  `ToolID`
FROM
  `mstuser` where UserName = '".$username."' and Password = '".$password."'";
      $data['MstUser'] = $this->db->query($sql)->result();

			$sql = "SELECT
  `CountryID`,
  `CountryCode`,
  `CountryName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstcountry`";
			$data['MstCountry'] = $this->db->query($sql)->result();
      
      $sql = "SELECT
  `StateID`,
  `CountryID`,
  `StateCode`,
  `StateName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mststate`";
      $data['Mststate'] = $this->db->query($sql)->result();
      
      $sql = "SELECT `DistrictID`, `StateID`, `CountryID`, `DistrictCode`, replace(`DistrictName`,'\'','`') as DistrictName, ifnull(`IsDeleted`,0) as IsDeleted FROM `mstdistrict`";
      $data['MSTDistrict'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `BlockID`,
  `DistrictID`,
  `StateID`,
  `CountryID`,
  `BlockCode`,
  `BlockName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstblock`";
      $data['MstBlock'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `ParameterID`,
  `ParameterCode`,
  `Name`,
  `uoM`,
  `Classification`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstparameter`";
      $data['MSTParameter'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `SourceID`,
  `SourceName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstsource`";
      $data['MSTSource'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `SeasonID`,
  `SeasonName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstseason`";
      $data['MstSeason'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `WeatherID`,
  `WeatherName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstweather`";
      $data['MstWeather'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `StandardID`,
  `StandardCode`,
  `StandardName`,
  `CountryID`,
  ifnull(`IsDeleted`,0) as IsDeleted,
  `Sequence`
FROM
  `mststandard`";
      $data['MSTstandard'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `StandardParameterID`,
  `ParameterID`,
  `StandardID`,
  `AcceptableLimit`,
  `uoM`,
  `Conversion`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mststandardparameterdetail`";
      $data['MSTStandardParameterDetail'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `TechnologyID`,
  `TechnologyName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `msttechnology`";
      $data['MSTTechnology'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `TreatmentID`,
  `GUID`,
  `TreatmetntCode`,
  `Description`,
  `ProcessDesc`,
  `ProcessDiagramLink`,
  `FlowChartLink`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `msttreatment`";
      $data['MSTTreatment'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `VillageID`,
  `BlockID`,
  `DistrictID`,
  `StateID`,
  `CountryID`,
  `VillageCode`,
  `VillageName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstvillage`";
      $data['MstVillage'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `TreatmentPercentID`,
  `TreatmentQualityID`,
  `TreatmentQuality`,
  `LowerPercent`,
  `UpperPercent`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `msttreatmentpercent`";
      $data['MstTreatmentPercent'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `TreatmentTechnologyID`,
  `ParameterID`,
  `TechnologyID`,
  `TreatmentPercentID`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `msttreatmenttechnology`";
      $data['MstTreatmentTechnology'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `WaterTreatmentID`,
  `WaterTreatmentName`,
  ifnull(`IsDeleted`,0) as IsDeleted
FROM
  `mstwatertreatment`";
      $data['MstWaterTreatment'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `WTechnologyID`,
  `WTechnologyName`,
  `Capacitycost`,
  `OperationCost`,
  `ReliableWaterQuality`,
  `EnvironmentalImpact`,
  ifnull(`IsDeleted`, 0) as IsDeleted
FROM
  `mstwatertreatmenttechnology`";
      $data['MstWaterTreatmentTechnology'] = $this->db->query($sql)->result();

      $sql = "SELECT
  `ContainerID`,
  `ContainerType`,
  ifnull(`IsDeleted`, 0) as IsDeleted
FROM
  `mstcontainertype`";
      $data['MstContainerType'] = $this->db->query($sql)->result();

			echo json_encode($data);

			}
	}

	public function GetUserData()
	{
		header("Content-type: application/json");

	if($this->input->post('UserName') === null || $this->input->post('Password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('UserName'));
		$password = $this->security->xss_clean($this->input->post('Password'));

		$authlogin = $this->authenticate($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

      $data = array();
      //write code for user data tables here

      $userid =  $this->getuserid($username, $password);

      $sql = "SELECT
  `LocationUID`,
  `GUID`,
  `CountryID`,
  `StateID`,
  `DistrictID`,
  `BlockName`,
  `VillageName`,
  `SourceID`,
  `WeatherID`,
  `GroundWaterDepth`,
  `Xvalue`,
  `Yvalue`,
  `PhotoLink`,
  `SynchedBy`,
  date_format(`SynchedOn`, '%Y-%m-%d') as SynchedOn,
  ifnull(`IsTablet`,0) as IsTablet,
  ifnull(`IsDeleted`,0) as IsDeleted,
  `IsWasteHeatAvailable`,
  `IsElectricityAvailable`,
  `UserID`,
  `CityName`,
  `SeasonID`,
  `ifnull(`CreatedBy`,0) as CreatedBy`,
  `CreatedOn`,
  `ifnull(`UpdatedBy`,0) as UpdatedBy`,
  `UpdatedOn`
FROM
  `tbltstlocation` where userid = ".$userid;
      $data['tblTSTLocation'] = $this->Common_model->query_data($sql);

      $sql = "select GUID from tbltstlocation where userid = ".$userid;
      $res_loc_guids = $this->Common_model->query_data($sql);

      foreach ($res_loc_guids as $row) {
          $guids .= "'".$row->GUID."',";
      }

      $guids = substr($guids, 0,-1);

      $sql = "SELECT
  `SampleUID`,
  `SampleGUID`,
  `LocationGUID`,
  `CollectedBy`,
  date_format(`CollectedDate`,'%Y-%m-%d') as CollectedDate,
  `CollectedTime`,
  `Address`,
  `Mobile`,
  `ContainerType`,
  `Quantity`,
  `AgencyName`,
  date_format(`CourieredDate`,'%Y-%m-%d') as CourieredDate,
  `CourieredTime`,
  `Contact`,
  ifnull(`IsTablet`,0) as IsTablet,
  ifnull(`IsDeleted`,0) as IsDeleted,
  `CreatedOn`,
  `ifnull(`CreatedBy`,0) as CreatedBy`,
  `UpdatedOn`, 
  `ifnull(`UpdatedBy`,0) as UpdatedBy`,
  `ifnull(`Pincode`,0) as Pincode`
FROM
  `tbltstsampleinfo` where LocationGUID in (".$guids.")";
      $data['tblTSTSampleInfo'] = $this->Common_model->query_data($sql);

      // $sql = "select * from tblTSTLaboratoryDetail where LocationGUID in (".$guids.")";
      // $data['tblTSTLaboratoryDetail'] = $this->Common_model->query_data($sql);

      $sql = "SELECT
  `TestGUID`,
  `LabGUID`,
  `LocationGUID`,
  `SampleGUID`,
  `ParameterID`,
  `ParameterValue`,
  `ImpurityFlag`,
  ifnull(`IsTablet`,0) as IsTablet,
  ifnull(`IsDeleted`,0) as IsDeleted,
  `CreatedOn`,
  `ifnull(`CreatedBy`,0) as CreatedBy`,
  `ifnull(`UpdatedBy`,0) as UpdatedBy`,
  `UpdatedOn`
FROM
  `tbltstsampletestresult` where LocationGUID in (".$guids.")";
      $data['tblTSTSampleTestResult'] = $this->Common_model->query_data($sql);

      $sql = "SELECT
    `LabUID`,
    `LabGUID`,
    `SampleGUID`,
    `LocationGUID`,
    `LabName`,
    `Address`,
    `ReportID`,
    `ReportDate`,
    `CreatedOn`,
    `ifnull(`CreatedBy`,0) as CreatedBy`,
    `IsTablet`,
    `IsDeleted`,
    `Contact`,
    `LabTestReport`,
    `UserType`,
    `UpdatedOn`,
    `ifnull(`UpdatedBy`,0) as UpdatedBy`,
    `ifnull(`Pincode`,0) as Pincode`
FROM
    `tbltstlaboratorydetails`
WHERE
    LocationGUID in  (".$guids.")";
      $data['tblTSTLaboratoryDetails'] = $this->Common_model->query_data($sql);

			echo json_encode($data);

		}
	}

	public function uploadData()
	{
		
		$this->load->model('Tstapi_model');
		$data = $this->input->post('data');
		$data_array = json_decode($data);
   
 		if($this->input->post('UserName') === null || $this->input->post('Password') === null)
		{
			echo json_encode("ERROR : You must send username and password with the request");
			die();
		}
		$username = $this->security->xss_clean($this->input->post('UserName'));
		$password = $this->security->xss_clean($this->input->post('Password'));

	 	$authlogin = $this->authenticate($username, $password);

		if(!$authlogin)
		{
			echo json_encode("Incorrect username and/or password");
		}
		else
		{
		
		if($data_array == null)
		{
			echo json_encode("ERROR: Please send properly formatted json");	
		}
		else
		{
			$res = $this->Tstapi_model->uploaddata();
			
		}

		}




	}


}