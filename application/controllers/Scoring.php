<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scoring extends CI_Controller 
{
	public $model = '';
	public $view  = '';
	public $valid = array();
	
	public function Scoring()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
	    $this->model = $this->$mod;
		
		$this->view['title']  = ucfirst($this->router->method).' '. ucfirst(str_replace('_',' ',$this->router->class));
		//$this->view['status'] = array(0=>'Inactive',1=>'Active',2=>'Deleted');
		
		//SET VALIDATION RULES
		$this->valid = array(
				   
				   array(
						 'field'   => 'form[OldPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' OldPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				  array(
						 'field'   => 'form[NewPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' NewPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				  array(
						 'field'   => 'form[ConfrimPassword]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' ConfrimPassword', 
						 'rules'   => 'trim|required'
				   ),
				   
				 );
		 $this->form_validation->set_rules($this->valid);
		//$this->form_validation->set_rules('form[email]', ucfirst(str_replace('_',' ',$this->router->class)).' email', 'callback_name_validation');
		$this->form_validation->set_rules('form[UserName]', ' username', 'callback_username_validation');
		
		
		$this->load->view(FTOP, $this->view);
		$this->load->view(NAVTOP, $this->view);		
	}
	
	/**
	 * Method index() get all accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function index()
	{
	   	$this->load->helper(array('form','url'));
		//$data['UserName'] = $this->model->getUserName($_SESSION['USERID']);
		
		//$this->load->view($this->router->class.'/'.$this->router->method,$data);
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method add() add new accessory.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function add()
	{
		//SET VALIDATION RULES
		$this->valid[] = array(
						 'field'   => 'form[FirstName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' FirstName', 
						 'rules'   => 'trim|required'
				   		);

		$this->valid[] = array(
				    	 'field'   => 'form[MiddleName]', 
						 'label'   => ucfirst(str_replace('_',' ',$this->router->class)).' MiddleName', 
						 'rules'   => 'trim|required|min_length[8]|max_length[20]'
				 		);
				 
		if($this->input->post($this->router->method))
		{
			if($this->form_validation->run())
			{
				if($this->model->add() == '1'){
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' added successfully!!');
					redirect($this->router->class);
				}
				else
				{
					$this->session->set_flashdata('message', ucfirst(str_replace('_',' ',$this->router->class)).' not added!!');
					redirect($this->router->class.'/'.$this->router->method);
				}
			}
		}
		
		$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	
	
	
	 /**
	 * Method view() view accessory detail.
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function view()
	{	
		ini_set('max_execution_time', 300);

		$token  = ($this->uri->segment(URI_SEGMENT)) ? $this->uri->segment(URI_SEGMENT) : 0;
		$patpod = $this->model->getPatPodDetail($token);
		$poguid = $patpod->POGUID;

		$content['Tab_Social_Sustainability'] 	= $this->model->getPATSocial($token);
		$content['Tab_Operational'] 			= $this->model->getPATOperational($token);
		$content['Tab_Financial'] 				= $this->model->getPATfinancial($token);
		$content['Tab_Institutional'] 			= $this->model->getPATInstitutional($token);
		$content['Tab_Environmental'] 			= $this->model->getPATEnvironmental($token,$poguid);
		// $content['Tab_TotalScore'] 				= $this->model->getPATSofiescore($token);
		$content['Tab_TotalScore'] 				= $this->model->getTotalScore($token);
		// print_r($content['Tab_TotalScore']); die();
			
		$content['subview']= $this->router->class.'/'.$this->router->method;
	    $this->load->view($this->router->class.'/'.$this->router->method, $content);	

		//$this->load->view($this->router->class.'/'.$this->router->method, $this->view);
		$this->load->view(FBOTTOM, $this->view);
	}
	
	/**
	 * Method name_validation() check unique email.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function name_validation()
	{
		return $this->model->name_validation();
	}
	
	
	/**
	 * Method username_validation() check unique username.
	 * @access	public
	 * @param	
	 * @return	string
	 */
	public function username_validation()
	{
		return $this->model->username_validation();
	}
}